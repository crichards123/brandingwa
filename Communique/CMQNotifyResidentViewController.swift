//
//  CMQNotifyResidentViewController.swift
//  Communique
//
//  Created by Andre White on 2/2/18.
//  Copyright © 2018 Communique, LLC. All rights reserved.
//

import UIKit
import ParseUI
import SVProgressHUD
public class CMQNotifyResidentViewController: CMQQueryTableViewController {
    @IBInspectable public var enabledColor: UIColor?
    @IBInspectable public var disabledColor:UIColor?
    @IBOutlet public weak var sendNotificationsButton:UIButton!
    @IBOutlet public weak var selectButton: UIBarButtonItem!
    private var transitionHandler:CMQTransitionDelegate?
    private var selected = Array<IndexPath>()
    override public func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.tableFooterView = UIView.init()

    }
    public override func initView() {
        //
    }
    public override func queryForTable() -> PFQuery<PFObject> {
        return PFQuery.residentsToNotifyQuery()
    }
    public func backToHome(){
        self.performSegue(withIdentifier: "BackToHome", sender: nil)
    }
    public func updateSelectButton(){
        selectButton.title = selected.count == self.objects?.count ? "Deselect All" : "Select All"
    }
    public func updateSendButton(){
        sendNotificationsButton.setTitle(selected.count > 0 ? "SEND NOTIFICATIONS": "SEND ALL NOTIFICATIONS", for: .normal)
    }
    public override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath, object: PFObject?) -> PFTableViewCell? {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "reuse", for: indexPath) as? CMQPackageDetailsTableViewCell, let user = object as? CMQUser else{return PFTableViewCell()}
        cell.checkView.isHighlighted = selected.contains(indexPath)
        cell.residentNameLabel.text = user.fullName
        cell.buildingButton.setTitle(user.buildingUnit, for: .normal)
        cell.selectionStyle = .none
        return cell
    }
    public func prepareTransition(){
        transitionHandler = CMQTransitionDelegate.init(size: CGSize.init(), origin: CGPoint.init())
        
    }
    public override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let index = selected.index(of: indexPath){
            selected.remove(at: index)
        }else{
            selected.append(indexPath)
        }
        updateSendButton()
        tableView.reloadData()
    }
    @IBAction public func notificationPressed(){
        SVProgressHUD.show(withStatus: "Notifying Residents")
        var residents = Array<CMQUser>()
        if selected.count == 0{
            residents = objects as! [CMQUser]
        }else{
            for path in selected {
                residents.append(object(at: path) as! CMQUser)
            }
        }
        CMQPackageDataSourceManager.sharedInstance().notify(residents: residents) { (error) in
            SVProgressHUD.dismiss()
            if let error = error{
                handle(error: error, message: "Problem Notifying Residents", shouldDisplay: true)
            }else{
                self.performSegue(withIdentifier: "Success", sender: nil)
            }
        }
    }
    public override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Success"{
            if let dest = segue.destination as? CMQPackageSuccessViewController{
                self.prepareTransition()
                dest.transitioningDelegate = self.transitionHandler
                dest.modalPresentationStyle = .custom
                dest.prompt = "Residents(s)"
            }
        }
    }
}
