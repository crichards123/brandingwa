//
//  BleDataHandler.h
//  legic-sdk
//
//  Created by admin on 03/10/2014.
//  Copyright (c) 2014 Legic. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    BleBatteryStatus_OK,
    BleBatterStatus_Low
} BleBatteryStatus;

@interface BleDataHandler : NSObject

/**
 * Initialises with the code, length and data supplied by BLE reader.
 *
 * @param code    The code for the data.
 * @param message The message returned from the BLE plugin.
 *
 * @return BleDataHandler instance.
 */
-(id)initWithCode:(int)code Message:(NSData*)message;

/**
 * Returns String Representation of received message.
 *
 * @return String representation of received message.
 */
-(NSMutableString*) getMessageString;

/**
 * Returns whether access was granted.
 *
 * @return True if access was granted.
 */
-(BOOL)isAccessGranted;

/**
 * Returns the error code returned for access.
 *
 * @return NSInteger containing the access error code.
 */
-(NSInteger)getAccessError;

/**
 * Returns the access flags.
 *
 * @return NSInteger the access flags.
 */
-(NSInteger)getFlags;

/**
 * Returns the battery status.
 *
 * @return BleBatteryStatus the status of the battery.
 */
-(BleBatteryStatus)getBatteryStatus;

/**
 * Returns the voltage of the battery.
 *
 * @return NSString containing the voltage of the battery.
 */
-(NSString*)getBatteryVoltage;
@end
