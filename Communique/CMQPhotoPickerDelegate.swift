//
//  CMQPhotoPickerDelegate.swift
//  Communique
//
//  Created by Andre White on 1/24/18.
//  Copyright © 2018 Communique, LLC. All rights reserved.
//

import UIKit
/**
 Photo Picker delegate with camera or library picker. photoCompletionBlock should be initialized before use with a closure handling the image that was chosen.
 
 */
public class CMQPhotoPickerDelegate: NSObject, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    public var photoCompletionBlock: ((UIImage?)->Void)!
    public var controller: UIViewController
    private var shouldEdit:Bool
    public var popOverView:UIView!
    public var alertController: UIAlertController!{
        let alert = UIAlertController(title: "Select Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(cameraAction)
        alert.addAction(libraryAction)
        alert.addAction(cancelAction)
        if let aView = self.popOverView{
            alert.popoverPresentationController?.sourceView = aView
        }
        return alert
    }
    
    private var cancelAction:UIAlertAction!{
        return UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil)
    }
    private var libraryAction: UIAlertAction!{
        return UIAlertAction.init(title: "Photo Library", style: .default) { (action) in
            guard let photoPicker = self.photoPicker else {return}
            self.controller.present(photoPicker, animated: true, completion: nil)
        }
    }
    private var cameraAction: UIAlertAction!{
        return UIAlertAction.init(title: "Take Photo", style: .default) { (action) in
            guard let cameraPicker = self.cameraPicker else {return}
            self.controller.present(cameraPicker, animated: true, completion: nil)
        }
    }
    private var photoPicker: UIImagePickerController?{
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
            let controller = UIImagePickerController()
            controller.sourceType = .photoLibrary
            controller.delegate = self
            controller.allowsEditing = shouldEdit
            return controller
        }
        return nil
    }
    private var cameraPicker: UIImagePickerController?{
        if UIImagePickerController.isSourceTypeAvailable(.camera){
            let controller = UIImagePickerController()
            controller.sourceType = .camera
            controller.delegate = self
            controller.allowsEditing = shouldEdit
            return controller
        }
        return nil
        
    }
    public required init(controller: UIViewController, wEditing:Bool = true) {
        self.controller = controller
        self.shouldEdit = wEditing
        super.init()
        
    }
    public func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        controller.dismiss(animated: true, completion: nil)
    }
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.editedImage] as? UIImage{
            controller.dismiss(animated: true, completion: {
                self.photoCompletionBlock(image)
            })
            
        }else if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage{
            self.photoCompletionBlock(image)
        }
    }

}
extension UIImage{
    static func cropImage(info:[UIImagePickerController.InfoKey : Any])->UIImage?{
        guard let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage else {return nil}
        var rect = info[UIImagePickerController.InfoKey.cropRect] as? CGRect
        UIGraphicsBeginImageContext((rect?.size)!)
        let context = UIGraphicsGetCurrentContext()!
        switch image.imageOrientation{
        case .up:
            context.translateBy(x: 0, y: image.size.height)
            context.scaleBy(x: 1, y: -1)
            rect = CGRect.init(x: (rect?.origin.x)!, y:-(rect?.origin.y)!, width: (rect?.size.width)!, height: (rect?.size.height)!)
        case .down:
            context.translateBy(x: image.size.width, y: 0)
            context.scaleBy(x: -1, y: 1)
            rect = CGRect.init(x: -(rect?.origin.x)!, y:(rect?.origin.y)!, width: (rect?.size.width)!, height: (rect?.size.height)!)
        case .right:
            context.scaleBy(x: 1, y: -1)
            context.rotate(by: CGFloat(-.pi/2.0))
            rect = CGRect.init(x: (rect?.origin.x)!, y:-(rect?.origin.y)!, width: (rect?.size.width)!, height: (rect?.size.height)!)
        default:
            print("someThing")
        }
        context.translateBy(x: -(rect?.origin.x)!, y: -(rect?.origin.y)!)
        context.draw(image.cgImage!, in: CGRect.init(x: 0, y: 0, width: image.size.width, height: image.size.height))
        let cropped = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return cropped
        
    }
}
