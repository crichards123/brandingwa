//
//  CMQQueryCollectionViewController.swift
//  Communique
//
//  Created by Andre White on 1/11/18.
//  Copyright © 2018 Communique, LLC. All rights reserved.
//

import UIKit
import ParseUI
import SVProgressHUD

private let reuseIdentifier = "PackageCell"
private let segueIdentifier = "view"
public class CMQQueryCollectionViewController: PFQueryCollectionViewController {
    @objc public var isCheckIn: ObjCBool = true

    override public func queryForCollection() -> PFQuery<PFObject> {
        let residentQuery = PFQuery.residentQuery()
        let packageQuery = isCheckIn.boolValue ? PFQuery.packageQuery() : PFQuery.packagesAlreadyCheckedOutQuery()
        residentQuery.whereKey("objectId", matchesKey: "recipientID", in: packageQuery)
        return residentQuery
    }
    override public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (self.view.frame.size.width/2)-1, height: 180)
    }
    override public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 2
    }
    override public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    override public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath, object: PFObject?) -> PFCollectionViewCell? {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as? CMQPackageCollectionViewCell, let resident = object as? CMQUser else {return nil}
        cell.residentNameLabel.text = resident.fullName
        cell.buildingLabel.text = resident.buildingUnit
        return cell 
    }
    /**
     Gets packages for resident and performs segue after completion, unless there was an error. Subclass should grab packages from sender variable and filter it accordingly. 
 */
    override public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let residentAtIndex = object(at: indexPath) as? CMQUser else{ return}
        collectionView.allowsSelection = false
        let completion = { (packages:[CMQPackage]?, error:Error?) in
            collectionView.allowsSelection = true
            if let error = error{
                //unable to load packages for resident
                handle(error: error, message: "Unable to load packages for resident. Please try again later", shouldDisplay: true)
            }else if let packages = packages{
                self.performSegue(withIdentifier: segueIdentifier, sender: packages)
            }
        }
        CMQPackageDataSourceManager.sharedInstance().packagesFor(resident: residentAtIndex, isCheckIn: isCheckIn.boolValue, completion: completion)
    }
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override public func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let packagesForSelectedResident = sender as? [CMQPackage], let destinationVC = segue.destination as? CMQPackageLogTableViewController, let separator = Array<CMQPackage>.Separator(rawValue: (self.isCheckIn.boolValue ? "CheckIn":"CheckOut")) {
            destinationVC.packagesForResidentByDate = packagesForSelectedResident.separateBy(separator: separator) as? [[CMQPackageDataSourceProtocol]]
            destinationVC.isCheckIn = self.isCheckIn.boolValue
        }
    }
}
extension CMQQueryCollectionViewController: UISearchBarDelegate{
    
}
