//
//  CMQCommunityCollectionViewCell.swift
//  Communique
//
//  Created by Andre White on 2/28/18.
//  Copyright © 2018 Communique, LLC. All rights reserved.
//

import UIKit
import ParseUI
class CMQCommunityCollectionViewCell: PFCollectionViewCell {
    @IBOutlet weak var communityImageView:PFImageView!
    @IBOutlet weak var communityNameLabel: UILabel!
    @IBOutlet weak var checkedView: UIImageView!{
        didSet{
            checkedView?.layer.cornerRadius = checkedView.frame.height/2
        }
    }
    func reset() {
        self.imageView.image = nil
        self.communityNameLabel.text = ""
        self.checkedView.isHidden = true
        self.setNeedsDisplay()
    }
}
