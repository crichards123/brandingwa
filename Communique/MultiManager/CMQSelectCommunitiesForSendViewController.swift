//
//  CMQSelectCommunitiesForSendViewController.swift
//  Communique
//
//  Created by Andre White on 3/11/18.
//  Copyright © 2018 Communique, LLC. All rights reserved.
//

import UIKit
import ParseUI
public class CMQSelectCommunitiesForSendViewController: CMQCommunitiesViewController {
    public var pendingItem: FeedItemStruct!
    public var selectedApartments:[CMQApartment] = [CMQApartment]()
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        updateNavButton()
        navigationItem.hidesBackButton = false
    }
    public override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let index = selected.index(of: indexPath){
            selected.remove(at: index)
        }else{
            selected.append(indexPath)
        }
        //collectionView.reloadData()
        if let cell = collectionView.cellForItem(at: indexPath) as? CMQCommunityCollectionViewCell{
            cell.checkedView.isHidden = !cell.checkedView.isHidden
            cell.setNeedsDisplay()
        }
        
        //collectionView.reloadItems(at: [indexPath])
        updateNavButton()
    }
    public override func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    public override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = super.collectionView(collectionView, cellForItemAt: indexPath) as? CMQCommunityCollectionViewCell else {return UICollectionViewCell()}
        cell.checkedView.isHidden = !selected.contains(indexPath)
        cell.alpha = 1
        return cell
    }
    /*
    override public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath, object: PFObject?) -> PFCollectionViewCell? {
        guard let cell = super.collectionView(collectionView, cellForItemAt: indexPath, object: object) as? CMQCommunityCollectionViewCell else {return nil}
    
        cell.checkedView.isHidden = !selected.contains(indexPath)
        cell.alpha = 1
        return cell
    }*/
    override func navButtonPressed(_ sender: Any) {
        if selected.count >= 1{
            for indexPath in selected{
                guard let apt = self.dataSource.object(at: indexPath) as? CMQApartment else {return}
                selectedApartments.append(apt)
            }
            self.performSegue(withIdentifier: "Selected", sender: nil)
        }
    }
}
