//
//  CMQCommunitiesViewController.swift
//  Communique
//
//  Created by Andre White on 2/28/18.
//  Copyright © 2018 Communique, LLC. All rights reserved.
//

import UIKit
import ParseUI
private let reuseIdentifier = "communityCell"

public class CMQCommunitiesViewController: CMQCollectionViewController {
    public var selected:[IndexPath] = []
    @IBOutlet weak var viewButton: UIBarButtonItem!
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        //navigationItem.hidesBackButton = true
        /*if let nav = self.navigationController as? CMQNavigationController{
            nav.restoreNavBar()
        }*/
        viewButton.isEnabled = CMQAPIManager.sharedInstance().apartment != nil
    }
    /*override public func queryForCollection() -> PFQuery<PFObject> {
        let query = CMQApartment.query()
        let commIDs = CMQUser.current().communities.compactMap({($0 as! CMQApartment).objectId})
        query?.whereKey("objectId", containedIn: commIDs)
        query?.order(byAscending: "aptName")
        return query!
    }*/

    public override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as? CMQCommunityCollectionViewCell, let community = self.dataSource.object(at: indexPath) as? CMQApartment else {return UICollectionViewCell()}
        cell.alpha = 1
        if let file = community.aptPhoto{
            cell.communityImageView.file = file
            cell.communityImageView.loadInBackground()
        }else{
            cell.communityImageView.image = nil
        }
        cell.communityNameLabel.text = community.aptName
        if let apt = CMQAPIManager.sharedInstance().apartment{
            if community.objectId == apt.objectId{
                cell.alpha = 0.25
            }
        }
        return cell
    }
    /*
    override public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath, object: PFObject?) -> PFCollectionViewCell? {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as? CMQCommunityCollectionViewCell, let commmunity = object as? CMQApartment else {return nil}
        //cell.reset()
        cell.alpha = 1
        if let file = commmunity.aptPhoto{
            cell.communityImageView.file = file
            cell.communityImageView.loadInBackground()
        }else{
            cell.communityImageView.image = nil
        }
        cell.communityNameLabel.text = commmunity.aptName
        if let apt = CMQAPIManager.sharedInstance().apartment{
            if commmunity.objectId == apt.objectId{
                cell.alpha = 0.25
            }
        }
        cell.contentView.isUserInteractionEnabled = false;
        //cell.checkedView.isHidden = !selected.contains(indexPath)
    
        return cell
    }*/
    /*
    override public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let apartment = object(at: indexPath) as? CMQApartment{
            CMQAPIManager.sharedInstance().apartment = apartment
            self.performSegue(withIdentifier: "aptSelected", sender: nil)
        }
    }
    public override func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        guard let apartment = object(at: indexPath) as? CMQApartment else {return false}
        guard let curApt = CMQAPIManager.sharedInstance().apartment else {return true}
        return apartment.objectId != curApt.objectId
    }
    public override func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.row == objects.endIndex-1{
            loadNextPage()
        }
    }*/
    public func updateNavButton(){
        viewButton.isEnabled = selected.count>0
    }
    @IBAction func navButtonPressed(_ sender: Any) {
        if let apartments = self.dataSource.myObjects as? [CMQApartment]{
            CMQAPIManager.sharedInstance().apartments = apartments
            self.performSegue(withIdentifier: "aptSelected", sender: nil)
        }
    }
    @IBAction func selectCommunities(segue:UIStoryboardSegue){
        /*if let nav = self.navigationController as? CMQNavigationController{
            nav.restoreNavBar()
        }*/
        
    }

     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
    override public func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using [segue destinationViewController].
     // Pass the selected object to the new view controller.
        if segue.identifier == "aptSelected"{
            if let _ = segue.destination as? CMQHomeFeedViewController{
                
                //dest.restoreInitialState()
                //dest.removeObjects()
                //dest.loadObjects()
                //dest.willChangeSource(to: CMQHomeFeedItems())
                //dest.tableView.reloadData()
                //dest.tableView.setContentOffset(CGPoint.init(x: 0, y: 2), animated: true)
            }
        }
     }
    override public func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if identifier == "aptSelected"{
            guard let cell = sender as? UICollectionViewCell else {return false}
            guard let path = self.collectionView?.indexPath(for: cell) else {return false}
            guard let selectedApt = self.dataSource.object(at: path) else {return false}
            guard let apt = CMQAPIManager.sharedInstance().apartment else {CMQAPIManager.sharedInstance().apartment = selectedApt as? CMQApartment;return true}
            guard apt.objectId != selectedApt.objectId else {return false}
            CMQAPIManager.sharedInstance().apartment = selectedApt as? CMQApartment
            return true
            }
        return true
        }

}
