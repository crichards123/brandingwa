//
//  CMQTextField.swift
//  Communique
//
//  Created by Andre White on 2/5/18.
//  Copyright © 2018 Communique, LLC. All rights reserved.
//

import UIKit

public class CMQTextField: UITextField {
    public var editingPropertyType:PropertyType?

    public override func awakeFromNib() {
        super.awakeFromNib()
        self.delegate = self
        self.returnKeyType = .done
        let toolBar = UIToolbar.init(frame: CGRect.init(origin: CGPoint.init(x: 0, y: 0), size: CGSize.init(width: self.frame.width, height: 50)))
        let doneButton = UIBarButtonItem.init(barButtonSystemItem: .done, target: self, action: #selector(donePressed(sender:)))
        let prevButton = UIBarButtonItem.init(title: "Prev", style: .plain, target: self, action: #selector(prevPressed(sender:)))
        let nextButton = UIBarButtonItem.init(title: "Next", style: .plain, target: self, action: #selector(nextPressed(sender:)))
        let space = UIBarButtonItem.init(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        toolBar.items = [prevButton, nextButton, space, doneButton]
        self.inputAccessoryView = toolBar
        
    }
    @objc public func donePressed(sender:UIBarButtonItem){
        self.resignFirstResponder()
    }
    @objc public func nextPressed(sender:UIBarButtonItem){
        NotificationCenter.default.post(name: .nextPressed, object: nil)
    }
    @objc public func prevPressed(sender:UIBarButtonItem){
        NotificationCenter.default.post(name: .prevPressed, object: nil)
    }
}
extension CMQTextField: UITextFieldDelegate{
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.resignFirstResponder()
        //NotificationCenter.default.post(name: .returnPressed, object: nil)
        return true
    }
    
}
