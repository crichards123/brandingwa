//
//  CMQEventCell.h
//  Communique
//
//  Created by Colby Melvin on 2/24/17.
//  Copyright © 2017 Communique, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CMQEventCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *userPhoto;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *eventDateLabel;
@property (weak, nonatomic) IBOutlet UIImageView *eventPhotoImageView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageViewHeightConstraint;
@property (weak, nonatomic) IBOutlet UILabel *imageLoadingLabel;
@property (weak, nonatomic) IBOutlet UILabel *eventTitleLabel;
@property (weak, nonatomic) IBOutlet TTTAttributedLabel *eventContentLabel;
@property (weak, nonatomic) IBOutlet UIButton *eventDateStartButton;
@property (weak, nonatomic) IBOutlet UILabel *eventLocationLabel;
@property (weak, nonatomic) IBOutlet UIImageView *eventLocationIcon;
@property (weak, nonatomic) IBOutlet UIButton *eventAttendeesButton;
@property (weak, nonatomic) IBOutlet UIButton *eventAttendButton;

@end
