//
//  CMQPackagePromptView.swift
//  Communique
//
//  Created by Andre White on 4/5/18.
//  Copyright © 2018 Communique, LLC. All rights reserved.
//

import UIKit

public class CMQPackagePromptView: UIView {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var promptLabel: UILabel!
    @IBOutlet weak var pinLabel: UILabel!
    @IBOutlet weak var responseLabel: UILabel!
    @IBOutlet weak var yesButton: UIButton!
    @IBOutlet weak var noButton: UIButton!
    public var accepted: Bool = false
    public var currentState:PackageMessageState?{
        didSet{
            /*switch currentState {
            case .some(.Initial):
                packageViewInitial()
            case .some(.Prompted):
                packageViewConfirm()
            case .some(.Accepted):
                packageViewAccepted()
            case .some(.Declined):
                packageViewDeclined()
            default:
                return
            }*/
            self.handleViews()
        }
    }
    public var item:FeedItemStruct!{
        didSet{
            if item != nil{
                self.awakeFromNib()
            }
        }
    }
    public var resetDeliveryCompletion:((Error?)->Void)?{
        guard self.item.packageMessage == true else {return  nil}
        let completion = { (error:Error?)->Void in
            guard error == nil else { self.isUserInteractionEnabled = true;return}
            //self.nextState(forward: false)
            self.isUserInteractionEnabled = true
            self.currentState = .Initial
        }
        return completion
    }
    public var declineDeliveryCompletion:((Error?)->Void)?{
        guard self.item.packageMessage == true else {return nil}
        let completion = { (error:Error?)->Void in
            //declined delivery. Update UI: remove pacakge Prompt, Show package declined view.
            guard error == nil else { self.isUserInteractionEnabled = true;return}
            //self.packageViewDeclined()
            //self.nextState(forward: false)
             self.isUserInteractionEnabled = true
            self.currentState = .Declined
            DispatchQueue.main.asyncAfter(deadline: .now()+1.5) {
                NotificationCenter.default.post(name: .packageNotification, object: false)
            }
        }
        return completion
    }
    private var packageLabelRecognizer:UITapGestureRecognizer{
        return UITapGestureRecognizer.init(target: self, action: #selector(didTapDeliveryPrompt(gestureRecogizer:)))
    }
    public var acceptDeliveryCompletion:((Error?)->Void)?{
        guard self.item.packageMessage == true else {return nil}
        let completion = { (error:Error?)->Void in
            //delivery accepted.  Update UI: remove pacakge Prompt, show delivery accepted view.
            guard error == nil else { self.isUserInteractionEnabled = true;return}
            //self.nextState(forward: true)
            //self.packageViewAccepted()
            self.isUserInteractionEnabled = true
            self.currentState = .Accepted
            DispatchQueue.main.asyncAfter(deadline: .now()+1.5) {
                NotificationCenter.default.post(name: .packageNotification, object: true)
            }
        }
        return completion
    }
    
    @IBAction public func didDeclineDelivery(sender:UIButton){
        nextState(forward: false)
//        guard let completion = declineDeliveryCompletion else {return}
//        item.set(delivery:false, completion:completion)
    }
    @IBAction public func didAcceptDelivery(sender:UIButton){
        //guard let _ = acceptDeliveryCompletion else {return}
        nextState(forward: true)
        //packageViewConfirm()
        //NotificationCenter.default.post(name: .didRespondToPackagePrompt, object: self)
    }
    @IBAction public func respondingToConfirmation(sender:UIButton){
        switch sender {
        case noButton:
            nextState(forward: false)
            //didDeclineDelivery(sender: sender)
        default:
            nextState(forward: true)
            //acceptedDelivery()
        }
    }
    
//    public func acceptedDelivery(){
////        guard let completion = acceptDeliveryCompletion else {return}
////        item.set(delivery:true, completion:completion)
//    }
    @objc public func didTapDeliveryPrompt(gestureRecogizer:UITapGestureRecognizer){
        //self.responseLabel.removeGestureRecognizer(gestureRecogizer)
        self.nextState(forward: true)
        //self.packageViewInitial()
    }
    private func handlePromptLabel(){
        switch self.currentState {
        case .some(.Initial):
            promptLabel.isHidden = false
            promptLabel.text = "Do you want this package to be delivered to your apartment?"
        case .some(.Prompted):
            guard let apartment = CMQAPIManager.sharedInstance().apartment, let price = apartment.deliveryPrice else {return}
            promptLabel.isHidden = false
            if pinLabel == nil {
                promptLabel.text = String.init(format: "Confirm delivery to apartment. This will cost $%@", price)
            }
        case .some(.Accepted):
            promptLabel.text = "Do you want this package to be delivered to your apartment?"
            promptLabel.isHidden = false
            return
        case .some(.Declined):
            promptLabel.text = "Do you want this package to be delivered to your apartment?"
            promptLabel.isHidden = false
        default:
            return
            
        }
    }
    private func handleButtons(){
        switch self.currentState {
        case .some(.Initial):
            hideButtons(shouldHide: false)
            handleButtonAction(initial: true)
        case .some(.Prompted):
            hideButtons(shouldHide: false)
            handleButtonAction(initial: false)
        case .some(.Accepted):
            hideButtons(shouldHide: true)
            return
        case .some(.Declined):
           hideButtons(shouldHide: true)
        default:
            return
            
        }
    }
    private func hideButtons(shouldHide:Bool){
        yesButton.isHidden = shouldHide
        noButton.isHidden = shouldHide
    }
    private func handleResponseLabel(){
        switch self.currentState {
        case .some(.Initial):
            responseLabel.gestureRecognizers = nil
            responseLabel.isHidden = true
        case .some(.Prompted):
            responseLabel.isHidden = true
            responseLabel.gestureRecognizers = nil
        case .some(.Accepted):
            responseLabel.isHidden = false
            responseLabel.text = "Delivery Pending"
            responseLabel.gestureRecognizers = nil
        case .some(.Declined):
            responseLabel.isHidden = false
            self.responseLabel.addGestureRecognizer(self.packageLabelRecognizer)
            responseLabel.text = "Picking up myself"
        default:
            return
            
        }
    }
    private func handlePinLabel(){
        switch self.currentState {
        case .some(.Initial):
            self.pinLabel.isHidden = false
            if let item = item, let pin = item.packagePin{
                self.pinLabel?.text = "Pin Code: \(pin.intValue)"
            }
        case .some(.Prompted):
            self.pinLabel.isHidden = false
            guard let apartment = CMQAPIManager.sharedInstance().apartment, let price = apartment.deliveryPrice else {return}
            pinLabel.isHidden = false
            pinLabel.text = String.init(format: "Confirm delivery to apartment. This will cost $%@", price)
        case .some(.Accepted):
            self.pinLabel.isHidden = true
        case .some(.Declined):
            self.pinLabel.isHidden = false
            if let item = item, let pin = item.packagePin{
                self.pinLabel?.text = "Pin Code: \(pin.intValue)"
            }
        default:
            return
            
        }
    }
    private func handleViews(){
        handleButtons()
        if let _ = pinLabel{
            handlePinLabel()
        }
        handlePromptLabel()
        handleResponseLabel()
    }
    /*public func packageViewInitial(){
        self.promptLabel.text = "Do you want this package to be delivered to your apartment?"
        self.noButton.isHidden = false
        self.yesButton.isHidden = false
        self.responseLabel.isHidden = true
        if let item = item, let pin = item.packagePin{
            self.pinLabel?.text = "Pin Code: \(pin.intValue)"
        }
        self.responseLabel.removeGestureRecognizer(self.packageLabelRecognizer)
        handleButtonAction(initial: true)
    }
    public func packageViewDeclined(){
        self.responseLabel.text = "Picking up myself"
        self.noButton.isHidden = true
        self.yesButton.isHidden = true
        self.responseLabel.isHidden = false
        if let item = item, let pin = item.packagePin{
            self.pinLabel?.text = "Pin Code: \(pin.intValue)"
        }
        self.responseLabel.addGestureRecognizer(self.packageLabelRecognizer)
        self.setNeedsLayout()
        
    }
    public func packageViewAccepted(){
        self.responseLabel.isHidden = false
        self.responseLabel.text = "Delivery Pending"
        self.noButton.isHidden = true
        self.yesButton.isHidden = true
        self.responseLabel.removeGestureRecognizer(self.packageLabelRecognizer)
        self.setNeedsLayout()
        
    }
    
    public func packageViewConfirm(){
        guard let apartment = CMQAPIManager.sharedInstance().apartment, let price = apartment.deliveryPrice else {return}
        if pinLabel == nil {
            self.promptLabel.text = String.init(format: "Confirm delivery to apartment. This will cost $%@", price)
        }else{
            self.pinLabel?.text = String.init(format: "Confirm delivery to apartment. This will cost $%@", price)
        }
        
        handleButtonAction(initial: false)
    }*/
    public func packageViewNoDelivery(){
        self.responseLabel.isHidden = true
        self.noButton.isHidden = true
        self.noButton.isEnabled = false
        self.yesButton.isEnabled = false
        self.yesButton.isHidden = true
        self.pinLabel?.isHidden = false
        if let item = item, let pin = item.packagePin{
            self.pinLabel?.text = "Pin Code: \(pin.intValue)"
        }
        self.promptLabel.isHidden = true
    }
    private func handleButtonAction(initial:Bool){
        if initial{
            self.noButton.removeTarget(self, action: #selector(respondingToConfirmation(sender:)), for: .touchUpInside)
            self.yesButton.removeTarget(self, action: #selector(respondingToConfirmation(sender:)), for: .touchUpInside)
            self.noButton.addTarget(self, action: #selector(didDeclineDelivery(sender:)), for: .touchUpInside)
            self.yesButton.addTarget(self, action: #selector(didAcceptDelivery(sender:)), for: .touchUpInside)
        }else{
            self.noButton.removeTarget(self, action: #selector(didDeclineDelivery(sender:)), for: .touchUpInside)
            self.yesButton.removeTarget(self, action: #selector(didAcceptDelivery(sender:)), for: .touchUpInside)
            self.noButton.addTarget(self, action: #selector(respondingToConfirmation(sender:)), for: .touchUpInside)
            self.yesButton.addTarget(self, action: #selector(respondingToConfirmation(sender:)), for: .touchUpInside)
        }
    }
    public override func awakeFromNib() {
        /*self.layer.shadowRadius = 2
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOpacity = 0.5
        self.layer.shadowOffset = .zero*/
        guard let apartment = CMQAPIManager.sharedInstance().apartment, apartment.hasPackageDelivery else {packageViewNoDelivery();return}
        switch item?.deliveryStatus {
        case .some("initial"):
            self.currentState = .Initial
        case .some("YES"):
            self.currentState = .Accepted
        case .some("NO"):
            self.currentState = .Declined
        default:
            self.currentState = .Initial
        }
        
    }
}
extension CMQPackagePromptView{
    override public func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == #keyPath(CMQMessage.deliveryStatus){
            
        }
    }
    public enum PackageMessageState{
        case Initial
        case Prompted
        case Accepted
        case Declined
    }
    
    public func nextState(forward:Bool){
        
        switch self.currentState {
        case .some(.Initial):
            if forward{
                self.currentState = .Prompted
            }else{
                guard let completion = declineDeliveryCompletion else {return}
                self.isUserInteractionEnabled = false
                item.set(delivery:false, completion:completion)
            }
        case .some(.Prompted):
            if forward{
                guard let completion = acceptDeliveryCompletion else {return}
                self.isUserInteractionEnabled = false
                item.set(delivery:true, completion:completion)
            }else{
                guard let completion = declineDeliveryCompletion else {return}
                self.isUserInteractionEnabled = false
                item.set(delivery:false, completion:completion)
            }
        case .some(.Accepted):
            
            return
        case .some(.Declined):
            if forward{
                guard let completion = resetDeliveryCompletion else {return}
                self.isUserInteractionEnabled = false
                item.set(delivery: nil, completion: completion)
                
            }else{
                return
            }
        default:
            return
            
        }
        
    
    }
    
}
