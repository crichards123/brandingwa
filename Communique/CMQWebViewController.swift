//
//  CMQWebViewController.swift
//  Communique
//
//  Created by Andre White on 3/27/18.
//  Copyright © 2018 Communique, LLC. All rights reserved.
//

import UIKit
import WebKit
public class CMQWebViewController: CMQParentViewController, WKNavigationDelegate{
    private enum CMQWebViewButtonTypes:Int{
        case Back
        case Forward
    }
    @IBOutlet var toolBarButtons: [UIBarButtonItem]!{
        didSet{
            self.toolbarItems = toolBarButtons
        }
    }
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    public var url:URL!
    public var navTitle: String?
    override public func viewDidLoad() {
        super.viewDidLoad()
        self.webView.navigationDelegate = self
        self.navigationController?.setToolbarHidden(false, animated: true)
        updateToolBarButtons()
        var frame:CGRect
        if let toolBarHeight = self.navigationController?.toolbar.frame.height{
            frame = CGRect.init(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height-toolBarHeight-5)
        }else{
            frame = self.view.frame
        }
        self.showWebView(request: URLRequest.init(url: url), frame: frame, navTitle: navTitle?.capitalizingFirstLetter)
        self.view.bringSubviewToFront(activityIndicator)
    }
    override public func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setToolbarHidden(true, animated: true)
    }
    
    public func updateToolBarButtons(){
        for button in toolBarButtons{
            if let type = CMQWebViewButtonTypes(rawValue: button.tag){
                if navTitle == "Epproach Support"{
                    button.isEnabled = false
                }else{
                    switch type{
                    case .Back:
                        button.isEnabled = self.webView.canGoBack
                    case .Forward:
                        button.isEnabled = self.webView.canGoForward
                    }
                }
                
            }
        }
    }
    public func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        activityIndicator.stopAnimating()
        updateToolBarButtons()
    }
    public func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        activityIndicator.stopAnimating()
        handle(error: error, message: "Error Loading Page. Please try again later", shouldDisplay: false)
    }
    
    public func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        activityIndicator.stopAnimating()
        handle(error: error, message: "Error Loading Page. Please try again later", shouldDisplay: false)
    }
    @IBAction func tabButtonPressed(_ sender: UIBarButtonItem) {
        guard let type = CMQWebViewButtonTypes(rawValue:sender.tag) else {return}
        switch type {
        case .Back:
            self.webView.goBack()
        case .Forward:
            self.webView.goForward()
        }
        updateToolBarButtons()
        
    }
    

}
