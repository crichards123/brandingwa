//
//  CMQCheckInSelectResidentTableViewController.swift
//  Communique
//
//  Created by Andre White on 5/31/18.
//  Copyright © 2018 Communique, LLC. All rights reserved.
//

import UIKit
struct ResidentCellCreator:IDTableCellConfigureProtocol,IDTableViewCellCreatorProtocol {
    typealias ViewType = UITableView
    
    typealias CellType = UITableViewCell
    
    var dataSource: IDViewDataSourceProtocol

    
    var reuseIdentifier: String
    func configure(cell: UITableViewCell, atIndex: IndexPath) {
        guard let cell = cell as? CMQPackageDetailsTableViewCell else {return}
        guard let object = self.dataSource.object(at:atIndex) as? CMQUser else {return}
        cell.residentNameLabel.text = object.fullName
        cell.buildingButton.setTitle(object.buildingUnit, for: .normal)
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return cell(forTable: tableView, atIndex: indexPath)
    }
}

public class CMQCheckInSelectResidentTableViewController: IDTableViewController, IDSearchControllerProtocol {
    
    @objc public var ocrText:String?
    var searchController: UISearchController!{
        didSet{
            self.customize(controller:searchController)
            self.customize(searchBar:searchController.searchBar)
            if #available(iOS 11.0, *) {
                self.navigationItem.searchController = searchController
                self.navigationItem.hidesSearchBarWhenScrolling = false
            } else {
                self.tableView.tableHeaderView = searchController.searchBar
            }
        }
    }
    var resultsController: UITableViewController!{
        didSet{
            resultsController.tableView.delegate = self
            self.prepareResultsController()
        }
    }
    override public func viewDidLoad() {
        super.viewDidLoad()
        self.prepareDataSource()
        if let text = ocrText{
            //Sweet Spot time to get it to work. Need a more elegant solution
            DispatchQueue.main.asyncAfter(deadline: .now()+0.65) {
                self.searchController.searchBar.text = text
                self.searchController.searchBar.becomeFirstResponder()
                self.updateSearchResults(for: self.searchController)
            }
        }
    }
    public func prepareDataSource(){
        self.dataSource = CMQResidentContainer()
        self.cellCreator = ResidentCellCreator.init(dataSource: self.dataSource, reuseIdentifier: "reuse")
        self.loadResultsController()
        self.dataSource.willChangeState(to: .loading)
    }
    public func prepareResultsController(){
        if let controller = resultsController as? IDTableViewController{
            controller.dataSource = CMQResidentContainer()
            controller.dataSource.willChangeState(to: .loading)
            controller.cellCreator = ResidentCellCreator.init(dataSource: controller.dataSource, reuseIdentifier: "ResidentCell")
        }
    }
    public func updateSearchResults(for searchController: UISearchController) {
        self.search()
    }
    public override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    public override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let dataSource = tableView == self.tableView ? self.dataSource : (self.resultsController as! IDTableViewController).dataSource
        if let user = dataSource?.object(at: indexPath) as? CMQUser{
            self.performSegue(withIdentifier: "Select", sender: user)
        }
    }
    @IBAction func backFromCheckIn(sender:UIStoryboardSegue){
        
    }
    // MARK: - Navigation

    override public func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let controller = segue.destination as? CMQCheckInTableViewController{
            controller.dataSource = sender as? CMQPackageDataSourceProtocol
        }
    }

}
