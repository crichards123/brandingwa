//
//  CMQImageView.swift
//  Communique
//
//  Created by Andre White on 1/24/18.
//  Copyright © 2018 Communique, LLC. All rights reserved.
//

import UIKit
import ParseUI
public class CMQImageView: PFImageView {

    public override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    override public func layoutSubviews() {
        super.layoutSubviews()
        self.layer.cornerRadius = self.frame.height/2
        self.layer.borderWidth = 0.5
        self.layer.borderColor = UIColor.lightGray.cgColor
        self.layer.masksToBounds = true
    }

}
