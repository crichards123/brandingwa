//
//  CMQParentViewController.swift
//  Communique
//
//  Created by Andre White on 1/8/18.
//  Copyright © 2018 Communique, LLC. All rights reserved.
//

import UIKit
import WebKit
import ParseUI
public class CMQParentViewController: UIViewController, WKUIDelegate {
    /**
     Any textfields that are a part of the view. These should be added in storyBoard to any child of this controller.
    */

    @IBOutlet var textFields: [UITextField]!
    @IBOutlet var textViews: [UITextView]!
    @IBOutlet var carouselLabels:[UILabel]!
    @IBOutlet var carouselViews:[UIView]!
    public var offViewFrame:CGRect!
    public var onViewFrame:CGRect!
    public var animating:Bool = false
    private let appFeatures = ["Community News Feed","Package Deliveries on Demand", "Quick and Easy Rent Payment",]
    @IBOutlet weak var centerLabel: UILabel!{
        didSet{
            if centerLabel != nil{
                self.onViewFrame = centerLabel.frame
            }
        }
    }
    @IBOutlet weak var offViewLabel: UILabel!{
        didSet{
            if offViewLabel != nil{
                self.offViewFrame = offViewLabel.frame
            }
        }
    }
    
    /**
     UIImageView that will show the main apartments logo. As an IBoutlet it should be set in Storyboard.
    */
    @IBOutlet public var apartmentImageView:PFImageView?{
        didSet{
            if let _ = apartmentImageView{
                if let _ = UIApplication.shared.delegate as? CMQAppDelegate{
                    //App Delegate doesn't get the apartment until after signing in. This won't work until after some restructuring. 
                    //view.file = delegate.apartment.aptPhoto
                    //view.loadInBackground()
                }
            }
        }
    }
    public var webView:WKWebView = WKWebView.init(frame: .zero, configuration: WKWebViewConfiguration()){
        didSet{
            webView.uiDelegate = self
        }
    }
    public var allFields:[UIView]{
        var allFields = [UIView]()
        if textFields != nil{
            for field in textFields{
                allFields.append(field)
            }
        }
        if textViews != nil{
            for view in textViews{
                allFields.append(view)
            }
        }
        return allFields.sorted(by: {$0.tag<$1.tag})
    }
    public var timer:Timer?
    override public func viewDidLoad() {
        super.viewDidLoad()
        
    }
    override public func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(handleReturnPressed(sender:)), name: .prevPressed, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleReturnPressed(sender:)), name: .nextPressed, object: nil)
        if (textFields) != nil ||  (textViews) != nil{
            addGestureRecognizer()
        }
    }
    override public func viewDidLayoutSubviews() {
        if self is CMQLoginLandingViewController{
            if self.allFields.filter({$0.isFirstResponder}).first == nil{
                self.onViewFrame = centerLabel.frame
                self.offViewFrame = offViewLabel.frame
            }
        }
        super.viewDidLayoutSubviews()
    }
    public override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: .prevPressed, object: nil)
        NotificationCenter.default.removeObserver(self, name: .nextPressed, object: nil)
    }
    public func addGestureRecognizer(){
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(screenTapped))
        self.view.addGestureRecognizer(tap)
    }
    @objc public func handleReturnPressed(sender:Notification){
        if sender.name == .prevPressed{
            self.prevField()
        }else if sender.name == .nextPressed{
            self.nextField()
        }
    }
    /**
     Gets first responder from the textfields and resigns.
    */
    @objc public func screenTapped(){
        if let firstResponder = textFields?.filter({$0.isFirstResponder}).first {
            firstResponder.resignFirstResponder()
        }
        if let firstResponder = textViews?.filter({$0.isFirstResponder}).first {
            firstResponder.resignFirstResponder()
        }
    }
    public func showWebView(request:URLRequest, frame:CGRect?, navTitle:String?){
        if let frame = frame{
            webView.frame = frame
        }else{
            webView.frame = self.view.frame
        }
        if let navTitle = navTitle {
            self.navigationItem.title = navTitle
        }
        self.view.addSubview(webView)
        webView.load(request)
    }
    /**
     Add Done navigation button on left of navigation bar.
     This is used when the WebView is using the entire view. The default action is to dismiss the webView.
 
    */
    public func addDoneNavButton(){
        let NavBarbutton = UIBarButtonItem.init(barButtonSystemItem: UIBarButtonItem.SystemItem.done, target: self, action: #selector(removeWebView))
        self.navigationItem.leftBarButtonItem = NavBarbutton
        
    }
    /**
     Remove webView from screen and also the done NavBarButton and navigationItemTitle
     */
    @objc public func removeWebView(){
        UIView.animate(withDuration: 0.5, animations: {
            self.webView.frame.origin.y = self.view.frame.height
        }) { (completed) in
            if completed{
                self.webView.removeFromSuperview()
                self.navigationItem.leftBarButtonItem = nil
                self.navigationItem.title = ""
            }
        }
    }
    public func prevField(){
        if let firstResponder = allFields.filter({$0.isFirstResponder}).first{
            var field:UIView?
            if let index = allFields.index(of: firstResponder){
                if index == allFields.startIndex {
                    field = allFields[allFields.endIndex-1]
                }else{
                    field = allFields[allFields.index(before: index)]
                }
            }
            field?.becomeFirstResponder()
        }
    }
    public func nextField(){
        if let firstResponder = allFields.filter({$0.isFirstResponder}).first{
            var field:UIView?
            if let index = allFields.index(of: firstResponder){
                if index == allFields.endIndex-1 {
                    field = allFields[allFields.startIndex]
                }else{
                    field = allFields[allFields.index(after: index)]
                }
            }
            field?.becomeFirstResponder()
        }
    }
    public func carouselNext(){
        let views = self.carouselViews.sorted(by: {$0.tag<$1.tag})
        if let currentView = views.filter({$0.backgroundColor == .white}).first{
            guard let nextView = views.next(cur: currentView), let nextText = appFeatures.next(cur: centerLabel.text!) else {return}
            if !animating{
                UIView.animate(withDuration: 0.6, animations: {
                    currentView.backgroundColor = .DarkColor
                    nextView.backgroundColor = .white
                    self.centerLabel.slideInFromLeft()
                    self.centerLabel.text = nextText
                }, completion: { (finished) in
                    if finished{
                        
                    }
                })
            }
        }
    }
    public func carouselPrev(){
        let views = self.carouselViews.sorted(by: {$0.tag<$1.tag})
        let labels = self.carouselLabels.sorted(by: {$0.tag<$1.tag})
        var prevView:UIView?
        var prevLabel:UILabel?
        var curLabel:UILabel?
        if let currentView = views.filter({$0.backgroundColor == .white}).first{
            if let index = views.index(of: currentView){
                curLabel = labels[index]
                if index == views.startIndex{
                    prevView = views[views.endIndex - 1]
                    prevLabel = labels[labels.endIndex - 1]
                }else{
                    prevView = views[views.index(before: index)]
                    prevLabel = labels[labels.index(before: index)]
                }
            }
            guard let prevLabel = prevLabel, let _ = prevView, let curLabel = curLabel else {return}
            
            UIView.transition(from: curLabel, to: prevLabel, duration: 0.6, options: .transitionFlipFromLeft, completion: { (finished) in
                //Nothing to do
                
            })
            /*prevLabel.frame = (curLabel.frame)
            prevLabel.transform = CGAffineTransform.init(translationX: -self.view.frame.width, y: 0)
            UIView.animate(withDuration: 0.6, animations: {
                currentView.backgroundColor = .CMQDarkColor()
                prevView.backgroundColor = .white
                let transform = CGAffineTransform.init(translationX: -self.view.frame.width, y: 0)
                curLabel.transform = .identity
                prevLabel.transform = transform
                prevLabel.isHidden = false
            }, completion: { (finished) in
                if finished{
                    curLabel.isHidden = true
                    curLabel.transform = .identity
                }
            })*/
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
