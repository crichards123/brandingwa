//
//  CMQSuccessPresentationController.h
//  Communique
//
//  Created by Andre White on 9/22/17.
//  Copyright © 2017 Communique, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CMQSuccessPresentationController : UIPresentationController
@property(retain, nonatomic)UIView* dimmingView;
@end
