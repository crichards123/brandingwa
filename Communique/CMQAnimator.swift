//
//  CMQAnimator.swift
//  Communique
//
//  Created by Andre White on 1/19/18.
//  Copyright © 2018 Communique, LLC. All rights reserved.
//

import UIKit

public class CMQAnimator: NSObject, UIViewControllerAnimatedTransitioning {
    public var isDismiss: Bool
    public var fromBottom: Bool
    public init (dimiss:Bool, fromBottom:Bool){
        self.isDismiss = dimiss
        self.fromBottom = fromBottom
        super.init()
        
    }
    public override init() {
        isDismiss = false
        fromBottom = false
        super.init()
    }
    public func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return TimeInterval.init(0.35)
    }
    
    public func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
    guard let toVC = transitionContext.viewController(forKey: .to), let fromVC = transitionContext.viewController(forKey: .from) else {return}
        let containerView = transitionContext.containerView
        if isDismiss{
            if fromBottom{
                UIView.animate(withDuration: transitionDuration(using: transitionContext), animations: {
                    fromVC.view.transform = CGAffineTransform.init(translationX: 0, y: containerView.bounds.height)
                }, completion: { (finished) in
                    transitionContext.completeTransition(finished)
                })
            }else{
                UIView.animate(withDuration: transitionDuration(using: transitionContext), animations: {
                    fromVC.view.transform = CGAffineTransform.init(translationX: -containerView.bounds.width, y: 0)
                    //toVC.view.transform = .identity
                }, completion: { (finished) in
                    transitionContext.completeTransition(finished)
                })
            }
            
        }
        else{
            if fromBottom{
                toVC.view.transform = CGAffineTransform(translationX: 0, y: containerView.bounds.height)
                containerView.addSubview(toVC.view)
                UIView.animate(withDuration: transitionDuration(using: transitionContext), animations: {
                    toVC.view.transform = CGAffineTransform.identity
                }) { (finished) in
                    transitionContext.completeTransition(finished)
                }
            }else{
                
                toVC.view.transform = CGAffineTransform(translationX: -toVC.view.frame.width, y: 0)
                containerView.addSubview(toVC.view)
                UIView.animate(withDuration: transitionDuration(using: transitionContext), animations: {
                    toVC.view.transform = CGAffineTransform.identity
                    //fromVC.view.transform = CGAffineTransform(translationX: toVC.view.frame.width*0.75, y: 0)
                }) { (finished) in
                    transitionContext.completeTransition(finished)
                }
            }
            
        }
        
    }
    
    

}
