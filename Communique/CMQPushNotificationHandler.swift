//
//  CMQPushNotificationHandler.swift
//  Communique
//
//  Created by Andre White on 5/3/18.
//  Copyright © 2018 Communique, LLC. All rights reserved.
//

import UIKit
public enum CMQPushSetting{
    case All
    case UrgentOnly
    case None
    case Undetermined
    case Denied
    
    fileprivate var channelKey:String{
        switch self {
        case .All:
            return kParsePushChannelAllKey
        case .UrgentOnly:
            return kParsePushChannelUrgentKey
        default:
            return ""
        }
    }
    fileprivate func save(){
        switch self {
        case .All, .UrgentOnly, .None:
            if let firstChannel = PFInstallation.current()?.channels?.first{
                PFPush.unsubscribeFromChannel(inBackground: firstChannel) { (success, error) in
                    guard error == nil else {return}
                    if success{
                        PFInstallation.current()?.channels = [self.channelKey]
                        PFInstallation.current()?.saveInBackground()
                    }
                }
            }else{
                PFInstallation.current()?.channels = [self.channelKey]
                PFInstallation.current()?.saveInBackground()
            }
        default:
            return
        }
    }
}
@available(iOS 10.0, *)
public class CMQPushNotificationHandler: NSObject {
    

    public enum NotificationID:String{
        case Package = "packageNoti"
        case Delivery = "delivery"
        case Thermostat = "thermostat"
        case Lock = "lock"
        case LightGroup = "lightGroup"
        case Light = "light"
        case Socket = "socket"
        case Fan = "fan"
    }
    public var userPushSetting:CMQPushSetting?{
        didSet{
            userPushSetting?.save()
        }
    }
    private(set) var userNotificationPreference:String?
    @available(iOS 10.0, *)
    @objc public var notifcationCategories:Set<UNNotificationCategory>{
        let deliverAction = UNNotificationAction(identifier: "Deliver", title: "Deliver my package", options: UNNotificationActionOptions(rawValue:0))
        let dontDeliverAction = UNNotificationAction(identifier: "DontDeliver", title: "I'll pick it up", options:
            UNNotificationActionOptions(rawValue:0))
        let cat = UNNotificationCategory(identifier: NotificationID.Package.rawValue, actions: [deliverAction, dontDeliverAction], intentIdentifiers: [], options: UNNotificationCategoryOptions(rawValue: 0))
        return Set([cat])
    }
    
    private static let shared = CMQPushNotificationHandler()
    @objc public class func sharedInstance()->CMQPushNotificationHandler{
        return .shared
    }
    public func checkPushPermissions(completion:@escaping (CMQPushSetting)->Void){
        let notCenter = UNUserNotificationCenter.current()
        notCenter.getNotificationSettings { (setting) in
            
            switch setting.authorizationStatus{

            case .provisional:
                completion(.All)
            case .notDetermined:
            
                completion(.Undetermined)
            case .denied:
                completion(.Denied)
            case .authorized:
                if let channel = PFInstallation.current()?.channels?.first{
                    switch channel {
                    case kParsePushChannelUrgentKey:
                        completion(.UrgentOnly)
                    case kParsePushChannelAllKey:
                        completion(.All)
                    default:
                        return completion(.None)
                    }
                } else {
                    completion(.None)
                }
            }
        }
        
        
    }
    @available(iOS 10.0, *)
    @objc public func requestPushPermission(){
        
        let notCenter = UNUserNotificationCenter.current()
        notCenter.delegate = UIApplication.shared.delegate as? CMQAppDelegate
        notCenter.requestAuthorization(options: [.sound,.alert,.badge]) { (granted, error) in
            guard error == nil else {print("Push Registration Failed \n \(String(describing: error))");return}
            if granted{
                DispatchQueue.main.async(execute: {
                    UIApplication.shared.registerForRemoteNotifications()
                })
                
                self.userPushSetting = .All
            }else{
                self.userPushSetting = .Denied
            }
            
        }
    }
    public func initUserPreferences(){
        checkPushPermissions { (setting) in
            switch setting{
                
            case .Undetermined:
                DispatchQueue.main.async {
                    self.requestPushPermission()
                }
            case .Denied:
                self.userPushSetting = setting
            default:
                self.userPushSetting = setting
                DispatchQueue.main.async {
                    UIApplication.shared.registerForRemoteNotifications()
                }
            }
        }
        
    }
    @objc public func didRegister(token:Data){
        print("Did Register, saving device Token")
        let installation = PFInstallation.current()
        installation?.setDeviceTokenFrom(token)
        installation?.saveInBackground(block: { (success, error) in
            guard error == nil else {handle(error: error!, message: "Saving Installation Push Token", shouldDisplay: false);return}
            if success{
                print("Saved Push Token")
            }
        })
    
    }
    
    public func unsubscribe(){
        let installation = PFInstallation.current()
        if let channel = installation?.channels?.first{
            PFPush.unsubscribeFromChannel(inBackground: channel) { (success, error) in
                if let error = error{
                    handle(error: error, message: "Unsubscribing From Push", shouldDisplay: false)
                }else if success{
                    print("Successfully Unsubscribed")
                }
            }
        }
    }
    
    @available(iOS 10.0, *)
    @objc public func didReceive(response:UNNotificationResponse, completion:@escaping ()->Void){
        guard let notifID = NotificationID.init(rawValue: response.notification.request.content.categoryIdentifier) else { completion(); return}
        guard notifID == .Package else {completion();return}
        let message = response.notification.request.content.body
        let userResponse =  response.actionIdentifier == "Deliver" ? "YES" : "NO"
        let query = CMQMessage.query()!
        query.whereKey("messageContent", contains: message)
        query.whereKey("aptComplexID", equalTo: CMQUser.current().complexID)
        query.getFirstObjectInBackground { (object, error) in
            guard error == nil else {print(error!);return}
            guard let messageObj = object as? CMQMessage else {return}
            messageObj.deliveryStatus = userResponse
            messageObj.saveInBackground()
            completion()
        }
        
    }
    @available(iOS 10.0, *)
    @objc public func willPresent(notification:UNNotification, topController:UIViewController, completion:@escaping (UNNotificationPresentationOptions) -> Void){
        if let notID = NotificationID.init(rawValue: notification.request.content.categoryIdentifier){
            switch notID{
            case .Delivery:
                if topController is CMQCheckOutSearchTableViewController && notID == .Delivery{
                    topController.perform(#selector(CMQCheckOutSearchTableViewController.deliveryUpdate))
                }
            case .Package:
                print("package")
            case .Thermostat:
                DispatchQueue.main.asyncAfter(deadline: .now()+4.5) {
                    SmartHomeDataSource.shared.cachedDevices.removeValue(forKey: "therm")
                    CMQAPIManager.sharedInstance().smartAptUnit?.updateDevices()
                }
            case .Lock:
                DispatchQueue.main.asyncAfter(deadline: .now()+4.5) {
                    SmartHomeDataSource.shared.cachedDevices.removeValue(forKey: "lock")
                    CMQAPIManager.sharedInstance().smartAptUnit?.updateDevices()
                }
            default:
                DispatchQueue.main.asyncAfter(deadline: .now()+4.5) {
                    for room in CMQAPIManager.sharedInstance().smartAptUnit!.userRooms{
                        SmartHomeDataSource.shared.cachedDevices.removeValue(forKey: room.name)
                    }
                    CMQAPIManager.sharedInstance().smartAptUnit?.updateDevices()
                }
            }
        }
        else{
            if topController is CMQHomeFeedViewController{
                topController.perform(#selector(CMQHomeFeedViewController.refreshObjects))
            }else if topController is CMQFeedItemTableViewController{
                topController.perform(#selector(CMQFeedItemTableViewController.onRefresh))
            }else{
                completion([.alert, .badge, .sound])
            }
        }
        
    }
    @objc public func clearNotificationBadge(){
        guard let installation = PFInstallation.current() else {return}
        installation.badge = 0
        installation.saveInBackground()
        UIApplication.shared.applicationIconBadgeNumber = 0

    }
    @objc public func didReceive(payload:[AnyHashable : Any],completion:@escaping (UIBackgroundFetchResult) -> Void){
        let dict = payload["aps"] as! [String:Any]
        guard let _ = NotificationID.init(rawValue: dict["category"] as! String) else {PFPush.handle(payload);
            completion(.noData);return}
        completion(.noData)
        //guard let category = payload["category"] as? String, category != "delivery", category != "thermostat" else {completion(.noData);return}
        
    }
}
