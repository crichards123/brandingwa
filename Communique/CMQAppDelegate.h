//
//  CMQAppDelegate.h
//  Communique
//
//  Created by Chris Hetem on 9/15/14.
//  Copyright (c) 2014 WaveRider, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CMQApartment.h"
#import "CMQManagement.h"
#import <UserNotifications/UserNotifications.h>

@interface CMQAppDelegate : UIResponder <UIApplicationDelegate, UNUserNotificationCenterDelegate>

typedef enum{
    CMQAppearanceTypeMail,
    CMQAppearanceTypeDefault
}CMQAppearanceType;
@property(assign, nonatomic)BOOL shouldRotate;
@property (strong, nonatomic) UIWindow *window;

/**
 *  the apartment associated with the current user
 */
@property (strong, nonatomic) CMQApartment *apartment;

/**
 *  the management company associated with the current user's apartment
 */
@property (strong, nonatomic) CMQManagement *management;

/**
 *  the residents associated with the apartment associated with the current user
 */
@property (strong, nonatomic) NSMutableArray *currentResidents;

/**
 *  the notification queue
 */
@property (strong, nonatomic) NSMutableArray *notificationQueue;

/**
 *  whether or not we should check for available updates
 */
@property (assign, nonatomic) BOOL shouldCheckForUpdate;
@property (readonly) int aslLevelNotice;
@end
