//
//  CMQProfileCollectionViewController.m
//  Communique
//
//  Created by Colby Melvin on 12/20/16.
//  Copyright © 2016 Communique, LLC. All rights reserved.
//

#import "CMQProfileCollectionViewController.h"
#import "UIView+Animations.h"
#import <MessageUI/MessageUI.h>
#import "UIApplication+AppInfo.h"
#import "NSString+NSHash.h"

static NSString *CMQProfileCellIdentifier = @"CMQProfileTableViewCell";

@interface CMQProfileCollectionViewController () <UITableViewDelegate, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate>
{
    CMQUser *currentUser;
    PFInstallation *currentInstallation;
    NSMutableArray *profileArray;
    NSMutableArray *userSettingsArray;
    NSMutableArray *userCommunicationArray;
    NSMutableArray *communitySettingsArray;
    NSMutableArray *associatedUserArray;
    NSInteger firstNameTag, lastNameTag, emailTag, phoneTag, passwordTag, currentTag;
    
    NSString *firstName, *lastName, *username, *phone;
    BOOL push, pushAll, pushUrgent, sms, email;
    
    UIButton *userPhoto;
}

@end

@implementation CMQProfileCollectionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.screenName = @"Profile";
    
    firstNameTag = 9999;
    lastNameTag = 9998;
    emailTag = 9997;
    phoneTag = 9996;
    passwordTag = 9995;
    
    [self getUserDetails];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/**
 *  configures the UI with the user's details
 */
-(void)getUserDetails{
    currentTag = 0;
    
    // init arrays
    profileArray = [[NSMutableArray alloc] init];
    userSettingsArray = [[NSMutableArray alloc] init];
    userCommunicationArray = [[NSMutableArray alloc] init];
    communitySettingsArray = [[NSMutableArray alloc] init];
    associatedUserArray = [[NSMutableArray alloc] init];
    
    //fetch user object
    currentUser = [[CMQUser currentUser]fetch];
    
    //fetch installation object
    currentInstallation = [[PFInstallation currentInstallation]fetch];
    
    /******************************************************************/
    /********************* User Settings ******************************/
    /******************************************************************/
    firstName = (currentUser.firstName && currentUser.firstName.length > 0) ? currentUser.firstName : @"Add Last Name";
    [userSettingsArray addObject:@[@"First Name", firstName, @YES, [NSNumber numberWithInteger:firstNameTag]]];
    
    lastName = (currentUser.lastName && currentUser.lastName.length > 0) ? currentUser.lastName : @"Add Last Name";
    [userSettingsArray addObject:@[@"Last Name", lastName, @YES, [NSNumber numberWithInteger:lastNameTag]]];
    
    username = (currentUser.username && currentUser.username.length > 0) ? currentUser.username : @"Add Email";
    [userSettingsArray addObject:@[@"Email", username, @YES, [NSNumber numberWithInteger:emailTag]]];
    
    phone = (currentUser.phone && currentUser.phone.length > 0) ? currentUser.phone : @"Add Number";
    [userSettingsArray addObject:@[@"Mobile Phone", phone, @YES, [NSNumber numberWithInteger:phoneTag]]];
    
    [userSettingsArray addObject:@[@"Password", @"Change Password", @YES, [NSNumber numberWithInteger:passwordTag]]];
    
    /******************************************************************/
    /********************* Communication Settings *********************/
    /******************************************************************/
    push = NO, pushAll = NO, pushUrgent = NO;
    if (currentInstallation.channels.count > 0){
        push = YES;
        
        for(NSString *channel in currentInstallation.channels){
            pushAll = [channel isEqualToString:kParsePushChannelAllKey];
            pushUrgent = [channel isEqualToString:kParsePushChannelUrgentKey];
            break;
        }
    }
    [userCommunicationArray addObject:@[@"Push Notifications", [NSNumber numberWithBool:push]]];
    [userCommunicationArray addObject:@[@"      All", [NSNumber numberWithBool:pushAll]]];
    [userCommunicationArray addObject:@[@"      Urgent", [NSNumber numberWithBool:pushUrgent]]];
    
    sms = (currentUser.texts && currentUser.phone && currentUser.phone.length > 0);
    [userCommunicationArray addObject:@[@"Urgent Text Messages", [NSNumber numberWithBool:sms]]];
    
    email = (currentUser.alerts && currentUser.email && currentUser.email.length > 0);
    [userCommunicationArray addObject:@[@"Email", [NSNumber numberWithBool:email]]];

    /******************************************************************/
    /********************* Community Info *****************************/
    /******************************************************************/
    NSString *aptName = (CMQAD.apartment.aptName) ? CMQAD.apartment.aptName : @"???";
    [communitySettingsArray addObject:@[@"Community", aptName, @NO]];

    NSString *buildingUnit = (currentUser.buildingUnit) ? currentUser.buildingUnit : @"???";
    if ([currentUser.userRole isEqualToString:kPermissionResident]) [communitySettingsArray addObject:@[@"Unit", buildingUnit, @NO]];
    
    if (![currentUser.userRole isEqualToString:kPermissionInterestedParty] && CMQAD.apartment.aptAccessCodes && [CMQAD.apartment.aptAccessCodes count] > 0){
        for (NSArray *array in CMQAD.apartment.aptAccessCodes){
            [communitySettingsArray addObject:@[array[0], array[1], @NO]];
        }
    }
    
    /******************************************************************/
    /********************* Associated Users ***************************/
    /******************************************************************/
    if (currentUser.associatedUsers && [currentUser.associatedUsers count] > 0){
        PFQuery *query = [CMQUser query];
        [query whereKey:@"objectId" containedIn:currentUser.associatedUsers];
        [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
            if (!error) {
                for (int i = 0; i < [objects count]; i ++){
                    CMQUser *associatedUser = objects[i];
                    
                    NSString *textLabelText = (associatedUser.fullName) ? associatedUser.fullName : associatedUser.username;
                    NSString *detailsTextLabelText = ([currentUser.userRole isEqualToString:kPermissionInterestedParty] && associatedUser.buildingUnit) ? associatedUser.buildingUnit : @"";
                    
                    [associatedUserArray addObject:@[textLabelText, detailsTextLabelText, @NO]];
                }
                
                if ([associatedUserArray count] > 0) [profileArray addObject:associatedUserArray];
                
                [self.tableView reloadData];
            }
        }];
    }
    
    if ([userSettingsArray count] > 0) [profileArray addObject:userSettingsArray];
    
    if ([userCommunicationArray count] > 0) [profileArray addObject:userCommunicationArray];
    
    if ([communitySettingsArray count] > 0) [profileArray addObject:communitySettingsArray];
    
    if ([associatedUserArray count] > 0) [profileArray addObject:associatedUserArray];
    
    [self.tableView reloadData];
}

//email validation
-(BOOL)isValidEmail:(NSString*)string{
    BOOL strictFilter = NO; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *strictFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxFilterString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = strictFilter ? strictFilterString : laxFilterString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    return [emailTest evaluateWithObject:string];
}

//phone validation
-(BOOL)isValidPhone:(NSString*)string{
    //only accept properly formatted phone numbers
    return ([self stripNonNumericCharacters:string].length == 10);
}

-(BOOL)isEmptyPhone:(NSString*)string{
    //only accept empty string
    return ([self stripNonNumericCharacters:string].length == 0);
}

//strip anything that isn't a digit
-(NSString*)stripNonNumericCharacters:(NSString*)string{
    NSCharacterSet *charactersToRemove = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
    return [[string componentsSeparatedByCharactersInSet:charactersToRemove] componentsJoinedByString:@""];
}


#pragma mark - Button Actions

/**
 * attempts to bring up photo library
 *
 * @param sender the user photo button
 */

- (IBAction)onSelectUserPhoto:(id)sender {
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc]init];
    imagePicker.delegate = self;
    imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    imagePicker.allowsEditing = YES;
    imagePicker.modalPresentationStyle = UIModalPresentationPopover;
    imagePicker.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    
    UIPopoverPresentationController *popover = imagePicker.popoverPresentationController;
    popover.sourceView = self.view;
    popover.sourceRect = CGRectMake(CGRectGetMidX(self.view.bounds),
                                    self.tableView.frame.origin.y + userPhoto.frame.size.height,
                                    0,
                                    0);
    
    [self presentViewController:imagePicker animated:YES completion:nil];
}

/**
 *  dismisses this view
 *
 *  @param sender the home button
 */
- (IBAction)onClose:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

/**
 *  logs the user out and dismisses this view
 *
 *  @param sender the logout button
 */
- (IBAction)onLogout:(id)sender {
    [[CMQAPIClient sharedClient]logout];
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - UISwitchView
/**
 *  handles UISwitch taps
 *  Tags 1 & 2 - subscribe/unsubscribe from push channels (push notifications)
 *  Tag 3 - toggle user.texts (sms messages)
 *  Tag 4 - toggle user.alerts (emails)
 *
 *  @param switchView the UISwitch
 */
- (void)switchValueChanged:(UISwitch *)switchView{
    UISwitch *switch1 = (UISwitch *)[self.view viewWithTag:1]; // all notifications
    UISwitch *switch2 = (UISwitch *)[self.view viewWithTag:2]; // urgent notifications
    
    switch (switchView.tag){
        case 1: // all push notifications
            if (switch1.isOn){
                if (switch2.isOn){ // turn off urgent
                    [switch2 setOn:NO animated:YES];
                    pushUrgent = NO;
                }
                
                pushAll = YES;
                [[CMQAPIClient sharedClient]subscribeToChannel:kParsePushChannelAllKey];
            }else if (!switch1.isOn && !switch2.isOn){
                [[CMQNotificationHandler sharedHandler]unsubscribeFromParsePush];
                pushAll = pushUrgent = NO;
            }
            break;
        case 2: // urgent push notifications
            if (switch2.isOn){
                if (switch1.isOn){ // turn off all
                    [switch1 setOn:NO animated:YES];
                    pushAll = NO;
                }
                
                pushUrgent = YES;
                [[CMQAPIClient sharedClient]subscribeToChannel:kParsePushChannelUrgentKey];
            }else if (!switch2.isOn && !switch1.isOn){
                [[CMQNotificationHandler sharedHandler]unsubscribeFromParsePush];
                pushUrgent = pushAll = NO;
            }
            break;
        case 3: // texts
            if (currentUser.phone && currentUser.phone.length > 0){
                currentUser.texts = sms = switchView.isOn;
                NSLog(@"sms is toggled %d",sms);
                [currentUser saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error){
                    if (error) [SVProgressHUD showInfoWithStatus:[NSString stringWithFormat:@"Could not save: %@", error.localizedDescription]];
                    /*else if (succeeded) [PFCloud callFunctionInBackground:@"sendConfirmationText" withParameters:nil block:^(NSString *success, NSError *error){}]; //send api call for confirmation text (shortcode requirement)*/
                }];
            }else{
                [SVProgressHUD showInfoWithStatus:@"Please enter a phone number to allow text messages."];
                [switchView setOn:NO animated:NO];
            }
            
            break;
        case 4: // emails
            if (currentUser.email && currentUser.email.length > 0){
                currentUser.alerts = email = switchView.isOn;
                [currentUser saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error){
                    if (error) [SVProgressHUD showInfoWithStatus:[NSString stringWithFormat:@"Could not save: %@", error.localizedDescription]];
                }];
            }else{
                [SVProgressHUD showInfoWithStatus:@"Please enter an email address to allow emails."];
                [switchView setOn:NO animated:NO];
            }
            break;
        default:
            break;
    }
}


#pragma mark - UITableView Delegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = (UITableViewCell *)[self.tableView cellForRowAtIndexPath:indexPath];

    if (cell.tag){
        UITextField *textField = (UITextField*)[cell.contentView viewWithTag:cell.tag];

        if (![textField isFirstResponder]) [textField becomeFirstResponder];
    }else
        return;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return [profileArray count];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    NSString *title;
    if (profileArray[section] == userSettingsArray)
        title = @"Account";
    else if (profileArray[section] == userCommunicationArray)
        title = @"Communication";
    else if (profileArray[section] == communitySettingsArray)
        title = @"Community";
    else if (profileArray[section] == associatedUserArray){
        if ([currentUser.userRole isEqualToString:kPermissionResident]){
            if (CMQAD.apartment.interestedPartiesRefString && CMQAD.apartment.interestedPartiesRefString.length > 0)
                title = [NSString stringWithFormat:@"Your %@", CMQAD.apartment.interestedPartiesRefString];
            else
                title = @"Your Interested Parties";
        }else if ([currentUser.userRole isEqualToString:kPermissionInterestedParty])
            title = @"Your Residents";
    }else
        title = @"";
    
    return title;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [profileArray[section] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell;
    
    if (profileArray[indexPath.section] == userSettingsArray){ //display string values
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CMQProfileCellIdentifier];
        cell.textLabel.text = profileArray[indexPath.section][indexPath.row][0];
        
        if (profileArray[indexPath.section][indexPath.row][2] == [NSNumber numberWithBool:YES]){
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.tag = 9999 - indexPath.row;
            
            UITextField *textField = [[UITextField alloc] init];
            
            switch (cell.tag){
                case 9999:
                    textField.keyboardType = UIKeyboardTypeDefault;
                    textField.autocapitalizationType = UITextAutocapitalizationTypeWords;
                    textField.returnKeyType = UIReturnKeyDone;
                    cell.detailTextLabel.text = textField.text = firstName;
                    textField.placeholder = @"Add First Name";
                    break;
                case 9998:
                    textField.keyboardType = UIKeyboardTypeDefault;
                    textField.autocapitalizationType = UITextAutocapitalizationTypeWords;
                    textField.returnKeyType = UIReturnKeyDone;
                    cell.detailTextLabel.text = textField.text = lastName;
                    textField.placeholder = @"Add Last Name";
                    break;
                case 9997:
                    textField.keyboardType = UIKeyboardTypeEmailAddress;
                    textField.autocapitalizationType = UITextAutocapitalizationTypeNone;
                    textField.returnKeyType = UIReturnKeyDone;
                    cell.detailTextLabel.text = textField.text = username;
                    textField.placeholder = @"Add Email";
                    break;
                case 9996:
                    textField.keyboardType = UIKeyboardTypePhonePad;
                    textField.returnKeyType = UIReturnKeyDone;
                    cell.detailTextLabel.text = textField.text = phone;
                    textField.placeholder = @"Add Number";
                    break;
                case 9995:
                    textField.keyboardType = UIKeyboardTypeDefault;
                    textField.autocapitalizationType = UITextAutocapitalizationTypeNone;
                    textField.returnKeyType = UIReturnKeyDone;
                    cell.detailTextLabel.text = textField.text = textField.placeholder = @"Change Password";
                    break;
                default:
                    textField.keyboardType = UIKeyboardTypeDefault;
                    textField.autocapitalizationType = UITextAutocapitalizationTypeWords;
                    textField.returnKeyType = UIReturnKeyDone;
                    break;
            }
            
            // setup editable UITextField in place of detailTextLabel
            /********************************************************
             * https://github.com/fulldecent/FDTextFieldTableViewCell
             *********************************************************/
            cell.detailTextLabel.hidden = YES;
            [[cell viewWithTag:3] removeFromSuperview];
            textField.frame = cell.detailTextLabel.frame;
            textField.tag = cell.tag;
            textField.translatesAutoresizingMaskIntoConstraints = NO;
            textField.textAlignment = NSTextAlignmentRight;
            [cell.contentView addSubview:textField];
            [cell addConstraint:[NSLayoutConstraint constraintWithItem:textField attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:cell.textLabel attribute:NSLayoutAttributeTrailing multiplier:1 constant:8]];
            [cell addConstraint:[NSLayoutConstraint constraintWithItem:textField attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:cell.contentView attribute:NSLayoutAttributeTop multiplier:1 constant:8]];
            [cell addConstraint:[NSLayoutConstraint constraintWithItem:textField attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:cell.contentView attribute:NSLayoutAttributeBottom multiplier:1 constant:-8]];
            [cell addConstraint:[NSLayoutConstraint constraintWithItem:textField attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:cell.detailTextLabel attribute:NSLayoutAttributeTrailing multiplier:1 constant:0]];
            /*********************************************************/
            
            textField.spellCheckingType = UITextSpellCheckingTypeNo;
            textField.delegate = self;
            [textField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
        }
    }else if (profileArray[indexPath.section] == userCommunicationArray){ //add UISwitch for all but first cell in this section
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CMQProfileCellIdentifier];
        cell.textLabel.text = profileArray[indexPath.section][indexPath.row][0];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        if (indexPath.row != 0){
            UISwitch *switchView = [[UISwitch alloc] initWithFrame:CGRectZero];
            [switchView addTarget:self action:@selector(switchValueChanged:) forControlEvents:UIControlEventValueChanged];
            switchView.tag = indexPath.row;
            cell.accessoryView = switchView;
            
            switch (indexPath.row){
                case 1:
                    [switchView setOn:pushAll animated:YES];
                    break;
                case 2:
                    [switchView setOn:pushUrgent animated:YES];
                    break;
                case 3:
                    [switchView setOn:sms animated:YES];
                    break;
                case 4:
                    [switchView setOn:email animated:YES];
                    break;
                default:
                    break;
            }
        }
    }else{
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CMQProfileCellIdentifier];
        cell.textLabel.text = profileArray[indexPath.section][indexPath.row][0];
        cell.detailTextLabel.text = profileArray[indexPath.section][indexPath.row][1];
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return (section == 0) ? 120 : 40;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return (section == profileArray.count - 1) ? 90 : 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (section == 0){
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, 120)];
        
        userPhoto = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
        userPhoto.layer.cornerRadius = 50.0f;
        userPhoto.layer.borderWidth = .5f;
        userPhoto.layer.borderColor = [UIColor blackColor].CGColor;
        userPhoto.layer.masksToBounds = YES;
        [userPhoto setCenter:CGPointMake(view.center.x, view.center.y - 10)];
        userPhoto.imageView.contentMode = UIViewContentModeScaleAspectFill;
        [userPhoto setTitle:@"Upload Photo" forState:UIControlStateNormal];
        [userPhoto setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [userPhoto.titleLabel setFont:[UIFont fontWithName:kCMQFontGeometriaLight size:10]];
        
        if ([CMQUser currentUser].userPhoto){
            //fetch our image data on a background thread
            dispatch_queue_t queue = dispatch_get_global_queue (DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
            dispatch_async(queue, ^{
                UIImage *image = [UIImage getImageFromPFFile:[CMQUser currentUser].userPhoto];
                //display our image on the main thread
                dispatch_async(dispatch_get_main_queue(), ^{
                    [userPhoto setImage:image forState:UIControlStateNormal];
                    [userPhoto setImage:image forState:UIControlStateHighlighted];
                });
            });
        }
        
        [userPhoto addTarget:self action:@selector(onSelectUserPhoto:) forControlEvents:UIControlEventTouchUpInside];
        
        [view addSubview:userPhoto];
        return view;
    }
    return nil;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    if (section == profileArray.count - 1){
        UILabel *footerLabel = [[UILabel alloc] init];
        [footerLabel setFont:[UIFont fontWithName:@"Helvetica" size:11]];
        footerLabel.text = [NSString stringWithFormat:@"You are using %@ %@\nA Communiqué Product",[[[NSBundle mainBundle] localizedInfoDictionary] objectForKey:@"CFBundleDisplayName"], [UIApplication versionAndBuild]];
        footerLabel.textColor = [UIColor grayColor];
        footerLabel.textAlignment = NSTextAlignmentCenter;
        footerLabel.numberOfLines = 2;
        return footerLabel;
    }
    return nil;
}
    
#pragma mark - UITextFieldDelegate

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    return YES;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    currentTag = textField.tag;
    
    if (currentTag == passwordTag){
        textField.secureTextEntry = YES;
        textField.text = @"";
    }else if (currentTag == phoneTag && [textField.text isEqualToString:@"Add Number"])
        textField.text = @"";
}

-(BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    return YES;
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    BOOL shouldSave = NO;
    
    switch (currentTag) {
        case 9999:
            if (![textField.text isEqualToString:@""]) currentUser.firstName = firstName = textField.text;
            else textField.text = currentUser.firstName;
            
            shouldSave = ![textField.text isEqualToString:@""];
            break;
        case 9998:
            if (![textField.text isEqualToString:@""]) currentUser.lastName = lastName = textField.text;
            else textField.text = currentUser.lastName;
                
            shouldSave = ![textField.text isEqualToString:@""];
            break;
        case 9997:
            if ([self isValidEmail:textField.text]){
                currentUser.email = currentUser.username = username = textField.text;
                [textField setTextColor:[UIColor blackColor]];
            }else{
                [textField setTextColor:[UIColor redColor]];
                [SVProgressHUD showInfoWithStatus:@"Please enter a valid email address."];
            }
            
            shouldSave = [self isValidEmail:textField.text];
            break;
        case 9996:
            if ([self isValidPhone:textField.text]){
                currentUser.phone = phone = textField.text = [self stripNonNumericCharacters:textField.text];
                [textField setTextColor:[UIColor blackColor]];
            }else{
                if([self isEmptyPhone:textField.text]) currentUser.phone = @"";
                
                currentUser.texts = sms = NO;
                [(UISwitch *)[self.view viewWithTag:3] setOn:NO animated:YES];
                phone = textField.text = @"Add Number";
                [textField setTextColor:[UIColor blackColor]];
            }
            
            shouldSave = ([self isValidPhone:textField.text] || [self isEmptyPhone:textField.text]);
            break;
        case 9995:
            if (![textField.text isEqualToString:@""]) currentUser.password = textField.text;
            
            textField.secureTextEntry = NO;
            textField.text = @"Change Password";
            shouldSave = ![textField.text isEqualToString:@""];
            break;
        default:
            break;
    }
    
    currentTag = 0;
    
    if (shouldSave){
        [currentUser saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error){
            if (error) [SVProgressHUD showInfoWithStatus:[NSString stringWithFormat:@"Could not save: %@", error.localizedDescription]];
        }];
    }
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if (range.location == textField.text.length && [string isEqualToString:@" "]) {
        textField.text = [textField.text stringByAppendingString:@"\u00a0"];
        return NO;
    }
    return YES;
}

/**
 *  handles UITextfield input change
 *
 *  @param textField the UITextField
 */
- (void)textFieldDidChange:(UITextField *)textField{
    if (currentTag == phoneTag){
        if ([self isValidPhone:textField.text]){
            [textField setTextColor:[UIColor greenColor]];
            [textField resignFirstResponder];
        }else
            [textField setTextColor:[UIColor redColor]];
    }
}

#pragma mark - UIImagePickerController Delegate

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    [userPhoto setImage:nil forState:UIControlStateNormal];   //just clear the image, don't hide the button. that way the user can still select a new image
    UIImage *selectedImage = [UIImage resizeImage:info[UIImagePickerControllerEditedImage] newWidth:200.0 newHeight:200.0];    //get selected image & resize
    [userPhoto setImage:selectedImage forState:UIControlStateNormal];
    [userPhoto setImage:selectedImage forState:UIControlStateHighlighted];
    
    NSString *imageName = [NSString stringWithFormat:@"%f", [[NSDate date] timeIntervalSince1970]];
    imageName = [[imageName MD5] stringByAppendingPathExtension:@"png"];
    
    NSData *imageData = UIImagePNGRepresentation(selectedImage);
    PFFile *imageFile = [PFFile fileWithName:imageName data:imageData];
    currentUser.userPhoto = imageFile;
    [currentUser saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error){
        if (error) [SVProgressHUD showInfoWithStatus:[NSString stringWithFormat:@"Could not save: %@", error.localizedDescription]];
    }];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
