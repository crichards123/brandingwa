//
//  CMQDataSourceManager.swift
//  Communique
//
//  Created by Andre White on 4/20/18.
//  Copyright © 2018 Communique, LLC. All rights reserved.
//

import UIKit

public class CMQDataSourceManager: NSObject {
    public typealias CMQDataSourceResultBlock = (_ objects:[PFObject]?,_ error:Error?)->Void
    private var delegate:CMQDataSourceDelegate
    private var query:PFQuery<PFObject>
    public var myObjects:[PFObject] = [PFObject]()
    private var loading:Bool = false
    public var numberOfRows:Int{
        return self.myObjects.count
    }
    public var numberOfSections:Int = 1
    public init(query:PFQuery<PFObject>, delegate:CMQDataSourceDelegate) {
        self.delegate = delegate
        self.query = query
        super.init()
    }
    open func queryForObjects()->[PFQuery<PFObject>]{
        return [PFQuery<PFObject>]()
    }
    public func object(at:IndexPath)->PFObject?{
        guard myObjects.count > at.row else {return nil}
        return myObjects[at.row]
    }
    private func completionForObjects()->CMQDataSourceResultBlock{
        return { (objects:[PFObject]?, error:Error?)->Void in
            self.loading = false
            guard error == nil else{
                self.delegate.objectsLoaded(error)
                return
            }
            if let objects = objects{
                self.myObjects = objects
                self.delegate.objectsLoaded(nil)
            }
        }
    }
    public func loadObjects(){
        self.query.findObjectsInBackground(block: completionForObjects())
    }
    
}
