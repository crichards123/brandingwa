//
//  CMQExtensions.swift
//  Communique
//
//  Created by Andre White on 1/16/18.
//  Copyright © 2018 Communique, LLC. All rights reserved.
//

import Foundation
import SVProgressHUD


public var CMQBundle: Bundle?{
    guard let aClass = NSClassFromString("CMQPackage") else {return nil}
    return Bundle.init(for: aClass)
}
public var GeneratingKeyAnimationPath:String?{
    guard let bundle = CMQBundle else {return nil}
    return bundle.path(forResource: "gen_key_ani", ofType: "json")
}
public func loadNib(name:String)->UINib?{
    guard let bundle = CMQBundle else {return nil}
    return UINib.init(nibName: name, bundle: bundle)
}
public func generateKeySize(forView:UIView)->CGRect{
    let height = forView.frame.size.height*0.55
    let width = UIDevice.current.userInterfaceIdiom == .pad ? forView.frame.width * 0.40 : forView.frame.width*0.75
    let startY = forView.center.y - (height/2)
    let startX = forView.center.x - (width/2)
    let size = CGSize.init(width: width, height: height)
    let point = CGPoint.init(x: startX, y: startY)
    return CGRect.init(origin: point, size: size)
}
public func promptSize(forView:UIView)->CGRect{
    let height = CGFloat(211.0)//forView.frame.size.height*0.35
    let width = UIDevice.current.userInterfaceIdiom == .pad ? forView.frame.width * 0.40 : CGFloat(281)/*forView.frame.width*0.75*/
    let startY = (forView.frame.size.height*0.25)//forView.center.y - (height/2)
    let startX = forView.center.x - (width/2)
    let size = CGSize.init(width: width, height: height)
    let point = CGPoint.init(x: startX, y: startY)
    return CGRect.init(origin: point, size: size)
}
public var errorLoadingImage:UIImage?{
    return nil
}
public func handle(error:Error, message:String?, shouldDisplay:Bool){
    if shouldDisplay{
        error.display(message: message)
    }
    if let message = message{
        print("Error With: \(message)\n \(error)")
    }else{
        print("Error With: \(error)")
    }
    //error.log(message: message)
}
public func showSuccess(message:String?){
    if let message = message {
        CMQAPIManager.showFeedback(message: message)
        //SVProgressHUD.showSuccess(withStatus: message)
    }
    CMQAPIManager.showFeedback(message: "")
    //SVProgressHUD.showSuccess(withStatus: "")
}
/*extension CMQUser{
    public var isMultiUser:Bool{
        guard let comms = self.communities, CMQUser.current().isAdmin else {return false}
        return comms.count>1
    }
}*/
extension UIColor{
    public static let DarkColor = UIColor.init(red: 17.0/225.0, green: 19.0/225.0, blue: 53.0/225.0, alpha: 1.0)
    public static let EventColor = UIColor.init(red: 32.0/225.0, green: 178.0/225.0, blue: 170.0/225.0, alpha: 1.0)
    public static let MessageColor = UIColor.init(red: 224.0/225.0, green: 102.0/225.0, blue: 102.0/225.0, alpha: 1.0)
    public static let NewsColor = UIColor.init(red: 255.0/225.0, green: 187.0/225.0, blue: 57.0/225.0, alpha: 1.0)
}
extension Notification.Name{
    public static let didTapAttendees = Notification.Name("attendeesTapped")
    public static let didRespondToPackagePrompt = Notification.Name("packageResponse")
    public static let packageNotification = Notification.Name("packageNotification")
    public static let updateComposable = Notification.Name("updateComposable")
    public static let prevPressed = Notification.Name("prevPressed")
    public static let nextPressed = Notification.Name("nextPressed")
    public static let didSetTimeNotification = Notification.Name("didSetTime")
}
extension Date{
    static public let MidnightToday:Date? = Date.init(timeIntervalSinceNow: 0).atMidnight
    static public func fromParse(string:String)->Date?{
        let format = DateFormatter.init()
        format.dateFormat = "yyyy-MM-dd'T'HH:mm"
        format.timeZone = TimeZone.init(abbreviation: "UTC")!
        return format.date(from:string)
    }
    public var atMidnight:Date?{
        var components = Calendar.current.dateComponents(in: TimeZone.current, from: self)
        components.hour = 0
        components.minute = 0
        components.second = 0
        return Calendar.current.date(from: components)
    }
    public func adding(days:Int)->Date{
        let offset = TimeInterval(60*60*24*days)
        return self.addingTimeInterval(offset)
    }
    public var toParse:String{
        let format = DateFormatter.init()
        format.dateFormat = "yyyy-MM-dd'T'HH:mm"
        format.timeZone = TimeZone.init(abbreviation: "UTC")!
        return format.string(from: self)
    }
    public var scheduleMessageDateView:String{
        let format = DateFormatter.init()
        format.dateFormat = "EEEE, MMM d, yyyy h:mm a"
        format.timeZone = TimeZone.current
        return format.string(from: self)
    }
    public var feedDateView:String{
        let format = DateFormatter.init()
        format.dateFormat = "E MMM d, yyyy h:mm a"
        format.timeZone = TimeZone.current
        return format.string(from: self)
    }
    public var eventDateView:String{
        let format = DateFormatter.init()
        format.dateFormat = "E MMM d, yyyy"
        format.timeZone = TimeZone.current
        return format.string(from: self)
    }
    public var dateOnlyString:String{
        let format = DateFormatter.init()
        format.dateFormat = "MM-dd-yyyy"
        format.timeZone = .current
        return format.string(from: self)
    }
    public var timeOnlyString:String{
        let format = DateFormatter.init()
        format.dateFormat = "h:mm a"
        format.timeZone = .current
        return format.string(from: self)
    }
}
extension UIImage{
    static func imageWithRoundedCorners(image:UIImage, radius:CGFloat)->UIImage?{
        let layer = CALayer.init()
        layer.frame = CGRect.init(origin: .zero, size: CGSize.init(width: image.size.width, height: image.size.height))
        layer.contents = image.cgImage
        layer.masksToBounds = true
        layer.cornerRadius = radius
        UIGraphicsBeginImageContext(image.size)
        let rounded = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return rounded
    }
}
extension UIView {
    
    func slideInFromLeft(duration: TimeInterval = 0.6, completionDelegate: CAAnimationDelegate? = nil)/*->CATransition*/ {
        // Create a CATransition animation
        let slideInFromLeftTransition = CATransition()
        
        // Set its callback delegate to the completionDelegate that was provided (if any)
        if let delegate: CAAnimationDelegate = completionDelegate {
            slideInFromLeftTransition.delegate = delegate
        }
        
        // Customize the animation's properties
        slideInFromLeftTransition.type = CATransitionType.push
        slideInFromLeftTransition.subtype = CATransitionSubtype.fromLeft
        slideInFromLeftTransition.duration = duration
        slideInFromLeftTransition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        slideInFromLeftTransition.fillMode = CAMediaTimingFillMode.removed
        //return slideInFromLeftTransition
        // Add the animation to the View's layer
        self.layer.add(slideInFromLeftTransition, forKey: "slideInFromLeftTransition")
    }
}
extension Array where Element: Equatable{
    func next(cur:Element)->Element?{
        guard self.contains(cur) else {return nil}
        guard let index = self.index(of:cur) else {return nil}
        if index == self.endIndex - 1{
            return self[0]
        }else{
            return self[self.index(after: index)]
        }
    }
}
extension String{
    public func isEmpty()->Bool{
        if self.trimmingCharacters(in: .whitespacesAndNewlines) == ""{
            return true
        }
        return false;
    }
    public var isValidEmail:Bool{
        let regex = "[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$"
        let predicate = NSPredicate.init(format: "SELF MATCHES %@", regex)
        return predicate.evaluate(with:self)
    }
    var capitalizingFirstLetter: String {
        return prefix(1).uppercased() + dropFirst()
    }
    public var isValidPhone:Bool{
        let nums = CharacterSet.init(charactersIn: "0123456789")
        let set = CharacterSet.alphanumerics.subtracting(nums)
        if self.trimmingCharacters(in: set).count == 10{
            return true
        }
        return false
        //return count == 10 || count == 0
    }
}

extension Error{
    //Display an error to the screen
    public func display(message:String?){
        if let message = message{
            CMQAPIManager.showError(message: message)
            //SVProgressHUD.showError(withStatus: message)
        }else{
            CMQAPIManager.showError(message: self.localizedDescription)
            //SVProgressHUD.showError(withStatus: self.localizedDescription)
        }
    }
    public func log(message:String?){
        if let message = message {
            let string = "Error with message: description"
            
            print(string.replacingOccurrences(of: "message", with: message).replacingOccurrences(of: "description", with: self.localizedDescription))
        }else{
            print(self.localizedDescription)
        }
    }
    public var isCacheMiss:Bool?{
        //guard let error = self as? NSError else {return nil}
        if self._code == PFErrorCode.errorCacheMiss.rawValue{
            return true
        }else{
            return false
        }
    }
    public var isUnFound:Bool?{
        //guard let error = self as? NSError else {return nil}
        if self._code == PFErrorCode.errorObjectNotFound.rawValue{
            return true
        }else{
            return false
        }
    }

}
public extension PFObject{
    var sortDate:Date!{
        switch parseClassName {
        case "Message":
            guard let message = self as? CMQMessage else {return createdAt!}
            if message.scheduleMessage{
                let formatter = DateFormatter.init()
                formatter.dateFormat = "yyyy-MM-dd'T'HH:mm"
                formatter.timeZone = TimeZone.init(abbreviation: "UTC")
                guard let date = formatter.date(from: message.scheduleTime) else {return createdAt!}
                return date
            }else{
                return createdAt!
            }
        default:
            return createdAt!
        }
    }
}
extension PFObject:Viewing{
    public func getViewableProperty(propertyType: ItemProperty.PropertyType) -> ItemProperty? {
        return self.viewableProperties?.filter({$0.propertyType == propertyType}).first
    }
    public var viewableProperties: [ItemProperty]? {
        return ItemProperty.viewableProperties(itemType: self.itemType)
    }
    public var viewableType: PFObject.ItemType {
        return self.itemType
    }
    public var displayedProperties: [ItemProperty] {
        var viewable = [ItemProperty]()
        viewable.append(getViewableProperty(propertyType: .Title)!)
        viewable.append(getViewableProperty(propertyType: .Content)!)
        viewable.append(getViewableProperty(propertyType: .Sender)!)
        viewable.append(getViewableProperty(propertyType: .TimeSent)!)
        
        switch self.itemType {
        case .News:
            if isPropertySet(propertyType: .Url){
                viewable.append(getViewableProperty(propertyType: .Url)!)
            }
        case .Event:
            viewable.append(getViewableProperty(propertyType: .EventDetails)!)
            viewable.append(getViewableProperty(propertyType: .Attendees)!)
        case .Message:
            if isPropertySet(propertyType: .Package){
                viewable.append(getViewableProperty(propertyType: .Package)!)
            }
        }
        if isPropertySet(propertyType: .Image){
            viewable.append(getViewableProperty(propertyType: .Image)!)
        }
      return viewable
    }
}
extension PFObject:Composable{
    
    public enum ItemType:String{
        case Message = "Message"
        case Event = "Event"
        case News = "News"
        //case Announcement = "Announcement"
    }
    public var itemType:ItemType{
        return ItemType.init(rawValue: self.parseClassName)!
    }
    public var reqProperties: [ItemProperty]? {
        return self.composableProperties?.filter({$0.isRequired})
    }
    public var composableType:ItemType{
        return self.itemType
    }
    public var visibleProperties: [ItemProperty] {
        var visible = [ItemProperty]()
        visible.append(getProperty(propertyType: .Title)!)
        if itemType == .News{
            visible.append(getProperty(propertyType: .Url)!)
        }
        if itemType == .Event{
            visible.append(getProperty(propertyType: .Location)!)
        }
        visible.append(getProperty(propertyType: .Content)!)
        let imageProp = getProperty(propertyType: .Image)!
        if self.isPropertySet(propertyType: .Image){
            visible.append(imageProp)
        }
        if let dateProp = getProperty(propertyType: .Date){
            if self.isPropertySet(propertyType: .Date){
                visible.append(dateProp)
            }
        }
        return visible
    }
    
    public var isComposed: Bool {
        return self.unSetReqProperties == nil
    }
    public var unSetReqProperties:[ItemProperty.PropertyType]?{
        guard let reqArray = self.reqProperties else {return nil}
        var unSet:[ItemProperty.PropertyType]?
        for property in reqArray{
            if property.propertyType != .Recipients{
                if !isPropertySet(propertyType: property.propertyType){
                    if unSet == nil{
                        unSet = [ItemProperty.PropertyType]()
                    }
                    unSet?.append(property.propertyType)
                }
            }
        }
        return unSet
    }
    public func deletePropertyContent(propertyType: ItemProperty.PropertyType) {
        guard let _ = getProperty(propertyType: propertyType) else {return}
        if propertyType == .Image{
            switch self.itemType {
            case .Message:
                guard let message = self as? CMQMessage else {return}
                message.messagePhoto = nil
            case .Event:
                guard let event = self as? CMQEvent else {return}
                event.eventPhoto = nil
            case .News:
                guard let news = self as? CMQNewsArticle else {return}
                news.newsPhoto = nil
            }
        }
        if propertyType == .Date{
            switch self.itemType {
            case .Message:
                guard let message = self as? CMQMessage else {return}
                message.scheduleTime = nil
                message.scheduleMessage = false
                message.scheduleSent = false
            case .Event:
                guard let event = self as? CMQEvent else {return}
                event.eventDate = nil
                event.utcOffset = 0
            default:
                return
            }
        }
    }
    public func setProperty(propertyType: ItemProperty.PropertyType, propertyContent: Any?) {
        guard let _ = getProperty(propertyType: propertyType) else {return}
        switch propertyType {
        case .Title:
            guard let title = propertyContent as? String else {return}
            switch self.itemType {
            case .Message:
                guard let message = self as? CMQMessage  else {return}
                message.messageTitle = title
            case .Event:
                guard let event = self as? CMQEvent else {return}
                event.eventTitle = title
            case .News:
                guard let news = self as? CMQNewsArticle else {return }
                news.newsTitle = title
            }
        case .Content:
            guard let content = propertyContent as? String else {return}
            switch self.itemType {
            case .Message:
                guard let message = self as? CMQMessage else {return}
                message.messageContent = content
            case .Event:
                guard let event = self as? CMQEvent else {return }
                event.eventContent = content
            case .News:
                guard let news = self as? CMQNewsArticle else {return}
                news.newsContent = content
            }
        case .Url:
            guard let news = self as? CMQNewsArticle, let url = propertyContent as? String else {return}
            news.articleURL = url
        case .Date:
            guard let date = propertyContent as? Date else {return}
            switch self.itemType {
            case .Message:
                guard let message = self as? CMQMessage else {return}
                message.scheduleTime = date.toParse
                message.scheduleMessage = true
                message.scheduleSent = false
            case .Event:
                guard let event = self as? CMQEvent else {return}
                event.eventDate = date
                event.utcOffset = TimeZone.current.secondsFromGMT(for: date)/60
            default:
                return
            }
        case .Location:
            guard let event = self as? CMQEvent, let location = propertyContent as? String else {return}
            event.eventLocation = location
        case .Image:
            guard let photo = propertyContent as? UIImage else {return}
            switch self.itemType {
            case .Message:
                guard let message = self as? CMQMessage else {return}
                message.messagePhoto = photo.toParse()
            case .Event:
                guard let event = self as? CMQEvent else {return}
                event.eventPhoto = photo.toParse()
            case .News:
                guard let news = self as? CMQNewsArticle else {return}
                news.newsPhoto = photo.toParse()
            }
        case .Urgent:
            guard let message = self as? CMQMessage, let isUrgent = propertyContent as? Bool else {return}
            message.isCritical = isUrgent
        case .Recipients:
            guard let message = self as? CMQMessage, let recipients = propertyContent as? [String] else {return}
            message.addUniqueObjects(from: recipients, forKey: "recipients")
            message.isAnnouncement = true
            message.aptComplexID = nil
            
        default:
            return
        }
    }
    
    public func propertyContent(propertyType: ItemProperty.PropertyType) -> Any? {
        switch propertyType {
        case .Title:
            switch self.itemType {
            case .Message:
                guard let message = self as? CMQMessage else {return nil}
                return message.messageTitle
            case .Event:
                guard let event = self as? CMQEvent else {return nil}
                return event.eventTitle
            case .News:
                guard let news = self as? CMQNewsArticle else {return nil}
                return news.newsTitle
            }
        case .Content:
            switch self.itemType {
            case .Message:
                guard let message = self as? CMQMessage else {return nil}
                return message.messageContent
            case .Event:
                guard let event = self as? CMQEvent else {return nil}
                return event.eventContent
            case .News:
                guard let news = self as? CMQNewsArticle else {return nil}
                return news.newsContent
            }
        case .Url:
            guard let news = self as? CMQNewsArticle, let url = news.articleURL else {return nil}
            return url
        case .Date:
            switch self.itemType{
            case .Event:
                guard let event = self as? CMQEvent, let eventDate = event.eventDate else {return nil}
                return eventDate
            case .Message:
                guard let message = self as? CMQMessage, let dateString = message.scheduleTime, let date = Date.fromParse(string: dateString) else {return nil}
                return date
            default:
                return nil
            }
           
        case .Location:
            guard let event = self as? CMQEvent else {return nil}
            return event.eventLocation
        case .Urgent:
            guard let message = self as? CMQMessage else {return nil}
            return message.isCritical
        case .Image:
            switch self.itemType {
            case .Message:
                guard let message = self as? CMQMessage else {return nil}
                return message.messagePhoto
            case .Event:
                guard let event = self as? CMQEvent else {return nil}
                return event.eventPhoto
            case .News:
                guard let news = self as? CMQNewsArticle else {return nil}
                return news.newsPhoto
            }
        case .TimeSent:
            return self.sortDate
        case .Sender:
            return self.object(forKey: "userWhoPosted") as? CMQUser
        case .Color:
            switch self.itemType{
            case .Message:
                return UIColor.MessageColor
            case .Event:
                return UIColor.EventColor
            case .News:
                return UIColor.NewsColor
            }
        case .Attendees:
            guard let event = self as? CMQEvent else {return nil}
            guard let attendees = event.eventAttendees as? [String] else {return "Be the first to RSVP"}
            let amIAttending = { ()-> Bool in
                if let _ = attendees.filter({ (attendeeID) -> Bool in
                    attendeeID == CMQUser.current().objectId
                }).first{
                    return true
                }else{
                    return false
                }
            }
            let attending: Bool = amIAttending()
            switch attendees.count{
            case 0:
                return "Be the first to RSVP"
            case 1:
                if attending{
                    return "You are attending"
                }else{
                    return "1 attending"
                }
            case let x where x > 1 :
                if attending{
                    return "You and \(attendees.count-1) other members are attending"
                }else{
                    return "\(attendees.count) members are attending"
                }
            default:
                print("Unknown number of attendees")
                return nil
            }
        case .EventDetails:
            guard let event = self as? CMQEvent else {return nil}
            var dict = [String:Any?]()
            dict["Date"] = event.eventDate
            dict["Location"] = event.eventLocation
            return dict
        default:
            return nil
        }
    }
    public func getProperty(propertyType: ItemProperty.PropertyType) -> ItemProperty? {
        
        return self.composableProperties?.filter({$0.propertyType == propertyType}).first
    }
    public func post(completion:@escaping PFBooleanResultBlock){
        self.setObject(CMQUser.current(), forKey: "userWhoPosted")
        if !self.isPropertySet(propertyType: .Recipients){
            self.setObject(CMQUser.current().complexID, forKey: "aptComplexID")
        }
        self.saveInBackground(block: completion)
    }
    public func isPropertySet(propertyType: ItemProperty.PropertyType) -> Bool{
        guard let content = propertyContent(propertyType: propertyType) else {return false}
        if let text = content as? String{
            return !text.isEmpty()
        }
        return true
    }
    public var composableProperties: [ItemProperty]?{
        var imgProp = ItemProperty.init(propertyType: .Image)
        var properties = [ItemProperty.init(propertyType: .Title),ItemProperty.init(propertyType: .Content), imgProp]
        switch self.parseClassName {
        case "Message":
            guard let _ = self as? CMQMessage else {return nil}
            imgProp.isRequired = false
            var dateProp = ItemProperty.init(propertyType: .Date)
            dateProp.isRequired = false
            properties.append(dateProp)
            var urgProp = ItemProperty.init(propertyType: .Urgent)
            urgProp.isRequired = true
            properties.append(urgProp)
            if CMQAPIManager.sharedInstance().apartment == nil {
                var recsProp = ItemProperty.init(propertyType: .Recipients)
                recsProp.isRequired = true
                properties.append(recsProp)
            }
        case "Event":
            guard let _ = self as? CMQEvent else {return nil}
            imgProp.isRequired = false
            var dateProp = ItemProperty.init(propertyType: .Date)
            dateProp.isRequired = true
            properties.append(dateProp)
            var locationProp = ItemProperty.init(propertyType: .Location)
            locationProp.isRequired = true
            properties.append(locationProp)
        case "News":
            guard let _ = self as? CMQNewsArticle else {return nil}
            imgProp.isRequired = false
            var urlProp = ItemProperty.init(propertyType: .Url)
            urlProp.isRequired = false
            properties.append(urlProp)
        default:
            return nil
        }
        return properties
    }
    
}
extension PFQuery where PFGenericObject == PFObject{
    enum QueryType{
        case Events
        case EventsMulti
        case Message
        case MessageMulti
        case News
        case NewsMulti
    }
    static var EventsQuery:Any?{
        guard let query = CMQEvent.query() else {return nil}
        guard let newDate = Date.MidnightToday else {return nil}
        var apartmentIDs:[String] = [String]()
        if let apartment = CMQAPIManager.sharedInstance().apartment{
            apartmentIDs.append(apartment.objectId!)
        }else if let apartments = CMQAPIManager.sharedInstance().apartments{
            apartmentIDs = apartments.compactMap({$0.objectId})
        }
        query.order(byAscending: kParseEventDateKey)
        query.includeKey("userWhoPosted")
        query.whereKey(kParseEventDateKey, greaterThanOrEqualTo: newDate)
        query.whereKey(kParseAptComplexIDKey, containedIn: apartmentIDs)
        return query
    }
    static var NewsQuery:PFQuery<PFObject>?{
        guard let query = CMQNewsArticle.query() else {return nil}
        var apartmentIDs:[String] = [String]()
        if let apartment = CMQAPIManager.sharedInstance().apartment{
            apartmentIDs.append(apartment.objectId!)
        }else if let apartments = CMQAPIManager.sharedInstance().apartments{
            apartmentIDs = apartments.compactMap({$0.objectId})
        }
        query.order(byDescending: kParseObjectCreateDateKey)
        query.includeKey("userWhoPosted")
        query.whereKey(kParseAptComplexIDKey, containedIn: apartmentIDs)
        return query
    }
    @available(*, unavailable, renamed: "EventsQuery")
    class public func eventsQuery()->Any?{
       return nil
    }
    @available(*, unavailable, renamed: "NewsQuery")
    class public func newsQuery()->PFQuery<PFObject>?{
        return nil
    }
    
    class public func eventsQuery(apartmentIDs:[String])->PFQuery<PFObject>?{
        guard let query = CMQEvent.query() else {return nil}
        guard let newDate = Date.MidnightToday else {return nil}
        query.whereKey(kParseEventDateKey, greaterThanOrEqualTo: newDate)
        query.whereKey(kParseAptComplexIDKey, containedIn: apartmentIDs)
        return query
    }
    class public func newsQuery(apartmentIDs:[String])->PFQuery<PFObject>?{
        guard let query = CMQNewsArticle.query() else {return nil}
        query.whereKey(kParseAptComplexIDKey, containedIn: apartmentIDs)
        return query
    }
    class public func messageQuery(apartmentIDs:[String])->Any?{
        let query = CMQMessageDataSourceManager.sharedInstance().messageQuery(communityIDs: apartmentIDs)
        return query
    }
    class public func messageQuery()->Any?{
        var apartmentIDs:[String] = [String]()
        if let apartment = CMQAPIManager.sharedInstance().apartment{
            apartmentIDs.append(apartment.objectId!)
        }else if let apartments = CMQAPIManager.sharedInstance().apartments{
            apartmentIDs = apartments.compactMap({$0.objectId})
        }
        let query = CMQMessageDataSourceManager.sharedInstance().getMessageQuery()
        query.includeKey("userWhoPosted")
        query.includeKey("Package")
        query.whereKey("scheduleSent", notEqualTo: false)
        return query
    }
}
public extension Array where Element==CMQPackage{
    public enum Separator : String {
        case Resident = "Resident"
        case PackageType = "Type"
        case CheckInDate = "CheckIn"
        case CheckOutDate = "CheckOut"
        
    }
    public func separateBy(separator:Separator) -> Array<Array> {
        switch separator {
        case .Resident:
            return self.separateByResident()
        case .PackageType:
            return self.separateByType()
        case .CheckInDate:
            return self.separateByCheckInDate()
        case .CheckOutDate:
            return self.separateByCheckOutDate()
        }
    }
    private func separateByResident()->Array<Array>{
        let recipientIDs = Set(self.compactMap { (package) -> String in
            if let packageRecipient = package.recipient{
                return packageRecipient.objectId!
            }else{
                return ""
            }
        })
        return recipientIDs.map { (objectID) ->Array in
            self.filter({ (package) -> Bool in
                if let packageRecipient = package.recipient{
                    return packageRecipient.objectId==objectID
                }
                return false
            })
        }
    }
    
    private func separateByType()->Array<Array>{
        let types = Set(self.compactMap({ (package) -> String in
            return package.packageType
        }))
        return types.map({ (packageType) -> Array in
            self.filter({$0.packageType==packageType})
        })
    }
    
    private func separateByCheckInDate()->Array<Array>{
        let formatter = DateFormatter()
        formatter.dateFormat = "MMM d, yyyy - E"
        let dates = Set(self.compactMap({ (package) -> String in
            return formatter.string(from: package.checkedInDate)
        }))
        return dates.map({ (dateString) -> Array in
            self.filter({ (package) -> Bool in
                return dateString==formatter.string(from: package.checkedInDate)
            })
        }).sorted(by: { (array1, array2) -> Bool in
            if let date1 = array1.first?.checkedInDate , let date2 = array2.first?.checkedInDate{
                return date1>date2
            }
            return false
        })
    }
    private func separateByCheckOutDate()->Array<Array>{
        let formatter = DateFormatter()
        formatter.dateFormat = "MMM d, yyyy - E"
        //Create a container for the array of dates. Using a set to weed out duplicates.
        let checkedOutPackages = self.filter({ (package) -> Bool in
            //First filter out any packages that do not have a check out date.
            if (package.checkedOutDate) != nil{
                return true
            }else{
                return false
            }
        })
        let dates = Set(checkedOutPackages.compactMap({formatter.string(from: $0.checkedOutDate)}))
        //For each dateString in the container, create an array of packages checked out on that date, sorted by date
        return dates.map({ (dateString) -> Array in
            checkedOutPackages.filter({ (package) -> Bool in
                return dateString==formatter.string(from: package.checkedOutDate)
            })
        }).sorted(by: { (array1, array2) -> Bool in
            if let date1 = array1.first?.checkedOutDate , let date2 = array2.first?.checkedOutDate{
                return date1>date2
            }
            return false
        })
    }
}
