//
//  CMQLogSelectResidentTableViewController.swift
//  Communique
//
//  Created by Andre White on 6/4/18.
//  Copyright © 2018 Communique, LLC. All rights reserved.
//

import UIKit

public class CMQLogSelectResidentTableViewController: CMQCheckInSelectResidentTableViewController {

    override public func viewDidLoad() {
        super.viewDidLoad()

    }
    override public func prepareDataSource() {
        self.dataSource = CMQResidentWithPackageContainer()
        self.cellCreator = ResidentCellCreator.init(dataSource: self.dataSource, reuseIdentifier: "reuse")
        self.loadResultsController()
        self.dataSource.willChangeState(to: .loading)
    }
    override public func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override public func prepareResultsController(){
        if let controller = resultsController as? IDTableViewController{
            controller.dataSource = CMQResidentWithPackageContainer()
            controller.dataSource.willChangeState(to: .loading)
            controller.cellCreator = ResidentCellCreator.init(dataSource: controller.dataSource, reuseIdentifier: "ResidentCell")
        }
    }
    
    override public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
         tableView.allowsSelection = false
        let completion = { (packages:[CMQPackage]?, error:Error?) in
            tableView.allowsSelection = true
            if let error = error{
                //unable to load packages for resident
                handle(error: error, message: "Unable to load packages for resident. Please try again later", shouldDisplay: true)
            }else if let packages = packages{
                self.performSegue(withIdentifier: "Select", sender: packages)
            }
        }
        let dataSource = tableView == self.tableView ? self.dataSource : (self.resultsController as! IDTableViewController).dataSource
        if let user = dataSource?.object(at: indexPath) as? CMQUser{
            CMQPackageDataSourceManager.sharedInstance().packagesFor(resident: user, isCheckIn: true, completion: completion)
        }
    }
    
    // MARK: - Navigation

    override public func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let dest = segue.destination as? CMQPackageLogResidentViewController, let packages = sender as? [Any]{
            dest.dataSource = packages
        }
    }

}
