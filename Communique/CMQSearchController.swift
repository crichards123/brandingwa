//
//  CMQSearchController.swift
//  Communique
//
//  Created by Andre White on 1/12/18.
//  Copyright © 2018 Communique, LLC. All rights reserved.
//

import UIKit

public class CMQSearchController: UISearchController {

    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    override init(searchResultsController: UIViewController?) {
        super.init(searchResultsController: searchResultsController)
        if let textField = searchBar.value(forKey: "searchField") as? UITextField, let backgroundView = textField.subviews.first{
            backgroundView.backgroundColor = .white
            backgroundView.layer.cornerRadius = 10.0
            backgroundView.clipsToBounds = true
            searchBar.tintColor = .white
            searchBar.sizeToFit()
            searchBar.placeholder = "Resident Name"
            dimsBackgroundDuringPresentation = false
        }
    }
}
