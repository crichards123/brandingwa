//
//  UtilityFunctions.swift
//  Communique
//
//  Created by Andre White on 8/5/18.
//  Copyright © 2018 Communique, LLC. All rights reserved.
//

import Foundation

protocol Named {
    var name:String {get}
}
protocol Imaged {
    var image:UIImage {get}
}
protocol Identifier {
    var identifier:String {get}
}

protocol FloatingButton{
    var view:UIView! {get}
    var floatingButton:UIButton! {get}
    func addFloatingButton()
    func removeFloatingButton()
}
extension FloatingButton{
    func addFloatingButton(){
        guard !self.view.subviews.contains(floatingButton) else {return}
        self.view.addSubview(floatingButton)
    }
    func removeFloatingButton(){
        floatingButton.removeFromSuperview()
    }
}
///Simple protocol to get saved data from an object. In this case, a view controller.
protocol SavedData{
    var savedData:CollectionViewData? {get}
}
public protocol Observer{
    func addObservers()
    func removeObservers()
    func handle(notification:Notification)
}
extension Observer{
    public func removeObservers(){
        NotificationCenter.default.removeObserver(self)
    }
}
public protocol Updating{
    func startUpdating()
    func stopUpdating()
}
public protocol UpdatingLabel:Updating{
    var updatingLabel:UILabel {get}
    var updatingText:String {get}
    var updatedText:String {get}
}
extension UpdatingLabel{
    
    public func startUpdating(){
        self.updatingLabel.text = updatingText
        self.animateLabel()
    }
    private func animateLabel(){
        self.updatingLabel.fadeOut { (finished) in
            if finished{
                self.updatingLabel.fadeIn(completion: { (done) in
                    if done{
                        self.animateLabel()
                    }
                })
            }
        }
    }
    public func stopUpdating(){
        self.updatingLabel.text = updatedText
        self.updatingLabel.layer.removeAllAnimations()
        self.updatingLabel.alpha = 1.0
    }
    public var updatingText:String{
        return "Updating"
    }
}
public protocol BottomSlideView{
    var view:UIView {get}
    var parentView:UIView {get}
    func slideUp(duration:TimeInterval, completion:@escaping (Bool)->Void)
    func slideDown(duration:TimeInterval, completion:@escaping (Bool)->Void)
}
extension BottomSlideView{
    func slideUp(duration:TimeInterval = 1.0, completion:@escaping (Bool)->Void){
        self.view.slideFromBottom(of: self.parentView, duration: duration, completion: completion)
    }
    func slideDown(duration:TimeInterval = 1.0, completion:@escaping (Bool)->Void){
        self.view.slideDown(duration: duration, completion: completion)
    }
}
public protocol Thermostat:Equatable{
    var currentMode:SmartDevice.ThermostatMode! {get set}
    var currentTemp:String! {get set}
    var setToTemp:String! {get set}
    var controlIndex:Int! {get}
}

public protocol FiniteStateMachine {
    associatedtype State
    var state:State! {get}
    mutating func willChangeState(to:State)
}

protocol BoundedBy {
    associatedtype Units
    var bounds:(upper:Units, lower: Units) {get set}
}
public struct GenericBounded<T:Comparable&Strideable>:BoundedBy{
    typealias Units = T
    var value:T
    var bounds: (upper: T, lower: T)
    mutating func change(to:T){
        if to > bounds.upper{
            self.value = bounds.upper
        }else if to < bounds.lower{
            self.value = bounds.lower
        }else{
            self.value = to
        }
    }
}
extension GenericBounded{
    var range:Range<T>{
        return self.bounds.lower..<self.bounds.upper
    }
    var rangeCount:T.Stride{
        return self.bounds.lower.distance(to: self.bounds.upper)
    }
    init(value:T, range:Range<T>){
        self.value = value
        self.bounds = (range.lowerBound,range.upperBound)
    }
}

extension UIView{
    func fadeIn(duration:TimeInterval = 1.0, completion:@escaping (Bool)->Void){
        UIView.animate(withDuration: duration, delay: 0, options: UIView.AnimationOptions.curveEaseIn, animations: {
            self.alpha = 1.0
        }, completion: completion)
    }
    func fadeOut(duration:TimeInterval = 1.0, completion:@escaping (Bool)->Void){
        UIView.animate(withDuration: duration, delay: 0, options: UIView.AnimationOptions.curveEaseIn, animations: {
            self.alpha = 0.0
        }, completion: completion)
    }
    func slideFromBottom(of view:UIView, duration:TimeInterval = 1.0,completion:@escaping (Bool)->Void){
        self.frame.origin = CGPoint.init(x: 0, y: view.frame.height)
        view.addSubview(self)
        UIView.animate(withDuration: duration, animations: {
            self.frame.origin.y -= self.frame.height
        }, completion: completion)
    }
    func slideDown(duration:TimeInterval = 1.0,completion:@escaping (Bool)->Void){
        UIView.animate(withDuration: duration, animations: {
            self.frame.origin.y += self.frame.height
        }) { (finished) in
            self.removeFromSuperview()
            completion(finished)
        }
    }
}
extension CGRect{
    //returns center point of a rect
    var center:CGPoint{
        return CGPoint.init(x: self.size.width/2, y: self.size.height/2)
    }
    //Returns a rect that is in the center of this rect with the size given
    func centerRect(with size:CGSize)->CGRect{
        var newSize = size
        if size.height > self.size.height{
            newSize.height = self.size.height
        }
        if size.width > self.size.width{
            newSize.width = self.size.width
        }
        let x = self.center.x - size.width/2
        let y = self.center.y - size.height/2
        return CGRect.init(origin: CGPoint.init(x: x, y: y), size: newSize)
    }
    //Splits the rect into equal sections in a given direction.
    func split(direction:PaddingDirection, sections:Int)->[CGRect]{
        var rects = [CGRect]()
        switch direction {
        case .x:
            
            let maxWidth = self.size.width/CGFloat(sections)
            let childSize = CGSize.init(width: maxWidth, height: self.size.height)
            for num in 0...sections-1{
                let curOrigin = CGPoint.init(x: self.origin.x+(maxWidth*CGFloat(num)), y: self.origin.y)
                rects.append(CGRect.init(origin: curOrigin, size: childSize))
                
            }
        case .y:
            let maxHeight = self.size.height/CGFloat(sections)
            let childSize = CGSize.init(width: self.size.width, height: maxHeight)
            for num in 0...sections-1{
                let curOrigin = CGPoint.init(x: self.origin.x, y: self.origin.y+(maxHeight*CGFloat(num)))
                rects.append(CGRect.init(origin: curOrigin, size: childSize))
            }
        }
        return rects
    }
}
extension UISwitch:Viewable{
    func layout(in rect: CGRect) {
        self.frame = rect
    }
}
extension UILabel:Viewable{
    func layout(in rect: CGRect) {
        self.frame = rect
    }
}
struct CenterLayout:Viewable{
    var size:CGSize
    func layout(in rect: CGRect) {
        
    }
}
extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(rgb: Int) {
        self.init(
            red: (rgb >> 16) & 0xFF,
            green: (rgb >> 8) & 0xFF,
            blue: rgb & 0xFF
        )
    }
}
