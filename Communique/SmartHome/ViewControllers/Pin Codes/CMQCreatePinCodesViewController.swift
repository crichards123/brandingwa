//
//  CMQCreatePinCodesViewController.swift
//  Communique
//
//  Created by Andre White on 9/5/18.
//  Copyright © 2018 Communique, LLC. All rights reserved.
//

import UIKit

public class CMQCreatePinCodesViewController: UIViewController {
    @IBOutlet weak var saveButton: UIBarButtonItem!
    @IBOutlet var pinFields: [UITextField]!
    @IBOutlet weak var nameField: UITextField!
    
    override public func viewDidLoad() {
        super.viewDidLoad()
    
    }
    @IBAction func textEntered(_ sender: UITextField) {
        if sender != nameField{
            if sender.text == ""{
                if let field = self.prev(field: sender){
                    field.becomeFirstResponder()
                }
            }else{
                if let field = self.next(field: sender){
                    field.becomeFirstResponder()
                }else{
                    sender.resignFirstResponder()
                }
            }
        }
        
        self.handleSaveButton()
    }
    fileprivate func next(field:UITextField)->UITextField?{
        if field.tag == 3{
            return nil
        }
        return self.pinFields.filter({$0.tag == field.tag + 1}).first
    }
    fileprivate func prev(field:UITextField)->UITextField?{
        if field.tag == 0{
            return nil
        }
        return self.pinFields.filter({$0.tag == field.tag - 1}).first
    }
    func handleSaveButton(){
        guard pinFields.filter({$0.text?.count == 0}).count == 0 else {self.saveButton.isEnabled = false;return}
        guard nameField.text != "" else {self.saveButton.isEnabled = false;return}
        self.saveButton.isEnabled = true
    }
    fileprivate func formPin()->String{
        var pin = ""
        for field in self.pinFields.sorted(by: {$0.tag < $1.tag}){
            pin.append(field.text!)
        }
        return pin
    }
    @IBAction func savePressed(_ sender: Any) {
        self.saveButton.isEnabled = false
        let pin = self.formPin()
        guard let aptUnit = CMQAPIManager.sharedInstance().smartAptUnit else {return}
        let params = aptUnit.paramsFor(new: ["mode":pin, "name":self.nameField.text!])
        PFCloud.callFunction(inBackground: "smartApartmentCommand", withParameters: params) { (some, error) in
            if let error = error{
                print(error)
            }else{
                DispatchQueue.main.asyncAfter(deadline: .now()+1.0, execute: {
                     self.performSegue(withIdentifier: "Save", sender: nil)
                })
            }
        }
    }
}
extension CMQCreatePinCodesViewController:UITextFieldDelegate{
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField != nameField{
            let str = (textField.text! + string)
            if str.count <= 1 {
                return true
            }
            textField.text = String(str[str.startIndex..<str.index(after: str.startIndex)])//str.substring(to: str.index(str.startIndex, offsetBy: 1))
            return false
        }else{
            self.handleSaveButton()
            return true
        }
        
    }
    
}
