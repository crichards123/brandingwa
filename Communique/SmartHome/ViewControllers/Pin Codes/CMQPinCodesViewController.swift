//
//  CMQPinCodesViewController.swift
//  Communique
//
//  Created by Andre White on 9/5/18.
//  Copyright © 2018 Communique, LLC. All rights reserved.
//

import UIKit

public class CMQPinCodesViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tapGesture: UITapGestureRecognizer!
    fileprivate var shouldShowControls:IndexPath!
    fileprivate var deleteCompletion = {
        
    }
    fileprivate var pinCodes:[[String:String]]!
    override public func viewDidLoad() {
        super.viewDidLoad()
        loadCodes()
        self.tableView.tableFooterView = UIView()
    }
    public override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let nav = self.navigationController as? CMQSmartHomeNavigationController{
            nav.removeFloatingButton()
        }
    }
    func loadCodes(){
        let _ = try? CMQAPIManager.sharedInstance().smartAptUnit?.fetch()
        self.pinCodes = CMQAPIManager.sharedInstance().smartAptUnit?.pinCodes["pincodes"] as? [[String : String]]
    }
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(handle(notification:)), name: PresetCell.DeleteNotification, object: nil)
        if let nav = self.navigationController as? CMQSmartHomeNavigationController{
            nav.addFloatingButton()
        }
    }
    public override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
        if let path = shouldShowControls{
            shouldShowControls = nil
            self.tableView.reloadRows(at: [path], with: .automatic)
            //self.tapGesture.isEnabled = false
        }
    }
    @objc func handle(notification:Notification){
        let cell = notification.object as! UITableViewCell
        guard let path = self.tableView.indexPath(for: cell) else {return}
        switch notification.name{
        case PresetCell.DeleteNotification:
            let pinCode = self.pinCodes[path.row]
            self.deletePin(pinId: pinCode.keys.first!) { (error) in
                if let error = error{
                    print(error)
                }else{
                    if self.shouldShowControls != nil{
                        self.shouldShowControls = nil
                    }
                    self.pinCodes.remove(at: path.row)
                    self.tableView.deleteRows(at: [path], with: .automatic)
                    self.loadCodes()
                }
            }
        default:
            return
        }
        //self.tapGesture.isEnabled = false
        
    }
    func deletePin(pinId:String,completion:@escaping (Error?)->Void){
        guard let aptUnit = CMQAPIManager.sharedInstance().smartAptUnit else {return}
        let params = aptUnit.paramsFor(pin: pinId)
        PFCloud.callFunction(inBackground: "smartApartmentCommand", withParameters: params) { (some, error) in
            completion(error)
        }
    }
    
    
    @IBAction func longPressed(_ sender: UILongPressGestureRecognizer) {
        if sender.state == .began{
            self.tapGesture.isEnabled = true
            let point = sender.location(in: self.tableView)
            if let indexPath = self.tableView.indexPathForRow(at: point){
                var paths = [indexPath]
                if shouldShowControls != nil{
                    paths.append(shouldShowControls!)
                }
                self.shouldShowControls = indexPath
                self.tableView.reloadRows(at: paths, with: .automatic)
            }
            
        }
    }
    @IBAction func screenTapped(_ sender: UITapGestureRecognizer) {
        if let path = shouldShowControls{
            shouldShowControls = nil
            self.tableView.reloadRows(at: [path], with: .automatic)
            self.tapGesture.isEnabled = false
        }
    }
    @IBAction func pinSaved(_ sender:UIStoryboardSegue){
        self.loadCodes()
        self.tableView.reloadData()
    }

}
extension CMQPinCodesViewController: UITableViewDataSource, UITableViewDelegate{
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pinCodes.count
    }
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Pin", for: indexPath)
        let dict = pinCodes[indexPath.row]
        (cell.viewWithTag(1) as! UILabel).text  = dict.first?.value
        if shouldShowControls == indexPath{
            (cell.viewWithTag(333) as! UIButton).isHidden = false
        }else{
            (cell.viewWithTag(333) as! UIButton).isHidden = true
        }
        return cell
    }
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let path = shouldShowControls{
            shouldShowControls = nil
            tableView.reloadRows(at: [path], with: .automatic)
        }
    }
    
}
