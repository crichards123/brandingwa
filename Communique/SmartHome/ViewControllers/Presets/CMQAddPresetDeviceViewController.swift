//
//  CMQAddPresetDeviceViewController.swift
//  Communique
//
//  Created by Andre White on 7/23/18.
//  Copyright © 2018 Communique, LLC. All rights reserved.
//

import Foundation

public class CMQDeviceManipController:UIViewController, UICollectionViewDataSource{
    public static let DevicesUpdatedNotification = Notification.Name("DevicesUpdated")
    var data:CollectionViewData!
    var savedData:CollectionViewData?
    @IBOutlet weak var collectionView: UICollectionView!{
        didSet{
            collectionView.delegate = self
        }
    }
    override public func viewDidLoad() {
        super.viewDidLoad()
        addObservers()
        switch data {
        case is Room:
            self.title = (data as! Room).name
            self.savedData = data as! Room
        default:
            return
        }
    }
    override public func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        removeObservers()
    }
    private func addObservers(){
        NotificationCenter.default.addObserver(self, selector: #selector(handleSliderChange(notification:)), name: CMQDeviceManipController.DevicesUpdatedNotification, object: nil)
    }
    private func removeObservers(){
        NotificationCenter.default.removeObserver(self)
    }
    @objc private func handleSliderChange(notification:Notification){
        var devices = notification.object as! [CMQSmartDevice]
        var room = savedData as! Room
        if room.devices.count == devices.count{
            room.devices = devices
        }else{
            let deviceIDs = devices.compactMap({$0.identifier})
            let unChangedDevices = room.devices.filter({!deviceIDs.contains($0.identifier)})
            devices.append(contentsOf: unChangedDevices)
            room.devices = devices
        }
        self.savedData = room
    }
    
    public func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return data.numberOfCells
    }
    public func commandFromData()->Command{
        if self.data is Room{
            return Command.init(devices: (self.data as! Room).devices)
        }else{
            return Command.init(devices: [self.data as! CMQSmartDevice])
        }
        
    }
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: data.collectionCellIdentifier, for: indexPath) as! CMQSmartHomeCell
        if data is Room{
            cell.data = (data as! Room).deviceGroups[indexPath.row]
        }else{
            cell.data = data
        }
        cell.shouldPerformCommand = false
        return cell
    }
    
    
}
extension CMQDeviceManipController:UICollectionViewDelegateFlowLayout{
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let padding = 10 * 2
        let width = min(self.view.frame.width - CGFloat(padding), 415-CGFloat(padding))
        var room = self.data as! Room
        if let devices = SmartHomeDataSource.shared.cachedDevices[room.name] as? [CMQSmartDevice]{
            room.devices = devices
        }
        let data = room.deviceGroups[indexPath.row]
        
        let tableHeight = (100*data.numberOfCells)
        let height = data.groupType == GroupedControlData.GroupType.Fans ? CGFloat(50 + tableHeight) : CGFloat(140 + tableHeight)
        return CGSize.init(width: width, height: height)
    }
}
