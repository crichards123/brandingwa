//
//  CMQPresetAddMenuViewController.swift
//  Communique
//
//  Created by Andre White on 7/24/18.
//  Copyright © 2018 Communique, LLC. All rights reserved.
//

import Foundation
public class CMQPresetAddMenuViewController:UICollectionViewController, UICollectionViewDelegateFlowLayout{
    var viewStretched:Bool = false
    var userDevices:[CollectionViewData]!
    var availableDevices:[CollectionViewData]!{
        guard let devices = CMQAPIManager.sharedInstance().smartAptUnit?.userDevicesViewable else {return nil}
        let allIDs = devices.compactMap { (data) -> String in
            if let room = data as? Room{
                return room.identifier
            }else{
                let device = data as! CMQSmartDevice
                return device.identifier
            }
        }
        let theDevices = userDevices as [CollectionViewData]
        let currentlyUsedIds = theDevices.compactMap { (data) -> String in
            if let room = data as? Room{
                return room.identifier
            }else{
                let device = data as! CMQSmartDevice
                return device.identifier
            }
        }
        let usableIds = Set(allIDs).subtracting(currentlyUsedIds)
        
        return devices.filter({ (data) -> Bool in
            if let room = data as? Room{
                return usableIds.contains(room.identifier)
            }else{
                let device = data as! CMQSmartDevice
                return usableIds.contains(device.identifier)
            }
        })
    }
    
    override public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            if let button = SmartHomeDataSource.shared.button(at: indexPath){
                if button != .Rooms{
                    guard let navCon = self.presentingViewController as? CMQSmartHomeNavigationController else {return}
                    navCon.topViewController?.dismiss(animated: true, completion: {
                        let topController = navCon.topViewController as! CMQCreatePresetViewController
                        let segue = CMQCreatePresetViewController.Segue.init(editable: button.collectionViewData!)!
                        topController.performSegue(withIdentifier: segue.identifier, sender: button.collectionViewData)
                    })
                }else{
                    if !viewStretched{
                        
                        var size = self.view.frame.size
                        size.height += CGFloat((100 * CMQAPIManager.sharedInstance().smartAptUnit!.userRooms.count))
                        self.preferredContentSize = size
                        viewStretched = true
                    }
                    
                }
            }
        }else{
            guard let navCon = self.presentingViewController as? CMQSmartHomeNavigationController else {return}
            navCon.topViewController?.dismiss(animated: true, completion: {
                let topController = navCon.topViewController as! CMQCreatePresetViewController
                guard let smApt = CMQAPIManager.sharedInstance().smartAptUnit else {return}
                let segue = CMQCreatePresetViewController.Segue.init(editable: smApt.userRooms[indexPath.row])!
                topController.performSegue(withIdentifier: segue.identifier, sender: smApt.userRooms[indexPath.row])
            })
        }
        
    }
    override public func numberOfSections(in collectionView: UICollectionView) -> Int {
        guard let _ = CMQAPIManager.sharedInstance().smartAptUnit else {return 1}
        return 2
        
    }
    override public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        //guard let devices = availableDevices else {return 0}
        //return devices.count
        switch section{
        case 0:
            return SmartHomeDataSource.shared.collectionView(_:collectionView, numberOfItemsInSection:section)
        case 1:
            guard let smApt = CMQAPIManager.sharedInstance().smartAptUnit else {return 0}
            return smApt.userRooms.count
        default:
            return 0
        }
    }
    public override func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        if indexPath.section == 0{
            let button = SmartHomeDataSource.shared.button(at: indexPath)!
            switch button{
                
            case .Lock:
                return (self.userDevices.filter({$0 is CMQSmartDevice}) as! [CMQSmartDevice]).filter({$0.device.deviceTypeString == "lock"}).count == 0
            case .Rooms:
                return self.userDevices.filter({$0 is Room}).count != CMQAPIManager.sharedInstance().smartAptUnit!.userRooms.count
            case .Thermostat:
                return (self.userDevices.filter({$0 is CMQSmartDevice}) as! [CMQSmartDevice]).filter({$0.device.deviceTypeString == "thermostat"}).count == 0
            case .Presets:
                return false
            case .PinCode:
                return false
            }
        }else{
            let room = CMQAPIManager.sharedInstance().smartAptUnit?.userRooms[indexPath.row]
            return (self.userDevices.filter({$0 is Room}) as! [Room]).filter({$0.identifier==room?.identifier}).count == 0
        }
    }
    override public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.section == 0 {
            return SmartHomeDataSource.shared.collectionView(collectionView, cellForItemAt: indexPath)
        }else{
            let room = CMQAPIManager.sharedInstance().smartAptUnit!.userRooms[indexPath.row]
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Room", for: indexPath)
            let buttonView = cell.viewWithTag(777) as? UIButton
            buttonView?.setTitle(room.name, for: .normal)
            return cell
        }
    }
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if indexPath.section == 0 {
            let width = (collectionView.frame.width/3) - CGFloat(10)
            let height:CGFloat = 150
            return CGSize.init(width: width, height: height)
        }else{
            let width = collectionView.frame.width
            return CGSize.init(width: width, height: 100)
        }
        
        
    }
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        if section == 1{
            return 2
        }
        return 25
    }
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        if section == 0 {
            return UIEdgeInsets(top: 0, left: 0, bottom: 10, right: 0)
        }else{
            return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0  )
        }
    }
    public override func viewDidLoad() {
        super.viewDidLoad()
        self.collectionView?.delegate = self
        SmartHomeDataSource.shared.shouldContainPresets = false
        SmartHomeDataSource.shared.shouldContainPinCodes = false
    }
    override public func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        SmartHomeDataSource.shared.shouldContainPresets = true
        SmartHomeDataSource.shared.shouldContainPinCodes = true
        
    }
}
