//
//  CMQAddPresetViewController.swift
//  Communique
//
//  Created by Andre White on 6/19/18.
//  Copyright © 2018 Communique, LLC. All rights reserved.
//

import UIKit

public class CMQCreatePresetViewController: CMQSmartHomeParentViewController {
    
    //MARK: - Enums
    public enum Segue{
        case add
        case edit(editable:CollectionViewData)
        var identifier:String{
            switch self{
            case .add:
                return "Add"
            case .edit(let editable):
                switch editable {
                case is Room:
                    return "EditRoom"
                case is CMQSmartDevice:
                    switch (editable as! CMQSmartDevice).type{
                        
                    case .Thermostat:
                        return "EditTherm"
                    case .Light, .Fan ,.Outlet:
                        return "EditRoom"
                    case .Lock:
                        return "EditLock"
                    }
                default:
                    return ""
                }
            }
        }
        init?(identifier:String, editable:CollectionViewData? = nil){
            switch identifier{
            case "Add":
                self = .add
            case "EditRoom", "EditLock", "EditTherm":
                guard let edit = editable else {return nil}
                self = .edit(editable:edit)
            default:
                return nil
            }
        }
        init?(editable:CollectionViewData){
            self = .edit(editable: editable)
        }
    }
    public enum TableAction{
        case delete([IndexPath])
        case add([IndexPath])
        case update([IndexPath])
        case none
    }
    
    //MARK: - Properties
    @IBOutlet weak var saveButton: UIBarButtonItem!
    @IBOutlet weak var presetNameField: UITextField!{
        didSet{
            presetNameField.text = presetName ?? ""
        }
    }
    @IBOutlet weak var addLabel: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    fileprivate var indexOfEditingData:Array<CollectionViewData>.Index?
    public var presetName:String?
    public var presetId:String?
    public var devices:[CollectionViewData]! = [CollectionViewData]()
    private var transitionDelegate:CMQTransitionDelegate?
    public var state: State! = .Ready
    public var tableAction:TableAction!
    
    fileprivate var shouldEnableSaveButton:Bool{
        guard self.devices.count > 0 else {return false}
        guard let name = presetNameField.text else {return false}
        guard name != "" else {return false}
        return true
    }
    fileprivate var shouldHideAddLabel:Bool{
        guard self.devices.count != 0 else {return false}
        return true
    }
    
    
    
    // MARK: - Life Cycle
    override public func viewDidLoad() {
        super.viewDidLoad()
        self.addGestures()
        self.updateUI()
        let nav = self.navigationController as? CMQSmartHomeNavigationController
        //nav?.addFloatingButton()
    }
    public override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        removeObservers()
    }
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        addObservers()
    }
    func addObservers(){
        NotificationCenter.default.addObserver(self, selector: #selector(handle(notification:)), name: CMQSmartHomeCell.DeletePresetNotification, object: nil)
    }
    func removeObservers(){
        NotificationCenter.default.removeObserver(self)
    }
    @objc func handle(notification:Notification){
        if let cell = notification.object as? UICollectionViewCell{
            if let indexPath = self.collectionView.indexPath(for: cell){
                self.devices.remove(at: indexPath.row)
                if indexPath.row == indexOfEditingData{
                    indexOfEditingData = nil
                }
                self.updateUI(action: .delete([indexPath]))
            }
        }
    }
    
    // MARK: - Navigation
    override public func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        guard let identifier = Segue.init(identifier: segue.identifier!, editable: sender is CMQSmartDevice ? (sender as? CMQSmartDevice): sender as? Room)else {return}//segue.identifier.flatMap(Segue.init) else {return}
        switch identifier {
        case .add:
            let controller = segue.destination as! CMQPresetAddMenuViewController
            controller.userDevices = devices
            let viewFrame = self.view.frame
            let height = viewFrame.size.height*0.25
            transitionDelegate = CMQTransitionDelegate(size: CGSize.init(width: viewFrame.size.width, height: height), origin: CGPoint.init(x: 0, y: viewFrame.origin.y+viewFrame.height - height-10))
            segue.destination.transitioningDelegate = transitionDelegate
            segue.destination.modalPresentationStyle = .custom
        case .edit(let editable):
            let nav = segue.destination as! UINavigationController
            switch editable{
            case is Room:
                let controller = nav.topViewController as! CMQDeviceManipController
                controller.data = editable as! Room
            case is CMQSmartDevice:
                
                switch (editable as! CMQSmartDevice).type{
                    
                case .Thermostat:
                    let controller = nav.topViewController as! CMQSmartHomeThermostatViewController
                    controller.currentMode = .Preset
                    controller.thermostat = (editable as! CMQSmartDevice).device
                    controller.presetThermostat = (editable as! CMQSmartDevice).device
                    (controller.view as! CMQGradientView).gradientTopColor = UIColor.CMQSmartHomeGradientTopColor
                    (controller.view as! CMQGradientView).gradientBottomColor = UIColor.CMQSmartHomeGradientBottomColor
                    
                    
                case .Light,.Fan,.Outlet:
                    break
                case .Lock:
                    let controller = nav.topViewController as! CMQSmartHomeDoorLockViewController
                    controller.currentMode = .Preset
                    controller.lock = (editable as! CMQSmartDevice).device
                    
                    //let view = controller.view as! CMQGradientView
                    print(controller.lock)
                }
            default:
                return 
            }
        }
    }
}
// MARK: - State
extension CMQCreatePresetViewController:FiniteStateMachine{
    public typealias State = CreatePresetControllerState
    
    public enum CreatePresetControllerState{
        case Ready
        case Filled
        case Adding(devices:[CMQSmartDevice])
        
    }
    public func willChangeState(to: State) {

        switch to {
        case .Ready:
            self.state = to
            updateUI()
        case .Filled:
            self.state = to
            updateUI(action: self.tableAction)
        case .Adding(_):
            self.state = to
            updateUI()
        }
    }
    public func updateUI(action:TableAction? = nil){
        switch state! {
        case .Ready:
            self.presetNameField.text = self.presetName
        case .Adding(_):
            self.performSegue(withIdentifier: Segue.add.identifier, sender: nil)
        case .Filled:
            self.saveButton.isEnabled = shouldEnableSaveButton
            self.addLabel.isHidden = shouldHideAddLabel
            if let action = action{
                switch action{
                case .delete(let paths):
                    self.collectionView.deleteItems(at: paths)
                case .add(let paths):
                    self.collectionView.insertItems(at: paths)
                case .update(let paths):
                    self.collectionView.reloadItems(at: paths)
                case .none:
                    break
                }
            }
        }
    }
}
extension CMQCreatePresetViewController.CreatePresetControllerState:Equatable{
    public static func == (lhs: CMQCreatePresetViewController.CreatePresetControllerState, rhs: CMQCreatePresetViewController.CreatePresetControllerState) -> Bool{
        switch (lhs, rhs) {
        case (.Ready, .Ready):
            return true
        case (.Filled, .Filled):
            return true
        case let (.Adding(device1), .Adding(device2)):
            return device1 == device2
        default:
            return false
        }
    }
}
//MARK: - TextField
extension CMQCreatePresetViewController:UITextFieldDelegate{
    /*public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        updateUI(action: .none)
        return true
    }*/
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    public func textFieldDidBeginEditing(_ textField: UITextField) {
        self.addGestures()
    }
    public func textFieldDidEndEditing(_ textField: UITextField) {
        self.removeGestures()
        self.saveButton.isEnabled = shouldEnableSaveButton
    }
    @objc public func viewTapped(sender:UITapGestureRecognizer){
        self.view.endEditing(false)
    }
    fileprivate func addGestures(){
        let tapGest = UITapGestureRecognizer.init(target: self, action: #selector(viewTapped(sender:)))
        tapGest.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tapGest)
    }
    fileprivate func removeGestures(){
        if let tapGest = self.view.gestureRecognizers?.first as? UITapGestureRecognizer{
            self.view.removeGestureRecognizer(tapGest)
        }
    }
}
//MARK: - CollectionView
extension CMQCreatePresetViewController:UICollectionViewDataSource, UICollectionViewDelegate{
    public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == self.collectionView{
            self.indexOfEditingData = indexPath.row
            let segue = Segue.init(editable: devices[indexOfEditingData!])!
            self.performSegue(withIdentifier: segue
                .identifier, sender: devices[indexOfEditingData!])
        }
    }
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return devices.count
    }
    public func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let data = devices[indexPath.row]
        switch data{
        case is Room:
            let room = data as! Room
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DeviceGroup", for: indexPath) as! LightGroupCommandCell
            cell.data = room
            return cell
        case is CMQSmartDevice:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: data.collectionCellIdentifier, for: indexPath) as! CMQSmartHomeCell
            cell.data =  data.data(at: indexPath)
            return cell
        default:
            return UICollectionViewCell()
        }
    }
}
extension CMQCreatePresetViewController:UICollectionViewDelegateFlowLayout{
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let padding = 10 * 2
        let width = min(collectionView.frame.width - CGFloat(padding), 415-CGFloat(padding))
        let data = devices[indexPath.row]
        var height:CGFloat
        if data is Room {
            let room = data as! Room
            let tableHeight = (70 * room.devices.count)
            height = CGFloat(80 + tableHeight)
        }else{
            height = 105.0
        }
        return CGSize.init(width: width, height: height)
        
    }
}

extension CMQSmartHomeDoorLockViewController:SavedData{
    var savedData:CollectionViewData?{
        guard var device = CMQAPIManager.sharedInstance().smartAptUnit?.doorLock else {return nil}
        device.device = self.lock
        return device
    }
}
extension CMQSmartHomeThermostatViewController:SavedData{
    var savedData:CollectionViewData?{
        guard var device = CMQAPIManager.sharedInstance().smartAptUnit?.thermostat else {return nil}
        device.device = self.presetThermostat
        return device
    }
}
extension CMQDeviceManipController:SavedData{
    ///Implemeneted in class
}
//MARK: - IBActions
extension CMQCreatePresetViewController{
    @IBAction func selectDevicesPressed(sender:UIButton){
        guard CMQAPIManager.sharedInstance().smartAptUnit!.userDevicesViewable.count != self.devices.count else {return}
        self.willChangeState(to: .Adding(devices: [CMQSmartDevice]()))
    }
    
    @IBAction func savePressed(sender:Any){
        var commands = [Command]()
        for data in self.devices{
            if data is Room{
                let room = data as! Room
                for device in room.devices{
                    let command = Command.init(devices: [device])
                    commands.append(command)
                }
            }else if data is CMQSmartDevice{
                let device = data as! CMQSmartDevice
                if device.type == .Thermostat{
                    if device.device.currentMode.modeText != "off"{
                        let command1 = Command.init(devices: [device], command: "changeMode")
                        let command2 = Command.init(devices: [device], command: "setTemp")
                        commands.append(contentsOf: [command1, command2])
                    }else{
                        let command = Command.init(devices: [device], command: "changeMode")
                        commands.append(command)
                    }
                }else{
                    let command = Command.init(devices: [device])
                    commands.append(command)
                }
                
            }
        }
        let preset = Preset.init(name: presetNameField.text!, identifier: presetId ?? createPresetId(), commands: commands)
        preset.create {
            try? CMQUser.current().fetch()
            self.performSegue(withIdentifier: "Save", sender: nil)
        }
    }
    @IBAction func saveFromAdd(segue:UIStoryboardSegue){
        
        if let controller = segue.source as? SavedData{
            if let index = indexOfEditingData{
                self.devices[index] = controller.savedData!
                let path = IndexPath.init(row: index, section: 0)
                self.tableAction = .update([path])
                self.indexOfEditingData = nil
            }else{
                let saved = controller.savedData!
                self.devices.append(saved)
                let path = IndexPath.init(row: self.devices.endIndex-1, section:0)
                self.tableAction = .add([path])
            }
        }
        willChangeState(to: .Filled)
    }
    @IBAction func cancelFromAdd(segue:UIStoryboardSegue){
        //Canceled addition of
        guard case .Adding(let devices) = state! else {return}
        self.tableAction = .none
        willChangeState(to: .Filled)
    }
}
extension CMQCreatePresetViewController{
    func createPresetId()->String{
        let dateFormatter = DateFormatter.init()
        dateFormatter.dateFormat = "MMddyyHHmm"
        let presetId = self.presetNameField.text!.appending(dateFormatter.string(from: Date.init(timeIntervalSinceNow: 0)))
        return presetId
    }
}

extension UIColor{
    static var CMQSmartHomeGradientTopColor:UIColor{
        return UIColor.init(red: 115/255.0, green: 198/255.0, blue: 238/255.0, alpha: 1)
    }
    static var CMQSmartHomeGradientBottomColor:UIColor{
        return UIColor.init(red: 52/255.0, green: 89/255.0, blue: 124/255.0, alpha: 1)
    }
}
