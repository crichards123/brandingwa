
//
//  CMQPresetsViewController.swift
//  Communique
//
//  Created by Andre White on 6/19/18.
//  Copyright © 2018 Communique, LLC. All rights reserved.
//

import UIKit


public class PresetCell:UITableViewCell{
    static let DeleteNotification = Notification.Name("Delete")
    static let EditNotification = Notification.Name("Edit")
    @IBOutlet weak var label:UILabel!
    @IBOutlet var controls:[UIButton]!

    @IBAction func controlPressed(control:UIButton){
        if control.tag == 333{
            //Delete Button
            NotificationCenter.default.post(name: PresetCell.DeleteNotification, object: self)
        }else{
            NotificationCenter.default.post(name: PresetCell.EditNotification, object: self)
        }
    }
}

public class CMQPresetsViewController: UIViewController, FiniteStateMachine {
    public var state: State!
    @IBOutlet public var tableView:UITableView!{
        didSet{
            self.tableView.tableFooterView = UIView()
        }
    }
    var updatingView:UpdatingView!
    public var presets:[Preset] = [Preset]()
    public func willChangeState(to: State) {
        guard self.state != to else {return}
        switch to {
        case .Ready:
            //Set DataSource Preset
            //Update TableView
            self.tableView.reloadData()
        case .Activating:
            //Activate Preset w WebCall
            //When done, go back to .Ready with new Presets
            print("Activating")
        case .Editing(let preset):
            //Editing Preset
            print(preset)
        }
    }
    
    @IBOutlet var tapGesture: UITapGestureRecognizer!
    public typealias State = PresetsControllerState
    fileprivate var shouldShowControls:IndexPath?

    public enum PresetsControllerState{
        case Ready
        case Activating
        case Editing(preset:Preset)
    }
    override public func viewDidLoad() {
        super.viewDidLoad()
        
        updatePresets()
        // Do any additional setup after loading the view.
    }
    override public func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
        if let path = shouldShowControls{
            shouldShowControls = nil
            self.tableView.reloadRows(at: [path], with: .automatic)
            //self.tapGesture.isEnabled = false
        }
        if let nav = self.navigationController as? CMQSmartHomeNavigationController{
            nav.removeFloatingButton()
        }
    }
    
    override public func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(handle(notification:)), name: PresetCell.DeleteNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handle(notification:)), name: PresetCell.EditNotification, object: nil)
        
        if let nav = self.navigationController as? CMQSmartHomeNavigationController{
            nav.addFloatingButton()
        }
    }
    
    @objc func handle(notification:Notification){
        let cell = notification.object as! UITableViewCell
        guard let path = self.tableView.indexPath(for: cell) else {return}
        switch notification.name{
        case PresetCell.DeleteNotification:
            let preset = self.presets[path.row]
            preset.deleteFromServer {
                if self.shouldShowControls != nil{
                    self.shouldShowControls = nil
                }
                self.presets.remove(at: path.row)
                self.tableView.deleteRows(at: [path], with: .automatic)
            }
        case PresetCell.EditNotification:
            let preset = self.presets[path.row]
            self.performSegue(withIdentifier: "AddPreset", sender: preset)
        default:
            return
        }
        //self.tapGesture.isEnabled = false
        
    }
    func updatePresets(){
        guard let presets = CMQAPIManager.sharedInstance().smartAptUnit?.userPresets else {return}
        self.presets = presets
        willChangeState(to: .Ready)
    }
    @IBAction func longPressed(_ sender: UILongPressGestureRecognizer) {
        if sender.state == .began{
            //self.tapGesture.isEnabled = true
            let point = sender.location(in: self.tableView)
            if let indexPath = self.tableView.indexPathForRow(at: point){
                var paths = [indexPath]
                if shouldShowControls != nil{
                    paths.append(shouldShowControls!)
                }
                self.shouldShowControls = indexPath
                self.tableView.reloadRows(at: paths, with: .automatic)
            }
           
        }
    }
    @IBAction func screenTapped(_ sender: UITapGestureRecognizer) {
        if let path = shouldShowControls{
            shouldShowControls = nil
            self.tableView.reloadRows(at: [path], with: .automatic)
            //self.tapGesture.isEnabled = false
        }
    }
    // MARK: - Navigation
    @IBAction public func savedPreset(sender:UIStoryboardSegue){
        if let _ = sender.source as? CMQCreatePresetViewController{
            self.updatePresets()
        }
    }
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override public func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "AddPreset"{
            if let preset = sender as? Preset{
                let dest = segue.destination as! CMQCreatePresetViewController
                dest.devices = preset.commands.collectionViewData
                dest.state = .Filled
                dest.presetName = preset.name
                dest.presetId = preset.identifier
                //dest.presetNameField?.text = preset.name
            }
            
        }
    }
}
extension CMQPresetsViewController:UITableViewDelegate{
    typealias TableViewActionHandler = (UITableViewRowAction,IndexPath)->Void
    var tableViewActionHandler:TableViewActionHandler{
        return { (action,path)->Void in
            switch action.title!{
            case "Edit":
                let preset = self.presets[path.row]
                self.performSegue(withIdentifier: "AddPreset", sender: preset)
            case "Delete":
                let preset = self.presets[path.row]
                preset.deleteFromServer {
                    if self.shouldShowControls != nil{
                        self.shouldShowControls = nil
                    }
                    self.presets.remove(at: path.row)
                    self.tableView.deleteRows(at: [path], with: .automatic)
                }
                
            case "Activate":
                let preset = self.presets[path.row]
                preset.activate {
                    print("Activated")
                }
            default:
                return
            }
            
        }
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let path = shouldShowControls{
            shouldShowControls = nil
            tableView.reloadRows(at: [path], with: .automatic)
        }else{
            let preset = self.presets[indexPath.row]
            preset.activate {
                self.startUpdating()
                DispatchQueue.main.asyncAfter(deadline: .now()+2, execute: {
                    self.stopUpdating()
                })
            }
        }
        
        //self.performSegue(withIdentifier: "AddPreset", sender: preset)
    }/*
    public func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    public func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let edit = UITableViewRowAction.init(style: .default, title: "Edit", handler: self.tableViewActionHandler)
        edit.backgroundColor = UIColor.CMQSmartHomeGradientTopColor
        let delete = UITableViewRowAction.init(style: .destructive, title: "Delete", handler: self.tableViewActionHandler)
        let activate = UITableViewRowAction.init(style: .default, title: "Activate", handler: self.tableViewActionHandler)
        activate.backgroundColor = UIColor.CMQSmartHomeGradientBottomColor
        return [delete,edit,activate]
    }
    public func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        switch editingStyle{
            
        case .none:
            return
        case .delete:
            self.presets.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .automatic)
        case .insert:
            return
        }
    }*/
}
extension CMQPresetsViewController:UITableViewDataSource{
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.presets.count
    }
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let presetReuse = "Preset"
        let cell = tableView.dequeueReusableCell(withIdentifier: presetReuse, for: indexPath)
        (cell.viewWithTag(1) as! UILabel).text  = presets[indexPath.row].name
        if shouldShowControls == indexPath{
            (cell.viewWithTag(333) as! UIButton).isHidden = false
            (cell.viewWithTag(444) as! UIButton).isHidden = false        }else{
            (cell.viewWithTag(333) as! UIButton).isHidden = true
            (cell.viewWithTag(444) as! UIButton).isHidden = true
        }
        
        return cell
    }
}
extension CMQPresetsViewController.PresetsControllerState:Equatable{
    public static func == (lhs: CMQPresetsViewController.PresetsControllerState, rhs: CMQPresetsViewController.PresetsControllerState) -> Bool{
        switch (lhs, rhs) {
        case (.Ready,.Ready):
            return true
        case (.Activating, .Activating):
            return true
        case let (.Editing(preset1), .Editing(preset2)):
            return preset1 == preset2
        default:
            return false
        }
    }
}
extension CMQPresetsViewController:Updating{
    var bottomView:BottomView{
        let uiView = UIView.init(frame: CGRect.init(origin: .zero, size: CGSize.init(width: self.view.frame.width, height: 30)))
        let label = UILabel.init(frame: uiView.frame)
        label.textColor = .white
        label.textAlignment = .center
        label.text = "Preset Activated!"
        uiView.addSubview(label)
        uiView.addShadow()
        uiView.layer.shadowOffset = CGSize.init(width: 0, height: -2)
        uiView.backgroundColor = UIColor.init(red: 52/255.0, green: 89/255.0, blue: 124/255.0, alpha: 1.0)
        let bottom = BottomView.init(view: uiView, parentView: self.view, updatingLabel: label)
        
        return bottom
    }
    public func startUpdating() {
        self.updatingView = UpdatingView.init(bottomView: self.bottomView)
        self.updatingView.startUpdatingWithOutLabelAnimation()
    }
    public func stopUpdating() {
        self.updatingView?.stopUpdatingWithOutLabelAnimation()
    }
}
extension UpdatingView{
    func startUpdatingWithOutLabelAnimation(){
        bottomView.slideUp(duration: 0.3) { (finished) in
            if finished{

            }
        }
    }
    func stopUpdatingWithOutLabelAnimation(){
        bottomView.slideDown(duration: 0.3) { (finished) in
            if finished{

            }
        }
    }
}
