//
//  CMQSmartHomeHomeViewController.swift
//  Communique
//
//  Created by Andre White on 8/10/18.
//  Copyright © 2018 Communique, LLC. All rights reserved.
//

import UIKit

public class CMQSmartHomeHomeViewController: UIViewController {
    @IBOutlet weak var gradientView:CMQGradientView!
    @IBOutlet weak var collectionView:UICollectionView!
    @IBOutlet var Buttons: [CMQGradientButton]!
    
    @IBOutlet var viewsWShadow: [UIView]!{
        didSet{
            for view in viewsWShadow{
                view.addShadow()
            }
        }
    }
    override public func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        SmartHomeDataSource.shared.shouldContainPresets = true
        SmartHomeDataSource.shared.shouldContainPinCodes = true
        self.collectionView.delegate = self
        self.collectionView.dataSource = SmartHomeDataSource.shared
        self.collectionView.contentInset = UIEdgeInsets.init(top: 0, left: 20, bottom: 0, right: 20)
    }
    
    @IBAction func backPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override public func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let tag = sender as? Int{
            if let dest = segue.destination as? CMQContainerViewController{
                dest.initialPage = tag
                dest.selectedPath = IndexPath.init(row: tag, section: 0)
            }
        }
    }

}

extension CMQSmartHomeHomeViewController:UICollectionViewDelegateFlowLayout{
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = collectionView.frame.size
        let button = SmartHomeDataSource.shared.button(at:indexPath)!
        if button == .PinCode{
            return CGSize(width: (size.width * 0.90) , height: (size.height * 0.15) )
        }else{
            //Half the width of the view - 10 for spacing
            let height = (size.height * 0.40) - 10
            let width = (size.width * 0.45) - 10
            return CGSize(width: width, height: height)
        }
        
    }
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 2
    }
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 8
    }
    public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let button = SmartHomeDataSource.shared.button(at:indexPath){
            self.performSegue(withIdentifier: button.rawValue, sender: button.initialPageTag)
        }
    }
}
