//
//  CMQSmartHomeTableViewController.swift
//  Communique
//
//  Created by Andre White on 5/10/18.
//  Copyright © 2018 Communique, LLC. All rights reserved.
//

import UIKit

public class CMQSmartHomeTableViewController: CMQSmartHomeParentViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var tableView:UITableView!
    public var dataSource:CMQSmartHomeTableViewDataSource!
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.tableFooterView = UIView()
    
        // Do any additional setup after loading the view.
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let source = dataSource else {return 0}
        return source.numberOfDeviceGroups
    
    }
    public func numberOfSections(in tableView: UITableView) -> Int {
        guard let _ = dataSource else {return 0}
        return 1
    }
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let _ = dataSource else {return UITableViewCell()}
        //guard let cellSource = dataSource.object(at: indexPath) else {return UITableViewCell()}
        //guard let device = cellSource.
        return UITableViewCell()
    }
    

    
    // MARK: - Navigation

    @IBAction func returnFromPreset(segue:UIStoryboardSegue){
        
    }
    override public func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
}
public protocol CMQSmartHomeTableViewDataSource{
    var numberOfDeviceGroups:Int {get}
    func deviceGroup(at:IndexPath)->CMQSmartHomeTableCellDataSource?
}
public protocol CMQSmartHomeTableCellDataSource{
    var numberOfDevices:Int {get}
    func device(at:IndexPath)->CMQSmartHomeDevice?
    
}
public protocol DeviceCell{
    var smartDeviceType:CMQSmartHomeDevice.CMQSmartDeviceType {get}
    var devicePic:UIImage {get}
    var deviceMasterName:String {get}
    var deviceMasterControl:UIControl {get}
}

protocol Control:Equatable {
    associatedtype Value
    associatedtype ViewType
    var view:ViewType {get}
    var curValue:Value {get set}
    mutating func change(toValue:Value)
    func canChange(toValue:Value)->Bool
}
extension Control{
    mutating func change(toValue:Value) {
        guard canChange(toValue: toValue) else {return}
        self.curValue = toValue
    }
}

extension UISwitch:Control{
    var view: UISwitch {
        return self
    }
    typealias Value = Bool
    typealias ViewType = UISwitch
    var curValue:Value{
        get{
            return self.isOn
        }
        set{
            self.isOn = newValue
        }
    }
    func canChange(toValue: Bool) -> Bool {
        return self.curValue != toValue && self.isEnabled
    }
    
}
extension UISlider:Control{
    typealias Value = Float
    typealias ViewType = UISlider
    var view:ViewType{
        return self
    }
    var curValue:Value{
        get{
            return self.value
        }
        set{
            self.value = newValue
        }
    }
    func canChange(toValue:Value)->Bool{
        return self.curValue != toValue && toValue <= self.maximumValue && toValue >= self.minimumValue && self.isEnabled
    }
}
extension UISegmentedControl:Control{
    typealias Value = Int
    
    typealias ViewType = UISegmentedControl
    var view:ViewType{
        return self
    }
    var curValue:Value{
        get{
            return self.selectedSegmentIndex
        }
        set{
            self.selectedSegmentIndex = newValue
        }
    }
    func canChange(toValue:Value)->Bool{
        return self.selectedSegmentIndex != toValue && toValue < self.numberOfSegments && toValue >= 0
    }
    
}

extension UIImageView:Viewable{
    func layout(in rect: CGRect) {
        self.frame = rect
    }
}
/*extension UISwitch:Viewable{
    func layout(in rect: CGRect) {
        self.frame = rect
    }
}
extension UILabel:Viewable{
    func layout(in rect: CGRect) {
        self.frame = rect
    }
}*/
extension UISlider:Viewable{
    func layout(in rect: CGRect) {
        self.frame = rect
    }
}
protocol View {
    associatedtype UIViewType
    var content:[UIViewType] {get}
}
protocol Display:Viewable {
    
}

protocol Viewable {
    mutating func layout(in rect:CGRect)
    
}
protocol Displayable {
    mutating func display(in view:UIView)
}
enum PaddingDirection {
    case x
    case y
}
protocol PaddedLayoutProtocol:Viewable {
    var padding:CGFloat {get set}
    var direction:PaddingDirection {get}
}
struct SplitLayout:Viewable& Display{
    func display(in view: UIView) {
        
    }
    var leftLayout:Viewable
    var rightLayout:Viewable
    mutating func layout(in rect: CGRect) {
        let childSize = CGSize.init(width: rect.size.width/2, height: rect.size.height)
        let rightRect = CGRect.init(origin: CGPoint.init(x: childSize.width, y: rect.origin.y), size: childSize)
        let leftRect = CGRect.init(origin: rect.origin, size: childSize)
        leftLayout.layout(in: leftRect)
        rightLayout.layout(in: rightRect)
    }
}
let image = UIImage.init(named: "Beach.jpg")
struct ControlWithTitleLayout<T:Control&Viewable>:PaddedLayoutProtocol{
    
    var padding: CGFloat
    var direction: PaddingDirection
    var titleLabel:UILabel
    var control:T
    mutating func layout(in rect: CGRect) {
        var curPos:CGFloat = direction == .x ? rect.origin.x : rect.origin.y
        switch direction {
        case .x:
            let size = titleLabel.frame.size
            titleLabel.layout(in: CGRect.init(origin: CGPoint.init(x: curPos, y: rect.origin.y), size: size))
            curPos += size.width
            curPos += padding
            let frame = CGRect.init(origin: CGPoint.init(x: curPos, y: rect.origin.y), size:controlSize)
            let _ = CGPoint.init(x: (frame.size.width/2 - self.controlSize.width/2), y: frame.size.height/2 - self.controlSize.height/2)
            control.layout(in: CGRect.init(origin: CGPoint.init(x: rect.origin.x, y: curPos), size: controlSize))
            
        case .y:
            let size = titleLabel.frame.size
            titleLabel.layout(in: CGRect.init(origin: CGPoint.init(x: rect.origin.x, y: curPos), size: size))
            curPos += size.height
            curPos += padding
            let frame = CGRect.init(origin: CGPoint.init(x: rect.origin.x, y: curPos), size: controlSize)
            let _ = CGPoint.init(x: (frame.size.width/2 - self.controlSize.width/2), y: frame.size.height/2 - self.controlSize.height/2)
            control.layout(in: CGRect.init(origin: CGPoint.init(x: rect.origin.x, y: curPos), size: controlSize))
        }
    }
    
    var controlSize:CGSize{
        switch control {
        case is UISlider:
            return CGSize.init(width: 150, height: 50)
        case is UISwitch:
            return CGSize.init(width: 51, height: 31)
        case is UISegmentedControl:
            return CGSize.init(width: 200, height: 50)
        default:
            return .zero
        }
    }
}



struct SmartDeviceControl<T:Control>:Control{
    typealias Value = T.Value
    
    typealias ViewType = T
    
    var view:T
    
    var curValue: T.Value{
        get{
            return view.curValue
        }
        set{
            view.curValue = newValue
        }
    }
    func canChange(toValue: T.Value) -> Bool {
        return view.canChange(toValue: toValue)
    }
}
extension SmartDeviceControl{
    mutating func action(with:T){
        if self.view == with{
            change(toValue: with.curValue)
        }
    }
    var image:UIImage{
        return #imageLiteral(resourceName: "Light on")
    }
}
extension SmartDeviceControl:View{
    typealias UIViewType = ViewType
    
    var content: [T] {
        get {
            return [view]
        }
    }
}

protocol MasterControlProtocol:Control {
    var subControls:[ViewType] {get set}
    mutating func change(subControl atIndex:Int, toValue:Value)
}
let count = 0
struct MasterSmartDeviceLayout<T:Control&Viewable>:Viewable {
    
    var control:MasterSmartDeviceControl<T>
    var numOfcontrols:Int{
        return control.allControls.count
    }
    
    mutating func layout(in rect: CGRect) {
        var layouts = control.allControls.map({createLayout(forContol: $0)})
        let rects = self.splitRect(rect: rect)
        for index in layouts.indices{
            layouts[index].layout(in: rects[index])
        }
        
    }
    private func splitRect(rect:CGRect)->[CGRect]{
        var rects = [CGRect]()
        let rectSize = CGSize.init(width: rect.width, height: rect.height/CGFloat(numOfcontrols))
        for i in self.control.allControls.indices{
            let arbPoint = rect.origin.y + (CGFloat(i)*rectSize.height)
            rects.append(CGRect.init(origin: CGPoint.init(x: 0, y: arbPoint), size: rectSize))
        }
        return rects
    }
    init(control: MasterSmartDeviceControl<T>, otherContent:[UIView]? = nil) {
        self.control = control
        
    }
    private mutating func createLayout(forContol:T)->SplitLayout{
        let leftLayout = UIImageView.init(image: image)
        let titleLabel = UILabel.init(frame: CGRect.init(origin: .zero, size: CGSize.init(width: 150, height: 50)))
        titleLabel.textAlignment = .center
        titleLabel.text = "HEY"
        let rightLayout = ControlWithTitleLayout.init(padding: 7, direction: .y, titleLabel: titleLabel, control: forContol)
        
        _privateContent.append(leftLayout)
        _privateContent.append(titleLabel)
        
        return SplitLayout.init( leftLayout: leftLayout, rightLayout: rightLayout)
        
    }
    var _privateContent:[UIView] = [UIView]()
}
extension MasterSmartDeviceLayout{
    
    mutating func display(in view:UIView){
        self.layout(in: view.frame)
        for content in _privateContent{
            view.addSubview(content)
        }
        for control in self.control.allControls{
            switch control {
            case is UISwitch:
                let aSwitch = control as! UISwitch
                view.addSubview(aSwitch)
            case is UISlider:
                let aSlider = control as! UISlider
                view.addSubview(aSlider)
            case is UISegmentedControl:
                let seg = control as! UISegmentedControl
                view.addSubview(seg)
            default:
                return
            }
        }
    }
}



struct MasterSmartDeviceControl<T:Control>:MasterControlProtocol {
    
    typealias Value = T.Value
    typealias ViewType = T
    init(view:ViewType, subControls:[ViewType] = [ViewType]()) {
        self.subControls = subControls
        self.view = view
    }
    mutating func add(masterControl:ViewType){
        self.view = masterControl
    }
    mutating func add(control:ViewType){
        self.subControls.append(control)
    }
    mutating func replace(control atIndex:Int, withControl:ViewType){
        self.subControls.replaceSubrange(atIndex...atIndex, with: [withControl])
    }
    init(view:ViewType, numberOfSubControls:Int) {
        subControls = [ViewType]()
        self.view = view
        switch view {
        case is UISwitch:
            for _ in 1...numberOfSubControls{
                let control = UISwitch.init() as! ViewType
                subControls.append(control)
            }
        case is UISlider:
            for _ in 1...numberOfSubControls{
                let control = UISlider.init() as! ViewType
                subControls.append(control)
            }
        case is UISegmentedControl:
            for _ in 1...numberOfSubControls{
                let control = UISegmentedControl.init() as! ViewType
                subControls.append(control)
            }
        default:
            break
        }
    }
    var view:T
    var curValue: T.Value{
        get{
            return view.curValue
        }
        set{
            view.curValue = newValue
        }
    }
    var subControls: [ViewType]
    
    mutating func change(toValue: T.Value) {
        self.view.change(toValue: toValue)
        for index in subControls.indices{
            subControls[index].change(toValue: toValue)
        }
    }
    mutating func change(subControl atIndex:Int, toValue:Value){
        guard atIndex < subControls.count && atIndex >= 0 else {return}
        subControls[atIndex].change(toValue: toValue)
    }
    func canChange(toValue: T.Value) -> Bool {
        return view.canChange(toValue: toValue)
    }
    mutating func action(with:T){
        if self.view == with{
            change(toValue: with.curValue)
        }else if let index = self.subControls.index(where: {$0==with}){
            change(subControl: index, toValue: with.curValue)
        }
    }
}
extension MasterSmartDeviceControl{
    var allControls:[T]{
        var array = [view]
        array.append(contentsOf: self.subControls)
        return array
    }
}
public struct CMQSmartHomeDevice:Hashable{
    var isActive:Bool{
        return true
    }
    var deviceType:CMQSmartDeviceType
    
    public enum CMQSmartDeviceType:String{
        case Outlet = "Outlet"
        case Light = "Light"
        case Fan = "Fan"
    }
}

extension CMQSmartHomeDevice:Equatable{
    public static func == (lhs: CMQSmartHomeDevice, rhs: CMQSmartHomeDevice) -> Bool {
        return lhs.deviceType == rhs.deviceType && lhs.isActive == rhs.isActive
    }
}
extension CMQSmartHomeDevice.CMQSmartDeviceType:Hashable{}

