//
//  CMQSmartHomeParentViewController.swift
//  Communique
//
//  Created by Andre White on 5/9/18.
//  Copyright © 2018 Communique, LLC. All rights reserved.
//

import UIKit
extension UIView:ShadowProtocol{
    
}
public class CMQSmartHomeParentViewController: UIViewController {
    @IBOutlet weak var gradientView:CMQGradientView!
    @IBOutlet var Buttons: [CMQGradientButton]!
    
    @IBOutlet var viewsWShadow: [UIView]!{
        didSet{
            for view in viewsWShadow{
                view.addShadow()
            }
        }
    }
    override public func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        // Do any additional setup after loading the view.
    }

    override public func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func backPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override public func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "Presets"{
            let _ = self.navigationController as? CMQSmartHomeNavigationController
            //nav?.addFloatingButton()
        }else if let button = sender as? UIButton{
            if let dest = segue.destination as? CMQContainerViewController{
                dest.initialPage = button.tag
            }
        }
    }
}
