//
//  CMQRoomSelectViewController.swift
//  Communique
//
//  Created by Andre White on 6/29/18.
//  Copyright © 2018 Communique, LLC. All rights reserved.
//

import UIKit
public class CMQRoomCollectionCell:UICollectionViewCell{
    @IBOutlet var imageView:UIImageView!
    @IBOutlet var label:UILabel!
    @IBOutlet weak var backView: CMQGradientView!{
        didSet{
            backView.addShadow()
        }
    }
    @IBAction func settingsPressed(_ sender: UIButton) {
        NotificationCenter.default.post(name: CMQRoomSelectViewController.SettingsPressedNotification, object: self)
    }
}
public class CMQRoomSelectViewController: UIViewController {
    @IBOutlet weak var collectionView: UICollectionView!
    public var rooms:[Room]!
    private var observer:DeviceObserver<CMQRoomSelectViewController>?
    public override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Room"{
            let dest = segue.destination as! CMQRoomViewController
            dest.roomIndex = CMQAPIManager.sharedInstance().smartAptUnit?.userRooms.index(of: sender as! Room)
        }
    }
    override public func viewDidLoad() {
        self.observer = DeviceObserver.init(controller: self)
        CMQAPIManager.sharedInstance().smartAptUnit?.deviceDelegate = self
    }
}
extension CMQRoomSelectViewController:SmartUnitDeviceProtocol{
    public func devicesUpdated() {
        self.collectionView.reloadData()
    }
}


extension CMQRoomSelectViewController:UICollectionViewDataSource{
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let smartApt = CMQAPIManager.sharedInstance().smartAptUnit else {return UICollectionViewCell()}
        let room = smartApt.userRooms[indexPath.row]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Room", for: indexPath) as! CMQRoomCollectionCell
        cell.label.text = room.name
        let imageView = cell.viewWithTag(999) as? UIImageView
        
        let devices = (SmartHomeDataSource.shared.cachedDevices[room.name] as? [CMQSmartDevice])?.sorted(by: {$0.name>$1.name}) ?? room.devices.sorted(by: {$0.name>$1.name})
        let isOn = devices.filter({$0.device.deviceTypeString == SmartDevice.Light(level: 0).deviceTypeString}).filter({$0.device.level>0}).count > 0
        imageView?.image = !isOn ? UIImage.init(named: "Light Off", in: CMQBundle, compatibleWith: nil) : UIImage.init(named: "Light on", in: CMQBundle, compatibleWith: nil)  
        return cell
    }
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let smartApt = CMQAPIManager.sharedInstance().smartAptUnit else {return 0}
        return smartApt.userRooms.count
    }
}
extension CMQRoomSelectViewController:UICollectionViewDelegateFlowLayout{
    public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let smartApt = CMQAPIManager.sharedInstance().smartAptUnit else {return}
        let room = smartApt.userRooms[indexPath.row]
        var devices = (SmartHomeDataSource.shared.cachedDevices[room.name] as? [CMQSmartDevice])?.sorted(by: {$0.name>$1.name}) ?? room.devices.sorted(by: {$0.name>$1.name})
        let isOn = devices.filter({$0.type == CMQSmartDevice.DeviceType.Light}).filter({$0.device.level>0}).count > 0
        var nonLights = devices.filter({$0.type != CMQSmartDevice.DeviceType.Light})
        devices.removeAll()
        for device in room.devices.filter({$0.type == CMQSmartDevice.DeviceType.Light}){
            var newDevice = device
            newDevice.device.level = isOn ? 0 : 100
            devices.append(newDevice)
        }
        let command = Command.init(devices: devices)
        command.execute { (error) in
            if let error = error {
                Communique.handle(error: error, message: "Updating Lights", shouldDisplay: false)
            }else{
                nonLights.append(contentsOf: devices)
                SmartHomeDataSource.shared.cachedDevices[room.name] = nonLights
                collectionView.reloadItems(at: [indexPath])
                
            }
        }
        
    }
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if CMQAPIManager.sharedInstance().smartAptUnit!.userRooms.count == 1{
            return CGSize.init(width: collectionView.frame.width, height: 168)
        }else{
            return CGSize.init(width: 168, height: 168)
        }
    }
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.init(top: 0, left: 15, bottom: 0, right: 15)
    }
}
extension CMQRoomSelectViewController:UITableViewDataSource{
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let smartApt = CMQAPIManager.sharedInstance().smartAptUnit else {return 0}
        return smartApt.userRooms.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let smartApt = CMQAPIManager.sharedInstance().smartAptUnit else {return UITableViewCell()}
        let room = smartApt.userRooms[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "Room", for: indexPath)
        let label = cell.viewWithTag(1) as! UILabel
        let imageView = cell.viewWithTag(999) as? UIImageView
        let devices = room.devices.sorted(by: {$0.name>$1.name})
        let isOn = devices.filter({$0.device.level>0}).count > 0
        imageView?.image = isOn ? UIImage.init(named: "Light on", in: CMQBundle, compatibleWith: nil) : UIImage.init(named: "Light off", in: CMQBundle, compatibleWith: nil)
        label.text = room.name
        
        return cell
    }
}
extension CMQRoomSelectViewController:UITableViewDelegate{
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let smartApt = CMQAPIManager.sharedInstance().smartAptUnit else {return}
        let room = smartApt.userRooms[indexPath.row]
        self.performSegue(withIdentifier: "Room", sender: room)
    }
}
///Overrides viewWillDisappear(_ animated: Bool) in order to remove observers.
///Overrides viewWillAppear
extension CMQRoomSelectViewController:Observer{
    static var SettingsPressedNotification:Notification.Name{
        return Notification.Name("SettingsPressed")
    }
    public func addObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(handle(notification:)), name: CMQRoomSelectViewController.SettingsPressedNotification, object: nil)
    }
    override public func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        addObservers()
    }
    override public func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        removeObservers()
    }
    @objc public func handle(notification: Notification) {
        if let roomCell = notification.object as? UICollectionViewCell{
            let indexPath = self.collectionView.indexPath(for: roomCell)!
            guard let smartApt = CMQAPIManager.sharedInstance().smartAptUnit else {return}
            let room = smartApt.userRooms[indexPath.row]
            self.performSegue(withIdentifier: "Room", sender: room)
        }
    }
}
