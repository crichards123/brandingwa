//
//  CMQRoomViewController.swift
//  Communique
//
//  Created by Andre White on 7/3/18.
//  Copyright © 2018 Communique, LLC. All rights reserved.
//

import UIKit

public class CMQRoomViewController:UIViewController, UICollectionViewDataSource{
    var roomContainer:Room!{
        guard let smApt = CMQAPIManager.sharedInstance().smartAptUnit else {return nil}
        try? smApt.fetch()
        guard let index = roomIndex else {return nil}
        return smApt.userRooms[index]
    }
    var roomIndex:Array<Room>.Index!
    @IBOutlet weak var collectionView: UICollectionView!{
        didSet{
            collectionView.delegate = self
        }
    }
    private var observer:DeviceObserver<CMQRoomViewController>?
    var updatingView:UpdatingView!
    override public func viewDidLoad() {
        self.observer = DeviceObserver.init(controller: self)
        self.title = roomContainer.name
        CMQAPIManager.sharedInstance().smartAptUnit?.deviceDelegate = self
        NotificationCenter.default.addObserver(self, selector: #selector(startUpdating), name: CMQRoomViewController.lightUpdating, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(stopUpdating), name: CMQDeviceManipController.DevicesUpdatedNotification, object: nil)
    }
    override public func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
    
    public func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return roomContainer.numberOfCells
    }
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var room = self.roomContainer!
        if let devices = SmartHomeDataSource.shared.cachedDevices[roomContainer.name] as? [CMQSmartDevice]{
            room.devices = devices
        }
        let data = room.deviceGroups[indexPath.row]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: data.collectionCellIdentifier, for: indexPath) as! CMQSmartHomeCell
        cell.data = data
        return cell
    }
}

extension CMQRoomViewController:UICollectionViewDelegateFlowLayout{
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let padding = 10 * 2
        let width = min(self.view.frame.width - CGFloat(padding), 415-CGFloat(padding))
        var room = self.roomContainer!
        if let devices = SmartHomeDataSource.shared.cachedDevices[roomContainer.name] as? [CMQSmartDevice]{
            room.devices = devices
        }
        let data = room.deviceGroups[indexPath.row]
        
        let tableHeight = (100*data.numberOfCells)
        let height = data.groupType == GroupedControlData.GroupType.Fans ? CGFloat(50 + tableHeight) : CGFloat(140 + tableHeight)
        return CGSize.init(width: width, height: height)
    }
}
extension CMQRoomViewController{
    static var lightUpdating:Notification.Name{
        return Notification.Name.init("LightUpdating")
    }
}
extension CMQRoomViewController:SmartUnitDeviceProtocol{
    public func devicesUpdated() {
        try! CMQAPIManager.sharedInstance().smartAptUnit?.fetch()
        self.stopUpdating()
        self.collectionView.reloadData()
    }
}
extension CMQRoomViewController:Updating{
    var bottomView:BottomView{
        let uiView = UIView.init(frame: CGRect.init(origin: .zero, size: CGSize.init(width: self.view.frame.width, height: 30)))
        let label = UILabel.init(frame: uiView.frame)
        label.textColor = .white
        label.textAlignment = .center
        uiView.addSubview(label)
        uiView.addShadow()
        uiView.layer.shadowOffset = CGSize.init(width: 0, height: -2)
        uiView.backgroundColor = UIColor.init(red: 52/255.0, green: 89/255.0, blue: 124/255.0, alpha: 1.0)
        let bottom = BottomView.init(view: uiView, parentView: self.view, updatingLabel: label)
        return bottom
    }
    @objc public func startUpdating() {
        self.updatingView = UpdatingView.init(bottomView: self.bottomView)
        self.updatingView.startUpdating()
        self.enable = false
    }
    @objc public func stopUpdating() {
        self.updatingView?.stopUpdating()
        self.enable = true
        self.collectionView.reloadData()
    }
}
extension CMQRoomViewController:Enable{
    var enable: Bool {
        get {
            return self.collectionView.visibleCells.filter({($0 as! CMQSmartHomeCell).enable == false}).count == 0
        }
        set {
            for cell in self.collectionView.visibleCells as! [CMQSmartHomeCell]{
                cell.enable = newValue
            }
        }
    }
    
    
}


struct BottomView:BottomSlideView & UpdatingLabel{
    var view: UIView
    var parentView: UIView
    var updatingLabel: UILabel
    var updatedText: String{
        return "Updating"
    }
}


struct UpdatingView:Updating{
    var bottomView:BottomSlideView&Updating
    func startUpdating() {
        bottomView.slideUp(duration: 0.3) { (finished) in
            if finished{
                self.bottomView.startUpdating()
            }
        }
    }
    
    func stopUpdating() {
        bottomView.slideDown(duration: 0.3) { (finished) in
            if finished{
                self.bottomView.stopUpdating()
            }
        }
    }
}
