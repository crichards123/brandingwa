//
//  CMQSmartHomeThermostatViewController.swift
//  Communique
//
//  Created by Andre White on 6/9/18.
//  Copyright © 2018 Communique, LLC. All rights reserved.
//

import UIKit

public class CMQSmartHomeThermostatViewController: UIViewController {
    
    @IBOutlet var tempChangeGesture: UIPanGestureRecognizer!
    @IBOutlet weak var thermostatView: ThermostatView!
    @IBOutlet weak var thermostatStateControl: UISegmentedControl!{
        didSet{
            if CMQAPIManager.sharedInstance().smartAptUnit!.thermostatType.contains("Ecobee"){
                thermostatStateControl.insertSegment(withTitle: "Auto", at: 2, animated: false)
            }
        }
    }
    @IBOutlet weak var topLabel: UILabel!{
        didSet{
            topLabel.text = self.updatedText
        }
    }
    public var presetThermostat:SmartDevice = {CMQAPIManager.sharedInstance().smartAptUnit?.thermostat.device}()!
    let twoDigitconstant:CGFloat = -25
    let threeDigitConstat:CGFloat = -10
    var currentMode:Mode = .Device
    public var state: State!
    fileprivate var deviceObserver:DeviceObserver<CMQSmartHomeThermostatViewController>!
    private var recognizingView:UIView!
    private var readyView:UIView!
    private var startPoint:CGPoint!
    private var temperature:GenericBounded<CGFloat>!{
        return self.thermostatView.temperature
    }
    public var thermostat:SmartDevice!
    public override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if currentMode == .Preset{
            let view = self.view as? CMQGradientView
            view?.gradientTopColor = UIColor.gray
            view?.gradientBottomColor = UIColor.green
        }
    }
    override public func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if currentMode == .Preset{
            let view = self.view as? CMQGradientView
            view?.gradientTopColor = UIColor.gray
            view?.gradientBottomColor = UIColor.green
        }
    }
    override public func viewDidLoad() {
        super.viewDidLoad()
        self.updateThermostat()
        if currentMode == .Device{
            CMQAPIManager.sharedInstance().smartAptUnit?.deviceDelegate = self
        }else{
            
        }
        self.deviceObserver = DeviceObserver.init(controller: self)
        
    }
    
    public func updateThermostat(){
        switch self.currentMode{
        case .Device:
            if let therm = SmartHomeDataSource.shared.cachedDevices["therm"] as? SmartDevice{
                self.thermostat = therm
            }else{
                let _ = try? CMQAPIManager.sharedInstance().smartAptUnit?.fetch()
                self.thermostat = CMQAPIManager.sharedInstance().smartAptUnit?.thermostat.device
            }
        case .Preset:
            break
        }
        willChangeState(to: .Ready(therm: self.thermostat))
       
    }
    
    // MARK: - IBActions
    @IBAction func thermostatStateChanged(_ sender: UISegmentedControl) {
        guard sender.selectedSegmentIndex != self.thermostat.controlIndex else {return}
        var newTherm:SmartDevice = self.thermostat
        newTherm.controlIndex = sender.selectedSegmentIndex
        willChangeState(to: .Changing(therm: newTherm, type:.Mode))
        
    }
    
    @IBAction func didPan(_ sender: UIPanGestureRecognizer) {
        if case .Changing(_,_) = state!{
            return
        }else{
            switch sender.state {
            case .began:
                self.willChangeState(to: .Recognizing)
            case .changed:
                guard self.state == .Recognizing else {return}
                let locationInView = sender.location(in: self.view)
                self.thermostatView.tempShouldChange(sender: sender, height:self.view.frame.height)
                self.thermostatView.tempShouldChange(locationInView: locationInView, viewHeight:self.view.frame.height)
            case .ended:
                var newTherm = self.thermostat!
                let tempInt = Int(temperature.value)
                newTherm.setToTemp = String.init(describing: tempInt)
                willChangeState(to: .Changing(therm: newTherm, type: .Temp))
            default:
                return
            }
        }
    }
    //MARK: - View/Model Sync
    fileprivate func updateUI(){
        self.thermostatView.updateUI(for: self.state)
    }

}
extension CMQSmartHomeThermostatViewController:FiniteStateMachine{
    public func willChangeState(to: State) {
        //guard self.state != to else {return}
        switch to {
        case .Ready(let therm):
            self.state = to
            self.thermostat = therm
            self.thermostatView.thermostat = self.thermostat
            updateUI()
            
        case .Recognizing:
            switch self.thermostat.currentMode!{
            case .Off(_,_,_), .Auto(_, _,_):
                willChangeState(to: .Ready(therm: self.thermostat))
            default:
                state = to
            }
            updateUI()
        case .Changing(let therm, let type):
            guard therm != self.thermostat else {
                state = .Ready(therm:self.thermostat)
                return
            }
            //Commit change
            self.state = to
            self.updateUI()
            switch type{
            case .Mode:
                break
            case .Temp:
                break
            }
            switch self.currentMode{
            case .Device:
                self.startUpdating()
                self.enable = false
                self.performCommand(device:therm, type: type) { (error) in
                    self.stopUpdating()
                    self.enable = true
                    if let _ = error{
                        self.state = .Ready(therm:self.thermostat)
                    }else{
                        SmartHomeDataSource.shared.cachedDevices["therm"] = therm
                        self.willChangeState(to: .Ready(therm: therm))
                        let updater = DeviceUpdater()
                        SmartHomeDataSource.shared.updaters.append(updater)
                        updater.commandSent()
                        
                    }
                }
            case .Preset:
                presetThermostat = therm
                willChangeState(to: .Ready(therm: therm))
            }
        }
    }
    public typealias State = ThermostatControllerState
    
    public enum ThermostatControllerState:Equatable{
        case Ready(therm:SmartDevice)
        case Recognizing
        case Changing(therm:SmartDevice, type:CommandType)
        
        public static func == (lhs: CMQSmartHomeThermostatViewController.ThermostatControllerState, rhs: CMQSmartHomeThermostatViewController.ThermostatControllerState) -> Bool {
            switch (lhs,rhs) {
            case let (.Ready(therm1),.Ready(therm2)):
                return therm1 == therm2
            case (.Recognizing, .Recognizing):
                return true
            case let (.Changing(therm1), .Changing(therm2)):
                return therm1 == therm2
            default:
                return false
            }
        }
    }
}
extension CMQSmartHomeThermostatViewController{
    public func removeRecognizers(){
        
    }
    public func addRecognizers(){
        
    }
}
extension CMQSmartHomeThermostatViewController{
    enum Mode{
        case Preset
        case Device
    }
}

extension CMQSmartHomeThermostatViewController:CommandIssueProtocol{
    public enum CommandType:String{
        case Mode = "changeMode"
        case Temp = "setTemp"
        
    }
    func performCommand(device:SmartDevice, type:CommandType, completion:@escaping CommandCompletion){
        guard let smApt = CMQAPIManager.sharedInstance().smartAptUnit else {return}
        var aDevice = smApt.thermostat!
        aDevice.device = device
        let command = Command.init(devices: [aDevice], command: type.rawValue)
        command.execute(completion: completion)
        
    }
}
extension CMQSmartHomeThermostatViewController:SmartUnitDeviceProtocol{
    public func devicesUpdated() {
        self.stopUpdating()
        self.updateThermostat()
        self.enable = true
    }
}
extension CMQSmartHomeThermostatViewController:Enable{
    var enable: Bool {
        get {
            guard self.thermostatStateControl.isEnabled else {return false}
            return self.tempChangeGesture.isEnabled
        }
        set {
            self.tempChangeGesture?.isEnabled = newValue
            self.thermostatStateControl?.isEnabled = newValue
        }
    }
}
extension CMQSmartHomeThermostatViewController:UpdatingLabel{
    public var updatingLabel: UILabel {
        return self.topLabel
    }
    public var updatedText: String {
        return "Thermostat"
    }
    
    
}

extension CMQSmartHomeThermostatViewController:DeviceController{
    var smartDevice: SmartDevice {
        get {
            return self.thermostat
        }
        set {
            self.thermostat = newValue
            willChangeState(to: .Ready(therm: self.thermostat))
        }
    }
}
