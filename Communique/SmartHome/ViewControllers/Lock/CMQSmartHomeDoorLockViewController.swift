//
//  CMQSmartHomeDoorLockViewController.swift
//  Communique
//
//  Created by Andre White on 6/9/18.
//  Copyright © 2018 Communique, LLC. All rights reserved.
//

import UIKit

extension CMQSmartHomeDoorLockViewController.LockControllerState:Equatable{
    public static func == (lhs: CMQSmartHomeDoorLockViewController.LockControllerState, rhs: CMQSmartHomeDoorLockViewController.LockControllerState) -> Bool {
        switch (lhs,rhs) {
        case let (.Ready(lock1), .Ready(lock2)), let (.Changing(lock1), .Changing(lock2)):
            return lock1 == lock2
        case (.Recognizing, .Recognizing):
            return true
        default:
            return false
        }
            
    }
    
}
public class CMQSmartHomeDoorLockViewController: UIViewController, CAAnimationDelegate, FiniteStateMachine {
    public var presetLock:SmartDevice = {CMQAPIManager.sharedInstance().smartAptUnit?.doorLock.device}()!
    public var state: State!
    var currentMode:Mode = .Device
        /*get{
            let lock = CMQAPIManager.sharedInstance().smartAptUnit?.doorLock
            return lock?.device == self.lock ? .Device : .Preset
        }*/
        
    public var lock:SmartDevice!{
        didSet{
            guard case .Lock(let value) = lock! else {return}
            self.updateLockColor(mode:value)
            lockView?.layer.borderColor = UIColor.white.cgColor
            self.updateLockImage(mode:value)
        }
    }
    public func willChangeState(to: State) {
        //guard self.state != to else {return}
        switch to {
        
        case .Ready(let lock):
            self.lock = lock
            self.state = to
            updateUI()
        case .Recognizing:
            self.state = to
            updateUI()
        case .Changing(let lock):
            guard lock != self.lock else {state = .Ready(lock: self.lock); return}
            //Commit change
            //After Changes successful
            //willChangetoState:.Ready(lock)
            //After Changes failed
            switch self.currentMode{
            case .Device:
                startUpdating()
                self.enable = false
                self.state = to
                updateUI()
                self.performCommand(lock: lock) { (error) in
                    self.stopUpdating()
                    self.enable = true
                    if let _ = error{
                        self.state = .Ready(lock:self.lock)
                    }else{
                        SmartHomeDataSource.shared.cachedDevices["lock"] = lock
                        self.willChangeState(to: .Ready(lock: lock))
                        let updater = DeviceUpdater()
                        SmartHomeDataSource.shared.updaters.append(updater)
                        updater.commandSent()
                    }
                }
            case .Preset:
                //self.lock = lock
                willChangeState(to: .Ready(lock: lock))
            }
            
        }
    }
    
    public typealias State = LockControllerState
    
    public enum LockControllerState{
        case Ready(lock:SmartDevice)
        case Recognizing
        case Changing(lock:SmartDevice)
        
    }
    
    @IBOutlet weak var lockView: UIView!{
        didSet{
            guard let lock = self.lock else {return}
            guard case .Lock(let value) = lock else {return}
            self.updateLockColor(mode:value)
            lockView.layer.borderColor = UIColor.white.cgColor
        }
    }
    @IBOutlet weak var lockImageView:UIImageView!{
        didSet{
            guard let lock = self.lock else {return}
            guard case .Lock(let value) = lock else {return}
            self.updateLockImage(mode:value)
        }
    }
    
    @IBOutlet weak var transparentView: UIView!
    
    @IBOutlet var lockGesture: UILongPressGestureRecognizer!
    @IBOutlet var tapLockGesture:UITapGestureRecognizer!
    private var circleLayer:CAShapeLayer!
    fileprivate var deviceObserver:DeviceObserver<CMQSmartHomeDoorLockViewController>!
    @IBOutlet weak var topLabel: UILabel!{
        didSet{
            topLabel.text = self.updatedText
        }
    }
    override public func viewDidLoad() {
        super.viewDidLoad()
        self.updateLock()
        if self.currentMode == .Device{
            CMQAPIManager.sharedInstance().smartAptUnit?.deviceDelegate = self
        }else{
            //(self.view as! CMQGradientView).gradientTopColor = UIColor.CMQSmartHomeGradientTopColor
            //(self.view as! CMQGradientView).gradientBottomColor = UIColor.CMQSmartHomeGradientBottomColor
        }
        self.deviceObserver = DeviceObserver.init(controller: self)
        handleGestures()
    }
    private func handleGestures(){
        switch currentMode {
        case .Device:
            self.lockGesture.isEnabled = true
            self.tapLockGesture.isEnabled = false
        default:
            self.tapLockGesture.isEnabled = true
            self.lockGesture.isEnabled = false
        }
    }
    override public func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        drawCircle()
    }
    
    @IBAction func lockTapped(_ sender: UILongPressGestureRecognizer) {
        guard currentMode == .Device else {return}
        switch sender.state {
        case .began:
            willChangeState(to: .Recognizing)
        case .ended:
            willChangeState(to: .Ready(lock: self.lock))
        default:
            return
        }
    }
    @IBAction func presetLockTapped(_ sender:UITapGestureRecognizer){
        guard self.currentMode == .Preset else {return}
        willChangeState(to: .Changing(lock: .Lock(state: !self.lock.lockMode!)))
    }
    public func animationDidStop(_ anim: CAAnimation, finished flag: Bool) {
        if flag{
            guard case .Lock(let value) = self.lock! else {return}
            willChangeState(to: .Changing(lock: .Lock(state: !value)))
        }
    }
    public func updateUI(){
        switch state! {
        case .Ready(_):
            if case .Lock(let value) = self.lock!{
                updateLockColor(mode: value)
                updateLockImage(mode: value)
            }
            cleanUpAnimation()
        case .Recognizing:
            animateCircle()
        case .Changing(_):
            //Update UI to show its changing
            cleanUpAnimation()
            print("Currently Changing")
        }
    }
    public func updateLock(){
        switch self.currentMode{
        case .Device:
            if let lock = SmartHomeDataSource.shared.cachedDevices["lock"] as? SmartDevice{
                self.willChangeState(to: .Ready(lock: lock))
            }else{
                guard let smApt = CMQAPIManager.sharedInstance().smartAptUnit else {return}
                try! smApt.fetch()
                guard let device = smApt.doorLock else {return}
                
                self.willChangeState(to: .Ready(lock: device.device))
            }
        case .Preset:
            self.state = .Ready(lock: self.lock)
            updateUI()
            break
            //self.willChangeState(to: .Ready(lock: self.lock))
        }
        
    }
    private func toggleLock(){
        if case .Lock(let value) = self.lock!{
            self.lock = .Lock(state:!value)
            updateLockColor(mode: !value)
            updateLockImage(mode: !value)
        }
        
    }
    private func updateLabel(){
        topLabel.text = updatedText
        topLabel.alpha = 1.0
    }
    private func updateLockColor(mode:SmartDevice.LockMode){
        lockView?.backgroundColor = mode.color
    }
    private func updateLockImage(mode:SmartDevice.LockMode){
        lockImageView?.image = mode.image
    }
    
    private func cleanUpAnimation(){
        guard self.currentMode == .Device else {return}
        self.circleLayer?.strokeEnd = 0.0
        self.circleLayer?.removeAnimation(forKey: "animateCircle")
    }
    private func drawCircle(){
        circleLayer = CAShapeLayer()
        let circlePath = UIBezierPath.init(arcCenter: self.transparentView.center, radius: self.transparentView.frame.width/2, startAngle: CGFloat(-Double.pi/2), endAngle: CGFloat(7.0*Double.pi/4), clockwise: true)
        circleLayer.path = circlePath.cgPath
        circleLayer.fillColor = nil
        circleLayer.strokeColor = UIColor.white.cgColor
        circleLayer.lineWidth = 10.0
        circleLayer.strokeEnd = 0.0
        
        self.transparentView.layer.addSublayer(circleLayer)
        
    }
    
    private func animateCircle(){
        let animation = CABasicAnimation(keyPath: "strokeEnd")
        
        // Set the animation duration appropriately
        animation.duration = 1.5
        
        // Animate from 0 (no circle) to 1 (full circle)
        animation.fromValue = 0
        animation.toValue = 0.90
        animation.delegate = self
        
        // Do a linear animation (i.e. the speed of the animation stays the same)
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
        
        // Set the circleLayer's strokeEnd property to 1.0 now so that it's the
        // right value when the animation ends.
        circleLayer.strokeEnd = 1.0
        
        // Do the actual animation
        circleLayer.add(animation, forKey: "animateCircle")
    }
}



extension SmartDevice.LockMode{
    
    static prefix func !(lockMode:SmartDevice.LockMode)->SmartDevice.LockMode{
        switch lockMode {
        case .unlocked:
            return .locked
        case .locked:
            return .unlocked
        }
    }
    private var LockedColor:UIColor{
        return UIColor.init(red: 171/255.0, green: 36/255.0, blue: 26/255.0, alpha: 1.0)
    }
    private var UnlockedColor:UIColor{
        return UIColor.init(red: 69/255.0, green: 156/255.0, blue: 82/255.0, alpha: 1.0)
    }
    var color:UIColor{
        switch self {
        case .locked:
            return LockedColor
        case .unlocked:
            return UnlockedColor
        }
    }
    var image:UIImage!{
        switch self {
        case .locked:
            return UIImage.init(named: "Locked Icon", in: CMQBundle, compatibleWith: nil)
        case .unlocked:
            return UIImage.init(named: "Unlock Icon", in: CMQBundle, compatibleWith: nil)
            
        }
    }
}
extension CMQSmartHomeDoorLockViewController:CommandIssueProtocol{
    
    func performCommand(lock:SmartDevice, completion:@escaping CommandCompletion){
        guard let smApt = CMQAPIManager.sharedInstance().smartAptUnit else {return}
        var device = smApt.doorLock!
        device.device = lock
        issueCommand(devices: [device], completion: completion)
    }
}
extension CMQSmartHomeDoorLockViewController:SmartUnitDeviceProtocol{
    public func devicesUpdated() {
        self.stopUpdating()
        self.updateLock()
        self.enable = true
    }
}
extension CMQSmartHomeDoorLockViewController:UpdatingLabel{
    public var updatingLabel: UILabel {
        return self.topLabel
    }
    
    public var updatedText: String {
        return "Front Door"
    }
}
extension CMQSmartHomeDoorLockViewController:Enable{
    var enable: Bool {
        get {
            return self.lockGesture.isEnabled
        }
        set {
            self.lockGesture.isEnabled = newValue
        }
    }
}
extension CMQSmartHomeDoorLockViewController{
    enum Mode{
        case Preset
        case Device
    }
    
}
public class LockView:UIView{
    @IBOutlet public var transparentView:UIView!
    @IBOutlet public var lockImageView:UIImageView!
    public var lock:SmartDevice!{
        didSet{
            self.update(with: lock)
        }
    }
    private func update(with lock:SmartDevice){
        self.updateLockColor(mode: lock.lockMode!)
        self.updateLockImage(mode: lock.lockMode!)
    }
    private func updateLockColor(mode:SmartDevice.LockMode){
        self.backgroundColor = mode.color
    }
    private func updateLockImage(mode:SmartDevice.LockMode){
        lockImageView?.image = mode.image
    }
    override public func layoutSubviews() {
        super.layoutSubviews()
        self.layer.cornerRadius = self.frame.width/2
        self.layer.borderWidth = self.frame.width > 125 ? 7 : 2
        self.layer.borderColor = UIColor.white.cgColor
    }
    public override func awakeFromNib() {
        //update(with: lock)
    }
}
extension CMQSmartHomeDoorLockViewController:DeviceController{
    var smartDevice: SmartDevice {
        get {
            return self.lock
        }
        set {
            self.lock = newValue
            self.willChangeState(to: .Ready(lock: self.lock))
        }
    }
}
