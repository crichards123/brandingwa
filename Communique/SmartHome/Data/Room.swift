//
//  Room.swift
//  Communique
//
//  Created by Andre White on 7/16/18.
//  Copyright © 2018 Communique, LLC. All rights reserved.
//

import Foundation

public struct Room:DeviceGroup, Named, Hashable,Identifier {
    var identifier: String
    public var hashValue: Int{
        return name.hashValue
    }
    var name: String
    var devices: [CMQSmartDevice]
    enum CodingKeys:String, CodingKey{
        case devices
        case name = "roomName"
        case identifier = "roomId"
    }
}
extension Room{
    var icon:UIImage{
        return UIImage.init(named: "Room Icon", in: CMQBundle, compatibleWith: nil)!
    }
}
extension Room:Decodable{
    public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        name = try values.decode(String.self, forKey: .name)
        identifier = try values.decode(String.self, forKey: .identifier)
        devices = try values.decode([CMQSmartDevice].self, forKey: .devices)
    }
    
}
extension Room:CollectionViewData{
    public var numberOfCells: Int {
        return deviceGroups.count
    }
    var deviceGroups:[GroupedControlData]{
        return GroupedControlData.GroupType.AllCases.map({GroupedControlData.init(groupType: $0, devices: self.devices)}).filter({$0.shouldInclude})
    }
    public var collectionCellIdentifier: String {
        return "DeviceGroup"
    }
    
    public func data(at indexPath: IndexPath) -> CMQSmartDevice? {
        guard indexPath.row < devices.count else {return nil}
        return devices.sorted(by: {$0.name>$1.name})[indexPath.row]
    }
}
public struct GroupedControlData:CollectionViewData{
    public var collectionCellIdentifier: String{
        return "DeviceGroup"
    }
    public var numberOfCells: Int{
        return self.data.count
    }
    
    public func data(at indexPath: IndexPath) -> CMQSmartDevice? {
        return self.data.sorted(by: {$0.name>$1.name})[indexPath.row]
    }
    
    public enum GroupType:String{
        case Lights
        case Fans
        case Outlets
        static var AllCases:[GroupType]{
            return [.Lights, .Fans, .Outlets]
        }
        //Only returns Light Images
        var highlightedImage:UIImage{
            switch self{
                
            case .Lights:
                return UIImage.init(named: "All Lights - on", in: CMQBundle, compatibleWith: nil)!
            case .Fans:
                return UIImage.init(named: "Fan Icon", in: CMQBundle, compatibleWith: nil)!
            case .Outlets:
                return UIImage.init(named: "All Outlets - on", in: CMQBundle, compatibleWith: nil)!
            }
            
        }
        var image:UIImage{
            switch self{
                
            case .Lights:
                return UIImage.init(named: "All Lights - off", in: CMQBundle, compatibleWith: nil)!
            case .Fans:
                return UIImage.init(named: "Fan Icon - off", in: CMQBundle, compatibleWith: nil)!
            case .Outlets:
                return UIImage.init(named: "All Outlets - off", in: CMQBundle, compatibleWith: nil)!
            }
            
        }
    }
    public var data:[CMQSmartDevice]{
        switch self.groupType {
        case .Lights:
            return devices.filter({if case .Light(_) = $0.device{return true};return false})
        case .Fans:
            return devices.filter({if case .Fan(_) = $0.device{return true};return false})
        case .Outlets:
            return devices.filter({if case .Outlet(_) = $0.device{return true};return false})
        }
    }
    public var groupName:String{
        return self.groupType.rawValue
    }
    public var controlName:String{
        return "All \(groupName)"
    }
    public var controlImage:(UIImage,UIImage){
        return (self.groupType.image,self.groupType.highlightedImage)
    }
    public var groupType:GroupType
    var devices:[CMQSmartDevice]
    var shouldInclude:Bool{
        return data.count != 0
    }
}
