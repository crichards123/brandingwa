//
//  Command.swift
//  Communique
//
//  Created by Andre White on 7/16/18.
//  Copyright © 2018 Communique, LLC. All rights reserved.
//

import Foundation

public struct Command:Hashable, Decodable{
    fileprivate var garbTemp:String = {
        return CMQAPIManager.sharedInstance().smartAptUnit?.currentTemp ?? ""
    }()
    public var hashValue: Int{
        return command.hashValue
    }
    
    
    var devices:[CMQSmartDevice]
    var provider:String {
        return devices.first!.device.providerString
    }
    var command:String
    var state:State
    enum CodingKeys:CodingKey{
        case deviceId
        case deviceName
        case command
        case state
    }
    public init(devices:[CMQSmartDevice],command:String = ""){
        self.devices = devices
        self.command = command == "" ? self.devices.first!.device.commandString : command
        self.state = State.init(mode: .off)
    }
    public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        let deviceIDs = try values.decode([String].self, forKey: .deviceId)
        self.devices = [CMQSmartDevice]()
        command = try values.decode(String.self, forKey: .command)
        state = try values.decode(State.self, forKey: .state)
        
        if command == "setLightLevel"{
            for deviceID in deviceIDs{
                if var device = CMQSmartDevice.device(with: deviceID){
                    device.device = .Light(level: Float(state.arguments))
                    self.devices.append(device)
                }
            }
        }else if command == "setFanLevel"{
            for deviceID in deviceIDs{
                if var device = CMQSmartDevice.device(with: deviceID){
                    device.device = .Fan(value: Int(state.arguments))
                    self.devices.append(device)
                }
            }
            
        }else if command == "setSocketMode"{
            for deviceID in deviceIDs{
                if var device = CMQSmartDevice.device(with: deviceID){
                    device.device = state.mode == .off ? .Outlet(value:false) : .Outlet(value: true)
                    self.devices.append(device)
                }
            }
        }else{
            switch state.mode {
                
            case .off:
                for deviceID in deviceIDs{
                    if var device = CMQSmartDevice.device(with: deviceID){
                        switch device.type{
                        case .Thermostat:
                            device.device = .Thermostat(value: .Off(curTemp: garbTemp, heat: garbTemp, cool: garbTemp))
                        case .Outlet:
                            device.device = .Outlet(value: false)
                        default:
                            return
                        }
                        self.devices.append(device)
                    }
                }
            case .heat:
                for deviceID in deviceIDs{
                    if var device = CMQSmartDevice.device(with: deviceID){
                        switch command{
                        case "setTemp":
                            device.device = .Thermostat(value: .Heat(curTemp: garbTemp, heat: "\(state.arguments)", cool: garbTemp))
                        default:
                            //Change mode
                            let heat = device.device.currentMode.heatTemp
                            device.device = .Thermostat(value: .Heat(curTemp: garbTemp, heat: heat, cool: garbTemp))
                        }
                        self.devices.append(device)
                    }
                    
                }
            case .cool:
                for deviceID in deviceIDs{
                    if var device = CMQSmartDevice.device(with: deviceID){
                        switch command{
                        case "setTemp":
                            device.device = .Thermostat(value: .Cool(curTemp: garbTemp, heat: garbTemp, cool: "\(state.arguments)"))
                        default:
                            //Change mode
                            let cool = device.device.currentMode.coolTemp
                            device.device = .Thermostat(value: .Cool(curTemp: garbTemp, heat: garbTemp, cool: cool))
                        }
                        self.devices.append(device)
                    }
                    
                }
            case .locked:
                for deviceID in deviceIDs{
                    if var device = CMQSmartDevice.device(with: deviceID){
                        device.device = .Lock(state: .locked)
                        self.devices.append(device)
                    }
                    
                }
            case .unlocked:
                for deviceID in deviceIDs{
                    if var device = CMQSmartDevice.device(with: deviceID){
                        device.device = .Lock(state: .unlocked)
                        self.devices.append(device)
                    }
                }
            case .on:
                for deviceID in deviceIDs{
                    if var device = CMQSmartDevice.device(with: deviceID){
                        device.device = .Outlet(value: true)
                        self.devices.append(device)
                    }
                }
            }
        }
    }
    struct State: Codable,Hashable{
        var mode:Mode
        var arguments:Int
        public func encode()->[String:Any]{
            var returnable = [String:Any]()
            returnable["state"] = ["arguments":self.arguments, "mode":self.mode.rawValue]
            return returnable
        }
        init(mode:Mode, arguments:Int = 0) {
            self.mode = mode
            self.arguments = arguments
        }
        enum CodingKeys:CodingKey{
            case mode
            case arguments
        }
        public func encode(to encoder:Encoder) throws{
            var container = encoder.container(keyedBy: CodingKeys.self)
            if self.mode != .locked || self.mode != .unlocked{
                try? container.encode(self.arguments, forKey: .arguments)
            }
            try? container.encode(self.mode, forKey: .mode)
        }
        public init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            if let mode = try? values.decode(Mode.self, forKey: .mode){
                self.mode = mode
            }else{
                mode = .off
            }
            if let args = try? values.decode(Int.self, forKey: .arguments){
                self.arguments = args
            }else{
                self.arguments = 0
            }
        }
    }
    public enum Mode:String, Codable, Hashable{
        case off
        case heat
        case cool
        case locked
        case unlocked
        case on
        
    }
}
extension Command:Encodable{
    private enum EncodingKeys:CodingKey{
        case deviceType
        case roomId
        case deviceId
        case provider
        case command
        case deviceName
        case state
    }
    public func encode()->[String:Any]{
        var returnable = [String:Any]()
        returnable["deviceType"] = self.devices.first!.device.deviceTypeString
        if !self.isRoomCommand{
            returnable["roomId"] = ""
            returnable["roomName"] = ""
        }else{
            let room = CMQAPIManager.sharedInstance().smartAptUnit?.userRooms.filter({devices.first!.belongs(to: $0)}).first!
            returnable["roomId"] = room!.identifier
            returnable["roomName"] = room!.name
            returnable["allLights"] = false
        }
        returnable["deviceName"] = self.devices.compactMap({$0.name})
        returnable["deviceId"] = self.devices.compactMap({$0.identifier})
        returnable["provider"] = self.provider
        returnable["command"] = self.command
        switch command{
        case "setTemp":
            let mode = Mode.init(rawValue: self.devices.first!.device.currentMode.modeText)!
            let args = Int(self.devices.first!.device.setToTemp)!
            let state = State.init(mode: mode, arguments: args)
            returnable["state"] = state.encode()["state"]
            break
        case "setSocketMode":
            let mode:Mode = Mode.init(rawValue: self.devices.first!.device.On ? "on" : "off")!
            var state = State.init(mode: mode).encode()["state"] as! [String:Any]
            state.removeValue(forKey: "arguments")
            returnable["state"] = state
        case "changeMode":
            let mode:Mode = Mode.init(rawValue: devices.first!.device.currentMode.modeText)!
            var state = State.init(mode: mode).encode()["state"] as! [String:Any]
            state.removeValue(forKey: "arguments")
            returnable["state"] = state
            break
        case "setLightLevel":
            let dict = ["arguments":Int(self.devices.first!.device.level)]
            returnable["state"] = dict
        case "setFanLevel":
            let dict = ["arguments":Int(self.devices.first!.device.fanLevel)]
            returnable["state"] = dict
        case "lock":
            let unlocked = self.devices.first!.device.isUnlocked!
            let text = unlocked ? "unlocked":"locked"
            let mode = Mode.init(rawValue: text)!
            let state = State.init(mode: mode)
            var encodedState = state.encode()["state"] as! [String:Any]
            encodedState.removeValue(forKey: "arguments")
            returnable["state"] = encodedState
        default:
            return returnable
            
        }
        
        
        return returnable
    }
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: EncodingKeys.self)
        try container.encode(self.devices.first!.device.deviceTypeString, forKey: .deviceType)
        if !isRoomCommand{
            try container.encode("", forKey: .roomId)
        }else{
            let room = CMQAPIManager.sharedInstance().smartAptUnit?.userRooms.filter({devices.first!.belongs(to: $0)}).first!
            try container.encode(room?.identifier, forKey: .roomId)
        }
        try container.encode(self.devices.compactMap({$0.name}), forKey:.deviceName)
        try container.encode(self.devices.compactMap({$0.identifier}), forKey: .deviceId)
        try container.encode(self.provider, forKey: .provider)
        try container.encode(self.command, forKey: .command)

        switch command{
        case "setTemp":
            let mode = Mode.init(rawValue: self.devices.first!.device.currentMode.modeText)!
            let args = Int(self.devices.first!.device.setToTemp)!
            let state = State.init(mode: mode, arguments: args)
            try container.encode(state, forKey: EncodingKeys.state)
            break
        case "changeMode":
            let mode = Mode.init(rawValue: devices.first!.device.currentMode.modeText)!
            let state = State.init(mode: mode)
            try container.encode(state, forKey: .state)
            break
        case "setLightLevel":
            let dict = ["arguments":Int(self.devices.first!.device.level)]
            try container.encode(dict, forKey: .state)
        case "lock":
            let unlocked = self.devices.first!.device.isUnlocked!
            let text = unlocked ? "unlocked":"locked"
            let mode = Mode.init(rawValue: text)!
            let state = State.init(mode: mode)
            try container.encode(state, forKey: .state)
        default:
            return
            
        }
    }
}
extension Command:CollectionViewData{
    public var collectionCellIdentifier: String {
        return self.devices.first?.collectionCellIdentifier ?? "Reuse"
    }
    
    public var numberOfCells: Int {
        return 1
    }
    
    public func data(at indexPath: IndexPath) -> CMQSmartDevice? {
        guard indexPath.row<self.devices.endIndex else {return nil}
        return self.devices[indexPath.row]
    }
}
extension Command{
    var deviceType:String{
        return self.devices.first!.device.deviceTypeString
    }
    var isRoomCommand:Bool{
        return ["lightGroup","socket","fan"].contains(self.deviceType)
    }
}
extension Command{
    /*func room(with other:Command)->Room?{
        guard other.deviceType == self.deviceType else {return nil}
        guard other.isRoomCommand && self.isRoomCommand else {return nil}
        
    }*/
    func device(with other:Command)->CMQSmartDevice?{
        guard other.deviceType == self.deviceType else {return nil}
        guard other.devices.count ==  self.devices.count else {return nil}
        //Should be used with Thermostats since there are two different commands
        switch self.command {
        case "setTemp":
            if other.command == "changeMode"{
                var aDevice = other.devices.first!
                aDevice.device.setToTemp = self.devices.first!.device.setToTemp
                return aDevice
            }
            return nil
        case "changeMode":
            if other.command == "setTemp"{
                var aDevice = self.devices.first!
                aDevice.device.setToTemp = other.devices.first!.device.setToTemp
                return aDevice
            }
            return nil
        default:
            return nil
        }
    }
}
extension Sequence where Self.Element == Command{
    var collectionViewData:[CollectionViewData]{
        var returnable = [CollectionViewData]()
        if let therm = self.getThermostat(){
            returnable.append(therm)
        }
        if let lock = self.getLock(){
            returnable.append(lock)
        }
        if let smApt = CMQAPIManager.sharedInstance().smartAptUnit{
            for room in smApt.userRooms{
                if var newRoom = self.getRoom(room: room){
                    returnable.append(newRoom)
                }
            }
        }
        return returnable
    }
    func getThermostat()->CMQSmartDevice?{
        let therm = self.filter({$0.deviceType=="thermostat"})
        switch therm.count {
        case 1:
            return therm.first!.devices.first
        case 2:
            return therm.first!.device(with: therm.last!)
        default:
            return nil
        }
    }
    func getRoom(room:Room)->Room?{
        var devices = Set<CMQSmartDevice>()
        let commandsWithRoom = self.filter({$0.devices.first!.belongs(to: room)})
        for command in commandsWithRoom{
            devices.formUnion(command.devices.filter({$0.type==CMQSmartDevice.DeviceType.Light}))
            devices.formUnion(command.devices.filter({$0.type==CMQSmartDevice.DeviceType.Fan}))
            devices.formUnion(command.devices.filter({$0.type==CMQSmartDevice.DeviceType.Outlet}))
        }
        var newRoom = room
        newRoom.devices = devices.map({$0})
        return newRoom.devices.count == 0 ? nil : newRoom
    }
    func getLock()->CMQSmartDevice?{
        guard let lock = self.filter({$0.deviceType=="lock"}).first else {return nil}
        return lock.devices.first
        
    }
}
///Struct that updates the SmartApartmentUnit object and posts notification informing observers of that with the updated smApt object.
struct DeviceUpdater{
    static let UpdatedNotification = Notification.Name("DevicesUpdated")
    func commandSent(){
        DispatchQueue.main.asyncAfter(deadline: .now()+5) {
            CMQAPIManager.sharedInstance().smartAptUnit?.fetchInBackground(block: { (apartment, error) in
                if let smApt = apartment as? CMQSmartAptUnit{
                    SmartHomeDataSource.shared.cachedDevices.removeAll()
                    NotificationCenter.default.post(name: DeviceUpdater.UpdatedNotification, object: smApt)
                }
            })
        }
    }
}
protocol DeviceController{
    var smartDevice:SmartDevice {get set}
}
///Generic Class that observes the DeviceUpdaters UpdatedNotification. This class handles the addition and removal of the observers and handles the notification after it is received. It should be instatiated with an instance that conforms to the DeviceController protocol in order to set the device after it has been updated. handle(notification:) should be implemented via an extension or subclass.
class DeviceObserver<T:DeviceController>:Observer{
    
    
    var controller:T
    init(controller:T) {
        self.controller = controller
        self.addObservers()
    }
    @objc func notification(not:Notification){
        switch controller{
        case is CMQSmartHomeDoorLockViewController:
            let obs = self as! DeviceObserver<CMQSmartHomeDoorLockViewController>
            obs.handleLock(notification: not)
        case is CMQSmartHomeThermostatViewController:
            let obs = self as! DeviceObserver<CMQSmartHomeThermostatViewController>
            obs.handleTherm(notification: not)
        case is CMQRoomViewController:
            let obs = self as! DeviceObserver<CMQRoomViewController>
            obs.handleRoom(notification: not)
        case is CMQRoomSelectViewController:
            let obs = self as! DeviceObserver<CMQRoomSelectViewController>
            obs.handleRoomSelection(notification: not)
        default:
            self.handle(notification: not)
        }
    }
    public func handle(notification: Notification) {}
    public func addObservers(){
        NotificationCenter.default.addObserver(self, selector: #selector(notification(not:)), name: DeviceUpdater.UpdatedNotification, object: nil)
    }
    public func removeObservers(){
        NotificationCenter.default.removeObserver(self, name: DeviceUpdater.UpdatedNotification, object: nil)
    }
    deinit {
        removeObservers()
    }
}
extension DeviceObserver where T == CMQSmartHomeThermostatViewController{
     func handleTherm(notification: Notification) {
        guard self.controller.currentMode == .Device else {return}
        guard let smApt = notification.object as? CMQSmartAptUnit else {return}
        guard let therm = smApt.thermostat else {return}
        self.controller.smartDevice = therm.device
    }
}
extension DeviceObserver where T == CMQSmartHomeDoorLockViewController{
    func handleLock(notification:Notification){
        guard self.controller.currentMode == .Device else {return}
        guard let smApt = notification.object as? CMQSmartAptUnit else {return}
        guard let lock = smApt.doorLock else {return}
        self.controller.smartDevice = lock.device
    }
}
extension DeviceObserver where T == CMQRoomViewController{
    func handleRoom(notification:Notification){
        guard let _ = notification.object as? CMQSmartAptUnit else {return}
        self.controller.collectionView.reloadData()
    }
}
extension DeviceObserver where T == CMQRoomSelectViewController{
    func handleRoomSelection(notification:Notification){
        guard let _ = notification.object as? CMQSmartAptUnit else {return}
        self.controller.collectionView.reloadData()
    }
}
extension CMQRoomSelectViewController:DeviceController{
    var smartDevice: SmartDevice {
        get {
            return .Light(level:0)
        }
        set {
            
        }
    }
    
    
}
extension CMQRoomViewController:DeviceController{
    var smartDevice: SmartDevice {
        get {
            return .Light(level: 0)
        }
        set {
            
        }
    }
    
    
}
