//
//  Preset.swift
//  Communique
//
//  Created by Andre White on 7/22/18.
//  Copyright © 2018 Communique, LLC. All rights reserved.
//

import Foundation

public struct Preset:Named, Hashable, Identifier {
    var identifier: String
    public var hashValue: Int{
        return name.hashValue
    }
    var commands:[Command]
    var name: String
    enum CodingKeys:String, CodingKey{
        case name = "presetName"
        case identifier = "presetId"
        case commands
        case rooms
    }
    init(name:String, identifier:String, commands:[Command], rooms:[Room] = [Room]()){
        self.name = name
        self.commands = commands
        self.identifier = identifier
    }
}
extension Preset:Decodable{
    public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        commands = [Command]()
        let nonRoomCommands = try values.decode([Command].self, forKey: .commands)
        commands.append(contentsOf: nonRoomCommands)
        let roomCommands = try values.decode([Command].self, forKey: .rooms)
        commands.append(contentsOf: roomCommands)
        name = try values.decode(String.self, forKey: .name)
        identifier = try values.decode(String.self, forKey: .identifier)
    }
}
extension Preset{
    var isSaveable:Bool{
        return true
    }
}
extension Preset{
    func deleteFromServer(completion:@escaping ()->Void){
        PFCloud.callFunction(inBackground: "deletePreset", withParameters: ["presetId":self.identifier]) { (response, error) in
            if let error = error{
                handle(error: error, message: "Error Deleting Preset", shouldDisplay: true)
            }
            completion()
        }
    }
    
    func activate(completion:@escaping()->Void){
        guard let smApt = CMQAPIManager.sharedInstance().smartAptUnit else {completion();return}
        
        var params = activateParameters
        smApt.fetchInBackground { (apt, error) in
            if let error = error{
                print(error)
                completion()
            }else if let apt = apt as? CMQSmartAptUnit{
                params["refresh"] = apt.tokens["refreshToken"] as! String
                PFCloud.callFunction(inBackground: "smartApartmentPreset", withParameters: params) { (response, error) in
                    if let error = error{
                        handle(error: error, message: "Error Activating Preset", shouldDisplay: true)
                    }
                    completion()
                }
                
            }
        }
        
    }
    var activateParameters:[String:Any]{
        var returnable =  [String:Any]()
        guard let smApt = CMQAPIManager.sharedInstance().smartAptUnit else {return returnable}
        returnable["smartAptUnit"] = smApt.objectId
        returnable["installId"] = PFInstallation.current()?.installationId
        returnable["refresh"] = smApt.tokens["refreshToken"] as! String
        returnable["presetId"] = self.identifier
        return returnable
    }
    
    func create(completion:@escaping ()->Void){
        PFCloud.callFunction(inBackground: "createPreset", withParameters: self.saveParameters) { (response, error) in
            if let error = error{
                handle(error: error, message: "Error Saving Preset", shouldDisplay: true)
            }
            completion()
        }
    }
    var saveParameters:[String:Any]{
        do {
            var returnable = [String:Any]()
            guard let smApt = CMQAPIManager.sharedInstance().smartAptUnit else {return returnable}
            returnable["smartAptUnit"] = smApt.objectId
            returnable["installId"] = PFInstallation.current()?.installationId
            returnable["refresh"] = smApt.tokens["refreshToken"] as! String
            let commandsEncoded = self.commands.filter({!$0.isRoomCommand}).map({$0.encode()})
            let roomsEncoded = self.commands.filter({$0.isRoomCommand}).map({$0.encode()})
            returnable["commands"] = commandsEncoded//String.init(data: json, encoding: .utf8)
            returnable["rooms"] = roomsEncoded
            returnable["presetName"] = self.name
            returnable["presetId"] = self.identifier
            return returnable
        }
    }
}
extension Preset{
    var data:[CollectionViewData]{
        var returnable = [CollectionViewData]()
        let nonThermCommands = self.commands.filter({$0.deviceType != "thermostat"})
        returnable.append(contentsOf: nonThermCommands as [CollectionViewData])
        let thermostatCommands = Set(commands).subtracting(nonThermCommands)
        switch thermostatCommands.count{
        case 2:
            //There can be a setTemp and a ChangeMode Command in Preset.
            let firstIndex = thermostatCommands.startIndex, lastIndex = thermostatCommands.index(after: firstIndex)
            if let device = thermostatCommands[firstIndex].device(with: thermostatCommands[lastIndex]){
                returnable.append(device)
            }
        case 1:
            returnable.append(thermostatCommands.first!)
        default:
            break
        }
        return returnable
    }
}
public struct PresetCollectionViewData:CollectionViewData{
    public enum GroupType:String{
        case Thermostat
        case DoorLock
        case Room
        static var AllCases:[GroupType]{
            return [.Thermostat, .DoorLock, .Room]
        }
    }
    var groupType:GroupType
    public var collectionCellIdentifier: String{
        return groupType.rawValue
    }
    var devices:[CMQSmartDevice]
    public var data:[CMQSmartDevice]{
        switch self.groupType {
        case .Thermostat:
            return devices.filter({if case .Thermostat(_) = $0.device{return true};return false})
        case .DoorLock:
            return devices.filter({if case .Lock(_) = $0.device{return true};return false})
        case .Room:
            return devices.filter({ (device) -> Bool in
                switch device.device{
                case .Light(_), .Outlet(_), .Fan(_):
                    return true
                default:
                    return false
                    
                }
            })
        }
    }
    public var numberOfCells: Int{
        return data.count
    }
    
    public func data(at indexPath: IndexPath) -> CMQSmartDevice? {
        return data[indexPath.row]
    }
}
class PresetRoomCell:UICollectionViewCell{
    @IBOutlet public var deviceImageView:UIImageView!
    @IBOutlet public var deviceStateLabel1:UILabel!
    @IBOutlet public var deviceStateLabel2:UILabel!
    public var room:Room!{
        didSet{
            
        }
    }
    public var device:CMQSmartDevice!{
        didSet{
            switch device.device {
            case .Fan(let value):
                deviceStateLabel2.isHidden = false
                deviceStateLabel1.text = "Speed"
                deviceStateLabel2.text = value > 0 ? "\(device.device.fanLevelIndex!)": "Off"
                deviceImageView.image = value > 0 ? device.device.iconHighlighted : device.device.icon
            case .Light(let value):
                self.deviceStateLabel1.text = device.name
                let levelInt = Int(device.device.level)
                self.deviceStateLabel2.text = value > 0 ?"\(String.init(describing: levelInt))%":"Off"
                deviceImageView.image = value > 0 ? device.device.iconHighlighted : device.device.icon
            case .Outlet(let value):
                deviceStateLabel1.text = device.name
                deviceStateLabel2.isHidden = false
                deviceStateLabel2.text = value ? "On" : "Off"
                deviceImageView.image = value ? device.device.iconHighlighted : device.device.icon
            default:
                return
            }
        }
    }
}
