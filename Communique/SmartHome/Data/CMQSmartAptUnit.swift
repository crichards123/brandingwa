//
//  CMQSmartAptUnit.swift
//  Communique
//
//  Created by Andre White on 6/26/18.
//  Copyright © 2018 Communique, LLC. All rights reserved.
//

import UIKit
public protocol SmartUnitDeviceProtocol{
    func devicesUpdated()
    
}
public class CMQSmartAptUnit: PFObject {
    @NSManaged var devices:[String:Any]!
    @NSManaged var tokens:[String:Any]!
    @NSManaged var pinCodes:[String:Any]!
    @NSManaged var thermostatType:String!
    fileprivate var updating:Bool = false
    var pinParameters:[String:Any]{
        var returnable = [String:Any]()
        returnable["deviceType"] = "lock"
        returnable["deviceId"] = self.doorLock.identifier
        returnable["provider"] = "SmartThings"
        returnable["command"] = "deleteCode"
        returnable["smartAptUnit"] = self.objectId
        returnable["installId"] = PFInstallation.current()?.installationId
        returnable["isMobileCode"] = true
        returnable["state"] = ["mode":"11"]
        return returnable
    }
    func paramsFor(new pin:[String:Any])->[String:Any]{
        var params = pinParameters
        params["command"] = "changeCode"
        params["state"] = pin
        params["residentPins"] = self.pinsAsDict()
        return params
    }
    func pinsAsDict()->[String:String]{
        var pins = [String:String]()
        if let some = self.pinCodes["pincodes"] as? [[String:String]]{
            for dict in some{
                for key in dict.keys{
                    pins[key] = dict[key]
                }
            }
        }
        return pins
    }
    func paramsFor(pin withID:String)->[String:Any]{
        var params = pinParameters
        params["state"] = ["mode":withID]
        return params
    }
    public var userDevices:[CMQSmartDevice]{
        var returnable = [CMQSmartDevice]()
        for room in userRooms{
            returnable.append(contentsOf: room.devices)
        }
        if let therm = thermostat{
            returnable.append(therm)
        }
        if let lock = doorLock{
            returnable.append(lock)
        }
        return returnable
    }
    public var userRooms:[Room] {
        var returnable = [Room]()
        let rooms = devices["rooms"] as! [[String:Any]]
        for dict in rooms{
            let data = try? JSONSerialization.data(withJSONObject: dict, options: .prettyPrinted)
            let room = try! JSONDecoder().decode(Room.self, from: data!)
            returnable.append(room)
        }
        guard let userRooms = CMQUser.current().smartAptRoom as? [String] else {return [Room]()}
        return returnable.filter({userRooms.contains($0.identifier)})
    }
    public var deviceDelegate:SmartUnitDeviceProtocol?
    public var userPresets:[Preset]!{
        var returnable = [Preset]()
        let _ = try? CMQUser.current().fetch()
        guard let presetsDict = CMQUser.current().smartAptPresets else {return returnable}
        let presets = presetsDict["presets"] as! [[String:Any]]
        for dict in presets{
            let data = try? JSONSerialization.data(withJSONObject: dict, options: .prettyPrinted)
            let preset = try? JSONDecoder().decode(Preset.self, from: data!)
            returnable.append(preset!)
        }
        
        return returnable
    }
    public var thermostat:CMQSmartDevice!{
        var returnable = [CMQSmartDevice]()
        let thermostats = devices["thermostats"] as! [[String:Any]]
        for dict in thermostats{
            let data = try? JSONSerialization.data(withJSONObject: dict, options: .prettyPrinted)
            let device = try! JSONDecoder().decode(CMQSmartDevice.self, from: data!)
            returnable.append(device)
        }
        return returnable.first
    }
    public var currentTemp:String!{
        guard let aThermostat = self.thermostat else {return nil}
        return aThermostat.device.currentTemp
    }
    public var doorLock:CMQSmartDevice!{
        var returnable = [CMQSmartDevice]()
        let locks = devices["locks"] as! [[String:Any]]
        for dict in locks{
            let data = try? JSONSerialization.data(withJSONObject: dict, options: .prettyPrinted)
            let device = try! JSONDecoder().decode(CMQSmartDevice.self, from: data!)
            returnable.append(device)
        }
        return returnable.first
    }
    public var userDevicesViewable:[CollectionViewData]{
        var returnable = [CollectionViewData]()
        returnable.append(contentsOf: userRooms)
        returnable.append(contentsOf: self.userDevices.filter({if case .Thermostat(_) = $0.device{return true};return false}))
        returnable.append(contentsOf: self.userDevices.filter({if case .Lock(_) = $0.device{return true};return false}))
        return returnable
    }
    public func commandParams(for devices:[CMQSmartDevice])->[String:Any]{
        
        var params = devices.first!.createCommand()
        params["deviceId"] = devices.compactMap({$0.identifier})
        params["smartAptUnit"] = self.objectId!
        params["refresh"] = self.tokens["refreshToken"] as! String
        for room in userRooms{
            if devices.first!.belongs(to: room){
                params["roomId"] = room.identifier
            }
        }
        params["installId"] = PFInstallation.current()?.installationId
        return params
    }
}
extension Command{
    enum CommandError:Error{
        case NoSmartApt
    }
    func execute(completion:@escaping(_ error:Error?)->Void){
        guard let smApt = CMQAPIManager.sharedInstance().smartAptUnit else {completion(CommandError.NoSmartApt);return}
        
        smApt.fetchInBackground { (object, error) in
            if let error = error{
                completion(error)
            }else if let object = object as? CMQSmartAptUnit{
                var params = object.commandParams(for: self.devices)
                params["command"] = self.command
                PFCloud.callFunction(inBackground: "smartApartmentCommand", withParameters: params) { (object, error) in
                    if let error = error{
                        completion(error)
                    }else if let _ = object{
                       // print(object)
                        completion(nil)
                    }
                }
            }
        }
    }
}

extension CMQSmartAptUnit:PFSubclassing{
    public static func parseClassName() -> String {
        return "SmartAptUnit"
    }
}
extension CMQSmartAptUnit{
    public func updateDevices(){
        DispatchQueue.main.asyncAfter(deadline: .now()) {
            guard !self.updating else {return}
            self.updating = true
            let _ = try? self.fetch()
            self.deviceDelegate?.devicesUpdated()
            self.updating = false
            /*
            self.fetchInBackground { (object, error) in
                self.updating = false
                if let error = error{
                    handle(error: error, message: "Error Updating Devices", shouldDisplay: false)
                }else if let smApt = object as? CMQSmartAptUnit{
                    //smApt.getUnit()
                    self.deviceDelegate?.devicesUpdated()
                    
                    
                }
            }*/
        }
        
    }
    public func device(withID:String)->CMQSmartDevice?{
        return self.userDevices.filter({$0.identifier == withID}).first
    }
    /*public func getUnit(){
        userDevices.removeAll()
        userRooms.removeAll()
        let locks = devices["locks"] as! [[String:Any]]
        for dict in locks{
            let data = try? JSONSerialization.data(withJSONObject: dict, options: .prettyPrinted)
            let device = try! JSONDecoder().decode(CMQSmartDevice.self, from: data!)
            userDevices.append(device)
        }
        let thermostats = devices["thermostats"] as! [[String:Any]]
        for dict in thermostats{
            let data = try? JSONSerialization.data(withJSONObject: dict, options: .prettyPrinted)
            let device = try! JSONDecoder().decode(CMQSmartDevice.self, from: data!)
            userDevices.append(device)
        }
        let rooms = devices["rooms"] as! [[String:Any]]
        for dict in rooms{
            let data = try? JSONSerialization.data(withJSONObject: dict, options: .prettyPrinted)
            let room = try! JSONDecoder().decode(Room.self, from: data!)
            userRooms.append(room)
            userDevices.append(contentsOf: room.devices)
        }
        
        self.deviceDelegate?.devicesUpdated()
        /* let presets = (CMQUser.current().smartAptPresets as! [String:Any])["presets"] as! [Any]
         for preset in presets{
         let data = try? JSONSerialization.data(withJSONObject: preset, options: .prettyPrinted)
         let aPreset = try! JSONDecoder().decode(Preset.self, from: data!)
         print(aPreset)
         userPresets.append(aPreset)
         //let room = try! JSONDecoder().decode(Room.self, from: data!)
         
         }*/
    }*/
}

extension CMQSmartDevice{
    func createCommand()->[String:Any]{
        var returnable:[String:Any] = self.device.changeParams
        returnable["deviceId"] = self.identifier
        return returnable
        
    }
}

