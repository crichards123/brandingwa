//
//  CMQSmartHomeDataSource.swift
//  Communique
//
//  Created by Andre White on 6/26/18.
//  Copyright © 2018 Communique, LLC. All rights reserved.
//

import UIKit
///Unused Class

public class SmartHomeDataSource: NSObject {
    public static let shared = SmartHomeDataSource()
    public var shouldContainPresets:Bool = true
    public var shouldContainPinCodes:Bool = true
    public var cachedDevices:[String:Any] = [String:Any]()
    public var updatingDeviceKey:String = ""
    private override init() {
        /*fatalError("This is a singleton. Use CMQSmartHomeDataSource.shared() to access.")*/
    }
    var devices:Set<CMQSmartDevice> = Set<CMQSmartDevice>()
    var rooms:Set<Room> = Set<Room>()
    var presets:Set<Preset> = Set<Preset>()
    var updaters:[DeviceUpdater] = [DeviceUpdater]()
    public func updateDevices(deviceID:String){
        
    }
}
extension SmartHomeDataSource:UICollectionViewDataSource{
    private var collectionViewCellImageViewTag:Int{
        return 999
    }
    private var collectionViewCelllabelTag:Int{
        return 888
    }
    public var buttonData:[ButtonType]{
        var returnable = [ButtonType]()
        if shouldContainPresets{
            returnable.append(.Presets)
        }
        
        if let rooms = CMQAPIManager.sharedInstance().smartAptUnit?.userRooms{
            if rooms.count > 0{
                returnable.append(.Rooms)
            }
        }
        if let _ = CMQAPIManager.sharedInstance().smartAptUnit?.doorLock{
            returnable.append(.Lock)
            if shouldContainPinCodes{
                returnable.append(.PinCode)
            }
        }
        if let _ = CMQAPIManager.sharedInstance().smartAptUnit?.thermostat{
            returnable.append(.Thermostat)
        }
        return returnable
    }
    public func button(at indexPath:IndexPath)->ButtonType?{
        guard indexPath.row < buttonData.count else {return nil}
        return buttonData.sorted(by: {$0.initialPageTag < $1.initialPageTag})[indexPath.row]
    }
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let data = buttonData.sorted(by: {$0.initialPageTag < $1.initialPageTag})[indexPath.row]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "reuse", for: indexPath)
        let label = cell.viewWithTag(collectionViewCelllabelTag) as? UILabel
        label?.text = data.title
        let imageView = cell.viewWithTag(collectionViewCellImageViewTag) as? UIImageView
        if data == .PinCode{
            imageView?.isHidden = true
        }else{
            imageView?.isHidden = false
        }
        imageView?.image = data.image
        return cell
    }
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.buttonData.count
    }
    public func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
}
public enum ButtonType:String{
    case Lock
    case Rooms
    case Thermostat
    case Presets
    case PinCode
    
    var image:UIImage?{
        switch self{
        case .Lock:
            return UIImage.init(named: "Door Handle Icon", in: CMQBundle, compatibleWith: nil)
        case .Rooms:
            return UIImage.init(named: "Room Icon", in: CMQBundle, compatibleWith: nil)
        case .Thermostat:
            return UIImage.init(named: "Thermostat Icon", in: CMQBundle, compatibleWith: nil)
        case .Presets:
            return UIImage.init(named: "Preset Icon", in: CMQBundle, compatibleWith: nil)
        case .PinCode:
            return nil
        }
    }
    var unhighlightedImage:UIImage?{
        switch self{
        case .Lock:
            return UIImage.init(named: "Door Handle Tab Icon - disabled", in: CMQBundle, compatibleWith: nil)
        case .Rooms:
            return UIImage.init(named: "Room Icon Tab Icon - disabled", in: CMQBundle, compatibleWith: nil)
        case .Thermostat:
            return UIImage.init(named: "Thermostat  Tab Icon - disabled", in: CMQBundle, compatibleWith: nil)
        case .Presets:
            return UIImage.init(named: "Preset Icon", in: CMQBundle, compatibleWith: nil)
        case .PinCode:
            return nil
        }
    }
    var title:String{
        switch self{
        case .Lock:
            return "Door Lock"
        case .PinCode:
            return "Pin Codes"
        default:
            return self.rawValue
        }
    }
    var initialPageTag:Int{
        switch self{
        case .Lock:
            return 0
        case .Thermostat:
            return 1
        case .Rooms:
            return 2
        case .Presets:
            return 3
        case .PinCode:
            return 1000
        }
    }
    var viewController:UIViewController?{
        var controller:UIViewController
        switch self {
        case .Lock:
            controller = UIStoryboard(name: "SmartHome", bundle: nil).instantiateViewController(withIdentifier: "Lock")
            
        case .Thermostat:
            controller = UIStoryboard(name: "SmartHome", bundle: nil).instantiateViewController(withIdentifier: "Thermostat")
        case .Rooms:
            controller =  UIStoryboard(name: "SmartHome", bundle: nil).instantiateViewController(withIdentifier: "Rooms")
        case .Presets:
            return nil
        case .PinCode:
            return nil
        }
        (controller.view as! CMQGradientView).gradientTopColor = .clear
        (controller.view as! CMQGradientView).gradientBottomColor = .clear
        return controller
    }
    var collectionViewData:CollectionViewData?{
        switch self{
        case .Lock:
            return CMQAPIManager.sharedInstance().smartAptUnit?.doorLock
        case .Thermostat:
            return CMQAPIManager.sharedInstance().smartAptUnit?.thermostat
        case .Rooms:
            return nil//CMQAPIManager.sharedInstance().smartAptUnit?.userRooms
        case .Presets:
            return nil
        case .PinCode:
            return nil
        }
    }
}

