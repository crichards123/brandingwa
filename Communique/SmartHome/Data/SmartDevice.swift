//
//  SmartDevice.swift
//  Communique
//
//  Created by Andre White on 6/26/18.
//  Copyright © 2018 Communique, LLC. All rights reserved.
//

import Foundation
enum SmartError:Int , Error{
    case Decode = 0
    case Encode
}

public enum SmartDevice{
    
    case Thermostat(value:ThermostatMode)
    case Fan(value:Int)
    case Light(level:Float)
    case Lock(state:LockMode)
    case Outlet(value:Bool)
    
    public enum LockMode:String, Codable, Hashable{
        case unlocked
        case locked
        public init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            let someMode = try values.decode(String.self, forKey: .mode)
            if someMode == "unknown"{
                self = .locked
            }else{
                self = LockMode.init(rawValue: someMode)!
            }
            
        }
        enum CodingKeys:CodingKey{
            case mode
        }
    }
    public enum LightMode:Codable, Hashable{
        public init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            let someMode = try values.decode(String.self, forKey: .mode)
            let level = try values.decode(String.self, forKey: .level)
            switch someMode {
            case "on":
                self = .on(level: level)
            case "off":
                self = .off(level: level)
            default:
                throw SmartError.Decode
            }
        }
        public func encode(to encoder: Encoder) throws {
            var container = encoder.container(keyedBy: CodingKeys.self)
            switch self {
            case .on(let level):
                try container.encode("on", forKey: .mode)
                try container.encode(level, forKey: .level)
            case .off(let level):
                try container.encode("off", forKey: .mode)
                try container.encode(level, forKey: .level)
            }
        }
        
        case on(level:String)
        case off(level:String)
        enum CodingKeys:CodingKey{
            case mode
            case level
        }
    }
    public enum ThermostatMode:Codable{
        public init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            
            let someMode = try values.decode(String.self, forKey: .mode)
            let curTemp = try values.decode(String.self, forKey: .curTemp)
            let heat = try values.decode(String.self, forKey: .heat)
            let cool = try values.decode(String.self, forKey: .cool)
            switch someMode {
            case "off":
                self = .Off(curTemp:curTemp, heat: heat, cool: cool)
            case "heat":
                self = .Heat(curTemp:curTemp, heat: heat, cool: cool)
            case "cool":
                self = .Cool(curTemp:curTemp, heat: heat, cool: cool)
            default:
                self = .Off(curTemp:curTemp, heat: heat, cool: cool)
            }
            
        }
        
        public func encode(to encoder: Encoder) throws {
            var container = encoder.container(keyedBy: CodingKeys.self)
            switch self {
            case .Auto(let curTemp, let heat, let cool):
                try container.encode("Auto", forKey: .mode)
                try container.encode(curTemp, forKey: .curTemp)
                try container.encode(heat, forKey: .heat)
                try container.encode(cool, forKey: .cool)
            case .Heat(let curTemp, let heat, let cool):
                try container.encode("Heat", forKey: .mode)
                try container.encode(curTemp, forKey: .curTemp)
                try container.encode(heat, forKey: .heat)
                try container.encode(cool, forKey: .cool)
            case .Cool(let curTemp, let heat, let cool):
                try container.encode("Cool", forKey: .mode)
                try container.encode(curTemp, forKey: .curTemp)
                try container.encode(heat, forKey: .heat)
                try container.encode(cool, forKey: .cool)
            case .Off(let curTemp, let heat, let cool):
                try container.encode("Off", forKey: .mode)
                try container.encode(curTemp, forKey: .curTemp)
                try container.encode(heat, forKey: .heat)
                try container.encode(cool, forKey: .cool)
            }
        }
        var HeatColor:UIColor {
            return UIColor.init(red: 248/255.0, green: 147.0/255.0, blue: 31/255.0, alpha: 1.0)
        }
        var CoolColor:UIColor{
            return UIColor.init(red: 115/255.0, green: 198/255.0, blue: 238/255.0, alpha: 1.0)
        }
        var AutoColor:UIColor{
            return UIColor.init(red: 115/255.0, green: 238.0/255.0, blue: 127/255.0, alpha: 1.0)
        }
        var OffColor:UIColor {
            return UIColor.init(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.30)
        }
        
        case Heat(curTemp:String, heat:String, cool:String)
        case Cool(curTemp:String, heat:String, cool:String)
        case Off(curTemp:String, heat:String, cool:String)
        case Auto(curTemp:String, heat:String, cool:String)
        func borderColor()->CGColor{
            switch self {
            case .Heat:
                return HeatColor.cgColor
            case .Cool:
                return CoolColor.cgColor
            case .Off:
                return OffColor.cgColor
            case .Auto:
                return AutoColor.cgColor
            }
        }
        enum CodingKeys:String, CodingKey{
            case curTemp = "currentTemp"
            case heat  = "heatingSetpoint"
            case cool = "coolingSetpoint"
            case mode
        }
    }
}
extension SmartDevice.ThermostatMode{
    public var modeText:String{
        switch self{
        case .Heat(_, _, _):
            return "heat"
        case .Cool(_, _, _):
            return "cool"
        case .Off(_, _, _):
            return "off"
        case .Auto(_, _,_):
            return "auto"
        }
    }
    public var heatTemp:String{
        switch self {
        case .Heat(_, let heat,_):
            return heat
        case .Cool(_, let heat,_):
            return heat
        case .Off(_, let heat,_):
            return heat
        case .Auto(_, let heat,_):
            return heat
        }
    }
    public var coolTemp:String{
        switch self {
        case .Heat(_, _,let cool):
            return cool
        case .Cool(_, _,let cool):
            return cool
        case .Off(_, _,let cool):
            return cool
        case .Auto(_, _,let cool):
            return cool
        }
    }
}

extension SmartDevice:Decodable{
    enum CodingKeys:CodingKey{
        case state
    }
    public init(from decoder: Decoder) throws {
        let parent = try! decoder.container(keyedBy: CMQSmartDevice.CodingKeys.self)
        let deviceType = try! parent.decode(String.self, forKey: .deviceType)
        let type = CMQSmartDevice.DeviceType.init(rawValue: deviceType)!
        switch type{
            
        case .Thermostat:
            let thermMode = try parent.decode(ThermostatMode.self, forKey: .device)
            self = .Thermostat(value: thermMode)
        case .Light:
            let lightMode = try parent.decode(LightMode.self, forKey: .device)
            switch lightMode{
                
            case .on(let level):
                self = .Light(level: (level as NSString).floatValue)
            case .off(let level):
                self = .Light(level: (level as NSString).floatValue)
            }
        case .Lock:
            let lockmode = try parent.decode(LockMode.self, forKey: .device)
            self = .Lock(state: lockmode)
        case .Outlet:
            let stateDict = try parent.decode([String:String?].self, forKey: .device)
            let mode = stateDict["mode"]!
            switch mode{
            case "off":
                self = .Outlet(value: false)
            case "on":
                self = .Outlet(value: true)
            default:
                throw SmartError.Decode
            }
        case .Fan:
            let lightMode = try parent.decode(LightMode.self, forKey: .device)
            switch lightMode{
                
            case .on(let level):
                self = .Fan(value: (level as NSString).integerValue)
            case .off(_):
                self = .Fan(value: 0)
            }
        }
    }
}
extension SmartDevice.ThermostatMode:Hashable{
    public var hashValue:Int{
        switch self{
        case .Auto(let curTemp, _, _):
            return curTemp.hashValue
        case .Heat(let curTemp, _, _):
            return curTemp.hashValue
        case .Cool(let curTemp, _, _):
            return curTemp.hashValue
        case .Off:
            return false.hashValue
        }
    }
}

extension SmartDevice.ThermostatMode:Equatable{
    public static func == (lhs: SmartDevice.ThermostatMode, rhs: SmartDevice.ThermostatMode) -> Bool{
        switch (lhs, rhs){
        case let (.Heat(param1),.Heat(param2)):
            return param1.cool == param2.cool && param1.heat == param2.heat && param1.curTemp == param2.curTemp
        case let (.Cool(param1), .Cool(param2)):
            return param1.cool == param2.cool && param1.heat == param2.heat && param1.curTemp == param2.curTemp
        case let (.Auto(param1), .Auto(param2)):
            return param1.cool == param2.cool && param1.heat == param2.heat && param1.curTemp == param2.curTemp
        case (.Off, .Off):
            return true
        default:
            return false
            
        }
        
    }
}

extension SmartDevice:Hashable{
    public var hashValue: Int{
        switch self{
            
        case .Thermostat(let value):
            return value.hashValue
        case .Fan(let value):
            return value.hashValue
        case .Light(let value):
            return value.hashValue
        case .Lock(let value):
            return value.hashValue
        case .Outlet(let value):
            return value.hashValue
        }
    }
}
extension SmartDevice:Equatable{
    public static func == (lhs: SmartDevice, rhs: SmartDevice) -> Bool{
        switch (lhs, rhs) {
        case let (.Thermostat(value1),.Thermostat(value2)):
            return value1 == value2
        case let (.Fan(value1), .Fan(value2)):
            return value1 == value2
        case let (.Light(value1), .Light(value2)):
            return value1 ==  value2
        case let (.Lock(value1), .Lock(value2)):
            return value1 == value2
        case let (.Outlet(value1), .Outlet(value2)):
            return value1 == value2
        default:
            return false
        }
    }
}
extension SmartDevice{
    public var lockMode:SmartDevice.LockMode?{
        if case .Lock(let value) = self{
            return value
        }
        return nil
    }
}
public struct CMQSmartDevice:Named,Identifier, Equatable, Hashable{
    
    static func device(with iD:String)->CMQSmartDevice?{
        guard let smApt = CMQAPIManager.sharedInstance().smartAptUnit else {return nil}
        var allLights:[CMQSmartDevice] = [CMQSmartDevice]()
        for room in smApt.userRooms{
            allLights.append(contentsOf: room.devices)
        }
        if let device = allLights.filter({$0.identifier==iD}).first{
            return device
        }
        if smApt.thermostat.identifier == iD{
            return smApt.thermostat
        }else if smApt.doorLock.identifier==iD{
            return smApt.doorLock
        }
        return nil
    }
    var name: String
    var identifier: String
    var device:SmartDevice
    
    enum CodingKeys:String, CodingKey{
        case name
        case identifier = "deviceId"
        case device = "state"
        case deviceName
        case deviceType
    }
}
extension CMQSmartDevice{
    public enum DeviceType:String{
        case Thermostat
        case Light = "light"
        case Lock = "Door Lock"
        case Outlet = "socket"
        case Fan = "fan"
        init?(with:SmartDevice){
            switch with {
            case .Thermostat(_):
                self = .Thermostat
            case .Fan(_):
                self = .Fan
            case .Light(_):
                self = .Light
            case .Lock(_):
                self = .Lock
            case .Outlet(_):
                self = .Outlet
            }
        }
    }
    public var type:DeviceType{
        return DeviceType.init(with: self.device)!
    }
}
extension CMQSmartDevice:Decodable{
    public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        if values.contains(.deviceName){
            if let aName = try? values.decode(String.self, forKey: .deviceName){
                name = aName
            }else{
                name = ""
            }
        }else{
            if let aName = try? values.decode(String.self, forKey: .name){
                name = aName
            }else{
                name = ""
            }
        }
        identifier = try values.decode(String.self, forKey: .identifier)
        //device = try values.decode(SmartDevice.self, forKey: .device)
        device = try SmartDevice.init(from: decoder)
        
    }
}
extension CMQSmartDevice:TableViewData{
    public var tableCellIdentifier: String {
        switch device {
    
        case .Fan(_):
            return "Fan"
        case .Light(_):
            return "Light"
        case .Outlet(_):
            return "Outlet"
        default:
            return "UNKNOWN"
        }
    }
    
    public func data(at indexPath: IndexPath) -> CMQSmartDevice? {
        return self
    }
    
    
}
protocol DeviceGroup:Equatable {
    var devices:[CMQSmartDevice] {get}
}
public protocol CollectionViewData{
    var collectionCellIdentifier:String {get}
    var numberOfCells:Int {get}
    func data(at indexPath:IndexPath)->CMQSmartDevice?
}

public protocol TableViewData{
    var tableCellIdentifier:String {get}
    func data(at indexPath:IndexPath)->CMQSmartDevice?
}

protocol CommandIssueProtocol {
    typealias CommandCompletion = (_ error:Error?)->Void
    func issueCommand(devices:[CMQSmartDevice], completion:@escaping CommandCompletion)
    func command(for device:CMQSmartDevice)->Command
    var canIssue:Bool {get}
}
extension CommandIssueProtocol{
    func command(for device:CMQSmartDevice)->Command{
        return Command.init(devices: [device])
    }
    var canIssue:Bool{
        guard let _ = CMQAPIManager.sharedInstance().smartAptUnit?.userDevices else {return false}
        return true
    }
    func issueCommand(devices:[CMQSmartDevice], completion: @escaping CommandCompletion){
        guard canIssue else {return}
        if devices.count == 1{
            if let fDevice = devices.first{
                let command = self.command(for: fDevice)
                command.execute(completion: completion)
            }
        }
        
    }
    
}

extension CMQSmartDevice{
    func belongs(to room:Room) -> Bool {
        return room.devices.contains(where: {$0.identifier == self.identifier})
    }
}

extension CMQSmartDevice:CollectionViewData{
    public var numberOfCells: Int {
        return 1
    }
    public var collectionCellIdentifier:String {
        switch self.device{
        case .Thermostat(_):
          return "Thermostat"
        case .Lock(_):
            return "Lock"
        default:
            return ""
        }
    }
}

extension SmartDevice{
    var iconHighlighted:UIImage{
        switch self{
            
        case .Thermostat(_):
            return UIImage.init(named: "Thermostat Icon", in: CMQBundle, compatibleWith: nil)!
        case .Fan(_):
            return UIImage.init(named: "Fan Icon", in: CMQBundle, compatibleWith: nil)!
        case .Light(_):
            return UIImage.init(named: "Light on", in: CMQBundle, compatibleWith: nil)!
        case .Lock(_):
            return UIImage.init(named: "Door Handle Icon", in: CMQBundle, compatibleWith: nil)!
        case .Outlet(_):
            return UIImage.init(named: "All Outlets - on", in: CMQBundle, compatibleWith: nil)!
        }
    }
    var icon:UIImage{
        switch self{
            
        case .Thermostat(_):
            return UIImage.init(named: "Thermostat  Tab Icon - disabled", in: CMQBundle, compatibleWith: nil)!
        case .Fan(_):
            return UIImage.init(named: "Fan Icon - off", in: CMQBundle, compatibleWith: nil)!
        case .Light(_):
            return UIImage.init(named: "Light Off", in: CMQBundle, compatibleWith: nil)!
        case .Lock(_):
            return UIImage.init(named: "Door Handle Tab Icon - disabled", in: CMQBundle, compatibleWith: nil)!
        case .Outlet(_):
            return UIImage.init(named: "All Outlets - off", in: CMQBundle, compatibleWith: nil)!
        }
    }
}
extension SmartDevice{
    var providerString:String{
        return "SmartThings"
    }
    var deviceTypeString:String{
        switch self{
            
        case .Thermostat(_):
            return "thermostat"
        case .Fan(_):
            return "fan"
        case .Light(_):
            return "lightGroup"
        case .Lock(_):
            return "lock"
        case .Outlet(_):
            return "socket"
        }
    }
    var commandString:String{
        switch self {
            
        case .Thermostat(_):
            return "setTemp||changeMode"
        case .Fan(_):
            return "setFanLevel"
        case .Light(_):
            return "setLightLevel"
        case .Outlet(_):
            return "setSocketMode"
        case .Lock(_):
            return "lock"
        }
    }
    var stateDictionary:[String:Any]{
        switch self {
            
        case .Thermostat(let value):
            switch value{
            case .Auto(let curTemp, let heat, let cool):
                return ["mode":"auto"]
            case .Heat(let curTemp, let heat, let cool):
                return ["mode":"heat", "arguments":Int(heat) ?? 80]
            case .Cool(let curTemp, let heat, let cool):
                return ["mode":"cool","arguments":Int(cool) ?? 80]
            case .Off(let curTemp, let heat, let cool):
                return ["mode":"off"]
            }
            
        case .Fan(let level):
            return ["arguments":level]
        case .Light(let level):
            return ["arguments":level]
        case .Lock(let state):
            return ["mode":state.rawValue]
        case .Outlet( let level):
            return ["mode":level ? "on":"off"]
        }
    }
    var changeParams:[String:Any]{
        var returnable:[String:Any] = [String:Any]()
        returnable["deviceType"] = self.deviceTypeString
        returnable["provider"] = self.providerString
        returnable["state"] = self.stateDictionary
        returnable["command"] = self.commandString
        return returnable
        
    }
}
protocol LightProtocol{
    var level:Float! {get set}
}
protocol LockProtocol {
    var isUnlocked:Bool! {get set}
}
protocol OutletProtocol{
    var On:Bool! {get set}
}
protocol FanProtocol {
    var fanLevel:Int! {get set}
}
extension SmartDevice:FanProtocol{
    var fanLevel:Int! {
        get{
            guard case .Fan(let value) = self  else {return nil}
            return value
        }
        set{
            self = .Fan(value: newValue)
        }
    }
}
extension SmartDevice:OutletProtocol{
    var On:Bool!{
        get{
            guard case .Outlet(let value) = self else { return nil}
            return value
        }
        set{
            self = .Outlet(value: newValue)
        }
    }
}
extension SmartDevice:LightProtocol{
    var level: Float! {
        get {
            guard case .Light(let value) = self else {return nil}
            return value
        }
        set {
            self = .Light(level: newValue)
        }
    }
}
extension SmartDevice:LockProtocol{
    var isUnlocked: Bool! {
        get {
            guard case .Lock(let value) = self else {return nil}
            switch value {
            case .unlocked:
                return true
            case .locked:
                return false
            }
        }
        set {
            self = .Lock(state: newValue ? .unlocked : .locked)
        }
    }
}
extension SmartDevice.ThermostatMode{
    var temp:String{
        switch self{
            
        case .Heat(_, let heat,_):
            return heat
        case .Cool(_, _, let cool):
            return cool
        case .Off(_,_,_):
            return "--"
        case .Auto(_, let heat, let cool):
            return "\(heat)-\(cool)"
        }
    }
}
extension SmartDevice:Thermostat{
    public var controlIndex: Int! {
        get{
            switch self.currentMode!{
            case .Heat(_,_,_):
                return 0
            case .Cool(_,_,_):
                return 1
            case .Off(_,_,_):
                return CMQAPIManager.sharedInstance().smartAptUnit!.thermostatType.contains("Ecobee") ? 3 : 2
            case .Auto(_,_,_):
                return 2
            }
        }
        set{
            switch newValue{
            case 0:
                self.currentMode = .Heat(curTemp:
                    self.currentTemp, heat: self.heatTemp, cool: self.coolTemp)
            case 1:
                self.currentMode = .Cool(curTemp:
                    self.currentTemp, heat: self.heatTemp, cool: self.coolTemp)
            case 2:
                if CMQAPIManager.sharedInstance().smartAptUnit!.thermostatType.contains("Ecobee"){
                    self.currentMode = .Auto(curTemp:
                        self.currentTemp, heat: self.heatTemp, cool: self.coolTemp)
                }else{
                    self.currentMode = .Off(curTemp:
                        self.currentTemp, heat: self.heatTemp, cool: self.coolTemp)
                }
            case 3:
                self.currentMode = .Off(curTemp:
                    self.currentTemp, heat: self.heatTemp, cool: self.coolTemp)
            default:
                return
            }
        }
    }
    
    
    public var currentMode: SmartDevice.ThermostatMode! {
        get {
            guard case SmartDevice.Thermostat(let value) = self else{ return nil}
            return value
        }
        set {
            self = SmartDevice.Thermostat(value: newValue)
        }
    }
    public var heatTemp:String!{
        guard case SmartDevice.Thermostat(let value) = self else{ return nil}
        switch value{
        case .Heat(_, let heat, _), .Cool(_, let heat, _),.Off(_, let heat,_),.Auto(_, let heat,_):
            return heat
        }
    }
    public var coolTemp:String!{
        guard case SmartDevice.Thermostat(let value) = self else{ return nil}
        switch value{
        case .Heat(_, _, let cool), .Cool(_, _, let cool),.Off(_, _, let cool),.Auto(_, _, let cool):
            return cool
        }
    }
    public var currentTemp: String! {
        get {
            guard case SmartDevice.Thermostat(let value) = self else{ return nil}
            switch value {
            case .Auto(let curTemp, _, _):
                return curTemp
            case .Heat(let curTemp, _, _):
                return curTemp
            case .Cool(let curTemp,_, _):
                return curTemp
            case .Off(let curTemp,_, _):
                return curTemp
            }
        }
        set {
            guard case SmartDevice.Thermostat(let value) = self else{ return }
            switch value {
            case .Heat(_, let heat, let cool):
                self = SmartDevice.Thermostat(value: .Heat(curTemp: newValue, heat: heat, cool: cool))
            case .Auto(_, let heat, let cool):
                self = SmartDevice.Thermostat(value: .Auto(curTemp: newValue, heat: heat, cool: cool))
            case .Cool(_, let heat, let cool):
                self = SmartDevice.Thermostat(value: .Cool(curTemp: newValue, heat: heat, cool: cool))
            case .Off(_, let heat, let cool):
                self = SmartDevice.Thermostat(value: .Off(curTemp: newValue, heat: heat, cool: cool))
            }
        }
    }
    
    public var setToTemp: String! {
        get {
            guard case SmartDevice.Thermostat(let value) = self else{ return nil}
            switch value {
            case .Auto(_, let heat, let cool):
                return "\(heat)-\(cool)"
            case .Heat(_, let heat, _):
                return heat
            case .Cool(_,_,let cool):
                return cool
            case .Off(_,_, _):
                return "--"
            }
        }
        set {
            guard case SmartDevice.Thermostat(let value) = self else{ return }
            switch value {
            case .Auto(let curTemp, _, let cool):
                self = .Thermostat(value: .Auto(curTemp: curTemp, heat: newValue, cool: cool))
            case .Heat(let curTemp, _, let cool):
                self = .Thermostat(value: .Heat(curTemp: curTemp, heat: newValue, cool: cool))
            case .Cool(let curTemp,let heat,_):
                self = .Thermostat(value: .Cool(curTemp: curTemp, heat: heat, cool: newValue))
            case .Off(_,_, _):
                return
            }
        }
    }
}
