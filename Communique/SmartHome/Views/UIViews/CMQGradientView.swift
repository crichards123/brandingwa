//
//  CMQGradientView.swift
//  Communique
//
//  Created by Andre White on 5/9/18.
//  Copyright © 2018 Communique, LLC. All rights reserved.
//

import UIKit
public class CMQGradientView: UIView, GradientProtocol {
    @IBInspectable public var gradientTopColor:UIColor! = .clear//UIColor.CMQSmartHomeGradientTopColor
    @IBInspectable public var gradientBottomColor:UIColor! = .clear//UIColor.CMQSmartHomeGradientBottomColor
    @IBInspectable public var shouldAddShadow:Bool = false
    var colors: [UIColor]{
        return [self.gradientTopColor, self.gradientBottomColor]
    }
    override open class var layerClass: AnyClass {
        return CAGradientLayer.classForCoder()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    public override func awakeFromNib() {
        super.awakeFromNib()
        self.addGradient()
        if shouldAddShadow{
            self.addShadow()
        }
    }

}
