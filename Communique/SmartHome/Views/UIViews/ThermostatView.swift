//
//  ThermostatView.swift
//  Communique
//
//  Created by Andre White on 8/5/18.
//  Copyright © 2018 Communique, LLC. All rights reserved.
//

import UIKit

public class ThermostatView: UIView {
    private let twoDigitconstant:CGFloat = -25
    private let threeDigitConstat:CGFloat = -10
    
    @IBOutlet weak var setToTemperatureLabel: UILabel!
    @IBOutlet weak var thermostatStateControl: UISegmentedControl!
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var bottomStackView: UIStackView!
    @IBOutlet weak var labelConstraint: NSLayoutConstraint!
    public var temperature:GenericBounded<CGFloat> = GenericBounded<CGFloat>.init(value: 78, bounds: (upper: 110, lower: 55))
    @IBOutlet weak var topSetToLabel: UILabel!
    public var thermostat:SmartDevice!

    public override func layoutSubviews() {
        super.layoutSubviews()
        self.layer.cornerRadius = self.frame.size.height/2
        self.layer.borderWidth = 7
    }
    private func updateBorderColor(){
        self.layer.borderColor = self.thermostat.currentMode.borderColor()
    }
    private func updateTemp(){
        let tempInt = Int(temperature.value)
        self.temperatureLabel.text = String(describing: tempInt)
        if (self.temperatureLabel.text?.count)! >= 3{
            self.labelConstraint.constant = threeDigitConstat
        }else{
            self.labelConstraint.constant = twoDigitconstant
        }
    }
    fileprivate func updateView(){
        self.thermostatStateControl.selectedSegmentIndex = self.thermostat.controlIndex
        self.setToTemperatureLabel.text = self.thermostat.setToTemp
        self.temperatureLabel.text = self.thermostat.currentTemp
        
        
        if (self.temperatureLabel.text?.count)! >= 3{
            self.labelConstraint.constant = threeDigitConstat
        }else{
            self.labelConstraint.constant = twoDigitconstant
        }
    }
    
    fileprivate func toggleView(){
        UIView.animate(withDuration: 0.25) {
            self.topSetToLabel.isHidden = !self.topSetToLabel.isHidden
            self.bottomStackView.isHidden = !self.topSetToLabel.isHidden
        }
    }
    
    public func updateUI(for state:CMQSmartHomeThermostatViewController.State){
        self.temperature.value = CGFloat((self.thermostat.setToTemp as NSString).floatValue)
        switch state{
        case .Recognizing:
            self.toggleView()
        case .Changing(let therm,let type):
            switch type{
            case .Mode:
                if !self.topSetToLabel.isHidden{
                    self.toggleView()
                }
            case .Temp:
                self.toggleView()
                self.setToTemperatureLabel.text = self.thermostat.setToTemp
                self.temperatureLabel.text = self.thermostat.currentTemp
            }
        default:
            updateTemp()
            updateBorderColor()
            updateView()
        }
        
    }
    public func tempShouldChange(sender:UIPanGestureRecognizer, height:CGFloat){
        let translation = sender.translation(in: self)
        let ratio = translation.y/height
        //print(ratio)
        let temp = temperature.value
        print(temp  + (temp * ratio) )
    }
    public func tempShouldChange(locationInView:CGPoint, viewHeight:CGFloat){
        let ratioToTop = locationInView.y/viewHeight
        let tempRange = temperature.rangeCount
        let temp = temperature.bounds.upper-(tempRange*ratioToTop)
        self.temperature.change(to: temp)
        self.updateTemp()
    }
    @IBAction func thermostatStateChanged(_ sender: UISegmentedControl){
        
    }
    

}
