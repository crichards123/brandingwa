//
//  MasterControlHeader.swift
//  Communique
//
//  Created by Andre White on 7/11/18.
//  Copyright © 2018 Communique, LLC. All rights reserved.
//

import UIKit

public class MasterControlHeader: UIView {
    public class func instanceFromNib(wSlider:Bool)->MasterControlHeader?{
        guard let nib = loadNib(name: "MasterControlHeader") else {return nil}
        let header =  nib.instantiate(withOwner: nil, options: nil).first as? MasterControlHeader
        if wSlider{
            header?.controlView = header?.slider
            header?.aSwitch.isHidden = true
        }else{
            header?.controlView = header?.aSwitch
            header?.slider.isHidden = true
        }
        return header
    }
    @IBOutlet weak var controlLabel: UILabel!
    public var controlView: UIControl!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var slider:UISlider!
    @IBOutlet weak var aSwitch:UISwitch!
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
