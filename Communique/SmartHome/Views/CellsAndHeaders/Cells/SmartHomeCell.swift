//
//  SmartHomeCell.swift
//  Communique
//
//  Created by Andre White on 7/12/18.
//  Copyright © 2018 Communique, LLC. All rights reserved.
//

import Foundation

public class CMQSmartHomeCell:UICollectionViewCell{
    @IBOutlet public var label:UILabel!
    @IBOutlet fileprivate var tableview:UITableView!
    @IBOutlet public var gradientView:CMQGradientView!{
        didSet{
            gradientView.addShadow()
        }
    }
    private let LockViewTag = 999
    private let ThermModeLabelTag = 888
    private let ThermTempLabelTag = 777
    public var data:CollectionViewData!{
        didSet{
            updateLabel()
            if shouldUseTableView{
                tableview.reloadData()
            }else{
                let device = self.data as! CMQSmartDevice
                switch device.type{
                case .Thermostat:
                    if let label = self.viewWithTag(ThermModeLabelTag) as? UILabel{
                        label.text = device.device.currentMode.modeText
                    }
                    if let label = self.viewWithTag(ThermTempLabelTag) as? UILabel{
                        let text = device.device.currentMode.temp
                        label.text = text.capitalizingFirstLetter
                    }
                case .Lock:
                    if let lockView = self.viewWithTag(LockViewTag) as? LockView{
                        lockView.lock = device.device
                    }
                default:
                    return
                }
            }
        }
    }
    public var shouldPerformCommand:Bool = true
    private var shouldUseTableView:Bool{
        return data is GroupedControlData || data is Room
    }
    fileprivate lazy var lightGesture:UITapGestureRecognizer = {
        return UITapGestureRecognizer.init(target: self, action: #selector(masterImageTapped(sender:)))
    }()
    fileprivate var headerView:MasterControlHeader?{
        didSet{
            guard let group = data as? GroupedControlData, let header = headerView else {return}
            header.imageView.addGestureRecognizer(self.lightGesture)
            header.controlView.addTarget(self, action: #selector(masterControlChanged(sender:)), for: .valueChanged)
            header.controlLabel.text = group.controlName
            if group.groupType == .Lights{
                if group.data.filter({$0.device.level>0}).count == group.data.count{
                    //All lights are on. Master should be on
                    header.imageView.image = group.controlImage.1
                }else{
                    header.imageView.image = group.controlImage.0
                }
                let allEqual = group.data.filter({$0.device.level != group.data.first?.device.level!}).count==0// group.data.reduce(0, {max($0, $1.device.level)})
                let control = header.controlView as! UISlider
                control.value = allEqual ? (group.data.first?.device.level)! : 0
            }else if group.groupType == .Outlets{
                if group.data.filter({$0.device.On}).count == group.data.count{
                    //All outlets are on. Master should be on
                    header.imageView.image = group.controlImage.1
                    (header.controlView as! UISwitch).On = true
                }else{
                    header.imageView.image = group.controlImage.0
                    (header.controlView as! UISwitch).On = false
                }
            }
            
        }
    }
    public var numberOfDevicesForTableView:Int{
        guard shouldUseTableView else {return 0}
        switch data {
        case is GroupedControlData:
            return data.numberOfCells
        case is Room:
            let room = data as! Room
            return room.devices.count
        default:
            return data.numberOfCells
        }
        
    }
    static let DeletePresetNotification = Notification.Name("DeletePreset")
    @IBAction func didPressDelete(_ sender: Any) {
        NotificationCenter.default.post(name: CMQSmartHomeCell.DeletePresetNotification, object: self)
    }
    
    override public func awakeFromNib() {
        self.updateLabel()
        
        
    }
    func updateLabel(){
        if let some = data as? GroupedControlData{
            label.text = some.groupName
        }else if let some = data as? Room{
            label.text = some.name
        }
    }
    func didChangeValue(sender:UISwitch){
        
    }
    
    @objc func masterImageTapped(sender:UITapGestureRecognizer){
        guard let header = self.headerView else {return}
        guard let group = self.data as? GroupedControlData else {return}
        guard shouldPerformCommand else {return}
        switch header.imageView.image!{
        case group.controlImage.0:
            //Turn all lights on
            header.imageView.image = group.controlImage.1
            performCommand(level:100)
        case group.controlImage.1:
            header.imageView.image = group.controlImage.0
            performCommand(level: 0)
        default:
            return
        }
        
    }
    @objc func masterControlChanged(sender:UIControl){
        
        if shouldPerformCommand{
            if let slider = sender as? UISlider{
                performCommand(level: slider.value)
            }else if let aSwitch = sender as? UISwitch{
                performCommand(outletOn: aSwitch.isOn)
            }
        }else{
            for cell in self.tableview.visibleCells{
                let smCell = cell as! SmartDeviceTableCell
                if let control = smCell.control as? UISlider{
                    control.value = (sender as! UISlider).value
                }else if let control = smCell.control as? UISwitch{
                    control.isOn = (sender as! UISwitch).isOn
                }
            }
            let devices = self.data as! GroupedControlData
            var newDevices:[CMQSmartDevice] = [CMQSmartDevice]()
            for var device in devices.data{
                if let control = sender as? UISlider{
                    device.device.level = control.value.rounded()
                }else if let control = sender as? UISwitch{
                    device.device.On = control.On
                }
                newDevices.append(device)
            }
            NotificationCenter.default.post(name: CMQDeviceManipController.DevicesUpdatedNotification, object: newDevices)
        }
        
    }
    func performCommand(outletOn:Bool){
        guard let data = data as? GroupedControlData else {return}
        var devices:[CMQSmartDevice] = [CMQSmartDevice]()
        for var device in data.data{
            device.device.On = outletOn
            devices.append(device)
        }
        let command = Command.init(devices: devices)
        NotificationCenter.default.post(name: CMQRoomViewController.lightUpdating, object: nil)
        command.execute { (error) in
            if let error = error{
                handle(error: error, message: "Error Updating Outlet", shouldDisplay: false)
            }else{
                self.updated(with: devices)
            }
        }
    }
    func performCommand(level:Float){
        if let data = data as? GroupedControlData{
            var devices:[CMQSmartDevice] = [CMQSmartDevice]()
            for var device in data.data{
                device.device.level = level.rounded()
                devices.append(device)
            }
            let command = Command.init(devices: devices)
            NotificationCenter.default.post(name: CMQRoomViewController.lightUpdating, object: nil)
            command.execute { (error) in
                if let error = error{
                    handle(error: error, message: "Error Updating Lights", shouldDisplay: false)
                }else{
                    self.updated(with: devices)
                    //print("Executed")
                }
            }
        }
    }
    func updated(with devices:[CMQSmartDevice]){
        DeviceUpdater().commandSent()
        NotificationCenter.default.post(name: CMQDeviceManipController.DevicesUpdatedNotification, object: devices)
        for room in CMQAPIManager.sharedInstance().smartAptUnit!.userRooms{
            if devices.first!.belongs(to: room){
                let currentIds = devices.compactMap({$0.identifier})
                var others = room.devices.filter({!currentIds.contains($0.identifier)})
                others.append(contentsOf: devices)
                SmartHomeDataSource.shared.cachedDevices[room.name] = others
            }
        }
    }
}
    

extension CMQSmartHomeCell:UITableViewDelegate, UITableViewDataSource{
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return numberOfDevicesForTableView
    }
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let dataForCell = self.data.data(at: indexPath) else {return UITableViewCell()}
        let cell = tableView.dequeueReusableCell(withIdentifier: dataForCell.tableCellIdentifier, for: indexPath) as! SmartDeviceTableCell
        cell.shouldPerformCommand = self.shouldPerformCommand
        cell.data = dataForCell
        cell.control.isEnabled = true
        return cell
    }
    public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let device = self.data.data(at: IndexPath.init(row: 0, section: section)), device.type != .Fan else {return nil}
        guard let header = MasterControlHeader.instanceFromNib(wSlider: device.type == .Light) else {return nil}
        self.headerView = header
        self.headerView?.controlView.isEnabled = true
        return header
    }
    public func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        guard let device = self.data.data(at: IndexPath.init(row: 0, section: section)), device.type != .Fan else {return 0}
        return 100
    }
}
extension SmartDeviceTableCell{
    var controlValue:Float{
        return (self.control as! UISlider).value
    }
}
protocol Enable{
    var enable:Bool {set get}
}
extension SmartDeviceTableCell:Enable{
    var enable: Bool {
        get {
            return self.control.isEnabled
        }
        set {
            self.control.isEnabled = newValue
        }
    }
}
extension MasterControlHeader:Enable{
    var enable: Bool {
        get {
            return self.controlView.isEnabled
        }
        set {
            self.controlView.isEnabled = newValue
        }
    }
}
extension CMQSmartHomeCell:Enable{
    var enable: Bool {
        get {
            guard self.lightGesture.isEnabled else {return false}
            guard self.headerView!.enable else {return false}
            return self.tableview.visibleCells.filter({($0 as! SmartDeviceTableCell).enable == false}).count == 0
        }
        set {
            self.headerView?.enable = newValue
            for cell in self.tableview.visibleCells as! [SmartDeviceTableCell]{
                cell.enable = newValue
            }
            self.lightGesture.isEnabled = newValue
        }
    }
}
extension CMQSmartHomeCell{
    func updateUI(with:CMQSmartDevice){
        
    }
    
    
}
