//
//  LightGroupCommandCell.swift
//  Communique
//
//  Created by Andre White on 7/24/18.
//  Copyright © 2018 Communique, LLC. All rights reserved.
//

import Foundation

public class LightGroupCommandCell:UICollectionViewCell,UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    @IBOutlet private var collectionView:UICollectionView!{
        didSet{
            collectionView.delegate = self
            collectionView.dataSource = self
        }
    }
    @IBOutlet private var gradientView:CMQGradientView!{
        didSet{
            gradientView.addShadow()
        }
    }
    @IBOutlet public var roomNameLabel:UILabel!
    public var data:CollectionViewData!{
        didSet{
            self.collectionView.reloadData()
            self.roomNameLabel?.text = (data as! Room).name
        }
    }
    @IBAction func didPressedDelete(sender:UIButton){
        NotificationCenter.default.post(name: CMQSmartHomeCell.DeletePresetNotification, object: self)
    }
    public func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (data as! Room).devices.count
    }
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let data = (self.data as! Room).devices.sorted(by: {$0.name>$1.name})[indexPath.row]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Reuse", for: indexPath) as! PresetRoomCell
        cell.device = data
        return cell
    }
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let padding = 10 * 2
        let width = (self.contentView.frame.width) - CGFloat(padding)
        return CGSize.init(width: width, height: 70)
    }
    
}
