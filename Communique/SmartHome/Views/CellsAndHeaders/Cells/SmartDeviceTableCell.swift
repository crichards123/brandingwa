//
//  SmartDeviceTableCell.swift
//  Communique
//
//  Created by Andre White on 8/6/18.
//  Copyright © 2018 Communique, LLC. All rights reserved.
//

import Foundation

public class SmartDeviceTableCell:UITableViewCell{
    public var shouldPerformCommand:Bool = true
    public var data:TableViewData!{
        didSet{
            self.updateImageView()
            self.updateLabel()
            self.updateControl()
        }
    }
    @IBOutlet var deviceImageView:UIImageView!
    @IBOutlet var control:UIControl!{
        didSet{
            switch control {
            default:
                self.control?.addTarget(self, action: #selector(controlChanged(sender:)), for: .valueChanged)
            }
        }
    }
    @IBOutlet var nameLabel:UILabel!
    override public func awakeFromNib() {
        
    }
    func updateImageView(){
        let tapGesture = UITapGestureRecognizer.init(target: self, action: #selector(imageTapped(sender:)))
        self.deviceImageView?.addGestureRecognizer(tapGesture)
    }
    @objc func imageTapped(sender:UIGestureRecognizer){
        
    }
    func updateLabel(){
        self.nameLabel?.text = data.data(at: IndexPath.init(row: 0, section: 0))?.name.capitalizingFirstLetter
    }
    @objc func controlChanged(sender:UIControl){
    
        if shouldPerformCommand{
            if let slider = sender as? UISlider{
                performCommand(level: slider.value)
            }else if let aSwitch = sender as? UISwitch{
                performCommand(outletOn: aSwitch.On)
            }else if let control = sender as? UISegmentedControl{
                performCommand(level: Float(control.fanLevel))
            }
            
        }else{
            var device = data as! CMQSmartDevice
            switch device.type{
            case .Light:
                device.device.level = (sender as! UISlider).value.rounded()
            case .Fan:
                device.device.fanLevel = (sender as! UISegmentedControl).fanLevel
            case .Outlet:
                device.device.On = (sender as! UISwitch).isOn
            default:
                return
            }
            updateImage()
            NotificationCenter.default.post(name: CMQDeviceManipController.DevicesUpdatedNotification, object: [device])
        }
        
    }
    
    func performCommand(outletOn:Bool){
        guard var some = data as? CMQSmartDevice else {return}
        guard case .Outlet(_) = some.device else {return}
        some.device = SmartDevice.Outlet(value: outletOn)
        let command = Command.init(devices: [some])
        NotificationCenter.default.post(name: CMQRoomViewController.lightUpdating, object: nil)
        command.execute { (error) in
            if let error = error{
                handle(error: error, message: "Error Updating Outlet", shouldDisplay: false)
            }else{
                self.updated(with: some)
            }
        }
    }
    func performCommand(level:Float){
        if let data = data as? CMQSmartDevice{
            var device = data
            switch data.type{
            case .Light:
                device.device.level = level.rounded()
            case .Fan:
                device.device.fanLevel = Int(level)
            default:
                return
            }
            let command = Command.init(devices: [device])
            NotificationCenter.default.post(name: CMQRoomViewController.lightUpdating, object: nil)
            command.execute { (error) in
                if let error = error{
                    handle(error: error, message: "Error Updating \(device.device.deviceTypeString)", shouldDisplay: false)
                }else{
                    self.updated(with: device)
                }
            }
        }
    }
    func updated(with device:CMQSmartDevice){
        DeviceUpdater().commandSent()
        NotificationCenter.default.post(name: CMQDeviceManipController.DevicesUpdatedNotification, object: [device])
        self.data = device
        updateImage()
        for room in CMQAPIManager.sharedInstance().smartAptUnit!.userRooms{
            if device.belongs(to: room){
                if let devices = SmartHomeDataSource.shared.cachedDevices[room.name] as? [CMQSmartDevice]{
                    var newDevices = devices.filter({$0.identifier != device.identifier})
                    newDevices.append(device)
                    SmartHomeDataSource.shared.cachedDevices[room.name] = newDevices
                }else{
                    var aRoom = room
                    aRoom.devices = room.devices.filter({$0.identifier != device.identifier})
                    aRoom.devices.append(device)
                    SmartHomeDataSource.shared.cachedDevices[room.name] = aRoom.devices
                }
                break
            }
        }
    }
    func updateImage(){
        let device = self.data as! CMQSmartDevice
        self.deviceImageView.image = device.device.controlImage
        self.setNeedsLayout()
    }
    func updateControl(){
        switch data {
        case is CMQSmartDevice:
            let device = data as! CMQSmartDevice
            deviceImageView?.image = device.device.controlImage
            switch device.device{
            case .Fan(let value):
                let aControl = control as! UISegmentedControl
                aControl.fanLevel = value
            case .Light(let level):
                let aControl = control as! UISlider
                aControl.value = level
            case .Outlet(let value):
                let aControl = control as! UISwitch
                aControl.On = value
            default:
                return
            }
        default:
            return
        }
    }
}
extension SmartDevice{
    var controlImage:UIImage?{
        switch self{
        case .Fan(let value):
            return value > 0 ? self.iconHighlighted : self.icon
        case .Light(let level):
            return level > 0 ? self.iconHighlighted : self.icon
        case .Outlet(let value):
            return value ? self.iconHighlighted : self.icon
        default:
            return nil
        }
    }
}
extension UISwitch:OutletProtocol{
    var On: Bool! {
        get {
            return self.isOn
        }
        set {
            self.isOn = newValue
        }
    }
    
    
}
extension SmartDevice{
    var fanLevelIndex:Int!{
        switch fanLevel {
        case 0:
            return 0
        case 25:
            return 1
        case 50:
            return 2
        case 100:
            return 3
        default:
            return nil
        }
    }
}
extension UISegmentedControl:FanProtocol{
    var fanLevel: Int! {
        get {
            switch self.selectedSegmentIndex{
            case 0:
                return 0
            case 1:
                return 25
            case 2:
                return 50
            case 3:
                return 100
            default:
                return nil
            }
        }
        set {
            switch newValue {
            case 0:
                self.selectedSegmentIndex = 0
            case 25:
                self.selectedSegmentIndex = 1
            case 50:
                self.selectedSegmentIndex = 2
            case 100:
                self.selectedSegmentIndex = 3
            default:
                return
            }
        }
    }
    
}
