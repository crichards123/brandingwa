//
//  CMQGradientButton.swift
//  Communique
//
//  Created by Andre White on 5/9/18.
//  Copyright © 2018 Communique, LLC. All rights reserved.
//

import UIKit
public class CMQGradientButton: UIButton, GradientProtocol {
    
    @IBInspectable public var gradientTopColor:UIColor!
    @IBInspectable public var gradientBottomColor:UIColor!
    @IBInspectable public var cornerRadius:NSNumber!
    internal var colors: [UIColor]{
        return [self.gradientTopColor, self.gradientBottomColor]
    }
    override open class var layerClass: AnyClass {
        return CAGradientLayer.classForCoder()
    }
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    override public func layoutSubviews() {
        super.layoutSubviews()
        self.layer.shadowPath = UIBezierPath.init(rect: self.bounds).cgPath
    }
    public override func awakeFromNib() {
        super.awakeFromNib()
        self.addGradient()
        self.addShadow()
    }

}
