//
//  CMQSmartHomeNavigationController.swift
//  Communique
//
//  Created by Andre White on 6/19/18.
//  Copyright © 2018 Communique, LLC. All rights reserved.
//

import UIKit

public class CMQSmartHomeNavigationController: UINavigationController,FloatingButton {
    @IBOutlet var floatingButton: UIButton!{
        didSet{
            let padding:CGFloat = 10.0
            let buttonWidth  = min(self.view.frame.width*(2/8), 75)
            let size = CGSize.init(width: buttonWidth, height: buttonWidth)
            let pos = CGPoint.init(x: self.view.frame.width - padding - buttonWidth , y: self.view.frame.height - (padding*2) - buttonWidth)
            floatingButton.frame = CGRect.init(origin: pos, size: size)
            floatingButton.layer.cornerRadius = buttonWidth / 2
            floatingButton.addTarget(self, action: #selector(buttonAction(sender:)), for: .touchUpInside)
            floatingButton.addShadow()
        }
    }
    
    override public func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    @objc public func buttonAction(sender:UIButton){
        switch self.topViewController {
        
        case is CMQCreatePresetViewController:
            let controller = topViewController as! CMQCreatePresetViewController
            switch controller.state!{
            case .Ready:
                controller.willChangeState(to: .Adding(devices: [CMQSmartDevice]()))
            case .Filled:
                controller.willChangeState(to: .Adding(devices: [CMQSmartDevice]()))
            default:
                return
            }
        case is CMQPresetsViewController:
            self.topViewController?.performSegue(withIdentifier: "AddPreset", sender: nil)
        case is CMQPinCodesViewController:
            (self.topViewController as! CMQPinCodesViewController).loadCodes()
            if let some =  CMQAPIManager.sharedInstance().smartAptUnit?.pinCodes["pincodes"] as? [[String : String]]{
                guard some.count < 5 else {return}
                self.topViewController?.performSegue(withIdentifier: "Create", sender: nil)
            }else{
                self.topViewController?.performSegue(withIdentifier: "Create", sender: nil)
            }
            
        default:
            return 
        }
        
    }
    override public func popViewController(animated: Bool) -> UIViewController? {
        if self.topViewController is CMQSmartHomeTableViewController {
            self.removeFloatingButton()
        }
        if self.topViewController is CMQPresetsViewController{
            self.removeFloatingButton()
        }
        if self.topViewController is CMQPinCodesViewController{
            self.removeFloatingButton()
        }
        return super.popViewController(animated: animated)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
