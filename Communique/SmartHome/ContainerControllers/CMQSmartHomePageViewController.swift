//
//  CMQSmartHomePageViewController.swift
//  Communique
//
//  Created by Andre White on 7/1/18.
//  Copyright © 2018 Communique, LLC. All rights reserved.
//

import UIKit

public class CMQSmartHomePageViewController: UIPageViewController {
    fileprivate lazy var pages: [UIViewController] = {
        return [self.getViewController(withIdentifier: "Lock"),
                self.getViewController(withIdentifier: "Thermostat"),
                self.getViewController(withIdentifier: "Rooms"),
        ]
    }()
    fileprivate func getViewController(withIdentifier identifier: String) -> UIViewController
    {
        return UIStoryboard(name: "SmartHome", bundle: nil).instantiateViewController(withIdentifier: identifier)
    }
}
