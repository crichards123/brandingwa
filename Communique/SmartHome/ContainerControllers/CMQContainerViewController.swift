//
//  CMQContainerViewController.swift
//  Communique
//
//  Created by Andre White on 7/1/18.
//  Copyright © 2018 Communique, LLC. All rights reserved.
//

import UIKit
public class ButtonCell:UICollectionViewCell{
    var button:ButtonType?
    
    @IBOutlet var buttonImageView:UIImageView!
    
    func toggleImage(selected:Bool){
        if selected{
            self.buttonImageView.image = button?.image
        }else{
            self.buttonImageView.image = button?.unhighlightedImage
        }
    }
}

public class CMQContainerViewController: UIViewController {
    fileprivate var pageController:CMQSmartHomePageViewController!{
        return self.children.first as? CMQSmartHomePageViewController
    }
    fileprivate var currentPage:UIViewController{
        return (self.pageController.viewControllers?.first)!
    }
    @IBOutlet var buttons: [UIButton]!
    @IBOutlet var imageViews:[UIImageView]!
    @IBOutlet weak var selectionView: UIView!
    @IBOutlet weak var collectionView:UICollectionView!
    public var selectedPath:IndexPath?
    fileprivate var changingPaths:(UICollectionViewCell,IndexPath)!
    public var initialPage:Int = -1
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        self.pageController.delegate = self
        self.pageController.dataSource = self
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        //self.collectionView.contentInset = UIEdgeInsets.init(top: 0, left: 20, bottom: 0, right: 20)
        if let page = Page.init(rawValue: initialPage){
            self.pageController.setViewControllers([page.viewController], direction: .forward, animated: true, completion: { (completed) in
                if completed{
                    self.completedChange(to: page)
                }
            })
        }
    }
    
    @IBAction func buttonPressed(_ sender: UIButton) {
        guard let page = Page.init(rawValue: sender.tag) else {return}
        guard let curPage = Page.init(controller: self.currentPage) else {return}
        change(to: page, from: curPage)
        
    }
    fileprivate func change(to:Page, from:Page){
        guard to != from else {return}
        self.pageController.setViewControllers([to.viewController], direction: from.rawValue <= to.rawValue ? .forward:.reverse, animated: true, completion:{ (completed) in
            if completed{
                self.completedChange(to:to, from:from)
            }
        })
    }
    
    fileprivate func completedChange(to:Page, from:Page? = nil){
        var paths = [IndexPath]()
        let indexOfTo = Page.allPages.firstIndex(of: to)!
        let toPath = IndexPath.init(row: indexOfTo, section: 0)
        paths.append(toPath)
        if let from = from{
            let indexOfFrom = Page.allPages.firstIndex(of: from)!
            let fromPath = IndexPath.init(row: indexOfFrom, section: 0)
            paths.append(fromPath)
        }
        self.collectionView.reloadData()
        //self.collectionView.reloadItems(at: paths)
        
    }

    fileprivate func imageView(for page:Page)->UIImageView{
        return imageViews.filter({$0.tag==page.rawValue}).first!
    }
    fileprivate func button(for page:Page)->UIButton{
        return buttons.filter({$0.tag==page.rawValue}).first!
    }
    fileprivate func viewOrigin(for button:UIButton)->CGPoint{
        return CGPoint.init(x: button.frame.origin.x, y: button.frame.origin.y + button.frame.size.height)
    }
    fileprivate enum Page:Int{
        static var allPages:[Page] {
            var returnable = [Page]()
            if let _ = CMQAPIManager.sharedInstance().smartAptUnit?.doorLock{
                returnable.append(.Lock)
            }
            if let _ = CMQAPIManager.sharedInstance().smartAptUnit?.thermostat{
                returnable.append(.Thermostat)
            }
            if let rooms = CMQAPIManager.sharedInstance().smartAptUnit?.userRooms{
                if rooms.count > 0{
                    returnable.append(.Room)
                }
                
            }
            return returnable.sorted(by: {$0.rawValue < $1.rawValue})
        
        }
        case Lock = 0
        case Thermostat
        case Room
        init?(button:ButtonType){
            switch button{
                
            case .Lock:
                self = .Lock
            case .Rooms:
                self = .Room
            case .Thermostat:
                self = .Thermostat
            case .Presets:
                return nil
            case .PinCode:
                return nil
            }
        }
        var index:Int?{
            return Page.allPages.firstIndex(of: self)
        }
        var next:Page?{
            guard let index = self.index else {return nil}
            guard Page.allPages.index(after: index) < Page.allPages.count else {return nil}
            return Page.allPages[Page.allPages.index(after: index)]
        }
        var prev:Page?{
            guard let index = self.index else {return nil}
            guard index != 0 else {return nil}
            return Page.allPages[Page.allPages.index(before: index)]
        }
        var viewController:UIViewController{
            var controller:UIViewController
            switch self {
            case .Lock:
                controller = UIStoryboard(name: "SmartHome", bundle: nil).instantiateViewController(withIdentifier: "Lock")
               
            case .Thermostat:
                controller = UIStoryboard(name: "SmartHome", bundle: nil).instantiateViewController(withIdentifier: "Thermostat")
            case .Room:
                controller =  UIStoryboard(name: "SmartHome", bundle: nil).instantiateViewController(withIdentifier: "Rooms")
            }
            (controller.view as! CMQGradientView).gradientTopColor = .clear
            (controller.view as! CMQGradientView).gradientBottomColor = .clear
            return controller
        }
        init?(controller:UIViewController){
            switch controller {
            case is CMQSmartHomeThermostatViewController:
                self = .Thermostat
            case is CMQSmartHomeDoorLockViewController:
                self = .Lock
           case is CMQRoomSelectViewController:
                self = .Room
            default:
                return nil
            }
        }
    }
}
extension CMQContainerViewController:UIPageViewControllerDataSource{
    
    public func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let page = Page.init(controller: viewController) else {return nil}
        return page.prev?.viewController
    }

    public func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let page = Page.init(controller: viewController) else {return nil}
        return page.next?.viewController
    }
}
extension CMQContainerViewController:UIPageViewControllerDelegate{
    public func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if completed{
            guard let curPage = Page.init(controller: self.currentPage) else {return}
            guard let prevPage = Page.init(controller: previousViewControllers.first!) else {return}
            self.completedChange(to: curPage, from:prevPage)
        }
    }
    
    public func pageViewController(_ pageViewController: UIPageViewController, willTransitionTo pendingViewControllers: [UIViewController]) {

    }
}
extension CMQContainerViewController:UICollectionViewDelegateFlowLayout{
    
    public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let page = Page.allPages[indexPath.row]
        guard let curPage = Page.init(controller: self.currentPage) else {return}
        change(to: page, from: curPage)
        let cell = collectionView.cellForItem(at: indexPath) as? ButtonCell
        cell?.toggleImage(selected: true)
       // self.handleCellImage(cell: cell, indexPath: indexPath)
        
    }
    public func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as? ButtonCell
        cell?.toggleImage(selected: false)
    }
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = collectionView.frame.size
        let itemNum = Page.allPages.count
        let width = (size.width/CGFloat(itemNum))-10
        return CGSize(width: width, height: size.height)
    }
}
extension CMQContainerViewController:UICollectionViewDataSource{
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let page = Page.allPages[indexPath.row]
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "reuse", for: indexPath) as! ButtonCell
        cell.button = ButtonType.init(page:page)
        let curPage = Page.init(controller: self.currentPage)!
        cell.toggleImage(selected: curPage == page)
        return cell

    }
    public func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return Page.allPages.count
    }

    fileprivate func handleCellImage(cell:UICollectionViewCell, indexPath:IndexPath){
        //let imageView = cell.viewWithTag(999) as? UIImageView
        //let data = SmartHomeDataSource.shared.button(at: indexPath)!
        /*let page = Page.init(rawValue: indexPath.row)
        let curPage = Page.init(controller: self.currentPage)
        imageView?.image = page == curPage ? data.image : data.unhighlightedImage*/
        //imageView?.image = indexPath == selectedPath ? data.image : data.unhighlightedImage
    }
    
}
extension ButtonType{
    fileprivate init(page:CMQContainerViewController.Page) {
        switch page{
            
        case .Lock:
            self = .Lock
        case .Thermostat:
            self = .Thermostat
        case .Room:
            self = .Rooms
        }
    }
}
