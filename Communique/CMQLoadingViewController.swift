//
//  CMQLoadingViewController.swift
//  Communique
//
//  Created by Andre White on 4/22/18.
//  Copyright © 2018 Communique, LLC. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

public class CMQLoadingViewController: UIViewController {
    @IBOutlet weak var activityView: NVActivityIndicatorView!
    

    
    @IBOutlet weak var promptLabel: UILabel!
    open var promptText:String!
    override public func viewDidLoad() {
        super.viewDidLoad()
        initView()
    }
    public func initView(){
        
        activityView?.startAnimating()
        promptLabel.text = promptText
    }
}
