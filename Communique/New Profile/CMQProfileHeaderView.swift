//
//  CMQProfileHeaderView.swift
//  Communique
//
//  Created by Andre White on 4/3/18.
//  Copyright © 2018 Communique, LLC. All rights reserved.
//

import UIKit
import ParseUI
public class CMQProfileHeaderView: UIView {
    @IBOutlet weak var imageView:CMQImageView!
    @IBOutlet weak var nameLabel:UILabel!
    @IBOutlet weak var emailLabel:UILabel!
    @IBOutlet weak var phoneLabel:UILabel!
    @IBOutlet weak var editButton:UIButton!
    public weak var user:CMQUser?{
        didSet{
            if let aUser = user{
                nameLabel?.text = "\(aUser.firstName!) \(aUser.lastName!)"//aUser.fullName
                emailLabel?.text = aUser.email
                phoneLabel?.text = aUser.phone
                imageView?.file = aUser.userPhoto
                imageView.loadInBackground()
            }
        }
    }
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
}
