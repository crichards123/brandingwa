//
//  CMQEditProfileViewController.swift
//  Communique
//
//  Created by Andre White on 1/24/18.
//  Copyright © 2018 Communique, LLC. All rights reserved.
//

import UIKit
import CropViewController
public class CMQEditProfileViewController: UITableViewController, CropViewControllerDelegate {
    @IBOutlet var headerView: CMQProfileHeaderView!
    private var pickerDelegate:CMQPhotoPickerDelegate?{
        didSet{
            guard let delegate = pickerDelegate else {return}
            delegate.photoCompletionBlock = {
                (chosenPhoto: UIImage?)->Void in
                guard let image = chosenPhoto else {return}
                self.dismiss(animated: true, completion: {
                    let cropViewController = CropViewController(image: image)
                    cropViewController.delegate = self
                    self.present(cropViewController, animated: true, completion: nil)
                })
            }
        }
    }
    //MARK: - Crop ViewController
    public func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        self.dismiss(animated: true) {
            self.photo = image
            self.tableView.reloadData()
        }
        
    }
    public var photo: UIImage?{
        didSet{
            guard let _ = photo else {return}
            self.tableView.reloadData()
        }
    }
    @IBOutlet weak var profilePic: CMQImageView!{
        didSet{
            guard let view = profilePic else {return}
            view.file = CMQUser.current().userPhoto
            view.loadInBackground()
        }
    }
    @IBOutlet weak var lastNameCell: CMQEditFieldCell!
    @IBOutlet weak var firstNameCell: CMQEditFieldCell!
    @IBOutlet weak var emailCell: CMQEditFieldCell!
    @IBOutlet weak var phoneCell: CMQEditFieldCell!
    override public var preferredStatusBarStyle: UIStatusBarStyle{
        return .default
    }
    private var allFields:[UIView]{
        let  fields:[CMQEditFieldCell] = [firstNameCell, lastNameCell, phoneCell, emailCell]
        return fields.map({$0.textField}).sorted(by: {$0.tag<$1.tag})
    }
    public var changes:[String:String] = [String:String]()
   
    func resignResponder(){
        let  fields = [firstNameCell, lastNameCell, phoneCell, emailCell]
        if let firstResponder = fields.filter({ (cell) -> Bool in
            if let cell = cell{
                return cell.textField.isFirstResponder
            }else{
                return false
            }
        }).first{
            firstResponder?.textField.resignFirstResponder()
        }
    }
    override public func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //TODO:Update StatusBar
        UIApplication.shared.statusBarStyle = .default
    }
    override public func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        //TODO:Update StatusBar
        UIApplication.shared.statusBarStyle = .lightContent
        NotificationCenter.default.removeObserver(self, name: .prevPressed, object: nil)
        NotificationCenter.default.removeObserver(self, name: .nextPressed, object: nil)
    }
    override public func viewDidLoad() {
        super.viewDidLoad()
        pickerDelegate = CMQPhotoPickerDelegate.init(controller: self, wEditing: false)
        NotificationCenter.default.addObserver(self, selector: #selector(handleReturnPressed(sender:)), name: .prevPressed, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleReturnPressed(sender:)), name: .nextPressed, object: nil)
        self.tableView.contentInset = UIEdgeInsets.init(top: 5, left: 0, bottom: 0, right: 0)
        
    }
    @objc public func handleReturnPressed(sender:Notification){
        if sender.name == .prevPressed{
            self.prevField()
        }else if sender.name == .nextPressed{
            self.nextField()
        }
    }
    @IBAction func showPhotoController(sender:UIButton){
        guard let delegate = pickerDelegate else {return}
        self.resignResponder()
        if (UIDevice.current.userInterfaceIdiom == .pad){
            delegate.popOverView = sender
        }
        self.present(delegate.alertController, animated: true, completion: nil)
    }
    @IBAction public func saveButtonPressed(sender:UIButton){
        let  fields = [firstNameCell, lastNameCell, phoneCell, emailCell]
        if fields.filter({$0!.didChange}).count > 0 || photo != nil{
            if let image = photo {
                CMQUser.current().userPhoto = image.toParse()
            }
            CMQUser.current().saveInBackground(block: { (completed, error) in
                if let error = error{
                    handle(error: error, message: "Updating User", shouldDisplay: true)
                }else if completed{
                    self.performSegue(withIdentifier: "Save", sender: nil)
                }
            })
        }
    }
    override public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let cell = tableView.cellForRow(at: indexPath) as? CMQEditFieldCell else {return}
        cell.textField.becomeFirstResponder()
    }
    @IBAction func textFieldDidChange(_ sender: CMQTextField) {
        let  fields = [firstNameCell, lastNameCell, phoneCell, emailCell]
        guard let cell = fields.filter({$0?.textField == sender}).first as? CMQEditFieldCell else{return}
        changes[cell.fieldType] = cell.editingFinished(sender)
    }
    
    override public func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.y<0{
            scrollView.contentOffset.y = 0
        }
    }
    
    public func prevField(){
        if let firstResponder = allFields.filter({$0.isFirstResponder}).first{
            var field:UIView?
            if let index = allFields.index(of: firstResponder){
                if index == allFields.startIndex {
                    field = allFields[allFields.endIndex-1]
                }else{
                    field = allFields[allFields.index(before: index)]
                }
            }
            field?.becomeFirstResponder()
        }
    }
    public func nextField(){
        if let firstResponder = allFields.filter({$0.isFirstResponder}).first{
            var field:UIView?
            if let index = allFields.index(of: firstResponder){
                if index == allFields.endIndex-1 {
                    field = allFields[allFields.startIndex]
                }else{
                    field = allFields[allFields.index(after: index)]
                }
            }
            field?.becomeFirstResponder()
        }
    }
    public override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        guard section == 0 else {return 5}
        return self.view.frame.height*0.28
    }
    override public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let header = headerView, section == 0 else {return nil}
        if let image = photo{
            header.imageView.image = image
        }else{
            header.user = CMQUser.current()
        }
        header.imageView.layer.cornerRadius = (header.imageView.frame.height-5)/2
        return header
        
    }
    

}

