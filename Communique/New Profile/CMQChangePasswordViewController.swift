//
//  CMQChangePasswordViewController.swift
//  Communique
//
//  Created by Andre White on 3/12/18.
//  Copyright © 2018 Communique, LLC. All rights reserved.
//

import UIKit

public class CMQChangePasswordViewController: CMQParentViewController {
    @IBOutlet weak var newPasswordField: UITextField!
    @IBOutlet weak var confirmationField: UITextField!
    override public var preferredStatusBarStyle: UIStatusBarStyle{
        return .default
    }
    override public func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override public func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override public func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //TODO:Update
        UIApplication.shared.statusBarStyle = .default
    }
    override public func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        //TODO:Update
        //[UIViewController preferredStatusBarStyle]
        UIApplication.shared.statusBarStyle = .lightContent
    }
    private func fieldsMatch()->Bool{
        return newPasswordField.text == confirmationField.text
    }
    @IBAction public func saveButtonPressed(sender: UIButton){
        guard  let user = CMQUser.current() else {return}
        if fieldsMatch(){
            if newPasswordField.text?.replacingOccurrences(of: " ", with: "") != "" {
                user.password = newPasswordField.text
                performSegue(withIdentifier: "Save", sender: nil)
            }
        }else{
            print("Fields Don't match")
            CMQAPIManager.showError(message: "Passwords do not match")
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
