//
//  CMQEditFieldCell.swift
//  Communique
//
//  Created by Andre White on 1/24/18.
//  Copyright © 2018 Communique, LLC. All rights reserved.
//

import UIKit

public class CMQEditFieldCell: UITableViewCell {
    public var didChange = false
    @IBInspectable public var fieldType:String!
    @IBOutlet weak var textField: UITextField!{
        didSet{
            guard let field = textField else {return}
            field.delegate = self
            switch fieldType {
            case "Last Name":
                field.text = CMQUser.current().lastName
            case "First Name":
                field.text = CMQUser.current().firstName
            case "Email":
                field.text = CMQUser.current().email
            case "Phone":
                field.text = CMQUser.current().phone
            default:
                print("Unknown Case")
            }
        }
    }
}
extension CMQEditFieldCell: UITextFieldDelegate{
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let charSet = CharacterSet.init(charactersIn: string)
        switch fieldType {
        case "Phone":
            let str = (textField.text! + string)
            if str.count <= 10 {
                return true
            }
            textField.text = String(str[str.startIndex...str.index(str.startIndex, offsetBy: 10)])//str.substring(to: str.index(str.startIndex, offsetBy: 10))
            return false
            /*if range.location == 10{
                return false
            }
            let allowed = CharacterSet.decimalDigits
            return allowed.isSuperset(of: charSet)*/
        
        case "Email":
            if range.length == 1{
                return true
            }
            let notAllowed = CharacterSet.whitespacesAndNewlines
            return !notAllowed.isSuperset(of: charSet)
        default:
            return true
        }
    }
    public func editingFinished(_ textField: UITextField)->String? {
        guard let text = textField.text else {return nil}
        switch fieldType {
        case "Last Name":
            if text.replacingOccurrences(of: " ", with: "") != CMQUser.current().lastName{
                didChange = true
            }else{
                didChange = false
            }
        case "First Name":
            if text.replacingOccurrences(of: " ", with: "") != CMQUser.current().firstName{
                didChange = true
            }else{
                didChange = false
            }
        case "Email":
            if !text.isValidEmail{
                CMQAPIManager.showError(message: "Please enter a valid email")
                return nil
            }
            if text.replacingOccurrences(of: " ", with: "") != CMQUser.current().email{
                didChange = true
            }else{
                didChange = false
            }
        case "Phone":
            if !text.isValidPhone{
                CMQAPIManager.showError(message: "Please Enter a valid phone number")
                return nil
            }
            if text.replacingOccurrences(of: " ", with: "") != CMQUser.current().phone{
                didChange = true
            }else{
                didChange = false
            }
        default:
            print("Unknown Case")
        }
        return textField.text
    }
}
