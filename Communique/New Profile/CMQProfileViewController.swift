//
//  CMQProfileViewController.swift
//  Communique
//
//  Created by Andre White on 1/18/18.
//  Copyright © 2018 Communique, LLC. All rights reserved.
//

import UIKit
import ParseUI
@available(iOS 10.0, *)
public class CMQProfileViewController: UITableViewController {
    @IBOutlet var headerView: CMQProfileHeaderView!
    @IBOutlet weak var profileImageView: PFImageView!{
        didSet{
            guard let view = profileImageView else {return }
            view.file = CMQUser.current().userPhoto
            view.loadInBackground()
        }
    }
    @IBOutlet weak var phoneLabel: UILabel!{
        didSet{
            guard let label = phoneLabel else {return}
            label.text = CMQUser.current().phone
        }
    }
    @IBOutlet weak var emailLabel: UILabel!{
        didSet{
            guard let label = emailLabel else {return}
            label.text = CMQUser.current().email
        }
    }
    @IBOutlet weak var nameLabel: UILabel!{
        didSet{
            guard let label = nameLabel else {return}
            label.text = CMQUser.current().fullName
        }
    }
    @IBOutlet weak var passwordLabel: UILabel!{
        didSet{
            guard let _ = passwordLabel else {return}
            //label.text = CMQUser.current().password
        }
    }
    @IBOutlet weak var communityLabel: UILabel!{
        didSet{
            guard let label = communityLabel, let delegate = UIApplication.shared.delegate as? CMQAppDelegate, let apartment = delegate.apartment else{return}
            label.text = apartment.aptName
        }
    }
    @IBOutlet weak var urgentMsgNotifications: UISwitch!{
        didSet{
            guard let urgent = urgentMsgNotifications, let user = CMQUser.current() else {return}
            urgent.isOn = user.texts && user.phone.count > 0
        }
    }
    @IBOutlet weak var emailNotifications: UISwitch!{
        didSet{
            guard let email = emailNotifications, let user = CMQUser.current() else {return}
            email.isOn = user.alerts && user.email.count > 0
        }
    }
    @IBOutlet weak var pushSettings: UISegmentedControl!{
        didSet{
            guard let control = pushSettings  else {return}
            switch CMQPushNotificationHandler.sharedInstance().userPushSetting  {
            case .some(.All):
                control.selectedSegmentIndex = 0
            case .some(.UrgentOnly):
                control.selectedSegmentIndex = 1
            default:
                control.selectedSegmentIndex = 2
            }
        }
    }
    override public func viewDidLoad() {
        super.viewDidLoad()
        CMQPushNotificationHandler.sharedInstance().checkPushPermissions { (setting) in
            CMQPushNotificationHandler.sharedInstance().userPushSetting = setting
            DispatchQueue.main.async {
                self.pushSettings.selectedSegmentIndex = setting.viewTag
            }
            
        }
    }
    
    
    @IBAction func saveChanges(sender:UIStoryboardSegue){
        guard let _ = CMQUser.current() else {return}
        guard let editVC = sender.source as? CMQEditProfileViewController else {return}
        self.updateUser(updateDict: editVC.changes)
        //CMQAPIManager.showFeedback(message: "User Updated")
        
    }
    public func updateUser(updateDict:[String:String]){
        guard let user = CMQUser.current() else {return}
        if let first = updateDict["First Name"]{
            user.firstName = first
        }
        if let last = updateDict["Last Name"]{
            user.lastName = last
        }
        if let phone = updateDict["Phone"]{
            user.phone = phone
        }
        if let email = updateDict["Email"]{
            user.username = email
            user.email = email
        }
        user.saveInBackground { (success, error) in
            guard error == nil else {CMQAPIManager.showError(message:"Error Updating User"); return }
            if success{
                CMQAPIManager.showFeedback(message: "User Updated")
            }
        }
        self.tableView.reloadData()
    }
    @IBAction func cancelChanges(sender:UIStoryboardSegue){
        
    }
    @IBAction func didSwitch(sender:UISwitch){
        guard let user = CMQUser.current() else { return}
        switch sender {
        case urgentMsgNotifications:
            if user.phone.count > 0{
                user.texts = sender.isOn
            }else{
                //Alert: Please enter a phone number to allow text messages.
                CMQAPIManager.showError(message: "Please enter a phone number to allow text messages.")
            }
        case emailNotifications:
            if user.email.count > 0{
                user.alerts = sender.isOn
            }else{
                //Alert: Please enter an email address to allow emails.
                CMQAPIManager.showError(message: " Please enter an email address to allow emails")
            }
        default:
            print("unkown sender")
        }
        user.saveInBackground()
        
    }
    @IBAction func pushSettingsChanged(_ sender: UISegmentedControl) {
        guard let setting = CMQPushNotificationHandler.sharedInstance().userPushSetting else {return}
        guard setting != .Denied else {CMQAPIManager.showError(message: "Please Allow Push Notifications in settings.");sender.selectedSegmentIndex = CMQPushNotificationHandler.sharedInstance().userPushSetting!.viewTag; return}
        CMQPushNotificationHandler.sharedInstance().userPushSetting = CMQPushSetting.setting(viewTag: sender.selectedSegmentIndex)

    }
    override public func scrollViewDidScroll(_ scrollView: UIScrollView) {
        guard let height = self.navigationController?.navigationBar.frame.height else {return}
        if scrollView.contentOffset.y<(-1)*(height+UIApplication.shared.statusBarFrame.height){
            scrollView.contentOffset.y = (-1)*(height+UIApplication.shared.statusBarFrame.height)
        }
    }
    public override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        guard section == 0 else {return 10}
        return self.view.frame.height*0.30
    }
    override public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let header = headerView, section == 0 else {return nil}
        header.user = CMQUser.current()
        header.backgroundColor = .DarkColor
        return header
        
    }
   
}
extension CMQPushSetting{
    fileprivate static func setting(viewTag:Int)->CMQPushSetting{
        switch viewTag {
        case 0:
            return .All
        case 1:
            return .UrgentOnly
        default:
            return .None
        }
    }
    fileprivate var viewTag:Int{
        switch self {
        case .All:
            return 0
        case .UrgentOnly:
            return 1
        default:
            return 2
        }
    }
}
