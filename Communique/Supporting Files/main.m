//
//  main.m
//  Communique
//
//  Created by Chris Hetem on 9/15/14.
//  Copyright (c) 2014 WaveRider, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([CMQAppDelegate class]));
    }
}
