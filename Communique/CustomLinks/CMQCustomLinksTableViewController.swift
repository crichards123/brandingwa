//
//  CMQCustomLinksTableViewController.swift
//  Communique
//
//  Created by Andre White on 3/27/18.
//  Copyright © 2018 Communique, LLC. All rights reserved.
//

import UIKit

public class CMQCustomLinksTableViewController: UITableViewController {
    private var links = CMQAPIManager.sharedInstance().customLinks
    
    //MARK: - Life cycle
    override public func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.tableFooterView = UIView()
     }

    // MARK: - Table view data source

    public override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    public override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let links = links else {return 0}
        return links.count
    }

    public override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "linkCell", for: indexPath)
        guard let links = links else {return UITableViewCell()}
        let link = links[indexPath.row];
        cell.textLabel?.text = link["title"]
        cell.detailTextLabel?.text = link["url"]
        return cell
    }
    override public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let links = links else {return}
        let link = links[indexPath.row]
        guard let urlString = link["url"], let _ = URL.init(string: urlString) else {return}
        performSegue(withIdentifier: "WebView", sender: link)
        
    }
    //MARK: - Navigation
    public override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "WebView"{
            if let dest = segue.destination as? CMQWebViewController{
                if let link = sender as? [String:String]{
                    guard let urlString =  link["url"], let url = URL.init(string: urlString) else {
                        CMQAPIManager.showError(message: "Unable to load URL"); return}
                    dest.url = url
                    dest.navTitle = link["title"]
                }
            }
        }
    }

}
