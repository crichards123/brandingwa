//
//  CMQLoginViewController.swift
//  Communique
//
//  Created by Andre White on 1/8/18.
//  Copyright © 2018 Communique, LLC. All rights reserved.
//


import UIKit
import WebKit
import MessageUI
/**
 This ViewController controls the login main view.
 Conforms to:
 - MFMailComposeViewControllerDelegate: If help button is pressed then the email view comes for email help.
 
 Observes:
 - UIKeyboardWillShow
    - Raise view with keyboard
 - UIKeyboardWillHide
    - lower view with keyboard
 
 Ways in:
 - When the application starts, if a user is not logged in they will start at this screen
 - BackToLogin: ExitSegue, If the user logs out, they will be brought back to this page.
    - Identifier:
 
 Ways Out:
 - Successful Login: Segue to Home VC
    - Identifier: Login
 - Forgot Password
 
 Actions:
 - login
 - showPrivacyPolicy
 - showTermsOfService
 - showHelp
 
 Outlets:
    - emailField
    - passwordField
 */
public class CMQLoginLandingViewController: CMQParentViewController, MFMailComposeViewControllerDelegate {
    @IBOutlet private var emailField: UITextField!{
        didSet{
            if #available(iOS 10.0, *) {
                emailField?.textContentType = UITextContentType(rawValue: "")
            } else {
                // Fallback on earlier versions
            }
        }
    }
    @IBOutlet private var passwordField: UITextField!{
        didSet{
            if #available(iOS 10.0, *) {
                passwordField?.textContentType = UITextContentType(rawValue: "")
            } else {
                // Fallback on earlier versions
            }
        }
    }
    private var keyBoardSize: CGFloat?
    private var mailComposerVC : MFMailComposeViewController!{
        let controller = MFMailComposeViewController()
        controller.mailComposeDelegate = self
        controller.setMessageBody(String.init(format: "\n\n\n\n\n\nSent from the %@ App %@\nA Communiqué Product", Bundle.main.localizedInfoDictionary!["CFBundleDisplayName"] as! CVarArg,UIApplication.versionAndBuild()), isHTML: false)
        controller.setToRecipients([kLoginSupportEmail])
        return controller
    }
    private var tosRequest :URLRequest?{
        guard let tosUrl = URL(string: kTermsOfServiceURL) else{ return nil}
        return URLRequest(url:tosUrl)
    }
    private var privPolRequest :URLRequest?{
        guard let privPolUrl = URL(string: kPrivacyPolicyURL) else{ return nil}
        return URLRequest(url:privPolUrl)
    }
    override public func viewDidLoad() {
        super.viewDidLoad()
        
    
        //addObservers()
    }
    override public func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        emailField?.text = ""
        passwordField?.text = ""
        removeObservers()
        self.timer?.invalidate()
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    override public func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setToolbarHidden(true, animated: false)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        if (CMQUser.current() != nil){
           let block =  CMQAPIManager.sharedInstance().loginCompletion(controller: self)
            block(CMQUser.current(),nil)
        }
        self.runTimer()
        if !loginButton.isEnabled{
            loginButton.isEnabled = true
        }
        addObservers()
        super.viewWillAppear(animated)
    }
    @IBOutlet weak var loginButton: UIButton!
    override public var preferredStatusBarStyle: UIStatusBarStyle{
        return .lightContent
    }
    override public func viewDidAppear(_ animated: Bool) {
        super .viewDidAppear(animated)
        if let firstResponder = textFields.filter({$0.isFirstResponder}).first{
            firstResponder.resignFirstResponder()
        }
        //TODO:Update statusBarStyle
        UIApplication.shared.statusBarStyle = .lightContent
    }
    override public func showWebView(request: URLRequest, frame: CGRect?, navTitle: String?) {
        super.showWebView(request: request, frame: frame, navTitle: navTitle)
        self.addDoneNavButton()
    }
    public func addObservers(){
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    public func removeObservers(){
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    public func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        self.dismiss(animated: true, completion: nil)
    }
    public func presentMailController(){
        self.present(mailComposerVC, animated: true, completion: nil)
    }
    public func runTimer(){
        if #available(iOS 10.0, *) {
            self.timer = Timer.scheduledTimer(withTimeInterval: 3, repeats: true, block: { (_) in
                self.carouselNext()
            })
        } else {
            // Fallback on earlier versions
        }
    }
    @IBAction public func login(){
        guard let email = emailField.text?.replacingOccurrences(of: " ", with: ""), let password = passwordField.text else{
            CMQAPIManager.showError(message: "Username and Password required");
            return }
        if let first = textFields.filter({$0.isFirstResponder}).first{
            first.resignFirstResponder()
        }
        self.loginButton.isEnabled = false
        CMQUser.logInWithUsername(inBackground: email, password: password, block: CMQAPIManager.sharedInstance().loginCompletion(controller: self))
    }
    
    @IBAction public func showHelp(sender:Any?){
        guard MFMailComposeViewController.canSendMail() == true else{
            CMQAPIManager.showError(message: "To use this feature, please set up email on this device");
            return}
        self.presentMailController()
    }
    @IBAction public func backToLogin(sender:UIStoryboardSegue){
        //addObservers()
    }
    @IBAction public func showPrivacyPolicy(sender:Any){
        guard let privPolRequest = self.privPolRequest, let imageView = self.view.viewWithTag(300) as? UIImageView else {return}
        timer?.invalidate()
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        let navBarHeight = self.navigationController?.navigationBar.frame.size.height ?? CGFloat(35)
        let rect = CGRect.init(origin: imageView.frame.origin, size: CGSize.init(width: imageView.frame.width, height: imageView.frame.height-navBarHeight+8))//Still about 8 pixels off
        self.showWebView(request: privPolRequest, frame: rect, navTitle: "Privacy Policy")
    }
    @IBAction public func showTermsOfService(sender:Any){
        guard let tosRequest = self.tosRequest, let imageView = self.view.viewWithTag(300) as? UIImageView else {return}
        timer?.invalidate()
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        let navBarHeight = self.navigationController?.navigationBar.frame.size.height ?? CGFloat(35)
        let rect = CGRect.init(origin: imageView.frame.origin, size: CGSize.init(width: imageView.frame.width, height: imageView.frame.height-navBarHeight+8))//Still about 8 pixels off
        self.showWebView(request: tosRequest, frame: rect, navTitle: "Terms Of Service")
    }
    override public func removeWebView() {
        super.removeWebView()
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        self.runTimer()
    }
    @objc public func keyboardWillShow(notification: NSNotification) {
        if keyBoardSize == nil{
            keyBoardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.height
        }
        guard self.view.transform == .identity else {return}
        self.view.transform = CGAffineTransform.init(translationX: 0, y: -keyBoardSize!)
    }
    @objc public func keyboardWillHide(notification: NSNotification) {
        guard self.view.transform != .identity else {return}
        self.view.transform = .identity
    }
    public override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
            
    }
    
}
