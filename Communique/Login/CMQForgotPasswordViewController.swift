//
//  CMQForgotPasswordViewController.swift
//  Communique
//
//  Created by Andre White on 1/17/18.
//  Copyright © 2018 Communique, LLC. All rights reserved.
//

import UIKit

public class CMQForgotPasswordViewController: CMQParentViewController {
    @IBOutlet private var emailField:UITextField!
    override public var preferredStatusBarStyle: UIStatusBarStyle{
        return .default
    }
    override public func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //TODO:Update statusBarStyle
        UIApplication.shared.statusBarStyle = .default
    }
    override public func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        //TODO:Update statusBarStyle
        UIApplication.shared.statusBarStyle = .lightContent
    }
    override public func viewDidLoad() {
        super.viewDidLoad()
    }
    @IBAction public func resetPassword(){
        guard let email = emailField.text?.replacingOccurrences(of: " ", with: "") else {return}
        guard email.isValidEmail else {CMQAPIManager.showError(message: "Please Enter A Valid Email");return}
        PFUser.requestPasswordResetForEmail(inBackground: email) { (succeed, error) in
            if let error = error{
                handle(error: error, message: "User \(email) not found", shouldDisplay: true)
            }
            else if succeed {
                showSuccess(message: "A password email has been sent to \(email)")
                self.performSegue(withIdentifier: "Exit", sender: nil)
            }
        }
    }
}
