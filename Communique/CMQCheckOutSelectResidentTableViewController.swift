//
//  CMQCheckOutSelectResidentTableViewController.swift
//  Communique
//
//  Created by Andre White on 6/4/18.
//  Copyright © 2018 Communique, LLC. All rights reserved.
//

import UIKit
struct CheckOutCellCreator:IDTableCellConfigureProtocol, IDTableViewCellCreatorProtocol{
    typealias ViewType = UITableView
    
    typealias CellType = UITableViewCell
    var dataSource: IDViewDataSourceProtocol
    var reuseIdentifier: String{
        return "PackageCell"
    }
    func configure(cell: UITableViewCell, atIndex: IndexPath) {
        guard let cell = cell as? CMQPackageDetailsTableViewCell else {return}
        guard let resident = dataSource.object(at: atIndex) as? CMQUser else {return}
        cell.reset()
        let completion = { (packages:[CMQPackage]?, error:Error?) in
            if let error = error as NSError? {
                handle(error: error, message: "Unable to Fetch Packages", shouldDisplay: true)
                //unable to load packages for resident
            }else if let packages = packages{
                let packageByType = packages.separateBy(separator: Array.Separator.PackageType)
                cell.configure(with: resident, andPackages: packageByType)
            }
        }
        CMQPackageDataSourceManager.sharedInstance().checkOutPackages(resident: resident, completion: completion)
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return cell(forTable: tableView, atIndex: indexPath)
    }
}
public class CMQCheckOutSelectResidentTableViewController: IDTableViewController, IDSearchControllerProtocol {
    private var transitionHandler: CMQTransitionDelegate?
    var searchController: UISearchController!{
        didSet{
            self.customize(controller:searchController)
            self.customize(searchBar:searchController.searchBar)
            if #available(iOS 11.0, *) {
                self.navigationItem.searchController = searchController
                self.navigationItem.hidesSearchBarWhenScrolling = false
            } else {
                self.tableView.tableHeaderView = searchController.searchBar
            }
        }
    }
    
    var resultsController: UITableViewController!{
        didSet{
            resultsController.tableView.delegate = self
            if let controller = resultsController as? IDTableViewController{
                controller.dataSource = CMQCheckOutResidentContainer()
                controller.dataSource.willChangeState(to: .loading)
                controller.cellCreator = CheckOutCellCreator.init(dataSource: controller.dataSource)
            }
        }
    }
    
    public func updateSearchResults(for searchController: UISearchController) {
        self.search()
    }
    

    override public func viewDidLoad() {
        super.viewDidLoad()
        self.dataSource = CMQCheckOutResidentContainer()
        self.cellCreator = CheckOutCellCreator.init(dataSource: self.dataSource)
        self.loadResultsController()
        self.dataSource.willChangeState(to: .loading)
        
    }
    public override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    public override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let dataSource = tableView == self.tableView ? self.dataSource : (self.resultsController as! IDTableViewController).dataSource
        if let user = dataSource?.object(at: indexPath) as? CMQUser{
            tableView.allowsSelection = false
            let completion = { (packages:[CMQPackage]?, error:Error?) in
                tableView.allowsSelection = true
                if let error = error{
                    handle(error: error, message: "Unable to CheckOut Package", shouldDisplay: true)
                    //unable to load packages for resident
                }else if let packages = packages{
                    let hasPending = CMQPackageDataSourceManager.sharedInstance().isPendingDelivery(packages: packages)
                    let segID = hasPending ? "CheckOut" : "Pin"
                    self.performSegue(withIdentifier: segID, sender: packages)
                }
            }
            CMQPackageDataSourceManager.sharedInstance().checkOutPackages(resident: user, completion: completion)
        }
    }
    
    @objc public func deliveryUpdate(){
        self.dataSource.willChangeState(to: .refreshing)
    }
    
    // MARK: - Navigation
    public func prepareTransition(){
        let size = CGSize.init(width: 350.0, height: 250.0)
        
        let startX = self.view.center.x - size.width/2
        let startY = self.view.frame.height * 0.25
        self.transitionHandler = CMQTransitionDelegate(size: size, origin: CGPoint.init(x: startX, y: startY))
    }
    @IBAction func correctPinEntry(sender:CMQPinExitSegue){
        guard let sourceVC = sender.source as? CMQCheckOutEnterPinViewController else {return}
        sender.completion = { () in
            self.performSegue(withIdentifier: "CheckOut", sender: sourceVC.packagesForResident)
        }
    }
    @IBAction func cancelPinEntry(sender:UIStoryboardSegue){
        
    }
    
    override public func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Pin"{
            if let dest = segue.destination as?
                CMQCheckOutEnterPinViewController{
                self.prepareTransition()
                dest.transitioningDelegate = self.transitionHandler
                dest.modalPresentationStyle = .custom
                dest.packagesForResident = sender as? [CMQPackage]
            }
        }else if let dest = segue.destination as? CMQPackagesCheckOutViewController{
            dest.packagesForResident = sender as? [CMQPackage]
        }
    }


}
