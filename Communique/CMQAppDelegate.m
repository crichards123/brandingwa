//
//  CMQAppDelegate.m
//  Communique
//
//  Created by Chris Hetem on 9/15/14.
//  Copyright (c) 2014 WaveRider, Inc. All rights reserved.
//

#define ROOT_PROJECT_DIR @"$(SCROOT)"

#import "CMQHomeViewController.h"
#import "CMQMessageViewController.h"
#import "CMQSelectResidentWPackageTableViewController.h"
#import "CMQPackagesNavigationViewController.h"
#import <MessageUI/MessageUI.h>
#import "CMQAppConfig.h"
#import <asl.h>


@implementation CMQAppDelegate

#pragma mark - App Lifecycle

- (BOOL)application:(UIApplication *)application willFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    return YES;
}
-(UIInterfaceOrientationMask)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window{
    if (self.shouldRotate)
        return UIInterfaceOrientationMaskLandscape;
    else
        return UIInterfaceOrientationMaskPortrait;
}
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    //Crashyltics/Fabric
    [Fabric with:@[[Crashlytics class]]];
    
    //Google Analytics
    [GAI sharedInstance].trackUncaughtExceptions = YES;
    [[GAI sharedInstance].logger setLogLevel:kGAILogLevelError];
    [GAI sharedInstance].dispatchInterval = 20;
    [[GAI sharedInstance] trackerWithTrackingId:@"UA-58506764-1"];
    
    //Configure audio session
    // WHY?! deprecating.
    /*AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    NSError *setCategoryError = nil;
    BOOL ok = [audioSession setCategory:AVAudioSessionCategoryPlayback error:&setCategoryError];
    if (!ok)
        NSLog(@"%s setCategoryError=%@", __PRETTY_FUNCTION__, setCategoryError);
    */
    [self setupParseWithLaunchOptions:launchOptions];   //call for parse configuration. app id/key, tracking
    
    //Push Notifications
    if (@available(iOS 10.0, *)) {
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        center.delegate = self;
        
        [center setNotificationCategories:[CMQPushNotificationHandler sharedInstance].notifcationCategories];
    } else {
        // Fallback on earlier versions
    }
    
    
    //create nav bar controller with homeVC as root
    NSBundle* comBundle = [NSBundle bundleForClass:[CMQHomeViewController class]];
    CMQHomeViewController *homeVC = [[CMQHomeViewController alloc]initWithNibName:@"CMQHomeViewController" bundle:comBundle];
    UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:homeVC];
    nav.navigationBarHidden = YES;
    //UIStoryboard* storyBoard = [UIStoryboard storyboardWithName:@"main" bundle:comBundle];
    
    UIStoryboard* storyBoard = nil;
     if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad){
        storyBoard = [UIStoryboard storyboardWithName:@"iPadMain" bundle:comBundle];
    }else{
        storyBoard = [UIStoryboard storyboardWithName:@"main" bundle:comBundle];
    }
    if([CMQUser currentUser]){
        //[self.window.rootViewController.childViewControllers.firstObject performSegueWithIdentifier:@"Login" sender:nil];
    }

    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    self.window.rootViewController = storyBoard.instantiateInitialViewController;
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    UIViewController* controller = [self topController];
    if ([controller isKindOfClass:[CMQLoginLandingViewController class]]){
        [controller.view endEditing:true];
    }
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}
-(UIViewController*)topController{
    UIViewController* controller = self.window.rootViewController;
    while ([controller isKindOfClass:[UINavigationController class]]) {
        controller = [(UINavigationController*)controller topViewController];
    }
    return controller;
}
- (void)applicationDidBecomeActive:(UIApplication *)application
{
    
    //clear push notication badges
    [[CMQPushNotificationHandler sharedInstance]clearNotificationBadge];
    
    //Get top controller
    UIViewController* controller = [self topController];
    //If top controller can be refreshed, refresh
    if ([controller respondsToSelector:@selector(refreshObjects)]){
        [controller performSelector:@selector(refreshObjects)];
    }
    //Restore kaba session
    if ([controller isKindOfClass:[CMQAdditionalInfoViewController class]]){
        [[CMQDormaKabaManager sharedInstance]restoreSession];
    }
    
    
    [[NSHTTPCookieStorage sharedHTTPCookieStorage] setCookieAcceptPolicy:NSHTTPCookieAcceptPolicyAlways];
    
    NSMutableDictionary *cookieProperties = [NSMutableDictionary dictionary];
    [cookieProperties setObject:@"mobileApp" forKey:NSHTTPCookieName];
    [cookieProperties setObject:@"1" forKey:NSHTTPCookieValue];
    [cookieProperties setObject:@"www.example.com" forKey:NSHTTPCookieDomain];
    [cookieProperties setObject:@"www.example.com" forKey:NSHTTPCookieOriginURL];
    [cookieProperties setObject:@"/" forKey:NSHTTPCookiePath];
    [cookieProperties setObject:@"0" forKey:NSHTTPCookieVersion];
    
    NSHTTPCookie *cookie = [NSHTTPCookie cookieWithProperties:cookieProperties];
    [[NSHTTPCookieStorage sharedHTTPCookieStorage] setCookie:cookie];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

-(BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{    
    return YES;
}

#pragma mark - Parse Setup

-(void)setupParseWithLaunchOptions:(NSDictionary *)launchOptions
{
    //parse app id/key/server
    [Parse initializeWithConfiguration:[ParseClientConfiguration configurationWithBlock:^(id<ParseMutableClientConfiguration> configuration) {
        configuration.applicationId = kParseAppId;
        configuration.clientKey = kParseClientKey;
        configuration.server = kParseServerURL;
    }]];
    
    //track Parse Analytics
    [PFAnalytics trackAppOpenedWithLaunchOptions:launchOptions];
}

#pragma mark - Push Notifications
-(void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error{
    NSLog(@"Failed Registration:%@",error.localizedDescription);
}
//Push Notifications registration callback
-(void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken{
    DLogOrange(@"did register for remote notifications with deviceToken: %@", deviceToken);
    [[CMQPushNotificationHandler sharedInstance]didRegisterWithToken:deviceToken];

}

-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    DLogGreen(@"");
}

//This will only get called if the app is in the background when a notification was received. it's called when the app gets opened from a notification
//Push Notifications received callback
-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
{
    DLogBlue(@"userInfo: %@", userInfo);
    [[CMQPushNotificationHandler sharedInstance]didReceiveWithPayload:userInfo completion:completionHandler];
    
}

-(void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler
{
    [[CMQPushNotificationHandler sharedInstance]willPresentWithNotification:notification topController:[self topController] completion:completionHandler];

    
}


-(void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(nonnull UNNotificationResponse *)response withCompletionHandler:(nonnull void (^)(void))completionHandler{
    [[CMQPushNotificationHandler sharedInstance] didReceiveWithResponse:response completion:^{
        completionHandler();
    }];
    
}
-(int)aslLevelNotice{
    return ASL_LEVEL_NOTICE;
}
@end
