//
//  CMQCollectionViewController.swift
//  Communique
//
//  Created by Andre White on 4/20/18.
//  Copyright © 2018 Communique, LLC. All rights reserved.
//

import UIKit

public class CMQCollectionViewController: UICollectionViewController,UICollectionViewDelegateFlowLayout, CMQDataSourceDelegate {
    public var dataSource: CMQDataSourceManager!
    
    //Life Cycle
    override public func viewDidLoad() {
        super.viewDidLoad()
        dataSource.loadObjects()
        
    }
    
    //CMQDataSourceDelegate
    public func objectsLoaded(_ error: Error?) {
        guard error == nil else{ handle(error: error!, message: "unable to Load objects", shouldDisplay: false);return}
        self.collectionView?.reloadData()
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (self.view.frame.size.width/2)-1
        return CGSize(width: width, height: width)
    }
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 2
    }
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    //CollectionViewDataSource
    public override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return dataSource.numberOfSections
    }
    public override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataSource.numberOfRows
    }

    

}
