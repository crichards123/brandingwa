//
//  CMQPackage.m
//  Communique
//
//  Created by Colby Melvin on 3/11/16.
//  Copyright © 2016 Communique, LLC. All rights reserved.
//

#import "CMQPackage.h"
//@import Parse;
#import <Parse/PFObject+Subclass.h>
@interface CMQPackage()<CMQPackageDataSourceProtocol>

@end
@implementation CMQPackage

@dynamic aptComplexID;
@dynamic recipient;
@dynamic isRecipientNotified;
@dynamic userWhoCheckedIn;
@dynamic isCheckedIn;
@dynamic checkedInDate;
@dynamic userWhoCheckedOut;
@dynamic isCheckedOut;
@dynamic checkedOutDate;
@dynamic userWhoDelivered;
@dynamic isDelivered;
@dynamic isSameDayDelivery;
@dynamic deliveredDate;
@dynamic packagePhoto;
@dynamic carrier;
@dynamic packageType;
@dynamic tags;
@dynamic totalCharge;
@dynamic pinCode;
@dynamic recipientID;

#pragma mark - PFSubclassing Protocol

+ (void)load{
    [self registerSubclass];
}

+ (NSString *)parseClassName{
    return @"Package";
}
-(NSString*)residentName{
    CMQUser* user=(CMQUser*)self.recipient;
    if (user.isDataAvailable) {
        return user.fullName;
    }
    [user fetchIfNeeded];
    return user.fullName;
}
-(NSString*)residentBuilding{
    CMQUser* user=(CMQUser*)self.recipient;
    if (user.isDataAvailable) {
        return user.buildingUnit;
    }
    [user fetchIfNeeded];
    return user.buildingUnit;
}
-(NSInteger)type{
    if ([self.packageType isEqualToString:kPackageTypeSmallBox]) {
        return CMQPackageTypeBoxSmall;
    }
    else if([self.packageType isEqualToString:kPackageTypeLargeBox]){
        return CMQPackageTypeBoxLarge;
    }
    else if ([self.packageType isEqualToString:kPackageTypeOther]){
        return CMQPackageTypeOther;
    }
    else if ([self.packageType isEqualToString:kPackageTypeEnvelope]){
        return CMQPackageTypeEnvelope;
    }
    return -1;
    
}
@end
