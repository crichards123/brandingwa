//
//  CMQPackage.h
//  Communique
//
//  Created by Colby Melvin on 3/11/16.
//  Copyright © 2016 Communique, LLC. All rights reserved.
//

//@import Foundation;
//@import Parse;
@class CMQUser;
#import <Foundation/Foundation.h>
#import <Parse/Parse.h>
@interface CMQPackage : PFObject <PFSubclassing>
+ (NSString *)parseClassName;

/**
 *  the apartment's objectId this package is associated with
 */
@property (strong, nonatomic) NSString *aptComplexID;

/**
 *  user that is supposed to receive package
 */
@property (assign, nonatomic) CMQUser *recipient;

/**
 *  whether or not the a notification has been sent
 */
@property (assign, nonatomic) BOOL isRecipientNotified;

/**
 *  user that checked in
 */
@property (assign, nonatomic) PFUser *userWhoCheckedIn;

/**
 *  whether or not the package is checked in
 */
@property (assign, nonatomic) BOOL isCheckedIn;

/**
 *  date checked in
 */
@property (strong, nonatomic) NSDate *checkedInDate;

/**
 *  user that chekced out
 */
@property (assign, nonatomic) PFUser *userWhoCheckedOut;

/**
 *  whether or not the package is checked out
 */
@property (assign, nonatomic) BOOL isCheckedOut;

/**
 *  date checked out
 */
@property (strong, nonatomic) NSDate *checkedOutDate;

/**
 *  user that delivered
 */
@property (assign, nonatomic) PFUser *userWhoDelivered;

/**
 *  whether or not the package is delivered
 */
@property (assign, nonatomic) BOOL isDelivered;

/**
 *  whether or not the package is supposed to be delivered today
 */
@property (assign, nonatomic) BOOL isSameDayDelivery;

/**
 *  date delivered
 */
@property (strong, nonatomic) NSDate *deliveredDate;

/**
 *  a photo of the package/label
 */
@property (strong, nonatomic) PFFile *packagePhoto;

/**
 *  package carrier
 */
@property (strong, nonatomic) NSString *carrier;

/**
 *  package type
 */
@property (strong, nonatomic) NSString *packageType;

/**
 *  package tags
 */
@property (strong, nonatomic) NSString *tags;

/**
 *  package charge
 */
@property (strong, nonatomic) NSString *totalCharge;

@property (strong, nonatomic) NSNumber *pinCode;

@property(strong, nonatomic)NSString* buildingUnit;
@property(strong, nonatomic)NSString* notifyDate;
@property(strong, nonatomic)PFFile* signaturePhoto;
@property(strong, nonatomic)CMQPackage* parentPackage;
@property(assign, nonatomic)BOOL pickedUp;
@property(strong, nonatomic)NSString* deliveryStatus;
@property(strong, nonatomic)NSString* recipientID;
@property(readonly)NSInteger type;


@end
