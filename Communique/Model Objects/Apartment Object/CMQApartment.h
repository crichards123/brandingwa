//
//  CMQApartment.h
//  Communique
//
//  Created by Chris Hetem on 10/1/14.
//  Copyright (c) 2014 WaveRider, Inc. All rights reserved.
//

//@import Parse;
#import <Parse/Parse.h>

@interface CMQApartment : PFObject <PFSubclassing>

+ (NSString *)parseClassName;

/**
 *  the name of the apartment
 */
@property (strong, nonatomic) NSString *aptName;

/**
 *  the name of the apartment (lowercase)
 */
@property (strong, nonatomic) NSString *aptNameToLowercase;

/**
 * The name of the first tab
 */
@property (strong, nonatomic) NSString *firstTabTitle;

/**
 * The name of the second tab
 */
@property (strong, nonatomic) NSString *secondTabTitle;

/**
 *  the URL as a string for the apartment repair URL
 */
@property (strong, nonatomic) NSString *aptRepairURL;

/**
 *  the URL as a string for the apartment payment URL
 */
@property (strong, nonatomic) NSString *aptPaymentURL;

/**
 *  the email address associated with the apartment
 */
@property (strong, nonatomic) NSString *aptEmail;

/**
 *  the phone number associated with the apartment
 */
@property (strong, nonatomic) NSString *aptPhone;

/**
 *  the additional contacts associated with the apartment
 */
@property (strong, nonatomic) NSArray *additionalContacts;

/**
 *  community codes associated with the apartment
 */
@property (strong, nonatomic) NSArray *aptAccessCodes;

/**
 *  a photo (PFFile) associated with the apartment
 */
@property (strong, nonatomic) PFFile *aptPhoto;

/**
 *  a logo (PFFile) associated with the apartment
 */
@property (strong, nonatomic) PFFile *aptLogo;

/**
 *  the URL of the apartment complex
 */
@property (strong, nonatomic) NSString *complexURL;

/**
 *  the emergency phone number associated with the apartment
 */
@property (strong, nonatomic) NSString *emergencyPhone;

/**
 *  the mangement company associated with the apartment
 */
@property (strong, nonatomic) PFObject *management;

/**
 *  whether or not the community is an epproach customer
 */
@property (assign, nonatomic) BOOL hasEpproachSupport;

/**
 *  whether or not the community is an epproach smarthome customer
 */
@property (assign, nonatomic) BOOL hasSmartHomeIntegration;

/**
 *  whether or not the community is an area supported by Uber
 */
@property (assign, nonatomic) BOOL hasUber;

/**
 *  whether or not the community has a PayLease integration
 */
@property (assign, nonatomic) BOOL hasPayLease;

/**
*  the street address
*/
@property (strong, nonatomic) NSString *aptAddress;

/**
 *  the city
 */
@property (strong, nonatomic) NSString *aptCity;

/**
 *  the state
 */
@property (strong, nonatomic) NSString *aptState;

/**
 *  the zip code
 */
@property (strong, nonatomic) NSString *aptZip;

/**
 *  whether or not the community has Interested Party functionality
 */
@property (assign, nonatomic) BOOL hasInterestedParties;

/**
 *  reference string for Interested Parties
 */
@property (strong, nonatomic) NSString *interestedPartiesRefString;

@property (assign, nonatomic) BOOL hasPackageDelivery;
@property (assign, nonatomic) BOOL hasPackageServiceEnabled;
@property (strong, nonatomic) NSNumber* deliveryPrice;
@property (assign, nonatomic) BOOL hasUpdatedPackages;
@property (strong, nonatomic) NSArray* customLink;
//Dormakaba
@property (strong, nonatomic) NSString* kabaIP;
@property (assign, nonatomic) BOOL hasKaba;
@property (assign, nonatomic) BOOL legacySmartHome;
@property (assign, nonatomic) BOOL hasRealpage;
@property (assign, nonatomic) BOOL hasSTwo;
@property (assign, nonatomic) BOOL hasEppSup;
@end
