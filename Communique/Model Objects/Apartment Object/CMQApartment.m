//
//  CMQApartment.m
//  Communique
//
//  Created by Chris Hetem on 10/1/14.
//  Copyright (c) 2014 WaveRider, Inc. All rights reserved.
//

#import "CMQApartment.h"
//@import Parse;
#import <Parse/PFObject+Subclass.h>


@implementation CMQApartment

@dynamic aptEmail;
@dynamic aptName;
@dynamic aptNameToLowercase;
@dynamic firstTabTitle;
@dynamic secondTabTitle;
@dynamic aptPaymentURL;
@dynamic aptPhone;
@dynamic aptPhoto;
@dynamic additionalContacts;
@dynamic aptAccessCodes;
@dynamic aptLogo;
@dynamic aptRepairURL;
@dynamic complexURL;
@dynamic emergencyPhone;
@dynamic management;
@dynamic hasEpproachSupport;
@dynamic hasSmartHomeIntegration;
@dynamic hasUber;
@dynamic hasPayLease;
@dynamic aptAddress;
@dynamic aptCity;
@dynamic aptState;
@dynamic aptZip;
@dynamic hasInterestedParties;
@dynamic interestedPartiesRefString;
@dynamic hasPackageDelivery;
@dynamic hasPackageServiceEnabled;
@dynamic deliveryPrice;
@dynamic hasUpdatedPackages;
@dynamic customLink;
@dynamic kabaIP;
@dynamic hasKaba;
@dynamic legacySmartHome;
@dynamic hasRealpage;
@dynamic hasSTwo;
@dynamic hasEppSup;
#pragma mark - PFSubclassing Protocol

+ (void)load{
    [self registerSubclass];
}

+ (NSString *)parseClassName{
    return @"Apartment";
}

@end
