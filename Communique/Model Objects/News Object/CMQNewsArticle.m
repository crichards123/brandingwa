//
//  CMQNewsArticle.m
//  Communique
//
//  Created by Chris Hetem on 9/29/14.
//  Copyright (c) 2014 WaveRider, Inc. All rights reserved.
//

#import "CMQNewsArticle.h"
//@import Parse;
#import <Parse/PFObject+Subclass.h>

@implementation CMQNewsArticle

@dynamic newsContent;
@dynamic newsTitle;
@dynamic newsPhoto;
@dynamic aptComplexID;
@dynamic articleURL;
@dynamic userWhoPosted;

#pragma mark - PFSubclassingProtocol

+ (void)load{
    [self registerSubclass];
}

+ (NSString *)parseClassName{
    return @"News";
}

@end
