//
//  CMQNewsArticle.h
//  Communique
//
//  Created by Chris Hetem on 9/29/14.
//  Copyright (c) 2014 WaveRider, Inc. All rights reserved.
//

//@import Parse;
#import <Parse/Parse.h>

@interface CMQNewsArticle : PFObject <PFSubclassing>

+ (NSString *)parseClassName;

/**
 *  the title of this news article
 */
@property (strong, nonatomic) NSString *newsTitle;

/**
 *  the content/description of this news article
 */
@property (strong, nonatomic) NSString *newsContent;

/**
 *  the photo (PFFile) of this news article
 */
@property (strong, nonatomic) PFFile *newsPhoto;

/**
 *  the apartment objectId associated with this news article
 */
@property (strong, nonatomic) NSString *aptComplexID;

/**
 *  the article URL for this news article
 */
@property (strong, nonatomic) NSString *articleURL;

/**
 *  user that posted news article
 */
@property (assign, nonatomic) PFUser *userWhoPosted;

@end
