//
//  CMQEvent.h
//  Communique
//
//  Created by Chris Hetem on 9/24/14.
//  Copyright (c) 2014 WaveRider, Inc. All rights reserved.
//
//@import Parse;
#import <Parse/Parse.h>

@interface CMQEvent : PFObject <PFSubclassing>

+ (NSString *)parseClassName;

/**
 *  the date of the event
 */
@property (strong, nonatomic) NSDate *eventDate;

/**
 *  the end date/time of the event
 */
@property (strong, nonatomic) NSDate *eventEndDate;

/**
 *  timezone offset of the event
 */
@property (assign, nonatomic) NSInteger utcOffset;

/**
 *  the location of the event
 */
@property (strong, nonatomic) NSString *eventLocation;

/**
 *  the title of the event
 */
@property (strong, nonatomic) NSString *eventTitle;

/**
 *  the content/description of the event
 */
@property (strong, nonatomic) NSString *eventContent;

/**
 *  a photo of the event
 */
@property (strong, nonatomic) PFFile *eventPhoto;

/**
 *  the CMQUser objectIds who are going to this event
 */
@property (strong, nonatomic) NSArray *eventAttendees;

/**
 *  the apartment objectId this event is associated with
 */
@property (strong, nonatomic) NSString  *aptComplexID;

/**
 *  user that posted event
 */
@property (assign, nonatomic) PFUser *userWhoPosted;

/**
 *  whether or not the event is private
 */
@property (assign, nonatomic) BOOL isPrivate;

@end
