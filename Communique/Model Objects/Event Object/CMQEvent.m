//
//  CMQEvent.m
//  Communique
//
//  Created by Chris Hetem on 9/24/14.
//  Copyright (c) 2014 WaveRider, Inc. All rights reserved.
//

#import "CMQEvent.h"
//@import Parse;
#import <Parse/PFObject+Subclass.h>

@implementation CMQEvent

@dynamic eventDate;
@dynamic eventEndDate;
@dynamic utcOffset;
@dynamic eventLocation;
@dynamic eventTitle;
@dynamic eventContent;
@dynamic eventPhoto;
@dynamic eventAttendees;
@dynamic aptComplexID;
@dynamic userWhoPosted;
@dynamic isPrivate;

+ (void)load{
    [self registerSubclass];
}

+ (NSString *)parseClassName{
    return @"Event";
}

@end
