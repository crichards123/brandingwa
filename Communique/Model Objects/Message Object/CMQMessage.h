//
//  CMQMessage.h
//  Communique
//
//  Created by Chris Hetem on 9/23/14.
//  Copyright (c) 2014 WaveRider, Inc. All rights reserved.
//

//@import Foundation;
//@import Parse;
#import <Foundation/Foundation.h>
#import <Parse/Parse.h>
@class CMQUser;
@class CMQPackage;
@interface CMQMessage : PFObject <PFSubclassing>
+ (NSString *)parseClassName;

/**
 *  the apartment's objectId this message is associated with
 */
@property (strong, nonatomic) NSString *aptComplexID;

/**
 *  the title of this message
 */
@property (strong, nonatomic) NSString *messageTitle;

/**
 *  the content of this message
 */
@property (strong, nonatomic) NSString *messageContent;

/**
 *  the buildings/units this message goes to
 */
@property (strong, nonatomic) NSArray *recipients;

/**
 *  whether or not the message is urgent
 */
@property (assign, nonatomic) BOOL isCritical;

/**
 *  whether or not the message is an announcement
 */
@property (assign, nonatomic) BOOL isAnnouncement;

/**
 *  whether or not the message is an announcement
 */
@property (assign, nonatomic) BOOL isIndividualMessage;

/**
 *  user that posted message
 */
@property (assign, nonatomic)CMQUser *userWhoPosted;
@property(strong, nonatomic)PFFile* messagePhoto;
@property(strong, nonatomic)CMQPackage* Package;
@property(strong, nonatomic)NSString* deliveryStatus;
@property(assign, nonatomic)BOOL packageNoti;
@property(assign, nonatomic)BOOL scheduleSent;
@property(assign, nonatomic)BOOL scheduleMessage;
@property(strong, nonatomic)NSString* scheduleTime;
@end
