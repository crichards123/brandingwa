//
//  CMQMessage.m
//  Communique
//
//  Created by Chris Hetem on 9/23/14.
//  Copyright (c) 2014 WaveRider, Inc. All rights reserved.
//

#import "CMQMessage.h"
//@import Parse;
#import <Parse/PFObject+Subclass.h>


@implementation CMQMessage

@dynamic aptComplexID;
@dynamic messageTitle;
@dynamic messageContent;
@dynamic recipients;
@dynamic isCritical;
@dynamic isAnnouncement;
@dynamic isIndividualMessage;
@dynamic userWhoPosted;
@dynamic Package;
@dynamic packageNoti;
@dynamic scheduleSent;
@dynamic scheduleTime;
@dynamic scheduleMessage;
#pragma mark - PFSubclassing Protocol

+ (void)load{
    [self registerSubclass];
}

+ (NSString *)parseClassName{
    return @"Message";
}
@end
