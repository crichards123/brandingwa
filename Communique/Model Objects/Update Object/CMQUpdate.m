//
//  CMQUpdate.m
//  Communique
//
//  Created by Colby Melvin on 4/7/16.
//  Copyright © 2016 Communique, LLC. All rights reserved.
//

#import "CMQUpdate.h"
//@import Parse;
#import <Parse/PFObject+Subclass.h>

@implementation CMQUpdate

@dynamic version;
@dynamic isRequiredUpdate;
@dynamic isiOSUpdate;
@dynamic message;
@dynamic messages;
@dynamic date;

#pragma mark - PFSubclassing Protocol

+ (void)load{
    [self registerSubclass];
}

+ (NSString *)parseClassName{
    return @"Update";
}
@end
