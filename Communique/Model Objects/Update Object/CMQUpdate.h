//
//  CMQUpdate.h
//  Communique
//
//  Created by Colby Melvin on 4/7/16.
//  Copyright © 2016 Communique, LLC. All rights reserved.
//

//@import Foundation;
//@import Parse;
#import <Foundation/Foundation.h>
#import <Parse/Parse.h>

@interface CMQUpdate : PFObject <PFSubclassing>
+ (NSString *)parseClassName;

/**
 *  current stable version
 */
@property (strong, nonatomic) NSString *version;

/**
 *  whether or not the update is critical/required
 */
@property (assign, nonatomic) BOOL isRequiredUpdate;

/**
 *  whether or not the update is for iOS
 */
@property (assign, nonatomic) BOOL isiOSUpdate;

/**
 *  customized string to display
 */
@property (strong, nonatomic) NSString *message;

/**
 *  customized string(s) to display
 */
@property (strong, nonatomic) NSArray *messages;

/**
 *  date service ends/udpate is requried
 */
@property (strong, nonatomic) NSDate *date;

@end
