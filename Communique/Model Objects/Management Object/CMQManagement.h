//
//  CMQManagement.h
//  Communique
//
//  Created by Colby Melvin on 3/31/15.
//  Copyright (c) 2015 WaveRider, Inc. All rights reserved.
//

@interface CMQManagement : PFObject <PFSubclassing>

+ (NSString *)parseClassName;

/**
 *  a photo (PFFile) associated with the management company
 */
@property (strong, nonatomic) PFFile *companyLogo;

/**
 *  the name of the management company
 */
@property (strong, nonatomic) NSString *companyName;

@end
