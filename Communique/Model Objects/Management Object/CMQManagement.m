//
//  CMQManagement.m
//  Communique
//
//  Created by Colby Melvin on 3/31/15.
//  Copyright (c) 2015 WaveRider, Inc. All rights reserved.
//

#import "CMQManagement.h"
//@import Parse;
#import <Parse/PFObject+Subclass.h>

@implementation CMQManagement

@dynamic companyLogo;
@dynamic companyName;

#pragma mark - PFSubclassing Protocol

+ (void)load{
    [self registerSubclass];
}

+ (NSString *)parseClassName{
    return @"Management";
}

@end

