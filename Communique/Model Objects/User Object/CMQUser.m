//
//  CMQUser.m
//  Communique
//
//  Created by Chris Hetem on 10/14/14.
//  Copyright (c) 2014 WaveRider, Inc. All rights reserved.
//

@interface CMQUser()<CMQPackageDataSourceProtocol>

@end


@implementation CMQUser

@dynamic email;
@dynamic phone;
@dynamic building;
@dynamic buildingToLowercase;
@dynamic unit;
@dynamic unitToLowercase;
@dynamic buildingUnit;
@dynamic aptComplexID;
@dynamic tempAptComplexID;
@dynamic communities;
@dynamic groups;
@dynamic associatedUsers;
@dynamic userRole;
@dynamic fullName;
@dynamic fullNameToLowercase;
@dynamic alerts;
@dynamic texts;
@dynamic firstName;
@dynamic lastName;
@dynamic messagesUpToDate;
@dynamic eventsUpToDate;
@dynamic newsUpToDate;
@dynamic aptComplex;
@dynamic isActive;
@dynamic urgentNotifications;
@dynamic allNotifications;
@dynamic userPhoto;
@dynamic isSmartHomeUser;
@dynamic management;
@dynamic kabaExpire;
@dynamic kabaPublicSEId;
@dynamic kabaRegToken;
@dynamic kabaReservationNumber;
@dynamic needKabaSync;
@dynamic smartAptUnit;
@dynamic smartAptPresets;
@dynamic smartAptRoom;
+ (void)load{
    [self registerSubclass];
}

+ (CMQUser *)currentUser{
    return (CMQUser *)[PFUser currentUser];
}
-(NSString*)residentName{
    return self.fullName;
}
-(NSString*)residentBuilding{
    return self.buildingUnit;
}
-(BOOL)isAdmin{
    return [self.userRole isEqualToString:kPermissionAdmin]||[self.userRole isEqualToString:kPermissionCompanyAdmin]||[self.userRole isEqualToString:kPermissionRegionalAdmin];
}
-(BOOL)isInterestedParty{
    return [self.userRole isEqualToString:kPermissionInterestedParty];
}
-(BOOL)isResident{
    return [self.userRole isEqualToString:kPermissionResident];
}
-(BOOL)hasMultipleSites{
    return self.communities.count > 1;
}
-(BOOL)isMultiUser{
    return self.communities.count > 1 && self.isAdmin;
}
-(NSString*)complexID{
    return self.tempAptComplexID.length>0 ? self.tempAptComplexID:self.aptComplexID;
}
-(NSNumber*)walletID{
    NSString* seid = self.kabaPublicSEId;
    NSRange rangeOfDash = [seid rangeOfString:@"-"];
    NSRange rangeOfPound = [seid rangeOfString:@"#"];
    NSRange rangeOfID = NSMakeRange(rangeOfPound.location+1, rangeOfDash.location - (rangeOfPound.location+1));
    return [NSNumber numberWithInteger:[seid substringWithRange:rangeOfID].integerValue];
}
-(NSString*)SEID{
    NSString* seid = self.kabaPublicSEId;
    NSRange rangeOfPound = [seid rangeOfString:@"#"];
    return [seid substringFromIndex:rangeOfPound.location+1];
    
}
@end
