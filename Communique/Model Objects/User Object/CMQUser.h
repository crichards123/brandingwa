//
//  CMQUser.h
//  Communique
//
//  Created by Chris Hetem on 10/14/14.
//  Copyright (c) 2014 WaveRider, Inc. All rights reserved.
//

//@import Parse;
#import <Parse/Parse.h>

@interface CMQUser : PFUser <PFSubclassing>

/**
 *  returns an instace of the current user
 *
 *  @return the current user
 */
+ (CMQUser *)currentUser;

/**
 *  the user's email
 */
@property (strong, nonatomic) NSString *email;

/**
 *  the user's phone #
 */
@property (strong, nonatomic) NSString *phone;

/**
 *  the user's building
 */
@property (strong, nonatomic) NSString *building;

/**
 *  the user's building in lowercase
 */
@property (strong, nonatomic) NSString *buildingToLowercase;

/**
 *  the user's unit #
 */
@property (strong, nonatomic) NSString *unit;

/**
 *  the user's unit # in lowercase
 */
@property (strong, nonatomic) NSString *unitToLowercase;

/**
 *  concatenated building & unit
 */
@property (strong, nonatomic) NSString *buildingUnit;

/**
 *  the user's role (Resident, Admin, SuperAdmin)
 */
@property (strong, nonatomic) NSString *userRole;

/**
 *  the CMQApartment objectId the user is associated with
 */
@property (strong, nonatomic) NSString *aptComplexID;

/**
 *  the CMQApartment objectId the user is associated with
 */
@property (strong, nonatomic) NSString *tempAptComplexID;

/**
 *  communities this user is associate with
 */
@property (strong, nonatomic) NSArray *communities;

/**
 *  groups this user is associate with
 */
@property (strong, nonatomic) NSArray *groups;

/**
 *  users this user is associate with
 */
@property (strong, nonatomic) NSArray *associatedUsers;

/**
 *  the user's first name
 */
@property (strong, nonatomic) NSString *firstName;

/**
 *  the user's last name
 */
@property (strong, nonatomic) NSString *lastName;

/**
 *  the user's fullName
 */
@property (strong, nonatomic) NSString *fullName;

/**
 *  the user's fullName in lowercase
 */
@property (strong, nonatomic) NSString *fullNameToLowercase;

/**
 *  whether or not the user has subscribe to email alerts
 */
@property (assign, nonatomic) BOOL alerts;

/**
 *  whether or not the user has subscribe to sms messages
 */
@property (assign, nonatomic) BOOL texts;

/**
 *  whether or not the user is active
 */
@property (assign, nonatomic) BOOL isActive;

/**
 *  whether or not the user has subscribe to all notifications
 */
@property (assign, nonatomic) BOOL allNotifications;

/**
 *  whether or not the user has subscribe to urgent notifications
 */
@property (assign, nonatomic) BOOL urgentNotifications;

/**
 *  whether or not the user is up to date on messages
 */
@property (assign, nonatomic) BOOL messagesUpToDate;

/**
 *  whether or not the user is up to date on events
 */
@property (assign, nonatomic) BOOL eventsUpToDate;

/**
 *  whether or not the user is up to date on news
 */
@property (assign, nonatomic) BOOL newsUpToDate;

/**
 *  whether or not the user is a smarthome user/has a smarthome integration
 */
@property (assign, nonatomic) BOOL isSmartHomeUser;

/**
 *  the pointer of the apartment associated with the user
 */
@property (strong, nonatomic) PFObject *aptComplex;

/**
 *  a photo of the user
 */
@property (strong, nonatomic) PFFile *userPhoto;

@property(readonly)BOOL isAdmin;
@property(readonly)BOOL isInterestedParty;
@property(readonly)BOOL isResident;
@property(readonly)BOOL isMultiUser;
@property(readonly)NSString* complexID;
@property(strong, nonatomic)PFObject* management;
@property(readonly)BOOL hasMultipleSites;

//Dormakaba
@property(strong, nonatomic) NSString* kabaReservationNumber;
@property(strong, nonatomic) NSString* kabaPublicSEId;
@property(strong, nonatomic) NSString* kabaExpire;
@property(strong, nonatomic) NSString* kabaRegToken;
@property(readonly) NSNumber* walletID;
@property(readonly) NSString* SEID;
@property(assign, nonatomic) BOOL needKabaSync;

//SmartApt
@property(strong, nonatomic) PFObject* smartAptUnit;
@property(strong, nonatomic)NSDictionary* smartAptPresets;
@property(strong, nonatomic)NSArray* smartAptRoom;
@end
