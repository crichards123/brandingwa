//
//  CMQAPIManager.swift
//  Communique
//
//  Created by Andre White on 1/19/18.
//  Copyright © 2018 Communique, LLC. All rights reserved.
//

import UIKit
import SVProgressHUD

public class CMQAPIManager: NSObject {
    private static let sharedManager :CMQAPIManager = CMQAPIManager()
    /**
     Class Singleton
     */
    public class func sharedInstance()->CMQAPIManager{
        return .sharedManager
    }
    public class func showFeedback(message:String){
        SVProgressHUD.showSuccess(withStatus: message)
        DispatchQueue.main.asyncAfter(deadline: .now()+1.5) {
            SVProgressHUD.dismiss()
        }
    }
    public class func showError(message:String){
        SVProgressHUD.showError(withStatus: message)
        DispatchQueue.main.asyncAfter(deadline: .now()+1.5) {
            SVProgressHUD.dismiss()
        }
    }
    public var associatedUsers:[CMQUser]?
    public var smartAptUnit:CMQSmartAptUnit?
    public var apartments: [CMQApartment]?{
        didSet{
            if apartments != nil{
                apartment = nil
                if let delegate = UIApplication.shared.delegate as? CMQAppDelegate{
                    delegate.apartment = nil
                }
            }
        }
    }
    /* community for current user */
    public var apartment : CMQApartment!{
        didSet{
            guard let apartment = apartment, let installation = PFInstallation.current(), let user = CMQUser.current() else {return}
            apartments = nil
            installation["apartment"] = apartment
            installation["aptName"] = apartment.aptName
            installation["user"] = user
            installation["userFullName"] = user.fullName
            installation.saveInBackground(block: booleanCompletion(message: "Update installation apartment, user, and channel"))
            CMQUser.current().tempAptComplexID = apartment.objectId
            CMQUser.current().saveInBackground(block: booleanCompletion(message: "Update User TempAptID"))
            if let delegate = UIApplication.shared.delegate as? CMQAppDelegate{
                delegate.apartment = apartment
            }
            if apartment.hasKaba{
                CMQDormaKabaManager.sharedInstance().initDormakaba()
            }
        }
    }
    public func getAssociatedUsers(completion:@escaping ()->Void){
        guard let _ = CMQUser.current().associatedUsers as? [String] else {return}
        let userQuery = CMQUser.query()!
        userQuery.whereKey("objectId", containedIn: CMQUser.current().associatedUsers)
        userQuery.findObjectsInBackground { (objects, error) in
            if let _ = error{
                completion()
            }else if let objects = objects as? [CMQUser]{
                self.associatedUsers = objects
                completion()
            }
        }
    }
    /**
     Completion block to be completed after login. In the event of an error, shouldDisplay on handle is true.
     After successful login, requestPushPermissions is called, the apartment is fetched and the controller that is passed as a parameter performs a segue with identifier "Login"
     
    */
    public func loginCompletion(controller:UIViewController)->PFUserResultBlock{
        return { (user:PFUser?, error:Error?) ->() in
            if let error = error{
                handle(error: error, message: nil, shouldDisplay: true)
            }
            else if let user = user as? CMQUser, let managementID = user.management.objectId  {
                let acceptedMgmtValues = kAcceptedManagements.components(separatedBy: ",")
                if acceptedMgmtValues.contains("all") || acceptedMgmtValues.contains(managementID){
                    if #available(iOS 10.0, *) {
                        CMQPushNotificationHandler.sharedInstance().initUserPreferences()
                    } else {
                        // Fallback on earlier versions
                    }
                    if user.isInterestedParty{
                        self.getAssociatedUsers(completion: {
                            self.getApartment(forUser: user, controller: controller)
                        })
                    }else{
                        //CMQNotificationHandler.shared().requestPushPermissions()
                        self.getApartment(forUser: user, controller: controller)
                    }
                    if let smartApt = user.smartAptUnit{
                        smartApt.fetchInBackground(block: { (apt, error) in
                            if let error = error{
                                handle(error: error, message: "Error Getting SmartHomeUnit", shouldDisplay: false)
                            }else if let apt = apt{
                                self.smartAptUnit = apt as? CMQSmartAptUnit
                                //self.smartAptUnit?.getUnit()
                                
                            }
                        })
                    }
                    
                }
                else{
                    CMQUser.logOut()
                    CMQAPIManager.showError(message: "You do not have permission to use this app")
                    //You do not have permission to use this app.
                }
            }
            else{
                //Login failed
                
            }
            if let vc = controller as? CMQLoginLandingViewController{
                vc.loginButton.isEnabled = true
            }
        }
    }
    /**
     Block to be completed after the apartment is retrieved.
 
    */
    public var apartmentCompletion: ((_ objects:[PFObject]?, _ error:Error?) ->()) {
        return { (objects:[PFObject]?, error:Error?) ->() in
            if let error = error{
                handle(error: error, message: nil, shouldDisplay: true)
            }
            else if let apts = objects as? [CMQApartment] /*,let anApartment = objects.first as? CMQApartment*/{
                if apts.count == 1{
                    if let apt = apts.first{
                        self.apartment = apt
                    }
                }else{
                    self.apartments = apts
                }
            }
        }
    }
    private func getApartment(forUser: CMQUser, controller:UIViewController){
        let query = CMQApartment.query()
        let commIDs:[String] = forUser.communities.compactMap({($0 as! CMQApartment).objectId})
        //query?.whereKey("objectId", containedIn: commIDs)
        query?.whereKey("objectId", containedIn: commIDs)
        query?.order(byAscending: "aptName")
        query?.findObjectsInBackground(block: { (objects, error) in
            if let _ = objects {
                if !controller.isBeingDismissed{
                    controller.performSegue(withIdentifier: "Login", sender: nil)
                }
            }
            self.apartmentCompletion(objects, error)
        })
    }
    /**
        This builds a generic completion for queries that have PFBooleanResultBlock. The Message parameter should be used to debug, so a simple message explaining what object is being saved and what property on the object is being updated should suffice.
 
    */
    public func booleanCompletion(message:String?)->PFBooleanResultBlock{
        return { (success:Bool, error:Error?) ->() in
            if let error = error{
                handle(error: error, message: message, shouldDisplay: false)
            }else{
                if success{
                    if let message = message{
                         print("Success with \(message)")
                    }
                }
            }
        }
    }
    /**
        Bool representing if the Package Module should be enabled or not
 */
    public var shouldShowPackages:Bool {
        guard self.apartment != nil else {return false}
        return (CMQUser.current().isAdmin && apartment.hasPackageServiceEnabled)
    }
    public var shouldShowPayRentNWorkOrder:Bool{
        guard self.apartment != nil else {return false}
        return self.apartment.hasRealpage
    
    }
    public var shouldShowSupport:Bool{
        guard self.apartment != nil else {return false}
        guard !CMQUser.current()!.isAdmin else {return false}
        return apartment.hasEppSup
    }
    public var shouldShowSmartHome:Bool{
        guard self.apartment != nil else {return false}
        return apartment.hasSmartHomeIntegration && CMQUser.current().isSmartHomeUser
    }
    public var shouldShowPayLease:Bool{
        guard self.apartment != nil else {return false}
        return apartment.hasPayLease && !CMQUser.current().isAdmin
    }
    public var customLinks:[[String:String]]?{
        guard self.apartment != nil, let links = self.apartment.customLink else {return nil}
        if let dict = links.first as? [String:[Any?]]{
            if let custLinks = dict["customlink"] as? [[String:String]]{
                return custLinks
            }
        }
       return nil
    }
    @objc public func clearNotificationBadge(){
        guard let installation = PFInstallation.current() else {return}
        if installation.badge > 0{
            installation.badge = 0
            installation.saveInBackground()
        }
    }
    /**
     Function that uses GCD to run queries on a background thread and notify the main thread through the completion closure. Ultimately used in the HomeVC for loading the objects, but can be used anywhere that 2 or more  different Classes need to be queried.
 
 */
    public static func runConcurrentQueries(queries:[PFQuery<PFObject>], completion:@escaping ([PFObject]?, Error?)->Void){
        var queryError:Error?
        let queryGroup = DispatchGroup()
        var posts = Set<PFObject>()
        for query in queries{
            queryGroup.enter()
            query.findObjectsInBackground(block: { (objects, error) in
                if let error = error{
                    queryError = error
                }else if let objects = objects{
                    posts.formUnion(objects)
                    //posts.append(contentsOf: objects)
                }
                queryGroup.leave()
            })
        }
        queryGroup.notify(queue: DispatchQueue.main) {
            //let set = Set.init(posts)
            completion(posts.sorted(by: {$0.sortDate>$1.sortDate}), queryError)
        }
    }
    fileprivate let queryGroup = DispatchGroup()
    fileprivate var transitionDelegate:CMQTransitionDelegate?
    public func logOut(homeController:CMQHomeFeedViewController){
        //SVProgressHUD.setBackgroundColor(UIColor.CMQDarkColor())
        //SVProgressHUD.show()
        let storyBoard = UIStoryboard.init(name: "main", bundle: CMQBundle!)
        
        guard let controller = storyBoard.instantiateViewController(withIdentifier: "Loading") as? CMQLoadingViewController else {return}
        let height = homeController.view.frame.size.height*0.35
        let width = UIDevice.current.userInterfaceIdiom == .pad ? homeController.view.frame.width * 0.40 : homeController.view.frame.width*0.75
        let startY = height
        let startX = homeController.view.center.x - (width/2)
        let size = CGSize.init(width: width, height: height)
        let point = CGPoint.init(x: startX, y: startY)
        self.transitionDelegate = CMQTransitionDelegate(size: size, origin: point)
        controller.transitioningDelegate = transitionDelegate
        controller.modalPresentationStyle = .custom
        controller.promptText = "Logging Out. See you again!"
        homeController.present(controller, animated: true) {
            self.queryGroup.enter()
            
            CMQUser.logOutInBackground { (error) in
                guard error == nil else {return}
                self.queryGroup.leave()
            }
            /*if let installation = PFInstallation.current(){
                self.queryGroup.enter()
                installation["apartment"] = ""
                installation["aptName"] = ""
                installation["user"] = ""
                installation["userFullName"] = ""
                installation.saveInBackground(block: { (success, error) in
                    guard error == nil else {return}
                    if success{
                        self.queryGroup.leave()
                    }
                })
            }*/
            guard let apt = self.apartment else {return}
            guard apt.hasKaba else {return}
            if let _ = CMQDormaKabaManager.sharedInstance().connectManager {
                self.queryGroup.enter()
                CMQDormaKabaManager.sharedInstance().unregisterWallet()
            }
        }
        queryGroup.notify(queue: .main) {
            homeController.dismiss(animated: true, completion: {
                homeController.performSegue(withIdentifier: "LogOut", sender: nil) 
            })
            //SVProgressHUD.dismiss()
            
        }
        
    }
}
extension CMQAPIManager: LegicConnectionDelegate{
    public func result(_ successful: Bool, status: LegicStatus!) {
        print("Removed Wallet")
        queryGroup.leave()
    }
}
