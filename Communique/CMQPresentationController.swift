//
//  CMQPresentationController.swift
//  Communique
//
//  Created by Andre White on 1/19/18.
//  Copyright © 2018 Communique, LLC. All rights reserved.
//

import UIKit
/**
    Presentation controller with dimming view and optional tap recognizer to dismiss. Size and Origin are optionals but are required to be set
 */
public class CMQPresentationController: UIPresentationController {
    private var dimmingView = UIView()
    public var size : CGSize!
    public var origin: CGPoint!
    public var isDimmingViewTappable = true
    public var specialDimming = false
    
    override public init(presentedViewController: UIViewController, presenting presentingViewController: UIViewController?) {
        super.init(presentedViewController: presentedViewController, presenting: presentingViewController)
        dimmingView.backgroundColor = UIColor.init(white: 0, alpha: 0.4)
        dimmingView.alpha = 0
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(handleTap))
        self.dimmingView.addGestureRecognizer(tap)
    }
    
    override public var frameOfPresentedViewInContainerView: CGRect{
        var presentedViewFrame = CGRect.zero
        presentedViewFrame.size = self.size(forChildContentContainer: self.presentedViewController, withParentContainerSize: (self.containerView?.bounds.size)!)
        presentedViewFrame = CGRect.init(origin: origin, size: size)
        return presentedViewFrame
    }
    
    override public func size(forChildContentContainer container: UIContentContainer, withParentContainerSize parentSize: CGSize) -> CGSize {
        if let _ = size, let _ = self.containerView {
            /*if size.width > view.frame.size.width{
                self.size = CGSize.init(width: view.frame.size.width, height: size.height)
            }
            if size.height > view.frame.size.height{
                self.size = CGSize.init(width: size.width, height: view.frame.size.height)
            }*/
        }
        return size
    }
    
    override public func presentationTransitionWillBegin() {
        guard let view = self.containerView, let coordinator = self.presentedViewController.transitionCoordinator else {return}
        //let x = self.origin.x + self.size.width
        dimmingView.frame = specialDimming ? CGRect.init(origin: self.origin, size: view.frame.size)/*CGRect.init(origin: CGPoint.init(x: x, y: self.origin.y), size:CGSize.init(width: view.frame.width - self.size.width, height: self.size.height))*/: view.bounds
        dimmingView.alpha = 0
        view.insertSubview(dimmingView, at: 0)
        coordinator.animate(alongsideTransition: { (context) in
            self.dimmingView.alpha = 1.0
        }, completion: nil)
    }
    
    override public func containerViewWillLayoutSubviews() {
        self.presentedView?.frame = self.frameOfPresentedViewInContainerView
    }

    @objc public func handleTap()->Void{
        if isDimmingViewTappable{
            switch self.presentingViewController{
            case is CMQSmartHomeNavigationController:
                let navCon = self.presentingViewController as! CMQSmartHomeNavigationController
                if let createCon = navCon.topViewController as? CMQCreatePresetViewController{
                    navCon.dismiss(animated: true) {
                        if case .Adding(_) = createCon.state!{
                            createCon.tableAction = .none
                            createCon.willChangeState(to: .Filled)
                        }
                        
                    }
                }else{
                    self.presentingViewController.dismiss(animated: true, completion: nil)
                }
            default:
                self.presentingViewController.dismiss(animated: true, completion: nil)
            }
            
            
        }
    }
    override public func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        print("Here")
    }
    override public func preferredContentSizeDidChange(forChildContentContainer container: UIContentContainer) {
        super.preferredContentSizeDidChange(forChildContentContainer: container)
        if container is CMQPresetAddMenuViewController{
            if container.preferredContentSize.height > 0{
                self.size = container.preferredContentSize
                self.presentedViewController.view.frame.size = self.size
                self.origin.y = self.containerView!.frame.size.height - self.size.height
                //self.containerView?.setNeedsLayout()
                
            
            }
            
        }
    }

}
