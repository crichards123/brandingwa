//
//  CMQDottedLine.m
//  Communique
//
//  Created by Chris Hetem on 9/17/14.
//  Copyright (c) 2014 WaveRider, Inc. All rights reserved.
//

#import "CMQDottedLine.h"

@implementation CMQDottedLine

- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}


/**
 *  creates a light gray dotted line
 *
 *  @param rect the rect for the line
 */
- (void)drawRect:(CGRect)rect{

    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetStrokeColorWithColor(context, [[UIColor lightGrayColor] CGColor]);
    CGContextSetFillColorWithColor(context, [[UIColor clearColor] CGColor]);
    CGContextSetLineWidth(context, 5.0f);
    
    // Because your line width is 5 you need pattern 5 times “solid”, 5 times “empty”
    CGFloat dash[2]={2 , 2};
    CGContextSetLineDash(context, 0, dash ,2);
    
    
    CGContextMoveToPoint(context, 0, 0);
    
    //sets horizontal or vertical line
    if(rect.size.width > rect.size.height){
        CGContextAddLineToPoint(context, rect.size.width, 0);
    }else{
        CGContextAddLineToPoint(context, 0, rect.size.height);
    }
    
    CGContextStrokePath(context);
 
}


@end
