//
//  CMQTransitionAnimationController.h
//  Communique
//
//  Created by Chris Hetem on 9/16/14.
//  Copyright (c) 2014 WaveRider, Inc. All rights reserved.
//

//@import Foundation;
#import <Foundation/Foundation.h>

@interface CMQTransitionAnimationController : NSObject <UIViewControllerAnimatedTransitioning>

@end

