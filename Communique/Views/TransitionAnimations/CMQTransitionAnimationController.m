//
//  CMQTransitionAnimationController.m
//  Communique
//
//  Created by Chris Hetem on 9/16/14.
//  Copyright (c) 2014 WaveRider, Inc. All rights reserved.
//

#import "CMQTransitionAnimationController.h"

#define DURATION        0.3

@implementation CMQTransitionAnimationController

-(NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext{
    return DURATION;
}


-(void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext{
    UIViewController *toVC = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    
    //make sure our final frame is correct
    toVC.view.frame = [transitionContext finalFrameForViewController:toVC];
    
    //container view
    UIView *containerView = [transitionContext containerView];
 
    //initially hide our toVC by alpha = 0
    toVC.view.alpha = 0.0;
    
    //add our toVC to the transition containerView
    [containerView addSubview:toVC.view];
    
    
    [UIView animateWithDuration:[self transitionDuration:transitionContext] animations:^{
        toVC.view.alpha = 1.0;  //fade in our view to unhide it
    } completion:^(BOOL finished) {
        [transitionContext completeTransition:YES];
    }];
    
}

@end
