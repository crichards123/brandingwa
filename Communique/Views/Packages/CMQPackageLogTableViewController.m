//
//  CMQPackageLogTableViewController.m
//  Communique
//
//  Created by Andre White on 8/30/17.
//  Copyright © 2017 Communique, LLC. All rights reserved.
//

#import "CMQPackageLogTableViewController.h"
#import "CMQResidentSearchResultsTableViewController.h"
#import "CMQPackageDetailsTableViewCell.h"
//#import "CommuniqueiOS/CommuniqueiOS-Swift.h"
@interface CMQPackageLogTableViewController ()
@property(retain, nonatomic)NSDictionary* packagesForDataSource;
@end

@implementation CMQPackageLogTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title=_packagesForResidentByDate.firstObject.firstObject.residentName;
    self.tableView.tableFooterView=[[UIView alloc]init];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section{
    UITableViewHeaderFooterView* theView=(UITableViewHeaderFooterView*)view;
    theView.textLabel.textAlignment=NSTextAlignmentCenter;
    theView.textLabel.textColor=[UIColor whiteColor];
    UIView* aView=[[UIView alloc]init];
    aView.backgroundColor=[UIColor colorWithRed:35/255.0 green:173/255.0 blue:165/255.0 alpha:1.0];
    theView.backgroundView=aView;
}

#pragma mark - Table view data source
-(NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    CMQPackage* package=[_packagesForResidentByDate objectAtIndex:section].firstObject;
    NSDateFormatter* format=[[NSDateFormatter alloc]init];
    format.dateFormat=@"MMM d, yyyy - E";
    return [format stringFromDate:_isCheckIn?package.checkedInDate:package.checkedOutDate];
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 50;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [_packagesForResidentByDate count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSArray* packagesByType=[[CMQPackageDataSourceManager sharedInstance]separateWithArray:[_packagesForResidentByDate objectAtIndex:section] separator:@"Type"];
    
    return packagesByType.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CMQPackageDetailsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"reuse" forIndexPath:indexPath];
    NSArray<NSArray<CMQPackage*>*>* packagesForType=[[CMQPackageDataSourceManager sharedInstance]separateWithArray:[_packagesForResidentByDate objectAtIndex:indexPath.section] separator:@"Type"];
    CMQPackage* source=[packagesForType objectAtIndex:indexPath.row].firstObject;
    cell.deliveryLabel.text=source.packageType;
    cell.buildingLabel.text=[NSString stringWithFormat:@"%ld",(unsigned long)[packagesForType objectAtIndex:indexPath.row].count];
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    return cell;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
