//
//  CMQPackageCheckOutPinPresentationController.m
//  Communique
//
//  Created by Andre White on 8/29/17.
//  Copyright © 2017 Communique, LLC. All rights reserved.
//

#import "CMQPackageCheckOutPinPresentationController.h"

@implementation CMQPackageCheckOutPinPresentationController
- (CGRect)frameOfPresentedViewInContainerView {
    CGRect presentedViewFrame = CGRectZero;
    presentedViewFrame.size=[self sizeForChildContentContainer:self.presentedViewController withParentContainerSize:self.containerView.bounds.size];
    CGFloat startX = self.containerView.center.x - presentedViewFrame.size.width/2;
    CGFloat startY = self.containerView.center.y - presentedViewFrame.size.height/2;
    presentedViewFrame = CGRectMake(startX, startY, presentedViewFrame.size.width, presentedViewFrame.size.height);
    
    /*presentedViewFrame=_isScheduledMessage?CGRectMake(10, self.containerView.bounds.size.height*1/5, presentedViewFrame.size.width-20, presentedViewFrame.size.height):CGRectMake(10, self.containerView.bounds.size.height*1/4, presentedViewFrame.size.width-20, presentedViewFrame.size.height);*/
    return presentedViewFrame;
}
- (instancetype)initWithPresentedViewController:(UIViewController *)presentedViewController
                       presentingViewController:(UIViewController *)presentingViewController {
    self = [super initWithPresentedViewController:presentedViewController
                         presentingViewController:presentingViewController];
    if(self) {
        // Create the dimming view and set its initial appearance.
        self.dimmingView = [[UIView alloc] init];
        [self.dimmingView setBackgroundColor:[UIColor colorWithWhite:0.0 alpha:0.4]];
        [self.dimmingView setAlpha:0.0];
        UITapGestureRecognizer* recoginzer=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleTap)];
        [self.dimmingView addGestureRecognizer:recoginzer];
    }
    return self;
}
-(CGSize)sizeForChildContentContainer:(id<UIContentContainer>)container withParentContainerSize:(CGSize)parentSize{
    if (self.contentSize.width>self.containerView.frame.size.width) {
        self.contentSize = CGSizeMake(self.containerView.frame.size.width,self.contentSize.height);
    }
    if (self.contentSize.height>self.containerView.frame.size.height){
        self.contentSize = CGSizeMake(self.contentSize.width, self.containerView.frame.size.height);
    }
    return self.contentSize;
}
-(void)handleTap{
    if (self.isDimmingViewTappable) {
        [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
    }
}
-(void)containerViewWillLayoutSubviews{
    self.presentedView.frame=[self frameOfPresentedViewInContainerView];
}
- (void)presentationTransitionWillBegin {
    // Get critical information about the presentation.
    UIView* containerView = [self containerView];
    UIViewController* presentedViewController = [self presentedViewController];
    
    // Set the dimming view to the size of the container's
    // bounds, and make it transparent initially.
    [[self dimmingView] setFrame:[containerView bounds]];
    [[self dimmingView] setAlpha:0.0];
    
    // Insert the dimming view below everything else.
    [containerView insertSubview:[self dimmingView] atIndex:0];
    
    // Set up the animations for fading in the dimming view.
    if([presentedViewController transitionCoordinator]) {
        [[presentedViewController transitionCoordinator]
         animateAlongsideTransition:^(id<UIViewControllerTransitionCoordinatorContext>
                                      context) {
             // Fade in the dimming view.
             [[self dimmingView] setAlpha:1.0];
         } completion:nil];
    }
    else {
        [[self dimmingView] setAlpha:1.0];
    }
}
@end
