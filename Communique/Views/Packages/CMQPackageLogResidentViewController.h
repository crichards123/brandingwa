//
//  CMQPackageLogResidentViewController.h
//  Communique
//
//  Created by Andre White on 10/13/17.
//  Copyright © 2017 Communique, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CMQPackageLogResidentViewController : UIViewController
@property(retain, nonatomic)NSArray* dataSource;
@property(retain, nonatomic)IBInspectable UIColor* highlightedColor;
@property(retain, nonatomic)IBInspectable UIColor* unhighlightedColor;
@end
