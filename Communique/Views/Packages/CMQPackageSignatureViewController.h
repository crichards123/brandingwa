//
//  CMQPackageSignatureViewController.h
//  Communique
//
//  Created by Andre White on 9/1/17.
//  Copyright © 2017 Communique, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CMQCheckInTableViewController.h"
@class CMQSignatureView;
@interface CMQPackageSignatureViewController : UIViewController
@property(retain, nonatomic)id<CMQPackageDataSourceProtocol>dataSource;
@property (weak, nonatomic) IBOutlet CMQSignatureView *signatureView;
@end
