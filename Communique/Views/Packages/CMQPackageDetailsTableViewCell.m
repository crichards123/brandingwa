//
//  CMQPackageDetailsTableViewCell.m
//  Communique
//
//  Created by Andre White on 9/1/17.
//  Copyright © 2017 Communique, LLC. All rights reserved.
//

#import "CMQPackageDetailsTableViewCell.h"
@interface CMQPackageDetailsTableViewCell()<UITableViewDelegate, UITableViewDataSource>
@property(retain, nonatomic)NSArray<NSArray<CMQPackage*>*>* packagesByType;
@property(weak, nonatomic)IBOutlet UIView* theBackgroundView;
@property(assign)BOOL hasPending;
@property(weak, nonatomic)IBOutlet NSLayoutConstraint* tableViewHeight;
@end
@implementation CMQPackageDetailsTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.packageTable.dataSource=self;
    self.theBackgroundView.layer.shadowColor=[UIColor blackColor].CGColor;
    self.theBackgroundView.layer.shadowOffset=CGSizeMake(-2.0, 2.0);
    self.theBackgroundView.layer.shadowRadius=3;
    self.theBackgroundView.layer.shadowOpacity=.3;
    // Initialization code
}
-(void)reset{
    self.residentNameLabel.text = @"";
    self.buildingLabel.text = @"";
    self.packagesByType = nil;
    [self.packageTable reloadData];
    self.deliveryImageView.hidden = true;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _packagesByType.count;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell* cell=[tableView dequeueReusableCellWithIdentifier:@"reuse" forIndexPath:indexPath];
    NSArray* packagesForType=[_packagesByType objectAtIndex:indexPath.row];
    CMQPackage* package=packagesForType.firstObject;
    cell.textLabel.text=package.packageType;
    cell.detailTextLabel.text=[NSString stringWithFormat:@"%ld",(long) packagesForType.count];
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    return cell;
}
-(void) configureWithUser:(CMQUser*)user andPackages:(NSArray<NSArray<CMQPackage*>*>*)packages{
    self.residentNameLabel.text = user.fullName;
    self.buildingLabel.text = user.buildingUnit;
    self.packagesByType = packages;
    [self.packageTable reloadData];
    self.tableViewHeight.constant = packages.count*44;
    [self layoutIfNeeded];
    self.hasPending = [[CMQPackageDataSourceManager sharedInstance]isPendingDeliveryWithPackages:packages.firstObject];
    self.deliveryImageView.hidden = !self.hasPending;
    
}
@end
