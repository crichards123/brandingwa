//
//  CMQPackageNotifyResidentTableViewController.m
//  Communique
//
//  Created by Andre White on 8/30/17.
//  Copyright © 2017 Communique, LLC. All rights reserved.
//

#import "CMQPackageNotifyResidentTableViewController.h"
#import "CMQPackageDetailsTableViewCell.h"
#import "CMQPackageSuccessViewController.h"
#import "CMQSuccessPresentationController.h"
//#import "CommuniqueiOS/CommuniqueiOS-Swift.h"
@interface CMQPackageNotifyResidentTableViewController ()
@property (weak, nonatomic) IBOutlet UIButton *sendNotificationsButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem* selectButton;

@property (retain, nonatomic) IBInspectable UIColor* enabledColor;
@property (retain, nonatomic) IBInspectable UIColor* disabledColor;

@property (retain, nonatomic) NSMutableArray* selected;

@end

@implementation CMQPackageNotifyResidentTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _selected=[NSMutableArray new];
    self.tableView.tableFooterView=[[UIView alloc]init];
}

- (IBAction)sendNotifcationsPressed:(id)sender {
    //NSLog(@"Notifying %ld Residents", (unsigned long)_selected.count);
    [SVProgressHUD showWithStatus:@"Notifying Residents"];
    NSMutableSet* notifyingResidents=[NSMutableSet new];
    if (_selected.count==0) {
        for (int x=0; x<self.allResidentsToNotify.count; x++) {
            CMQPackage<CMQPackageDataSourceProtocol>* package=_allResidentsToNotify[x].firstObject;
            [notifyingResidents addObject:package.residentName];
        }
    }
    else{
        for (NSIndexPath* indexPath in _selected) {
            CMQPackage<CMQPackageDataSourceProtocol>* package=_allResidentsToNotify[indexPath.row].firstObject;
            [notifyingResidents addObject:package.residentName];
            
        }
    }
    /*
    [[CMQPackageDataSourceManager sharedInstance]notifyResidentsWithResidentNames:notifyingResidents.allObjects completion:^(NSError *  _Nullable error ) {
        [SVProgressHUD dismiss];
        if (!error) {
            [self performSegueWithIdentifier:@"Success" sender:nil];
        }
    }];*/
}
-(void)backToHome{
    [self performSegueWithIdentifier:@"BackToHome" sender:nil];
}
-(void)errorWithString:(NSString*)errorDesc{
    NSLog(@"%@",errorDesc);
    [SVProgressHUD showErrorWithStatus:@"Unable to notify residents. Please try again later."];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)updateButtons{
    [self updateSelectButton];
    [self updateSendButton];
}
-(void)updateSelectButton{
    _selectButton.title=_selected.count==_allResidentsToNotify.count?@"Deselect All":@"Select All";
}
-(void)updateSendButton{
    [_sendNotificationsButton setTitle:_selected.count>0?@"SEND NOTIFICATIONS":@"SEND ALL NOTIFICATIONS" forState:UIControlStateNormal];
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.allResidentsToNotify.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CMQPackageDetailsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"reuse" forIndexPath:indexPath];
    if ([_selected containsObject:indexPath]) {
        cell.checkView.highlighted=YES;
    }
    else{
        cell.checkView.highlighted=NO;
    }
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    CMQPackage<CMQPackageDataSourceProtocol>* source=[self.allResidentsToNotify objectAtIndex:indexPath.row].firstObject;
    cell.residentNameLabel.text=source.residentName;
    [cell.buildingButton setTitle:source.residentBuilding forState:UIControlStateNormal];
    UILabel* packageTypeLabel=cell.packageLabels[1];
    UILabel* packageCarrierLabel=cell.packageLabels[0];
    packageTypeLabel.text=source.packageType;
    packageCarrierLabel.text=source.carrier;
    
    
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if([_selected containsObject:indexPath]){
        [_selected removeObject:indexPath];
    }
    else{
        [_selected addObject:indexPath];
    }
    [self updateButtons];
    [tableView reloadData];
}
-(UIPresentationController*)presentationControllerForPresentedViewController:(UIViewController *)presented presentingViewController:(UIViewController *)presenting sourceViewController:(UIViewController *)source{
    CMQSuccessPresentationController* controller=[[CMQSuccessPresentationController alloc]initWithPresentedViewController:presented presentingViewController:presenting];
    return controller;
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"Success"]) {
        segue.destinationViewController.transitioningDelegate=self;
        CMQPackageSuccessViewController* dest=segue.destinationViewController;
        dest.prompt=@"Resident(s)"; //_selected.count>1?[NSString stringWithFormat:@"%ld Residents have been", _selected.count]:@"Resident has been";
        segue.destinationViewController.modalPresentationStyle=UIModalPresentationCustom;
    }
}

@end
