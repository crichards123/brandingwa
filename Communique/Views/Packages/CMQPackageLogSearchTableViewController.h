//
//  CMQPackageLogSearchTableViewController.h
//  Communique
//
//  Created by Andre White on 10/13/17.
//  Copyright © 2017 Communique, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CMQPackageLogSearchTableViewController : UITableViewController
@property(retain, nonatomic)NSArray<id<CMQPackageDataSourceProtocol>>* allResidents;
@end
