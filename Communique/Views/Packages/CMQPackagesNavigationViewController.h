//
//  CMQPackagesNavigationViewController.h
//  Communique
//
//  Created by Andre White on 8/28/17.
//  Copyright © 2017 Communique, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CMQPackagesNavigationViewController : UINavigationController

@end
