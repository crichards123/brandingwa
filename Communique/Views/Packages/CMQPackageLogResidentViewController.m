//
//  CMQPackageLogResidentViewController.m
//  Communique
//
//  Created by Andre White on 10/13/17.
//  Copyright © 2017 Communique, LLC. All rights reserved.
//

#import "CMQPackageLogResidentViewController.h"
#import "CMQPackageLogTableViewController.h"
@interface CMQPackageLogResidentViewController ()

@property(retain, nonatomic)CMQPackageLogTableViewController* checkInController;
@property(retain, nonatomic)CMQPackageLogTableViewController* checkOutController;

@property (weak, nonatomic) IBOutlet UIView* selectionView;
@property(retain, nonatomic)IBOutlet UILabel* checkInLabel;
@property(retain, nonatomic)IBOutlet UILabel* checkOutLabel;
@property(retain, nonatomic)IBOutlet UIButton* checkInButton;
@property(retain, nonatomic)IBOutlet UIButton* checkOutButton;

@property(retain, nonatomic)UIScreenEdgePanGestureRecognizer* gesture;

@property(assign, nonatomic)BOOL isAnimating;
@end

@implementation CMQPackageLogResidentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpControllers];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)setUpControllers{
    NSBundle* comBundle = [NSBundle bundleForClass:[CMQPackageLogTableViewController class]];
    _checkInController=self.childViewControllers.firstObject;
    _checkOutController=[[UIStoryboard storyboardWithName:@"Log" bundle:comBundle]instantiateViewControllerWithIdentifier:@"LogTableViewController"];
    //_checkInController.collectionView.dataSource=self;
    _checkInController.packagesForResidentByDate=[[CMQPackageDataSourceManager sharedInstance]separateWithArray:_dataSource separator:@"CheckIn"];
    
    CMQPackage* package= _dataSource.firstObject;
    _checkOutController.packagesForResidentByDate=[[CMQPackageDataSourceManager sharedInstance]separateWithArray:_dataSource separator:@"CheckOut"];
    _checkInController.isCheckIn=YES;
    _checkOutController.isCheckIn=NO;
    _gesture= [[UIScreenEdgePanGestureRecognizer alloc]initWithTarget:self action:@selector(handleGesture:)];
    _gesture.edges= UIRectEdgeRight;
    [_checkInController.view addGestureRecognizer:_gesture];
    self.navigationItem.title= package.recipient.fullName;
}
- (void)handleGesture:(UIScreenEdgePanGestureRecognizer*)gesture{
    if (gesture.state== UIGestureRecognizerStateBegan) {
        if ([self.childViewControllers.firstObject isEqual:_checkInController]) {
            [self cycleFromViewController:_checkInController toViewController:_checkOutController];
        }
        else{
            [self cycleFromViewController:_checkOutController toViewController:_checkInController];
        }
    }
}
- (void)cycleFromViewController: (UIViewController*) oldVC
               toViewController: (UIViewController*) newVC {
    
    // Prepare the two view controllers for the change.
    _isAnimating=YES;
    [oldVC willMoveToParentViewController:nil];
    [self addChildViewController:newVC];
    // Get the start frame of the new view controller and the end frame
    // for the old view controller. Both rectangles are offscreen.
    newVC.view.frame = [self newViewStartFrame];
    CGRect endFrame = [self oldViewEndFrame];
    UILabel* newLabel= oldVC==_checkInController?_checkOutLabel:_checkInLabel;
    UILabel* oldLabel= oldVC==_checkInController?_checkInLabel:_checkOutLabel;
    UIButton* newButton= oldVC==_checkInController?_checkOutButton:_checkInButton;
    // Queue up the transition animation.
    [self transitionFromViewController: oldVC toViewController: newVC
                              duration: 0.5 options:0
                            animations:^{
                                // Animate the views to their final positions.
                                newVC.view.frame = oldVC.view.frame;
                                oldVC.view.frame = endFrame;
                                newLabel.textColor=_highlightedColor;
                                oldLabel.textColor=_unhighlightedColor;
                                _selectionView.frame=CGRectMake(newButton.frame.origin.x, _selectionView.frame.origin.y, _selectionView.frame.size.width, _selectionView.frame.size.height);
                                
                                
                            }
                            completion:^(BOOL finished) {
                                // Remove the old view controller and send the final
                                // notification to the new view controller.
                                [oldVC.view removeGestureRecognizer:_gesture];
                                [oldVC removeFromParentViewController];
                                [newVC didMoveToParentViewController:self];
                                _gesture.edges=newVC==_checkInController?UIRectEdgeRight:UIRectEdgeLeft;
                                [newVC.view addGestureRecognizer:_gesture];
                                _isAnimating=NO;
                            }];
}
-(CGRect)newViewStartFrame{
    if ([self.childViewControllers.firstObject isEqual:_checkInController]) {
        return CGRectMake(self.view.frame.size.width, 0, self.view.frame.size.width, self.view.frame.size.height);
    }
    else{
        return CGRectMake(-self.view.frame.size.width, 0, self.view.frame.size.width, self.view.frame.size.height);
    }
}
-(CGRect)oldViewEndFrame{
    if ([self.childViewControllers.firstObject isEqual:_checkInController]) {
        return CGRectMake(-self.view.frame.size.width, 0, self.view.frame.size.width, self.view.frame.size.height);
    }
    else{
        return CGRectMake(self.view.frame.size.width, 0, self.view.frame.size.width, self.view.frame.size.height);
    }
}
-(IBAction)buttonPressed:(UIButton*)sender{
    if (!_isAnimating) {
        switch (sender.tag) {
            case 0:
                //checkInPressed
                if ([self.childViewControllers.firstObject isEqual:_checkOutController]) {
                    [self cycleFromViewController:_checkOutController toViewController:_checkInController];
                }
                break;
            case 1:
                //checkOutPressed
                if ([self.childViewControllers.firstObject isEqual:_checkInController]) {
                    [self cycleFromViewController:_checkInController toViewController:_checkOutController];
                }
                break;
            default:
                break;
        }
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
