//
//  CMQCheckOutEnterPinViewController.h
//  Communique
//
//  Created by Andre White on 8/29/17.
//  Copyright © 2017 Communique, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CMQCheckInTableViewController.h"
@interface CMQCheckOutEnterPinViewController : UIViewController
@property(retain, nonatomic)NSArray<CMQPackage*>* packagesForResident;
@property(readonly, nonatomic)CGSize contentSize;
@end
