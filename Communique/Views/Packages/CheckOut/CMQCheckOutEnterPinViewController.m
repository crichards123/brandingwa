//
//  CMQCheckOutEnterPinViewController.m
//  Communique
//
//  Created by Andre White on 8/29/17.
//  Copyright © 2017 Communique, LLC. All rights reserved.
//

#import "CMQCheckOutEnterPinViewController.h"
//#import "CommuniqueiOS/CommuniqueiOS-Swift.h"
@interface CMQCheckOutEnterPinViewController ()<UITextFieldDelegate>
@property(weak, nonatomic)IBOutlet UITextField* pinField;
@property (weak, nonatomic) IBOutlet UILabel *residentNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *residentBuildingLabel;
@property (weak, nonatomic) IBOutlet UILabel *pinPromptLabel;

@end

@implementation CMQCheckOutEnterPinViewController
- (IBAction)proceedButtonPressed:(id)sender {
    [self checkPin];
}
-(CGSize)contentSize{
    return CGSizeMake(350.0, 250.0);
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self initView];
    // Do any additional setup after loading the view.
}
-(void)initView{
    CMQPackage<CMQPackageDataSourceProtocol>* package=self.packagesForResident.firstObject;
    _residentNameLabel.text=package.residentName;
    _residentBuildingLabel.text=package.residentBuilding;
    [_pinField becomeFirstResponder];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if (range.location>3) {
        return NO;
    }
    return YES;
}
-(void)checkPin{
    NSNumber* pin=[NSNumber numberWithInteger:_pinField.text.integerValue];
    if ([self confirmPin:pin]) {
        [_pinField resignFirstResponder];
        [self performSegueWithIdentifier:@"correctPin" sender:nil];
    }
    else{
        self.view.transform = CGAffineTransformMakeTranslation(20, 0);
        [UIView animateWithDuration:0.4 delay:0.0 usingSpringWithDamping:0.2 initialSpringVelocity:1.0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
            self.view.transform = CGAffineTransformIdentity;
        } completion:^(BOOL finished) {
            if (finished) {
                _pinField.text=@"";
            }
        }];
    }
}
-(BOOL)confirmPin:(NSNumber*) pin{
    if (pin.intValue<1000){
        return false;
    }else{
        return [[CMQPackageDataSourceManager sharedInstance]confirmPinWithPackages:self.packagesForResident pin:pin];
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
