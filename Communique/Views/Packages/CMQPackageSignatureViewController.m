//
//  CMQPackageSignatureViewController.m
//  Communique
//
//  Created by Andre White on 9/1/17.
//  Copyright © 2017 Communique, LLC. All rights reserved.
//

#import "CMQPackageSignatureViewController.h"
#import "CMQAppDelegate.h"
//#import "CommuniqueiOS/CommuniqueiOS-Swift.h"
@interface CMQPackageSignatureViewController ()
@property(assign, nonatomic)BOOL didStartDrawing;
@property(weak, nonatomic)IBOutlet UIButton* cancelButton;
@property(weak, nonatomic)IBOutlet UIButton* doneButton;
@end

@implementation CMQPackageSignatureViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didStartSignature) name:self.signatureView.startedDrawingNotification object:nil];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)didStartSignature{
    _didStartDrawing= YES;
    [_cancelButton setTitle:@"CLEAR" forState:UIControlStateNormal];
    _doneButton.enabled=YES;
}
- (IBAction)donePressed:(id)sender {
    if (_didStartDrawing) {
        [self performSegueWithIdentifier:@"CheckOut" sender:nil];
    }
    
}
-(IBAction)clearPressed:(id)sender{
    if (!_didStartDrawing) {
        [self performSegueWithIdentifier:@"Cancel" sender:nil];
    }
    else{
        _signatureView.tempImageView.image=nil;
        _didStartDrawing=NO;
        [_cancelButton setTitle:@"CANCEL" forState:UIControlStateNormal];
        _doneButton.enabled=NO;

    }
}
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    CMQAppDelegate *appDelegate = (CMQAppDelegate*)[UIApplication sharedApplication].delegate;
    [appDelegate setShouldRotate:NO];
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

@end
