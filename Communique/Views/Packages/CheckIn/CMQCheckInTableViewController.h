//
//  CMQCheckInTableViewController.h
//  Communique
//
//  Created by Andre White on 8/23/17.
//  Copyright © 2017 Communique, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "CMQSelectResidentTableViewController.h"
@interface CMQCheckInTableViewController : UITableViewController
@property(retain, nonatomic)id<CMQPackageDataSourceProtocol>dataSource;
@end
