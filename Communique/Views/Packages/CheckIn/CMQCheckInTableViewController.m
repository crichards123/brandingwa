//
//  CMQCheckInTableViewController.m
//  Communique
//
//  Created by Andre White on 8/23/17.
//  Copyright © 2017 Communique, LLC. All rights reserved.
//

#import "CMQCheckInTableViewController.h"
#import "CMQPackageDetailsTableViewCell.h"
#import "CMQSuccessPresentationController.h"
#import "CMQPackageSuccessViewController.h"
//#import "CommuniqueiOS/CommuniqueiOS-Swift.h"

typedef NS_ENUM(NSInteger, CMQCHECKINSECTION){
    CMQCHECKINSECTIONCARRIER,
    CMQCHECKINSECTIONPACKAGE,
    CMQCHECKINSECTIONTAGS
};
@interface CMQCheckInTableViewController ()<UIViewControllerTransitioningDelegate, UITextFieldDelegate>
@property(strong, nonatomic)IBOutletCollection(UIButton)NSArray* carrierButtons;
@property(strong, nonatomic)IBOutletCollection(UIButton)NSArray* packageButtons;
@property(strong, nonatomic)IBOutletCollection(UIButton)NSArray* tagButtons;
@property(weak, nonatomic)IBOutlet UIButton* cancelButton;
@property(weak, nonatomic)IBOutlet UIButton* checkInButton;
@property(weak, nonatomic)IBOutlet UITextField* otherField;
@property(retain, nonatomic)IBInspectable UIColor* highlightColor;
@property(retain, nonatomic)IBInspectable UIColor* disabledColor;
@property(assign, nonatomic)NSInteger selectedCarrier;
@property(assign, nonatomic)NSInteger selectedPackage;
@property(assign, nonatomic)NSInteger selectedTag;
@property(retain, nonatomic)NSMutableArray* packagesToCheckIn;
@property(assign, nonatomic)NSInteger packageCount;
@property(strong, nonatomic)IBOutlet CMQPackageDetailsTableViewCell* residentCell;

@end

@implementation CMQCheckInTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initFirstCell];
    [self addBordersToButtons];
    [self resetButtons];
    
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    _packageCount=0;
}
-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [self removeAllPackages];
}
-(void)initFirstCell{
    _residentCell.residentNameLabel.text=self.dataSource.residentName;
    [_residentCell.buildingButton setTitle:self.dataSource.residentBuilding forState:UIControlStateNormal];
    _packageCount=0;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)addBordersToButtons{
    for (UIButton* button in _carrierButtons) {
        button.layer.borderWidth=1;
        button.layer.borderColor=_highlightColor.CGColor;
    }
    for (UIButton* button in _packageButtons) {
        button.layer.borderWidth=1;
        button.layer.borderColor=_highlightColor.CGColor;
    }
    for (UIButton* button in _tagButtons) {
        button.layer.borderWidth=1;
        button.layer.borderColor=_highlightColor.CGColor;
    }
}
-(void)updateButtons{
    [self updateButtonsWithType:CMQCHECKINSECTIONPACKAGE];
    [self updateButtonsWithType:CMQCHECKINSECTIONTAGS];
    [self updateButtonsWithType:CMQCHECKINSECTIONCARRIER];
}
-(void)resetButtons{
    _selectedCarrier=-1;
    _selectedTag=-1;
    _selectedPackage=-1;
    [self updateButtons];
}
-(void)updateCheckInButtton{
    if ([self checkInReqsMet]) {
        _checkInButton.backgroundColor=_highlightColor;
        _checkInButton.enabled=YES;
        [_checkInButton setTitle:@"Add Package" forState:UIControlStateNormal];
    }
    else if(_packageCount>0&&![self anySelected]){
        _checkInButton.backgroundColor=_highlightColor;
        _checkInButton.enabled=YES;
        [_checkInButton setTitle:[NSString stringWithFormat:@"Check In(%ld)",(long)_packageCount] forState:UIControlStateNormal];
    }
    else{
        _checkInButton.enabled=NO;
        _checkInButton.backgroundColor=_disabledColor;
        [_checkInButton setTitle:@"Add Package" forState:UIControlStateNormal];
    }
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return CGFLOAT_MIN;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return CGFLOAT_MIN;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    switch (indexPath.row) {
        case 0:
            return self.view.frame.size.height*.08;
            break;
        case 1:
            return self.view.frame.size.height*.18;
            break;
        case 2:
            return self.view.frame.size.height*.32;
            break;
        case 3:
            return self.view.frame.size.height*.42;
            break;
        default:
            break;
    }
    return 0;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([_otherField isFirstResponder]) {
        [_otherField resignFirstResponder];
    }
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    if ([textField isFirstResponder]) {
        [textField resignFirstResponder];
    }
    return YES;
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    if (string.length==1) {
        for (UIButton* button in _tagButtons) {
            if (button.tag==3) {
                [button setTitle:@"SAVE" forState:UIControlStateNormal];
                return YES;
            }
            
        }
        return YES;
    }
    else if (textField.text.length==1){
        for (UIButton* button in _tagButtons) {
            if (button.tag==3) {
                [button setTitle:@"OTHER" forState:UIControlStateNormal];
                return YES;
            }
        }
        return YES;
    }
    return YES;
}
-(IBAction)carrierButtonPressed:(UIButton*)sender{
    if(_selectedCarrier!=sender.tag){
        _selectedCarrier=sender.tag;
    }
    else{
        _selectedCarrier=-1;
    }
    [self updateButtonsWithType:CMQCHECKINSECTIONCARRIER];
}
-(IBAction)packageButtonPressed:(UIButton*)sender{
    if (sender.tag!=_selectedPackage) {
        _selectedPackage=sender.tag;
    }
    else{
        _selectedPackage=-1;
    }
    [self updateButtonsWithType:CMQCHECKINSECTIONPACKAGE];
}
-(IBAction)tagsButtonPressed:(UIButton*)sender{
    if (sender.tag!=_selectedTag) {
         _selectedTag=sender.tag;
    }
    else{
        if (_otherField.text.length==0) {
            _selectedTag=-1;
        }
        else{
            [sender setTitle:@"OTHER" forState:UIControlStateNormal];
        }
    }
    [self updateButtonsWithType:CMQCHECKINSECTIONTAGS];
}
-(void)updateButtonsWithType:(CMQCHECKINSECTION)section{
    switch (section) {
        case CMQCHECKINSECTIONCARRIER:
            for (UIButton* button in _carrierButtons) {
                if (button.tag==_selectedCarrier) {
                    button.backgroundColor=_highlightColor;
                    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                }
                else{
                    button.backgroundColor=[UIColor whiteColor];
                    [button setTitleColor:_highlightColor forState:UIControlStateNormal];
                }
            }
            break;
        case CMQCHECKINSECTIONTAGS:
            for (UIButton* button in _tagButtons) {
                if (button.tag==_selectedTag) {
                    button.backgroundColor=_highlightColor;
                    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                }
                else{
                    if (_selectedTag==3) {
                        button.hidden=!_otherField.isFirstResponder;
                    }
                    else{
                        button.hidden=NO;
                    }
                    button.backgroundColor=[UIColor whiteColor];
                    [button setTitleColor:_highlightColor forState:UIControlStateNormal];
                }
            }
            if (_selectedTag==3) {
                if (_otherField.isFirstResponder) {
                    [_otherField resignFirstResponder];
                    _otherField.hidden=YES;
                }
                else{
                    [_otherField becomeFirstResponder];
                    _otherField.hidden=NO;
                }
            }
            else{
                [_otherField resignFirstResponder];
                _otherField.hidden=YES;
            }
            break;
        case CMQCHECKINSECTIONPACKAGE:
            for (UIButton* button in _packageButtons) {
                if (button.tag==_selectedPackage) {
                    button.backgroundColor=_highlightColor;
                    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                }
                else{
                    button.backgroundColor=[UIColor whiteColor];
                    [button setTitleColor:_highlightColor forState:UIControlStateNormal];
                }
            }
            break;
        default:
            break;
    }
    [self updateCheckInButtton];
}
-(void)removeAllPackages{
    [[CMQPackageDataSourceManager sharedInstance] clearCheckInPackages];
    _packageCount=0;
}
-(BOOL)anySelected{
    return [self carrierSelected]||[self packageSelected]||[self tagSelected];
}
-(BOOL)checkInReqsMet{
    return [self carrierSelected]&&[self packageSelected];
}
-(BOOL)carrierSelected{
    return _selectedCarrier!=-1;
}
-(BOOL)packageSelected{
    return _selectedPackage!=-1;
}
-(BOOL)tagSelected{
    return _selectedTag!=-1;
}
- (IBAction)checkInPressed:(id)sender {
    if ([self checkInReqsMet]) {
        _packageCount=[[CMQPackageDataSourceManager sharedInstance] createPackageWithType:_selectedPackage tag:[self tagSelected]?_selectedTag:0 carrier:_selectedCarrier tagString:^NSString * _Nonnull{
            if (_selectedTag==3) {
                return _otherField.text;
            }
            return nil;
        }];
        [self resetButtons];
    }
    else if(_packageCount>0){
        [SVProgressHUD showWithStatus:@"Checking in Package"];
        [[CMQPackageDataSourceManager sharedInstance]checkInUser:_dataSource completion:^(NSError * _Nullable error) {
            [SVProgressHUD dismiss];
            if (!error) {
                //Successfully Checked In packages
                [self performSegueWithIdentifier:@"Success" sender:nil];
            }
            else{
                [self errorWithString:error.localizedDescription];
            }
        }];
    }
    else{
        //Check In Requirements not met, inform user they must complete all requirements (Carrier, packagetype, optional tag) before completing
    }
    [self updateCheckInButtton];
}
-(void)errorWithString:(NSString*)errorDesc{
    NSLog(@"%@",errorDesc);
    [SVProgressHUD showErrorWithStatus:@"Unable to Check In Package"];
}
-(void)backToHome{
    [self performSegueWithIdentifier:@"BackToHome" sender:nil];
}
-(IBAction)cancelPressed:(id)sender{
    if ([self checkInReqsMet]||_packageCount>0) {
        [self presentViewController:[self cancelAlert] animated:YES completion:nil];
    }
    else{
        [self.navigationController popViewControllerAnimated:YES];
    }
}

-(UIAlertController*)cancelAlert{
    UIAlertController* controller=[UIAlertController alertControllerWithTitle:@"" message:@"Are you sure you want to cancel? All progress will be lost" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* okAction=[UIAlertAction actionWithTitle:@"Cancel Check In" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self removeAllPackages];
        [self performSegueWithIdentifier:@"BackToHome" sender:nil];
    }];
    UIAlertAction* cancel=[UIAlertAction actionWithTitle:@"Continue Check In" style:UIAlertActionStyleCancel handler:nil];
    [controller addAction:cancel];
    [controller addAction:okAction];
    if (_packageCount>0) {
        UIAlertAction* removePackagesAction=[UIAlertAction actionWithTitle:@"Remove Packages" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self removeAllPackages];
            dispatch_async(dispatch_get_main_queue(), ^{
                _checkInButton.enabled=NO;
                [_checkInButton setTitle:@"Add Package" forState:UIControlStateNormal];
                _checkInButton.backgroundColor=_disabledColor;
            });
        }];
        [controller addAction:removePackagesAction];
    }
    return controller;
}
-(UIPresentationController*)presentationControllerForPresentedViewController:(UIViewController *)presented presentingViewController:(UIViewController *)presenting sourceViewController:(UIViewController *)source{
    CMQSuccessPresentationController* controller=[[CMQSuccessPresentationController alloc]initWithPresentedViewController:presented presentingViewController:presenting];
    return controller;
}
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"Success"]) {
        segue.destinationViewController.transitioningDelegate=self;
        CMQPackageSuccessViewController* dest=segue.destinationViewController;
        dest.packageCount=_packageCount;
        dest.prompt=_packageCount>1?[NSString stringWithFormat:@"Your %ld packages have been", (long)_packageCount]:@"Your package has been";
        segue.destinationViewController.modalPresentationStyle=UIModalPresentationCustom;
    }
}
@end
