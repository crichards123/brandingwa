//
//  CMQResidentSearchResultsTableViewController.m
//  Communique
//
//  Created by Andre White on 8/24/17.
//  Copyright © 2017 Communique, LLC. All rights reserved.
//

#import "CMQResidentSearchResultsTableViewController.h"
#import "CMQPackageDetailsTableViewCell.h"
//#import "CommuniqueiOS/CommuniqueiOS-Swift.h"
typedef NS_ENUM(NSInteger, CMQSearchTable){
  CMQSearchTableCheckIn=0,
    CMQSearchTableLog,
    CMQSearchTableCheckOut
};
@interface CMQResidentSearchResultsTableViewController ()

@end

@implementation CMQResidentSearchResultsTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.tableFooterView=[[UIView alloc]init];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    switch (_tableType) {
        case CMQSearchTableCheckIn:
            return UITableViewAutomaticDimension;
            break;
        case CMQSearchTableCheckOut:{
            return 206;
            /*NSArray<CMQPackage*>* packagesForResident=[self.filteredResidents objectAtIndex:indexPath.row];
            NSInteger numberOfTypes=[[[CMQPackageDataSourceManager sharedInstance]separateWithArray:packagesForResident separator:@"Type"]count];
            return 64+numberOfTypes*44;*/
        }
            break;
        default:
            break;
    }
    return 0;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _filteredResidents.count;
    //return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CMQPackageDetailsTableViewCell* cell;
    switch (_tableType) {
        case CMQSearchTableCheckIn:{
            cell=[tableView dequeueReusableCellWithIdentifier:@"ResidentCell" forIndexPath:indexPath];
            cell.residentNameLabel.text=[_filteredResidents objectAtIndex:indexPath.row].residentName;
            [cell.buildingButton setTitle:[_filteredResidents objectAtIndex:indexPath.row].residentBuilding forState:UIControlStateNormal];
            
        }
            break;
        case CMQSearchTableLog:{
            cell=[tableView dequeueReusableCellWithIdentifier:@"ResidentCell" forIndexPath:indexPath];
            id<CMQPackageDataSourceProtocol> source=[_filteredResidents objectAtIndex:indexPath.row];
            cell.residentNameLabel.text=source.residentName;
            [cell.buildingButton setTitle:source.residentBuilding forState:UIControlStateNormal];
        }
            break;
        case CMQSearchTableCheckOut:{
            cell=[tableView dequeueReusableCellWithIdentifier:@"PackageCell" forIndexPath:indexPath];
            [cell reset];
            CMQUser* resident=[self.filteredResidents objectAtIndex:indexPath.row];
            cell.residentNameLabel.text = resident.fullName;
            cell.buildingLabel.text = resident.buildingUnit;
            /*[[CMQPackageDataSourceManager sharedInstance]packagesForResidentName:resident.fullName completion:^(NSArray * _Nullable packages, NSError * _Nullable error) {
                if (error == nil){
                    [cell configureWithUser:resident andPackages:[[CMQPackageDataSourceManager sharedInstance]separateWithArray:packages separator:@"Type"]];
                }
            }];*/
            
        }
        default:
            break;
    }
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    return cell;
}
-(NSString*)packageStringFromType:(CMQPackageType)type{
    switch (type) {
        case CMQPackageTypeBoxSmall:
            return @"Small Box";
            break;
        case CMQPackageTypeBoxLarge:
            return @"Large Box";
            break;
        case CMQPackageTypeOther:
            return @"Other";
            break;
        case CMQPackageTypeEnvelope:
            return @"Envelope";
            break;
        default:
            return nil;
            break;
    }
}
@end
