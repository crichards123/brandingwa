//
//  CMQSignatureView.swift
//  Communique
//
//  Created by Andre White on 9/8/17.
//  Copyright © 2017 Communique, LLC. All rights reserved.
//

import UIKit

@objc public class CMQSignatureView: UIView {
    @IBOutlet public weak var tempImageView : UIImageView?
    var lastPoint=CGPoint.zero
    var swiped=false
    @objc public let startedDrawingNotification = Notification.Name("StartedDrawing")
    override init(frame: CGRect) {
        lastPoint=CGPoint.zero
        swiped=false
        super.init(frame:frame)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        lastPoint=CGPoint.zero
        swiped=false
        super.init(coder:aDecoder)
    }
    
    override public func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        swiped=false
        if let touch=touches.first {
            lastPoint=touch.location(in: self)
        }
        NotificationCenter.default.post(Notification(name: startedDrawingNotification))
    }
    
    @objc func drawLine(fromPoint: CGPoint, toPoint: CGPoint){
        UIGraphicsBeginImageContextWithOptions(self.frame.size, false, 0.0)
        let context=UIGraphicsGetCurrentContext()
        tempImageView?.image?.draw(in: CGRect(x: 0, y: 0, width: self.frame.size.width, height: self.frame.size.height
        ))
        context?.move(to: fromPoint)
        context?.addLine(to: toPoint)
        context?.setLineCap(CGLineCap.round)
        context?.setLineWidth(2.0)
        context?.setStrokeColor(red: 0, green: 0, blue: 0, alpha: 1)
        context?.setBlendMode(CGBlendMode.normal)
        context?.strokePath()
        tempImageView?.image=UIGraphicsGetImageFromCurrentImageContext()
        tempImageView?.alpha=1
        UIGraphicsEndImageContext()
    }
    override public func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        swiped=true
        if let touch=touches.first {
            let currentPoint = touch.location(in: self)
            drawLine(fromPoint: lastPoint, toPoint: currentPoint)
            lastPoint=currentPoint
            
        }
        
    }
    override public func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        if !swiped {
            drawLine(fromPoint: lastPoint, toPoint: lastPoint)
        }
    }

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
