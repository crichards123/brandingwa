//
//  CMQSelectResidentWPackageTableViewController.m
//  Communique
//
//  Created by Andre White on 9/6/17.
//  Copyright © 2017 Communique, LLC. All rights reserved.
//

#import "CMQSelectResidentWPackageTableViewController.h"
#import "CMQResidentSearchResultsTableViewController.h"
#import "CMQPackageDetailsTableViewCell.h"
#import "CMQPackagesCheckOutViewController.h"
#import "CMQPackageCheckOutPinPresentationController.h"
#import "CMQCheckOutEnterPinViewController.h"
//#import "CommuniqueiOS/CommuniqueiOS-Swift.h"


@interface CMQSelectResidentWPackageTableViewController ()<UISearchControllerDelegate, UISearchResultsUpdating>
@property(retain, nonatomic)CMQResidentSearchResultsTableViewController* resultsController;
@property(retain, nonatomic)UISearchController* searchController;
//@property(retain, nonatomic)NSDictionary* separatedByResident;
@end

@implementation CMQSelectResidentWPackageTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUp];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)setUp{
    //search Controller setUp
    NSBundle* comBundle = [NSBundle bundleForClass:[CMQSelectResidentWPackageTableViewController class]];
    _resultsController=[[UIStoryboard storyboardWithName:@"Packages" bundle:comBundle]instantiateViewControllerWithIdentifier:@"SearchResults"];
    _resultsController.tableView.delegate=self;
    _searchController=[[UISearchController alloc]initWithSearchResultsController:_resultsController];
    _searchController.searchBar.placeholder=@"Resident Name";
    _searchController.searchResultsUpdater=self;
    [_searchController.searchBar sizeToFit];
    _searchController.delegate = self;
    _searchController.dimsBackgroundDuringPresentation = NO; // default is YES
    if (@available(iOS 11.0, *)) {
        self.navigationItem.searchController=_searchController;
        UISearchBar* searchBar=_searchController.searchBar;
        
        searchBar.tintColor=[UIColor whiteColor];
        //searchBar.barTintColor=[UIColor whiteColor];
        UITextField* textField= (UITextField*)[searchBar valueForKey:@"searchField"];
        //textField.textColor=_textColor;
        UIView* backgroundView= textField.subviews.firstObject;
        backgroundView.backgroundColor= [UIColor whiteColor];
        backgroundView.layer.cornerRadius= 10;
        backgroundView.clipsToBounds=YES;
    }
    else{
        self.tableView.tableHeaderView=_searchController.searchBar;
    }
    self.definesPresentationContext=YES;
    //_allResidentsWPackages=[[CMQPackageDataSourceManager sharedInstance]residentsWPackages];
}
-(IBAction)correctPinEntry:(CMQPinExitSegue*)sender{
    CMQCheckOutEnterPinViewController* source=sender.sourceViewController;
    sender.completion = ^{
        [self performSegueWithIdentifier:@"CheckOut" sender:source.packagesForResident];
    };
}
-(IBAction)cancelPinEntry:(UIStoryboardSegue*)sender{
    
    
}
-(void)checkOut:(NSArray*)array{
    
}
-(void)deliveryUpdate{
   /* dispatch_async(dispatch_get_main_queue(), ^{
        [SVProgressHUD showWithStatus:@"Updating Packages"];
        [[CMQPackageDataSourceManager sharedInstance]getPackagesToCheckOutWithCompletion:^(NSError * _Nullable error) {
            [SVProgressHUD dismiss];
            if (!error) {
                //self.allResidentsWPackages=[[CMQPackageDataSourceManager sharedInstance]residentsWPackagesToCheckOut];
                [self.tableView reloadData];
            }
            else{
                [SVProgressHUD showWithStatus:@"Error Occurred Updating Packages"];
                [self.navigationController popViewControllerAnimated:YES];
            }
        }];
    });*/
    
}
#pragma mark - Table view data source

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (![tableView isEqual:self.tableView]) {
        return [_resultsController tableView:tableView heightForRowAtIndexPath:indexPath];
    }
    NSArray<CMQPackage*>* packagesForResident=[self.allResidentsWPackages objectAtIndex:indexPath.row];
    NSInteger numberOfTypes=[[[CMQPackageDataSourceManager sharedInstance]separateWithArray:packagesForResident separator:@"Type"]count];
    
    //4 equals 237
    //return 237;
    //3 = 193
    //return 193;
    //2 = 149
    //return 149;
    //1= 105;
    return 64+numberOfTypes*44;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.allResidentsWPackages.count;
}

-(NSString*)packageStringFromType:(CMQPackageType)type{
    switch (type) {
        case CMQPackageTypeBoxSmall:
            return @"Small Box";
            break;
        case CMQPackageTypeBoxLarge:
            return @"Large Box";
            break;
        case CMQPackageTypeOther:
            return @"Other";
            break;
        case CMQPackageTypeEnvelope:
            return @"Envelope";
            break;
        default:
            return nil;
            break;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CMQPackageDetailsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PackageCell" forIndexPath:indexPath];
    NSArray<CMQPackageDataSourceProtocol>* packagesForResident=[self.allResidentsWPackages objectAtIndex:indexPath.row];
    [cell configureCheckOutCellWithPackgesbyType:[[CMQPackageDataSourceManager sharedInstance]separateWithArray:packagesForResident separator:@"Type"]];
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    id sender;
    if ([tableView isEqual:self.tableView]) {
        sender=[self.allResidentsWPackages objectAtIndex:indexPath.row];
    }
    else{
        sender=[_resultsController.filteredResidents objectAtIndex:indexPath.row];
    }
    CMQPackageDetailsTableViewCell* cell= [tableView cellForRowAtIndexPath:indexPath];
    NSArray<CMQPackageDataSourceProtocol>* packagesForResident=[self.allResidentsWPackages objectAtIndex:indexPath.row];
    CMQPackage<CMQPackageDataSourceProtocol>* package =  packagesForResident.firstObject;
    /*BOOL pendingDelivery=[[CMQPackageDataSourceManager sharedInstance]userHasPackagePendingDeliveryWithResidentFullName:package.residentName];
    if (pendingDelivery) {
        [self performSegueWithIdentifier:@"CheckOut" sender:sender];
    }
    else{
        [self performSegueWithIdentifier:@"Pin" sender:sender];
    }
    */
}
-(void)updateSearchResultsForSearchController:(UISearchController *)searchController{
    NSString* searchText=searchController.searchBar.text;
    _resultsController.tableType=2;
    _resultsController.filteredResidents=[[CMQPackageDataSourceManager sharedInstance]searchArrayWithText:searchText array:self.allResidentsWPackages];
    [_resultsController.tableView reloadData];
}
-(UIPresentationController*)presentationControllerForPresentedViewController:(UIViewController *)presented presentingViewController:(UIViewController *)presenting sourceViewController:(UIViewController *)source{
       CMQPackageCheckOutPinPresentationController* controller=[[CMQPackageCheckOutPinPresentationController alloc]initWithPresentedViewController:presented presentingViewController:presenting];
    controller.contentSize = [(CMQCheckOutEnterPinViewController*)presented contentSize];
        return controller;
}
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"Pin"]) {
        CMQCheckOutEnterPinViewController* dest=segue.destinationViewController;
        dest.transitioningDelegate=self;
        dest.modalPresentationStyle=UIModalPresentationCustom;
        dest.packagesForResident=sender;
    }
    else{
        CMQPackagesCheckOutViewController* dest=segue.destinationViewController;
        dest.packagesForResident=sender;
    }
    
    
    
}


@end
