//
//  CMQPackageOCRViewController.m
//  Communique
//
//  Created by Andre White on 8/22/17.
//  Copyright © 2017 Communique, LLC. All rights reserved
#import "CMQPackageOCRViewController.h"
//#import <TesseractOCR/TesseractOCR.h>
#import <AVFoundation/AVFoundation.h>
#import "CMQSelectResidentTableViewController.h"
//#import "CommuniqueiOS/CommuniqueiOS-Swift.h"

@interface CMQPackageOCRViewController ()</*G8TesseractDelegate,*/UINavigationControllerDelegate, UIImagePickerControllerDelegate, AVCapturePhotoCaptureDelegate>
@property (weak, nonatomic) IBOutlet UIView *squareView;
@property (weak, nonatomic) IBOutlet UIView *viewFinder;
@property (weak, nonatomic) IBOutlet UIButton *scanButton;
@property (weak, nonatomic) IBOutlet UILabel *promptLabel1;
@property (weak, nonatomic) IBOutlet UILabel *promptLabel2;

@property(retain, nonatomic)AVCaptureSession* session;
@property(retain, nonatomic)AVCapturePhotoOutput* output;
@property(retain, nonatomic)AVCaptureVideoPreviewLayer* previewLayer;
@property(assign, nonatomic)AVAuthorizationStatus cameraAuth;

@property(nonatomic) dispatch_queue_t sessionQueue;
@property(nonatomic) dispatch_block_t recognizeBlock;

@property(assign)BOOL isRunning;
@property(assign)BOOL isLoaded;
@property(assign)BOOL processing;
@property(assign)BOOL leaving;
@property(assign)BOOL capturing;

@property(retain, nonatomic)NSString* textFromTess;
@property (retain, nonatomic)UIImageView* previewView;
//@property(retain, nonatomic)G8Tesseract* tess;

@end

@implementation CMQPackageOCRViewController
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if (!_isLoaded) {
        UIBezierPath *overlayPath = [UIBezierPath bezierPathWithRect:self.view.bounds];
        UIBezierPath *transparentPath = [UIBezierPath bezierPathWithRect:_squareView.frame];
        [overlayPath appendPath:transparentPath];
        [overlayPath setUsesEvenOddFillRule:YES];
        
        CAShapeLayer *fillLayer = [CAShapeLayer layer];
        fillLayer.path = overlayPath.CGPath;
        
        fillLayer.fillRule = kCAFillRuleEvenOdd;
        fillLayer.opacity=.5;
        [self.view.layer addSublayer:fillLayer];
        [self.view bringSubviewToFront:_scanButton];
        [self.view bringSubviewToFront:_promptLabel1];
        [self.view bringSubviewToFront:_promptLabel2];
        _squareView.layer.borderWidth=1;
        _squareView.layer.borderColor=[UIColor whiteColor].CGColor;
        [self.view bringSubviewToFront:_squareView];
        _isLoaded=YES;
    }
    _previewLayer.frame=_viewFinder.bounds;
    _scanButton.layer.borderWidth=1;
    _scanButton.layer.borderColor=[UIColor whiteColor].CGColor;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    _capturing = NO;
    _sessionQueue=dispatch_queue_create("session_queue", DISPATCH_QUEUE_SERIAL);
    _cameraAuth=[AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    [self configureSession];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    _leaving=NO;
    __weak typeof (self) weakSelf = self;
    _recognizeBlock = dispatch_block_create(DISPATCH_BLOCK_BARRIER, ^{
       /* [weakSelf.tess recognize];
        // Retrieve the recognized text
        _textFromTess=[_tess recognizedText];
        NSMutableArray* words=[NSMutableArray new];
        NSArray *lines = [weakSelf.tess recognizedBlocksByIteratorLevel:G8PageIteratorLevelTextline];
        for (G8RecognizedBlock* line in lines) {
            [words addObjectsFromArray:[line.text componentsSeparatedByString:@" "]];
        }
        //NSLog(@"%@", _textFromTess);
        //NSLog(@"%@",words);
        [weakSelf searchWithWords:words];*/
    });
    dispatch_async(_sessionQueue, ^{
        switch (_cameraAuth) {
            case AVAuthorizationStatusAuthorized:
            {
                [self addObservers];
                [_session startRunning];
                _isRunning=_session.isRunning;
                break;
            }
            case -1:
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self showCameraFailedAlert];
                });
                break;
            }
            default:
                break;
        }
    });
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    _leaving=YES;
    _scanButton.enabled=YES;
    dispatch_block_cancel(_recognizeBlock);
    //self.tess = nil;
}
- (void)viewDidDisappear:(BOOL)animated
{
    dispatch_async( self.sessionQueue, ^{
            [self.session stopRunning];
            [self removeObservers];
    } );
    
    [super viewDidDisappear:animated];
}
-(void)addObservers{
    
}
-(void)removeObservers{
    
}
-(void)showCameraFailedAlert{
    NSString *message = NSLocalizedString( @"Unable to capture media", @"Camera Failed alert" );
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Error" message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *ok = [UIAlertAction actionWithTitle:NSLocalizedString( @"OK", @"Alert OK button" ) style:UIAlertActionStyleCancel handler:nil];
    [alertController addAction:ok];
    [self presentViewController:alertController animated:YES completion:nil];
}
-(void)showAccessDeniedAlert{
    NSString *message = NSLocalizedString( @"Communique doesn't have permission to use the camera, please change privacy settings", @"Alert message when the user has denied access to the camera" );
    UIAlertController *alertController =
    [UIAlertController alertControllerWithTitle:@"Error"
                                        message:message
                                 preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *ok = [UIAlertAction actionWithTitle:NSLocalizedString( @"OK", @"Alert OK button" ) style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:ok];
    UIAlertAction *settingsAction = [UIAlertAction actionWithTitle:NSLocalizedString( @"Settings", @"Alert button to open Settings" ) style:UIAlertActionStyleDefault handler:^( UIAlertAction *action ) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString] options:@{} completionHandler:nil];
    }];
    [alertController addAction:settingsAction];
    [self presentViewController:alertController animated:YES completion:nil];
}

-(void)configureSession{
    _session=[[AVCaptureSession alloc]init];
    if (_cameraAuth!=AVAuthorizationStatusAuthorized) {
        return;
    }
    NSError* error=nil;
    
    [_session beginConfiguration];
    _session.sessionPreset=AVCaptureSessionPresetPhoto;
    // Choose the back default camera.
    AVCaptureDevice *cam = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    AVCaptureDeviceInput* input=[AVCaptureDeviceInput deviceInputWithDevice:cam error:&error];
    if (!input&&error) {
        NSLog(@"%@", error.localizedDescription);
        _cameraAuth=-1; //failed
        [_session commitConfiguration];
        return;
    }
    if ([_session canAddInput:input]) {
        [_session addInput:input];
        dispatch_async( dispatch_get_main_queue(), ^{
            UIInterfaceOrientation statusBarOrientation = [UIApplication sharedApplication].statusBarOrientation;
            AVCaptureVideoOrientation initialVideoOrientation = AVCaptureVideoOrientationPortrait;
            if ( statusBarOrientation != UIInterfaceOrientationUnknown ) {
                initialVideoOrientation = (AVCaptureVideoOrientation)statusBarOrientation;
            }
            
            self.previewLayer.connection.videoOrientation = initialVideoOrientation;
        } );
    }
    else {
        NSLog( @"Could not add video device input to the session" );
        _cameraAuth = -1;
        [_session commitConfiguration];
        return;
    }
    _output=[[AVCapturePhotoOutput alloc]init];
    if ([_session canAddOutput:_output]) {
        [_session addOutput:_output];
        _output.livePhotoCaptureEnabled=NO;
        if (@available(iOS 11.0, *)) {
            _output.depthDataDeliveryEnabled=NO;
        } else {
            // Fallback on earlier versions
            //No need to set up unavailable option
        }
        
    }
    else{
        NSLog( @"Could not add video device output to the session" );
        _cameraAuth = -1;
        [_session commitConfiguration];
        return;
    }
    _previewLayer=[[AVCaptureVideoPreviewLayer alloc]initWithSession:_session];
    _previewLayer.videoGravity=AVLayerVideoGravityResizeAspectFill;
    _previewLayer.connection.videoOrientation=AVCaptureVideoOrientationPortrait;
    [_viewFinder.layer addSublayer:_previewLayer];
    [_session commitConfiguration];
}
/*
-(void)recognizeImage:(UIImage*)image{
    _processing=YES;
    _tess=[[G8Tesseract alloc]initWithLanguage:@"eng"];
    _tess.delegate=self;
    _tess.image =[image g8_blackAndWhite];
    _tess.charWhitelist = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    UIImageView* view=[[UIImageView alloc]initWithImage:image];
    CGFloat heightRatio=_squareView.frame.size.height/self.view.frame.size.height;
    CGFloat widthRatio=_squareView.frame.size.width/self.view.frame.size.width;
    CGFloat xPosRatio=_squareView.frame.origin.x/self.view.frame.size.width;
    CGFloat yPosRatio=_squareView.frame.origin.y/self.view.frame.size.height;
    CGRect rect=CGRectMake(xPosRatio*view.frame.size.width, yPosRatio*view.frame.size.height, widthRatio*view.frame.size.width, heightRatio*view.frame.size.height);
    _tess.rect=rect;
    
    // Optional: Limit recognition time with a few seconds
    _tess.maximumRecognitionTime = 2;
    _tess.engineMode=G8OCREngineModeTesseractOnly;
    // Start the recognition
    dispatch_group_t group= dispatch_group_create();
    dispatch_group_async(group, dispatch_get_main_queue(), ^{
        _previewView=[[UIImageView alloc]initWithFrame:_squareView.frame];
        _previewView.image=_tess.thresholdedImage;
        [self.view addSubview:_previewView];
    });
    
    dispatch_group_notify(group, dispatch_get_main_queue(), _recognizeBlock);
}
- (void)extracted:(NSArray *)results {
    if (!_leaving) {
        [self performSegueWithIdentifier:@"CheckIn" sender:results.firstObject];
    }
}
*/
-(void)searchWithWords:(NSArray*)words{
    BOOL found=NO;
    for (NSString* word in words) {
        if (word.length>2) {
            NSArray* results=[[CMQPackageDataSourceManager sharedInstance]searchResidentsWithText:word];
            if (results.count>0) {
                [SVProgressHUD dismiss];
                if (results.count>1) {
                    found=YES;
                    //Send word to manual search
                    if (!_leaving) {
                        [self performSegueWithIdentifier:@"Manual" sender:word];
                    }
                }
                else{
                    //[self extracted:results];
                    if (!_leaving) {
                        [self performSegueWithIdentifier:@"CheckIn" sender:results.firstObject];
                    }
                }
                break;
            }
        }
    }
    if (!found&&!_leaving) {
        [SVProgressHUD showInfoWithStatus:@"Unable to locate resident. Please try again"];
        _scanButton.enabled=YES;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [SVProgressHUD dismiss];
        });
        
    }
    _processing=NO;
    [_previewView removeFromSuperview];
}

-(IBAction)capturePhoto{
    _scanButton.enabled=NO;
    if (!_processing) {
        _capturing = YES;
        AVCaptureVideoOrientation videoPreviewLayerVideoOrientation = _previewLayer.connection.videoOrientation;
        dispatch_async(_sessionQueue, ^{
            AVCaptureConnection *theConnection = [_output connectionWithMediaType:AVMediaTypeVideo];
            theConnection.videoOrientation = videoPreviewLayerVideoOrientation;
            AVCapturePhotoSettings *photoSettings;
            if (@available(iOS 11.0, *)) {
                if ( [_output.availablePhotoCodecTypes containsObject:AVVideoCodecTypeHEVC] ) {
                    if (@available(iOS 11.0, *)) {
                        photoSettings = [AVCapturePhotoSettings photoSettingsWithFormat:@{ AVVideoCodecKey : AVVideoCodecTypeJPEG }];
                    } else {
                        //can't get here if not
                    }
                }
                else {
                    photoSettings = [AVCapturePhotoSettings photoSettings];
                }
            }
            else {
                if ([_output.availablePhotoCodecTypes containsObject:AVVideoCodecJPEG]) {
                    photoSettings= [AVCapturePhotoSettings photoSettingsWithFormat:@{AVVideoCodecKey:AVVideoCodecJPEG}];
                }
            }
            [_output capturePhotoWithSettings:photoSettings delegate:self];
            //[SVProgressHUD showWithStatus:@"Processing Image"];
        });
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/*
-(void)progressImageRecognitionForTesseract:(G8Tesseract *)tesseract {
    //NSLog(@"progress: %lu", (unsigned long)tesseract.progress);
}

- (BOOL)shouldCancelImageRecognitionForTesseract:(G8Tesseract *)tesseract {
    //return NO;  // return YES, if you need to interrupt tesseract before it finishes
    if (_leaving) {
        return YES;
    }
    return NO;
}*/
- (void)captureOutput:(AVCapturePhotoOutput *)output didFinishProcessingPhoto:(AVCapturePhoto *)photo error:(nullable NSError *)error {
    if (!error) {
        if (@available(iOS 11.0, *)) {
            NSData* data= [photo fileDataRepresentation];
            UIImage* image=[UIImage imageWithData:data];
            _capturing = NO;
            //[self recognizeImage:image];
        } else {
            //if not ios 11 captureOutput:didFinisheProcessingPhotoSamepleBuffer:previewPhotoSampleBuffer:resolvedSettings:bracketSettings:error: should be called.
        }
    }
}
- (void)captureOutput:(AVCapturePhotoOutput *)output didFinishProcessingPhotoSampleBuffer:(nullable CMSampleBufferRef)photoSampleBuffer previewPhotoSampleBuffer:(nullable CMSampleBufferRef)previewPhotoSampleBuffer resolvedSettings:(AVCaptureResolvedPhotoSettings *)resolvedSettings bracketSettings:(nullable AVCaptureBracketedStillImageSettings *)bracketSettings error:(nullable NSError *)error{
    if (!error) {
        NSData* data=[AVCapturePhotoOutput JPEGPhotoDataRepresentationForJPEGSampleBuffer:photoSampleBuffer previewPhotoSampleBuffer:previewPhotoSampleBuffer];
        UIImage* image=[UIImage imageWithData:data];
        _capturing = NO;
        //[self recognizeImage:image];
    }
}

#pragma mark - Navigation
-(IBAction)backFromCheckIn:(UIStoryboardSegue*)sender{
    
}
-(BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender{
    if ([identifier isEqualToString:@"Manual"]){
        return !_processing && !_capturing;
    }
    return true;
}
// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    /*if ([segue.identifier isEqualToString:@"image"]) {
        ViewController* dest=segue.destinationViewController;
        dest.image=sender;
        dest.rect=_squareView.frame;
    }
    else */if([segue.identifier isEqualToString:@"CheckIn"] ){
        CMQCheckInTableViewController* dest=segue.destinationViewController;
        dest.dataSource=sender;
    }
    else if ([segue.identifier isEqualToString:@"Manual"]){
        CMQCheckInSelectResidentTableViewController* dest=segue.destinationViewController;
        
        if ([sender isKindOfClass:[NSString class]]) {
            dest.ocrText = (NSString*) sender;
            //dest.ocrSearchText=sender;
        }
        //dest.allResidents=_allResidents;
        
    }
    _leaving = true;
    
}
@end
