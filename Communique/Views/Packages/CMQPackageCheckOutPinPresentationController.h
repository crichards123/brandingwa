//
//  CMQPackageCheckOutPinPresentationController.h
//  Communique
//
//  Created by Andre White on 8/29/17.
//  Copyright © 2017 Communique, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CMQPackageCheckOutPinPresentationController : UIPresentationController
@property(retain, nonatomic)UIView* dimmingView;
@property(assign, nonatomic)CGSize contentSize;
@property(assign, nonatomic)BOOL isDimmingViewTappable;
@end
