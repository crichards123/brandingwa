//
//  CMQPackageLogSearchTableViewController.m
//  Communique
//
//  Created by Andre White on 10/13/17.
//  Copyright © 2017 Communique, LLC. All rights reserved.
//

#import "CMQPackageLogSearchTableViewController.h"
#import "CMQResidentSearchResultsTableViewController.h"
#import "CMQPackageDetailsTableViewCell.h"
#import "CMQPackageLogResidentViewController.h"
@interface CMQPackageLogSearchTableViewController ()
@property (weak, nonatomic) IBOutlet UITextField *searchField;
@property(retain, nonatomic)UISearchController* searchController;
@property(retain, nonatomic)CMQResidentSearchResultsTableViewController* resultsController;
@end

@implementation CMQPackageLogSearchTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUp];
    
}
-(void)initNavBarWithSearchBar:(UISearchBar*)searchBar{
    searchBar.tintColor=[UIColor whiteColor];
    UITextField* textField= (UITextField*)[searchBar valueForKey:@"searchField"];
    UIView* backgroundView= textField.subviews.firstObject;
    backgroundView.backgroundColor= [UIColor whiteColor];
    backgroundView.layer.cornerRadius= 10;
    backgroundView.clipsToBounds=YES;
    //[self.navigationController.navigationBar setShadowImage:[[UIImage alloc] init]];
    //[self.navigationController.navigationBar setBackgroundImage:[[UIImage alloc]init] forBarMetrics:UIBarMetricsDefault];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)setUp{
    //search Controller setUp
    NSBundle* comBundle = [NSBundle bundleForClass:[CMQPackageLogSearchTableViewController class]];
    _resultsController=[[UIStoryboard storyboardWithName:@"Packages" bundle:comBundle]instantiateViewControllerWithIdentifier:@"SearchResults"];
    _resultsController.tableView.delegate=self;
    _searchController=[[UISearchController alloc]initWithSearchResultsController:_resultsController];
    _searchController.searchResultsUpdater=self;
    [_searchController.searchBar sizeToFit];
    //_resultsController.headerHeight=_searchController.searchBar.frame.size.height;
    _searchController.delegate = self;
    _searchController.dimsBackgroundDuringPresentation = NO; // default is YES
    if (@available(iOS 11.0, *)) {
        self.navigationItem.searchController=_searchController;
        [self initNavBarWithSearchBar:_searchController.searchBar];
        
        self.navigationItem.hidesSearchBarWhenScrolling=false;
    }
    else{
        self.tableView.tableHeaderView=_searchController.searchBar;
    }
    _searchController.searchBar.placeholder=@"Resident Name";
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _allResidents.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 80;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CMQPackageDetailsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"reuse" forIndexPath:indexPath];
    id<CMQPackageDataSourceProtocol> source=[(NSArray*)[_allResidents objectAtIndex:indexPath.row]firstObject];
    cell.residentNameLabel.text=source.residentName;
    [cell.buildingButton setTitle:source.residentBuilding forState:UIControlStateNormal];
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    id sender;
    if ([tableView isEqual:self.tableView]) {
        sender=[self.allResidents objectAtIndex:indexPath.row];
    }
    else{
        sender=[_resultsController.filteredResidents objectAtIndex:indexPath.row];
    }
    [self performSegueWithIdentifier:@"Select" sender:sender];
}
-(void)updateSearchResultsForSearchController:(UISearchController *)searchController{
    NSString* searchText=searchController.searchBar.text;
    _resultsController.tableType=1;
    _resultsController.filteredResidents=[[CMQPackageDataSourceManager sharedInstance]searchArrayWithText:searchText array:_allResidents];
    [_resultsController.tableView reloadData];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    CMQPackageLogResidentViewController* dest= segue.destinationViewController;
    dest.dataSource= sender;
    
    
}

@end
