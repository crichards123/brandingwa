//
//  CMQPackagesCheckOutViewController.h
//  Communique
//
//  Created by Andre White on 8/29/17.
//  Copyright © 2017 Communique, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "CMQCheckInTableViewController.h"
@interface CMQPackagesCheckOutViewController : UIViewController
@property(retain, nonatomic)id<CMQPackageDataSourceProtocol>dataSource;
@property(retain, nonatomic)NSArray<CMQPackage*>* packagesForResident;
@end
