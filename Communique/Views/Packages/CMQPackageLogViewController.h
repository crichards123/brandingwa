//
//  CMQPackageLogViewController.h
//  Communique
//
//  Created by Andre White on 9/23/17.
//  Copyright © 2017 Communique, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CMQPackageLogViewController : UIViewController
@property(retain, nonatomic)IBInspectable UIColor* highlightedColor;
@property(retain, nonatomic)IBInspectable UIColor* unhighlightedColor;
@property(retain, nonatomic)NSArray* allPackages;
@property(retain, nonatomic)NSArray* packageFlags;
@end
