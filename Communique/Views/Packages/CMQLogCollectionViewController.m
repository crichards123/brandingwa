//
//  CMQLogCollectionViewController.m
//  Communique
//
//  Created by Andre White on 9/8/17.
//  Copyright © 2017 Communique, LLC. All rights reserved.
//

#import "CMQLogCollectionViewController.h"
#import "CMQPackageCollectionViewCell.h"
#import "CMQPackageLogTableViewController.h"
//#import "CommuniqueiOS/CommuniqueiOS-Swift.h"
@interface CMQLogCollectionViewController ()<UICollectionViewDelegateFlowLayout, UISearchBarDelegate>
@property(strong, nonatomic)IBOutletCollection(UIButton)NSArray* buttons;
@property(retain, nonatomic)UISearchController* controller;
@property(strong, nonatomic)NSMutableArray* views;
@property(retain, nonatomic)NSArray* array;
@end

@implementation CMQLogCollectionViewController
static NSString * const reuseIdentifier = @"PackageCell";

- (void)viewDidLoad {
    [super viewDidLoad];
}

-(void)resetSearch{
    _array=[_allResidents copy];
    [self.collectionView reloadData];
}
-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    if (searchText.length==0) {
        _array=_allResidents;
    }
    else{
        _array=[[CMQPackageDataSourceManager sharedInstance]searchArrayWithText:searchText array:_allResidents];
    }
    
    [self.collectionView reloadData];
    
}
-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    [searchBar resignFirstResponder];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake((self.view.frame.size.width/2)-1, 180);
}
/*
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    return CGSizeMake(self.view.frame.size.width, 60);
}*/
-(CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return 2;
}
-(CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    return 1;
}
#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {

    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {

    return _allResidents.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    CMQPackageCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    cell.backgroundColor=[UIColor whiteColor];
    id<CMQPackageDataSourceProtocol> source=[(NSArray*)[_allResidents objectAtIndex:indexPath.row]firstObject];
    cell.residentNameLabel.text=source.residentName;
    cell.buildingLabel.text=source.residentBuilding;
    
    return cell;
}

 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
     CMQPackageLogTableViewController* dest=segue.destinationViewController;
     NSUInteger row=[self.collectionView indexPathForCell:(UICollectionViewCell*)sender].row;
     //dest.dataSource=[_array objectAtIndex:row];
     NSArray* packagesForResident=[[CMQPackageDataSourceManager sharedInstance]separateWithArray:[_allResidents objectAtIndex:row]  separator:_isCheckIn?@"CheckIn":@"CheckOut"];
     dest.packagesForResidentByDate=packagesForResident;
     dest.isCheckIn=_isCheckIn;
 }
@end
