//
//  CMQResidentSearchResultsTableViewController.h
//  Communique
//
//  Created by Andre White on 8/24/17.
//  Copyright © 2017 Communique, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CMQSelectResidentTableViewController.h"
@interface CMQResidentSearchResultsTableViewController : UITableViewController
@property(retain, nonatomic)NSArray<id<CMQPackageDataSourceProtocol>>* filteredResidents;
@property(assign, nonatomic)NSInteger tableType;
@end
