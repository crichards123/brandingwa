//
//  CMQPinExitSegue.swift
//  Communique
//
//  Created by Andre White on 10/2/17.
//  Copyright © 2017 Communique, LLC. All rights reserved.
//

import UIKit

public class CMQPinExitSegue: UIStoryboardSegue {
    @objc public var completion:(()->Void)?
    override public func perform() {
        super.perform()
        if let completion = completion{
            completion()
        }
    }
}
