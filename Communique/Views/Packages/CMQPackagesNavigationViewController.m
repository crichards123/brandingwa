//
//  CMQPackagesNavigationViewController.m
//  Communique
//
//  Created by Andre White on 8/28/17.
//  Copyright © 2017 Communique, LLC. All rights reserved.
//

#import "CMQPackagesNavigationViewController.h"

@interface CMQPackagesNavigationViewController ()

@end

@implementation CMQPackagesNavigationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

@end
