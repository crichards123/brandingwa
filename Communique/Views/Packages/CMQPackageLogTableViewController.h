//
//  CMQPackageLogTableViewController.h
//  Communique
//
//  Created by Andre White on 8/30/17.
//  Copyright © 2017 Communique, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CMQPackageLogTableViewController : UITableViewController
@property(retain, nonatomic)id<CMQPackageDataSourceProtocol>dataSource;
@property(retain, nonatomic)NSArray<NSArray<id<CMQPackageDataSourceProtocol>>*>* packagesForResidentByDate;
@property(assign, nonatomic)BOOL isCheckIn;
@end
