//
//  CMQLogCollectionViewController.h
//  Communique
//
//  Created by Andre White on 9/8/17.
//  Copyright © 2017 Communique, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CMQCheckInTableViewController.h"
@interface CMQLogCollectionViewController : UICollectionViewController
@property(retain, nonatomic)NSArray<id<CMQPackageDataSourceProtocol>>* allResidents;
@property(assign, nonatomic)BOOL isCheckIn;
-(void)resetSearch;
@end
