//
//  CMQSelectResidentTableViewController.h
//  Communique
//
//  Created by Andre White on 8/23/17.
//  Copyright © 2017 Communique, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface CMQSelectResidentTableViewController : UITableViewController
@property(retain, nonatomic)NSArray<id<CMQPackageDataSourceProtocol>>* allResidents;
@property(retain, nonatomic)NSString* ocrSearchText;
@end
