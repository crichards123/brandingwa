//
//  CMQPackagesHomeViewController.m
//  Communique
//
//  Created by Andre White on 9/7/17.
//  Copyright © 2017 Communique, LLC. All rights reserved.
//

#import "CMQPackagesHomeViewController.h"
#import "CMQPackageOCRViewController.h"
#import "CMQPackageLogViewController.h"
//#import "CommuniqueiOS/CommuniqueiOS-Swift.h"
@interface CMQPackagesHomeViewController ()
@property (strong, nonatomic)IBOutletCollection(UIButton) NSArray *allButtons;
@property(retain, nonatomic)IBOutlet UIButton* notificationsButton;
@property(assign, nonatomic)AVAuthorizationStatus cameraAuth;
@end

@implementation CMQPackagesHomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    /*if (!CMQAD.apartment.hasUpdatedPackages) {
        
        dispatch_async(dispatch_queue_create("updatePackages", DISPATCH_QUEUE_CONCURRENT), ^{
            [[CMQPackageDataSourceManager sharedInstance]updatePackagesWithRecID];
        });
    }*/
    
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self shouldEnableButtons:YES];
    //[SVProgressHUD showWithStatus:@"Fetching Packages"];
    PFQuery* query = PFQuery.residentsToNotifyQuery;
    [query countObjectsInBackgroundWithBlock:^(int number, NSError * _Nullable error) {
       // [SVProgressHUD dismiss];
        if (!error) {
            self.notificationsButton.hidden = number == 0;
        }else{
            [self errorWithString:error.localizedDescription];
        }
    }];
}
-(void)addButtonShadows{
    for (UIButton* button in _allButtons) {
        button.layer.shadowOpacity = .250;
        button.layer.shadowRadius = 5.0;
        button.layer.shadowColor = [UIColor blackColor].CGColor;
        button.layer.shadowOffset = CGSizeMake(5.0, 5.0);
        button.layer.cornerRadius=5.0;
    }
}
-(void)shouldEnableButtons:(BOOL)shouldEnable{
    for (UIButton* button in _allButtons) {
        button.enabled=shouldEnable;
    }
}
-(void)errorWithString:(NSString*)errorDesc{
    NSLog(@"%@",errorDesc);
    [SVProgressHUD showWithStatus:@"Unable to communicate with server. Please try again later"];
    [SVProgressHUD dismissWithDelay:1.5];
}
- (IBAction)checkInPressed:(id)sender {
    [self shouldEnableButtons:NO];
    CMQPackageDataSourceManager* manager=[CMQPackageDataSourceManager sharedInstance];
    [SVProgressHUD showWithStatus:@"Fetching Residents"];
    [manager getAllResidentsWithCompletion:^(NSError * _Nullable error) {
        [SVProgressHUD dismiss];
        if (!error) {
            _cameraAuth= [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
            switch (_cameraAuth) {
                case AVAuthorizationStatusAuthorized:
                {
                    // The user has previously granted access to the camera.
                    [self performSegueWithIdentifier:@"CheckIn" sender:nil];
                    break;
                }
                case AVAuthorizationStatusNotDetermined:
                {
                    [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^( BOOL granted ) {
                        if ( ! granted ) {
                            dispatch_async(dispatch_get_main_queue(), ^{
                                [self performSegueWithIdentifier:@"CheckInNoCam" sender:nil];
                            });
                        }
                        else{
                            dispatch_async(dispatch_get_main_queue(), ^{
                                [self performSegueWithIdentifier:@"CheckIn" sender:nil];
                            });
                        }
                    }];
                    break;
                }
                default:
                {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self performSegueWithIdentifier:@"CheckInNoCam" sender:nil];
                    });
                    
                    // The user has previously denied access. Remind the user that we need camera access to be useful.
                    //[self showAccessDeniedAlert];
                }
            }
        }
        else{
            [self shouldEnableButtons:YES];
            [self errorWithString:error.localizedDescription];
        }
    }];
}
- (IBAction)checkOutPressed:(id)sender {
    [self shouldEnableButtons:NO];
    CMQPackageDataSourceManager* manager=[CMQPackageDataSourceManager sharedInstance];
    [SVProgressHUD showWithStatus:@"Fetching Packages"];
    [manager getPackagesToCheckOutWithCompletion:^(NSError * _Nullable error) {
        [SVProgressHUD dismiss];
        if (!error) {
            [self performSegueWithIdentifier:@"CheckOut" sender:nil];
        }
        else{
            [self errorWithString:error.localizedDescription];
            [self shouldEnableButtons:YES];
        }
    }];
}
- (IBAction)viewLogPressed:(id)sender {
    [self shouldEnableButtons:NO];
    [self performSegueWithIdentifier:@"ViewLog" sender:nil];
    
    
}
- (IBAction)notificationsPressed:(id)sender {
    [self shouldEnableButtons:NO];
    [self performSegueWithIdentifier:@"Notify" sender:nil];
    
}

-(IBAction)backPressed:(id)sender{
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

-(IBAction)backToHome:(UIStoryboardSegue*)sender{

}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    [self shouldEnableButtons:YES];
    if ([segue.destinationViewController respondsToSelector:@selector(setAllResidents:)]) {
        [segue.destinationViewController performSelector:@selector(setAllResidents:) withObject:[CMQPackageDataSourceManager sharedInstance].residents];
    }
    

}
@end
