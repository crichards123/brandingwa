//
//  Packages.swift
//  Communique
//
//  Created by Andre White on 10/29/18.
//  Copyright © 2018 Communique, LLC. All rights reserved.
//

import Foundation
import AVFoundation

struct GenericShadow:Shadow{
    var color: CGColor = UIColor.black.cgColor
    var offset: CGSize = CGSize.init(width: 5.0, height: 5.0 )
    var opacity: Float = 0.250
    static func apply(layer: CALayer) {
        GenericShadow().apply(layer: layer)
    }
}
class PackageHome:UIViewController{
    @IBOutlet var allButtons:[UIButton]!
    @IBOutlet var notificationsButton:UIButton!
    fileprivate var cameraAuth:AVAuthorizationStatus?
    
    
    func shouldEnableButtons(enabled:Bool){
        for button in allButtons{
            button.isEnabled = enabled
        }
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        for button in allButtons{
            GenericShadow.apply(layer: button.layer)
        }
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.checkNotifyableResidents()
    }
    @IBAction func checkInPressed(sender:UIButton){
        self.shouldEnableButtons(enabled: false)
        FeedbackManager.shared.show(message:"Fetching Residents")
        CMQPackageDataSourceManager.sharedInstance().getAllResidents
            { (error) in
            FeedbackManager.shared.dismiss()
            guard error == nil else {self.shouldEnableButtons(enabled: true);return}
            self.cameraAuth = AVCaptureDevice.authorizationStatus(for: .video)
            switch self.cameraAuth!{
            case .authorized:
                self.authorized()
            case .notDetermined:
                AVCaptureDevice.requestAccess(for: .video, completionHandler: { (granted) in
                    if granted{
                        self.authorized()
                    }else{
                        self.unauthorized()
                    }
                })
            default:
                self.unauthorized()
            }
        }
    }
    private func authorized(){
        DispatchQueue.main.async {
            self.performSegue(withIdentifier: "CheckIn", sender: nil)
        }
    }
    private func unauthorized(){
        DispatchQueue.main.async {
            self.performSegue(withIdentifier: "CheckInNoCam", sender: nil)
        }
    }
    @IBAction func checkOutPressed(sender:UIButton){
        self.shouldEnableButtons(enabled: false)
        FeedbackManager.shared.show(message: "Fetching Packages")
        CMQPackageDataSourceManager.sharedInstance().getPackagesToCheckOut { (error) in
            FeedbackManager.shared.dismiss()
            guard error == nil else {self.shouldEnableButtons(enabled: true);return}
            DispatchQueue.main.async {
                self.performSegue(withIdentifier: "CheckOut", sender: nil)
            }
        }
    }
    @IBAction func viewLogPressed(sender:UIButton){
        self.shouldEnableButtons(enabled: false)
        self.performSegue(withIdentifier: "ViewLog", sender: nil)
    }
    @IBAction func notificationsPressed(sender:UIButton){
        self.shouldEnableButtons(enabled: false)
        self.performSegue(withIdentifier: "Notify", sender: nil)
    }
    @IBAction func backPressed(sender:Any){
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    @IBAction func backToHome(sender:UIStoryboardSegue){}
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        self.shouldEnableButtons(enabled: true)

    }
    ///Checks to see if there are any residents pending notification of a package that has been checked in.
    func checkNotifyableResidents(){
        PFQuery.residentsToNotifyQuery().countObjectsInBackground { (num, error) in
            if error == nil{
                self.notificationsButton.isHidden = num == 0
            }else{
                handle(error: error!, message: "Unable to communicate with server. Please try again later", shouldDisplay: true)
            }
        }
    }
}
