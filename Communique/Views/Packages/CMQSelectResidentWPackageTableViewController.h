//
//  CMQSelectResidentWPackageTableViewController.h
//  Communique
//
//  Created by Andre White on 9/6/17.
//  Copyright © 2017 Communique, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CMQSelectResidentTableViewController.h"
@interface CMQSelectResidentWPackageTableViewController : UITableViewController
@property(retain, nonatomic)NSArray<NSArray<id<CMQPackageDataSourceProtocol>>*>* allResidentsWPackages;
-(void)deliveryUpdate;
@end
