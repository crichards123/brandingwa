//
//  CMQPackageSuccessViewController.h
//  Communique
//
//  Created by Andre White on 9/22/17.
//  Copyright © 2017 Communique, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CMQPackageSuccessViewController : UIViewController
@property(assign, nonatomic)NSInteger packageCount;
@property(retain, nonatomic)NSString* prompt;
@end
