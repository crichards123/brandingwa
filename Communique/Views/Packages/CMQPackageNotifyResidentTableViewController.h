//
//  CMQPackageNotifyResidentTableViewController.h
//  Communique
//
//  Created by Andre White on 8/30/17.
//  Copyright © 2017 Communique, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CMQCheckInTableViewController.h"
@interface CMQPackageNotifyResidentTableViewController : UITableViewController
@property(retain, nonatomic)NSArray<NSArray<CMQPackage*>*>*allResidentsToNotify;
@end
