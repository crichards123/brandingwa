//
//  CMQPackageOCRViewController.h
//  Communique
//
//  Created by Andre White on 8/22/17.
//  Copyright © 2017 Communique, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CMQCheckInTableViewController.h"
@interface CMQPackageOCRViewController : UIViewController
@property(retain, nonatomic)NSArray<id<CMQPackageDataSourceProtocol>>* allResidents;
@end
