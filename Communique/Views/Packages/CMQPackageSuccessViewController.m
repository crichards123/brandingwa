//
//  CMQPackageSuccessViewController.m
//  Communique
//
//  Created by Andre White on 9/22/17.
//  Copyright © 2017 Communique, LLC. All rights reserved.
//

#import "CMQPackageSuccessViewController.h"

@interface CMQPackageSuccessViewController ()
@property(retain, nonatomic)IBOutlet UILabel* promptLabel;
@end

@implementation CMQPackageSuccessViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.layer.cornerRadius=10.0;
    _promptLabel.text=_prompt;
    [self performSelector:@selector(goBackHome) withObject:nil afterDelay:2.0];
}
-(void)goBackHome{
    [self performSegueWithIdentifier:@"BackToHome" sender:nil];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
