//
//  CMQDataSourceManager.swift
//  Communique
//
//  Created by Andre White on 9/18/17.
//  Copyright © 2017 Communique, LLC. All rights reserved.
//

import UIKit
import Parse

@objc public class CMQPackageDataSourceManager: NSObject {
    @objc public var residents: Array<CMQUser>?
    //public var packages: Array<CMQPackage>?
    public var packagesToCheckOut: Array<CMQPackage>?
    public var packagesToCheckIn:Array<CMQPackage>?
    public var packagesToNotify:Array<CMQPackage>?
    public typealias CMQPackageResultBlock = (_ packages: [CMQPackage]?, _ error:Error?)->Void
    public typealias CMQPackageFlagResultBlock = (_ flag: CMQPackageFlag, _ error:Error?)->Void
    public typealias CMQCompletionBlock = (_ error:Error?)->Void
    private static let sharedManager :CMQPackageDataSourceManager = CMQPackageDataSourceManager()
    
    private override init() {
        packagesToCheckIn = Array.init()
        super.init()
    }
    public func updatePackagesWithRecID(){
        let query = PFQuery.packageQuery()
        query.findObjectsInBackground { (objects, error) in
            if let allPackages = objects as? [CMQPackage]{
                for package:CMQPackage in allPackages {
                    package.recipientID = package.recipient?.objectId
                }
                PFObject.saveAll(inBackground: allPackages, block: { (success, error) in
                    if success && error == nil{
                        CMQAPIManager.sharedInstance().apartment?.hasUpdatedPackages = true
                            CMQAPIManager.sharedInstance().apartment?.saveInBackground()
                    }
                })
            }
        }
    }
    /**
     Class Singleton
     */
    @objc public class func sharedInstance()->CMQPackageDataSourceManager{
        return .sharedManager
    }
    enum CMQDataError: NSInteger, Error {
        case fetchError=0
        case checkInError
    }
    /**
     Enum for each package carrier. rawValues mimic the tags in ViewController buttons and the stringValue method returns correct value for Parse backend.
     */
    enum CMQPACKAGECARRIER: NSInteger {
        case USPS = 0
        case UPS
        case FEDEX
        case OTHER
        
        var stringValue:String{
            switch self {
            case .UPS:
                return kPackageCarrierUPS
            case .USPS:
                return kPackageCarrierUSPS
            case .FEDEX:
                return kPackageCarrierFedEx
            case .OTHER:
                return kPackageTypeOther
            }
        }
    }
    /**
     Enum for each package tag. rawValues mimic the tags in ViewController buttons and the stringValue method returns correct value for Parse backend.
     */
    enum CMQPACKAGETAG : NSInteger{
        case NONE = 0
        case PERISHABLE
        case URGENT
        case OTHER
        
        var stringValue:String{
            switch self {
            case .NONE:
                return "None"
            case .PERISHABLE:
                return kPackageTagPerishable
            case .URGENT:
                return kPackageTagUrgent
            case .OTHER:
                return kPackageTagOther
            }
        }
        
    }
    /**
     Enum for each package type. rawValues mimic the tags in ViewController buttons and the stringValue method returns correct value for Parse backend.
 */
    enum CMQPACKAGETYPE : NSInteger{
        case SMALL = 0
        case LARGE
        case ENVELOPE
        case OTHER
        var stringValue:String {
            switch self {
            case .LARGE:
                return kPackageTypeLargeBox
            case .SMALL:
                return kPackageTypeSmallBox
            case .OTHER:
                return kPackageTypeOther
            case .ENVELOPE:
                return kPackageTypeEnvelope
            }
        }
    }
    /**
     Creates a CMQPackage with given parameters representing the raw values of the enum and adds to packagesToCheckIn array. According to the requirements, Packages have the ability to be created with a custom tag, use the tagString closure parameter returning the string to use as a tag.
     
     
     - parameter type: type of package represented by CMQPACKAGETYPE enum rawValue
     - parameter tag: package tag represented by CMQPACKAGETAG enum rawValue
     - parameter carrier: package carrier represented by CMQPACKAGECARRIER enum rawValue
     - parameter tagString: closure with the return value of the tag string. Use this parameter for a custom tag
     -returns: An integer representing the number of packages in the packagesToCheckInArray
     */
    @objc public func createPackage(type:NSInteger, tag:NSInteger, carrier:NSInteger, tagString:()->String)->NSInteger{
        
        guard let packageType = CMQPACKAGETYPE.init(rawValue: type), let packageTag = CMQPACKAGETAG.init(rawValue: tag), let packageCarrier = CMQPACKAGECARRIER.init(rawValue: carrier)
        else { return -1}
        let package = CMQPackage.init(className: "Package")
        package.carrier=packageCarrier.stringValue
        package.packageType=packageType.stringValue
        switch packageTag {
        case .OTHER:
            package.tags=tagString();
        default:
            package.tags=packageTag.stringValue
        }
        packagesToCheckIn?.append(package)
        let count = packagesToCheckIn?.count
        return count!
    }
    ///Clears packagesToCheckIn array
    @objc public func clearCheckInPackages(){
        packagesToCheckIn?.removeAll()
    }
    private func userWith(fullName: String, buildingUnit:String)->CMQUser?{
        guard let possibleResidents = residents else { return nil }
        let usersWithName = possibleResidents.filter { (user) -> Bool in
            return user.fullName == fullName
        }
        let usersWithUnit = usersWithName.filter { (user) -> Bool in
            return user.buildingUnit == buildingUnit
        }
        return usersWithUnit.first
    }
    /// Get CMQUser instance with the users full name
    ///Deprecated
    /*public func userForFullName(fullName:String) -> CMQUser? {
        guard let possibleResidents = residents else { return nil }
        let usersWithName = possibleResidents.filter { (user) -> Bool in
            return user.fullName == fullName
        }
        return usersWithName.first
    }*/
    
    //MARK: Package Array Queries
    /**
     Method to check if user has a package with the given pin.
 
     - parameter residentFullName: name of the resident to check pin for
     - returns: true if resident has correct pin for at least one package, else false
     */
    /*public func userHasPackageWithPin(residentFullName: String , pin: NSNumber)->Bool{
        guard let possiblePackages = try?self.packagesForResidentPendingCheckout(residentFullName: residentFullName)else {return false}
        return possiblePackages.filter { (package) -> Bool in
            return package.pinCode==pin
        }.count>0
    }*/
    /**
     Method to check if user has a package pending delivery.
 
     - parameter residentFullName: name of the resident to query
     - returns: true if resident has delivery pending for at least one package, else false
     */
    /*public func userHasPackagePendingDelivery(residentFullName:String)->Bool{
        guard let possiblePackages = try?self.packagesForResidentPendingCheckout(residentFullName: residentFullName)else{return false}
        return possiblePackages.filter({ (package) -> Bool in
            guard let deliveryStatus = package.deliveryStatus else{
                return false
            }
            return deliveryStatus.uppercased()=="PENDING"
        }).count>0
    }*/
    @objc public func isPendingDelivery(packages:[CMQPackage])->Bool{
        return packages.filter({ (package) -> Bool in
            guard let deliveryStatus = package.deliveryStatus else{
                return false
            }
            return deliveryStatus.uppercased()=="PENDING"
        }).count>0
    }
    @objc public func confirmPin(packages:[CMQPackage], pin:NSNumber)->Bool{
        return packages.filter({$0.pinCode == pin}).count>0
    }
    //MARK: Package Array Filters
    /**
         Method to get all residents with a package pending Checkout. This means that the package has been checked in and the resident has been notified. This method returns an array of an array of packages that are separated by resident (Array<Array<CMQPackage>>?).
     
     
 *//*
    public func residentsWPackagesToCheckOut()->Array<Array<CMQPackage>>?{
        guard let possiblePackages = self.packagesToCheckOut else {
            return nil
        }
        return possiblePackages.separateBy(separator: Array.Separator.Resident)
    }*/
    
    /**
         Helper method to separate an array by a parameter. This helps with the many ways the data is being manipulated and shown in the app.
     
     - parameter array: array of packages to separate
     - parameter separator: String representing the parameter to separate by. See Array.Separator for possible parameters.
 */
    @objc public func separate(array:Array<CMQPackage>, separator:String)->Array<Array<CMQPackage>>?{
        guard let separator = Array.Separator(rawValue: separator) else{return nil}
        return array.separateBy(separator: separator)
    }
    /**
     Method to Search residents array by resident name with given text. Used in CheckIn
     
     - parameter text: String to search array with
     - returns: Array with any residents with names containing text
     */
    
     @objc public func searchResidents(text:String) -> Array<CMQUser>? {
        guard let possibleResidents = residents else { return nil }
        return possibleResidents.filter { (user) -> Bool in
            return user.fullName.uppercased().range(of: text.uppercased()) != nil
        }
    }
    /**
     Method to Search an array of arrays for a given text. This method is used primarily for the search ViewControllers allowing for the lists to be filtered by name.
     - parameter text: String representing the searchText
     - parameter array: An array of arrays to be filtered usually returned by the self.separateBy function.
     - returns: an Array of array of Packages (Array<Array<CMQPackage>>)
     */
    @objc public func searchArray(text:String, array:Array<Array<CMQPackage>>) -> Array<Array<CMQPackage>> {
        return array.filter { (packageArray) -> Bool in
            return packageArray.filter({ (package) -> Bool in
                return package.recipient.fullName.uppercased().range(of: text.uppercased()) != nil
            }).count>0
        }
    }
    /**
 
 Method to get packages that have been already CheckedOut for Log.

     - returns: An array of packages that have already been checked out.
 
 *//*
    public func alreadyCheckedOut()->Array<CMQPackage>?{
        guard let allPackages = packages else {return nil}
        return allPackages.filter({ (package) -> Bool in
            if (package.isDataAvailable){
                return package.pickedUp
            }
            else{
                let possiblePackage = try?package.fetch()
                return (possiblePackage?.pickedUp)!
            }
        })
    }*/
    /**
     
     Method to get packages that have been already CheckedOut for Log given a resident name.
     
     - parameter residentName: String representing the name of the resident to query.
     - returns: an array of packages that have already been checked out for the resident queried or nil if none.
     *//*
    public func alreadyCheckedOut(residentName:String!)->Array<CMQPackage>?{
        guard let allPackages = packages else {return nil}
        let packagesForResident = allPackages.filter({$0.recipient.fullName==residentName})
        return packagesForResident.filter({ (package) -> Bool in
            if (package.isDataAvailable){
                return package.pickedUp
            }
            else{
                let possiblePackage = try?package.fetch()
                return (possiblePackage?.pickedUp)!
            }
        })
    }*/
    /**
     Method to get all packages for resident to checkOut. This method only gets packages from the packagesToCheckOut array
     
     - parameter residentFullName: name of the resident to get packages for
     - returns: Array containing packages for resident if any exist
     
     *//*
    public func packagesForResidentPendingCheckout(residentFullName: String)throws->Array<CMQPackage>{
        guard let possiblePackages = packagesToCheckOut else { throw CMQDataError.fetchError }
        return possiblePackages.filter({ (package) -> Bool in
            //removing blocking calls.
            //let somePackage=package//try?package.fetchIfNeeded()
            //let someResident=try?somePackage?.recipient.fetchIfNeeded() as? CMQUser
            return package.recipient.fullName==residentFullName
        })
    }*/
    // MARK: Get Methods
    /**
     Helper Method to get data from Parse Server. This is template function that takes any closure that can return a PFQuery and a second closure that can handle both the data and the error
     
     - paramter queryClosure: closure that returns a PFQuery Instance with the query parameters already set.
     - parameter completionClosure: closure that handles both the returned data from the query and an error if any.
     - parameter data: Array of objects returned from query
     - paramter error: Error returned by query
     */
    public func getDataFromServer(queryClosure:()->PFQuery<PFObject>, completionClosure:@escaping (_ data:Array<Any>?, _ error:Error?)->Void){
        let query=queryClosure();
        query.countObjectsInBackground { (count, anError) in
            if let anError = anError{
                completionClosure(nil, anError)
            }else{
                //query.limit = Int(count)
                query.findObjectsInBackground { (objects, error) in
                    guard let possibleObjects=objects else{
                        completionClosure(nil,error)
                        return
                    }
                    
                    completionClosure(possibleObjects, nil)
                }
            }
        }
    }
    /**
     Method to get all residents with a completion handler. This query gets all packages objects with the following properties:
     * userRole= "Resident"
     
     - parameter completion: completion closure handling the error if any
     - parameter error: error returned by query if any
     
     */
    
    @objc public func getAllResidents(completion:@escaping ( _ error:Error?)->Void) -> Void {
        let aCompletionClosure={ (objects:Array<Any>?, error:Error?)->Void in
            guard let possibleObjects = objects
                else{
                    completion(error)
                    return
            }
            self.residents=possibleObjects as? Array<CMQUser>
            completion(nil)
        }
        self.getDataFromServer(queryClosure: PFQuery.residentQuery, completionClosure: aCompletionClosure)
        
    }
    @objc public func packagesFor(resident:CMQUser, isCheckIn: Bool, completion: @escaping CMQPackageResultBlock){
        let packageQuery = isCheckIn ? PFQuery.packageQuery() : PFQuery.packagesAlreadyCheckedOutQuery()
        packageQuery.whereKey("recipient", equalTo: resident)
        packageQuery.findObjectsInBackground { (objects, error) in
            completion(objects as? [CMQPackage], error)
        }
    }
    /*
    public func packagesFor(residentName: String, completion:@escaping (_ packages:[Any]?,_ error:Error?)->Void){
        let objectQuery = { () -> PFQuery<PFObject> in
            let query = self.packageQuery()
            let userQuery = CMQUser.query()!
            userQuery.whereKey("fullName", equalTo: residentName)
            query.whereKey("recipient", matchesQuery: userQuery)
            return query
        }
        self.getDataFromServer(queryClosure: objectQuery, completionClosure: completion)
        
        
    }
    public func packagesFor(residentID: String, completion:@escaping (_ packages:[Any]?,_ error:Error?)->Void){
        let objectQuery = { () -> PFQuery<PFObject> in
                let query = self.packageQuery()
                let userQuery = CMQUser.query()!
                userQuery.whereKey("objectId", equalTo: residentID)
                query.whereKey("recipient", matchesQuery: userQuery)
                return query
            }
            self.getDataFromServer(queryClosure: objectQuery, completionClosure: completion)
    }*/
    /*
    public func residentQuery()->PFQuery<PFObject>{
        let query = CMQUser.query()!
        let currentUser=CMQUser.current()!
        query.whereKey(kParseAptComplexIDKey, equalTo: currentUser.complexID)
        query.whereKey("userRole", equalTo: "Resident")
        query.order(byAscending: "fullNameToLowercase")
        return query
    }
    public func packagesToCheckOutQuery()->PFQuery<PFObject>{
        let query = self.packageQuery()
        query.includeKey("parent")
        query.whereKey("isRecipientNotified", notEqualTo: NSNumber.init(value: false))
        query.whereKey("pickedUp", notEqualTo: NSNumber.init(value: true))
        query.order(byDescending: kParseObjectCreateDateKey)
        return query
    }*/
    /**
 
     Method to get packages to Check out with completion closure. This query gets all packages objects with the following properties:
     * isRecipientNotified= true
     * pickedUp= false
     * isCheckedOut= false (deprecated 11/8)
         - parameter completion: completion closure handling the error if any
         - parameter error: error returned by query if any
 */
    @objc public func getPackagesToCheckOut(completion:@escaping (_ error:Error?)->Void) -> Void {
        let aQueryClosure={ () -> PFQuery<PFObject> in
            let query = PFQuery.packagesToCheckOutQuery()
            return query
        }
        //Create Completion Closure
        let aCompletionClosure={ (objects:Array<Any>?, error:Error?)->Void in
            guard let possibleObjects = objects
                else{
                    completion(error)
                    return
            }
            self.packagesToCheckOut=possibleObjects.filter({ (object) -> Bool in
                guard let package = object as? CMQPackage, let _ = package.recipient else{ return false}
                return true
            }) as? Array<CMQPackage>
            completion(nil)
        }
        self.getDataFromServer(queryClosure: aQueryClosure, completionClosure: aCompletionClosure)
    }
    /*
    public func checkOutQuery()->PFQuery<PFObject>{
        let query = self.packageQuery()
        query.includeKey("parent")
        query.whereKey("isRecipientNotified", notEqualTo: NSNumber.init(value: false))
        query.whereKey("pickedUp", notEqualTo: NSNumber.init(value: true))
        query.order(byDescending: kParseObjectCreateDateKey)
        return query
    }*/
    public func checkOutPackages(resident:CMQUser, completion:@escaping CMQPackageResultBlock){
        if let packagesToCheckOut = self.packagesToCheckOut {
            completion(packagesToCheckOut.filter({$0.recipient.objectId == resident.objectId}), nil)
        }else{
            let query = PFQuery.packagesToCheckOutQuery()
            query.whereKey("recipient", equalTo: resident)
            query.findObjectsInBackground(block: { (packages, error) in
                if let error = error as NSError?{
                    if error.code != PFErrorCode.errorCacheMiss.rawValue{
                        completion(packages as? [CMQPackage], nil)
                    }
                }
                completion(packages as? [CMQPackage], error)
            })
        }
    }
    /**
 
     Method to query all packages from Parse server. This method queries parse and loads the self.packages array
 
     - parameter completion: A callback block that is called when all packages are retrieved or if the call results in an error.
 
 *//*
    public func getAllPackages(completion:@escaping (_ error:Error?)->Void) ->Void{
        
        //Create Completion Closure
        let aCompletionClosure={ (objects:Array<Any>?, error:Error?)->Void in
            guard let possibleObjects = objects
                else{
                    completion(error)
                    return
            }
            self.packages=possibleObjects.filter({ (object) -> Bool in
                guard let package = object as? CMQPackage, let _ = package.recipient else{ return false}
                return true
            }) as? Array<CMQPackage>
            completion(nil)
        }
        self.getDataFromServer(queryClosure: packageQuery, completionClosure: aCompletionClosure)
    }*/
    /*
    public func packageQuery()->PFQuery<PFObject>{
        let query = CMQPackage.query()!
        let currentUser=CMQUser.current()!
        if currentUser.tempAptComplexID.count>0 {
            query.whereKey(kParseAptComplexIDKey, equalTo: currentUser.tempAptComplexID)
        }
        else{
            query.whereKey(kParseAptComplexIDKey, equalTo: currentUser.aptComplexID)
        }
        /*let recipientExistsQuery = CMQUser.query()!
        recipientExistsQuery.whereKeyExists("objectId")
        query.whereKey("recipient", matchesQuery: recipientExistsQuery)*/
        query.includeKey("recipient")
        query.order(byDescending: kParseObjectCreateDateKey)
        return query
    }*/
    /**
 
    
    func residentsArrayFromPacakgesArray()->Array<CMQUser>?{
        guard let possiblePackages = self.packages
            else{
                return nil
        }
        //let residents = Set(possiblePackages.compactMap({$0.recipient.fullName}))
        return Set(possiblePackages.map({$0.recipient})).map({$0})
    }*/
    
    
    /**
     
     Method to get packages for residents that have not been notified. This package queries for Package objects with the following properties:
     * isRecipientNotified=false
     
     - parameter completion: completion closure handling the error if any
     - parameter error: error returned by query if any
     */
    /*
    public func getPackagesToNotify(completion:@escaping (_ error:Error?)->Void) -> Void {
        let aQueryClosure={ () -> PFQuery<PFObject> in
            let query = self.packageQuery()
            query.whereKey("isRecipientNotified", equalTo: NSNumber.init(value: false))
            query.includeKey("recipient")
            //query.whereKey("pickedUp", equalTo: NSNumber.init(value: false))
            query.order(byDescending: kParseObjectCreateDateKey)
            return query
        }
        //Create Completion Closure
        let aCompletionClosure={ (objects:Array<Any>?, error:Error?)->Void in
            guard let possibleObjects = objects
                else{
                    completion(error)
                    return
            }
            self.packagesToNotify=possibleObjects.filter({ (object) -> Bool in
                guard let package = object as? CMQPackage, let _ = package.recipient else{ return false}
                return true
            }) as? Array<CMQPackage>
            completion(nil)
        }
        self.getDataFromServer(queryClosure: aQueryClosure, completionClosure: aCompletionClosure)
    }
    */
    //MARK: Parse Functions
    /**
     Method to Check in pacages in packagesToCheckIn Array. Checking in package sets the following properties for the package object:
     * recipient
     * buildingUnit
     * isRecipientNotified
     * userWhoCheckedIn
     * isCheckedIn
     * CheckInDate
     * pinCode
     
     1/10/17
        Also updates PackageFlag object
     
     Upon completion, packagesToCheckIn is emptied.
     - parameter residentFullName: name of resident to be marked as packages recipient
     - parameter completion: completion closure that handles error if any
     - parameter error: error returned by check in if any
 */
    @objc public func checkIn(user:CMQUser, completion:@escaping CMQCompletionBlock){
        guard let thePackages = packagesToCheckIn else {return}
        var storedError:Error?
        for package in thePackages{
            setCheckIn(package: package, user: user)
        }
        let group = DispatchGroup.init()
        let funcCompletion = { (error:Error?)->Void in
            if let error = error{
                storedError = error
            }
            group.leave()
        }
        group.enter()
        self.updateFlag(user: user, completion: funcCompletion)
        group.enter()
        CMQPackage.saveAll(inBackground: thePackages) { (success, error) in
            if success{
                group.leave()
            }else if let error = error{
                storedError = error
                group.leave()
            }
        }
        group.notify(queue: DispatchQueue.main){
            completion(storedError)
        }
        
    }
    public func updateFlag(user:CMQUser, completion:@escaping CMQCompletionBlock){
        let noError = { (flag:CMQPackageFlag?) in
            let newFlag = self.setCheckIn(flag: flag, user: user)
            newFlag.saveInBackground(block: { (success, newError) in
                if success{
                    completion(nil)
                }else if let newError = newError{
                    completion(newError)
                }
            })
        }
        if let flagQuery = packageFlagQuery(userID: user.objectId!, complexID: user.complexID){
            flagQuery.getFirstObjectInBackground(block: { (flag, error) in
                if let error = error, let isCacheMiss = error.isCacheMiss, let unFound = error.isUnFound{
                    if !isCacheMiss && !unFound{
                        completion(error)
                    }else{
                        noError(flag as? CMQPackageFlag)
                    }
                }else{
                    noError(flag as? CMQPackageFlag)
                }
            })
        }
        
    }
    public func setCheckIn(package:CMQPackage, user:CMQUser){
        
        package.aptComplexID = user.complexID
        package.recipient = user
        package.buildingUnit = user.buildingUnit
        package.isRecipientNotified = false
        package.userWhoCheckedIn = CMQUser.current()
        package.isCheckedIn = true
        package.pickedUp = false;
        package.pinCode = 0;
        package.checkedInDate = Date.init(timeIntervalSinceNow: 0)
        
    }
    
    public func setCheckIn(flag:CMQPackageFlag?, user:CMQUser)->CMQPackageFlag{
        if let flag = flag{
            flag.hasPackageCheckedIn = true
            return flag
        }else{
            let packageFlag = CMQPackageFlag.init()
            packageFlag.aptComplexID = user.aptComplexID
            packageFlag.recipientString = user.objectId
            packageFlag.recipient = user
            packageFlag.hasPackageCheckedIn = true
            return packageFlag
        }
    }/*
    public func checkInPackagesWithResident(residentFullName:String, residentBuildingUnit:String, completion: @escaping (_ error:Error?)->Void)->Void{
        guard let possibleUser = self.userWith(fullName: residentFullName, buildingUnit: residentBuildingUnit), let possiblePackages=packagesToCheckIn else { completion(CMQDataError.checkInError)
                    return
            }
        for package in possiblePackages {
            let currentUser = CMQUser.current()!
            
            if(currentUser.tempAptComplexID.count>0){
                package.aptComplexID=currentUser.tempAptComplexID
            }
            else{
                package.aptComplexID=currentUser.aptComplexID
            }
            package.recipient=CMQUser.init(withoutDataWithObjectId: possibleUser.objectId)
            package.buildingUnit=possibleUser.buildingUnit
            package.isRecipientNotified=false
            package.userWhoCheckedIn = PFUser.init(withoutDataWithObjectId: CMQUser.current().objectId)
            package.isCheckedIn=true
            package.pickedUp=false;
            package.pinCode=0;
            package.checkedInDate=NSDate.init(timeIntervalSinceNow: 0) as Date!
            
            
        }
        if let query = packageFlagQuery(userID: possibleUser.objectId!, complexID: possibleUser.complexID){
            query.findObjectsInBackground(block: { (objects, error) in
                if let error = error{
                    completion(error)
                }
                else{
                    if let objects = objects, let object = objects.first, let flag = object as? CMQPackageFlag{
                        //Package Flag already created save package
                        flag.hasPackageCheckedIn = true
                        CMQPackage.saveAll(inBackground: possiblePackages) { (success, error) in
                            if((error == nil)){
                                self.packagesToCheckIn!.removeAll()
                                flag.saveInBackground(block: { (aSuccess, someError) in
                                    completion(error)
                                })
                            }else{
                                completion(error)
                            }
                        }
                    }
                    else{
                        //create package flag, save packageFlag and package
                        let packageFlag = CMQPackageFlag.init()
                        packageFlag.aptComplexID = possibleUser.aptComplexID
                        packageFlag.recipientString = possibleUser.objectId
                        packageFlag.recipient = possibleUser
                        packageFlag.hasPackageCheckedIn = true
                        CMQPackage.saveAll(inBackground: possiblePackages) { (success, error) in
                            if((error == nil)){
                                self.packagesToCheckIn!.removeAll()
                                packageFlag.saveInBackground(block: { (newSuccess, newError) in
                                    completion(error)
                                })
                                
                            }else{
                                completion(error)
                            }
                        }
                    }
                }
                
            })
        }
        
    }*/
    public func packageFlagQuery(userID:String, complexID: String)->PFQuery<PFObject>?{
        let query = CMQPackageFlag.query()
        query?.whereKey("recipientString", equalTo: userID)
        query?.whereKey("aptComplexID", equalTo: complexID)
        return query
    }
    public func packageFlagQuery(complexID:String)->PFQuery<PFObject>?{
        let query = CMQPackageFlag.query()
        query?.whereKey("aptComplexID", equalTo: complexID)
        return query
    }
    /*
    public func getPackageFlags(completion:@escaping (_ flags:[Any]?,_ error:Error?)->Void){
        let flagQuery = {
            ()->PFQuery<PFObject> in
            return self.packageFlagQuery(complexID: CMQUser.current().tempAptComplexID.count>0 ? CMQUser.current().tempAptComplexID:CMQUser.current().aptComplexID)!
        }
        /*let someCompletion = { (flags:[Any]?, error:Error?) in
            if let error = error{
                completion(nil,error)
                
            }else if let flags = flags {
                let iDs = flags.compactMap({ (flag) -> String? in
                    guard let flag = flag as? CMQPackageFlag else {return nil}
                    return flag.recipientString
                })
                completion(iDs,error)
            }
        }*/
        getDataFromServer(queryClosure:flagQuery, completionClosure: completion)
        
    }*/
    
    @objc public func checkOutPackages(packages:[CMQPackage], signature:UIImage?, completion:@escaping CMQCompletionBlock){
        let package = self.packageParent(packages: packages)
        self.setCheckOut(package: package, signature: signature)
        let group = DispatchGroup.init()
        var storedError:Error?
        let funcCompletion = { (error:Error?)->Void in
            if let error = error{
                storedError = error
            }
            group.leave()
        }
        group.enter()
        self.updateFlag(package: package, completion: funcCompletion)
        group.enter()
        self.updatePackageMessage(packagePin: package.pinCode.stringValue, completion: funcCompletion)
        group.enter()
        package.saveInBackground(block: { (success, error) in
            if success{
                group.leave()
            }else if let error = error{
                storedError = error
                group.leave()
            }
        })
        group.notify(queue: DispatchQueue.main){
            completion(storedError)
        }
        
    }
    /**
        returns parent package for set of packages or the first of the packages if there is no parent
    */
    public func packageParent(packages:[CMQPackage])->CMQPackage{
        if let parentID =  Set(packages.compactMap({ (package) -> String? in
            guard let packageParent = package.parent else {return nil}
            return packageParent.objectId
        })).first, let parentPackage = packages.filter({$0.objectId == parentID}).first{
            return parentPackage
        }
        else{
            return packages.first!
        }
    }
    /**
     
     Method to set checkout parameters for a package.
*/
    public func setCheckOut(package:CMQPackage, signature:UIImage?){
        package.pickedUp = true
        if let packageStatus = package.deliveryStatus{
            if packageStatus.uppercased() == "PENDING"{
                package.deliveryStatus = "delivered"
            }
        }
        package.userWhoCheckedOut = CMQUser.current()
        package.checkedOutDate = Date.init(timeIntervalSinceNow: 0)
        if let signature = signature{
            package.signaturePhoto = signature.toParse()
        }
        
    }
    /**
     
     Method to update packageFlag after checkout has been completed
 
 */
    public func updateFlag(package:CMQPackage, completion:@escaping CMQCompletionBlock){
        let noError = { (flag:CMQPackageFlag?) in
            let newFlag = self.setCheckOut(flag: flag, recipientID: package.recipientID)
            newFlag.saveInBackground(block: { (success, newError) in
                if success{
                    completion(nil)
                }else if let newError = newError{
                    completion(newError)
                }
            })
        }
        if let flagQuery = packageFlagQuery(userID: package.recipientID, complexID: package.recipient.complexID){
            flagQuery.getFirstObjectInBackground(block: { (flag, error) in
                if let error = error, let isCacheMiss = error.isCacheMiss, let unFound = error.isUnFound{
                    if !isCacheMiss && !unFound{
                        completion(error)
                    }else{
                        noError(flag as? CMQPackageFlag)
                    }
                }else{
                    noError(flag as? CMQPackageFlag)
                }
            })
        }
    }
    /**
 
    Method to Update a package Message after checkOut has been completed
 
    */
    public func updatePackageMessage(packagePin:String, completion: @escaping CMQCompletionBlock){
        let noError = { (message:PFObject?) in
            if let message = message as? CMQMessage{
                message.deliveryStatus = "DELIVERED OR PICKED UP"
                message.saveInBackground(block: { (success, newError) in
                    if success{
                        completion(nil)
                    }else if let newError = newError{
                        completion(newError)
                    }
                })
            }
        }
        let messageQuery = CMQMessage.query()
        let messageString = "You have a package to pick up at the office. Your pin code is " + packagePin
        messageQuery?.whereKey("messageContent", contains: messageString)
        messageQuery?.getFirstObjectInBackground(block: { (returnedMessage, error) in
            if let error = error as NSError?, let cacheMiss = error.isCacheMiss, let unFound = error.isUnFound {
                    if !cacheMiss && !unFound{
                        completion(error)
                    }
                    else{
                        noError(returnedMessage)
                    }
                }else{
                    noError(returnedMessage)
            }
        })
    }
    /**
        Method to set parameters for checkout
 
    */
    public func setCheckOut(flag:CMQPackageFlag?, recipientID:String)->CMQPackageFlag{
        if let flag = flag{
            flag.hasPackageCheckedOut = true
            return flag;
        }else{
            let packageFlag = CMQPackageFlag.init()
            let recipient = CMQUser.init(withoutDataWithObjectId: recipientID)
            packageFlag.aptComplexID = CMQUser.current().complexID
            packageFlag.hasPackageCheckedOut = true
            packageFlag.recipient = recipient
            packageFlag.recipientString = recipientID
            return packageFlag
        }
        
    }
    /*
     Deprecated 2/2/18
     /**
     Method to check out packages for a resident. Checking out a package sets the following properties for the package object
     * isCheckedOut (deprecated 11/8)
     * userWhoCheckedOut
     * checkedOutDate
     * packagePhoto
     * parentPackage
     * deliveryStatus
     * pickedUp
     1/10/17 PackageFlag logic added
     It also queries for the message using the pinCode and sets deliveryStatus to DELIVERED OR PICKED UP
     - parameter residentFullName: recipient name for package
     - parameter signature: UIImage representing the signature of the resident
     - parameter completion: closure handling the error, if any
     - parameter error: error returned by checkout query
     */
     
    public func checkOutPackagesForResident(residentFullName: String, signature:UIImage?, completion:@escaping (_ error:Error?)->Void)throws->Void{
        guard let possiblePackages=try?self.packagesForResidentPendingCheckout(residentFullName: residentFullName)else{
            throw CMQDataError.fetchError
        }
        let aPackage = possiblePackages.first
        let getApackage = { () -> CMQPackage in
            if let parentPackage = aPackage?.parent{
                if let possibleParent = possiblePackages.filter({ (somePackage) -> Bool in
                    return somePackage.objectId == parentPackage.objectId
                }).first{
                    return possibleParent
                }
                else{
                    _ = try? parentPackage.fetchIfNeeded()
                    return parentPackage
                }
            }
            else{
                return aPackage!
            }
        }
        let package = getApackage()
        //package.isCheckedOut=true
        package.pickedUp=true
        //_ = try? package.fetchIfNeeded()
        if let packageStatus = package.deliveryStatus
        {
            if packageStatus.uppercased() == "PENDING"{
                package.deliveryStatus = "delivered"
            }
        }
        package.userWhoCheckedOut=PFUser.init(withoutDataWithObjectId: PFUser.current()?.objectId)
        package.checkedOutDate=NSDate.init(timeIntervalSinceNow: 0) as Date!
        if let possibleSignature = signature{
            var fileName=String.init(format: "%f", NSDate.init(timeIntervalSinceNow: 0).timeIntervalSince1970)
            fileName=fileName.md5().appending("png")
            package.signaturePhoto=PFFile.init(name: fileName, data: UIImagePNGRepresentation(possibleSignature)!)
        }
        if let recipient = package.recipient, let flagQuery = packageFlagQuery(userID: recipient.objectId!, complexID: recipient.aptComplexID){
            flagQuery.findObjectsInBackground(block: { (objects, error) in
                if let error = error{
                    completion(error)
                }
                else{
                    if let objects = objects, let object = objects.first, let flag = object as? CMQPackageFlag{
                        flag.hasPackageCheckedOut = true
                        let messageQuery = CMQMessage.query()
                        let messageString = "You have a package to pick up at the office. Your pin code is " + package.pinCode.stringValue
                        
                        messageQuery?.whereKey("messageContent", contains: messageString)
                        messageQuery?.getFirstObjectInBackground(block: { (object, someError) in
                            if let possibleError = someError{
                                completion(possibleError)
                            }else{
                                let message = object as! CMQMessage
                                message.deliveryStatus = "DELIVERED OR PICKED UP"
                                message.saveInBackground(block: { (someSuccess, anotherError) in
                                    if let possibleError = anotherError{
                                        completion(possibleError)
                                    }else{
                                        package.saveInBackground { (success, error) in
                                            if let error = error {
                                                completion(error)
                                            }else{
                                                flag.saveInBackground(block: { (_,anError) in
                                                    completion(anError)
                                                })
                                            }
                                        }
                                    }
                                })
                            }
                        })
                        
                    }else{
                        let packageFlag = CMQPackageFlag.init()
                        packageFlag.aptComplexID = recipient.aptComplexID
                        packageFlag.hasPackageCheckedOut = true
                        packageFlag.recipient = recipient
                        packageFlag.recipientString = recipient.objectId
                        let messageQuery = CMQMessage.query()
                        let messageString = "You have a package to pick up at the office. Your pin code is " + package.pinCode.stringValue
                        
                        messageQuery?.whereKey("messageContent", contains: messageString)
                        messageQuery?.getFirstObjectInBackground(block: { (object, someError) in
                            if let possibleError = someError{
                                completion(possibleError)
                            }else{
                                let message = object as! CMQMessage
                                message.deliveryStatus = "DELIVERED OR PICKED UP"
                                message.saveInBackground(block: { (someSuccess, anotherError) in
                                    if let possibleError = anotherError{
                                        completion(possibleError)
                                    }else{
                                        package.saveInBackground { (success, error) in
                                            if let error = error {
                                                completion(error)
                                            }else{
                                                packageFlag.saveInBackground(block: { (_,anError) in
                                                    completion(anError)
                                                })
                                            }
                                        }
                                    }
                                })
                            }
                        })
                    }
                }
            })
            
        }
    
        
    }*/
    /**
     Method to notify a resident of a new package. Notifying a resident sets the following properties for a package:
     * isRecipientNotified
     * notifyDate - removed 9/26/17 per Cody
     
     - parameter indices: Array of NSIndexPaths representing the indices of the residents to notify. This array is taken directly from the ViewController.
     - parameter completion: completion closure to handle error if any
     - parameter error: error returned from the notify query
 */
 /*
    public func notifyResidents(residentNames:Array<String>, completion:@escaping (_ error:Error?)->Void) -> Void {
        //Array from the instance calling this method is packagesToNotify separated by Resident name, which is type <Array<Array<CMQPackage>>
        //Filter that array by checking if the first object (CMQPackage) in each array has a recipient with a name in the residentName array.
        //Use map to create an array of the first package in each array.
        let packagesForResidents = packagesToNotify?.separateBy(separator: Array.Separator.Resident).filter({ (packageArray) -> Bool in
            return residentNames.filter({$0==packageArray.first?.recipient.fullName}).count>0
        })/*.map({ (packages) -> CMQPackage in
            return packages.first!
        })*/
        for residentPackageArray:Array<CMQPackage> in packagesForResidents!{
            for package:CMQPackage in residentPackageArray{
                package.isRecipientNotified=true
            }
            CMQPackage.saveAll(inBackground: residentPackageArray) { (success, error) in
                completion(error)
            }
        }
        /*for package:CMQPackage in packagesForResidents! {
            package.isRecipientNotified=true;
        }*/
        
    }*/
    @objc public func update(completion: @escaping (_ error:Error?)->Void )->Void{
        CMQPackage.fetchAll(inBackground: self.packagesToCheckOut) { (_, error) in
            completion(error)
        }
    }
    public func notify(residents: [CMQUser], completion: @escaping CMQCompletionBlock){
        var storedError: Error?
        let group = DispatchGroup.init()
        let someCompletion = { (packages: [CMQPackage]?, error:Error?)->Void in
            if let packages = packages{
                for package in packages{
                    package.isRecipientNotified = true
                }
                CMQPackage.saveAll(inBackground: packages, block: { (success, error) in
                    if success{
                        group.leave()
                    }else if let error = error{
                        storedError = error
                        group.leave()
                    }
                })
            }
        }
        for resident in residents{
            group.enter()
            self.packagesToNotifyFor(resident: resident, completion: someCompletion)
        }
        group.notify(queue: DispatchQueue.main) {
            completion(storedError)
        }
        
    }
    public func packagesToNotifyFor(resident:CMQUser, completion: @escaping CMQPackageResultBlock){
        let query = PFQuery.packagesToNotifyQuery()
        query.whereKey("recipient", equalTo: resident)
        query.findObjectsInBackground { (objects, error) in
            completion (objects as? [CMQPackage], error)
        }
    }
    
    // MARK: Message Methods that'll probably be moved to a message DataSource for separation of concerns.
    
    
    /**
     Method to check if the delivery image should be used based on the message Delivery Status. Currently if status=="NO" the red box image is returned and if status=="PENDING" the green box image is returned.
     - parameter message: The message to query
     - returns: UIImage if the status mathces any status needed to return an image else nil.
 
     */
    /*func deliveryImage(message:CMQMessage)->UIImage?{
        guard let possibleStatus =  message.deliveryStatus else{ return nil}
        //if message.package?.isCheckedOut == true {
        //    return nil
        //}
        switch possibleStatus {
        case "NO":
            return #imageLiteral(resourceName: "packageDelivery")
        case "PENDING":
            return #imageLiteral(resourceName: "packagePending")
        default:
            return nil;
        }
    }*/
    /**
     Method to query if a message should show the prompt for delivery. Currently if message.package.deliveryStatus contains DELIVERED or PICKED UP or PENDING the function will return false if it doesn't and message.packageNoti is true the function will return true
     - parameter message: the message to query
     - returns: true if the message should show the prompt else false
 */
  /*  func shouldShowPrompt(message:CMQMessage)->Bool{
        guard let messagePackage = message.package else{
            return false
        }
        let possibleStatus = messagePackage.deliveryStatus
        return /*message.messageContent.range(of: "You have a package") != nil &&*/ message.packageNoti == true && possibleStatus?.range(of: "DELIVERED") == nil && possibleStatus?.range(of: "PICKED UP") == nil && possibleStatus?.range(of: "PENDING") == nil
    }*/
    /**
         Method to set delivery status on a message, it simply sets the message.deliveryStatus to either YES or NO and saves the object on the server.
     - parameter message: the message to set
     - parameter response: true if setting deliveryStatus to YES false if setting to NO
     - parameter completion: the callback block which is called after completion or in the event that an error occurred.
 */
  /*  func setDelivery(message:CMQMessage, response: Bool, completion:@escaping (_ error:Error?)->Void) -> Void {
        if response == true {
            message.deliveryStatus="YES"
        }
        else{
            message.deliveryStatus="NO"
        }
        message.saveInBackground(block: { (messageSuccess, messageError) in
            if messageSuccess{
                completion(nil)
            }
            else{
                completion(messageError!)
            }
        })
    }*/
}
@objc public extension PFQuery{
    
    class func packageQuery()->PFQuery<PFObject>{
        let query = CMQPackage.query()!
        let currentUser=CMQUser.current()!
        query.whereKey(kParseAptComplexIDKey, equalTo: currentUser.complexID)
        query.includeKey("recipient")
        query.includeKey("parent")
        query.order(byDescending: kParseObjectCreateDateKey)
        return query
    }
    class func residentQuery()->PFQuery<PFObject>{
        let query = CMQUser.query()!
        let currentUser=CMQUser.current()!
        query.whereKey(kParseAptComplexIDKey, equalTo: currentUser.complexID)
        query.limit = 20000
        query.whereKey("userRole", equalTo: "Resident")
        query.order(byAscending: "fullNameToLowercase")
        return query
    }
    class func residentsToNotifyQuery()->PFQuery<PFObject>{
        let query = PFQuery.residentQuery()
        query.whereKey("objectId", matchesKey: "recipientID", in: PFQuery.packagesToNotifyQuery())
        query.limit = 20000
        return query
    }
    class func packagesToNotifyQuery()->PFQuery<PFObject>{
        let packageQuery = PFQuery.packageQuery()
        packageQuery.whereKey("isRecipientNotified", equalTo: false)
        return packageQuery
    }
    class func packagesToCheckOutQuery()->PFQuery<PFObject>{
        let query = PFQuery.packageQuery()
        query.whereKey("isRecipientNotified", notEqualTo: NSNumber.init(value: false))
        query.whereKey("pickedUp", notEqualTo: NSNumber.init(value: true))
        return query
    }
    class func packagesAlreadyCheckedOutQuery()->PFQuery<PFObject>{
    let query = PFQuery.packageQuery()
        query.whereKeyExists("checkedOutDate")
        return query
    }
}
public extension UIImage{
    public func toParse()->PFFile?{
        return PFFile.init(name: String.fileName(), data: UIImage.jpegData(self)(compressionQuality: 0.0)!)
    }
}
public extension String{
    static func fileName()->String{
        var string = String.init(format: "%f", NSDate.init(timeIntervalSinceNow: 0).timeIntervalSince1970)
        string = string.md5()
        string.append("jpg")
        return string
    }
}

