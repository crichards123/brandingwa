//
//  CMQPackagesCheckOutViewController.m
//  Communique
//
//  Created by Andre White on 8/29/17.
//  Copyright © 2017 Communique, LLC. All rights reserved.
//

#import "CMQPackagesCheckOutViewController.h"
#import "CMQPackageCheckOutPinPresentationController.h"
#import "CMQSuccessPresentationController.h"
#import "CMQPackageDetailsTableViewCell.h"
#import "CMQCheckOutEnterPinViewController.h"
#import "CMQPackageSignatureViewController.h"
#import "CMQPackageSuccessViewController.h"
//#import "CommuniqueiOS/CommuniqueiOS-Swift.h"
@interface CMQPackagesCheckOutViewController ()<UIViewControllerTransitioningDelegate>
@property (weak, nonatomic) IBOutlet UIView* residentInfoView;
@property (weak, nonatomic) IBOutlet UIView *blankView;
@property (weak, nonatomic) IBOutlet UILabel *residentNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *residentBuildingLabel;
@property (weak, nonatomic) IBOutlet UIButton *acceptButton;

@property (strong, nonatomic) IBOutletCollection(UIStackView) NSArray *packageStacks;

@property(assign, nonatomic) BOOL pendingDelivery;

@end

@implementation CMQPackagesCheckOutViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initView];
    //[self performSegueWithIdentifier:@"Pin" sender:nil];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)initView{
    
    
    CMQPackage<CMQPackageDataSourceProtocol>* package= _packagesForResident.firstObject;
    _pendingDelivery=[[CMQPackageDataSourceManager sharedInstance]isPendingDeliveryWithPackages:self.packagesForResident];
    if (_pendingDelivery) {
        [_acceptButton setTitle:@"DELIVER" forState:UIControlStateNormal];
    }
    _residentNameLabel.text=package.residentName;
    _residentBuildingLabel.text=package.residentBuilding;
    NSArray<NSArray*>* array= [[CMQPackageDataSourceManager sharedInstance]separateWithArray:_packagesForResident separator:@"Type"];
    
    for (UIStackView* stack in _packageStacks) {
        if (stack.tag>=array.count) {
            stack.hidden=YES;
        }
        else{
            stack.hidden=NO;
            UILabel* typeLabel=(UILabel*)[stack.subviews objectAtIndex:0];
            UILabel* quantityLabel=(UILabel*)[stack.subviews objectAtIndex:1];
            quantityLabel.text=[NSNumber numberWithInteger:[array objectAtIndex:stack.tag].count].stringValue;
            typeLabel.text= [(CMQPackage*)[array objectAtIndex:stack.tag].firstObject packageType];
        }
    }
    _blankView.hidden=YES;
}
-(UIPresentationController*)presentationControllerForPresentedViewController:(UIViewController *)presented presentingViewController:(UIViewController *)presenting sourceViewController:(UIViewController *)source{
    if ([presented isKindOfClass:[CMQPackageSuccessViewController class]]) {
        CMQSuccessPresentationController* controller=[[CMQSuccessPresentationController alloc]initWithPresentedViewController:presented presentingViewController:presenting ];
        return controller;
    }
    else{
        CMQPackageCheckOutPinPresentationController* controller=[[CMQPackageCheckOutPinPresentationController alloc]initWithPresentedViewController:presented presentingViewController:presenting];
        return controller;
    }
}
- (IBAction)acceptButtonPressed:(id)sender {
    _acceptButton.enabled= NO;
    if (!_pendingDelivery) {
        [self performSegueWithIdentifier:@"Signature" sender:nil];
    }
    else{
        CMQPackage<CMQPackageDataSourceProtocol>* package= _packagesForResident.firstObject;
        [[CMQPackageDataSourceManager sharedInstance] checkOutPackagesWithPackages:_packagesForResident signature:nil completion:^(NSError * _Nullable error) {
            if (!error) {
                //success
                [self performSegueWithIdentifier:@"Success" sender:nil];
                _acceptButton.enabled= YES;
                //[SVProgressHUD showSuccessWithStatus:@"Successfully Checked out"];
                //[self performSelector:@selector(backToHome) withObject:nil afterDelay:3];
            }
            else{
                _acceptButton.enabled = YES;
                [self errorWithString:error.localizedDescription];
            }
        }];
    }
}

-(IBAction)correctPinEntry:(UIStoryboardSegue*)sender{
    _residentInfoView.hidden=NO;
    [self initView];
}
-(IBAction)cancelPinEntry:(UIStoryboardSegue*)sender{
    
    
}
-(IBAction)cancelSignature:(UIStoryboardSegue*)sender{
    
}
-(IBAction)checkOut:(UIStoryboardSegue*)sender{
    CMQPackageSignatureViewController* source=sender.sourceViewController;
    CMQPackage<CMQPackageDataSourceProtocol>* package= _packagesForResident.firstObject;
    [[CMQPackageDataSourceManager sharedInstance]checkOutPackagesWithPackages:_packagesForResident signature:source.signatureView.tempImageView.image completion:^(NSError * _Nullable error) {
        if (!error) {
            //success
            [self performSegueWithIdentifier:@"Success" sender:nil];
            //[SVProgressHUD showSuccessWithStatus:@"Successfully Checked out"];
            //[self performSelector:@selector(backToHome) withObject:nil afterDelay:3];
        }
        else{
            [self errorWithString:error.localizedDescription];
        }
    }];
}
-(void)errorWithString:(NSString*)errorDesc{
    NSLog(@"%@",errorDesc);
    [SVProgressHUD showErrorWithStatus:@"Unable to Check Out. Please Try again later"];
}
-(void)backToHome{
    [self performSegueWithIdentifier:@"BackToHome" sender:nil];
    
}
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.destinationViewController respondsToSelector:@selector(setPackagesForResident:)]) {
        [segue.destinationViewController performSelector:@selector(setPackagesForResident:) withObject:self.packagesForResident];
    }
    if ([segue.identifier isEqualToString:@"Pin"]) {
        CMQCheckOutEnterPinViewController* dest=segue.destinationViewController;
        dest.transitioningDelegate=self;
        dest.modalPresentationStyle=UIModalPresentationCustom;
    }
    if([segue.identifier isEqualToString:@"Signature"]){
        CMQAppDelegate *appDelegate = (CMQAppDelegate*)[UIApplication sharedApplication].delegate;
        [appDelegate setShouldRotate:YES];
    }
    if ([segue.identifier isEqualToString:@"Success"]) {
        segue.destinationViewController.transitioningDelegate=self;
        segue.destinationViewController.modalPresentationStyle=UIModalPresentationCustom;
        CMQPackageSuccessViewController* dest=segue.destinationViewController;
        dest.prompt=@"Your packages have been";
    }
}


@end
