//
//  CMQSelectResidentTableViewController.m
//  Communique
//
//  Created by Andre White on 8/23/17.
//  Copyright © 2017 Communique, LLC. All rights reserved.
//

#import "CMQSelectResidentTableViewController.h"
#import "CMQResidentSearchResultsTableViewController.h"
#import "CMQPackageDetailsTableViewCell.h"
#import "CMQCheckInTableViewController.h"
//#import "CommuniqueiOS/CommuniqueiOS-Swift.h"

@interface CMQSelectResidentTableViewController ()<UISearchResultsUpdating, UISearchControllerDelegate, UISearchBarDelegate>
@property (weak, nonatomic) IBOutlet UITextField *searchField;
@property(retain, nonatomic)UISearchController* searchController;
@property(retain, nonatomic)CMQResidentSearchResultsTableViewController*
resultsController;
@property(retain, nonatomic)IB_DESIGNABLE UIColor* textColor;
@end

@implementation CMQSelectResidentTableViewController

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUp];
}
-(void)initNavBarWithSearchBar:(UISearchBar*)searchBar{
    searchBar.tintColor=[UIColor whiteColor];
    UITextField* textField= (UITextField*)[searchBar valueForKey:@"searchField"];
    UIView* backgroundView= textField.subviews.firstObject;
    backgroundView.backgroundColor= [UIColor whiteColor];
    backgroundView.layer.cornerRadius= 10;
    backgroundView.clipsToBounds=YES;
}
-(void)setUp{
    //search Controller setUp
    NSBundle* comBundle = [NSBundle bundleForClass:[CMQSelectResidentTableViewController class]];
    _resultsController=[[UIStoryboard storyboardWithName:@"Packages" bundle:comBundle]instantiateViewControllerWithIdentifier:@"SearchResults"];
    _resultsController.tableView.delegate=self;
    _searchController=[[UISearchController alloc]initWithSearchResultsController:_resultsController];
    _searchController.searchResultsUpdater=self;
    [_searchController.searchBar sizeToFit];
    _searchController.delegate = self;
    _searchController.dimsBackgroundDuringPresentation = NO; // default is YES
    if (@available(iOS 11.0, *)) {
        self.navigationItem.searchController=_searchController;
        [self initNavBarWithSearchBar:_searchController.searchBar];
        self.navigationItem.hidesSearchBarWhenScrolling=_ocrSearchText!=nil;
    }
    else{
        self.tableView.tableHeaderView=_searchController.searchBar;
    }
    _searchController.searchBar.placeholder=@"Resident Name";
    if (_ocrSearchText) {
        //here from the segue indicating more than one result has come from the label scan. Add the text to the search bar and perform the search
        _searchController.searchBar.text=_ocrSearchText;
        _searchController.active=YES;
        [_searchController.searchResultsUpdater updateSearchResultsForSearchController:_searchController];
        
    }
}
-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    
}
#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.allResidents.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 80;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CMQPackageDetailsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"reuse" forIndexPath:indexPath];
    
    cell.residentNameLabel.text=[self.allResidents objectAtIndex:indexPath.row].residentName;
    [cell.buildingButton setTitle:[self.allResidents objectAtIndex:indexPath.row].residentBuilding forState:UIControlStateNormal];
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    id sender;
    if ([tableView isEqual:self.tableView]) {
        sender=[self.allResidents objectAtIndex:indexPath.row];
    }
    else{
        sender=[_resultsController.filteredResidents objectAtIndex:indexPath.row];
    }
    [self performSegueWithIdentifier:@"Select" sender:sender];
}
-(void)updateSearchResultsForSearchController:(UISearchController *)searchController{
    
    //_resultsController.tableType=0;
    _resultsController.filteredResidents=[[CMQPackageDataSourceManager sharedInstance]searchResidentsWithText:searchController.searchBar.text];
    [_resultsController.tableView reloadData];
}

 #pragma mark - Navigation
-(IBAction)backFromCheckIn:(UIStoryboardSegue*)sender{
    
}
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {

     //CMQCheckInTableViewController* dest=segue.destinationViewController;
     if ([segue.destinationViewController respondsToSelector:@selector(setDataSource:)]) {
         [segue.destinationViewController performSelector:@selector(setDataSource:) withObject:sender];
     }
     //dest.dataSource=sender;

 }
@end
