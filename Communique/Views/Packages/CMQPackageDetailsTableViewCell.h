//
//  CMQPackageDetailsTableViewCell.h
//  Communique
//
//  Created by Andre White on 9/1/17.
//  Copyright © 2017 Communique, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface CMQPackageDetailsTableViewCell : PFTableViewCell
@property(weak, nonatomic)IBOutlet UILabel* residentNameLabel;
@property(weak, nonatomic)IBOutlet UILabel* buildingLabel;
@property(weak, nonatomic)IBOutlet UIButton* buildingButton;
@property(weak, nonatomic)IBOutlet UIImageView* checkView;
@property(weak, nonatomic)IBOutlet UIStackView* labelStack;
@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *packageLabels;
@property(weak, nonatomic)IBOutlet UILabel* deliveryLabel;
@property(weak, nonatomic)IBOutlet UITableView* packageTable;
@property(weak, nonatomic)IBOutlet UIImageView* deliveryImageView;
-(void)configureCheckOutCellWithPackgesbyType:(NSArray<NSArray<CMQPackage*>*>*)dataSource;
-(void) configureWithUser:(CMQUser*)user andPackages:(NSArray<NSArray<CMQPackage*>*>*)packages;
-(void)reset;
@end
