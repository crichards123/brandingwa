//
//  CMQLoginViewController.m
//  Communique
//
//  Created by Chris Hetem on 9/15/14.
//  Copyright (c) 2014 WaveRider, Inc. All rights reserved.
//

#import "CMQOldLoginViewController.h"
#import "UIView+Animations.h"
#import "DZNWebViewController.h"
#import <MessageUI/MessageUI.h>
#import "UIApplication+AppInfo.h"

#define SubmitButtonVerticalSpaceMin            20
#define SubmitButtonVerticalSpaceMax            50

@interface CMQOldLoginViewController () <CMQAPIClientDelegate, UITextFieldDelegate, MFMailComposeViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UILabel *loginLabel;
@property (weak, nonatomic) IBOutlet UIButton *helpButton;
@property (weak, nonatomic) IBOutlet UITextField *usernameTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UIButton *submitButton;
@property (weak, nonatomic) IBOutlet UIButton *forgotPasswordButton;
@property (strong, nonatomic) IBOutlet UIView *forgotPasswordView;
@property (strong, nonatomic) IBOutlet UITextField *forgotPasswordEmailTextField;
@property (weak, nonatomic) IBOutlet UIButton *forgotPasswordCancelButton;
@property (weak, nonatomic) IBOutlet UIButton *forgotPasswordSubmitButton;
@property (weak, nonatomic) IBOutlet UILabel *legalityLabel1;
@property (weak, nonatomic) IBOutlet UILabel *legalityLabel2;
@property (weak, nonatomic) IBOutlet UIButton *tosButton;
@property (weak, nonatomic) IBOutlet UIButton *ppButton;
@property (strong, nonatomic) DZNWebViewController *webViewController;

@end

@implementation CMQOldLoginViewController

#pragma mark - View Lifecycle

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
    
    self.screenName = @"Login";
    
    [self configureUI];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}

-(void)viewWillDisappear:(BOOL)animated{
    [self.view endEditing:YES]; //end editing in order to close the keyboard
}

#pragma mark - UI Stuff

/**
 *  configures the UI on initial loading. Sets texts, colors, button states, etc
 */
- (void)configureUI{
    //self.view
    
    //text fields
    
    [self.usernameTextField setFont:[UIFont fontWithName:kCMQFontGeometriaBold size:15]];
    self.usernameTextField.layer.borderWidth = .5f;
    self.usernameTextField.layer.borderColor = [UIColor blackColor].CGColor;
    
    [self.passwordTextField setFont:[UIFont fontWithName:kCMQFontGeometriaBold size:15]];
    self.passwordTextField.layer.borderWidth = .5f;
    self.passwordTextField.layer.borderColor = [UIColor blackColor].CGColor;
    
    [self.submitButton.titleLabel setFont:[UIFont fontWithName:kCMQFontGeometriaBold size:15]];
    self.submitButton.backgroundColor = [UIColor CMQGreenColor];
    [self.loginLabel setFont:[UIFont fontWithName:kCMQFontGeometriaLight size:14]];
    [self.helpButton.titleLabel setFont:[UIFont fontWithName:kCMQFontGeometriaLight size:14]];
    [self.forgotPasswordButton.titleLabel setFont:[UIFont fontWithName:kCMQFontGeometriaLight size:14]];
    [self.legalityLabel1 setFont:[UIFont fontWithName:kCMQFontGeometriaLight size:11]];
    [self.legalityLabel2 setFont:[UIFont fontWithName:kCMQFontGeometriaLight size:11]];
    [self.tosButton.titleLabel setFont:[UIFont fontWithName:kCMQFontGeometriaLight size:13]];
    [self.ppButton.titleLabel setFont:[UIFont fontWithName:kCMQFontGeometriaLight size:13]];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleTap:)];
    [self.view addGestureRecognizer:tapGesture];
}


#pragma mark - Internal

/**
 *  checks for valid fields in the username and password textfield and calls the CMQAPIClient to log in the user. If fields are invalid, it notifies the user and doesn't attempt to log them in.
 */
-(void)login{
    NSString *username = self.usernameTextField.text;
    NSString *password = self.passwordTextField.text;
    
    username = [NSString stringByStrippingWhitespace:username];
    
    //check that username and password field contain at least something
    if ([username isEqualToString:@""] || [password isEqualToString:@""])
        [SVProgressHUD showInfoWithStatus:@"All fields must be filled"];
    else
        [[CMQAPIClient sharedClient]loginWithUsername:username password:password alerts:YES notifications:YES channel:kParsePushChannelAllKey];
}

#pragma mark - UITapGestureRecognizer
/**
 *  handles a tap anywhere on the screen. used to dismiss the keyboard if it's showing
 *
 *  @param recognizer the gesture recognizer that accepted the tap
 */
-(void)handleTap:(UITapGestureRecognizer *)recognizer{
    [self.view endEditing:YES];
}

#pragma mark - Button Actions

/**
 *  called when the help button was pressed
 *
 *  @param sender the help button
 */
- (IBAction)onHelpPressed:(id)sender {
    [self presentMailViewController];
}

/**
 *  called when the submit button was pressed and attempts to log the user in
 *
 *  @param sender the submit button
 */
- (IBAction)onSubmit:(id)sender {
    [self login];
}

/**
 *  called when the forgot password button was pressed
 *
 *  @param sender the forgot password button
 */
- (IBAction)onForgotPassword:(id)sender {
    [self showForgotPasswordDialog];
}

/**
 *  called when the tos button was pressed
 *
 *  @param sender the tos button
 */
- (IBAction)onTermsOfService:(id)sender {
    [self showWebViewWithURL:[NSURL URLWithString:kTermsOfServiceURL]
             backgroundColor:[UIColor lightGrayColor]
                   tintColor:[UIColor whiteColor]
                       title:@"Terms of Service"];
}

/**
 *  called when the privacy policy button was pressed
 *
 *  @param sender the tos button
 */
- (IBAction)onPrivacyPolicy:(id)sender {
    [self showWebViewWithURL:[NSURL URLWithString:kPrivacyPolicyURL]
             backgroundColor:[UIColor lightGrayColor]
                   tintColor:[UIColor whiteColor]
                       title:@"Privacy Policy"];
}

/**
 * Called when the "help" button is selected,
 * present a view controller to send an email
 */
- (void)presentMailViewController {
    //NOTE: Presenting the mail VC in the iOS 8 simulator has a known bug where it won't prefill the recipients, messagebody, etc and will close itself after about 2 seconds. This does not happen for a device, so we're not worrying about it at the time of this writing (10/9/2014). A known workaround is to use a static global variable for the mail VC and "recycle" it. See SO answer here: http://stackoverflow.com/a/25864182/3469207
    if([MFMailComposeViewController canSendMail]){
        MFMailComposeViewController *vc = [[MFMailComposeViewController alloc]init];
        vc.mailComposeDelegate = self;
        [vc setMessageBody:[NSString stringWithFormat:@"\n\n\n\n\n\nSent from the %@ App %@\nA Communiqué Product", [[[NSBundle mainBundle] localizedInfoDictionary] objectForKey:@"CFBundleDisplayName"], [UIApplication versionAndBuild]] isHTML:NO];
        [vc setToRecipients:@[kLoginSupportEmail]];
        
        [self presentViewController:vc animated:YES completion:nil];
    }else{
        [SVProgressHUD showInfoWithStatus:@"To use this feature, please set up email on this device"];
    }
}

/**
 *  shows an instance of DZNWebViewController with the given URL
 *
 *  @param url the url to show in the web view
 */
-(void)showWebViewWithURL:(NSURL *)url backgroundColor:(UIColor *)bgColor tintColor:(UIColor *)tintColor title:(NSString *)title{
    //create our web view controller
    
    //check url for validity
    if ([[url absoluteString].lowercaseString hasPrefix:@"http"])
        self.webViewController = [[DZNWebViewController alloc]initWithURL:url];
    else
        self.webViewController = [[DZNWebViewController alloc]initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://%@", [url absoluteString]]]];
    
//    self.webViewController.toolbarTintColor = tintColor;
//    self.webViewController.toolbarBackgroundColor = bgColor;
//    self.webViewController.supportedActions = DZNWebViewControllerActionNone;
//    self.webViewController.titleColor = [UIColor clearColor];
    self.webViewController.supportedWebNavigationTools = DZNWebNavigationToolAll;
    self.webViewController.supportedWebActions = DZNWebActionAll;
    self.webViewController.showLoadingProgress = YES;
    self.webViewController.allowHistory = YES;
    self.webViewController.hideBarsWithGestures = YES;
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithTitle:@"<" style:UIBarButtonItemStylePlain target:self action:@selector(dismissWebViewController:)];
    barButton.tintColor = [UIColor blackColor];
    [barButton setTitleTextAttributes:@{NSFontAttributeName: [UIFont fontWithName:kCMQFontGeometriaLight size:19] } forState:UIControlStateNormal];
    self.webViewController.navigationItem.leftBarButtonItem = barButton;
    
    UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:self.webViewController];
//    navController.navigationBar.barTintColor = bgColor;
    
    [self presentViewController:navController animated:YES completion:nil];
}

/**
 *  called from our web view's navigation controller's bar button item
 *
 *  @param sender the bar button item to dismiss the webview
 */
- (void)dismissWebViewController:(id)sender{
    [_webViewController dismissViewControllerAnimated:YES completion:^{
        _webViewController = nil;
    }];
}


-(void) showForgotPasswordDialog {
    self.forgotPasswordView.layer.cornerRadius = 5;
    self.forgotPasswordView.center = self.view.center;
    [[[[UIApplication sharedApplication]delegate]window] addSubview:self.forgotPasswordView];
    [UIView animateWithDuration:0.2 animations:^{
        self.view.alpha = 0.5;
    }];
    
    [self.forgotPasswordView doPopInAnimationWithDuration:0.4];
}

-(void)dismissForgotPasswordDialog{
    [self.forgotPasswordEmailTextField setText:@""];
    [self.forgotPasswordView removeFromSuperview];
    [UIView animateWithDuration:0.2 animations:^{
        self.view.alpha = 1.0;
    }];
}

- (IBAction)onCancelForgotPassword:(id)sender {
    [self dismissForgotPasswordDialog];
}

- (IBAction)onSubmitForgotPassword:(id)sender {
    NSString *email = self.forgotPasswordEmailTextField.text;
    
    if ((![email isEqualToString:@""]) && [email rangeOfString:@"@"].length != 0 && [email rangeOfString:@"."].length != 0){
        [self dismissForgotPasswordDialog];
        
        [PFUser requestPasswordResetForEmailInBackground:email
                block:^(BOOL succeeded, NSError *error) {
                    NSString *statusString;
                    if (!error){
                        statusString = [NSString stringWithFormat:@"A password reset email was sent to %@", email];
                        [SVProgressHUD showSuccessWithStatus:statusString];
                    }else{
                        statusString = [NSString stringWithFormat:@"User %@ not found", email];
                        [SVProgressHUD showInfoWithStatus:statusString];
                        [[CMQAPIClient sharedClient] handleParseError:error];
                    }
                }];
    }
}


#pragma mark - UITextFieldDelegate

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    if(textField == self.usernameTextField) {
        [self.passwordTextField becomeFirstResponder];
    }else if (textField == self.forgotPasswordEmailTextField){
        [textField resignFirstResponder];
        return YES;
    }else
        [self login];
    
    return NO;
}

#pragma mark - MFMailComposeViewController Delegate

-(void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error{
    BOOL success = NO;
    switch (result) {
        case MFMailComposeResultCancelled:
            DLogOrange(@"mail cancelled");
            break;
        case MFMailComposeResultSaved:
            DLogOrange(@"mail saved to drafts");
            break;
        case MFMailComposeResultSent:
            success = YES;
            break;
        case MFMailComposeResultFailed:
            [SVProgressHUD showInfoWithStatus:@"Failed to send email"];
            break;
            
        default:
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:^{
        if(success)[SVProgressHUD showSuccessWithStatus:@"Email Sent!"];
    }];
}

@end
