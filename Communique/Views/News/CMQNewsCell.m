//
//  CMQNewsCell.m
//  Communique
//
//  Created by Chris Hetem on 9/29/14.
//  Copyright (c) 2014 WaveRider, Inc. All rights reserved.
//

#import "CMQNewsCell.h"

@implementation CMQNewsCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    // Initialization code
    [self configureUI];
}

/**
 *  configures the UI on initial loading
 */
-(void)configureUI{
    //TTTAttributedLabel setup
    [self.newsContentLabel setVerticalAlignment:TTTAttributedLabelVerticalAlignmentTop];
    [self.newsContentLabel setTextColor:[UIColor blackColor]];
    [self.newsContentLabel setLineBreakMode:NSLineBreakByWordWrapping];
    [self.newsContentLabel setNumberOfLines:0];
    
    //fonts
    [self.nameLabel setFont:[UIFont fontWithName:kCMQFontGeometriaLight size:17]];
    [self.newsContentLabel setFont:[UIFont fontWithName:kCMQFontGeometriaLight size:14]];
    [self.urlLabel setFont:[UIFont fontWithName:kCMQFontGeometriaLight size:14]];
    [self.newsTitleLabel setFont:[UIFont fontWithName:kCMQFontGeometriaBold size:14]];
    [self.newsDateLabel setFont:[UIFont fontWithName:kCMQFontGeometriaLight size:10]];
    [self.imageLoadingLabel setFont:[UIFont fontWithName:kCMQFontGeometriaLight size:12]];

    self.userPhoto.layer.cornerRadius = 20.0f;
    self.userPhoto.layer.borderWidth = .5f;
    self.userPhoto.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.userPhoto.layer.masksToBounds = YES;
}

@end
