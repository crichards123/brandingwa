//
//  CMQCreateNewsViewController.m
//  Communique
//
//  Created by Chris Hetem on 9/30/14.
//  Copyright (c) 2014 WaveRider, Inc. All rights reserved.
//

#import "CMQCreateNewsViewController.h"
#import "CMQHTMLParser.h"
#import "CMQCreateNewsViewModel.h"
#import "CMQNewsImageCell.h"
//@import SDWebImage;
#import <SDWebImage/UIImageView+WebCache.h>
#import "NSString+NSHash.h"

#define CollectionViewHeight            60
#define ContainerViewHeightMax          448
#define ContainerViewHeightMin          388

static NSString *cellIdentifier = @"CellIdentifier";
static NSString *articleContentPlaceholder = @"Description";

@interface CMQCreateNewsViewController ()<UITextFieldDelegate, CMQHTMLParserDelegate, UICollectionViewDataSource, UICollectionViewDelegate, UITextViewDelegate, CMQAPIClientDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (strong, nonatomic) CMQCreateNewsViewModel *viewModel;

//IBOutlets
@property (weak, nonatomic) IBOutlet UITextField *articleTitleTextField;
@property (weak, nonatomic) IBOutlet UITextField *urlTextField;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UIView *articleDetailsContainerView;
@property (weak, nonatomic) IBOutlet UITextView *articleContentTextView;
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *collectionViewHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *containerViewHeightConstraint;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UIButton *postButton;
@property (weak, nonatomic) IBOutlet UIButton *selectPhotoButton;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@end

@implementation CMQCreateNewsViewController

#pragma mark - View Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self configure];
    
}

#pragma mark - Config
/**
 *  configures the view on initial loading
 */
-(void)configure{
    //self.view
    [self.view setBackgroundColor:[UIColor CMQPurpleColor]];
    self.collectionViewHeightConstraint.constant = 0;
    self.containerViewHeightConstraint.constant = ContainerViewHeightMin;
    
    //fonts
    [self.articleTitleTextField setFont:[UIFont fontWithName:kCMQFontGeometriaLight size:17]];
    [self.articleContentTextView setFont:[UIFont fontWithName:kCMQFontGeometriaLight size:14]];
    [self.urlTextField setFont:[UIFont fontWithName:kCMQFontGeometriaLight size:14]];
    [self.backButton.titleLabel setFont:[UIFont fontWithName:kCMQFontGeometriaLight size:19]];
    [self.backButton setTitleColor:[UIColor lightTextColor] forState:UIControlStateNormal];
    
    //button states
    NSBundle* comBundle = [NSBundle bundleForClass:[CMQCreateNewsViewController class]];
    [self.cancelButton setImage:[UIImage imageNamed:@"btn-cancel" inBundle:comBundle compatibleWithTraitCollection:nil] forState:UIControlStateNormal];
    [self.cancelButton setImage:[UIImage imageNamed:@"btn-cancel-pressed" inBundle:comBundle compatibleWithTraitCollection:nil] forState:UIControlStateHighlighted];
    [self.postButton setImage:[UIImage imageNamed:@"btn-post-news" inBundle:comBundle compatibleWithTraitCollection:nil] forState:UIControlStateNormal];
    [self.postButton setImage:[UIImage imageNamed:@"btn-post-news-pressed" inBundle:comBundle compatibleWithTraitCollection:nil] forState:UIControlStateHighlighted];
    
    [self.selectPhotoButton setImage:[UIImage imageNamed:@"news_upload_photo" inBundle:comBundle compatibleWithTraitCollection:nil] forState:UIControlStateNormal];
    
    //this is just a clear view that sits to the let of the text view. it's used for 'indenting' since there's no easy way to do it otherwise (i.e. no contentInset or anything of that sort for textfields)
    UIView *spacerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 10)];
    [self.urlTextField setLeftViewMode:UITextFieldViewModeAlways];
    [self.urlTextField setLeftView:spacerView];
    
    //view mdoel
    self.viewModel = [[CMQCreateNewsViewModel alloc]init];
    
    //HTML Parser
    [[CMQHTMLParser sharedParser]setDelegate:self];
    
    //collectionview
    [self.collectionView registerNib:[UINib nibWithNibName:@"CMQNewsImageCell" bundle:comBundle] forCellWithReuseIdentifier:cellIdentifier];
    
}

#pragma mark - UIView stuff

/**
 *  expands the height of the collection view constraint to show its contents. updates our "select a photo" button image
 */
-(void)expandCollectionView{
    //[self.selectPhotoButton setImage:[UIImage imageNamed:@"news_select_photo"] forState:UIControlStateNormal];
    
    self.collectionViewHeightConstraint.constant = CollectionViewHeight;
    self.containerViewHeightConstraint.constant = ContainerViewHeightMax;
    
    [UIView animateWithDuration:0.25 animations:^{
        [self.articleDetailsContainerView layoutIfNeeded];
    }];
}

/**
 *  shrinks the height of the collection view constraint to hide its contents. updates our "select a photo" button image
 */
-(void)shrinkCollectionView{
    //[self.selectPhotoButton setImage:[UIImage imageNamed:@"news_upload_photo"] forState:UIControlStateNormal];
    
    self.collectionViewHeightConstraint.constant = 0;
    self.containerViewHeightConstraint.constant = ContainerViewHeightMin;
    
    [UIView animateWithDuration:0.25 animations:^{
        [self.articleDetailsContainerView layoutIfNeeded];
    }];
}

/**
 *  slides up the view to show to allow for the keyboard to show without obstructing anything
 */
-(void)slideUpViewForKeyboard{
    CGRect frame = self.view.frame;
    frame.origin.y = -100;
    
    [UIView animateWithDuration:0.25 animations:^{
        self.view.frame = frame;
    }];
}

/**
 *  slides down the view once the keyboard is dismissed
 */
-(void)slideDownViewForKeyboard{
    CGRect frame = self.view.frame;
    frame.origin.y = 0;
    
    [UIView animateWithDuration:0.25 animations:^{
        self.view.frame = frame;
    }];
}

/**
 *  clears and resets the entire view
 */
-(void)resetView{
    self.urlTextField.text = @"";
    self.articleTitleTextField.text = @"";
    self.viewModel.imageURLsArray = nil;
    self.viewModel.selectedIndexPath = nil;
    self.articleContentTextView.text = articleContentPlaceholder;
    [self shrinkCollectionView];
    [self.collectionView reloadData];
}

#pragma mark - CMQHTMLParserDelegate

-(void)CMQHTMLParserDidFindData:(NSDictionary *)urlData{
    DLogCyan(@"");
    if(urlData){
        DLogCyan(@"urlDate %@", urlData);
        NSString *title = urlData[kHTMLParserTitleKey];
        NSArray *imageURLs = urlData[kHTMLParserImageURLArray];
        
        self.articleTitleTextField.text = title;
        self.viewModel.imageURLsArray = imageURLs;
        [self.collectionView reloadData];
        
        //shrink or expand our collection view depending on how many images we got back
        if(imageURLs.count > 0){
            [self expandCollectionView];
            [self resetImageView];
        }else{
            [self shrinkCollectionView];
        }
    }
    [SVProgressHUD dismiss];

}

-(void)CMQHTMLParserFailedToFindData:(NSString *)error{
    DLogRed(@"error finding imageURLS: %@", error);
    [SVProgressHUD showInfoWithStatus:@"Failed to load URL"];
    [self shrinkCollectionView];
}

#pragma mark - Button Actions

/**
 *  presents the Photo Libary
 *
 *  @param sender <#sender description#>
 */
- (IBAction)onSelectPhoto:(id)sender {
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc]init];
    imagePicker.delegate = self;
    imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    imagePicker.modalPresentationStyle = UIModalPresentationPopover;
    imagePicker.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    
    UIPopoverPresentationController *popover = imagePicker.popoverPresentationController;
    popover.sourceView = self.view;
    popover.sourceRect = CGRectMake(CGRectGetMidX(self.view.bounds),
                                    self.articleDetailsContainerView.frame.origin.y + self.selectPhotoButton.frame.origin.y + self.selectPhotoButton.frame.size.height,
                                    0,
                                    0);
    
    [self presentViewController:imagePicker animated:YES completion:nil];
}

- (IBAction)onGoBack:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)onCancel:(id)sender {
    [self resetView];
    [self dismissViewControllerAnimated:YES completion:nil];
}

/**
 *  posts a new article via the CMQAPIClient if all required fields are filled
 *
 *  @param sender the post button
 */
- (IBAction)onPost:(id)sender {
    
    //get all news field values
    NSString *title = self.articleTitleTextField.text;
    NSString *content = self.articleContentTextView.text;
    NSString *articleURL = self.urlTextField.text;
    
    if(title == (id) [NSNull null] ||
       title.length == 0 ||
       [content isEqualToString:articleContentPlaceholder])
        [SVProgressHUD showInfoWithStatus:@"All fields must be filled"];
    else{
        
        UIImage *selectedImage;
        if(self.viewModel.selectedIndexPath){
             CMQNewsImageCell *cell = (CMQNewsImageCell *)[self.collectionView cellForItemAtIndexPath:self.viewModel.selectedIndexPath];
            selectedImage = cell.imageView.image;
        }else{
            selectedImage = self.imageView.image;
        }
       
        //create a Parse News object
        CMQNewsArticle *newsArticle = [[CMQNewsArticle alloc]init];
        newsArticle.newsTitle = title;
        newsArticle.newsContent = content;
        
        if ([CMQUser currentUser].tempAptComplexID && [CMQUser currentUser].tempAptComplexID.length > 0)
            newsArticle.aptComplexID = [CMQUser currentUser].tempAptComplexID;
        else
            newsArticle.aptComplexID = [CMQUser currentUser].aptComplexID;
        
        //check url for validity
        //only append http:// if user entered a url
        if (![articleURL hasPrefix:@"http://"] &&
            ![articleURL hasPrefix:@"https://"] &&
            !([articleURL length] <= 0))
            articleURL = [@"http://" stringByAppendingString:articleURL];
        newsArticle.articleURL = articleURL;
        
        newsArticle.userWhoPosted = [CMQUser currentUser];
        
        if(selectedImage){   //only add photo if one exists
            //get selected photo
            NSString *imageName = [NSString stringWithFormat:@"%f", [[NSDate date] timeIntervalSince1970]];
            imageName = [[imageName MD5] stringByAppendingPathExtension:@"png"];
    
//            NSData *imageData = UIImagePNGRepresentation(selectedImage);
            NSData *imageData = UIImageJPEGRepresentation(selectedImage, 1.00f);
            @try{
               PFFile *imageFile = [PFFile fileWithName:imageName data:imageData];
                newsArticle.newsPhoto = imageFile;
                
                [SVProgressHUD showWithStatus:@"Posting news article"];
                
                //CMQAPIClient
                [[CMQAPIClient sharedClient]postNews:newsArticle];
            }
            @catch(NSException *e){
                if([e.name isEqualToString:NSInvalidArgumentException]){
                    [SVProgressHUD showInfoWithStatus:@"Image too large, try another"];
                    [self resetImageView];
                }
            }
        }
        else {
            [SVProgressHUD showWithStatus:@"Posting news article"];
            
            //CMQAPIClient
            [[CMQAPIClient sharedClient]postNews:newsArticle];
        }
    }
}

#pragma mark - UICollectionView Datasource

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    CMQNewsImageCell *cell = (CMQNewsImageCell *)[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    [self configureCell:cell forIndex:indexPath.item];
    
    return cell;
}

/**
 *  configures the cell
 *
 *  @param cell the cell to configure
 *  @param index the index of the cell
 */
-(void)configureCell:(CMQNewsImageCell *)cell forIndex:(NSInteger)index{
    [cell.imageView sd_setImageWithURL:[self.viewModel.imageURLsArray objectAtIndex:index] placeholderImage:nil];
    cell.selectionIndicator.hidden = cell.selected ? NO : YES;
    
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.viewModel.imageURLsArray.count;
}

#pragma mark - UICollectionView Delegate

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    CMQNewsImageCell *cell = (CMQNewsImageCell *)[collectionView cellForItemAtIndexPath:indexPath];
    cell.selectionIndicator.hidden = NO;
    self.viewModel.selectedIndexPath = indexPath;
}

-(void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath{
    CMQNewsImageCell *cell = (CMQNewsImageCell *)[collectionView cellForItemAtIndexPath:indexPath];
    cell.selectionIndicator.hidden = YES;
}

#pragma mark - UITextFieldDelegate

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    if(textField == self.urlTextField && ![textField.text isEqualToString:@""]){
        [SVProgressHUD showWithStatus:@"Loading web page"];
        NSString *urlString = textField.text;
        if(![urlString hasPrefix:@"http"]){  //url must start with 'http://' or 'https://', so check for prefix http
            urlString = [@"http://" stringByAppendingString:urlString];
        }
        urlString = [NSString stringByStrippingWhitespace:urlString];   //strip any whitespace before parsing URL
        textField.text = urlString; //set the textview text too so that we have the full string when we pull from it later to actually create the news article
        [[CMQHTMLParser sharedParser] parseURL:[NSURL URLWithString:urlString]];
    }
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - UITextViewDelegate

-(void)textViewDidBeginEditing:(UITextView *)textView{
    [self slideUpViewForKeyboard];
    if([textView.text isEqualToString:articleContentPlaceholder]){
        textView.text = @"";    //remove placeholder
    }
}

- (void)textViewDidEndEditing:(UITextView *)textView{
    [self slideDownViewForKeyboard];
    if([textView.text isEqualToString:@""]){
        textView.text = articleContentPlaceholder;  //replace placeholder
    }
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    if ([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    return YES;
}

#pragma mark - UIImagePickerController Delegate

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    [self handlePhotoSelection:[info objectForKey:UIImagePickerControllerOriginalImage]];
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Image Picker Helpers

/**
 *  handles a photo selection from the UIImagePickerController delegate. sets our imageview and updates the button state. also deselects any selected collectionview items
 *
 *  @param image the image to handle
 */
-(void)handlePhotoSelection:(UIImage *)image{
    self.imageView.image = image;
    [self.selectPhotoButton setImage:nil forState:UIControlStateNormal];
    [self collectionView:self.collectionView didDeselectItemAtIndexPath:self.viewModel.selectedIndexPath];
    self.viewModel.selectedIndexPath = nil;
}

-(void)resetImageView{
    NSBundle* comBundle = [NSBundle bundleForClass:[CMQCreateNewsViewController class]];
    self.imageView.image = nil;
    [self.selectPhotoButton setImage:[UIImage imageNamed:@"news_select_photo" inBundle:comBundle compatibleWithTraitCollection:nil] forState:UIControlStateNormal];
}


@end
