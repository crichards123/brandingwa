//
//  CMQNewsImageCell.h
//  Communique
//
//  Created by Chris Hetem on 9/30/14.
//  Copyright (c) 2014 WaveRider, Inc. All rights reserved.
//

//@import UIKit;
#import <UIKit/UIKit.h>

@interface CMQNewsImageCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIImageView *selectionIndicator;

@end
 