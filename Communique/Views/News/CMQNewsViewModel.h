//
//  CMQNewsViewModel.h
//  Communique
//
//  Created by Chris Hetem on 9/29/14.
//  Copyright (c) 2014 WaveRider, Inc. All rights reserved.
//

//@import Foundation;
#import <Foundation/Foundation.h>

@interface CMQNewsViewModel : NSObject

/**
 *  an array of all the news articles
 */
@property (strong, nonatomic) NSMutableArray *newsArray;

/**
 *  formats a date speficially for the News view
 *
 *  @param date the date to format
 *
 *  @return the formatted date as a string
 */
-(NSString *)formatDate:(NSDate *)date;
@end
