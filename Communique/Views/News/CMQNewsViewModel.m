//
//  CMQNewsViewModel.m
//  Communique
//
//  Created by Chris Hetem on 9/29/14.
//  Copyright (c) 2014 WaveRider, Inc. All rights reserved.
//

#import "CMQNewsViewModel.h"

@implementation CMQNewsViewModel

-(NSString *)formatDate:(NSDate *)date{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    
    //calendar to extract day and month compenents
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:(NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear) fromDate:date];
    
    NSInteger month = [components month]-1;
    NSInteger day = [components day];
    
    NSString *monthName = [[dateFormatter shortMonthSymbols]objectAtIndex:month];
    NSString *dateNumber = [NSString stringWithFormat:@"%ld", (long)day];
    
    NSString *formatStringForHours = [NSDateFormatter dateFormatFromTemplate:@"j" options:0 locale:[NSLocale currentLocale]];
    
    NSRange containsA = [formatStringForHours rangeOfString:@"a"];
    BOOL hasAMPM = containsA.location != NSNotFound;
    
    if (hasAMPM) { dateFormatter.dateFormat = @"h:mm a";}
    else { dateFormatter.dateFormat = @"HH:mm";}
    NSString *time = [dateFormatter stringFromDate:date];
    
    return [NSString stringWithFormat:@"%@. %@, %@", monthName, dateNumber, time];
}

@end
