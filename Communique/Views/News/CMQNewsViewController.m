//
//  CMQNewsViewController.m
//  Communique
//
//  Created by Chris Hetem on 9/26/14.
//  Copyright (c) 2014 WaveRider, Inc. All rights reserved.
//

#import "CMQNewsViewController.h"
#import "CMQNewsCell.h"
#import "CMQNewsViewModel.h"
#import "DZNWebViewController.h"
#import "CMQCreateNewsViewController.h"
#import "CMQTransitionAnimationController.h"


#define DEFAULT_CELL_HEIGHT                 73
#define DEFAULT_CONTENT_SPACING_TITLE       31
#define ALTERNATE_CONTENT_SPACING_TITLE     5

static NSString *cellIdentifer = @"CellIdentifier";

@interface CMQNewsViewController () <UICollectionViewDataSource, UICollectionViewDelegate, CMQAPIClientDelegate, UIViewControllerTransitioningDelegate>
{
    DZNWebViewController *_webViewController;
}
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (strong, nonatomic) CMQNewsViewModel  *viewModel;
@property (strong, nonatomic) UIRefreshControl *refreshControl;
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UIButton *createNewsButton;
@property (strong, nonatomic) CMQCreateNewsViewController *createNewsVC;
@property (weak, nonatomic) IBOutlet UIView *alertContainerView;
@property (weak, nonatomic) IBOutlet UILabel *alertMessage;

@end

@implementation CMQNewsViewController

#pragma mark - View Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];

    // Do any additional setup after loading the view from its nib.
    
    self.screenName = @"News";
    
    [self configure];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    //make sure we're the delegate if we're returning from creating a new article
    if([CMQAPIClient sharedClient].delegate != self) [[CMQAPIClient sharedClient]setDelegate:self];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [SVProgressHUD dismiss];
}

/**
 *  called from our web view's navigation controller's bar button item
 *
 *  @param sender the bar button item to dismiss the webview
 */
- (void)dismissWebViewController:(id)sender{
    [_webViewController dismissViewControllerAnimated:YES completion:^{_webViewController = nil;}];
}


#pragma mark - Config

/**
 *  configures the view on first load
 */
-(void)configure{
    //self.view
    [self.view setBackgroundColor:[UIColor CMQPurpleColor]];
    self.viewModel = [[CMQNewsViewModel alloc]init];
    
    //back button
    [self.backButton.titleLabel setFont:[UIFont fontWithName:kCMQFontGeometriaLight size:19]];
    [self.backButton setTitleColor:[UIColor lightTextColor] forState:UIControlStateNormal];
    
    //alert
    [self.alertContainerView setHidden:YES];
    [self.alertMessage setFont:[UIFont fontWithName:kCMQFontGeometriaLight size:15]];
    
    //refresh control
    self.refreshControl = [[UIRefreshControl alloc]init];
    [self.collectionView addSubview:self.refreshControl];
    [self.refreshControl addTarget:self action:@selector(onRefresh) forControlEvents:UIControlEventValueChanged];
    
    //check if user is Admin or not. if not, hide our createEventButton
    if([[CMQUser currentUser].userRole isEqualToString:kPermissionAdmin] ||
       [[CMQUser currentUser].userRole isEqualToString:kPermissionRegionalAdmin] ||
       [[CMQUser currentUser].userRole isEqualToString:kPermissionCompanyAdmin])
        self.createNewsButton.hidden = NO;
    else
        self.createNewsButton.hidden = YES;
    
    //HUD
    [SVProgressHUD showWithStatus:@"Fetching news articles"];
    
    //APIClient
    [[CMQAPIClient sharedClient]setDelegate:self];
    [[CMQAPIClient sharedClient]fetchNews];

    //collectionView
    NSBundle* comBundle = [NSBundle bundleForClass:[CMQNewsCell class]];
    [self.collectionView registerNib:[UINib nibWithNibName:@"CMQNewsCell" bundle:comBundle] forCellWithReuseIdentifier:cellIdentifer];
}

#pragma mark - Button Actions

- (IBAction)onGoBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

/**
 *  presents on instance of CMQCreateNewsViewController
 *
 *  @param sender the create button
 */
- (IBAction)onCreateNewsArticle:(id)sender {
    NSBundle* comBundle = [NSBundle bundleForClass:[CMQCreateNewsViewController class]];
    self.createNewsVC = [[CMQCreateNewsViewController alloc]initWithNibName:@"CMQCreateNewsViewController" bundle:comBundle];
    self.createNewsVC.transitioningDelegate = self;
    [self presentViewController:self.createNewsVC animated:YES completion:nil];
}

#pragma mark - CMQAPIClient Delegate

- (void)CMQAPIClientDidFetchNewsSuccessfully:(NSArray *)news{
    self.viewModel.newsArray = [NSMutableArray arrayWithArray:news];
    [self.refreshControl endRefreshing];
    [self.collectionView reloadData];
    [SVProgressHUD dismiss];
    
    CMQUser *user = [CMQUser currentUser];
    user.newsUpToDate = true;
    [user saveInBackground];
    
    [self.alertContainerView setHidden:[self.viewModel.newsArray count] > 0];
}

- (void)CMQAPIClientDidFailFetchWithMessage:(NSString *)message isCache:(BOOL)isCache{
    DLogRed(@"failed to fetch News:%@", message);
    [SVProgressHUD showInfoWithStatus:@"Failed to fetch news aricles"];
}

-(void)CMQAPIClientDidFailToPostObject:(PFObject *)object withMessage:(NSString *)message{
    DLogRed(@"Failed to post News Article: %@", message);
    [SVProgressHUD showInfoWithStatus:@"Failed to post article, sorry"];
}

-(void)CMQAPIClientDidPostObjectSuccessfully:(PFObject *)object{
    [SVProgressHUD showSuccessWithStatus:@"News posted!"];
    [self.createNewsVC dismissViewControllerAnimated:YES completion:^{
        [self insertNewsArticle:(CMQNewsArticle *)object atIndex:0];
        
        CMQUser *user = [CMQUser currentUser];
        user.newsUpToDate = true;
        [user saveInBackground];
        
        self.createNewsVC = nil;
        
        [self.alertContainerView setHidden:YES];
    }];
}

#pragma Refresh Control

/**
 *  refreshes the news articles
 */
-(void)onRefresh{
    [SVProgressHUD showWithStatus:@"Refreshing news"];
    [[CMQAPIClient sharedClient]fetchNews];
}

#pragma mark - UICollectionView Datasource

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    CMQNewsCell *cell = (CMQNewsCell *)[self.collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifer forIndexPath:indexPath];
    
    [self configureCell:cell forRow:indexPath.row]; //call to configure our row
    
    return cell;
}

/**
 *  configures a given cell at a given row with formatting and such
 *
 *  @param cell the cell to configure
 *  @param row  the row for the cell to configure
 */
-(void)configureCell:(CMQNewsCell *)cell forRow:(NSInteger)row{
    CMQNewsArticle *newsArticle = [self.viewModel.newsArray objectAtIndex:row];
    
    CMQUser *userWhoPosted = (CMQUser *) newsArticle.userWhoPosted.fetchIfNeeded;
    
    if([[CMQUser currentUser].userRole isEqualToString:kPermissionAdmin] ||
       [[CMQUser currentUser].userRole isEqualToString:kPermissionRegionalAdmin] ||
       [[CMQUser currentUser].userRole isEqualToString:kPermissionCompanyAdmin])
        cell.nameLabel.text = (userWhoPosted.fullName) ? userWhoPosted.fullName : @"Staff Member";
    else
        cell.nameLabel.text = (userWhoPosted.firstName) ? userWhoPosted.firstName : @"Staff Member";
    
    cell.newsTitleLabel.text = newsArticle.newsTitle;
    cell.newsContentLabel.text = newsArticle.newsContent;
    cell.newsDateLabel.text = [self.viewModel formatDate:newsArticle.createdAt];
    
    //userWhoPosted.userPhoto
    if (userWhoPosted.userPhoto){
        //fetch our image data on a background thread
        dispatch_queue_t queue = dispatch_get_global_queue (DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
        dispatch_async(queue, ^{
            UIImage *userPhoto = [UIImage getImageFromPFFile:userWhoPosted.userPhoto];
            //display our image on the main thread
            dispatch_async(dispatch_get_main_queue(), ^{
                cell.userPhoto.contentMode = UIViewContentModeScaleAspectFill;
                cell.userPhoto.image = userPhoto;
            });
        });
    }else{
        NSBundle* bundle = [NSBundle bundleForClass:[CMQNewsViewController class]];
        cell.userPhoto.contentMode = UIViewContentModeTop;
        cell.userPhoto.image = [UIImage imageNamed:@"user-icon" inBundle:bundle compatibleWithTraitCollection:nil];
    }
    
    //load image if there is one
    cell.newsPhotoImageView.image = nil;
    cell.newsPhotoImageView.alpha = 0.0;
    if(newsArticle.newsPhoto){
        cell.imageLoadingLabel.hidden = NO;

        //fetch our image data on a background thread
        dispatch_queue_t queue = dispatch_get_global_queue (DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
        dispatch_async(queue, ^{
            UIImage *newsImage = [UIImage getImageFromPFFile:newsArticle.newsPhoto];
            //display our image on the main thread
            dispatch_async(dispatch_get_main_queue(), ^{
                cell.newsPhotoImageView.hidden = NO;
                cell.imageLoadingLabel.hidden = YES;
                cell.imageViewHeightConstraint.constant = [UIImage getHeightForImageContainerWithWidth:cell.newsPhotoImageView.frame.size.width imageWidth:newsImage.size.width imageHeight:newsImage.size.height];
                
                cell.newsPhotoImageView.image = newsImage;
                [UIView animateWithDuration:0.75 animations:^{
                    cell.newsPhotoImageView.alpha = 1.0;
                }];
            });
        });
    }else{
        cell.newsPhotoImageView.hidden = YES;
        cell.imageViewHeightConstraint.constant = 0;
        cell.imageLoadingLabel.hidden = YES;
    }
    
    if(newsArticle.articleURL && newsArticle.articleURL.length > 0){
        cell.urlLabel.hidden = NO;
        cell.urlLabel.text = newsArticle.articleURL;
        cell.newsContentVerticalSpacingToTitle.constant = DEFAULT_CONTENT_SPACING_TITLE;
    }else{
        cell.urlLabel.hidden = YES;
        cell.newsContentVerticalSpacingToTitle.constant = ALTERNATE_CONTENT_SPACING_TITLE;
    }
    
    cell.backgroundColor = [UIColor whiteColor];
}

/**
 *  inserts a news article into our collection view at a given index. updates our collection view data source too
 *
 *  @param newsArticle the news article to insert
 *  @param index      the index at which to enter the news article
 */
-(void)insertNewsArticle:(CMQNewsArticle *)newsArticle atIndex:(NSInteger)index{
    //update our collectionview datasource
    [self.viewModel.newsArray insertObject:newsArticle atIndex:index];
    
    //insert our new cell after a half second. This is because we are still deleting the "CMQCreateMessageCell" when we are also inserting the new one. It's just a visual thing really.
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        //insert a new cell at the top of the collection view
        NSIndexPath *indexPath = [NSIndexPath indexPathForItem:index inSection:0];
        if(self.viewModel.newsArray.count == 1){
            /*[self.collectionView insertItemsAtIndexPaths:@[indexPath]];
            [self.collectionView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionTop animated:NO];*/
        }else{
           /* [self.collectionView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionTop animated:NO];
            [self.collectionView insertItemsAtIndexPaths:@[indexPath]];*/
        }
    });
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.viewModel.newsArray.count;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = DEFAULT_CELL_HEIGHT;
    
    CMQNewsArticle *newsArticle = [self.viewModel.newsArray objectAtIndex:indexPath.row];
    
    cellHeight += [newsArticle.newsTitle boundingRectWithSize:CGSizeMake(self.collectionView.frame.size.width - 32, CGFLOAT_MAX) options:(NSStringDrawingUsesFontLeading) | (NSStringDrawingUsesLineFragmentOrigin) attributes:@{NSFontAttributeName : [UIFont fontWithName:kCMQFontGeometriaBold size:14.5]} context:nil].size.height;
    
    cellHeight += [newsArticle.newsContent boundingRectWithSize:CGSizeMake(self.collectionView.frame.size.width - 32, CGFLOAT_MAX) options:(NSStringDrawingUsesFontLeading) | (NSStringDrawingUsesLineFragmentOrigin) attributes:@{NSFontAttributeName : [UIFont fontWithName:kCMQFontGeometriaLight size:14.5]} context:nil].size.height;
    
    if(newsArticle.newsPhoto){
        UIImage *newsImage = [UIImage getImageFromPFFile:newsArticle.newsPhoto];
        cellHeight += [UIImage getHeightForImageContainerWithWidth:self.collectionView.frame.size.width - 20 imageWidth:newsImage.size.width imageHeight:newsImage.size.height];
    }
    
    if(newsArticle.articleURL && newsArticle.articleURL.length > 0) cellHeight += 21;
    
    return CGSizeMake(self.collectionView.frame.size.width - 20, cellHeight);
}

#pragma mark - UICollectionView Delegate

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    //get our url and show it
    CMQNewsArticle *article = [self.viewModel.newsArray objectAtIndex:indexPath.row];
    NSString *urlString = article.articleURL;
    
    if([urlString isEqualToString:@""]) return;
    
    if(![urlString hasPrefix:@"http"]) urlString = [urlString stringByAppendingString:@"http://"];
        
    NSURL *url  = [NSURL URLWithString:urlString];
    if(url)[self showArticleWithURL:url];
}

/**
 *  shows a news article's URL in a webview controller
 *
 *  @param url the url to show
 */
-(void)showArticleWithURL:(NSURL *)url{
    //create our web view controller
    _webViewController = [[DZNWebViewController alloc]initWithURL:url];
//    _webViewController.toolbarTintColor = [UIColor whiteColor];
//    _webViewController.toolbarBackgroundColor = [UIColor CMQPurpleColor];
//    _webViewController.supportedActions = DZNWebViewControllerActionNone;
//    _webViewController.titleColor = [UIColor whiteColor];
    
    _webViewController.supportedWebNavigationTools = DZNWebNavigationToolAll;
    _webViewController.supportedWebActions = DZNWebActionAll;
    _webViewController.showLoadingProgress = YES;
    _webViewController.allowHistory = YES;
    _webViewController.hideBarsWithGestures = YES;
    
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithTitle:@"<" style:UIBarButtonItemStylePlain target:self action:@selector(dismissWebViewController:)];
    barButton.tintColor = [UIColor blackColor];
    _webViewController.navigationItem.leftBarButtonItem = barButton;

    UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:_webViewController];
//    navController.navigationBar.barTintColor = [UIColor CMQPurpleColor];

    [self presentViewController:navController animated:YES completion:nil];
}

#pragma mark - UIViewControllerTransitioningDelegate

-(id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented
                                                                 presentingController:(UIViewController *)presenting
                                                                     sourceController:(UIViewController *)source{
    return [[CMQTransitionAnimationController alloc]init];
}

-(id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed{
    return [[CMQTransitionAnimationController alloc]init];
}

@end
