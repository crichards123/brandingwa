//
//  CMQCreateNewsViewModel.h
//  Communique
//
//  Created by Chris Hetem on 9/30/14.
//  Copyright (c) 2014 WaveRider, Inc. All rights reserved.
//

//@import Foundation;
#import <Foundation/Foundation.h>

@interface CMQCreateNewsViewModel : NSObject

/**
 *  a list of imageURLs retrieved from our HTMLParser
 */
@property (copy, nonatomic) NSArray *imageURLsArray;

/**
 *  the selected indexPath if a user selected a photo retrieved from our HTML Parser
 */
@property (strong, nonatomic) NSIndexPath *selectedIndexPath;

@end
