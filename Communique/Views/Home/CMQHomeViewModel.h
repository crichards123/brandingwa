//
//  CMQHomeViewModel.h
//  Communique
//
//  Created by Chris Hetem on 9/15/14.
//  Copyright (c) 2014 WaveRider, Inc. All rights reserved.
//

//@import Foundation;
#import <Foundation/Foundation.h>

@interface CMQHomeViewModel : NSObject

/**
 *  the background colors to use for the home collectionview cells
 */
@property (copy, nonatomic) NSArray *bgColors;

/**
 *  the titles to user for the home collectionview cells
 */
@property (copy, nonatomic) NSArray *cellTitles;

@property (assign, nonatomic) BOOL firstTabEnabled;
@property (assign, nonatomic) BOOL secondTabEnabled;

//@property (assign, nonatomic) BOOL isStaff;


@end
