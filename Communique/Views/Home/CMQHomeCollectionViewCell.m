//
//  CMQHomeCollectionViewCell.m
//  Communique
//
//  Created by Chris Hetem on 9/15/14.
//  Copyright (c) 2014 WaveRider, Inc. All rights reserved.
//

#import "CMQHomeCollectionViewCell.h"

@implementation CMQHomeCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.contentView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    self.contentView.translatesAutoresizingMaskIntoConstraints = YES;
}

- (void)layoutIfNeeded {
    [super layoutIfNeeded];
}

-(void)roundTopCornersForView:(UIView *)view{
    // create bezier mask path
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:view.frame byRoundingCorners:UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii:CGSizeMake(10, 10)];
    
    //create shape layer and set its path
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    maskLayer.frame = view.frame;
    maskLayer.path = maskPath.CGPath;
    
    view.layer.mask = maskLayer;
}

@end
