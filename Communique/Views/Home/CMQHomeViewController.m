//
//  CMQHomeViewController.m
//  Communique
//
//  Created by Chris Hetem on 9/15/14.
//  Copyright (c) 2014 WaveRider, Inc. All rights reserved.
//

#import "CMQHomeViewController.h"
#import "CMQHomeCollectionViewCell.h"
#import "CMQHomeViewModel.h"
#import "CMQMessageViewController.h"
#import "CMQEventsViewController.h"
#import "CMQNewsViewController.h"
#import "CMQOldLoginViewController.h"
#import "CMQManagement.h"
#import "UIApplication+AppInfo.h"
#import <MessageUI/MessageUI.h>
#import "DZNWebViewController.h"
#import "TGLStackedLayout.h"
#import "TGLExposedLayout.h"
#import "CMQTransitionAnimationController.h"
#import "CMQProfileCollectionViewController.h"
#import "CMQCommunityViewController.h"
#import "CMQCommunityPostsViewController.h"
#import "UIView+Animations.h"
@import CoreLocation;
#import <CoreLocation/CoreLocation.h>
//#import "CommuniqueiOS/CommuniqueiOS-Swift.h"
//@import UberRides;

#define CellTopRevealiPhoneSmall              50
#define CellTopRevealiPhoneStandard           65
//#define CellTopRevealiPadSmall               100
#define CellTopRevealiPadStandard            100
#define SwitchLeadingSpace                     8
//#define UberLeadingSpace        52
#define PayLeaseLeadingSpace    60
#define SmartHomeLeadingSpace   100
#define PackagesLeadingSpace    130

typedef void (^compBlock)();

static NSString *contentCellIdentifier = @"CellIdentifier";

@interface CMQHomeViewController () <UICollectionViewDataSource, UICollectionViewDelegate, CMQAPIClientDelegate, MFMailComposeViewControllerDelegate, UINavigationControllerDelegate, CMQCommunityPostsViewControllerDelegate, CMQCommunityViewControllerDelegate, UITextViewDelegate, /*UBSDKRideRequestViewControllerDelegate,*/ CLLocationManagerDelegate>

//IBOutlets
@property (weak, nonatomic) IBOutlet UIImageView *aptImageView;
//@property (weak, nonatomic) IBOutlet UIImageView *aptManagementLogo;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (strong, nonatomic) IBOutlet TGLStackedLayout *stackedLayout;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *aptImgHeightConstraint;
@property (weak, nonatomic) IBOutlet UIButton *profileButton;
@property (weak, nonatomic) IBOutlet UIButton *switchCommunityButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *switchCommunityButtonLeadingSpaceConstraint;
@property (weak, nonatomic) IBOutlet UIButton *uberButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *uberButtonLeadingSpaceConstraint;
@property (weak, nonatomic) IBOutlet UIButton *payLeaseButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *payLeaseButtonLeadingSpaceConstraint;
@property (weak, nonatomic) IBOutlet UIButton *smartHomeButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *smartHomeButtonLeadingSpaceConstraint;
@property (weak, nonatomic) IBOutlet UIButton *packagesButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *packagesButtonLeadingSpaceConstraint;

@property (strong, nonatomic) CLLocationManager *locationManager;
//@property (strong, nonatomic) UBSDKRideRequestViewController *rideRequestViewController;
@property (strong, nonatomic) IBOutlet UIView *wifiSupportView;
@property (weak, nonatomic) IBOutlet UITextView *wifiSupportTextView;
@property (weak, nonatomic) IBOutlet UIButton *wifiSupportCancelButton;
@property (weak, nonatomic) IBOutlet UIButton *wifiSupportSubmitButton;
@property (weak, nonatomic) IBOutlet UIButton *wifiSupportCallButton;

@property (strong, nonatomic) CMQHomeViewModel *viewModel;
@property (strong, nonatomic) DZNWebViewController *webViewController;
@property (strong, nonatomic) CMQOldLoginViewController *loginVC;
@property (strong, nonatomic) CMQCommunityViewController *communityVC;
@property (strong, nonatomic) CMQCommunityPostsViewController *communityPostsVC;
@property (strong, nonatomic) CMQProfileCollectionViewController *profileCollectionVC;
    
    @property (strong, nonatomic) NSBundle* comBundle;

-(void)checkContentStatuses:(id)sender;

@end

@implementation CMQHomeViewController{
    int celltopReveal;
    NSMutableArray *contacts;
//    NSString *uberMode;
    BOOL uberButtonTapped, payLeaseButtonTapped, smartHomeButtonTapped;
}

#pragma mark - View Lifecycle

- (void)viewDidLoad{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.comBundle = [NSBundle bundleForClass:[CMQOldLoginViewController class]];
    self.screenName = @"Home";
    
    //create view model
    self.viewModel = [[CMQHomeViewModel alloc]init];
    
    self.collectionView.hidden = YES;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) // iPad
        celltopReveal = CellTopRevealiPadStandard;
    else // iPhone
        celltopReveal = ([[UIScreen mainScreen]bounds].size.height > 480) ? CellTopRevealiPhoneStandard : CellTopRevealiPhoneSmall;
    
    //configure UI
    [self configureUI];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(checkContentStatuses:)
                                                 name:UIApplicationDidBecomeActiveNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(checkContentStatuses:)
                                                 name:UIApplicationWillEnterForegroundNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(checkForUpdate:)
                                                 name:UIApplicationDidBecomeActiveNotification
                                               object:nil];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    //set ourselves as API delegate
    [[CMQAPIClient sharedClient]setDelegate:self];
    
    [self checkForUpdate:self];
    
    //check out layout. if a VC was just popped, there's a 50/50 chance the layout is an instance of TGLExposedLayout
    //and we need to reset it back to a TGLStackedLayout (self.stackedLayout)
    (self.collectionView.collectionViewLayout != self.stackedLayout) ? [self resetStackLayout:YES] : [self resetStackLayout:NO];
}


-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    //show login if no user is set
    if (![CMQUser currentUser])
        [self showLogin];
    else{
        if ([CMQUser currentUser].userPhoto){
            //fetch our image data on a background thread
            dispatch_queue_t queue = dispatch_get_global_queue (DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
            dispatch_async(queue, ^{
                UIImage *userPhoto = [UIImage getImageFromPFFile:[CMQUser currentUser].userPhoto];
                //display our image on the main thread
                dispatch_async(dispatch_get_main_queue(), ^{
                    self.profileButton.imageView.contentMode = UIViewContentModeScaleAspectFill;
                    [self.profileButton setImage:userPhoto forState:UIControlStateNormal];
                    [self.profileButton setImage:userPhoto forState:UIControlStateHighlighted];
                });
            });
        }else{
            self.profileButton.imageView.contentMode = UIViewContentModeTop;
            [self.profileButton setImage:[UIImage imageNamed:@"user-icon" inBundle:_comBundle compatibleWithTraitCollection:nil] forState:UIControlStateNormal];
            [self.profileButton setImage:[UIImage imageNamed:@"user-icon" inBundle:_comBundle compatibleWithTraitCollection:nil] forState:UIControlStateHighlighted];
        }
        
        [self checkContentStatuses:self];
    }
}

/**
 *  presents a given View Controller depending on which row was selected in the collectionview
 *
 *  @param row the row that was selected in the collectionview
 */
- (void)presentViewControllerForRow:(NSInteger)row{
    switch (row) {
        case 0:{ //Messages
            //CMQMessageViewController *vc = [[UIStoryboard storyboardWithName:@"Messages" bundle:[NSBundle mainBundle]]instantiateInitialViewController];
            CMQMessageViewController *vc =[[CMQMessageViewController alloc] initWithNibName:@"CMQMessageViewController" bundle:self.comBundle];
            self.navigationController.delegate = self;
            //[self presentViewController:vc animated:YES completion:nil];
            [self.navigationController pushViewController:vc animated:YES];
            break;
        }
        case 1:{ //Events
            CMQEventsViewController *vc = [[CMQEventsViewController alloc]initWithNibName:@"CMQEventsViewController" bundle:self.comBundle];
            self.navigationController.delegate = self;
            [self.navigationController pushViewController:vc animated:YES];
            break;
        }
        case 2:{ //News
            CMQNewsViewController *vc = [[CMQNewsViewController alloc]initWithNibName:@"CMQNewsViewController" bundle:self.comBundle];
            self.navigationController.delegate = self;
            [self.navigationController pushViewController:vc animated:YES];
            break;
        }
        case 3:{ //Menu tab 1
            if(self.viewModel.firstTabEnabled){
                id tracker = [[GAI sharedInstance] defaultTracker];
                [tracker set:kGAIScreenName value:@"Custom Tab 1"];
                [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
                
                [self showWebViewWithURL:[NSURL URLWithString:CMQAD.apartment.aptRepairURL]
                         backgroundColor:[UIColor CMQYellowColor]
                               tintColor:[UIColor blackColor]
                                   title:CMQAD.apartment.firstTabTitle];
            }else
                [self showActionSheetForMode:kActionSheetContact];
            
            break;
        }
        case 4:{ //Menu tab 2
            if(self.viewModel.secondTabEnabled){
                id tracker = [[GAI sharedInstance] defaultTracker];
                [tracker set:kGAIScreenName value:@"Custom Tab 2"];
                [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
                
                [self showWebViewWithURL:[NSURL URLWithString:CMQAD.apartment.aptPaymentURL]
                         backgroundColor:[UIColor CMQGreenColor]
                               tintColor:[UIColor whiteColor]
                                   title:CMQAD.apartment.secondTabTitle];
            }else
                [self showActionSheetForMode:kActionSheetContact];
            
            break;
        }
        case 5:{ //Contact
            [self showActionSheetForMode:kActionSheetContact];
            break;
        }
        default:
            break;
    }
}

#pragma mark - UI Stuff

/**
 *  initially configures UI components, such as colors, fonts, etc
 */
-(void)configureUI{
    self.profileButton.layer.cornerRadius = 20.0f;
    self.profileButton.layer.borderWidth = .5f;
    self.profileButton.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.profileButton.layer.masksToBounds = YES;
    
    //initially hide image view
    self.aptImageView.alpha = 0.0;
//    self.aptManagementLogo.alpha = 0.0;
    
    //collectionview - Init cell and register the nib file
    UINib *cellNib = [UINib nibWithNibName:@"CMQHomeCollectionViewCell" bundle:self.comBundle];
    [self.collectionView registerNib:cellNib forCellWithReuseIdentifier:contentCellIdentifier];
}

/**
 *  gets the start point for the collection view (ie. contentInset y-value)
 *
 *  @return the y-value to start the collectionview content
 */
-(CGFloat)getCollectionViewContentStartPoint{
    CGFloat viewHeight = self.view.frame.size.height;   //our view height
    CGFloat cvContentHeight = celltopReveal * self.viewModel.cellTitles.count;  //the amount of each cell that is showing * the number of cells
    CGFloat cvGap = viewHeight-cvContentHeight; //our views height minus the height of the collection view's visible content
    CGFloat imgHeight = 175.00;
    CGFloat aptImgHeight = self.aptImgHeightConstraint.constant;
    CGFloat cvStartPoint;
    
    //if our cvGap is greater than the apartment image height, make that our start point and adjust our apartment image height too. This is mostly used for iPhone 6(+). Otherwise there's a big awkward gap. If the apartment image height is bigger, use that. Otherwise on iPhone 4 the apartment image is covered up by the collection view. Maybe not the best way to handle it, but so be it.
    if(cvGap > imgHeight){
        cvStartPoint = cvGap;
        self.aptImgHeightConstraint.constant = cvGap + 10.00;
        [self.view layoutIfNeeded];
    }else
        cvStartPoint = aptImgHeight - 10.00;

    return cvStartPoint;
}

/**
 *  sets the collectionview layout to our stackedLayout property
 *
 *  @param animated whether or not to animate it
 */
-(void)resetStackLayout:(BOOL)animated{
    //layout
    self.stackedLayout.itemSize = CGSizeMake(self.view.frame.size.width, self.view.frame.size.height);  //item size
    self.stackedLayout.topReveal = celltopReveal;    //how much of each cell to show
    self.stackedLayout.layoutMargin = UIEdgeInsetsMake([self getCollectionViewContentStartPoint], 0, 0, 0); //start CV below apartment image
    [self.collectionView setCollectionViewLayout:self.stackedLayout animated:animated];
}

/**
 * Checks for app update
 */
-(void)checkForUpdate:(id)sender{
    if (CMQAD.shouldCheckForUpdate)
        [[CMQAPIClient sharedClient]fetchUpdate];
}

/**
 * Checks content statuses and updates icon visibility appropriately
 */
-(void)checkContentStatuses:(id)sender{
    [[CMQUser currentUser]fetchIfNeededInBackground];
    CMQUser *user = [CMQUser currentUser];
    
    for(CMQHomeCollectionViewCell *cell in self.collectionView.visibleCells){
        NSString *title = cell.titleLabel.text;
        if ([title isEqualToString:@"Messages"] && !user.messagesUpToDate)
            [cell.iconNew setImage:[UIImage imageNamed:@"icon-new" inBundle:_comBundle compatibleWithTraitCollection:nil] forState:UIControlStateNormal];
        else if ([title isEqualToString:@"Events"] && !user.eventsUpToDate)
            [cell.iconNew setImage:[UIImage imageNamed:@"icon-new" inBundle:_comBundle compatibleWithTraitCollection:nil] forState:UIControlStateNormal];
        else if ([title isEqualToString:@"News"] && !user.newsUpToDate)
            [cell.iconNew setImage:[UIImage imageNamed:@"icon-new" inBundle:_comBundle compatibleWithTraitCollection:nil] forState:UIControlStateNormal];
        else
            [cell.iconNew setImage:nil forState:UIControlStateNormal];
    }
    
    // Go ahead and update user's active status
    user[@"isActive"] = [NSNumber numberWithBool:YES];
    [user saveInBackground];
}

/**
 * starts Uber logic 
 */
/*-(void)startUber{
    UBSDKAccessToken *token = [UBSDKTokenManager fetchToken];
    
    if(token){
        [self buildUber];
    }else{
        UBSDKLoginManager *loginManager = [[UBSDKLoginManager alloc] initWithLoginType:UBSDKLoginTypeImplicit];
        [loginManager loginWithRequestedScopes:@[UBSDKRidesScope.RideWidgets] presentingViewController:self completion:^(UBSDKAccessToken * _Nullable accessToken, NSError * _Nullable error) {
            if (accessToken){
                    [self buildUber];
            }
        }];
    }
}*/

/**
 * builds Uber parameters
 */
/*-(void)buildUber{
    UBSDKRideRequestViewRequestingBehavior *requestBehavior = [[UBSDKRideRequestViewRequestingBehavior alloc] initWithPresentingViewController:self];
    requestBehavior.modalRideRequestViewController.rideRequestViewController.delegate = self;
    __block UBSDKRideParametersBuilder *builder = [[UBSDKRideParametersBuilder alloc] init];
    [builder setPickupToCurrentLocation];
    
    if ([uberMode isEqualToString:kUberModeHome]){
        BOOL canProcessAddress = YES;
        
        NSMutableString *address = [NSMutableString stringWithString:@""];
        
        CMQApartment *apartment = [CMQAD apartment];
        if (apartment.aptAddress && apartment.aptCity && apartment.aptState && apartment.aptZip){
            [address appendString:[NSString stringWithFormat:@"%@, %@, %@ %@", apartment.aptAddress, apartment.aptCity, apartment.aptState, apartment.aptZip]];
        }else{
            canProcessAddress = NO;
        }
        
        if (canProcessAddress){
            CLGeocoder *geocoder = [[CLGeocoder alloc] init];
            [geocoder geocodeAddressString:address completionHandler:^(NSArray* placeMarks, NSError* error){
                if ([placeMarks count] == 1){
                    CLPlacemark* placeMark = [placeMarks objectAtIndex:0];
                    
                    [builder setDropoffLocation:placeMark.location address:address];
                    [self showUberWithParameterBuilder:builder behavior:requestBehavior];
                }else{
                    [SVProgressHUD showInfoWithStatus:@"We couldn't find your home address, you will need to enter your destination"];
                    [self showUberWithParameterBuilder:builder behavior:requestBehavior];
                }
            }];
        }else{
            [SVProgressHUD showInfoWithStatus:@"We couldn't find your home address, you will need to enter your destination"];
            [self showUberWithParameterBuilder:builder behavior:requestBehavior];
        }
    }else if ([uberMode isEqualToString:kUberModePickup]){
        [self showUberWithParameterBuilder:builder behavior:requestBehavior];
    }
}*/

/**
 * shows Uber
 */
/*-(void)showUberWithParameterBuilder:(UBSDKRideParametersBuilder *)builder behavior:(UBSDKRideRequestViewRequestingBehavior *)requestBehavior {
    id tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"Uber"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
    UBSDKRideParameters *parameters = [builder build];
    
    UBSDKRideRequestButton *button = [[UBSDKRideRequestButton alloc] initWithRideParameters:parameters requestingBehavior:requestBehavior];
    [button sendActionsForControlEvents:UIControlEventTouchUpInside];
}*/

/**
 *  shows the login VC
 */
-(void)showLogin{
    self.loginVC = [[CMQOldLoginViewController alloc]initWithNibName:@"CMQOldLoginViewController" bundle:self.comBundle];
    [self presentViewController:self.loginVC animated:YES completion:nil];
}

/**
 *  shows the communities VC
 */
-(void)showCommunities{
    self.communityVC = [[CMQCommunityViewController alloc]initWithNibName:@"CMQCommunityViewController" bundle:self.comBundle];
    self.communityVC.delegate = self;
    
    [self presentViewController:self.communityVC animated:YES completion:nil];
}

/**
 *  shows the communities posts VC
 */
-(void)showCommunityPosts{
    [SVProgressHUD showWithStatus:@"Loading all posts"];
    
    self.communityPostsVC = [[CMQCommunityPostsViewController alloc]initWithNibName:@"CMQCommunityPostsViewController" bundle:self.comBundle];
    self.communityPostsVC.delegate = self;
    
    [self presentViewController:self.communityPostsVC animated:YES completion:nil];
}

/**
 * shows profile VC
 */
-(void)showProfile{
    self.profileCollectionVC = [[CMQProfileCollectionViewController alloc]initWithNibName:@"CMQProfileCollectionViewController" bundle:self.comBundle];
    self.navigationController.delegate = self;
    [self.navigationController pushViewController:self.profileCollectionVC animated:YES];
}

/**
 * shows wifi support popup
 */
-(void)showWifiSupport {
    self.wifiSupportView.layer.cornerRadius = 5;
    self.wifiSupportView.center = self.view.center;
    self.wifiSupportTextView.layer.borderWidth = 0.5f;
    self.wifiSupportTextView.layer.cornerRadius = 5;
    self.wifiSupportTextView.layer.borderColor = [[UIColor grayColor] CGColor];
    [[[[UIApplication sharedApplication]delegate]window] addSubview:self.wifiSupportView];
    [UIView animateWithDuration:0.2 animations:^{
        self.view.alpha = 0.5;
    }];
    
    [self.collectionView setUserInteractionEnabled:NO];
    [self.switchCommunityButton setUserInteractionEnabled:NO];
//    [self.uberButton setUserInteractionEnabled:NO];
    [self.payLeaseButton setUserInteractionEnabled:NO];
    [self.smartHomeButton setUserInteractionEnabled:NO];
    [self.profileButton setUserInteractionEnabled:NO];
    
    [self.wifiSupportView doPopInAnimationWithDuration:0.4];
}

/**
 * dismisses wifi support popup
 */
-(void)dismissWifiSupport{
    [self.wifiSupportTextView setText:@""];
    [self.wifiSupportView removeFromSuperview];
    [UIView animateWithDuration:0.2 animations:^{
        self.view.alpha = 1.0;
    }];
    
    [self.collectionView setUserInteractionEnabled:YES];
    [self.switchCommunityButton setUserInteractionEnabled:YES];
//    [self.uberButton setUserInteractionEnabled:YES];
    [self.payLeaseButton setUserInteractionEnabled:YES];
    [self.smartHomeButton setUserInteractionEnabled:YES];
    [self.profileButton setUserInteractionEnabled:YES];
}

- (IBAction)onCancelWifiSupport:(id)sender {
    [self dismissWifiSupport];
}

/**
 * send email to wifi support
 */
- (IBAction)onSubmitWifiSupport:(id)sender {
    //stuff
    NSString *text = self.wifiSupportTextView.text;
    
    if (![text isEqualToString:@""]){
        //NOTE: Presenting the mail VC in the iOS 8 simulator has a known bug where it won't prefill the recipients, messagebody, etc and will close itself after about 2 seconds. This does not happen for a device, so we're not worrying about it at the time of this writing (10/9/2014). A known workaround is to use a static global variable for the mail VC and "recycle" it. See SO answer here: http://stackoverflow.com/a/25864182/3469207
        if([MFMailComposeViewController canSendMail]){
            MFMailComposeViewController *vc = [[MFMailComposeViewController alloc]init];
            vc.mailComposeDelegate = self;
            [vc setMessageBody:[NSString stringWithFormat:
                                @"%@\n\n\nProblem:\n%@\n\n\nSent from the %@ App %@\nA Communiqué Product",
                                [[CMQAPIClient sharedClient] fetchUserAttributes],
                                text,
                                [[[NSBundle mainBundle] localizedInfoDictionary] objectForKey:@"CFBundleDisplayName"],
                                [UIApplication versionAndBuild]] isHTML:NO];
            [vc setToRecipients:@[kEpproachSupportEmail]];
            [vc setSubject:[NSString stringWithFormat:@"%@: Wifi Support Request", [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleDisplayName"]]];
            
            [self dismissWifiSupport];
            [self presentViewController:vc animated:YES completion:nil];
        }else
            [SVProgressHUD showInfoWithStatus:@"To use this feature, please set up email on this device"];
    }
}

/**
 * call wifi support
 */
- (IBAction)onCallWifiSupport:(id)sender {
    //stuff
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"tel://"]]){
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:kEpproachSupportPhoneNumber]];
        [self dismissWifiSupport];
    }else
        [SVProgressHUD showInfoWithStatus:@"This device cannot make calls"];
    
}

/**
 *  shows an instance of DZNWebViewController with the given URL
 *
 *  @param url the url to show in the web view
 */
-(void)showWebViewWithURL:(NSURL *)url backgroundColor:(UIColor *)bgColor tintColor:(UIColor *)tintColor title:(NSString *)title{
    //create our web view controller
    
    //check url for validity
    if ([[url absoluteString].lowercaseString hasPrefix:@"http"])
        self.webViewController = [[DZNWebViewController alloc]initWithURL:url];
    else
        self.webViewController = [[DZNWebViewController alloc]initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://%@", [url absoluteString]]]];
    
//    self.webViewController.toolbarTintColor = tintColor;
//    self.webViewController.toolbarBackgroundColor = bgColor;
//    self.webViewController.supportedweb
    //    self.webViewController.supportedActions = DZNWebViewControllerActionNone;
    //    self.webViewController.titleColor = [UIColor clearColor];
    self.webViewController.supportedWebNavigationTools = DZNWebNavigationToolAll;
    self.webViewController.supportedWebActions = DZNWebActionAll;
    self.webViewController.showLoadingProgress = YES;
    self.webViewController.allowHistory = YES;
    self.webViewController.hideBarsWithGestures = YES;
    
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithTitle:@"<" style:UIBarButtonItemStylePlain target:self action:@selector(dismissWebViewController:)];
    barButton.tintColor = [UIColor blackColor];
    [barButton setTitleTextAttributes:@{NSFontAttributeName: [UIFont fontWithName:kCMQFontGeometriaLight size:19] } forState:UIControlStateNormal];
    self.webViewController.navigationItem.leftBarButtonItem = barButton;
    
    UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:self.webViewController];
//    navController.navigationBar.barTintColor = bgColor;
    
    [self presentViewController:navController animated:YES completion:nil];
}

/**
 * Called when we need to present a view controller to send an email
 */
- (void)presentMailViewControllerForEmail:(NSString *)email {
    //NOTE: Presenting the mail VC in the iOS 8 simulator has a known bug where it won't prefill the recipients, messagebody, etc and will close itself after about 2 seconds. This does not happen for a device, so we're not worrying about it at the time of this writing (10/9/2014). A known workaround is to use a static global variable for the mail VC and "recycle" it. See SO answer here: http://stackoverflow.com/a/25864182/3469207
    if([MFMailComposeViewController canSendMail]){
        MFMailComposeViewController *vc = [[MFMailComposeViewController alloc]init];
        vc.mailComposeDelegate = self;
        [vc setMessageBody:[NSString stringWithFormat:@"Type your message here:\n\n\n\n\n\n%@\n\n\nSent from the %@ App %@\nA Communiqué Product", [[CMQAPIClient sharedClient] fetchUserAttributes], [[[NSBundle mainBundle] localizedInfoDictionary] objectForKey:@"CFBundleDisplayName"], [UIApplication versionAndBuild]] isHTML:NO];
        [vc setToRecipients:@[email]];
        
        [self presentViewController:vc animated:YES completion:nil];
    }else
        [SVProgressHUD showInfoWithStatus:@"To use this feature, please set up email on this device"];
}

/**
 * presents an action sheet to be used for selection which method the user
 * would like to use to select a call recipient
 */
-(void)showActionSheetForMode:(NSString *)mode{
    /*if ([mode isEqualToString:kActionSheetUber]){ // uber
        UIAlertController * alert;
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) // iPad
            alert = [UIAlertController
                     alertControllerWithTitle:@"How would you like to use Uber?"
                     message:nil
                     preferredStyle:UIAlertControllerStyleAlert];
        else // iPhone
            alert = [UIAlertController
                     alertControllerWithTitle:@"How would you like to use Uber?"
                     message:nil
                     preferredStyle:UIAlertControllerStyleActionSheet];
        
        UIAlertAction* home = [UIAlertAction
                                 actionWithTitle:@"Take me home"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     uberMode = kUberModeHome;
                                     [self startUber];
                                     [alert dismissViewControllerAnimated:YES completion:nil];
                                 }];
        
        UIAlertAction* request = [UIAlertAction
                              actionWithTitle:@"Request a ride"
                              style:UIAlertActionStyleDefault
                              handler:^(UIAlertAction * action)
                              {
                                  uberMode = kUberModePickup;
                                  [self startUber];
                                  [alert dismissViewControllerAnimated:YES completion:nil];
                              }];
        
        UIAlertAction* cancel = [UIAlertAction
                                 actionWithTitle:@"Cancel"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     [alert dismissViewControllerAnimated:YES completion:nil];
                                 }];
        
        
        [alert addAction:home];
        [alert addAction:request];
        [alert addAction:cancel];
        [self presentViewController:alert animated:YES completion:nil];
    }else*/ if ([mode isEqualToString:kActionSheetContact]){ // contact
        id tracker = [[GAI sharedInstance] defaultTracker];
        [tracker set:kGAIScreenName value:@"Community Contacts"];
        [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
        
        UIAlertController * alert;
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) // iPad
            alert = [UIAlertController
                     alertControllerWithTitle:@"Who would you like to contact?"
                     message:nil
                     preferredStyle:UIAlertControllerStyleAlert];
        else // iPhone
            alert = [UIAlertController
                     alertControllerWithTitle:@"Who would you like to contact?"
                     message:nil
                     preferredStyle:UIAlertControllerStyleActionSheet];

        contacts = [[NSMutableArray alloc] init];
        
        if (CMQAD.apartment.aptEmail && CMQAD.apartment.aptEmail.length > 0){
            UIAlertAction* action = [UIAlertAction
                                   actionWithTitle:@"Email Office"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action)
                                   {
                                       [self presentMailViewControllerForEmail:CMQAD.apartment.aptEmail];
                                       [alert dismissViewControllerAnimated:YES completion:nil];
                                   }];
            [contacts addObject:action];
        }
        
        if (CMQAD.apartment.aptPhone && CMQAD.apartment.aptPhone.length > 0){
            UIAlertAction* action = [UIAlertAction
                                     actionWithTitle:@"Call Office"
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction * action)
                                     {
                                         if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"tel://"]]){
                                             [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel:%@", CMQAD.apartment.aptPhone]]];
                                             [alert dismissViewControllerAnimated:YES completion:nil];
                                         }else
                                             [SVProgressHUD showInfoWithStatus:@"This device cannot make calls"];
                                     }];
            [contacts addObject:action];
        }
        
        if (CMQAD.apartment.additionalContacts){
            for (NSArray *additionalContact in CMQAD.apartment.additionalContacts) {
                if ([additionalContact[0] isEqualToString:kContactModeEmail]){
                    UIAlertAction* action = [UIAlertAction
                                             actionWithTitle:[NSString stringWithFormat:@"Email %@", additionalContact[1]]
                                             style:UIAlertActionStyleDefault
                                             handler:^(UIAlertAction * action)
                                             {
                                                 [self presentMailViewControllerForEmail:additionalContact[2]];
                                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                             }];
                    [contacts addObject:action];
                }else if ([additionalContact[0] isEqualToString:kContactModePhone]){
                    UIAlertAction* action = [UIAlertAction
                                             actionWithTitle:[NSString stringWithFormat:@"Call %@", additionalContact[1]]
                                             style:UIAlertActionStyleDefault
                                             handler:^(UIAlertAction * action)
                                             {
                                                 if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"tel://"]]){
                                                     [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel:%@", additionalContact[2]]]];
                                                     [alert dismissViewControllerAnimated:YES completion:nil];
                                                 }else
                                                     [SVProgressHUD showInfoWithStatus:@"This device cannot make calls"];
                                             }];
                    [contacts addObject:action];
                }
            }
        }
        
        if (CMQAD.apartment.hasEpproachSupport){
            UIAlertAction* action = [UIAlertAction
                                     actionWithTitle:@"Contact Epproach WiFi Support"
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction * action)
                                     {
                                         [self showWifiSupport];
                                         [alert dismissViewControllerAnimated:YES completion:nil];
                                     }];
            [contacts addObject:action];
        }
        
        UIAlertAction* cancel = [UIAlertAction
                                 actionWithTitle:@"Cancel"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     [alert dismissViewControllerAnimated:YES completion:nil];
                                 }];
        [contacts addObject:cancel];
        
        for (UIAlertAction *action in contacts) {
            [alert addAction:action];
        }

        [self presentViewController:alert animated:YES completion:nil];
    }
}

/**
 *  sets the apartment home background image with the image from our CMQApartment object (CMQAD.apartment)
 */
-(void)setApartmentHomeImage{
    //fetch our image data on a background thread
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(queue, ^{
        UIImage *image = [UIImage getImageFromPFFile:CMQAD.apartment.aptPhoto];
        
        //display our image on the main thread
        dispatch_async(dispatch_get_main_queue(), ^{
            self.aptImageView.image = image;
            [UIView animateWithDuration:0.5 animations:^{
                self.aptImageView.alpha = 1.0;
            }];
        });
    });
}

/**
 *  sets the logo image with the image from our CMQApartment object (CMQAD.apartment),
 *  if no apartment logo exists then use logo from our CMQManagement object (CMQAD.management),
 *  if no management logo exists then use default Communique logo
 */
//-(void)setApartmentManagementLogoImage {
//    //fetch our image data on a background thread
//    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
//    dispatch_async(queue, ^{
//        UIImage *image = [UIImage getImageFromPFFile:CMQAD.apartment.aptLogo];
//        CGImageRef cgref = [image CGImage];
//        CIImage *cim = [image CIImage];
//        
//        UIImage *image2 = [UIImage getImageFromPFFile:CMQAD.management.companyLogo];
//        CGImageRef cgref2 = [image2 CGImage];
//        CIImage *cim2 = [image2 CIImage];
//        
//        //display our image on the main thread
//        dispatch_async(dispatch_get_main_queue(), ^{
//            // Check image data for null
//            // (management has option to NOT use logo), in which case
//            // the default Communique logo will be used
//            if (cim == nil && cgref == NULL){
//                if (cim2 == nil && cgref2 == NULL)
//                    self.aptManagementLogo.image = [UIImage imageNamed:@"home-logo"];
//                else
//                    self.aptManagementLogo.image = image2;
//            }else
//                self.aptManagementLogo.image = image;
//
//            [UIView animateWithDuration:0.5 animations:^{
//                self.aptManagementLogo.alpha = 1.0;
//            }];
//        });
//    });
//}

/**
 *  clears out any apartment specific settings for when the user logs out
 */
-(void)resetViewAfterLogout{
    self.collectionView.hidden = YES;
    self.profileButton.hidden = YES;
    self.switchCommunityButton.hidden = YES;
    self.uberButton.hidden = YES;
    self.payLeaseButton.hidden = YES;
    self.smartHomeButton.hidden = YES;
    self.aptImageView.image = nil;
//    self.aptManagementLogo.image = nil;
    self.aptImageView.alpha = 0.01;
//    self.uberButtonLeadingSpaceConstraint.constant = UberLeadingSpace;
    self.payLeaseButtonLeadingSpaceConstraint.constant = PayLeaseLeadingSpace;
    self.smartHomeButtonLeadingSpaceConstraint.constant = SmartHomeLeadingSpace;
    self.packagesButtonLeadingSpaceConstraint.constant = PackagesLeadingSpace;
    CMQAD.apartment = nil;
}

/**
 *  called from our web view's navigation controller's bar button item
 *
 *  @param sender the bar button item to dismiss the webview
 */
- (void)dismissWebViewController:(id)sender{
    [_webViewController dismissViewControllerAnimated:YES completion:^{
        _webViewController = nil;
    }];
}

/**
 *  called from our web view's navigation controller's bar button item
 *
 *  @param email the string representation of email address
 */
- (void)dismissWebViewControllerForHelp:(NSString *)email{
    [_webViewController dismissViewControllerAnimated:YES completion:^{
        _webViewController = nil;
        [self presentMailViewControllerForEmail:email];
    }];
}

-(IBAction)homeScreen:(UIStoryboardSegue*)segue{
    
}
#pragma mark - Button Actions

/**
 *  presents the ProfileVC
 *
 *  @param sender the profile button
 */
- (IBAction)onGoProfile:(id)sender {
    self.profileCollectionVC = [[CMQProfileCollectionViewController alloc]initWithNibName:@"CMQProfileCollectionViewController" bundle:self.comBundle];
    self.navigationController.delegate = self;
    [self.navigationController pushViewController:self.profileCollectionVC animated:YES];
}

- (IBAction)onGoPackages:(id)sender {
    
    UINavigationController* packagesInitial=  [[UIStoryboard storyboardWithName:@"Packages" bundle:self.comBundle]instantiateInitialViewController];
    //self.packagesVC=[[CMQPackagesViewController alloc]init];
    //self.navigationController.delegate = self;
    [self presentViewController:packagesInitial animated:YES completion:nil];
    //[self.navigationController pushViewController:self.packagesVC animated:YES];
}

/*- (IBAction)onGoPackages:(id)sender {
 self.signatureVC = [[CMQPackagesSignatureViewController alloc]init];
 self.navigationController.delegate = self;
 [self.navigationController pushViewController:self.signatureVC animated:YES];
 }*/

/**
 *  presents the CommunityVC
 *
 *  @param sender the switch button
 */
- (IBAction)onGoSwitchCommunities:(id)sender {
    [self showCommunities];
}

/**
 *  presents the Uber widget
 *
 *  @param sender the uber button
 */
/*- (IBAction)onGoUber:(id)sender {
    if (uberButtonTapped){
        if (CLLocationManager.authorizationStatus == kCLAuthorizationStatusAuthorizedWhenInUse || CLLocationManager.authorizationStatus == kCLAuthorizationStatusAuthorizedAlways){
            [self showActionSheetForMode:kActionSheetUber];
        }else{
            self.locationManager = [[CLLocationManager alloc] init];
            self.locationManager.delegate = self;
            [self.locationManager requestWhenInUseAuthorization];
        }
    }else{
        NSString *message = @"This service is provided by:\nUber";
        
        UIAlertController *alert = [UIAlertController
                                    alertControllerWithTitle:@"External Service"
                                    message:message
                                    preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:@"Ok"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 uberButtonTapped = YES;
                                 [self.uberButton sendActionsForControlEvents:UIControlEventTouchUpInside];
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                             }];
        
        UIAlertAction* cancel = [UIAlertAction
                                 actionWithTitle:@"Cancel"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     [alert dismissViewControllerAnimated:YES completion:nil];
                                 }];
        
        [alert addAction:ok];
        [alert addAction:cancel];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
}*/

/**
 * attempts PayLease SSO via cloud code
 *
 * @param sender the paylease button
 */
- (IBAction)onGoPayLease:(id)sender {
    if (payLeaseButtonTapped){
        [PFCloud callFunctionInBackground:@"performPayLeaseSSO" withParameters:nil block:^(NSDictionary *dict, NSError *error) {
            if(!error){
                
                id tracker = [[GAI sharedInstance] defaultTracker];
                [tracker set:kGAIScreenName value:@"PayLease"];
                [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
                
                if ([dict objectForKey:@"hasLateFee"] == [NSNumber numberWithBool:YES]){ // detected late fee
                    NSString *message = [NSString stringWithFormat:@"We detected a late fee of $%@.", [dict objectForKey:@"lateAmount"]];
                    UIAlertController *alert = [UIAlertController
                                                alertControllerWithTitle:@"You have a late fee!"
                                                message:message
                                                preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction* ok = [UIAlertAction
                                         actionWithTitle:@"Ok"
                                         style:UIAlertActionStyleDefault
                                         handler:^(UIAlertAction * action)
                                         {
                                             [self showWebViewWithURL:[NSURL URLWithString:[dict objectForKey:@"sso_url"]]
                                                      backgroundColor:[UIColor CMQYellowColor]
                                                            tintColor:[UIColor blackColor]
                                                                title:@"PayLease"];
                                         }];
                    
                    [alert addAction:ok];
                    
                    [self presentViewController:alert animated:YES completion:nil];
                }else{ // no late fee detected
                    [self showWebViewWithURL:[NSURL URLWithString:[dict objectForKey:@"sso_url"]]
                             backgroundColor:[UIColor CMQYellowColor]
                                   tintColor:[UIColor blackColor]
                                       title:@"PayLease"];
                }
            }
        }];
    }else{
        NSString *message = @"This service is provided by:\nPayLease";
        
        UIAlertController *alert = [UIAlertController
                                    alertControllerWithTitle:@"External Service"
                                    message:message
                                    preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:@"Ok"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 payLeaseButtonTapped = YES;
                                 [self.payLeaseButton sendActionsForControlEvents:UIControlEventTouchUpInside];
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                             }];
        
        UIAlertAction* cancel = [UIAlertAction
                                 actionWithTitle:@"Cancel"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     [alert dismissViewControllerAnimated:YES completion:nil];
                                 }];
        
        [alert addAction:ok];
        [alert addAction:cancel];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
}

/**
 * attempts Smarthome SSO via cloud code
 *
 * @param sender the smarthome button
 */
- (IBAction)onGoSmartHome:(id)sender {
    if (smartHomeButtonTapped){
        [self.smartHomeButton setUserInteractionEnabled:NO];
        [PFCloud callFunctionInBackground:@"performSmartHomeSSO" withParameters:nil block:^(NSString *url, NSError *error) {
            [self.smartHomeButton setUserInteractionEnabled:YES];
            
            if (!error && url) {
                NSLog(@"%@",url);
                id tracker = [[GAI sharedInstance] defaultTracker];
                [tracker set:kGAIScreenName value:@"SmartHome by Epproach"];
                [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
                
                [self showWebViewWithURL:[NSURL URLWithString:url]
                         backgroundColor:[UIColor CMQYellowColor]
                               tintColor:[UIColor blackColor]
                                   title:@"SmartHome by Epproach"];
            }else{
                NSString *message = [NSString stringWithFormat:@"ERROR: %ld\nFailed SmartHome login, contact Epproach support?", (long)error.code];
                
                UIAlertController *alert = [UIAlertController
                                            alertControllerWithTitle:@"Uh Oh!"
                                            message:message
                                            preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* ok = [UIAlertAction
                                     actionWithTitle:@"Contact"
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction * action)
                                     {
                                         [self showWifiSupport];
                                         [alert dismissViewControllerAnimated:YES completion:nil];
                                     }];
                
                UIAlertAction* cancel = [UIAlertAction
                                         actionWithTitle:@"Not now"
                                         style:UIAlertActionStyleDefault
                                         handler:^(UIAlertAction * action)
                                         {
                                             [alert dismissViewControllerAnimated:YES completion:nil];
                                         }];
                
                [alert addAction:ok];
                [alert addAction:cancel];
                
                [self presentViewController:alert animated:YES completion:nil];
            }
        }];
    }else{
        NSString *message = @"This service is provided by:\nEpproach Communications";
        
        UIAlertController *alert = [UIAlertController
                                    alertControllerWithTitle:@"External Service"
                                    message:message
                                    preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:@"Ok"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 smartHomeButtonTapped = YES;
                                 [self.smartHomeButton sendActionsForControlEvents:UIControlEventTouchUpInside];
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                             }];
        
        UIAlertAction* cancel = [UIAlertAction
                                 actionWithTitle:@"Cancel"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     [alert dismissViewControllerAnimated:YES completion:nil];
                                 }];
        
        [alert addAction:ok];
        [alert addAction:cancel];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
}

#pragma mark - CMQAPIClient Delegate

-(void)CMQAPIClientDidFailFetchWithMessage:(NSString *)message isCache:(BOOL)isCache{
    DLogRed(@"failed to fetch apartment photo: %@, isCache: %d", message, isCache);
    if(!isCache) [SVProgressHUD showInfoWithStatus:@"Failed to load community, some features may be temporarily unavailable"];
}

-(void)CMQAPIClientDidFailSwitchWithMessage:(NSString *)message isCache:(BOOL)isCache{
    DLogRed(@"failed to switch apartment: %@, isCache: %d", message, isCache);
    if(!isCache) [SVProgressHUD showInfoWithStatus:@"Failed to switch community, some features may be temporarily unavailable"];
}

- (void)CMQAPIClientDidFetchUpdateSuccessfully:(PFObject *)object{
    CMQAD.shouldCheckForUpdate = NO;
    
    CMQUpdate *update = (CMQUpdate *)object;
    if (update.isiOSUpdate){
        NSString *version = [[NSBundle mainBundle]objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
        
        if (![version isEqualToString:update.version]){
            DLogRed(@"App is NOT up-to-date");
            
            NSString *message = update.message;
            
            for(NSArray *messageArray in update.messages){
                if([messageArray[0] isEqualToString:version])
                    message = messageArray[1];
            }
            
            UIAlertController *alert = [UIAlertController
                                        alertControllerWithTitle:@"Update Available!"
                                        message:message
                                        preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction
                                 actionWithTitle:@"Update"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@", kiTunesURL ,kApplicationId]]];
                                     [alert dismissViewControllerAnimated:YES completion:nil];
                                 }];
            
            UIAlertAction* cancel = [UIAlertAction
                                     actionWithTitle:@"Not now"
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction * action)
                                     {
                                         [alert dismissViewControllerAnimated:YES completion:nil];
                                     }];
            
            [alert addAction:ok];
            [alert addAction:cancel];
            
            [self presentViewController:alert animated:YES completion:nil];
        }else
            DLogGreen(@"App is up-to-date");
    }
}

- (void)CMQAPIClientDidFetchApartmentSuccessfully:(NSArray *)apartments{
    [SVProgressHUD dismiss];
    CMQApartment *apartment = (CMQApartment *)[apartments objectAtIndex:0];   //this should only ever return 1 thing
    
    //check if we've already set our self.apartment property. This would be set first by our fetchApartment call returning cached data. This method gets run again once that call checks the server. If they're different, update our self.apartment, otherwise leave it be.
    if(!CMQAD.apartment || ([apartment.updatedAt compare:CMQAD.apartment.updatedAt] != NSOrderedSame)){
        CMQAD.apartment = apartment;
        [self setApartmentHomeImage];
    }
    
    // Associate installation with community
    PFInstallation *installation = [PFInstallation currentInstallation];
    installation[@"apartment"] = CMQAD.apartment;
    installation[@"aptName"] = CMQAD.apartment[@"aptName"];
    [installation saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error){
        if(succeeded)DLogGreen(@"associated apartment with installation");
        if(error)[[CMQAPIClient sharedClient] handleParseError:error];
    }];
    
    // Update user's tempAptComplexID
    if (![CMQUser currentUser].tempAptComplexID || [CMQUser currentUser].tempAptComplexID.length == 0){
        CMQUser *user = [CMQUser currentUser];
        user[@"tempAptComplexID"] = [CMQAD.apartment objectId];
        [user saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error){
            if(succeeded)DLogGreen(@"updated user's tempAptComplexID");
            if(error)[[CMQAPIClient sharedClient] handleParseError:error];
        }];
    }
    BOOL canShowPackages = ([CMQUser currentUser].isAdmin)&&(apartment.hasPackageServiceEnabled);
    _packagesButton.hidden=!canShowPackages;
    //create view model
    //[CMQMessageDataSourceManager sharedInstance].hasPackageDelivery = apartment.hasPackageDelivery;
    self.viewModel = [[CMQHomeViewModel alloc]init];
    
    /*******************************************************************************************/
    /*********************** Items in this block are here for UI reasons ***********************/
    /*******************************************************************************************/
    [self.collectionView reloadData];
    
    [self resetStackLayout:YES];
    
    // Show collection view
    ([CMQUser currentUser]) ? [self.collectionView setHidden:NO] : [self.collectionView setHidden:YES];
    
    self.switchCommunityButtonLeadingSpaceConstraint.constant = SwitchLeadingSpace;
//    self.uberButtonLeadingSpaceConstraint.constant = UberLeadingSpace;
    self.payLeaseButtonLeadingSpaceConstraint.constant = PayLeaseLeadingSpace;
    self.smartHomeButtonLeadingSpaceConstraint.constant = SmartHomeLeadingSpace;
    self.packagesButtonLeadingSpaceConstraint.constant = PackagesLeadingSpace;
    
    // Determine if user is associated with > 1 community and show 'switch' button if applicable
    if ([CMQUser currentUser].communities && [[CMQUser currentUser].communities count] > 1 &&
        ([[CMQUser currentUser].userRole isEqualToString:kPermissionStaff] ||
         [[CMQUser currentUser].userRole isEqualToString:kPermissionRegionalAdmin] ||
         [[CMQUser currentUser].userRole isEqualToString:kPermissionCompanyAdmin])){
        self.switchCommunityButton.hidden = NO;
    }else{
        self.switchCommunityButton.hidden = YES;
//        self.uberButtonLeadingSpaceConstraint.constant = SwitchLeadingSpace;
        self.payLeaseButtonLeadingSpaceConstraint.constant = SwitchLeadingSpace;
        self.smartHomeButtonLeadingSpaceConstraint.constant = PayLeaseLeadingSpace;
        self.packagesButtonLeadingSpaceConstraint.constant = SmartHomeLeadingSpace;
    }
    
    //!!!!!
    self.uberButton.hidden = YES;
    
    /*if (CMQAD.apartment.hasUber){
        self.uberButton.hidden = NO;
    }else{
        self.uberButton.hidden = YES;
//        self.payLeaseButtonLeadingSpaceConstraint.constant = self.uberButtonLeadingSpaceConstraint.constant;
    }*/
    
    //!!!!!
    self.payLeaseButton.hidden = YES;
    
    if (CMQAD.apartment.hasPayLease){
        self.payLeaseButton.hidden = NO;
    }else{
        self.payLeaseButton.hidden = YES;
    }
    
    self.smartHomeButton.hidden = YES;
    if (CMQAD.apartment.hasSmartHomeIntegration && [CMQUser currentUser].isSmartHomeUser && [[CMQUser currentUser].userRole isEqualToString:@"Resident"]){
        self.smartHomeButtonLeadingSpaceConstraint.constant = PayLeaseLeadingSpace;
        self.smartHomeButton.hidden = NO;
    }else if(CMQAD.apartment.hasSmartHomeIntegration && [[CMQUser currentUser].userRole isEqualToString:@"Admin"] ){
        self.smartHomeButtonLeadingSpaceConstraint.constant = PayLeaseLeadingSpace;
        self.smartHomeButton.hidden = NO;
    }
    
    // Show profile button
    self.profileButton.hidden = NO;
    /*******************************************************************************************/
    
    [[CMQAPIClient sharedClient]fetchManagementWithId:CMQAD.apartment.management.objectId];
}

- (void)CMQAPIClientDidSwitchApartmentSuccessfully{
    // Community switch successful, load community
    [self resetViewAfterLogout];
    self.collectionView.hidden = NO;
    
    [self.communityVC dismissViewControllerAnimated:YES completion:^{
        [[CMQAPIClient sharedClient]fetchApartmentWithId:[CMQUser currentUser].tempAptComplexID];
        self.communityVC = nil;
    }];
}

- (void)CMQAPIClientDidFetchManagementSuccessfully:(NSArray *)mgmt{
    CMQManagement *managementCompany = (CMQManagement *)[mgmt objectAtIndex:0];
    
    CMQAD.management = managementCompany;
    
//    [self setApartmentManagementLogoImage];
}

-(void)CMQAPIClientDidLoginSuccessfullyWithAlerts:(BOOL)alerts channel:(NSString *)channel notifications:(BOOL)notifications{
    DLogGreen(@"User logged in");
    CMQUser *user = [CMQUser currentUser];
    
//    [UBSDKTokenManager deleteToken];
    
    if(channel)[[CMQAPIClient sharedClient]subscribeToChannel:channel];
    
    // Determine if user is associated with > 1 community
    if (user.communities && [user.communities count] > 1){ // User is associated with > 1 community
        [self.loginVC dismissViewControllerAnimated:YES completion:^{
            self.loginVC = nil;
            
            [SVProgressHUD dismiss];
            
            [self showCommunityPosts];
            
            if ([CMQUser currentUser].tempAptComplexID && [CMQUser currentUser].tempAptComplexID.length > 0)
                [[CMQAPIClient sharedClient]fetchApartmentWithId:[CMQUser currentUser].tempAptComplexID];
            else
                [[CMQAPIClient sharedClient]fetchApartmentWithId:[CMQUser currentUser].aptComplexID];
        }];
    }else { // User is associated with only 1 community
        self.collectionView.hidden = NO;
        
        [self.loginVC dismissViewControllerAnimated:YES completion:^{
            self.loginVC = nil;
            
            [SVProgressHUD showWithStatus:@"Loading apartment"];
            
            [[CMQAPIClient sharedClient]fetchApartmentWithId:[[user.communities objectAtIndex:0] objectId]];
        }];
    }
}

-(void)CMQAPIClientDidFailLoginWithMessage:(NSString *)message{
    DLogRed(@"failed login, %@", message);
    [SVProgressHUD showInfoWithStatus:message];
}

-(void)CMQAPIClientDidLogout{
    DLogOrange(@"User logged out");
//    [UBSDKTokenManager deleteToken];
    
    uberButtonTapped = payLeaseButtonTapped = smartHomeButtonTapped = NO;
    
    [self resetViewAfterLogout];
}

-(void)CMQAPIClientDidLogoutPreemptively{
    DLogOrange(@"User logged out preemptively");
//    [UBSDKTokenManager deleteToken];
    [self resetViewAfterLogout];
    
    [SVProgressHUD showInfoWithStatus:@"Your session has expired, please login to continue."];
    [self showLogin];
}

#pragma mark - CMQCommunityViewController Delegate

/**
 * called for CMQCommunityViewController 'All My Communities' Posts' index
 */
- (void)CMQCommunityViewControllerDidSelectCommunityPostsIndex:(CMQCommunityViewController *)controller{
    [SVProgressHUD showWithStatus:@"Loading all posts"];
    
    self.communityPostsVC = [[CMQCommunityPostsViewController alloc]initWithNibName:@"CMQCommunityPostsViewController" bundle:self.comBundle];
    self.communityPostsVC.delegate = self;
    
    [self.communityVC dismissViewControllerAnimated:YES completion:^{
        self.communityVC = nil;
        [self presentViewController:self.communityPostsVC animated:YES completion:nil];
    }];
}

#pragma mark - CMQCommunityPostsViewController Delegate

/**
 * called for CMQCommunityPostsViewController switch button
 */
- (void)CMQCommunityPostsViewControllerDidTapSwitchButton:(CMQCommunityPostsViewController *)controller{
    self.communityVC = [[CMQCommunityViewController alloc]initWithNibName:@"CMQCommunityViewController" bundle:self.comBundle];
    self.communityVC.delegate = self;
    
    [self.communityPostsVC dismissViewControllerAnimated:YES completion:^{
        self.communityPostsVC = nil;
        [self presentViewController:self.communityVC animated:YES completion:nil];
    }];
}

/**
 * called when CMQCommunityPostsViewController successfully switches community
 */
- (void)CMQCommunityPostsViewControllerDidSwitchCommunity:(CMQCommunityPostsViewController *)controller{
    [self.communityPostsVC dismissViewControllerAnimated:YES completion:^{
        [[CMQAPIClient sharedClient]fetchApartmentWithId:[CMQUser currentUser].tempAptComplexID];
        self.communityPostsVC = nil;
    }];
}

#pragma mark - CLLocation
   
/*- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status{
    // If user just allowed location permissions, start Uber process
    if (status == kCLAuthorizationStatusAuthorizedWhenInUse || status == kCLAuthorizationStatusAuthorizedAlways){
        self.locationManager = nil;
        [self showActionSheetForMode:kActionSheetUber];
    }else if (status != kCLAuthorizationStatusNotDetermined){
        [SVProgressHUD showInfoWithStatus:@"To use Uber, please allow location services for this app in your settings"];
    }
}*/

#pragma mark - Uber

/*- (void)rideRequestViewController:(UBSDKRideRequestViewController *)rideRequestViewController didReceiveError:(NSError *)error {
    // Handle error here
    RideRequestViewErrorType errorType = (RideRequestViewErrorType)error.code;
    
    switch (errorType) {
        case RideRequestViewErrorTypeAccessTokenExpired:
        case RideRequestViewErrorTypeAccessTokenMissing:
        case RideRequestViewErrorTypeUnknown:
        default:
            DLogRed(@"Uber error: %@", error.localizedDescription);
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:^{
        [SVProgressHUD showInfoWithStatus:kUberConnectionErrorString];
    }];
}*/

#pragma mark - UICollectionView Delegate

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    NSInteger row = indexPath.item;
    
    if(row < 3)
         [self exposeItemAtIndexPath:indexPath withCompletion:^{[self presentViewControllerForRow:row];}];
    else //otherwise just present modally
         [self presentViewControllerForRow:row];
}

/**
 *  changes the collectionview layout to an instance of TGLExposedLayout, which sets the item at the indexpath to fill the entire screen and the rest to stack up
 *
 *  @param indexPath the index path to fill the screen (expose)
 */
-(void)exposeItemAtIndexPath:(NSIndexPath *)indexPath withCompletion:(compBlock)completion{
    TGLExposedLayout *exposedLayout = [[TGLExposedLayout alloc] initWithExposedItemIndex:indexPath.item];
    
    exposedLayout.layoutMargin = UIEdgeInsetsMake(0.0, 0.0, 0.0, 0.0);  //the space between the collectionview content view and the first cell
    exposedLayout.topOverlap =  0.0;    //how much to overlap the exposed cell with the underlying cells
    exposedLayout.bottomOverlap = 0.0;  //how much to overlap the cells beneath (ie, farther down in the scroll view) with the exposed cell
    exposedLayout.itemSize = CGSizeMake(CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame));
    
    //update our layout with animation
    [self.collectionView setCollectionViewLayout:exposedLayout animated:YES completion:^(BOOL finished) {completion();}];
}

#pragma mark - UICollectionView DataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.viewModel.cellTitles.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    //create and configure cell
    CMQHomeCollectionViewCell *cell = (CMQHomeCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:contentCellIdentifier forIndexPath:indexPath];
    [self configureCell:cell atIndexPath:indexPath];
    [cell layoutIfNeeded];
    
    return cell;
}

/**
 *  configures an instance of CMQHomeCollectionViewCell
 *
 *  @param cell      the cell to configure
 *  @param indexPath the index path for the cell
 */
-(void)configureCell:(CMQHomeCollectionViewCell *)cell atIndexPath:(NSIndexPath *)indexPath{
    //set cell colors
    cell.contentView.backgroundColor = [self.viewModel.bgColors objectAtIndex:indexPath.item];
    cell.backgroundColor = [UIColor clearColor];
    [cell sendSubviewToBack:cell.contentView];
    
    //remove any imageviews for cell recycling purposes
    for(UIView *subview in cell.contentView.subviews){
        if([subview isKindOfClass:[UIImageView class]])
            [subview removeFromSuperview];
    }

    //round content view corners (not the last cell)
    if(indexPath.item != self.viewModel.cellTitles.count-1)
        [cell roundTopCornersForView:cell.contentView]; //content view gets rounded for background colors
    
    //set cell title, font
    UIColor *textColor = indexPath.item % 2 ? [UIColor CMQBrownColor] : [UIColor lightTextColor];
    cell.titleLabel.text  = [self.viewModel.cellTitles objectAtIndex:indexPath.item];
    cell.titleLabel.textColor = textColor;
    [cell.titleLabel setFont:[UIFont fontWithName:kCMQFontGeometriaLight size:19]];
    
    CMQUser *user = [CMQUser currentUser];
    
    if (user){
        if (indexPath.item == 0 && !user.messagesUpToDate)
            [cell.iconNew setImage:[UIImage imageNamed:@"icon-new" inBundle:_comBundle compatibleWithTraitCollection:nil] forState:UIControlStateNormal];
        else if(indexPath.item == 1 && !user.eventsUpToDate)
            [cell.iconNew setImage:[UIImage imageNamed:@"icon-new" inBundle:_comBundle compatibleWithTraitCollection:nil] forState:UIControlStateNormal];
        else if(indexPath.item == 2 && !user.newsUpToDate)
            [cell.iconNew setImage:[UIImage imageNamed:@"icon-new" inBundle:_comBundle compatibleWithTraitCollection:nil] forState:UIControlStateNormal];
        else
            [cell.iconNew setImage:nil forState:UIControlStateNormal];
    }else
        [cell.iconNew setImage:nil forState:UIControlStateNormal];
    
    //MAIL Cell
    //apply image to backgroud view and set rounded top corners
    if(indexPath.item == self.viewModel.cellTitles.count-1){
        UIImage *image = [UIImage imageNamed:@"mail-nav-bar" inBundle:_comBundle compatibleWithTraitCollection:nil];
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, cell.frame.size.width, image.size.height)];
        imageView.image = image;
        
        [cell.contentView addSubview:imageView];
        [cell roundTopCornersForView:cell.contentView];  //background view gets rounded for image with clear color
        cell.titleLabel.textColor = [UIColor blackColor];
    }
}

#pragma mark - MFMailComposeViewController Delegate

-(void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error{
    BOOL success = NO;
    switch (result) {
        case MFMailComposeResultCancelled:
            DLogOrange(@"mail cancelled");
            break;
        case MFMailComposeResultSaved:
            DLogOrange(@"mail saved to drafts");
            break;
        case MFMailComposeResultSent:
            success = YES;
            break;
        case MFMailComposeResultFailed:
            [SVProgressHUD showInfoWithStatus:@"Failed to send email"];
            break;
        default:
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:^{
        if(success)[SVProgressHUD showSuccessWithStatus:@"Email Sent!"];
    }];
}

#pragma mark - UIViewControllerTransitioningDelegate

-(id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented
                                                                 presentingController:(UIViewController *)presenting
                                                                     sourceController:(UIViewController *)source{
    return [[CMQTransitionAnimationController alloc]init];
}

- (id<UIViewControllerAnimatedTransitioning>)navigationController:(UINavigationController *)navigationController
                                  animationControllerForOperation:(UINavigationControllerOperation)operation
                                               fromViewController:(UIViewController*)fromVC
                                                 toViewController:(UIViewController*)toVC{
    return [[CMQTransitionAnimationController alloc]init];
}

#pragma mark - UITextViewDelegate

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if ([text isEqualToString:@"\n"]){
        [textView resignFirstResponder];
        return NO;
    }
    return YES;
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    
    return YES;
}

- (BOOL)textViewShouldEndEditing:(UITextView *)textView {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    [self.view endEditing:YES];
    return YES;
}

- (void)keyboardWillShow:(NSNotification *)notification{
    NSDictionary *info  = notification.userInfo;
    NSValue      *value = info[UIKeyboardFrameEndUserInfoKey];
    
    CGRect rawFrame      = [value CGRectValue];
    CGRect keyboardFrame = [self.view convertRect:rawFrame fromView:nil];
    
    if (self.view.frame.size.height > 480)
        [self.wifiSupportView setFrame:CGRectMake(self.wifiSupportView.frame.origin.x,
                                                  self.view.frame.size.height - keyboardFrame.size.height - self.wifiSupportView.frame.size.height,
                                                  self.wifiSupportView.frame.size.width,
                                                  self.wifiSupportView.frame.size.height)];
    else
        [self.wifiSupportView setFrame:CGRectMake(self.wifiSupportView.frame.origin.x,
                                                  20,
                                                  self.wifiSupportView.frame.size.width,
                                                  self.wifiSupportView.frame.size.height)];
}

-(void)keyboardWillHide:(NSNotification *)notification{
    self.wifiSupportView.center = self.view.center;
}

@end
