//
//  CMQHomeCollectionViewCell.h
//  Communique
//
//  Created by Chris Hetem on 9/15/14.
//  Copyright (c) 2014 WaveRider, Inc. All rights reserved.
//

//@import UIKit;
#import <UIKit/UIKit.h>

@interface CMQHomeCollectionViewCell : UICollectionViewCell

/**
 *  the title of the cell
 */
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

/**
 *  icon that indicates new content
 */
@property (weak, nonatomic) IBOutlet UIButton *iconNew;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *iconNewTrailingSpaceConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *iconNewTopSpacingConstraint;

/**
 *  the icon indicating new content
 */
//@property (weak, nonatomic) IBOutlet UIImageView *iconNew;


/**
 *  rounds the corners of the passed in view. Uses a BezierPath and shape layer
 *
 *  @param view the view whose corners need rounding
 */
-(void)roundTopCornersForView:(UIView *)view;
@end
