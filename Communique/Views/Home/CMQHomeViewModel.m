//
//  CMQHomeViewModel.m
//  Communique
//
//  Created by Chris Hetem on 9/15/14.
//  Copyright (c) 2014 WaveRider, Inc. All rights reserved.
//

#import "CMQHomeViewModel.h"

@implementation CMQHomeViewModel

-(id)init{
    self = [super init];
	if (self) {
        if([CMQAD.apartment.firstTabTitle length] != 0 && [CMQAD.apartment.aptRepairURL length] != 0){
            if([CMQAD.apartment.secondTabTitle length] != 0 && [CMQAD.apartment.aptPaymentURL length] != 0){
                self.bgColors = @[[UIColor CMQRedColor], [UIColor CMQBlueColor], [UIColor CMQPurpleColor], [UIColor CMQYellowColor], [UIColor CMQGreenColor], [UIColor whiteColor]];
                self.cellTitles = @[@"Messages", @"Events", @"News", CMQAD.apartment.firstTabTitle, CMQAD.apartment.secondTabTitle, @"Community Contacts"];
                self.firstTabEnabled = self.secondTabEnabled = YES;
            }else{
                self.bgColors = @[[UIColor CMQRedColor], [UIColor CMQBlueColor], [UIColor CMQPurpleColor], [UIColor CMQYellowColor], [UIColor whiteColor]];
                self.cellTitles = @[@"Messages", @"Events", @"News", CMQAD.apartment.firstTabTitle, @"Community Contacts"];
                self.firstTabEnabled = YES;
                self.secondTabEnabled = NO;
            }
        }else if ([CMQAD.apartment.secondTabTitle length] != 0 && [CMQAD.apartment.aptPaymentURL length] != 0){
            self.bgColors = @[[UIColor CMQRedColor], [UIColor CMQBlueColor], [UIColor CMQPurpleColor], [UIColor CMQYellowColor], [UIColor whiteColor]];
            self.cellTitles = @[@"Messages", @"Events", @"News", CMQAD.apartment.secondTabTitle, @"Community Contacts"];
            self.firstTabEnabled = NO;
            self.secondTabEnabled = YES;
        }else{
            self.bgColors = @[[UIColor CMQRedColor], [UIColor CMQBlueColor], [UIColor CMQPurpleColor], [UIColor whiteColor]];
            self.cellTitles = @[@"Messages", @"Events", @"News", @"Community Contacts"];
            self.firstTabEnabled = self.secondTabEnabled = NO;
        }
    }
    return self;
}

@end


