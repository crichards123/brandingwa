//
//  CMQEventsViewModel.m
//  Communique
//
//  Created by Chris Hetem on 9/19/14.
//  Copyright (c) 2014 WaveRider, Inc. All rights reserved.
//

#import "CMQEventsViewModel.h"

@implementation CMQEventsViewModel

-(id)init{
    self = [super init];
    if(self){
        //do stuff
        self.eventsArray = [[NSArray alloc]init];
    }
    
    return self;
}

-(NSString *)formatDateForEventPosting:(NSDate *)date{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    
    //calendar to extract day and month compenents
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:(NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear) fromDate:date];
    
    NSInteger month = [components month]-1;
    NSInteger day = [components day];
    
    NSString *monthName = [[dateFormatter shortMonthSymbols]objectAtIndex:month];
    NSString *dateNumber = [NSString stringWithFormat:@"%ld", (long)day];
    
    NSString *formatStringForHours = [NSDateFormatter dateFormatFromTemplate:@"j" options:0 locale:[NSLocale currentLocale]];
    
    NSRange containsA = [formatStringForHours rangeOfString:@"a"];
    BOOL hasAMPM = containsA.location != NSNotFound;
    
    if (hasAMPM) { dateFormatter.dateFormat = @"h:mm a";}
    else { dateFormatter.dateFormat = @"HH:mm";}
    NSString *time = [dateFormatter stringFromDate:date];
    
    return [NSString stringWithFormat:@"%@. %@, %@", monthName, dateNumber, time];
}

-(NSString *)formatDateForExistingEvent:(NSDate *)date{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:(NSCalendarUnitWeekday | NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear) fromDate:date];
    NSDateComponents *today = [calendar components:(NSCalendarUnitWeekday | NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear) fromDate:[NSDate date]];
    
    NSString *string;
    if ([components year] == [today year] && [components month] == [today  month] && ([components day] == [today day] || [components day] == [today day] + 1)){
        string = ([components day] == [today day]) ? @"Today" : @"Tomorrow";
    }else{
        NSInteger month = [components month]-1;
        NSInteger dateNumber = [components day];
        NSInteger day = [components weekday]-1;
        
        NSString *monthName = [[dateFormatter shortMonthSymbols]objectAtIndex:month];
        NSString *dayName = [[dateFormatter weekdaySymbols]objectAtIndex:day];
        
        string = [NSString stringWithFormat:@"%@ %@. %ld", dayName, monthName, (long)dateNumber];
    }
    
    NSString *formatStringForHours = [NSDateFormatter dateFormatFromTemplate:@"j" options:0 locale:[NSLocale currentLocale]];
    NSRange containsA = [formatStringForHours rangeOfString:@"a"];
    BOOL hasAMPM = containsA.location != NSNotFound;
    if (hasAMPM) { dateFormatter.dateFormat = @"h:mm a";}
    else { dateFormatter.dateFormat = @"HH:mm";}
    NSString *time = [dateFormatter stringFromDate:date];
    
    return [NSString stringWithFormat:@"%@, %@", string, time];
}

-(NSString *)formatDateForNewEvent:(NSDate *)date{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"h:mm a"];
    
    //calendar to extract day and month compenents
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:(NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear | NSCalendarUnitWeekday) fromDate:date];
    
    NSInteger month = [components month]-1;
    NSInteger dateNumber = [components day];
    NSInteger day = [components weekday]-1;
    NSInteger year = [components year];
    
    NSString *monthName = [[dateFormatter shortMonthSymbols]objectAtIndex:month];
    NSString *dayName = [[dateFormatter weekdaySymbols]objectAtIndex:day];
    NSString *timeString = [dateFormatter stringFromDate:date];
    
    return [NSString stringWithFormat:@"%@, %@, %ld, %ld %@", dayName, monthName, (long)dateNumber, (long)year, timeString];
}

-(UIImage *)resizeImage:(UIImage *)image{
    CGSize originalSize = image.size;
    CGSize smallerSize;
    
    if(originalSize.height > originalSize.width){
        smallerSize = CGSizeMake(640.0, 960.0);
    }else{
        smallerSize = CGSizeMake(960.0, 640.0);
    }
    
    // Resize image
    UIGraphicsBeginImageContext(smallerSize);
    [image drawInRect: CGRectMake(0, 0, smallerSize.width, smallerSize.height)];
    UIImage *smallImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return smallImage;
}


@end
