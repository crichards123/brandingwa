//
//  CMQEventsViewController.m
//  Communique
//
//  Created by Chris Hetem on 9/19/14.
//  Copyright (c) 2014 WaveRider, Inc. All rights reserved.
//

#import "CMQEventsViewController.h"
#import "CMQEventCell.h"
#import "CMQEventsViewModel.h"
#import "CMQCreateEventView.h"
#import "CMQEvent.h"
#import "CMQEventAttendeeTableViewCell.h"
#import "UIColor+CMQColors.h"
#import "NSString+NSHash.h"
#import <EventKit/EventKit.h>

#define CREATEEVENTVIEW_NIBNAME     @"CMQCreateEventView"

#define ANIMATION_DURATION          0.25

#define DEFAULT_CONTAINER_SPACE     65
#define MAX_CONTAINER_SPACE         505

#define CREATE_EVENT_TOP_DEFAULT    75
#define CREATE_EVENT_TOP_MAX        -50

#define DEFAULT_CELL_HEIGHT         138

static NSString *cellIdentifer = @"CellIdentifier";

static NSString *attendeeCellIdentifier = @"attendeeCellIdentifier";

static NSString *eventContentPlaceholder = @"Event description";

@interface CMQEventsViewController () <CMQAPIClientDelegate, UITextFieldDelegate, UITextViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITableViewDelegate, UITableViewDataSource, UICollectionViewDataSource, UICollectionViewDelegate>

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *eventContainerYSpaceConstraint;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) CMQEventsViewModel *viewModel;
@property (strong, nonatomic) UIRefreshControl *refreshControl;
@property (strong, nonatomic) CMQCreateEventView *createEventView;
@property (weak, nonatomic) IBOutlet UIView *alertContainerView;
@property (weak, nonatomic) IBOutlet UILabel *alertMessage;

//IBOutlets
@property (weak, nonatomic) IBOutlet UIButton *createEventButton;
@property (weak, nonatomic) IBOutlet UIButton *backButton;

@end

@implementation CMQEventsViewController{
    CMQEvent *newEvent;
    NSMutableArray *attendeeArray;
}

#pragma mark - View Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
    
    self.screenName = @"Events";
    
    [self configure];   //configure the view
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [SVProgressHUD dismiss];
}

#pragma mark - Config
/**
 *  confiures the view on inital loading of self
 */
-(void)configure{
    [self.view setBackgroundColor:[UIColor CMQBlueColor]];
    
    self.viewModel = [[CMQEventsViewModel alloc]init];
    
    //back button
    [self.backButton.titleLabel setFont:[UIFont fontWithName:kCMQFontGeometriaLight size:19]];
    [self.backButton setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    
    //alert
    [self.alertContainerView setHidden:YES];
    [self.alertMessage setFont:[UIFont fontWithName:kCMQFontGeometriaLight size:15]];
    
    //check if user is Admin or not. if not, hide our createEventButton
    if([[CMQUser currentUser].userRole isEqualToString:kPermissionAdmin] ||
       [[CMQUser currentUser].userRole isEqualToString:kPermissionRegionalAdmin] ||
       [[CMQUser currentUser].userRole isEqualToString:kPermissionCompanyAdmin]){
        self.createEventButton.hidden = NO;
    }else
        self.createEventButton.hidden = YES;
    
    self->dateTimeSelectionMade = FALSE;
    
    //refresh control
    self.refreshControl = [[UIRefreshControl alloc]init];
    [self.collectionView addSubview:self.refreshControl];
    [self.refreshControl addTarget:self action:@selector(onRefresh) forControlEvents:UIControlEventValueChanged];
    
    //HUD
    [SVProgressHUD showWithStatus:@"Fetching events"];
    
    //fetch all events - returned to delegate methods
    [[CMQAPIClient sharedClient]setDelegate:self];
    [[CMQAPIClient sharedClient]fetchEvents];
    
    //collectionView
    [self.collectionView setBackgroundColor:[UIColor clearColor]];
    NSBundle* comBundle = [NSBundle bundleForClass:[CMQEventCell class]];
    [self.collectionView registerNib:[UINib nibWithNibName:@"CMQEventCell" bundle:comBundle] forCellWithReuseIdentifier:cellIdentifer];
    
    //tableview
    // Register our custom cells with our table views
    comBundle = [NSBundle bundleForClass:[CMQEventAttendeeTableViewCell class]];
    [self.tableView registerNib:[UINib nibWithNibName:@"CMQEventAttendeeTableViewCell" bundle:comBundle] forCellReuseIdentifier:attendeeCellIdentifier];
    self.tableView.scrollIndicatorInsets = UIEdgeInsetsZero;
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.tableView.backgroundColor = [UIColor clearColor];
    
    [self addCreateEventView];  //adds a view for creating an event behind the stack of events
}

#pragma mark - Internal
/**
 *  checks if all fields required to create an event have been filled
 *
 *  @return a bool whether or not the fields have been filled
 */
-(BOOL)validateNewEventValues{
    BOOL valid = YES;
    
    //check if all fields have been filled
    if([self.createEventView.eventTitleTextField.text isEqualToString:@""]) valid = NO;
    if([self.createEventView.eventContentTextView.text isEqualToString:@""] ||
       [self.createEventView.eventContentTextView.text isEqualToString:eventContentPlaceholder]) valid = NO;
    if(self.viewModel.createdEventDate == nil) valid = NO;
    
    return valid;
}

#pragma mark - CMQEvent/View Handling

/**
 *  resets the stack of events to as it was on initial load (except for index). this slides all views back into their original position
 */
-(void)resetEventStack{
    [self.view endEditing:YES];
    self.viewModel.isCreatingEvent = NO;  //toggle our bool value
    [self slideDownNewEventForKeyboard];
    [self resetCMQCreateEventView];
    [self rotateView:self.createEventButton radians:0];     //rotate the create button to become a +
    [self slideUpEvents];
}

#pragma mark - CMQCreateEventView Handling

/**
 *  creates and adds an instance of CMQCreateEventView loaded from a nib to our view
 */
-(void)addCreateEventView{
    NSBundle* comBundle = [NSBundle bundleForClass:[CMQCreateEventView class]];
    //create an instance of CMQEventView and add it to our view (self.view)
    CMQCreateEventView *createEventView = [[comBundle loadNibNamed:CREATEEVENTVIEW_NIBNAME owner:self options:nil] objectAtIndex:0];
    [self.view addSubview:createEventView];
    [self.view sendSubviewToBack:createEventView];
    createEventView.hidden = YES;
    
    //mas_make constraints are equal to CGRectMake(x, 75, 294, 414) + a centered x-value
    [createEventView mas_makeConstraints:^(MASConstraintMaker *make){
        make.top.equalTo(@75);
        make.width.equalTo(@([UIScreen mainScreen].bounds.size.width - 20));
        make.height.equalTo(@(414));
        make.centerX.equalTo(self.view);
    }];
    
    self.createEventView = createEventView; //assign this view to our property
    
    [self configureCMQCreateEventView:createEventView];
}

/**
 *  configures an instance of our CMQCreateEventView with all necesssary button actions and such
 *
 *  @param createEventView the view to configure
 */
-(void)configureCMQCreateEventView:(CMQCreateEventView *)createEventView{
    [createEventView.selectDateTimeButton addTarget:self action:@selector(onSelectDateTime:) forControlEvents:UIControlEventTouchUpInside];
    [createEventView.selectPhotoButton addTarget:self action:@selector(onSelectEventPhoto:) forControlEvents:UIControlEventTouchUpInside];
    [createEventView.eventLocationTextField setDelegate:self];
    [createEventView.eventTitleTextField setDelegate:self];
    [createEventView.eventContentTextView setDelegate:self];
    [createEventView.cancelButton addTarget:self action:@selector(onCancelCreatedEvent:) forControlEvents:UIControlEventTouchUpInside];
    [createEventView.postButton addTarget:self action:@selector(onPostCreatedEvent:) forControlEvents:UIControlEventTouchUpInside];
    [createEventView.eventDatePicker addTarget:self action:@selector(datePickerValueChanged:) forControlEvents:UIControlEventValueChanged];
    
    [self resetCMQCreateEventView];
}

/**
 *  resets our instance of CMQCreateEventView back to its original state. this is called when a user cancelled creating a new event
 */
-(void)resetCMQCreateEventView{
    NSBundle* comBundle = [NSBundle bundleForClass:[CMQCreateEventView class]];
    [self.createEventView.selectPhotoButton setImage:[UIImage imageNamed:@"events_select_photo" inBundle:comBundle compatibleWithTraitCollection:nil] forState:UIControlStateNormal];
    self.createEventView.eventImageView.image = nil;
    [self.createEventView.eventContentTextView setText:@"Event description"];
    [self.createEventView.eventTitleTextField setPlaceholder:@"Name of event"];
    [self.createEventView.eventTitleTextField setText:@""];
    [self.createEventView.eventLocationTextField setPlaceholder:@"Location of event"];
    [self.createEventView.eventLocationTextField setText:@""];
    [self.createEventView.selectDateTimeButton setTitle:@"Select event date and time" forState:UIControlStateNormal];
    self.createEventView.eventDatePicker.hidden = YES;
    self.createEventView.selectPhotoButton.hidden = NO;
    self.createEventView.eventImageView.hidden = NO;
    self.viewModel.createdEventDate = nil;
}

#pragma mark - CMQAPIClient Delegate

- (void)CMQAPIClientDidFetchEventsSuccessfully:(NSArray *)events{
    self.viewModel.eventsArray = events;
    [self.refreshControl endRefreshing];
    [self.collectionView reloadData];

    [SVProgressHUD dismiss];
    
    CMQUser *user = [CMQUser currentUser];
    user.eventsUpToDate = true;
    [user saveInBackground];
    
    [self.alertContainerView setHidden:[self.viewModel.eventsArray count] > 0];
}

- (void)CMQAPIClientDidFetchEventAttendeesSuccessfully:(NSArray *)attendees{
    attendeeArray = [[NSMutableArray alloc] initWithArray:attendees];
    
    for (int i = 0; i < attendeeArray.count; i ++){
        CMQUser *user = attendeeArray[i];
        if ([user.objectId isEqualToString:[CMQUser currentUser].objectId]){
            [attendeeArray removeObjectAtIndex:i];
            [attendeeArray insertObject:[CMQUser currentUser] atIndex:0];
            break;
        }
    }
    
    [self.tableView reloadData];
    self.tableView.hidden = NO;
    self.collectionView.hidden = YES;
    [self.backButton setTitle:@"< Back" forState:UIControlStateNormal];
    self.createEventButton.hidden = YES;
}

-(void)CMQAPIClientDidFailFetchWithMessage:(NSString *)message isCache:(BOOL)isCache{
    [SVProgressHUD showInfoWithStatus:@"Failed to load events"];
}

-(void)CMQAPIClientDidPostObjectSuccessfully:(PFObject *)object{
    [SVProgressHUD showSuccessWithStatus:@"Event posted!"];
    [self resetEventStack];
    
    [[CMQAPIClient sharedClient]fetchEvents];
    
    CMQUser *user = [CMQUser currentUser];
    user.eventsUpToDate = true;
    [user saveInBackground];
}

-(void)CMQAPIClientDidFailToPostObject:(PFObject *)object withMessage:(NSString *)message{
    [SVProgressHUD showInfoWithStatus:@"Failed to post event, sorry"];
    DLogRed(@"error posting event: %@", message);
    [self.alertContainerView setHidden:[self.viewModel.eventsArray count] > 0];
}

#pragma mark - Button Actions

//main view
- (IBAction)onCreateEvent:(id)sender {
    if(self.viewModel.isCreatingEvent){
        [self resetEventStack];
        [self.alertContainerView setHidden:[self.viewModel.eventsArray count] > 0];
    }else{
        self.viewModel.isCreatingEvent = YES; //toggle our bool value
        [self rotateView:self.createEventButton radians:M_PI_4];    //rotates the create button to become an X
        [self slideDownEvents];     //slides down the stack of events
        [self.alertContainerView setHidden:YES];
    }
}

- (IBAction)onGoBack:(id)sender {
    if ([self.tableView isHidden])
        [self.navigationController popViewControllerAnimated:YES];
    else{
        self.tableView.hidden = YES;
        self.collectionView.hidden = NO;
        [self.backButton setTitle:@"< Events" forState:UIControlStateNormal];
        
        //check if user is Admin or not. if not, hide our createEventButton
        if([[CMQUser currentUser].userRole isEqualToString:kPermissionAdmin] ||
           [[CMQUser currentUser].userRole isEqualToString:kPermissionRegionalAdmin] ||
           [[CMQUser currentUser].userRole isEqualToString:kPermissionCompanyAdmin]){
            self.createEventButton.hidden = NO;
        }
    }
}

//CMQEventView//
-(IBAction)onViewCalendar:(UIButton *)button{
    CMQEvent *event = [self.viewModel.eventsArray objectAtIndex:button.tag];
    
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"calshow://"]]){
        NSInteger interval = [event.eventDate timeIntervalSinceReferenceDate];
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"calshow:%ld", (long)interval]];
        [[UIApplication sharedApplication] openURL:url];
    }else
        return;
}

-(IBAction)onAttendEvent:(UIButton *)button{
    //get the event being viewed
    CMQEvent *event = [self.viewModel.eventsArray objectAtIndex:button.tag];
    
    int attendingCount = (int)event.eventAttendees.count;
    
    BOOL isAttending;
    if([button isSelected]){
        isAttending = NO;
        //unattend the event
        [event removeObject:[CMQUser currentUser].objectId forKey:kParseEventAttendeesKey];
        attendingCount -= 1;
    }else{  //attend the event
        isAttending = YES;
        [event addUniqueObject:[CMQUser currentUser].objectId forKey:kParseEventAttendeesKey];
        attendingCount += 1;
        
        EKAuthorizationStatus authorizationStatusEvent = [EKEventStore authorizationStatusForEntityType:EKEntityTypeEvent];
        
        if (authorizationStatusEvent == EKAuthorizationStatusNotDetermined) {
            [[[EKEventStore alloc] init] requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError *error) {
                if (granted) // Access granted
                    [self addEventToCalendar:event];
            }];
        } else {
            BOOL granted = (authorizationStatusEvent == EKAuthorizationStatusAuthorized);
            if (granted) // Access granted
                [self addEventToCalendar:event];
        }
    }
    
    CMQEventCell *cell = (CMQEventCell*)[self.collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:button.tag inSection:0]];
    [cell.eventAttendeesButton setTitle:[self getEventAttendeeString:attendingCount isAttending:isAttending] forState:UIControlStateNormal];
    
    //save the event. since it's not essential we know right away, this will save the event as soon as it can
    [event saveEventually];
    
    [button setSelected:![button isSelected]];  //toggle selected state of button
}

-(NSString *)getEventAttendeeString:(int)numAttending isAttending:(BOOL)attending {
    NSString *attendeeString;
    if (numAttending > 1)
        attendeeString = [NSString stringWithFormat:@"%d attending", numAttending];
    else if (numAttending == 1)
        attendeeString = (attending) ? @"You are attending" : @"1 attending";
    else
        attendeeString = @"0 attending";
    
    return attendeeString;
}

-(IBAction)onViewAttendees:(UILabel *)label{
    //get the event being viewed
    CMQEvent *event = [self.viewModel.eventsArray objectAtIndex:label.tag];
    
    if ([event.eventAttendees count] > 0 && !([[CMQUser currentUser].userRole isEqualToString:kPermissionInterestedParty] && event.isPrivate == YES))
        [[CMQAPIClient sharedClient] fetchEventAttendees:event];
    else
        return;
}

/**
 * Received CMQEvent and adds it to iOS calendar
 */
-(void)addEventToCalendar:(CMQEvent *)event{
    EKEventStore *store = [[EKEventStore alloc] init];
    
    EKEvent *calendarEvent = [EKEvent eventWithEventStore:store];
    calendarEvent.title = event.eventTitle;
    calendarEvent.notes = event.eventContent;
    calendarEvent.startDate = event.eventDate;
    calendarEvent.endDate = (event.eventEndDate) ? event.eventEndDate : [event.eventDate dateByAddingTimeInterval:(60*60)];
    
    // set alarm to 10 mins prior of the event
    [calendarEvent addAlarm:[EKAlarm alarmWithRelativeOffset:60*-10]];
    
    calendarEvent.calendar = [store defaultCalendarForNewEvents];
    
    NSError *err = nil;
    [store saveEvent:calendarEvent span:EKSpanThisEvent commit:YES error:&err];
}

//CMQCreateEventView//

-(void)onSelectDateTime:(id)sender{
    if(self.createEventView.eventDatePicker.hidden){
        self.createEventView.eventDatePicker.hidden = NO;
        self.createEventView.selectPhotoButton.hidden = YES;
        [self.createEventView.selectDateTimeButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        self.createEventView.eventImageView.hidden = YES;
        self->dateTimeSelectionMade = FALSE;
    }else{
        self.createEventView.eventDatePicker.hidden = YES;
        self.createEventView.confirmButton.hidden = YES;
        self.createEventView.selectPhotoButton.hidden = NO;
        self.createEventView.eventImageView.hidden = NO;
        if(self->dateTimeSelectionMade){
            [self.createEventView.selectDateTimeButton setTitleColor:[UIColor colorWithRed:0.533 green:0.745 blue:0.608 alpha:1] forState:UIControlStateNormal];
        }
    }
}

-(void)onSelectEventPhoto:(id)sender{
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc]init];
    imagePicker.delegate = self;
    imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    imagePicker.modalPresentationStyle = UIModalPresentationPopover;
    imagePicker.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    
    UIPopoverPresentationController *popover = imagePicker.popoverPresentationController;
    popover.sourceView = self.view;
    popover.sourceRect = CGRectMake(CGRectGetMidX(self.view.bounds),
                                    self.createEventView.eventImageView.frame.origin.y + self.createEventView.eventImageView.frame.size.height,
                                    0,
                                    0);
    
    [self presentViewController:imagePicker animated:YES completion:nil];
}

-(void)onCancelCreatedEvent:(id)sender{
    [self resetEventStack];
    [self.alertContainerView setHidden:[self.viewModel.eventsArray count] > 0];
}

-(void)onPostCreatedEvent:(id)sender{
    BOOL valid = [self validateNewEventValues];
    if(valid){
        newEvent = [[CMQEvent alloc]init];
        newEvent.eventDate = self.viewModel.createdEventDate;
        newEvent.utcOffset = [[NSTimeZone systemTimeZone] secondsFromGMTForDate:self.viewModel.createdEventDate]/60;
        newEvent.eventTitle = self.createEventView.eventTitleTextField.text;
        newEvent.eventContent = self.createEventView.eventContentTextView.text;
        newEvent.userWhoPosted = [CMQUser currentUser];
        
        if (![self.createEventView.eventLocationTextField.text isEqualToString:@""])
            newEvent.eventLocation = self.createEventView.eventLocationTextField.text;
        
        if ([CMQUser currentUser].tempAptComplexID && [CMQUser currentUser].tempAptComplexID.length > 0)
            newEvent.aptComplexID = [CMQUser currentUser].tempAptComplexID;
        else
            newEvent.aptComplexID = [CMQUser currentUser].aptComplexID;
        
        if(self.createEventView.eventImageView.image){
            NSString *imageName = [NSString stringWithFormat:@"%f", [[NSDate date] timeIntervalSince1970]];
            imageName = [[imageName MD5] stringByAppendingPathExtension:@"png"];
        
            NSData *imageData = UIImagePNGRepresentation(self.createEventView.eventImageView.image);
            PFFile *imageFile = [PFFile fileWithName:imageName data:imageData];
            newEvent.eventPhoto = imageFile;
        }
        
        [SVProgressHUD showWithStatus:@"Creating event"];
        
        [[CMQAPIClient sharedClient]postEvent:newEvent];   //API call to post event to Parse
               
        self.viewModel.createdEventDate = nil;
    }else
        [SVProgressHUD showInfoWithStatus:@"All fields must be must be filled"];
}

-(void)datePickerValueChanged:(UIDatePicker *)datePicker{
    NSDate *date = self.createEventView.eventDatePicker.date;
    self.viewModel.createdEventDate = date;
    [self.createEventView.selectDateTimeButton setTitle:[self.viewModel formatDateForNewEvent:date] forState:UIControlStateNormal];
    self->dateTimeSelectionMade = TRUE;
    self.createEventView.confirmButton.hidden = NO;
}

#pragma mark - UIView animations

/**
 *  rotates a view by a number of radians
 *
 *  @param view     the view to rotate
 *  @param rotation the amount of radians to rotate by
 */
-(void)rotateView:(UIView *)view radians:(CGFloat)rotation{
    [UIView animateWithDuration:ANIMATION_DURATION animations:^{
        view.transform = CGAffineTransformMakeRotation(rotation);
    }];
}

/**
 *  slides down the eventsContainerView to reveal the Create Event view beneath
 */
-(void)slideDownEvents{
    [self fadeInView:self.createEventView];
    
    self.eventContainerYSpaceConstraint.constant = MAX_CONTAINER_SPACE;
    
    [UIView animateWithDuration:ANIMATION_DURATION animations:^{[self.view layoutIfNeeded];}];
}

/**
 *  slides up the eventsContainerView to hide the Create Event view
 */
-(void)slideUpEvents{
    [self fadeOutView:self.createEventView];

    self.eventContainerYSpaceConstraint.constant = DEFAULT_CONTAINER_SPACE;
    
    [UIView animateWithDuration:ANIMATION_DURATION animations:^{
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        self.createEventView.hidden = YES;
    }];
}

/**
 *  slides up our CMQCreateEventView when the keyboard is called up
 */
-(void)slideUpNewEventForKeyboard{
    [self.createEventView mas_updateConstraints:^(MASConstraintMaker *make){make.top.equalTo(@(CREATE_EVENT_TOP_MAX));}];
    
    [UIView animateWithDuration:ANIMATION_DURATION animations:^{[self.view layoutIfNeeded];}];
}

/**
 *  slides down our CMQCreateEventView when the keyboard is called up
 */
-(void)slideDownNewEventForKeyboard{
    [self.createEventView mas_updateConstraints:^(MASConstraintMaker *make){make.top.equalTo(@(CREATE_EVENT_TOP_DEFAULT));}];
    
    [UIView animateWithDuration:ANIMATION_DURATION animations:^{[self.view layoutIfNeeded];}];
}

/**
 *  fades in a view and unhides it
 *
 *  @param view the view to fade in
 */
-(void)fadeInView:(UIView *)view{
    view.alpha = 0.0;
    view.hidden = NO;
    
    [UIView animateWithDuration:ANIMATION_DURATION animations:^{view.alpha = 1.0;}];
}

/**
 *  fades out a view and hides it on completion
 *
 *  @param view the view to fade out
 */
-(void)fadeOutView:(UIView *)view{
    [UIView animateWithDuration:ANIMATION_DURATION animations:^{
        view.alpha = 0.0;
    } completion:^(BOOL finished) {
         view.hidden = YES;
    }];
}

#pragma mark - UIImagePickerController Delegate

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    [self.createEventView.selectPhotoButton setImage:nil forState:UIControlStateNormal];   //just clear the image, don't hide the button. that way the user can still select a new image
    UIImage *selectedImage = info[UIImagePickerControllerOriginalImage];    //get selected image
    selectedImage = [self.viewModel resizeImage:selectedImage];
    self.createEventView.eventImageView.image = selectedImage;
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - UITextField Delegate

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    [self slideUpNewEventForKeyboard];
}


-(void)textFieldDidEndEditing:(UITextField *)textField{
    [self slideDownNewEventForKeyboard];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    if ([self.createEventView.eventLocationTextField isFirstResponder])
        [self.createEventView.eventTitleTextField becomeFirstResponder];
    else
        [self.createEventView.eventContentTextView becomeFirstResponder];
    
    return NO;
}

#pragma mark - UITextView Delegate

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    if ([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    return YES;
}

-(void)textViewDidBeginEditing:(UITextView *)textView{
    [self slideUpNewEventForKeyboard];
    if([textView.text isEqualToString:eventContentPlaceholder])textView.text = @"";
    [textView setTextColor:[UIColor blackColor]];
}

-(void)textViewDidEndEditing:(UITextView *)textView{
    if([textView.text isEqualToString:@""]){
        [textView setTextColor:[UIColor lightGrayColor]];
        textView.text = eventContentPlaceholder;
    }
    
    [self slideDownNewEventForKeyboard];
}

#pragma mark - UITableView Delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    // Return the number of rows in the section.
    return [attendeeArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    // Configure the cell...
    CMQEventAttendeeTableViewCell *cell;
    
    cell = (CMQEventAttendeeTableViewCell *)[tableView dequeueReusableCellWithIdentifier:attendeeCellIdentifier];
    
    CMQUser *user = (CMQUser *) [attendeeArray objectAtIndex:indexPath.row];
    
    if ([user.objectId isEqualToString:[CMQUser currentUser].objectId])
        cell.userFullNameLabel.text = @"You";
    else if([[CMQUser currentUser].userRole isEqualToString:kPermissionAdmin] ||
       [[CMQUser currentUser].userRole isEqualToString:kPermissionRegionalAdmin] ||
       [[CMQUser currentUser].userRole isEqualToString:kPermissionCompanyAdmin])
        cell.userFullNameLabel.text = user.fullName;
    else
        cell.userFullNameLabel.text = user.firstName;
    
    if (user.userPhoto){
        //fetch our image data on a background thread
        dispatch_queue_t queue = dispatch_get_global_queue (DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
        dispatch_async(queue, ^{
            UIImage *userPhoto = [UIImage getImageFromPFFile:user.userPhoto];
            //display our image on the main thread
            dispatch_async(dispatch_get_main_queue(), ^{
                cell.userPhoto.contentMode = UIViewContentModeScaleAspectFill;
                cell.userPhoto.image = userPhoto;
            });
        });
    }else{
        NSBundle* comBundle = [NSBundle bundleForClass:[CMQEventsViewController class]];
        cell.userPhoto.contentMode = UIViewContentModeTop;
        cell.userPhoto.image = [UIImage imageNamed:@"user-icon" inBundle:comBundle compatibleWithTraitCollection:nil];
    }

    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    return @"";
}

#pragma mark - UICollectionView Datasource

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    CMQEventCell *cell = (CMQEventCell *)[self.collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifer forIndexPath:indexPath];
    
    [self configureCell:cell forRow:indexPath.row]; //call to configure our row
    
    return cell;
}

/**
 *  configures a given cell at a given row with formatting and such
 *
 *  @param cell the cell to configure
 *  @param row  the row for the cell to configure
 */
-(void)configureCell:(CMQEventCell *)cell forRow:(NSInteger)row{
    CMQEvent *event = [self.viewModel.eventsArray objectAtIndex:row];
    
    CMQUser *userWhoPosted = (CMQUser *) event.userWhoPosted.fetchIfNeeded;
    
    if([[CMQUser currentUser].userRole isEqualToString:kPermissionAdmin] ||
       [[CMQUser currentUser].userRole isEqualToString:kPermissionRegionalAdmin] ||
       [[CMQUser currentUser].userRole isEqualToString:kPermissionCompanyAdmin])
        cell.nameLabel.text = (userWhoPosted.fullName) ? userWhoPosted.fullName : @"Staff Member";
    else
        cell.nameLabel.text = (userWhoPosted.firstName) ? userWhoPosted.firstName : @"Staff Member";
    
    cell.eventTitleLabel.text = event.eventTitle;
    cell.eventContentLabel.text = event.eventContent;
    cell.eventDateLabel.text = [NSString stringWithFormat:@"posted %@", [self.viewModel formatDateForEventPosting:event.createdAt]];
    cell.eventLocationLabel.text = (event.eventLocation && event.eventLocation.length > 0) ? [NSString stringWithFormat:@"%@", event.eventLocation] : @"n/a";
    
    //date needs formatting
    NSMutableString *dateString = [NSMutableString stringWithFormat:@"%@", [self.viewModel formatDateForExistingEvent:event.eventDate]];
    [cell.eventDateStartButton setTitle:dateString forState:UIControlStateNormal];
    cell.eventDateStartButton.tag = row;
    [cell.eventDateStartButton addTarget:self action:@selector(onViewCalendar:) forControlEvents:UIControlEventTouchUpInside];
    
    //attending stringneeds formatting
    BOOL isAttending = (event.eventAttendees && [event.eventAttendees indexOfObject:[CMQUser currentUser].objectId] != NSNotFound) ? YES : NO;
    [cell.eventAttendeesButton setTitle:[self getEventAttendeeString:(int)event.eventAttendees.count isAttending:isAttending] forState:UIControlStateNormal];
    [cell.eventAttendButton setSelected:isAttending];
    
    //attend button
    if (!([[CMQUser currentUser].userRole isEqualToString:kPermissionInterestedParty] && event.isPrivate == YES)){
        cell.eventAttendButton.tag = row;
        [cell.eventAttendButton addTarget:self action:@selector(onAttendEvent:) forControlEvents:UIControlEventTouchUpInside];
    }else
        cell.eventAttendButton.hidden = YES;
    cell.eventAttendeesButton.tag = row;
    [cell.eventAttendeesButton addTarget:self action:@selector(onViewAttendees:) forControlEvents:UIControlEventTouchUpInside];
    
    //userWhoPosted.userPhoto
    if (userWhoPosted.userPhoto){
        //fetch our image data on a background thread
        dispatch_queue_t queue = dispatch_get_global_queue (DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
        dispatch_async(queue, ^{
            UIImage *userPhoto = [UIImage getImageFromPFFile:userWhoPosted.userPhoto];
            //display our image on the main thread
            dispatch_async(dispatch_get_main_queue(), ^{
                cell.userPhoto.contentMode = UIViewContentModeScaleAspectFill;
                cell.userPhoto.image = userPhoto;
            });
        });
    }else{
        NSBundle* comBundle = [NSBundle bundleForClass:[CMQEventsViewController class]];
        cell.userPhoto.contentMode = UIViewContentModeTop;
        cell.userPhoto.image = [UIImage imageNamed:@"user-icon" inBundle:comBundle compatibleWithTraitCollection:nil];
    }

    //load image if there is one
    cell.eventPhotoImageView.image = nil;
    cell.eventPhotoImageView.alpha = 0.0;
    if(event.eventPhoto){
        cell.imageLoadingLabel.hidden = NO;
        
        //fetch our image data on a background thread
        dispatch_queue_t queue = dispatch_get_global_queue (DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
        dispatch_async(queue, ^{
            UIImage *newsImage = [UIImage getImageFromPFFile:event.eventPhoto];
            //display our image on the main thread
            dispatch_async(dispatch_get_main_queue(), ^{
                cell.eventPhotoImageView.hidden = NO;
                cell.imageLoadingLabel.hidden = YES;
                cell.imageViewHeightConstraint.constant = [UIImage getHeightForImageContainerWithWidth:cell.eventPhotoImageView.frame.size.width imageWidth:newsImage.size.width imageHeight:newsImage.size.height];
                
                cell.eventPhotoImageView.image = newsImage;
                [UIView animateWithDuration:0.75 animations:^{
                    cell.eventPhotoImageView.alpha = 1.0;
                }];
            });
        });
    }else{
        cell.imageLoadingLabel.hidden = YES;
        cell.eventPhotoImageView.hidden = YES;
        cell.imageViewHeightConstraint.constant = 0;
        cell.eventPhotoImageView.hidden = YES;
    }
    
    cell.backgroundColor = [UIColor whiteColor];
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.viewModel.eventsArray.count;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = DEFAULT_CELL_HEIGHT;
    
    CMQEvent *event = [self.viewModel.eventsArray objectAtIndex:indexPath.row];
    
    cellHeight += [event.eventTitle boundingRectWithSize:CGSizeMake(self.collectionView.frame.size.width - 32, CGFLOAT_MAX) options:(NSStringDrawingUsesFontLeading) | (NSStringDrawingUsesLineFragmentOrigin) attributes:@{NSFontAttributeName : [UIFont fontWithName:kCMQFontGeometriaBold size:14.5]} context:nil].size.height;
    
    cellHeight += [event.eventContent boundingRectWithSize:CGSizeMake(self.collectionView.frame.size.width - 32, CGFLOAT_MAX) options:(NSStringDrawingUsesFontLeading) | (NSStringDrawingUsesLineFragmentOrigin) attributes:@{NSFontAttributeName : [UIFont fontWithName:kCMQFontGeometriaLight size:14.5]} context:nil].size.height;
    
    if(event.eventPhoto){
        UIImage *eventImage = [UIImage getImageFromPFFile:event.eventPhoto];
        cellHeight += [UIImage getHeightForImageContainerWithWidth:self.collectionView.frame.size.width - 20 imageWidth:eventImage.size.width imageHeight:eventImage.size.height];
    }
    
    if(event.eventLocation && event.eventLocation.length > 0) cellHeight += 14;
    
    return CGSizeMake(self.collectionView.frame.size.width - 20, cellHeight);
}

#pragma mark - UICollectionView Delegate

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
}

#pragma Refresh Control

/**
 *  refreshes the events
 */
-(void)onRefresh{
    [SVProgressHUD showWithStatus:@"Refreshing events"];
    [[CMQAPIClient sharedClient]fetchEvents];
}

@end
