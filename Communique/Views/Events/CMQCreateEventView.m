//
//  CMQCreateEventView.m
//  Communique
//
//  Created by Chris Hetem on 9/24/14.
//  Copyright (c) 2014 WaveRider, Inc. All rights reserved.
//

#import "CMQCreateEventView.h"

@implementation CMQCreateEventView

-(void)awakeFromNib{
    [super awakeFromNib];
    
    [self configureUI];
}

/**
 *  configures the UI initially (button states, fonts, etc)
 */
-(void)configureUI{
    //this is just a clear view that sits to the let of the text field. it's used for 'indenting' since there's no easy way to do it otherwise (i.e. no contentInset or anything of that sort)
    UIView *spacerView1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 10)];
    UIView *spacerView2 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 10)];
    
    [self.eventLocationTextField setLeftViewMode:UITextFieldViewModeAlways];
    [self.eventLocationTextField setLeftView:spacerView1];
    
    [self.eventTitleTextField setLeftViewMode:UITextFieldViewModeAlways];
    [self.eventTitleTextField setLeftView:spacerView2];
    
    //fonts
    [self.selectDateTimeButton.titleLabel setFont:[UIFont fontWithName:kCMQFontGeometriaLight size:14]];
    [self.eventLocationTextField setFont:[UIFont fontWithName:kCMQFontGeometriaLight size:17]];
    [self.eventTitleTextField setFont:[UIFont fontWithName:kCMQFontGeometriaLight size:17]];
    [self.eventContentTextView setFont:[UIFont fontWithName:kCMQFontGeometriaLight size:14]];
    
    //button states
    NSBundle* comBundle = [NSBundle bundleForClass:[CMQCreateEventView class]];
    [self.cancelButton setImage:[UIImage imageNamed:@"btn-cancel" inBundle:comBundle compatibleWithTraitCollection:nil] forState:UIControlStateNormal];
    [self.cancelButton setImage:[UIImage imageNamed:@"btn-cancel-pressed" inBundle:comBundle compatibleWithTraitCollection:nil] forState:UIControlStateHighlighted];
    [self.postButton setImage:[UIImage imageNamed:@"btn-post-event" inBundle:comBundle compatibleWithTraitCollection:nil] forState:UIControlStateNormal];
    [self.postButton setImage:[UIImage imageNamed:@"btn-post-event-pressed" inBundle:comBundle compatibleWithTraitCollection:nil] forState:UIControlStateHighlighted];
    
    [self.eventDatePicker setMinimumDate:[NSDate date]];
}

@end
