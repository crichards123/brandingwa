//
//  CMQEventAttendeeTableViewCell.m
//  Communique
//
//  Created by Colby Melvin on 6/22/16.
//  Copyright © 2016 Communique, LLC. All rights reserved.
//

#import "CMQEventAttendeeTableViewCell.h"

@implementation CMQEventAttendeeTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self configureUI];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

/**
 *  initially configures the UI
 */
-(void)configureUI{
    [self.userFullNameLabel setVerticalAlignment:TTTAttributedLabelVerticalAlignmentCenter];
    [self.userFullNameLabel setFont:[UIFont fontWithName:kCMQFontGeometriaLight size:17]];
    
    self.userPhoto.layer.cornerRadius = 20.0f;
    self.userPhoto.layer.borderWidth = .5f;
    self.userPhoto.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.userPhoto.layer.masksToBounds = YES;
}

@end
