//
//  CMQEventAttendeeTableViewCell.h
//  Communique
//
//  Created by Colby Melvin on 6/22/16.
//  Copyright © 2016 Communique, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CMQEventAttendeeTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *userPhoto;
@property (weak, nonatomic) IBOutlet TTTAttributedLabel *userFullNameLabel;

@end
