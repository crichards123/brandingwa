//
//  CMQEventsViewModel.h
//  Communique
//
//  Created by Chris Hetem on 9/19/14.
//  Copyright (c) 2014 WaveRider, Inc. All rights reserved.
//

//@import Foundation;
#import <Foundation/Foundation.h>

@interface CMQEventsViewModel : NSObject

/**
 *  an array of all the events
 */
@property (copy, nonatomic) NSArray *eventsArray;

/**
 *  whether or not we are creating an event. used to determine if we should show our createEventView or not
 */
@property (assign, nonatomic) BOOL isCreatingEvent;

/**
 *  the date of the event that was just created
 */
@property (strong, nonatomic) NSDate *createdEventDate;

/**
 *  formats a date and returns a string specifically for the event stack
 *
 *  @param date the date to format
 *
 *  @return the formatted date as a string
 */
-(NSString *)formatDateForEventPosting:(NSDate *)date;

/**
 *  formats a date and returns a string specifically for the event stack
 *
 *  @param date the date to format
 *
 *  @return the formatted date as a string
 */
-(NSString *)formatDateForExistingEvent:(NSDate *)date;

/**
 *  formats a date and returns a string spefically for the create event view
 *
 *  @param date the date to format
 *
 *  @return the formatted date as a string
 */
-(NSString *)formatDateForNewEvent:(NSDate *)date;

/**
 *  resizes an image to 640x960 or 960x640 depending on landscape or portrait
 *
 *  @param image the image to resize
 *
 *  @return the resized image
 */
-(UIImage *)resizeImage:(UIImage *)image;


@end
