//
//  CMQCreateEventView.h
//  Communique
//
//  Created by Chris Hetem on 9/24/14.
//  Copyright (c) 2014 WaveRider, Inc. All rights reserved.
//

//@import UIKit;
#import <UIKit/UIKit.h>

@interface CMQCreateEventView : UIView
@property (weak, nonatomic) IBOutlet UIButton *selectDateTimeButton;
@property (weak, nonatomic) IBOutlet UIImageView *eventImageView;
@property (weak, nonatomic) IBOutlet UIButton *selectPhotoButton;
@property (weak, nonatomic) IBOutlet UITextField *eventLocationTextField;
@property (weak, nonatomic) IBOutlet UITextField *eventTitleTextField;
@property (weak, nonatomic) IBOutlet UITextView *eventContentTextView;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UIButton *postButton;
@property (weak, nonatomic) IBOutlet UIDatePicker *eventDatePicker;
@property (weak, nonatomic) IBOutlet UIButton *confirmButton;

@end
