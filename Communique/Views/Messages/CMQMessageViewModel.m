//
//  CMQNoticesViewModel.m
//  Communique
//
//  Created by Chris Hetem on 9/17/14.
//  Copyright (c) 2014 WaveRider, Inc. All rights reserved.
//

#import "CMQMessageViewModel.h"
//#import "CommuniqueiOS/CommuniqueiOS-Swift.h"
@interface CMQMessageViewModel()
@property(retain, nonatomic)NSMutableArray* privateMessages;
@end
@implementation CMQMessageViewModel


/**
 *  formats a notice title and details into one attributed string
 *
 *  @param title   the notice title
 *  @param details the notice details
 *
 *  @return an attributed string using the title and detail to be used for a single label
 */
-(NSMutableAttributedString *)getMessageForRow:(NSInteger)row{
    //get notice
   CMQMessage *message = [self.messagesArray objectAtIndex:row];
    
    NSString *title = message.messageTitle;
    NSString *details = message.messageContent;
    
    //create string to use for final string
    NSMutableAttributedString *fullNotice = [[NSMutableAttributedString alloc]init];
    
    //create two font types
    UIFont *boldFont = [UIFont fontWithName:kCMQFontGeometriaBold size:14];
    UIFont *regularFont = [UIFont fontWithName:kCMQFontGeometriaLight size:14];
    
    //create two attribute types
    NSDictionary *attrs = [NSDictionary dictionaryWithObjectsAndKeys:
                           boldFont, NSFontAttributeName,
                           [UIColor blackColor], NSForegroundColorAttributeName, nil];
    NSDictionary *subAttrs = [NSDictionary dictionaryWithObjectsAndKeys:
                              regularFont, NSFontAttributeName,
                              [UIColor blackColor], NSForegroundColorAttributeName, nil];
    
    //create two attributed strings using fonts and attributes
    NSAttributedString *noticeTitle = [[NSAttributedString alloc]initWithString:[NSString stringWithFormat:@"%@\n", title] attributes:attrs];
    NSAttributedString *noticeDetails = [[NSAttributedString alloc]initWithString:details attributes:subAttrs];
    
    //append to our full string and return
    [fullNotice appendAttributedString:noticeTitle];
    [fullNotice appendAttributedString:noticeDetails];
    
    return fullNotice;
}
-(NSMutableArray*)messagesArray{
    [CMQMessageDataSourceManager sharedInstance].shouldShowUnSentScheduledMessages = self.shouldShowUnsent;
    return [NSMutableArray arrayWithArray:[[CMQMessageDataSourceManager sharedInstance]filterWithMessages:_privateMessages]];
}
-(void)setMessagesArray:(NSMutableArray *)messagesArray{
    _privateMessages = messagesArray;
}
-(void)insertObject:(CMQMessage *)object inMessagesArrayAtIndex:(NSUInteger)index{
    [_privateMessages insertObject:object atIndex:index];
}
/**
 *  forms name of userWhoPosted into an attributed string
 *
 *  @param poster the objectId of the poster's user object
 *
 *  @return an attributed string using the title and detail to be used for a single label
 
-(NSString *)getLabelForRow:(NSInteger)row{
    //get notice
    CMQMessage *message = [self.messagesArray objectAtIndex:row];
    
    NSString *string = @"";
    
    if (message.isIndividualMessage){
        CMQUser *user = (CMQUser *)message.userWhoPosted;
        CMQUser *recipient = (CMQUser *)[PFQuery getUserObjectWithId:message.recipients[0]]; //Being called on main thread
    
        if ([user.objectId isEqualToString:[CMQUser currentUser].objectId])
            string = [NSString stringWithFormat:@"To: %@", (recipient && recipient.fullName) ? recipient.fullName : @"unknown"];
        else
            string = [NSString stringWithFormat:@"To: %@", [CMQUser currentUser].fullName];
    }else if(!message.recipients || [message.recipients count] == 0)
        string = @"To: Community";
    else if([message.recipients count] > 0 && message.isAnnouncement)
        string = ([message.recipients count] == 1) ? @"To: Community" : [NSString stringWithFormat:@"To: %lu communities", (unsigned long)[message.recipients count]];
    else if([message.recipients count] > 0)
        string = ([message.recipients count] == 1) ? [NSString stringWithFormat:@"To: %@", message.recipients[0]] : @"To: multiple apartments";
    
    return string;
}
*/
//Changed previous method to stop user from being loaded on the main thread, causing blocking when scrolling through messages.
-(void)getLabelForRow:(NSInteger)row withCompletion:(void(^)(NSString* string, NSError* error))completion{
    CMQMessage *message = [self.messagesArray objectAtIndex:row];
    NSString *string = @"";
    if (message.isIndividualMessage){
        [[[PFQuery queryWithClassName:@"User"]whereKey:@"objectId" equalTo:message.recipients[0]]findObjectsInBackgroundWithBlock:^(NSArray * _Nullable objects, NSError * _Nullable error) {
            if (!error) {
                NSString* string =[NSString new];
                CMQUser *user = (CMQUser *)message.userWhoPosted;
                CMQUser *recipient = (CMQUser *)objects.firstObject;
                if ([user.objectId isEqualToString:[CMQUser currentUser].objectId])
                    string = [NSString stringWithFormat:@"To: %@", (recipient && recipient.fullName) ? recipient.fullName : @"unknown"];
                else
                    string = [NSString stringWithFormat:@"To: %@", [CMQUser currentUser].fullName];
                completion(string,nil);
            }
            else{
                completion(nil,error);
            }
        }];
        
    }else if(!message.recipients || [message.recipients count] == 0)
        string = @"To: Community";
    else if([message.recipients count] > 0 && message.isAnnouncement)
        string = ([message.recipients count] == 1) ? @"To: Community" : [NSString stringWithFormat:@"To: %lu communities", (unsigned long)[message.recipients count]];
    else if([message.recipients count] > 0)
        string = ([message.recipients count] == 1) ? [NSString stringWithFormat:@"To: %@", message.recipients[0]] : @"To: multiple apartments";
    completion(string,nil);
    
}
-(NSString *)getDateForRow:(NSInteger)row{
    //get date from data
    CMQMessage *message = [self.messagesArray objectAtIndex:row];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    
    //calendar to extract day and month compenents
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateFormatter* formatter = [[NSDateFormatter alloc]init];
    formatter.dateFormat = @"yyyy-MM-dd'T'HH:mm";
    formatter.timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
    
    NSDateComponents *components = [calendar components:(NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear) fromDate:self.shouldShowUnsent?[formatter dateFromString:message.scheduleTime]:message.createdAt];
    
    NSInteger month = [components month]-1;
    NSInteger day = [components day];
    
    NSString *monthName = [[dateFormatter shortMonthSymbols]objectAtIndex:month];
    NSString *dateNumber = [NSString stringWithFormat:@"%ld", (long)day];
    
    NSString *formatStringForHours = [NSDateFormatter dateFormatFromTemplate:@"j" options:0 locale:[NSLocale currentLocale]];
    
    NSRange containsA = [formatStringForHours rangeOfString:@"a"];
    BOOL hasAMPM = containsA.location != NSNotFound;
    
    if (hasAMPM) { dateFormatter.dateFormat = @"h:mm a";}
    else { dateFormatter.dateFormat = @"HH:mm";}
    NSString *time = [dateFormatter stringFromDate:self.shouldShowUnsent?[formatter dateFromString:message.scheduleTime]:message.scheduleMessage?[formatter dateFromString: message.scheduleTime]:message.createdAt];
    
    return self.shouldShowUnsent?[NSString stringWithFormat:@"Scheduled for %@. %@, %@", monthName, dateNumber, time]:[NSString stringWithFormat:@"%@. %@, %@", monthName, dateNumber, time];
}
@end
