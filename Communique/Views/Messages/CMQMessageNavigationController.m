//
//  CMQMessageNavigationController.m
//  Communique
//
//  Created by Andre White on 10/19/17.
//  Copyright © 2017 Communique, LLC. All rights reserved.
//

#import "CMQMessageNavigationController.h"

@interface CMQMessageNavigationController ()

@end

@implementation CMQMessageNavigationController

- (void)viewDidLoad {
    [super viewDidLoad];
    [[UIBarButtonItem appearance]setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor],
                                                          //NSShadowAttributeName:shadow,
                                                          NSFontAttributeName:[UIFont fontWithName:kCMQFontGeometriaLight size:19]
                                                          } forState:UIControlStateNormal];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

@end
