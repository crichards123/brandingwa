//
//  CMQMessageViewController.h
//  Communique
//
//  Created by Chris Hetem on 9/16/14.
//  Copyright (c) 2014 WaveRider, Inc. All rights reserved.
//

//@import UIKit;
#import <UIKit/UIKit.h>

@interface CMQMessageViewController : GAITrackedViewController
-(void)onRefresh;
@end
