//
//  CMQScheduledMessageViewController.swift
//  Communique
//
//  Created by Andre White on 11/6/17.
//  Copyright © 2017 Communique, LLC. All rights reserved.
//

import UIKit

public class CMQScheduledMessageViewController: UIViewController {
    @IBOutlet weak public var dateButton: UIButton!
    @IBOutlet weak public var timeButton: UIButton!
    @IBOutlet weak public var saveButton: UIButton!
    @IBOutlet weak public var containerView: UIView!
    @IBInspectable public var unSelectedColor: UIColor!
    @IBInspectable public var selectedColor: UIColor!
    @objc public var isScheduledMessage:Bool = false
    @objc public let preferredSize = CGSize.init(width: 325, height: 450)
    private var isAnimating = false
    @objc public var selectedDayString : String?{
        didSet{
            if let _ = selectedDayString , let _ = selectedTimeString{
                if let button = saveButton{
                    button.isEnabled = true
                    button.backgroundColor = selectedColor
                }
                
            }else{
                if let button = saveButton{
                    button.isEnabled = false
                    button.backgroundColor = unSelectedColor
                }
                
            }
        }
    }
    @objc public var selectedTimeString: String?{
        didSet{
            if let _ = selectedDayString , let _ = selectedTimeString{
                if let button = saveButton{
                    button.isEnabled = true
                    button.backgroundColor = selectedColor
                }
                
            }else{
                if let button = saveButton{
                    button.isEnabled = false
                    button.backgroundColor = unSelectedColor
                }
                
            }
        }
    }
    public var selectedDate:Date?{
        get{
            guard let day = selectedDayString, let time = selectedTimeString else {return nil}
            let selectionFormat = DateFormatter.init()
            selectionFormat.dateFormat = "MM-dd-yyyy h:mm a"
            selectionFormat.timeZone = .current
            let string = String.init(format: "%@ %@", day, time)
           return  selectionFormat.date(from: string)
        }
        set(newDate){
            if let date = newDate{
                selectedDayString = date.dateOnlyString
                selectedTimeString = date.timeOnlyString
            }
        }
    }
    private var datePickerView:CMQCalendarDatePickerViewController!
    private var timePickerView:CMQTimePickerViewController!
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        self.initChildControllers()
        self.addNotificationObservers()
        // Do any additional setup after loading the view.
    }

    func initChildControllers(){
        if let aClass = NSClassFromString("CMQPackage"){
            let bundle = Bundle.init(for: aClass)
            self.datePickerView = self.children.first as? CMQCalendarDatePickerViewController
            self.timePickerView = UIStoryboard.init(name: "Messages", bundle: bundle).instantiateViewController(withIdentifier: "TimeView") as? CMQTimePickerViewController
            self.timePickerView.isScheduledMessage = self.isScheduledMessage
        
        }
            
       
    }
    func addNotificationObservers(){
        NotificationCenter.default.addObserver(self, selector: #selector(timeSelected(notification:)), name: .didSetTimeNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(daySelected(notification:)), name: datePickerView.didSetDateNotification, object: nil)
    }
    func removeNotificationObservers(){
        NotificationCenter.default.removeObserver(self, name: .didSetTimeNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: datePickerView.didSetDateNotification, object: nil)
    }
    @objc func timeSelected(notification:Notification){
        if let selectedTime = notification.object as? String{
            selectedTimeString = selectedTime
             print("time selected: "+selectedTimeString!)
        }
    }
    @objc func daySelected(notification: Notification){
        if let selectedDay = notification.object as? String{
            selectedDayString = selectedDay
            let today = Date.init(timeIntervalSinceNow: 0)
            let todayString = today.dateOnlyString
            timePickerView.isPickingForToday = todayString == selectedDayString
            print("Day selcted: "+selectedDayString!)
        }
        
    }
    func updateButtons(){
        if isAnimating {
            //inverted while animating
            dateButton.backgroundColor = self.children.first is CMQCalendarDatePickerViewController ? self.unSelectedColor : self.selectedColor
            timeButton.backgroundColor = self.children.first is CMQTimePickerViewController ? self.unSelectedColor : self.selectedColor
        }
        else{
            dateButton.backgroundColor = self.children.first is CMQCalendarDatePickerViewController ? self.selectedColor : self.unSelectedColor
            timeButton.backgroundColor = self.children.first is CMQTimePickerViewController ? self.selectedColor : self.unSelectedColor
        }
    }
    func cycleControllers(){
        self.isAnimating = true
        let newVC = self.children.first is CMQCalendarDatePickerViewController ? self.timePickerView : self.datePickerView
        let oldVC = self.children.first!
        let new = newVC!
        oldVC.willMove(toParent: nil)
        self.addChild(new)
        new.view.frame = self.getNewViewStartFrame()
        self.transition(from: oldVC, to: new, duration: 0.5, options: UIView.AnimationOptions(rawValue: 0), animations: {
            new.view.frame = oldVC.view.frame;
            oldVC.view.frame = self.getOldViewEndFrame()
            self.updateButtons()
        }) { (finished) in
            oldVC.removeFromParent()
            new.didMove(toParent: self)
            self.isAnimating = false
        }
    }
    func getNewViewStartFrame()->CGRect{
        return self.children.first is CMQCalendarDatePickerViewController ? CGRect.init(x: self.containerView.frame.width, y: 0, width: self.containerView.frame.width, height: self.containerView.frame.height) : CGRect.init(x: -self.containerView.frame.width, y: 0, width: self.view.frame.width, height: self.containerView.frame.height)
    }
    func getOldViewEndFrame()->CGRect{
        return self.children.first is CMQCalendarDatePickerViewController ? CGRect.init(x: -self.containerView.frame.width, y: 0, width: self.containerView.frame.width, height: self.containerView.frame.height) : CGRect.init(x: self.containerView.frame.width, y: 0, width: self.view.frame.width, height: self.containerView.frame.height)
    }
    @IBAction func dateButtonPressed(_ sender: Any) {
        if !isAnimating && self.children.first is CMQTimePickerViewController{
            cycleControllers()
        }
    }
    
    @IBAction func timeButtonPressed(_ sender: Any) {
        if !isAnimating && self.children.first is CMQCalendarDatePickerViewController{
            cycleControllers()
        }
    }
    @IBAction func saveButtonPressed(_ sender: Any) {
        guard let _ = selectedDayString else {return}
        self.performSegue(withIdentifier: "ScheduleMessage", sender: nil)
    }
    

}
