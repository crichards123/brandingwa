//
//  CMQCalendarDatePickerViewController.swift
//  Communique
//
//  Created by Andre White on 11/6/17.
//  Copyright © 2017 Communique, LLC. All rights reserved.
//

import UIKit
import JTAppleCalendar

public class CMQCalendarDatePickerViewController: UIViewController {
    @IBOutlet public weak var calendarView: JTAppleCalendarView!
    @IBOutlet public weak var monthLabel: UILabel!
    @IBOutlet public weak var yearLabel: UILabel!
    public var didSetDateNotification = Notification.Name("didSetDate")
    var formatter = DateFormatter()
    
    public var selectedDateString:String?{
        didSet{
            guard let string = selectedDateString else {return}
            if string.count>0 {
                NotificationCenter.default.post(name: didSetDateNotification, object: string)
            }
        }
    }
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        self.setUpCalendar()
        

        // Do any additional setup after loading the view.
    }
    
    override public func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func setUpCalendar(){
        calendarView.calendarDataSource = self
        calendarView.calendarDelegate = self
        calendarView.minimumLineSpacing = 0
        calendarView.minimumInteritemSpacing = 0
        
        calendarView.visibleDates { (visibleDates) in
            self.updateCalendarMonth(visibleDates: visibleDates)
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension CMQCalendarDatePickerViewController : JTAppleCalendarViewDelegate,JTAppleCalendarViewDataSource{
    func isCellSelectable(state: CellState)->Bool{
        if formatter.date(from:formatter.string(from:state.date))!<formatter.date(from:formatter.string(from:Date.init(timeIntervalSinceNow: 0)))! {
            return false
        }
        return true
    }
    func handleCell(dateCell: JTAppleCell? , state: CellState){
        guard let possibleCell = dateCell as? CMQCalendarDateCell else {return}
        
        if formatter.string(from: state.date) == self.selectedDateString {
            possibleCell.selectedView.isHidden = false
            possibleCell.dateLabel.textColor = UIColor.white
        }
        else{
            possibleCell.selectedView.isHidden = true
            if formatter.date(from:formatter.string(from:state.date))!<formatter.date(from:formatter.string(from:Date.init(timeIntervalSinceNow: 0)))!{
                possibleCell.dateLabel.textColor = UIColor.gray
                possibleCell.isUserInteractionEnabled = false
            }
            else{
                possibleCell.dateLabel.textColor = UIColor.black
                possibleCell.isUserInteractionEnabled = true
            }
            
        }
        switch state.dateBelongsTo {
        case .thisMonth:
            possibleCell.dateLabel.isHidden = false
            possibleCell.isUserInteractionEnabled = true
        default:
            possibleCell.selectedView.isHidden = true
            possibleCell.dateLabel.isHidden = true
            possibleCell.isUserInteractionEnabled = false
        }
        
    }
    func updateCalendarMonth(visibleDates:DateSegmentInfo){
        let date = visibleDates.monthDates.first!.date
        let formatter = DateFormatter.init()
        formatter.dateFormat = "yyyy"
        yearLabel.text = formatter.string(from: date)
        formatter.dateFormat = "MMMM"
        monthLabel.text = formatter.string(from: date)
    }
    public func calendar(_ calendar: JTAppleCalendarView, willDisplay cell: JTAppleCell, forItemAt date: Date, cellState: CellState, indexPath: IndexPath) {

    }
    public func calendar(_ calendar: JTAppleCalendarView, didScrollToDateSegmentWith visibleDates: DateSegmentInfo) {
        self.updateCalendarMonth(visibleDates: visibleDates)
    }
    public func calendar(_ calendar: JTAppleCalendarView, cellForItemAt date: Date, cellState: CellState, indexPath: IndexPath) -> JTAppleCell {
        let cell = calendar.dequeueReusableJTAppleCell(withReuseIdentifier: "dateCell", for: indexPath) as! CMQCalendarDateCell
        self.handleCell(dateCell: cell, state: cellState)
        cell.dateLabel.text = cellState.text
        return cell
    }
    public func calendar(_ calendar: JTAppleCalendarView, didSelectDate date: Date, cell: JTAppleCell?, cellState: CellState) {
        selectedDateString = formatter.string(from: cellState.date)
        self.handleCell(dateCell: cell, state: cellState)
    }
    public func calendar(_ calendar: JTAppleCalendarView, didDeselectDate date: Date, cell: JTAppleCell?, cellState: CellState) {
        self.selectedDateString = ""
        self.handleCell(dateCell: cell, state: cellState)
    }
    public func configureCalendar(_ calendar: JTAppleCalendarView) -> ConfigurationParameters {
        formatter.dateFormat = "MM-dd-yyyy"
        formatter.timeZone = Calendar.current.timeZone
        formatter.locale = Calendar.current.locale
        var selectedDate = self.selectedDateString == nil ? Date.init(timeIntervalSinceNow: 0).dateOnlyString: self.selectedDateString!
        
        let range = formatter.dateFormat.range(of: "dd")
        calendar.selectDates([formatter.date(from: selectedDate)!])
        selectedDate.replaceSubrange(range!, with: "01")
        let parameters = ConfigurationParameters(startDate: formatter.date(from: selectedDate)!, endDate: (formatter.date(from: selectedDate)?.addingTimeInterval(60*60*24*90))!)
        return parameters
    }
    public func calendar(_ calendar: JTAppleCalendarView, shouldSelectDate date: Date, cell: JTAppleCell?, cellState: CellState) -> Bool {
        return self.isCellSelectable(state: cellState)
    }
    
    
}
