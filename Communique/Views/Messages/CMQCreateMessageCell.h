//
//  CMQCreateNoticeCell.h
//  Communique
//
//  Created by Chris Hetem on 9/18/14.
//  Copyright (c) 2014 WaveRider, Inc. All rights reserved.
//

//@import UIKit;
#import <UIKit/UIKit.h>

@interface CMQCreateMessageCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UITextField *messageTitleTextField;
@property (weak, nonatomic) IBOutlet UITextView *messageDetailsTextView;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UIButton *postButton;
@property (weak, nonatomic) IBOutlet UIButton *allButton;
@property (weak, nonatomic) IBOutlet UIButton *urgentButton;
@property (weak, nonatomic) IBOutlet UIButton *photoButton;
@property (weak, nonatomic) IBOutlet UIButton *scheduleButton;
@property (weak, nonatomic) IBOutlet UILabel* scheduleLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageViewHeightConstraint;
@property (weak, nonatomic) IBOutlet UIImageView* pendingImageView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint* scheduledLabelHeightConstraint;
/**
 *  clears all fields
 */
-(void)reset;

@end
