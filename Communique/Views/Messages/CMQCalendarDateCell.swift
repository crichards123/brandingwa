//
//  CMQCalendarDateCell.swift
//  Communique
//
//  Created by Andre White on 11/9/17.
//  Copyright © 2017 Communique, LLC. All rights reserved.
//

import UIKit
import JTAppleCalendar
public class CMQCalendarDateCell: JTAppleCell {
    @IBOutlet public weak var dateLabel: UILabel!
    @IBOutlet public weak var selectedView: UIView!
    
    override public func awakeFromNib() {
        selectedView.layer.cornerRadius = self.selectedView.frame.height/2
    }
}
