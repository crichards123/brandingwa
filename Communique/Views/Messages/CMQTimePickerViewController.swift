//
//  CMQTimePickerViewController.swift
//  Communique
//
//  Created by Andre White on 11/6/17.
//  Copyright © 2017 Communique, LLC. All rights reserved.
//

import UIKit

public class CMQTimePickerViewController: UIViewController {
    @IBInspectable var selectedColor : UIColor!
    @IBInspectable var unselectedColor : UIColor!
    @IBInspectable var clockButtonTitleColor : UIColor!
    @IBInspectable var clockButtonDisabledTitleColor: UIColor!
    @IBOutlet weak var pmLabel: UILabel!
    @IBOutlet weak var amLabel: UILabel!
    @IBOutlet weak var minuteLabel: UILabel!
    @IBOutlet weak var hourLabel: UILabel!
    @IBOutlet var clockButtons: [UIButton]!
    @IBOutlet var clockView: CMQClockView!
    private var greenColor :UIColor{
        return clockView.greenColor
    }
    private var circleView: UIView!{
        return clockView.circleView
    }
    public var isPickingForToday = true
    private var selectedAMPMLabel: UILabel!
    private var selectedClockLabel:UILabel!
   
    private var selectedTime: String?{
        didSet{
            if let possibleTime = selectedTime{
                NotificationCenter.default.post(name: .didSetTimeNotification, object: possibleTime)
            }
        }
    }
    public var isScheduledMessage:Bool = false
    private var timeFormatter: DateFormatter{
        let formatter = DateFormatter.init()
        formatter.dateFormat = "h:mm a"
        return formatter
    }
    private var clockFormatter: NumberFormatter{
        let aFormatter = NumberFormatter.init()
        aFormatter.minimumIntegerDigits = self.selectedClockLabel == hourLabel ? 1:2
        return aFormatter
    }
    private var currentTime: Date{
        let todaysDate = Date.init(timeIntervalSinceNow: 0)
        let todaysTimeString = timeFormatter.string(from: todaysDate)
        return timeFormatter.date(from: todaysTimeString)!
    }
    public var selectedDate:Date?
    private var allLabels : Array<UILabel>{
        return [amLabel, pmLabel, minuteLabel, hourLabel]
    }
    private var selectedButton:UIButton?{
        return  clockButtons.filter { (button) -> Bool in
            let multiplier = selectedClockLabel == minuteLabel ? 5 : 1
            let comparableString = selectedClockLabel == minuteLabel ? self.removeColon(minuteString: minuteLabel.text!) : hourLabel.text!
            let possibleNumber = button.tag * multiplier == 60 ? 0 : button.tag * multiplier
            return clockFormatter.string(from: NSNumber.init(value: possibleNumber)) == comparableString
            }.first
    }
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        self.setUpView()
    }
    override public func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.handleLabelColors()
        self.handleButtonState()
    }
    override public func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func setUpView(){
        
        self.setCurrentTime()
        self.addSelectedView()
        self.handleLabelColors()
        self.handleButtonState()
    }

    // MARK: - IBActions
    @IBAction func hourTapped(_ sender: Any) {
        handleState(isMin: false)
    }
    @IBAction func minuteTapped(_ sender: Any) {
        handleState(isMin: true)
    }
    @IBAction func amTapped(_ sender: Any) {
        handleState(isAM: true)
    }
    @IBAction func pmTapped(_ sender: Any) {
        handleState(isAM: false)
    }
    @IBAction func clockButtonPressed(_ sender: UIButton) {
        
        if shouldChangeToTime(time: clockFormatter.number(from: sender.currentTitle!)!.intValue) {
            selectedClockLabel.text = selectedClockLabel == minuteLabel ? self.addColon(minuteString: sender.title(for: UIControl.State.normal)!) : sender.title(for: UIControl.State.normal)!
            //set selectedTime object
            if let possibleHour = hourLabel.text, let possibleMinute = minuteLabel.text {
                selectedTime = String.init(format: "%@%@ %@", possibleHour,possibleMinute, selectedAMPMLabel == amLabel ? "AM":"PM")
                self.handleSelectedView()
            }
        }
    }
    
    //MARK: State Changes
    func handleState(isMin:Bool){
        selectedClockLabel = isMin ? minuteLabel : hourLabel
        handleLabelColors()
        handleButtonState()
    }
    func handleState(isAM:Bool){
        guard let currentMinuteText = minuteLabel.text, let currentHourText = hourLabel.text else{
            return
        }
        if let minClockNum = clockFormatter.number(from: self.removeColon(minuteString: currentMinuteText)), let hrClockNum = clockFormatter.number(from: currentHourText){
            let selectedClockNum = selectedClockLabel == minuteLabel ? minClockNum : hrClockNum
            selectedAMPMLabel = isAM ? amLabel : pmLabel
            if shouldChangeToTime(time: selectedClockNum.intValue){
                handleLabelColors()
                handleButtonState()
                //setTime
                if let possibleHour = hourLabel.text, let possibleMinute = minuteLabel.text {
                    selectedTime = String.init(format: "%@%@ %@", possibleHour,possibleMinute, selectedAMPMLabel == amLabel ? "AM":"PM")
                }
            }
            else{
                selectedAMPMLabel = isAM ? pmLabel : amLabel
            }
        }
    }
    func handleButtonState(){
        for clockButton in clockButtons{
            self.handleTitle(clockButton: clockButton)
            self.handleButtonVisibility(clockButton: clockButton)
            self.handleEnabled(clockButton: clockButton)
            self.handleTitleColor(clockButton: clockButton)
        }
        self.handleSelectedView()
    }
    func addSelectedView(){
        //self.view.addSubview(circleView)
    }
    func handleSelectedView(){
        if let button = circleView.superview as? UIButton{
            button.setTitleColor(UIColor.black, for: UIControl.State.normal)
        }
        circleView.removeFromSuperview()
        selectedButton?.setTitleColor(UIColor.white, for: UIControl.State.normal)
        selectedButton?.addSubview(circleView)
        selectedButton?.sendSubviewToBack(circleView)
        //drawLineFromCenter()
    }
    func drawLineFromCenter(){
        let path = UIBezierPath()
        if let button = selectedButton{
            path.move(to: CGPoint(x: clockView.bounds.size.width/2, y: clockView.bounds.size.height/2))
            path.addLine(to: CGPoint(x: button.bounds.size.width/2, y: button.bounds.size.height/2))
            path.close()
        }
        let shapeLayer = CAShapeLayer()
        shapeLayer.path = path.cgPath
        clockView.shapeLayer = shapeLayer
        self.view.setNeedsDisplay()
    }
    func handleEnabled(clockButton:UIButton!){
        clockButton.isEnabled = shouldChangeToTime(time: clockFormatter.number(from: clockButton.currentTitle!)!.intValue)
    }
    func handleTitle(clockButton:UIButton!){
        switch selectedClockLabel {
        case hourLabel:
            clockButton.setTitle(clockFormatter.string(from: NSNumber.init(value: clockButton.tag)), for: UIControl.State.normal)
        case minuteLabel:
            clockButton.setTitle(clockFormatter.string(from: NSNumber.init(value: clockButton.tag*5 == 60 ? 0: clockButton.tag*5)), for: UIControl.State.normal)
        default:
            //do nothing because there should only be two cases.
            NSLog("%@", "Handling title failed. Unexpected case.")
        }
    }
    func handleButtonVisibility(clockButton:UIButton){
        switch selectedClockLabel {
        case minuteLabel:
            if let title = clockButton.title(for: UIControl.State.normal){
                if let titleNum = clockFormatter.number(from: title){
                    let num = titleNum.intValue
                    let shouldShow = isScheduledMessage ? num % 15 == 0 : num % 5 == 0
                    clockButton.isHidden = !shouldShow
                }
            }
        default:
            clockButton.isHidden = false
        }
    }
    func handleTitleColor(clockButton:UIButton!){
        clockButton.setTitleColor(clockButton.isEnabled ? clockButtonTitleColor : clockButtonDisabledTitleColor, for: UIControl.State.normal)
    }
    func handleLabelColors(){
        for label in allLabels {
            if label == selectedAMPMLabel || label == selectedClockLabel {
                label.textColor = selectedColor
            }
            else{
                label.textColor = unselectedColor
            }
        }
    }
    func shouldChangeToTime(time: Int)->Bool{
        if isPickingForToday{
            var minuteString: String, hourString: String
            switch selectedClockLabel {
            case hourLabel:
                minuteString = minuteLabel.text!
                hourString = clockFormatter.string(from: NSNumber.init(value: time))!
            case minuteLabel:
                minuteString = self.addColon(minuteString: clockFormatter.string(from: NSNumber.init(value: time))!)
                hourString = hourLabel.text!
            default:
                return false
            }
            let selectedTimeString = String.init(format: "%@%@ %@", hourString,minuteString, selectedAMPMLabel == amLabel ? "AM":"PM")
            let selectedTime = timeFormatter.date(from: selectedTimeString)!
            return selectedTime > currentTime
        }
        return true
    }
    func setCurrentTime(){
        var minString = String(), hrString = String(), amPmString = String()
        let currentTimeString = selectedDate == nil ? currentTime.timeOnlyString : selectedDate!.timeOnlyString
        let components = currentTimeString.split(separator: " ")
        let timeString = components.first!
        amPmString = String(describing:components.last!)
        let timeSplit = timeString.split(separator: ":")
        hrString = String(describing:timeSplit.first!)
        minString = String(describing:timeSplit.last!)
        
        //set minute string first
        //set property for correct clockFormatter options set
        selectedAMPMLabel = amPmString == "AM" ? amLabel : pmLabel
        selectedClockLabel = minuteLabel
        let currentMins = clockFormatter.number(from: minString)!
        
        for number in 1...4 {
            let minNum = number * 15
            if minNum>currentMins.intValue{
                if minNum == 60{
                    minString = clockFormatter.string(from: NSNumber.init(value: 0))!
                }
                else{
                    minString = clockFormatter.string(from: NSNumber.init(value: minNum))!
                }
                break
            }
        }
        minuteLabel.text = self.addColon(minuteString: minString)
        
        //set hour string
        //Change property for formatter again
        selectedClockLabel = hourLabel
        var hrNumber = clockFormatter.number(from: hrString)!
        if shouldChangeToTime(time: hrNumber.intValue) {
            hourLabel.text = hrString
        }
        else{
            //This likely means the min string is "00" which means we go to the next hour
            if hrString == "12"{
                hrNumber = clockFormatter.number(from: "1")!
            }else{
                hrNumber =  NSNumber.init(value: hrNumber.intValue+1)
            }
            hrString = clockFormatter.string(from: hrNumber)!
            if shouldChangeToTime(time: (clockFormatter.number(from: hrString)!.intValue)){
                hourLabel.text = hrString
            }
            else{
                //This likely means that there is an AMPM switch and we should change the AM PM label
                selectedAMPMLabel = pmLabel
                if shouldChangeToTime(time: (clockFormatter.number(from: hrString)!.intValue)){
                    hourLabel.text = hrString
                }
                else{
                    //We should never get here, if so the number will just be the default 12
                }
            }
        }
        if let possibleHour = hourLabel.text, let possibleMinute = minuteLabel.text {
            selectedTime = String.init(format: "%@%@ %@", possibleHour,possibleMinute, selectedAMPMLabel == amLabel ? "AM":"PM")
        }
        
    }
    func removeColon(minuteString:String)->String{
        if minuteString.first == ":" {
            let newString = minuteString.dropFirst()
            return String(describing: newString)
        }
        else{
            return minuteString
        }
    }
    func addColon(minuteString: String)->String{
        if minuteString.first != ":" {
            return String(format: ":%@",minuteString)
        }
        else{
            return minuteString
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
