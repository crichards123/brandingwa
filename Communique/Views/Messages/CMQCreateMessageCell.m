//
//  CMQCreateNoticeCell.m
//  Communique
//
//  Created by Chris Hetem on 9/18/14.
//  Copyright (c) 2014 WaveRider, Inc. All rights reserved.
//

#import "CMQCreateMessageCell.h"

@implementation CMQCreateMessageCell

- (void)awakeFromNib{
    [super awakeFromNib];
    
    [self configureUI];
}

/**
 *  initially configures the UI
 */
-(void)configureUI{
    //this is just a clear view that sits to the let of the notice title text view. it's used for 'indenting' since there's no easy way to do it otherwise (i.e. no contentInset or anything of that sort)
    UIView *spacerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 10)];
    [self.messageTitleTextField setLeftViewMode:UITextFieldViewModeAlways];
    [self.messageTitleTextField setLeftView:spacerView];
    
    //font
    [self.messageTitleTextField setFont:[UIFont fontWithName:kCMQFontGeometriaLight size:17]];
    [self.messageDetailsTextView setFont:[UIFont fontWithName:kCMQFontGeometriaLight size:14]];
    NSBundle* comBundle = [NSBundle bundleForClass:[CMQCreateMessageCell class]];
    [self.allButton setImage:[UIImage imageNamed:@"notif-btn-on" inBundle:comBundle compatibleWithTraitCollection:nil] forState:UIControlStateSelected];
    [self.allButton setImage:[UIImage imageNamed:@"notif-btn-off" inBundle:comBundle compatibleWithTraitCollection:nil] forState:UIControlStateNormal];
    [self.urgentButton setImage:[UIImage imageNamed:@"notif-btn-on" inBundle:comBundle compatibleWithTraitCollection:nil] forState:UIControlStateSelected];
    [self.urgentButton setImage:[UIImage imageNamed:@"notif-btn-off" inBundle:comBundle compatibleWithTraitCollection:nil] forState:UIControlStateNormal];
}

-(void)reset{
    [self.messageDetailsTextView setTextColor:[UIColor lightGrayColor]];
    self.messageDetailsTextView.text = @"Message text";;
    self.messageTitleTextField.text = @"";
}


@end
