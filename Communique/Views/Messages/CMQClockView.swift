//
//  CMQClockView.swift
//  Communique
//
//  Created by Andre White on 11/15/17.
//  Copyright © 2017 Communique, LLC. All rights reserved.
//

import UIKit

public class CMQClockView: UIView {
    public var circleView: UIView!
    public var greenColor: UIColor{
        return UIColor.init(red: 32/255, green: 178/255, blue: 170/255, alpha: 1.0)
    }
    public var shapeLayer:CAShapeLayer?{
        willSet{
            if shapeLayer != nil {
                shapeLayer?.removeFromSuperlayer()
            }
        }
        didSet{
            if shapeLayer != nil {
                shapeLayer?.strokeColor = greenColor.cgColor
                shapeLayer?.fillColor = greenColor.cgColor
                shapeLayer?.lineWidth = 3
                self.layer.addSublayer(shapeLayer!)
                setNeedsDisplay()
            }
        }
    }
    
    override public func awakeFromNib() {
        self.createCircleView()
        self.layer.cornerRadius = self.frame.height/2
        
    }
    func createCircleView(){
        circleView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: 28, height: 28))
        circleView.backgroundColor = greenColor
        circleView.layer.cornerRadius = 14
    }
    

}
