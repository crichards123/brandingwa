//
//  CMQPackageDeliveryConfirmationViewController.swift
//  Communique
//
//  Created by Andre White on 12/8/17.
//  Copyright © 2017 Communique, LLC. All rights reserved.
//

import UIKit

public class CMQPackageDeliveryConfirmationViewController: UIViewController {
    class func instanceFromStoryBoard()->CMQPackageDeliveryConfirmationViewController?{
        guard let aClass = NSClassFromString("CMQPackage") else {return nil}
        let bundle = Bundle.init(for: aClass)
        let storyBoard = UIStoryboard.init(name: "Messages", bundle: bundle)
        return storyBoard.instantiateViewController(withIdentifier: "Confirmation") as? CMQPackageDeliveryConfirmationViewController
    }
    
    
    @IBOutlet var promptView: CMQPackagePromptView!
    @objc public var isConfirmed = false
    @objc public let preferredSize = CGSize.init(width: 300.0, height: 171)
    public var item:FeedItemStruct!
    /*private var deliveryPrompt : String {
        if let apartment = CMQAPIManager.sharedInstance().apartment, let price = apartment.deliveryPrice{
                return String.init(format: "Confirm delivery to apartment. This will cost $%@", price)
        }else{
            return ""
        }
    }*/
    override public func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(handlePackageNotification(notification:)), name: .packageNotification, object: nil)
        self.promptView.item = item
        self.promptView.awakeFromNib()
    }
    @objc public func handlePackageNotification(notification:Notification){
        if let confirmed = notification.object as? Bool{
            isConfirmed = confirmed
            performSegue(withIdentifier: "Confirm", sender: nil)
        }
    }
    public override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    /*@IBAction func didConfirm(_ sender: UIButton) {
        switch sender.tag {
        case 0:
            isConfirmed = false
        default:
            isConfirmed = true
        }
        performSegue(withIdentifier: "Confirm", sender: nil)
    }*/

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
