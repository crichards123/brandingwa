//
//  CMQNoticeFlowLayout.m
//  Communique
//
//  Created by Chris Hetem on 9/18/14.
//  Copyright (c) 2014 WaveRider, Inc. All rights reserved.
//

#import "CMQMessageFlowLayout.h"

@interface CMQMessageFlowLayout ()

@property (strong, nonatomic) NSIndexPath *removedIndexPath;

@end

@implementation CMQMessageFlowLayout

- (void)prepareForCollectionViewUpdates:(NSArray *)updateItems
{
    [super prepareForCollectionViewUpdates:updateItems];
    
    [updateItems enumerateObjectsUsingBlock:^(UICollectionViewUpdateItem *updateItem, NSUInteger idx, BOOL *stop) {
        
        if(updateItem.updateAction == UICollectionUpdateActionDelete){
            self.removedIndexPath = updateItem.indexPathBeforeUpdate;
        }
    }];
}


// These layout attributes are applied to a cell that is "disappearing" and will be eased to from the nominal layout attribues prior to disappearing
// Cells "disappear" in several cases:
//  - Removed explicitly or via a section removal
//  - Moved as a result of a removal at a lower index path
//  - Result of an animated bounds change repositioning cells
- (UICollectionViewLayoutAttributes*)finalLayoutAttributesForDisappearingItemAtIndexPath:(NSIndexPath *)itemIndexPath{
    
    UICollectionViewLayoutAttributes *attributes = [super finalLayoutAttributesForDisappearingItemAtIndexPath:itemIndexPath];
    
    if([self.removedIndexPath isEqual: itemIndexPath]){
        CGAffineTransform scale = CGAffineTransformMakeScale(0.5, 0.5);
        CGAffineTransform translate = CGAffineTransformMakeTranslation(0, -100);
        attributes.transform = CGAffineTransformConcat(scale, translate);
        attributes.alpha = 0.01;
    }
    
    return attributes;
}
@end
