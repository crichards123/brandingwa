//
//  CMQMessageViewController.m
//  Communique
//
//  Created by Chris Hetem on 9/16/14.
//  Copyright (c) 2014 WaveRider, Inc. All rights reserved.
//

#import "CMQMessageViewController.h"
#import "CMQMessageViewModel.h"
#import "CMQMessageCell.h"
#import "CMQCreateMessageCell.h"
#import "UIColor+CMQColors.h"
#import "DZNWebViewController.h"
#import "NSString+NSHash.h"
#import "CMQPackageCheckOutPinPresentationController.h"
#define DEFAULT_CELL_HEIGHT         90

static NSString *contentCellIdentifier = @"contentCell";
static NSString *createMessageCellIdentifier = @"createMessageCell";
static NSString* packageCellIdentifier = @"packageCell";
static NSString* photoCellIdentifier = @"photoCell";
@interface CMQMessageViewController () <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UITextFieldDelegate, UITextViewDelegate, CMQAPIClientDelegate, TTTAttributedLabelDelegate, CMQMessageProtocol, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIViewControllerTransitioningDelegate>

//Properties
@property (strong, nonatomic) CMQMessageViewModel *viewModel;
@property (assign, nonatomic) BOOL isCreatingMessage;
@property (strong, nonatomic) UITextView *cellTextView;
@property (strong, nonatomic) UIRefreshControl *refreshControl;
@property (strong, nonatomic) DZNWebViewController *webViewController;
@property (retain, nonatomic) UIImage* messageImage;
//IBOutlets
@property (weak, nonatomic) IBOutlet UITableView* tableView;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UIButton *createMessageButton;
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UIView *alertContainerView;
@property (weak, nonatomic) IBOutlet UILabel *alertMessage;
@property (weak, nonatomic) IBOutlet UIButton* scheduledMessageButton;

@property(retain, nonatomic)NSMutableArray* shouldShowPrompt;

//state restoration
@property(retain, nonatomic)NSDate* selectedTime;
@property(retain, nonatomic)NSString* pendingTitle;
@property(retain, nonatomic)NSString* pendingMessage;
@property(retain, nonatomic)UITextView* pendingMessageView;
@property(retain, nonatomic)UITextField* pendingMessageTitleField;
@property(retain, nonatomic)CMQMessageCell* responseCell;
@property(retain, nonatomic)CMQMessage* responseMessage;
//IBActions
- (IBAction)onCreateMessage:(id)sender;

@end

@implementation CMQMessageViewController{
    CMQMessage *newMessage;
    unsigned long messageContentCharLimit;
    unsigned long messageTitleCharLimit;
}

#pragma mark - View Lifecycle

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad{
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
    
    self.screenName = @"Messages";
    
    [self configure];   //call for initial configuration
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [SVProgressHUD dismiss];
}

#pragma mark - Config

/**
 *  configures the UI on initial loading, such as fonts, colors, etcs
 */
-(void)configure{
    _shouldShowPrompt=[NSMutableArray new];
    self.scheduledMessageButton.hidden = ![CMQUser currentUser].isAdmin;
    //self.view
    self.view.backgroundColor = [UIColor CMQRedColor];  //set background color
    self.viewModel = [[CMQMessageViewModel alloc]init]; //init our view model
    
    //back button
    [self.backButton.titleLabel setFont:[UIFont fontWithName:kCMQFontGeometriaLight size:19]];
    [self.backButton setTitleColor:[UIColor lightTextColor] forState:UIControlStateNormal];
    
    //alert
    [self.alertContainerView setHidden:YES];
    [self.alertMessage setFont:[UIFont fontWithName:kCMQFontGeometriaLight size:15]];
    
    //check if user is Admin or not. if not, hide our createMessageButton
    if([CMQUser currentUser].isAdmin){
        self.createMessageButton.hidden = NO;
    }else{
        self.createMessageButton.hidden = YES;
    }
    
    //HUD
    [SVProgressHUD showWithStatus:@"Fetching messages"];
    
    //Fetch
    [[CMQAPIClient sharedClient]setDelegate:self];
    [[CMQAPIClient sharedClient]fetchMessages]; //call to fetch our messages
    //[CMQMessageDataSourceManager sharedInstance]getAllMessages

    //register both nib types
    NSBundle* comBundle = [NSBundle bundleForClass:[CMQMessageCell class]];
    [self.collectionView registerNib:[UINib nibWithNibName:@"CMQMessageCell" bundle:comBundle] forCellWithReuseIdentifier:contentCellIdentifier];
    [self.collectionView registerNib:[UINib nibWithNibName:@"CMQCreateMessageCell" bundle:comBundle] forCellWithReuseIdentifier:createMessageCellIdentifier];
    [self.collectionView registerNib:[UINib nibWithNibName:@"CMQMessagePackageCell" bundle:comBundle] forCellWithReuseIdentifier:packageCellIdentifier];
    [self.collectionView registerNib:[UINib nibWithNibName:@"CMQMessagePhotoCell" bundle:comBundle] forCellWithReuseIdentifier:photoCellIdentifier];
    //initially set creatingMessage to NO
    self.isCreatingMessage = NO;
    
    //refresh control
    self.refreshControl = [[UIRefreshControl alloc]init];
    [self.collectionView addSubview:self.refreshControl];
    [self.refreshControl addTarget:self action:@selector(onRefresh) forControlEvents:UIControlEventValueChanged];
}

#pragma mark - CMQAPIClient Delegate

- (void)CMQAPIClientDidFetchMessagesSuccessfully:(NSArray *)messages{
    
    self.viewModel.messagesArray = [NSMutableArray arrayWithArray:[[CMQMessageDataSourceManager sharedInstance]sortWithMessages:messages]];
    //[NSMutableArray arrayWithArray:[[CMQMessageDataSourceManager sharedInstance]filterWithMessages:messages]];
    
    [self.refreshControl endRefreshing];
    [self.collectionView reloadData];
    [SVProgressHUD dismiss];
    
    CMQUser *user = [CMQUser currentUser];
    user.messagesUpToDate = true;
    [user saveInBackground];
    if ([CMQUser currentUser].isAdmin) {
        self.scheduledMessageButton.hidden = ![[CMQMessageDataSourceManager sharedInstance] hasPendingScheduledMessageWithMessages:messages];
    }
    [self.alertContainerView setHidden:[self.viewModel.messagesArray count] > 0];
}

- (void)CMQAPIClientDidFailFetchWithMessage:(NSString *)message isCache:(BOOL)isCache{
    [SVProgressHUD showInfoWithStatus:@"Failed to load messages"];
}

-(void)CMQAPIClientDidPostObjectSuccessfully:(PFObject *)object{
    [self deleteCreateMessageCell]; //remove the cell
    CMQMessage* message = (CMQMessage*)object;
    if (!message.scheduleMessage){
        [self insertMessage:(CMQMessage *)object atIndex:0];
    }
    [SVProgressHUD showSuccessWithStatus:@"Message posted!"];
    
    CMQUser *user = [CMQUser currentUser];
    user.messagesUpToDate = true;
    [user saveInBackground];
    
    [self.alertContainerView setHidden:YES];
}

-(void)CMQAPIClientDidFailToPostObject:(PFObject *)object withMessage:(NSString *)message{
    DLogRed(@"failed to post message: %@", message);
    [SVProgressHUD showInfoWithStatus:@"Failed to post message, please try again."];
    [self.alertContainerView setHidden:[self.viewModel.messagesArray count] > 0];
}

#pragma mark - Button Actions

- (IBAction)onBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

/**
 *  inserts a new cell on the top of the view for creating a message. if a cell is already created, it deletes it. this is because the 'create' and 'delete' button are one in the same
 *
 *  @param sender the create/delete button
 */
- (IBAction)onCreateMessage:(id)sender {
    if(self.isCreatingMessage){
        [self deleteCreateMessageCell];
        [self.alertContainerView setHidden:[self.viewModel.messagesArray count] > 0];
    }else{
        [self rotateView:self.createMessageButton radians:M_PI_4];
        
        self.isCreatingMessage = YES;
       // [self.refreshControl removeTarget:self action:@selector(onRefresh) forControlEvents:UIControlEventValueChanged];

        NSIndexPath *indexPath = [NSIndexPath indexPathForItem:0 inSection:0];
        if(self.viewModel.messagesArray.count != 0){
            [self.collectionView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionTop animated:NO];
        }
        [self.collectionView insertItemsAtIndexPaths:@[indexPath]];
        [self.alertContainerView setHidden:YES];
    }
}

-(void)onTogglePushChannel:(id)sender{
    CMQCreateMessageCell *cell = (CMQCreateMessageCell *)[self.collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0]];
    
    //toggle both push channel button states
    cell.allButton.selected = !cell.allButton.selected;
    cell.urgentButton.selected = !cell.urgentButton.selected;
}

/**
 *  triggered from the CMQCreateMessageCell, this creates/posts a new message by calling the CMQAPIClient if all fields have been filled
 *
 *  @param sender the post button
 */
-(void)onPostMessage:(id)sender{
    //get text from cell
    CMQCreateMessageCell *cell = (CMQCreateMessageCell *)[self.collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0]];
    NSString *messageTitle = cell.messageTitleTextField.text;
    NSString *messageContent = cell.messageDetailsTextView.text;
    
    //check if fields are filled
    if([messageTitle isEqualToString:@""] || [messageContent isEqualToString:@""] || [messageContent isEqualToString:@"Message text"]){
        //not all fields have been filled, dont' post message
        [SVProgressHUD showInfoWithStatus:@"All fields must be filled"];

    }else{  //all fields are good, post it
        
        //create new message from cell text..
        newMessage = [[CMQMessage alloc]init];
        newMessage.messageContent = messageContent;
        newMessage.messageTitle = messageTitle;
        newMessage.isCritical = cell.urgentButton.isSelected;
        newMessage.userWhoPosted = [CMQUser currentUser];
        
        if ([CMQUser currentUser].tempAptComplexID && [CMQUser currentUser].tempAptComplexID.length > 0)
            newMessage.aptComplexID = [CMQUser currentUser].tempAptComplexID;
        else
            newMessage.aptComplexID = [CMQUser currentUser].aptComplexID;
        if (_messageImage) {
            NSString* fileName= [NSString stringWithFormat:@"%f", [NSDate dateWithTimeIntervalSinceNow:0].timeIntervalSince1970];
            fileName=[fileName MD5];
            fileName = [fileName stringByAppendingString:@"jpg"];
            CGSize originalSize = _messageImage.size;
            CGSize smallerSize;
            
            if(originalSize.height > originalSize.width){
                smallerSize = CGSizeMake(640.0, 960.0);
            }else{
                smallerSize = CGSizeMake(960.0, 640.0);
            }
            NSData* photoData=
            UIImageJPEGRepresentation([UIImage resizeImage:_messageImage newWidth:smallerSize.width newHeight:smallerSize.height],1.0);
            
            newMessage.messagePhoto=[PFFile fileWithName:fileName data:photoData];
        }
        if (_selectedTime) {
            //set Scheduled Messages stuff
            newMessage.scheduleMessage =YES;
            newMessage.scheduleSent= NO;
            NSDateFormatter* toParseFormat = [[NSDateFormatter alloc]init];
            toParseFormat.dateFormat= @"yyyy-MM-dd'T'HH:mm";
            toParseFormat.timeZone= [NSTimeZone timeZoneWithName:@"UTC"];
            newMessage.scheduleTime= [toParseFormat stringFromDate:_selectedTime];
        }
        [SVProgressHUD showWithStatus:@"Posting message"];
        [[CMQAPIClient sharedClient]postMessage:newMessage];   //post to Parse
    }
}

/**
 *  triggered from the CMQCreateMessageCell, this cancels the creating of a message
 *
 *  @param sender the cancel button
 */
-(void)onCancelPost:(id)sender{
    
    [self deleteCreateMessageCell];
    [self.alertContainerView setHidden:[self.viewModel.messagesArray count] > 0];
    
}

#pragma mark - Internal
/**
 *  removes our instance of CMQCreateMessageCell
 */
-(void)deleteCreateMessageCell{
    [self rotateView:self.createMessageButton radians:0];
    
    self.isCreatingMessage = NO; //toggle bool value
    _messageImage=nil;
    _selectedTime=nil;
    _pendingTitle = nil;
    _pendingMessage = nil;
    _pendingMessageTitleField = nil;
    _pendingMessageView = nil;
    //re[self.refreshControl addTarget:self action:@selector(onRefresh) forControlEvents:UIControlEventValueChanged];
    CMQCreateMessageCell *cell = (CMQCreateMessageCell *)[self.collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0]];
    cell.scheduleLabel.text =@"";
    cell.pendingImageView.image = nil;
    cell.scheduledLabelHeightConstraint.constant= 0;
    cell.imageViewHeightConstraint.constant = 0;
    //delete first index path where our createMessage cell is
    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:0 inSection:0];
    [self.collectionView deleteItemsAtIndexPaths:@[indexPath]];
    
    [cell reset];
    
    self.cellTextView = nil;
}

#pragma Refresh Control
/**
 *  refreshes the view by calling our APIClient
 */
-(void)onRefresh{
    [SVProgressHUD showWithStatus:@"Refreshing messages"];
    [[CMQAPIClient sharedClient]fetchMessages];
}

#pragma mark - UICollectionView Delegate/Datasource

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    //set count based on whether or not we are creating a message. we will have 1 extra cell if we are creating a message.
    NSInteger count = self.isCreatingMessage ? self.viewModel.messagesArray.count + 1 : self.viewModel.messagesArray.count;
    return count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    //check if we're creating a message or not. if so, add it to top of the collectionview
    if(self.isCreatingMessage && indexPath.item == 0){
        CMQCreateMessageCell *cell = (CMQCreateMessageCell *)[collectionView dequeueReusableCellWithReuseIdentifier:createMessageCellIdentifier forIndexPath:indexPath];
        
        //button states
        NSBundle* comBundle = [NSBundle bundleForClass:[CMQMessageViewController class]];
        [cell.postButton setImage:[UIImage imageNamed:@"btn-post-message" inBundle:comBundle compatibleWithTraitCollection:nil] forState:UIControlStateNormal];
        [cell.postButton setImage:[UIImage imageNamed:@"btn-post-message-pressed" inBundle:comBundle compatibleWithTraitCollection:nil] forState:UIControlStateHighlighted];
        [cell.cancelButton setImage:[UIImage imageNamed:@"btn-cancel" inBundle:comBundle compatibleWithTraitCollection:nil] forState:UIControlStateNormal];
        [cell.cancelButton setImage:[UIImage imageNamed:@"btn-cancel-pressed" inBundle:comBundle compatibleWithTraitCollection:nil] forState:UIControlStateHighlighted];

        //button targets
        [cell.postButton addTarget:self action:@selector(onPostMessage:) forControlEvents:UIControlEventTouchUpInside];
        [cell.cancelButton addTarget:self action:@selector(onCancelPost:) forControlEvents:UIControlEventTouchUpInside];
        [cell.allButton addTarget:self action:@selector(onTogglePushChannel:) forControlEvents:UIControlEventTouchUpInside];
        [cell.urgentButton addTarget:self action:@selector(onTogglePushChannel:) forControlEvents:UIControlEventTouchUpInside];
        [cell.photoButton addTarget:self action:@selector(didPressPhotoButton) forControlEvents:UIControlEventTouchUpInside];
        [cell.scheduleButton addTarget:self action:@selector(didPressScheduleButton) forControlEvents:UIControlEventTouchUpInside];
        
        //delegates
        cell.messageDetailsTextView.delegate = self;
        cell.messageTitleTextField.delegate = self;
        _pendingMessageView = cell.messageDetailsTextView;
        _pendingMessageTitleField = cell.messageTitleTextField;
        self.cellTextView = cell.messageDetailsTextView;
        
        if (_messageImage) {
            cell.pendingImageView.image = _messageImage;
            cell.imageViewHeightConstraint.constant = [UIImage getHeightForImageContainerWithWidth:cell.pendingImageView.frame.size.width imageWidth:_messageImage.size.width imageHeight:_messageImage.size.height];
        }
        if (_selectedTime) {
            cell.scheduledLabelHeightConstraint.constant = 16;
            NSDateFormatter* forViewFormat = [[NSDateFormatter alloc]init];
            forViewFormat.dateFormat = @"EEEE, MMM d, yyyy h:mm a";
            cell.scheduleLabel.text = [forViewFormat stringFromDate: _selectedTime];
        }
        if(_pendingMessage){
            cell.messageDetailsTextView.text = _pendingMessage;
            [cell.messageDetailsTextView setTextColor:[UIColor blackColor]];
        }
        if(_pendingTitle){
            cell.messageTitleTextField.text = _pendingTitle;
        }
        return cell;
    }
    CMQMessage* message= nil;
    if (_isCreatingMessage) {
         message =[self.viewModel.messagesArray objectAtIndex:indexPath.row-1];
    }
    else{
        message = [self.viewModel.messagesArray objectAtIndex:indexPath.row];
    }
   
    CMQMessageCell* cell;
    if ([[CMQMessageDataSourceManager sharedInstance] shouldShowPromptWithMessage:message]) {
        cell= (CMQMessageCell*)[collectionView dequeueReusableCellWithReuseIdentifier:packageCellIdentifier forIndexPath:indexPath];
        if ([_shouldShowPrompt containsObject:indexPath]||[message.deliveryStatus.uppercaseString isEqualToString:@"YES"]) {
            cell.deliveryImage.hidden=NO;
            //[cell removeConstraint:cell.packageViewBottomConstaind];
            cell.deliveryImage.image = [[CMQMessageDataSourceManager sharedInstance] deliveryImageWithMessage:message];
            if ([message.deliveryStatus.uppercaseString isEqualToString:@"YES"]) {
                cell.deliveryButton.enabled=NO;
            }
            else{
                cell.deliveryButton.enabled=YES;
            }
        }
        else{
            //[cell addConstraint:cell.packageViewBottomConstaind];
        }
        
    }
    else if (message.messagePhoto){
        cell = (CMQMessageCell*)[collectionView dequeueReusableCellWithReuseIdentifier:photoCellIdentifier forIndexPath:indexPath];
    }
    else{
        cell = (CMQMessageCell *)[collectionView dequeueReusableCellWithReuseIdentifier:contentCellIdentifier forIndexPath:indexPath];
        
    }
    
    //set index based on whether we are creating a message. if we are, decrement our row index to account for the createMessage cell at position 0
    cell.deliveryImage.image = [[CMQMessageDataSourceManager sharedInstance] deliveryImageWithMessage:message];
    cell.deliveryImage.hidden = cell.deliveryImage.image == nil;
    
    NSInteger row = self.isCreatingMessage ? indexPath.item-1 : indexPath.item;
    
    [self configureCell:cell forRow:row];
    
    return cell;
}

/**
 *  configures the collectionview ell
 *
 *  @param cell the cell to configure
 *  @param row  the row where the cell is
 */
-(void)configureCell:(CMQMessageCell *)cell forRow:(NSInteger)row{
    CMQMessage *message = [self.viewModel.messagesArray objectAtIndex:row];
    cell.delegate=self;
    CMQUser *userWhoPosted = (CMQUser*) message.userWhoPosted;//(CMQUser *) message.userWhoPosted.fetchIfNeeded;
    if (message.messagePhoto){
        //fetch our image data on a background thread
        dispatch_queue_t queue = dispatch_get_global_queue (DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
        dispatch_async(queue, ^{
            UIImage *userPhoto = [UIImage getImageFromPFFile:message.messagePhoto];
            //display our image on the main thread
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                //cell.messagePhoto.contentMode = UIViewContentModeScaleToFill;
                cell.messagePhoto.image = userPhoto;
                cell.imageViewHeightConstraint.constant = [UIImage getHeightForImageContainerWithWidth:cell.messagePhoto.frame.size.width imageWidth:userPhoto.size.width imageHeight:userPhoto.size.height];
            });
        });
    }
    if(message.isIndividualMessage || [CMQUser currentUser].isAdmin)
        cell.nameLabel.text = (userWhoPosted.fullName) ? userWhoPosted.fullName : @"Staff Member";
    else
        cell.nameLabel.text = (userWhoPosted.firstName) ? userWhoPosted.firstName : @"Staff Member";
    if (userWhoPosted.userPhoto){
        //fetch our image data on a background thread
        dispatch_queue_t queue = dispatch_get_global_queue (DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
        dispatch_async(queue, ^{
            UIImage *userPhoto = [UIImage getImageFromPFFile:userWhoPosted.userPhoto];
            //display our image on the main thread
            dispatch_async(dispatch_get_main_queue(), ^{
                cell.userPhoto.contentMode = UIViewContentModeScaleAspectFill;
                cell.userPhoto.image = userPhoto;
            });
        });
    }else{
        cell.userPhoto.contentMode = UIViewContentModeTop;
        NSBundle* comBundle = [NSBundle bundleForClass:[CMQMessageViewController class]];
        cell.userPhoto.image = [UIImage imageNamed:@"user-icon" inBundle:comBundle compatibleWithTraitCollection:nil];
    }
    
    //set delegate to self to support tapping URLs
    [cell.messageLabel setDelegate:self];
    //get message
    cell.dateLabel.text = [self.viewModel getDateForRow:row];
    cell.messageLabel.text = [self.viewModel getMessageForRow:row];
    [self.viewModel getLabelForRow:row withCompletion:^(NSString *string, NSError *error) {
        if(!error){
            cell.recipientLabel.text=string;
        }
        else{
            NSLog(@"Unable to get Label");
            cell.recipientLabel.hidden=YES;
        }
    }];
    NSString* messageLabelContent = [[self.viewModel getMessageForRow:row] string];
    
    //pick up any links present in message content
    NSDataDetector *linkDetector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypeLink error:nil];
    NSArray *matches = [linkDetector matchesInString:messageLabelContent options:0 range:NSMakeRange(0, [messageLabelContent length])];
    for (NSTextCheckingResult *match in matches) {
        if ([match resultType] == NSTextCheckingTypeLink) {
            NSURL *url = [match URL];
            [cell.messageLabel addLinkToURL:url withRange:[match range]];
        }
    }
}

- (void)attributedLabel:(TTTAttributedLabel *)label didSelectLinkWithURL:(NSURL *)url {
    NSString *urlString = [url absoluteString];
    
    [self showWebViewWithURL:[NSURL URLWithString:urlString]
             backgroundColor:[UIColor CMQRedColor]
                   tintColor:[UIColor blackColor]
                       title:@""];
}

/**
 *  shows an instance of DZNWebViewController with the given URL
 *
 *  @param url the url to show in the web view
 */
-(void)showWebViewWithURL:(NSURL *)url backgroundColor:(UIColor *)bgColor tintColor:(UIColor *)tintColor title:(NSString *)title{
    //create our web view controller
    
    //check url for validity
    if ([[url absoluteString].lowercaseString hasPrefix:@"http"])
        self.webViewController = [[DZNWebViewController alloc]initWithURL:url];
    else
        self.webViewController = [[DZNWebViewController alloc]initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://%@", [url absoluteString]]]];
//    
//    self.webViewController.toolbarTintColor = tintColor;
//    self.webViewController.toolbarBackgroundColor = bgColor;
//    self.webViewController.supportedActions = DZNWebViewControllerActionNone;
//    self.webViewController.titleColor = tintColor;
    self.webViewController.supportedWebNavigationTools = DZNWebNavigationToolAll;
    self.webViewController.supportedWebActions = DZNWebActionAll;
    self.webViewController.showLoadingProgress = YES;
    self.webViewController.allowHistory = YES;
    self.webViewController.hideBarsWithGestures = YES;
    
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithTitle:@"<" style:UIBarButtonItemStylePlain target:self action:@selector(dismissWebViewController:)];
    barButton.tintColor = [UIColor blackColor];
    [barButton setTitleTextAttributes:@{NSFontAttributeName: [UIFont fontWithName:kCMQFontGeometriaLight size:19] } forState:UIControlStateNormal];
    self.webViewController.navigationItem.leftBarButtonItem = barButton;
    
    UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:self.webViewController];
//    navController.navigationBar.barTintColor = bgColor;
    
    [self presentViewController:navController animated:YES completion:nil];
}

/**
 *  called from our web view's navigation controller's bar button item
 *
 *  @param sender the bar button item to dismiss the webview
 */
- (void)dismissWebViewController:(id)sender{
    [_webViewController dismissViewControllerAnimated:YES completion:^{
        _webViewController = nil;
    }];
}

/**
 *  inserts a new message into our date source and the collection view at the given index
 *
 *  @param message the message to insert
 *  @param index   the index at which it needs entering
 */
-(void)insertMessage:(CMQMessage *)message atIndex:(NSInteger)index{
    //update our collectionview datasource
    [self.viewModel insertObject:message inMessagesArrayAtIndex:index];
    //[self.viewModel.messagesArray insertObject:message atIndex:index];
    
    
    //insert our new cell after a half second. This is because we are still deleting the "CMQCreateMessageCell" when we are also inserting the new one. It's just a visual thing really.
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        //insert a new cell at the top of the collection view
        NSIndexPath *indexPath = [NSIndexPath indexPathForItem:index inSection:0];
        
        [self.collectionView insertItemsAtIndexPaths:@[indexPath]];
        if(self.viewModel.messagesArray.count != 0){
            [self.collectionView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionTop animated:NO];
        }
    });
}



#pragma mark - UICollectionViewFlowLayout

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    //return size of createMessageCell
    if(self.isCreatingMessage && indexPath.item == 0){
        CGFloat cellHeight = 325;
        if (_selectedTime) {
            cellHeight+=25;
        }
        if (_messageImage) {
            cellHeight+=[UIImage getHeightForImageContainerWithWidth:self.collectionView.frame.size.width - 20 imageWidth:_messageImage.size.width imageHeight:_messageImage.size.height];
        }
        return CGSizeMake(self.collectionView.frame.size.width - 20, cellHeight);
    }
    CGFloat cellHeight = DEFAULT_CELL_HEIGHT;
    
    //set index based on whether we are creating a message. if we are, decrement our row index to account for the createMessage cell at position 0
    NSInteger row = self.isCreatingMessage ? indexPath.item-1 : indexPath.item;
    
    //get the size the message post text so we can set our cell height accordingly
    cellHeight += [[self.viewModel getMessageForRow:row] boundingRectWithSize:CGSizeMake(self.collectionView.frame.size.width - 30, CGFLOAT_MAX)
                                                     options:(NSStringDrawingUsesLineFragmentOrigin)|(NSStringDrawingUsesFontLeading)
                                                     context:nil].size.height;
    if (!_isCreatingMessage) {
        CMQMessage* message=[self.viewModel.messagesArray objectAtIndex:indexPath.row];
        if ([[CMQMessageDataSourceManager sharedInstance] shouldShowPromptWithMessage:message]) {
            if (![_shouldShowPrompt containsObject:indexPath]) {
                cellHeight+=120;
               // cell.packageView.hidden=NO;
            }
            else{
                //cell.packageView.hidden=YES;
            }
        }
        else if(message.messagePhoto){
            if ([[CMQMessageDataSourceManager sharedInstance].messagePhotoCache objectForKey:message.objectId]) {
                UIImage* messagePhoto = [[CMQMessageDataSourceManager sharedInstance].messagePhotoCache objectForKey:message.objectId];
                cellHeight+=[UIImage getHeightForImageContainerWithWidth:self.collectionView.frame.size.width - 20 imageWidth:messagePhoto.size.width imageHeight:messagePhoto.size.height];
            }else{
                UIImage* messagePhoto= [UIImage getImageFromPFFile:message.messagePhoto];
                NSMutableDictionary* dictionary = [[NSMutableDictionary alloc]initWithDictionary:[CMQMessageDataSourceManager sharedInstance].messagePhotoCache];
                [dictionary setValue:messagePhoto forKey:message.objectId];
                [CMQMessageDataSourceManager sharedInstance].messagePhotoCache = dictionary;
                cellHeight+=[UIImage getHeightForImageContainerWithWidth:self.collectionView.frame.size.width - 20 imageWidth:messagePhoto.size.width imageHeight:messagePhoto.size.height];
            }
        }
    }
    else{
        if (indexPath.row>0) {
            CMQMessage* message = [self.viewModel.messagesArray objectAtIndex:indexPath.row-1];
            if (message.messagePhoto) {
                if ([[CMQMessageDataSourceManager sharedInstance].messagePhotoCache objectForKey:message.objectId]) {
                    UIImage* messagePhoto = [[CMQMessageDataSourceManager sharedInstance].messagePhotoCache objectForKey:message.objectId];
                    cellHeight+=[UIImage getHeightForImageContainerWithWidth:self.collectionView.frame.size.width - 20 imageWidth:messagePhoto.size.width imageHeight:messagePhoto.size.height];
                }else{
                    UIImage* messagePhoto= [UIImage getImageFromPFFile:message.messagePhoto];
                    NSMutableDictionary* dictionary = [[NSMutableDictionary alloc]initWithDictionary:[CMQMessageDataSourceManager sharedInstance].messagePhotoCache];
                    [dictionary setValue:messagePhoto forKey:message.objectId];
                    [CMQMessageDataSourceManager sharedInstance].messagePhotoCache = dictionary;
                    cellHeight+=[UIImage getHeightForImageContainerWithWidth:self.collectionView.frame.size.width - 20 imageWidth:messagePhoto.size.width imageHeight:messagePhoto.size.height];
                }
            }
        }
    }
    return CGSizeMake(self.collectionView.frame.size.width - 20, cellHeight);
}

#pragma mark - UIView animations
/**
 *  rotates a view by the given degrees
 *
 *  @param view     the view to rotate
 *  @param rotation the degress at which we rotate
 */
-(void)rotateView:(UIView *)view radians:(CGFloat)rotation{
    [UIView animateWithDuration:0.25 animations:^{
        view.transform = CGAffineTransformMakeRotation(rotation);
    }];
}

#pragma mark - UITextFieldDelegate

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self.cellTextView becomeFirstResponder];
    return NO;
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    _pendingTitle = [textField.text stringByAppendingString:string];
    return YES;
}
#pragma mark - UITextViewDelegate

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    if ([text isEqualToString:@"\n"]){
        [textView resignFirstResponder];
        return NO;
    }
    return YES;
}
-(void)textViewDidChange:(UITextView *)textView{
    _pendingMessage = textView.text;
}
-(void)textViewDidBeginEditing:(UITextView *)textView{
    if([textView.text isEqualToString:@"Message text"])textView.text = @"";
    [textView setTextColor:[UIColor blackColor]];
}

-(void)textViewDidEndEditing:(UITextView *)textView{
    if([textView.text isEqualToString:@""]){
        [textView setTextColor:[UIColor lightGrayColor]];
        textView.text = @"Message text";
    }
}
-(IBAction)didPressScheduleMessageButton:(id)sender{
    self.viewModel.shouldShowUnsent = !self.viewModel.shouldShowUnsent;
    
    [self.collectionView reloadData];
    
}
-(IBAction)deliveryConfirmation:(UIStoryboardSegue*)sender{
    CMQPackageDeliveryConfirmationViewController* source = (CMQPackageDeliveryConfirmationViewController*)sender.sourceViewController;
    [self updateMessageWithDeliveryResponse:source.isConfirmed];
}

- (void)cell:(CMQMessageCell *)cell receivedResponse:(BOOL)deliveryResponse {
    _responseMessage = [self.viewModel.messagesArray objectAtIndex:[self.collectionView indexPathForCell:cell].row];
    _responseCell = cell;
    if (deliveryResponse) {
        [self showPackageConfirmation];
    }
    else{
        [self updateMessageWithDeliveryResponse:deliveryResponse];
    }
}
-(void)showPackageConfirmation{
    NSBundle* comBundle = [NSBundle bundleForClass:[CMQMessageViewController class]];
    UIStoryboard* messagesStoryBoard = [UIStoryboard storyboardWithName:@"Messages" bundle:comBundle];
    CMQPackageDeliveryConfirmationViewController* controller = [messagesStoryBoard instantiateViewControllerWithIdentifier:@"Confirmation"];
    controller.transitioningDelegate = self;
    controller.modalPresentationStyle = UIModalPresentationCustom;
    [self presentViewController:controller animated:YES completion:nil];
    
}
-(void)updateMessageWithDeliveryResponse:(BOOL)deliveryResponse{
    /*[[CMQMessageDataSourceManager sharedInstance]setDeliveryWithMessage:_responseMessage response:deliveryResponse completion:^(NSError * _Nullable error) {
        if (!error) {
            [self deliveryPressedByCell:_responseCell];
            //[self.collectionView reloadData];
            [SVProgressHUD showSuccessWithStatus:deliveryResponse?@"Package will Be delivered":@"Package will not be delivered"];
            _responseCell = nil;
            _responseMessage = nil;
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
            });
        }
        else{
            [SVProgressHUD showErrorWithStatus:@"Unable to update package. Please try again later"];
            _responseCell = nil;
            _responseMessage = nil;
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
            });
        }
    }];*/
    
}
-(void)deliveryPressedByCell:(CMQMessageCell *)cell{
    NSIndexPath* path= [self.collectionView indexPathForCell:cell];
    CMQMessage* message=[self.viewModel.messagesArray objectAtIndex:path.row];
   // if (![message.deliveryStatus isEqualToString:@"PENDING"]) {
    if (path) {
        if ([_shouldShowPrompt containsObject:path]) {
            [_shouldShowPrompt removeObject:path];
            //cell.packageView.hidden=NO;
            //[cell addConstraint:cell.packageViewBottomConstaint];
            
        }
        else{
            [_shouldShowPrompt addObject:path];
            //cell.packageView.hidden=YES;
            //[cell removeConstraint:cell.packageViewBottomConstaint];
            
        }
        [self.collectionView reloadItemsAtIndexPaths:@[path]];
    }
    
 //   }
    
    
}
-(UIPresentationController*)presentationControllerForPresentedViewController:(UIViewController *)presented presentingViewController:(UIViewController *)presenting sourceViewController:(UIViewController *)source{
    CMQPackageCheckOutPinPresentationController* controller=[[CMQPackageCheckOutPinPresentationController alloc]initWithPresentedViewController:presented presentingViewController:presenting];
    if ([presented isKindOfClass:[CMQPackageDeliveryConfirmationViewController class]]){
        controller.contentSize = [(CMQPackageDeliveryConfirmationViewController*)presented preferredSize];
        controller.isDimmingViewTappable = YES;
        
    }else if([presented isKindOfClass:[CMQScheduledMessageViewController class]]){
        controller.contentSize = [(CMQScheduledMessageViewController*)presented preferredSize];
    }
    
    return controller;
}
-(IBAction)cancelScheduledMessage:(UIStoryboardSegue*)sender{
    
    
}
-(IBAction)scheduleMessage:(UIStoryboardSegue*)sender{
    CMQScheduledMessageViewController* source = (CMQScheduledMessageViewController*) sender.sourceViewController;
    NSDateFormatter* fromSelectionFormat = [[NSDateFormatter alloc]init];
    fromSelectionFormat.dateFormat = @"MM-dd-yyyy h:mm a";
    fromSelectionFormat.timeZone = [NSTimeZone systemTimeZone];
    
    self.selectedTime = [fromSelectionFormat dateFromString:[NSString stringWithFormat:@"%@ %@",source.selectedDayString, source.selectedTimeString]];
    [self.collectionView reloadItemsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:0]]];
    
    
}
-(void)didPressScheduleButton{
    if([_pendingMessageTitleField isFirstResponder]){
        [_pendingMessageTitleField resignFirstResponder];
    }
    if ([_pendingMessageView isFirstResponder]) {
        [_pendingMessageView resignFirstResponder];
    }
    NSBundle* comBundle = [NSBundle bundleForClass:[CMQScheduledMessageViewController class]];
    CMQScheduledMessageViewController* view=[[UIStoryboard storyboardWithName:@"Messages" bundle:comBundle]instantiateViewControllerWithIdentifier:@"ScheduledMessaging"];
    if (_selectedTime) {
        NSDateFormatter* timeFormat=[[NSDateFormatter alloc]init];
        timeFormat.dateFormat = @"h:mm a";
        view.selectedTimeString=[timeFormat stringFromDate:_selectedTime];
        timeFormat.dateFormat=@"MM-dd-yyyy";
        view.selectedDayString=[timeFormat stringFromDate:_selectedTime];
    }
    view.transitioningDelegate=self;
    view.modalPresentationStyle=UIModalPresentationCustom;
    
    [self presentViewController:view animated:YES completion:nil];
    
    
}
-(void)didPressPhotoButton{
    if([_pendingMessageTitleField isFirstResponder]){
        [_pendingMessageTitleField resignFirstResponder];
    }
    if ([_pendingMessageView isFirstResponder]) {
        [_pendingMessageView resignFirstResponder];
    }
    UIAlertController* controller = [UIAlertController alertControllerWithTitle:@"Send Image" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction* pickerAction=[UIAlertAction actionWithTitle:@"Photo Library" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self showPhotoPickerController];
    }];
    UIAlertAction* cameraAction= [UIAlertAction actionWithTitle:@"Take Photo" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self showCameraController];
    }];
    UIAlertAction* cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    [controller addAction:cameraAction];
    [controller addAction:pickerAction];
    [controller addAction:cancelAction];
    [self presentViewController:controller animated:YES completion:nil];
    
}
-(void)showPhotoPickerController{
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
        UIImagePickerController* controller=[[UIImagePickerController alloc]init];
        controller.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
        //controller.mediaTypes
        controller.delegate=self;
        controller.allowsEditing=NO;
        [self presentViewController:controller animated:YES completion:nil];
    }
    
}
-(void)showCameraController{
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        UIImagePickerController* controller=[[UIImagePickerController alloc]init];
        controller.sourceType=UIImagePickerControllerSourceTypeCamera;
        controller.delegate=self;
        controller.allowsEditing=NO;
        [self presentViewController:controller animated:YES completion:nil];
    }
    
}
-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    
    [self dismissViewControllerAnimated:YES completion:nil];
}
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info{
    UIImage* image=info[UIImagePickerControllerOriginalImage];
    [self dismissViewControllerAnimated:YES completion:^{
        //set reload cell after setting photo.
        if (image) {
            _messageImage=image;
            [self.collectionView reloadItemsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:0]]];
            
        }
    }];
    
    
}
@end
