//
//  CMQNoticesViewModel.h
//  Communique
//
//  Created by Chris Hetem on 9/17/14.
//  Copyright (c) 2014 WaveRider, Inc. All rights reserved.
//

//@import Foundation;
#import <Foundation/Foundation.h>

@interface CMQMessageViewModel : NSObject

/**
 *  all the messages
 */
@property (strong, nonatomic) NSMutableArray *messagesArray;
@property (assign, nonatomic)BOOL shouldShowUnsent;

/**
 *  returns a message at the given row
 *
 *  @param row the row or index
 *
 *  @return the message at the row
 */
-(NSMutableAttributedString *)getMessageForRow:(NSInteger)row;

/**
 *  returns string of userWhoPosted at the given row
 *
 *  @param row the row or index
 *
 *  @return the name of userWhoPosted at the row
 */
-(NSString *)getLabelForRow:(NSInteger)row;

/**
 *  formats a date for the given row
 *
 *  @param row the row or index
 *
 *  @return the attributed string formatted specifically for the messages' cell
 */
-(NSString *)getDateForRow:(NSInteger)row;
-(void)insertObject:(CMQMessage *)object inMessagesArrayAtIndex:(NSUInteger)index;
-(void)getLabelForRow:(NSInteger)row withCompletion:(void(^)(NSString* string, NSError* error))completion;
@end
