//
//  CMQNoticesCell.m
//  Communique
//
//  Created by Chris Hetem on 9/17/14.
//  Copyright (c) 2014 WaveRider, Inc. All rights reserved.
//

#import "CMQMessageCell.h"
@implementation CMQMessageCell

- (void)awakeFromNib{
    [super awakeFromNib];
    
    // Initialization code
    [self.messageLabel setVerticalAlignment:TTTAttributedLabelVerticalAlignmentTop];
    [self.messageLabel setTextColor:[UIColor blackColor]];
    
    [self.dateLabel setFont:[UIFont fontWithName:kCMQFontGeometriaLight size:10]];
    [self.dateLabel setTextColor:[UIColor blackColor]];
    
    [self.recipientLabel setFont:[UIFont fontWithName:kCMQFontGeometriaLight size:11]];
    [self.recipientLabel setVerticalAlignment:TTTAttributedLabelVerticalAlignmentCenter];
    [self.recipientLabel setTextColor:[UIColor blackColor]];
    
    [self.nameLabel setFont:[UIFont fontWithName:kCMQFontGeometriaLight size:17]];
    
    self.userPhoto.layer.cornerRadius = 20.0f;
    self.userPhoto.layer.borderWidth = .5f;
    self.userPhoto.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.userPhoto.layer.masksToBounds = YES;
}
- (IBAction)deliverPackage:(id)sender {
    [self.delegate cell:self receivedResponse:YES];
}
- (IBAction)declineDelivery:(id)sender {
    [self.delegate cell:self receivedResponse:NO];
}
-(IBAction)deliveryPressed:(id)sender{
    [self.delegate deliveryPressedByCell:self];
}

@end
