//
//  CMQMessageDataSourceManager.swift
//  Communique
//
//  Created by Andre White on 10/19/17.
//  Copyright © 2017 Communique, LLC. All rights reserved.
//

import Foundation
import Parse
import UserNotifications

@objc public class CMQMessageDataSourceManager: NSObject{
    public var hasPackageDelivery: Bool{
        return CMQAPIManager.sharedInstance().apartment.hasPackageDelivery
    }
    public var messages: Array<CMQMessage>?
    @objc public var shouldShowUnSentScheduledMessages: Bool
    public var UNNotificationCategoryIdentifier = "packageNoti"
    @objc public var messagePhotoCache : [String:UIImage] = Dictionary()
    
    @objc public class func sharedInstance()->CMQMessageDataSourceManager{
        return .sharedManager
    }
    
    private static let sharedManager:CMQMessageDataSourceManager = CMQMessageDataSourceManager()
    
    private override init() {
        self.shouldShowUnSentScheduledMessages = false
        super.init()
    }
    /**
     Method used to get the Message Category for notifications. This method builds the actions for the delivery notification that should then be used with the UNNotificationCenter's setNotificationCategory method. REQUIRES IOS 10
     - returns : UNNotificationCategory object to be used with UNNotificationCenter SetNotificationCategory method.
 
 */
    
    @objc @available(iOS 10.0, *)
    public func messageCategory()->UNNotificationCategory{
        let deliverAction = UNNotificationAction(identifier: "Deliver", title: "Deliver my package", options: UNNotificationActionOptions(rawValue:0))
        let dontDeliverAction = UNNotificationAction(identifier: "DontDeliver", title: "I'll pick it up", options:
            UNNotificationActionOptions(rawValue:0))
        return UNNotificationCategory(identifier: UNNotificationCategoryIdentifier, actions: [deliverAction, dontDeliverAction], intentIdentifiers: [], options: UNNotificationCategoryOptions(rawValue: 0))
    }
    // MARK: Get Methods
    /**
     Helper Method to get data from Parse Server. This is template function that takes any closure that can return a PFQuery and a second closure that can handle both the data and the error
     
     - paramter queryClosure: closure that returns a PFQuery Instance with the query parameters already set.
     - parameter completionClosure: closure that handles both the returned data from the query and an error if any.
     - parameter data: Array of objects returned from query
     - paramter error: Error returned by query
     */
    func getDataFromServer(queryClosure:()->PFQuery<PFObject>, completionClosure:@escaping (_ data:Array<Any>?, _ error:Error?)->Void){
        let query=queryClosure();
        query.findObjectsInBackground { (objects, error) in
            guard let possibleObjects=objects else{
                completionClosure(nil,error)
                return
            }
            completionClosure(possibleObjects, nil)
        }
    }
    /**
     Method to get all Messages from Parse Server.
     - parameter completion: the callback block which is called after completion or in the event that an error occurred.
     - parameter error: error if one occured, nil if not.
     
 
 */
    func getMessages(completion:@escaping (_ error:Error?)->Void)->Void{
        let aQueryClosure=self.getMessageQuery
        let aCompletionClosure = {(objects:Array<Any>?, error:Error?)->Void in
            guard let possibleObjects = objects else{
                completion(error)
                return
            }
            self.messages = possibleObjects as? Array<CMQMessage>
            completion(nil)
        }
        self.getDataFromServer(queryClosure: aQueryClosure, completionClosure: aCompletionClosure)
    }
    /**
     Method to build message Query. UNFINISHED AND UNUSED. REFER TO CMQAPIClient.fetchMessages() FOR COMPLETE QUERY
     returns: PFQuery object with needed parameters to get all messages.
 
 */
    @objc public func messageQuery(communityID:String)->PFQuery<PFObject>{
        var query = PFQuery()
        if !CMQUser.current().isInterestedParty {
            if CMQUser.current().isResident{
                //Resident - restrict viewing
                var queries = [PFQuery]()
                //Direct Message (Current user)
                let directMessageQuery = CMQMessage.query()
                directMessageQuery?.whereKey(kParseAptComplexIDKey, equalTo: communityID)
                directMessageQuery?.whereKey("isIndividualMessage", equalTo: NSNumber.init(value: true))
                directMessageQuery?.whereKey("recipients", equalTo: CMQUser.current().objectId!)
                queries.append(directMessageQuery!)
                
                //Messages Directly to a Building
                let messageQuery = CMQMessage.query()
                messageQuery?.whereKey(kParseAptComplexIDKey, equalTo: communityID)
                messageQuery?.whereKey("isIndividualMessage", notEqualTo: NSNumber.init(value: true))
                messageQuery?.whereKey("recipients", contains: CMQUser.current().buildingUnit)
                queries.append(messageQuery!)
                
                
                //Messages with specific recipients (no groups)
                let specificMessageQuery = CMQMessage.query()
                specificMessageQuery?.whereKey(kParseAptComplexIDKey, equalTo: communityID)
                specificMessageQuery?.whereKey("recipients", equalTo: CMQUser.current().buildingUnit)
                specificMessageQuery?.whereKeyDoesNotExist("groups")
                queries.append(directMessageQuery!)
                
                // Messages without specified recipients (no groups)
                let nonSpecificMessageQuery = CMQMessage.query()
                nonSpecificMessageQuery?.whereKey(kParseAptComplexIDKey, equalTo: communityID)
                nonSpecificMessageQuery?.whereKeyDoesNotExist("recipients")
                nonSpecificMessageQuery?.whereKeyDoesNotExist("groups")
                queries.append(nonSpecificMessageQuery!)
                
                //Announcements without specified groups
                let announcementQuery = CMQMessage.query()
                announcementQuery?.whereKey("isAnnouncement", equalTo: NSNumber.init(value: true))
                //announcementQuery?.whereKey("recipients", contains: communityID)
                announcementQuery?.whereKey("recipients", equalTo: communityID)
                //announcementQuery?.whereKey("recipients", containsAllObjectsIn: [communityID])
                announcementQuery?.whereKeyDoesNotExist("groups")
                
                queries.append(announcementQuery!)
                
                if let groups = CMQUser.current().groups{
                    //Message with specific recipients (with groups)
                    let specificMessageQueryWithGroups = CMQMessage.query()
                    specificMessageQueryWithGroups?.whereKey(kParseAptComplexIDKey, equalTo: communityID)
                    specificMessageQueryWithGroups?.whereKey("recipients", equalTo: CMQUser.current().buildingUnit)
                    specificMessageQueryWithGroups?.whereKey("groups", containedIn: groups)
                    queries.append(specificMessageQueryWithGroups!)
                    
                    // Messages without specified recipients (with groups)
                    let nonSpecificMessageQueryWithGroups = CMQMessage.query()
                    nonSpecificMessageQueryWithGroups?.whereKey(kParseAptComplexIDKey, equalTo: communityID)
                    nonSpecificMessageQueryWithGroups?.whereKeyDoesNotExist("recipients")
                    nonSpecificMessageQueryWithGroups?.whereKey("groups", containedIn: groups)
                    queries.append(nonSpecificMessageQueryWithGroups!)
                    
                    //Announcements with specific groups
                    let announcementQueryWithGroup = CMQMessage.query()
                    announcementQueryWithGroup?.whereKey("isAnnouncement", equalTo: NSNumber.init(value: true))
                    announcementQueryWithGroup?.whereKey("groups", containedIn: groups)
                    queries.append(announcementQueryWithGroup!)
                }
                query = PFQuery.orQuery(withSubqueries: queries)
            }
            else{
                // Staff/Admin - ignore buildingUnit and groups (exclude DMs)
                // Direct Message (current user)
                let directMessageQuery = CMQMessage.query()
                directMessageQuery?.whereKey(kParseAptComplexIDKey, equalTo: communityID)
                directMessageQuery?.whereKey("isIndividualMessage", equalTo: NSNumber.init(value: true))
                directMessageQuery?.whereKey("userWhoPosted", equalTo: CMQUser.current())
                directMessageQuery?.whereKeyDoesNotExist("Package")
                
                //Messages to this community
                let messageQuery = CMQMessage.query()
                messageQuery?.whereKey(kParseAptComplexIDKey, equalTo: communityID)
                messageQuery?.whereKey("isIndividualMessage", notEqualTo: NSNumber.init(value: true))
                
                //Announcements to this community
                let announcementQuery = CMQMessage.query()
                announcementQuery?.whereKey("isAnnouncement", equalTo: NSNumber.init(value: true))
                announcementQuery?.whereKey("isIndividualMessage", notEqualTo: NSNumber.init(value: true))
                announcementQuery?.whereKey("recipients", containedIn: [communityID])
                query = PFQuery.orQuery(withSubqueries: [directMessageQuery!,messageQuery!,announcementQuery!])
            }
        }
        else if CMQUser.current().isInterestedParty{
            var queries = Array<PFQuery>.init()
            if let associatedUsers = CMQAPIManager.sharedInstance().associatedUsers{
                let buildingUnits = associatedUsers.compactMap({$0.buildingUnit})
                //Public Message with specific recipients
                let specificMessageQuery = CMQMessage.query()!
                specificMessageQuery.whereKey(kParseAptComplexIDKey, equalTo: communityID)
                specificMessageQuery.whereKey("recipients", containedIn: buildingUnits)
                specificMessageQuery.whereKey("isPrivate", equalTo: NSNumber.init(value: false))
                queries.append(specificMessageQuery)
            }
            
            //Public Messages without specific recipients
            let messageQuery = CMQMessage.query()
            messageQuery?.whereKey(kParseAptComplexIDKey, equalTo: communityID)
            messageQuery?.whereKeyDoesNotExist("recipients")
            messageQuery?.whereKey("isPrivate", equalTo: NSNumber.init(value: false))
            queries.append(messageQuery!)
            
            query = PFQuery.orQuery(withSubqueries: queries)
        }
        query.whereKey("scheduleSent", notEqualTo: false)
        if CMQUser.current().isAdmin{
            query.whereKey("messageTitle", notEqualTo: "Package Reminder")
        }
        return query
    }
    @objc public func messageQuery(communityIDs:[String])->PFQuery<PFObject>{
        var query = PFQuery()
        if !CMQUser.current().isInterestedParty {
            if CMQUser.current().isResident{
                //Resident - restrict viewing
                var queries = [PFQuery]()
                //Direct Message (Current user)
                let directMessageQuery = CMQMessage.query()
                directMessageQuery?.whereKey(kParseAptComplexIDKey, containedIn: communityIDs)
                //directMessageQuery?.whereKey(kParseAptComplexIDKey, equalTo: communityID)
                directMessageQuery?.whereKey("isIndividualMessage", equalTo: NSNumber.init(value: true))
                directMessageQuery?.whereKey("recipients", equalTo: CMQUser.current().objectId!)
                queries.append(directMessageQuery!)
                
                //Messages Directly to a Building
                let messageQuery = CMQMessage.query()
                messageQuery?.whereKey(kParseAptComplexIDKey, containedIn: communityIDs)
                //messageQuery?.whereKey(kParseAptComplexIDKey, equalTo: communityID)
                messageQuery?.whereKey("isIndividualMessage", notEqualTo: NSNumber.init(value: true))
                messageQuery?.whereKey("recipients", contains: CMQUser.current().buildingUnit)
                queries.append(messageQuery!)
                
                
                //Messages with specific recipients (no groups)
                let specificMessageQuery = CMQMessage.query()
                specificMessageQuery?.whereKey(kParseAptComplexIDKey, containedIn: communityIDs)
                //specificMessageQuery?.whereKey(kParseAptComplexIDKey, equalTo: communityID)
                specificMessageQuery?.whereKey("recipients", equalTo: CMQUser.current().buildingUnit)
                specificMessageQuery?.whereKeyDoesNotExist("groups")
                queries.append(directMessageQuery!)
                
                // Messages without specified recipients (no groups)
                let nonSpecificMessageQuery = CMQMessage.query()
                nonSpecificMessageQuery?.whereKey(kParseAptComplexIDKey, containedIn: communityIDs)
                //nonSpecificMessageQuery?.whereKey(kParseAptComplexIDKey, equalTo: communityID)
                nonSpecificMessageQuery?.whereKeyDoesNotExist("recipients")
                nonSpecificMessageQuery?.whereKeyDoesNotExist("groups")
                queries.append(nonSpecificMessageQuery!)
                
                //Announcements without specified groups
                let announcementQuery = CMQMessage.query()
                announcementQuery?.whereKey("isAnnouncement", equalTo: NSNumber.init(value: true))
                //announcementQuery?.whereKey("recipients", contains: communityID)
                announcementQuery?.whereKey("recipients", containedIn: communityIDs)
                //announcementQuery?.whereKey("recipients", equalTo: communityID)
                //announcementQuery?.whereKey("recipients", containsAllObjectsIn: [communityID])
                announcementQuery?.whereKeyDoesNotExist("groups")
                
                queries.append(announcementQuery!)
                
                if let groups = CMQUser.current().groups{
                    //Message with specific recipients (with groups)
                    let specificMessageQueryWithGroups = CMQMessage.query()
                    specificMessageQueryWithGroups?.whereKey(kParseAptComplexIDKey, containedIn: communityIDs)
                    //specificMessageQueryWithGroups?.whereKey(kParseAptComplexIDKey, equalTo: communityID)
                    specificMessageQueryWithGroups?.whereKey("recipients", equalTo: CMQUser.current().buildingUnit)
                    specificMessageQueryWithGroups?.whereKey("groups", containedIn: groups)
                    queries.append(specificMessageQueryWithGroups!)
                    
                    // Messages without specified recipients (with groups)
                    let nonSpecificMessageQueryWithGroups = CMQMessage.query()
                    //nonSpecificMessageQueryWithGroups?.whereKey(kParseAptComplexIDKey, equalTo: communityID)
                    nonSpecificMessageQueryWithGroups?.whereKey(kParseAptComplexIDKey, containedIn: communityIDs)
                    nonSpecificMessageQueryWithGroups?.whereKeyDoesNotExist("recipients")
                    nonSpecificMessageQueryWithGroups?.whereKey("groups", containedIn: groups)
                    queries.append(nonSpecificMessageQueryWithGroups!)
                    
                    //Announcements with specific groups
                    let announcementQueryWithGroup = CMQMessage.query()
                    announcementQueryWithGroup?.whereKey("isAnnouncement", equalTo: NSNumber.init(value: true))
                    announcementQueryWithGroup?.whereKey("groups", containedIn: groups)
                    queries.append(announcementQueryWithGroup!)
                }
                query = PFQuery.orQuery(withSubqueries: queries)
            }
            else{
                // Staff/Admin - ignore buildingUnit and groups (exclude DMs)
                // Direct Message (current user)
                let directMessageQuery = CMQMessage.query()
                directMessageQuery?.whereKey(kParseAptComplexIDKey, containedIn: communityIDs)
                //directMessageQuery?.whereKey(kParseAptComplexIDKey, equalTo: communityID)
                directMessageQuery?.whereKey("isIndividualMessage", equalTo: NSNumber.init(value: true))
                directMessageQuery?.whereKey("userWhoPosted", equalTo: CMQUser.current())
                directMessageQuery?.whereKeyDoesNotExist("Package")
                
                //Messages to this community
                let messageQuery = CMQMessage.query()
                messageQuery?.whereKey(kParseAptComplexIDKey, containedIn: communityIDs)
                //messageQuery?.whereKey(kParseAptComplexIDKey, equalTo: communityID)
                messageQuery?.whereKey("isIndividualMessage", notEqualTo: NSNumber.init(value: true))
                
                //Announcements to this community
                let announcementQuery = CMQMessage.query()
                announcementQuery?.whereKey("isAnnouncement", equalTo: NSNumber.init(value: true))
                announcementQuery?.whereKey("isIndividualMessage", notEqualTo: NSNumber.init(value: true))
                announcementQuery?.whereKey("recipients", containedIn: communityIDs)
                //announcementQuery?.whereKey("recipients", containedIn: communityID)
                query = PFQuery.orQuery(withSubqueries: [directMessageQuery!,messageQuery!,announcementQuery!])
            }
        }
        else if CMQUser.current().isInterestedParty{
            var queries = Array<PFQuery>.init()
            if let associatedUsers = CMQAPIManager.sharedInstance().associatedUsers{
                let buildingUnits = associatedUsers.compactMap({$0.buildingUnit})
                //Public Message with specific recipients
                let specificMessageQuery = CMQMessage.query()!
                specificMessageQuery.whereKey(kParseAptComplexIDKey, containedIn: communityIDs)
                //specificMessageQuery.whereKey(kParseAptComplexIDKey, equalTo: communityID)
                specificMessageQuery.whereKey("recipients", containedIn: buildingUnits)
                specificMessageQuery.whereKey("isPrivate", equalTo: NSNumber.init(value: false))
                queries.append(specificMessageQuery)
            }
            
            //Public Messages without specific recipients
            let messageQuery = CMQMessage.query()
            messageQuery?.whereKey(kParseAptComplexIDKey, containedIn: communityIDs)
            //messageQuery?.whereKey(kParseAptComplexIDKey, equalTo: communityID)
            messageQuery?.whereKeyDoesNotExist("recipients")
            messageQuery?.whereKey("isPrivate", equalTo: NSNumber.init(value: false))
            queries.append(messageQuery!)
            
            query = PFQuery.orQuery(withSubqueries: queries)
        }
        query.whereKey("scheduleSent", notEqualTo: false)
        if CMQUser.current().isAdmin{
            query.whereKey("messageTitle", notEqualTo: "Package Reminder")
        }
        return query
    }
    @objc public func getMessageQuery()->PFQuery<PFObject>{
        let communityID = CMQUser.current().complexID!
        var query = PFQuery()
        if !CMQUser.current().isInterestedParty {
            if CMQUser.current().isResident{
                //Resident - restrict viewing
                var queries = [PFQuery]()
                //Direct Message (Current user)
                let directMessageQuery = CMQMessage.query()
                directMessageQuery?.whereKey(kParseAptComplexIDKey, equalTo: communityID)
                directMessageQuery?.whereKey("isIndividualMessage", equalTo: NSNumber.init(value: true))
                directMessageQuery?.whereKey("recipients", equalTo: CMQUser.current().objectId!)
                
                queries.append(directMessageQuery!)
                //Messages with specific recipients (no groups)
                let specificMessageQuery = CMQMessage.query()
                //specificMessageQuery?.whereKey(kParseAptComplexIDKey, equalTo: communityID)
                specificMessageQuery?.whereKey("recipients", equalTo: CMQUser.current().buildingUnit)
                specificMessageQuery?.whereKeyDoesNotExist("groups")
                queries.append(directMessageQuery!)
                
                //Messages Directly to a Building
                let messageQuery = CMQMessage.query()
                messageQuery?.whereKey(kParseAptComplexIDKey, equalTo: communityID)
                messageQuery?.whereKey("isIndividualMessage", notEqualTo: NSNumber.init(value: true))
                messageQuery?.whereKey("recipients", contains: CMQUser.current().buildingUnit)
                queries.append(messageQuery!)
                
                // Messages without specified recipients (no groups)
                let nonSpecificMessageQuery = CMQMessage.query()
                nonSpecificMessageQuery?.whereKey(kParseAptComplexIDKey, equalTo: communityID)
                nonSpecificMessageQuery?.whereKeyDoesNotExist("recipients")
                nonSpecificMessageQuery?.whereKeyDoesNotExist("groups")
                queries.append(nonSpecificMessageQuery!)
                
                //Announcements without specified groups
                let announcementQuery = CMQMessage.query()
                announcementQuery?.whereKey("isAnnouncement", equalTo: NSNumber.init(value: true))
                announcementQuery?.whereKey("recipients", containedIn: [communityID])
                announcementQuery?.whereKeyDoesNotExist("groups")
                queries.append(announcementQuery!)
                
                if let groups = CMQUser.current().groups{
                    //Message with specific recipients (with groups)
                    let specificMessageQueryWithGroups = CMQMessage.query()
                    specificMessageQueryWithGroups?.whereKey(kParseAptComplexIDKey, equalTo: communityID)
                    specificMessageQueryWithGroups?.whereKey("recipients", equalTo: CMQUser.current().buildingUnit)
                    specificMessageQueryWithGroups?.whereKey("groups", containedIn: groups)
                    queries.append(specificMessageQueryWithGroups!)
                    
                    // Messages without specified recipients (with groups)
                    let nonSpecificMessageQueryWithGroups = CMQMessage.query()
                    nonSpecificMessageQueryWithGroups?.whereKey(kParseAptComplexIDKey, equalTo: communityID)
                    nonSpecificMessageQueryWithGroups?.whereKeyDoesNotExist("recipients")
                    nonSpecificMessageQueryWithGroups?.whereKey("groups", containedIn: groups)
                    queries.append(nonSpecificMessageQueryWithGroups!)
                    
                    //Announcements with specific groups
                    let announcementQueryWithGroup = CMQMessage.query()
                    announcementQueryWithGroup?.whereKey("isAnnouncement", equalTo: NSNumber.init(value: true))
                    announcementQueryWithGroup?.whereKey("groups", containedIn: groups)
                    announcementQueryWithGroup?.whereKey("recipients", containsAllObjectsIn: [communityID])
                    queries.append(announcementQueryWithGroup!)
                }
                query = PFQuery.orQuery(withSubqueries: queries)
                
            }
            else{
                // Staff/Admin - ignore buildingUnit and groups (exclude DMs)
                // Direct Message (current user)
                let directMessageQuery = CMQMessage.query()
                directMessageQuery?.whereKey(kParseAptComplexIDKey, equalTo: communityID)
                directMessageQuery?.whereKey("isIndividualMessage", equalTo: NSNumber.init(value: true))
                directMessageQuery?.whereKey("userWhoPosted", equalTo: CMQUser.current())
                directMessageQuery?.whereKeyDoesNotExist("Package")
                
                //Messages to this community
                let messageQuery = CMQMessage.query()
                messageQuery?.whereKey(kParseAptComplexIDKey, equalTo: communityID)
                messageQuery?.whereKey("isIndividualMessage", notEqualTo: NSNumber.init(value: true))
                
                //Announcements to this community
                let announcementQuery = CMQMessage.query()
                announcementQuery?.whereKey("isAnnouncement", equalTo: NSNumber.init(value: true))
                announcementQuery?.whereKey("isIndividualMessage", notEqualTo: NSNumber.init(value: true))
                announcementQuery?.whereKey("recipients",containedIn: [communityID])
                
                query = PFQuery.orQuery(withSubqueries: [directMessageQuery!,messageQuery!,announcementQuery!])
            }
        }
        else if CMQUser.current().isInterestedParty{
            var queries = Array<PFQuery>.init()
            if let associatedUsers = CMQAPIManager.sharedInstance().associatedUsers{
                let buildingUnits = associatedUsers.compactMap({$0.buildingUnit})
                //Public Message with specific recipients
                let specificMessageQuery = CMQMessage.query()!
                specificMessageQuery.whereKey(kParseAptComplexIDKey, equalTo: communityID)
                specificMessageQuery.whereKey("recipients", containedIn: buildingUnits)
                specificMessageQuery.whereKey("isPrivate", equalTo: NSNumber.init(value: false))
                queries.append(specificMessageQuery)
            }
            
            //Public Messages without specific recipients
            let messageQuery = CMQMessage.query()
            messageQuery?.whereKey(kParseAptComplexIDKey, equalTo: communityID)
            messageQuery?.whereKeyDoesNotExist("recipients")
            messageQuery?.whereKey("isPrivate", equalTo: NSNumber.init(value: false))
            queries.append(messageQuery!)
            
            query = PFQuery.orQuery(withSubqueries: queries)
        }
        query.order(byDescending: kParseObjectCreateDateKey)
        query.includeKey("userWhoPosted")
        query.includeKey("Package")
        query.whereKey("scheduleSent", notEqualTo: false)
        query.order(byDescending: "updatedAt")
        //query.whereKey("userWhoPosted", notEqualTo: CMQUser.current())
        if CMQUser.current().isAdmin{
            query.whereKey("messageTitle", notEqualTo: "Package Reminder")
        }
        return query
    }
    // MARK: Package Methods
    
    /**
     Method to check if the delivery image should be used based on the message Delivery Status. Currently if status=="NO" the red box image is returned and if status=="PENDING" the green box image is returned.
     - parameter message: The message to query
     - returns: UIImage if the status mathces any status needed to return an image else nil.
     
     */
   @objc public func deliveryImage(message:CMQMessage)->UIImage?{
        guard let possibleStatus =  message.deliveryStatus else{ return nil}
        //if message.package?.isCheckedOut == true {
        //    return nil
        //}
        switch possibleStatus.uppercased() {
        case "NO":
            if let aClass = NSClassFromString("CMQPackage"){
                let bundle = Bundle.init(for: aClass)
                return UIImage.init(named: "packageDelivery", in: bundle, compatibleWith: nil)
            }
            return nil;
        case "YES":
            if let aClass = NSClassFromString("CMQPackage"){
                let bundle = Bundle.init(for: aClass)
                return UIImage.init(named: "packagePending", in: bundle, compatibleWith: nil)
            }
            return nil;
        default:
            return nil;
        }
    }
    /**
     Method to query if a message should show the prompt for delivery. Currently if message.package.deliveryStatus contains DELIVERED or PICKED UP or PENDING the function will return false if it doesn't and message.packageNoti is true the function will return true
     - parameter message: the message to query
     - returns: true if the message should show the prompt else false
     */
    @objc public func shouldShowPrompt(message:CMQMessage)->Bool{
        guard let _ = message.package else{
            return false
        }
        if hasPackageDelivery {
            //let possibleStatus = messagePackage.deliveryStatus
            /*return /*message.messageContent.range(of: "You have a package") != nil &&*/ messagePackage.isCheckedOut != true && messagePackage.pickedUp != true && message.packageNoti == true && possibleStatus?.uppercased().range(of: "DELIVERED") == nil && possibleStatus?.uppercased().range(of: "PICKED UP") == nil && possibleStatus?.uppercased().range(of: "PENDING") == nil*/
                return message.packageNoti == true && message.deliveryStatus != "DELIVERED OR PICKED UP" && message.messageContent.contains("You have a package")
        }
        else{
            return false
        }
    }
        
    /**
     Helper method used to remove package messages that shouldn't be shown. This method filters out any scheduled messages that are not yet beyond the sent date and package messages that contain "You have a package to pick up" and are beyond the checkout date
     - parameter message: Message object to query
     - returns: true if the message should be shown or false if not.
 
 */
    @objc public func shouldShowMessage(message:CMQMessage)->Bool{
        if shouldShowUnSentScheduledMessages{
            if message.scheduleMessage {
                if !message.scheduleSent{
                    return true
                }
            }
            return false
        }
        else{
            //let isPackageMessage = message.messageContent.contains("You have a package to pick up")
            let isPackageMessage = message.packageNoti
            if isPackageMessage {
                /*if let _ = message.package,  let _ = message.package.checkedOutDate {
                    // if there is a checkedOutDate we shouldn't show this message
                    return false;
                }*/
                if message.deliveryStatus == "DELIVERED OR PICKED UP"{
                    return false
                }
            }
            if message.scheduleMessage {
                if !message.scheduleSent {
                    return false
                }
            }
            return true
        }
        
    }
    /**
     Method used to filter messages after they are retrieved from the server.
     - parameter messages: Array of messages returned from the server after successful query
     - returns: Array of messages without messsages that shouldn't be shown.
 
 */
    @objc public func filter(messages:Array<CMQMessage>)->Array<CMQMessage>{
        return messages.filter { (message) -> Bool in
            if shouldShowPrompt(message: message){
                return true 
            }
            return self.shouldShowMessage(message: message)
        }
    }
    @objc public func sort(messages:Array<CMQMessage>)->Array<CMQMessage>{
       return messages.sorted { (message1, message2) -> Bool in
            let formatter = DateFormatter.init()
            formatter.dateFormat = "yyyy-MM-dd'T'HH:mm"
            formatter.timeZone = TimeZone.init(abbreviation: "UTC")
            let sortByDate1 = message1.scheduleMessage ? (formatter.date(from: message1.scheduleTime))!: (message1.createdAt)!
            let sortByDate2 = message2.scheduleMessage ? (formatter.date(from: message2.scheduleTime))!: (message2.createdAt)!
            return sortByDate1 > sortByDate2
        }
    }
    /**
     Helper method used to check if the array of messages returned by the server has a pending scheduled message. (Used for message button in Message VC)
     - parameter messages: Array of messages returned from the server after successful query
     - returns: true if there is a pending scheduled message, false if not
 
 */
    @objc public func hasPendingScheduledMessage(messages:Array<CMQMessage>)->Bool{
        shouldShowUnSentScheduledMessages = true
        let filteredArray = messages.filter { (message) -> Bool in
            return shouldShowMessage(message: message)
        }
        shouldShowUnSentScheduledMessages = false
        return filteredArray.count>0
    }
    /**
     Method to set delivery status on a message, it simply sets the message.deliveryStatus to either YES or NO and saves the object on the server.
     - parameter message: the message to set
     - parameter response: true if setting deliveryStatus to YES false if setting to NO
     - parameter completion: the callback block which is called after completion or in the event that an error occurred.
     - parameter error: error if one occured, nil if not.
     */
    public func setDelivery(message:CMQMessage, response: Bool?, completion:@escaping (_ error:Error?)->Void) -> Void {
        if response == true {
            message.setObject("YES", forKey: "deliveryStatus")
            //message.deliveryStatus="YES"
        }
        else if response == false{
            message.setObject("NO", forKey: "deliveryStatus")
            //message.deliveryStatus="NO"
        }else{
            message.remove(forKey: "deliveryStatus")
            //message.deliveryStatus = nil
        }
        message.saveInBackground(block: { (messageSuccess, messageError) in
            if messageSuccess{
                completion(nil)
            }
            else{
                completion(messageError!)
            }
        })
    }
}
