//
//  CMQNoticesCell.h
//  Communique
//
//  Created by Chris Hetem on 9/17/14.
//  Copyright (c) 2014 WaveRider, Inc. All rights reserved.
//

//@import UIKit;
#import <UIKit/UIKit.h>
@class CMQMessageCell;
@protocol CMQMessageProtocol
-(void)cell:(CMQMessageCell*)cell receivedResponse:(BOOL)deliveryResponse;
-(void)deliveryPressedByCell:(CMQMessageCell *)cell;
@end
@interface CMQMessageCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *userPhoto;
@property (weak, nonatomic) IBOutlet UIImageView* messagePhoto;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet TTTAttributedLabel *messageLabel;
@property (weak, nonatomic) IBOutlet TTTAttributedLabel *recipientLabel;
@property (weak, nonatomic) IBOutlet UIImageView* deliveryImage;
@property(weak, nonatomic) IBOutlet UIButton* deliveryButton;
@property (weak, nonatomic) id<CMQMessageProtocol> delegate;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageViewHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *packageViewHeightConstraint;
-(void)addRecognizer;
@end

