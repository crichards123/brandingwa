//
//  CMQMessageNavigationController.h
//  Communique
//
//  Created by Andre White on 10/19/17.
//  Copyright © 2017 Communique, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CMQMessageNavigationController : UINavigationController

@end
