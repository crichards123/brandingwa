//
//  CMQCommunityViewController.m
//  Communique
//
//  Created by Colby Melvin on 11/30/15.
//  Copyright © 2015 WaveRider, Inc. All rights reserved.
//

#import "CMQCommunityViewController.h"
#import "CMQTableViewCell.h"

static NSString *communityCellIdentifier = @"communityCellIdentifier";

@interface CMQCommunityViewController () <UITableViewDelegate, UISearchBarDelegate, UISearchControllerDelegate, UISearchResultsUpdating>
{
    NSArray *communities;
    NSArray *filteredCommunities;
}

@end

@implementation CMQCommunityViewController

- (void)viewDidLoad{
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
    
    self.screenName = @"Community Selection";
    
    [self configureUI];
    
    if([CMQUser currentUser]){
        [self getCommunities];
    }
}

- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
    
    // Dispose of any resources that can be recreated.
}

- (void) configureUI{
    // Register our custom cells with our table views (community/search table views)
    NSBundle* comBundle = [NSBundle bundleForClass:[CMQTableViewCell class]];
    [self.tableView registerNib:[UINib nibWithNibName:@"CMQTableViewCell" bundle:comBundle] forCellReuseIdentifier:communityCellIdentifier];
    self.tableView.scrollIndicatorInsets = UIEdgeInsetsZero;
    
    // Search Controller
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
    self.searchController.searchResultsUpdater = self;
    self.searchController.dimsBackgroundDuringPresentation = NO;
    self.searchController.searchBar.delegate = self;
    [self.searchController.searchBar sizeToFit];
    self.searchController.searchBar.placeholder = @"Search Communities By Name";
    self.tableView.tableHeaderView = self.searchController.searchBar;
    self.definesPresentationContext = YES;
    
    // Nav bar stuff
    [self.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor blackColor],
       NSFontAttributeName:[UIFont fontWithName:kCMQFontGeometriaLight size:21]}];
    
    UIImage *leftImage = [[UIImage imageNamed:@"feed-btn" inBundle:comBundle compatibleWithTraitCollection:nil] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    UIBarButtonItem *feedButton = [[UIBarButtonItem alloc]
                                     initWithImage:leftImage
                                     style:UIBarButtonItemStylePlain
                                     target:self
                                     action:@selector(goToFeed:)];
    self.navigationItem.leftBarButtonItem = feedButton;
}

/**
 *  Retrieves all current user's communities
 */
- (void) getCommunities{
    NSMutableArray *communityIDs = [[NSMutableArray alloc] init];
    
    for(CMQApartment *apartment in [CMQUser currentUser].communities){
        [communityIDs addObject:[apartment objectId]];
    }
    
    PFQuery *query = [CMQApartment query];
    query.cachePolicy = kPFCachePolicyCacheThenNetwork;
    [query whereKey:@"objectId" containedIn:communityIDs];
    [query orderByAscending:@"aptName"];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if(!error){ //successful request, notify caller
            communities = objects;
            
            [self.tableView reloadData];
        }else if(error.code != kPFErrorCacheMiss){  //failed request, notify caller
            [SVProgressHUD showInfoWithStatus:@"Failed to load communities, some features may be temporarily unavailable"];
            [[CMQAPIClient sharedClient] handleParseError:error];
        }
    }];
}

/**
 *  filters communities based on user input
 *
 *  @param value user input from search bar
 */
- (void) filterCommunities:(NSString *)value{
    NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"aptNameToLowercase contains %@", [value lowercaseString]];
    filteredCommunities = [communities filteredArrayUsingPredicate:resultPredicate];
    [self.tableView reloadData];
}

/**
 *  switches community to selection
 *
 *  @param communityID (objectId) of desired community
 */
- (void) switchCommunity:(NSString *)communityID{
    [SVProgressHUD showWithStatus:@"Loading community"];
    [[CMQAPIClient sharedClient]switchApartmentWithId:communityID];
}

#pragma mark - Button Actions

/**
 *  feed button clicked, go to feed
 *
 *  @param sender the feed button
 */
- (IBAction)goToFeed:(id)sender {
    if ([self.searchController isActive]){
        [self.searchController dismissViewControllerAnimated:YES completion:nil];
    }
    
    [self.delegate CMQCommunityViewControllerDidSelectCommunityPostsIndex:self];
}


#pragma mark - UITableView Delegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    CMQTableViewCell *cell = (CMQTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    
    if ([self.searchController isActive]){
        [self.searchController dismissViewControllerAnimated:YES completion:nil];
    }
    
    [self switchCommunity:cell.communityIdLabel.text];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    // Return the number of rows in the section.
    if ([self.searchController isActive])
        return [filteredCommunities count];
    
    return [communities count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    // Configure the cell...
    CMQTableViewCell *cell;
    
    cell = (CMQTableViewCell *)[tableView dequeueReusableCellWithIdentifier:communityCellIdentifier];

    CMQApartment *apartment;
    if ([self.searchController isActive]){
        apartment = (CMQApartment *)[filteredCommunities objectAtIndex:indexPath.row];
    }else{
        apartment = (CMQApartment *)[communities objectAtIndex:indexPath.row];
    }

    NSString *communityName = [apartment aptName];
    NSString *communityAddress = [NSString stringWithFormat:@"%@, %@", apartment.aptCity, apartment.aptState];
    NSString *communityID = [apartment objectId];
    
    cell.communityNameLabel.text = communityName;
    cell.communityAddressLabel.text = communityAddress;
    cell.communityIdLabel.text = communityID;
    
    cell.communityNameLabel.textAlignment = NSTextAlignmentLeft;
    cell.communityAddressLabel.textAlignment = NSTextAlignmentLeft;
    
    cell.accessoryType = UITableViewCellAccessoryNone;
    
    if ([CMQUser currentUser].tempAptComplexID && [[CMQUser currentUser].tempAptComplexID isEqualToString:communityID])
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    else if (![CMQUser currentUser].tempAptComplexID && [CMQUser currentUser].aptComplexID && [[CMQUser currentUser].aptComplexID isEqualToString:communityID])
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    return @"";
}


#pragma mark - UISearchResultsUpdating

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController{
    NSString *searchString = searchController.searchBar.text;
    [self filterCommunities:searchString];
}

@end
