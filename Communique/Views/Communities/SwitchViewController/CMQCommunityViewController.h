//
//  CMQCommunityViewController.h
//  Communique
//
//  Created by Colby Melvin on 11/30/15.
//  Copyright © 2015 WaveRider, Inc. All rights reserved.
//

//@import UIKit;
#import <UIKit/UIKit.h>

@class CMQCommunityViewController;

@protocol CMQCommunityViewControllerDelegate <NSObject>
- (void)CMQCommunityViewControllerDidSelectCommunityPostsIndex:(CMQCommunityViewController *)controller;

@end

@interface CMQCommunityViewController : GAITrackedViewController

@property (nonatomic, weak) id <CMQCommunityViewControllerDelegate> delegate;
@property (weak, nonatomic) IBOutlet UINavigationBar *navigationBar;
@property (weak, nonatomic) IBOutlet UINavigationItem *navigationItem;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) UISearchController *searchController;

@end
