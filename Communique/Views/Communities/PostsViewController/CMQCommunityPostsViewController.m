//
//  CMQCommunityPostsViewController.m
//  Communique
//
//  Created by Colby Melvin on 12/3/15.
//  Copyright © 2015 WaveRider, Inc. All rights reserved.
//

#import "CMQCommunityPostsViewController.h"
#import "CMQCommunityPostCell.h"
#import "CMQCommunityPostViewModel.h"

#define DEFAULT_CELL_HEIGHT                 116
#define DEFAULT_CONTENT_SPACING_TITLE       31
#define ALTERNATE_CONTENT_SPACING_TITLE     5

static NSString *communityPostCellIdentifier = @"communityPostCellIdentifier";

@interface CMQCommunityPostsViewController () <CMQAPIClientDelegate>
{
    NSArray *communities;
}

@property (strong, nonatomic) CMQCommunityPostViewModel  *viewModel;

@end

@implementation CMQCommunityPostsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
    
    self.screenName = @"All My Communities' Posts";
    
    [self configureUI];
    
    if([CMQUser currentUser]) [self getCommunities];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [[CMQAPIClient sharedClient]setDelegate:self];
    [[CMQAPIClient sharedClient]fetchCommunityPosts];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
    // Dispose of any resources that can be recreated.
}

/**
 *  initially configures the UI
 */
- (void) configureUI{
    //view model
    self.viewModel = [[CMQCommunityPostViewModel alloc]init];
    
    // Register our custom cells with our collection view
    NSBundle* comBundle = [NSBundle bundleForClass:[CMQCommunityPostCell class]];
    [self.collectionView registerNib:[UINib nibWithNibName:@"CMQCommunityPostCell" bundle:comBundle] forCellWithReuseIdentifier:communityPostCellIdentifier];
    [self.collectionView setContentInset:UIEdgeInsetsMake(10.0f, 0.0f, 0.0f, 0.0f)];
    
    // Nav bar stuff
    [self.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor blackColor],
       NSFontAttributeName:[UIFont fontWithName:kCMQFontGeometriaLight size:21]}];
    UIImage *leftImage = [[UIImage imageNamed:@"home-switch-btn" inBundle:comBundle compatibleWithTraitCollection:nil] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    UIBarButtonItem *searchButton = [[UIBarButtonItem alloc]
                                     initWithImage:leftImage
                                     style:UIBarButtonItemStylePlain
                                     target:self
                                     action:@selector(goSwitch:)];
    self.navigationItem.leftBarButtonItem = searchButton;
    
    //refresh control
    self.refreshControl = [[UIRefreshControl alloc]init];
    [self.collectionView addSubview:self.refreshControl];
    [self.refreshControl addTarget:self action:@selector(onRefresh) forControlEvents:UIControlEventValueChanged];
}

/**
 *  Retrieves all current user's communities
 */
- (void) getCommunities{
    NSMutableArray *communityIDs = [[NSMutableArray alloc] init];
    
    for(CMQApartment *apartment in [CMQUser currentUser].communities){
        [communityIDs addObject:[apartment objectId]];
    }
    
    PFQuery *query = [CMQApartment query];
    query.cachePolicy = kPFCachePolicyCacheThenNetwork;
    [query whereKey:@"objectId" containedIn:communityIDs];
    [query orderByAscending:@"aptName"];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if(!error){ //successful request, notify caller
            communities = objects;
        }else if(error.code != kPFErrorCacheMiss){  //failed request, notify caller
            [SVProgressHUD showInfoWithStatus:@"Failed to load communities, some features may be temporarily unavailable"];
            [[CMQAPIClient sharedClient] handleParseError:error];
        }
    }];
}

/**
 *  switches community to selection
 *
 *  @param communityID (objectId) of desired community
 */
- (void) switchCommunity:(NSString *)communityID{
    [SVProgressHUD showWithStatus:@"Loading community"];
    [[CMQAPIClient sharedClient]switchApartmentWithId:communityID];
}

#pragma Refresh Control
/**
 *  refreshes the view by calling our APIClient
 */
-(void)onRefresh{
    [SVProgressHUD showWithStatus:@"Refreshing posts"];
    [[CMQAPIClient sharedClient]fetchCommunityPosts];
}

#pragma mark - Button Actions

/**
 *  goes to 'Switch Community' view
 *
 *  @param sender the switch button
 */
- (IBAction)goSwitch:(id)sender {
    [self.delegate CMQCommunityPostsViewControllerDidTapSwitchButton:self];
}

#pragma mark - CMQAPIClient delegate
- (void)CMQAPIClientDidFetchCommunityPostsSuccessfully:(NSArray *)posts{
    [SVProgressHUD dismiss];
    
    [self.refreshControl endRefreshing];
    
    self.viewModel.communityPostsArray = [NSMutableArray arrayWithArray:posts];
    [self.collectionView reloadData];
}

- (void)CMQAPIClientDidSwitchApartmentSuccessfully{
    // Community switch successful, load community
    [self.delegate CMQCommunityPostsViewControllerDidSwitchCommunity:self];
}

#pragma mark - UICollectionView Datasource

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    CMQCommunityPostCell *cell = (CMQCommunityPostCell *)[self.collectionView dequeueReusableCellWithReuseIdentifier:communityPostCellIdentifier forIndexPath:indexPath];

    [self configureCell:cell forRow:indexPath.row]; //call to configure our row

    return cell;
}

/**
 *  configures a given cell at a given row with formatting and such
 *
 *  @param cell the cell to configure
 *  @param row  the row for the cell to configure
 */
-(void)configureCell:(CMQCommunityPostCell *)cell forRow:(NSInteger)row{
    cell.backgroundColor = [UIColor whiteColor];
    
    CMQUser *userWhoPosted;
    
    NSString *communityName = @"";
    for(CMQApartment *apartment in communities){
        if ([[apartment objectId] isEqualToString:[[self.viewModel.communityPostsArray objectAtIndex:row] aptComplexID]])
            communityName = apartment.aptName;
    }
    
    cell.communityID.text = [[self.viewModel.communityPostsArray objectAtIndex:row] aptComplexID];
    cell.communityPostImageView.image = nil;
    cell.communityPostImageView.hidden = YES;
    cell.communityPostImageViewTopSpacingConstraint = 0;
    cell.communityPostImageViewHeightConstraint.constant = 0;
    cell.communityPostImageLoadingLabel.text = @"image loading";
    cell.communityPostImageLoadingLabel.hidden = YES;
    cell.urlLabel.hidden = YES;
    cell.contentLabelVerticalSpacingToTitle.constant = ALTERNATE_CONTENT_SPACING_TITLE;
    
    if ([[self.viewModel.communityPostsArray objectAtIndex:row] isKindOfClass:[CMQMessage class]]){ // item is a message/announcement
        CMQMessage *message = [self.viewModel.communityPostsArray objectAtIndex:row];
        userWhoPosted = (CMQUser *) message.userWhoPosted.fetchIfNeeded;
        
        cell.communityPostMainLabel.text = [NSString stringWithFormat:@" %@", communityName];
        
        if (message.isAnnouncement && message.recipients.count > 1){
            cell.communityPostMainLabel.text = [NSString stringWithFormat:@" %lu Communities", (unsigned long)message.recipients.count];
        }else if (message.isAnnouncement && message.recipients.count == 1){
            for(CMQApartment *apartment in communities){
                if ([[apartment objectId] isEqualToString:[message.recipients objectAtIndex:0]]){
                    cell.communityPostMainLabel.text = [NSString stringWithFormat:@" %@", apartment.aptName];
                    cell.communityID.text = [apartment objectId];
                }
            }
        }
        
        cell.communityPostTitleLabel.text = message.messageTitle;
        cell.communityPostContentLabel.text = message.messageContent;
        cell.dateLabel.text = [NSString stringWithFormat:@"%@", [self.viewModel formatDate:message.createdAt]];
    }else if ([[self.viewModel.communityPostsArray objectAtIndex:row] isKindOfClass:[CMQEvent class]]){ // item is an event
        CMQEvent *event = [self.viewModel.communityPostsArray objectAtIndex:row];
        userWhoPosted = (CMQUser *) event.userWhoPosted.fetchIfNeeded;
        
        cell.communityPostMainLabel.text = [NSString stringWithFormat:@" %@", communityName];
        cell.communityPostTitleLabel.text = event.eventTitle;
        cell.communityPostContentLabel.text = event.eventContent;
        cell.dateLabel.text = [NSString stringWithFormat:@"starts %@", [self.viewModel formatDateForEventDate:event.eventDate]];
        
        //load image if there is one
        if(event.eventPhoto){
            //fetch our image data on a background thread
            dispatch_queue_t queue = dispatch_get_global_queue (DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
            dispatch_async(queue, ^{
                UIImage *eventImage = [UIImage getImageFromPFFile:event.eventPhoto];
                //display our image on the main thread
                dispatch_async(dispatch_get_main_queue(), ^{
                    cell.communityPostImageViewHeightConstraint.constant = [UIImage getHeightForImageContainerWithWidth:cell.communityPostImageView.frame.size.width imageWidth:eventImage.size.width imageHeight:eventImage.size.height];
                    
                    cell.communityPostImageView.image = eventImage;
                    cell.communityPostImageView.hidden = NO;
                    cell.communityPostImageLoadingLabel.hidden = NO;
                    [UIView animateWithDuration:0.75 animations:^{
                        cell.communityPostImageView.alpha = 1.0;
                    }];
                });
            });
        }
    }else if ([[self.viewModel.communityPostsArray objectAtIndex:row] isKindOfClass:[CMQNewsArticle class]]){ // item is news
        CMQNewsArticle *newsArticle = [self.viewModel.communityPostsArray objectAtIndex:row];
        userWhoPosted = (CMQUser *) newsArticle.userWhoPosted.fetchIfNeeded;
        
        cell.communityPostMainLabel.text = [NSString stringWithFormat:@" %@", communityName];
        cell.communityPostTitleLabel.text = newsArticle.newsTitle;
        cell.communityPostContentLabel.text = newsArticle.newsContent;
        cell.dateLabel.text = [NSString stringWithFormat:@"%@", [self.viewModel formatDate:newsArticle.createdAt]];
        
        if(newsArticle.articleURL && newsArticle.articleURL.length > 0){
            cell.urlLabel.hidden = NO;
            cell.urlLabel.text = newsArticle.articleURL;
            cell.contentLabelVerticalSpacingToTitle.constant = DEFAULT_CONTENT_SPACING_TITLE;
        }
    
        //load image if there is one
        if(newsArticle.newsPhoto){
            //fetch our image data on a background thread
            dispatch_queue_t queue = dispatch_get_global_queue (DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
            dispatch_async(queue, ^{
                UIImage *newsImage = [UIImage getImageFromPFFile:newsArticle.newsPhoto];
                //display our image on the main thread
                dispatch_async(dispatch_get_main_queue(), ^{
                    cell.communityPostImageViewHeightConstraint.constant = [UIImage getHeightForImageContainerWithWidth:cell.communityPostImageView.frame.size.width imageWidth:newsImage.size.width imageHeight:newsImage.size.height];
                    
                    cell.communityPostImageView.image = newsImage;
                    cell.communityPostImageView.hidden = NO;
                    cell.communityPostImageLoadingLabel.hidden = NO;
                    [UIView animateWithDuration:0.75 animations:^{
                        cell.communityPostImageView.alpha = 1.0;
                    }];
                });
            });
        }
    }
    
    cell.nameLabel.text = (userWhoPosted.fullName) ? userWhoPosted.fullName : @"Staff Member";
    if (userWhoPosted && userWhoPosted.userPhoto){
        //fetch our image data on a background thread
        dispatch_queue_t queue = dispatch_get_global_queue (DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
        dispatch_async(queue, ^{
            UIImage *userPhoto = [UIImage getImageFromPFFile:userWhoPosted.userPhoto];
            //display our image on the main thread
            dispatch_async(dispatch_get_main_queue(), ^{
                cell.userPhoto.contentMode = UIViewContentModeScaleAspectFill;
                cell.userPhoto.image = userPhoto;
            });
        });
    }else{
        cell.userPhoto.contentMode = UIViewContentModeTop;
        NSBundle* comBundle = [NSBundle bundleForClass:[CMQPackage class]];
        cell.userPhoto.image = [UIImage imageNamed:@"user-icon" inBundle:comBundle compatibleWithTraitCollection:nil];
    }
    
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.viewModel.communityPostsArray.count;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = DEFAULT_CELL_HEIGHT;
    
    if ([[self.viewModel.communityPostsArray objectAtIndex:indexPath.row] isKindOfClass:[CMQMessage class]]){ // item is a message or announcement
        CMQMessage *message = [self.viewModel.communityPostsArray objectAtIndex:indexPath.row];
        
        CGRect contentRect = [message.messageContent boundingRectWithSize:CGSizeMake(self.collectionView.frame.size.width - 30, CGFLOAT_MAX) options:(NSStringDrawingUsesFontLeading) | (NSStringDrawingUsesLineFragmentOrigin) attributes:@{NSFontAttributeName : [UIFont fontWithName:kCMQFontGeometriaLight size:14.5]} context:nil];
        
        cellHeight += contentRect.size.height;
    }else if ([[self.viewModel.communityPostsArray objectAtIndex:indexPath.row] isKindOfClass:[CMQEvent class]]){ // item is an event
        CMQEvent *event = [self.viewModel.communityPostsArray objectAtIndex:indexPath.row];
        
        if(event.eventPhoto){
            UIImage *eventImage = [UIImage getImageFromPFFile:event.eventPhoto];
            cellHeight += [UIImage getHeightForImageContainerWithWidth:self.collectionView.frame.size.width - 20 imageWidth:eventImage.size.width imageHeight:eventImage.size.height];
        }

        CGRect contentRect = [event.eventContent boundingRectWithSize:CGSizeMake(self.collectionView.frame.size.width - 30, CGFLOAT_MAX) options:(NSStringDrawingUsesFontLeading) | (NSStringDrawingUsesLineFragmentOrigin) attributes:@{NSFontAttributeName : [UIFont fontWithName:kCMQFontGeometriaLight size:14.5]} context:nil];
        
        cellHeight += contentRect.size.height;
    }else if ([[self.viewModel.communityPostsArray objectAtIndex:indexPath.row] isKindOfClass:[CMQNewsArticle class]]){ // item is a news article
        CMQNewsArticle *newsArticle = [self.viewModel.communityPostsArray objectAtIndex:indexPath.row];
        
        if(newsArticle.newsPhoto){
            UIImage *newsImage = [UIImage getImageFromPFFile:newsArticle.newsPhoto];
            cellHeight += [UIImage getHeightForImageContainerWithWidth:self.collectionView.frame.size.width - 20 imageWidth:newsImage.size.width imageHeight:newsImage.size.height];
        }
        
        CGRect contentRect = [newsArticle.newsContent boundingRectWithSize:CGSizeMake(self.collectionView.frame.size.width - 30, CGFLOAT_MAX) options:(NSStringDrawingUsesFontLeading) | (NSStringDrawingUsesLineFragmentOrigin) attributes:@{NSFontAttributeName : [UIFont fontWithName:kCMQFontGeometriaLight size:14.5]} context:nil];
        
        if(newsArticle.articleURL && newsArticle.articleURL.length > 0) cellHeight += 26;
        
        cellHeight += contentRect.size.height;
    }

    return CGSizeMake(self.collectionView.frame.size.width - 20, cellHeight);
}

#pragma mark - UICollectionView Delegate

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    // User clicked item, determine if we can switch to that community
    CMQCommunityPostCell *cell = (CMQCommunityPostCell *)[collectionView cellForItemAtIndexPath:indexPath];
    
    if (cell.communityID.text && cell.communityID.text.length > 0) // only populated if 1 community, perform switch
        [self switchCommunity:cell.communityID.text];
    else // This is an announcement that went to > 1 community, switch not available here
        [SVProgressHUD showInfoWithStatus: @"This post went to more than 1 community"];
}

@end
