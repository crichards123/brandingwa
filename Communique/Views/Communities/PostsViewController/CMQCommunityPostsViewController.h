//
//  CMQCommunityPostsViewController.h
//  Communique
//
//  Created by Colby Melvin on 12/3/15.
//  Copyright © 2015 WaveRider, Inc. All rights reserved.
//

//@import UIKit;
#import <UIKit/UIKit.h>

@class CMQCommunityPostsViewController;

@protocol CMQCommunityPostsViewControllerDelegate <NSObject>
- (void)CMQCommunityPostsViewControllerDidTapSwitchButton:(CMQCommunityPostsViewController *)controller;
- (void)CMQCommunityPostsViewControllerDidSwitchCommunity:(CMQCommunityPostsViewController *)controller;

@end

@interface CMQCommunityPostsViewController : GAITrackedViewController

@property (nonatomic, weak) id <CMQCommunityPostsViewControllerDelegate> delegate;
@property (weak, nonatomic) IBOutlet UINavigationBar *navigationBar;
@property (weak, nonatomic) IBOutlet UINavigationItem *navigationItem;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (strong, nonatomic) UIRefreshControl *refreshControl;

@end
