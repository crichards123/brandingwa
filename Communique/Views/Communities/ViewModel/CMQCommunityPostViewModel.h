//
//  CMQCommunityPostViewModel.h
//  Communique
//
//  Created by Colby Melvin on 12/7/15.
//  Copyright © 2015 WaveRider, Inc. All rights reserved.
//

//@import Foundation;
#import <Foundation/Foundation.h>

@interface CMQCommunityPostViewModel : NSObject

/**
 *  an array of all the posts
 */
@property (strong, nonatomic) NSMutableArray *communityPostsArray;

/**
 *  an array of all the messages
 */
@property (strong, nonatomic) NSMutableArray *messageArray;

/**
 *  an array of all the events
 */
@property (strong, nonatomic) NSMutableArray *eventArray;

/**
 *  an array of all the news articles
 */
@property (strong, nonatomic) NSMutableArray *newsArray;

/**
 *  formats a date for messages/news
 *
 *  @param date the date to format
 *
 *  @return the formatted date as a string
 */
-(NSString *)formatDate:(NSDate *)date;

/**
 *  formats a date events
 *
 *  @param date the date to format
 *
 *  @return the formatted date as a string
 */
-(NSString *)formatDateForEventDate:(NSDate *)date;

@end
