//
//  CMQCommunityPostViewModel.m
//  Communique
//
//  Created by Colby Melvin on 12/7/15.
//  Copyright © 2015 WaveRider, Inc. All rights reserved.
//

#import "CMQCommunityPostViewModel.h"

@implementation CMQCommunityPostViewModel

-(NSString *)formatDate:(NSDate *)date{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    
    //calendar to extract day and month compenents
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:(NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear) fromDate:date];
    
    NSInteger month = [components month]-1;
    NSInteger day = [components day];
    
    NSString *monthName = [[dateFormatter shortMonthSymbols]objectAtIndex:month];
    NSString *dateNumber = [NSString stringWithFormat:@"%ld", (long)day];
    
    NSString *formatStringForHours = [NSDateFormatter dateFormatFromTemplate:@"j" options:0 locale:[NSLocale currentLocale]];
    
    NSRange containsA = [formatStringForHours rangeOfString:@"a"];
    BOOL hasAMPM = containsA.location != NSNotFound;
    
    if (hasAMPM) { dateFormatter.dateFormat = @"h:mm a";}
    else { dateFormatter.dateFormat = @"HH:mm";}
    NSString *time = [dateFormatter stringFromDate:date];
    
    return [NSString stringWithFormat:@"%@. %@, %@", monthName, dateNumber, time];
}

/**
 *  Formats a date for existing events
 *
 *  @param date the date to format
 *
 *  @return the date as a string that's been formatted
 */
-(NSString *)formatDateForEventDate:(NSDate *)date{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    
    //calendar to extract day and month compenents
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:(NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitWeekday) fromDate:date];
    
    NSInteger month = [components month]-1;
    NSInteger dateNumber = [components day];
    NSInteger day = [components weekday]-1;
    
    NSString *monthName = [[dateFormatter shortMonthSymbols]objectAtIndex:month];
    NSString *dayName = [[dateFormatter shortWeekdaySymbols]objectAtIndex:day];
    
    NSString *formatStringForHours = [NSDateFormatter dateFormatFromTemplate:@"j" options:0 locale:[NSLocale currentLocale]];
    
    NSRange containsA = [formatStringForHours rangeOfString:@"a"];
    BOOL hasAMPM = containsA.location != NSNotFound;
    
    if (hasAMPM) { dateFormatter.dateFormat = @"h:mm a";}
    else { dateFormatter.dateFormat = @"HH:mm";}
    NSString *timeName = [dateFormatter stringFromDate: date];
    
    return [NSString stringWithFormat:@"%@, %@. %ld - %@", dayName, monthName, (long)dateNumber, timeName];
}

@end
