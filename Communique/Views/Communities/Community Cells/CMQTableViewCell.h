//
//  CMQTableViewCell.h
//  Communique
//
//  Created by Colby Melvin on 11/30/15.
//  Copyright © 2015 WaveRider, Inc. All rights reserved.
//

//@import UIKit;
#import <UIKit/UIKit.h>

@interface CMQTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet TTTAttributedLabel *communityNameLabel;
@property (weak, nonatomic) IBOutlet TTTAttributedLabel *communityAddressLabel;
@property (weak, nonatomic) IBOutlet TTTAttributedLabel *communityIdLabel;


@end
