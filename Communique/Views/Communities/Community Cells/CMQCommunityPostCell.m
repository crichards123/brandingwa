//
//  CMQCommunityPostCell.m
//  Communique
//
//  Created by Colby Melvin on 12/7/15.
//  Copyright © 2015 WaveRider, Inc. All rights reserved.
//

#import "CMQCommunityPostCell.h"

@implementation CMQCommunityPostCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self configureUI];
}

/**
 *  initially configures the UI
 */
-(void)configureUI
{
    //TTTAttributedLabel setup
    [self.communityPostMainLabel setTextColor:[UIColor blackColor]];
    [self.communityPostImageLoadingLabel setTextAlignment:NSTextAlignmentCenter];
    [self.communityPostContentLabel setVerticalAlignment:TTTAttributedLabelVerticalAlignmentTop];
    [self.communityPostContentLabel setTextColor:[UIColor blackColor]];
    [self.communityPostContentLabel setLineBreakMode:NSLineBreakByWordWrapping];
    [self.communityPostContentLabel setNumberOfLines:0];

    //fonts
    [self.communityPostMainLabel setFont:[UIFont fontWithName:kCMQFontGeometriaBold size:14]];
    [self.communityPostImageLoadingLabel setFont:[UIFont fontWithName:kCMQFontGeometriaLight size:12]];
    [self.communityPostTitleLabel setFont:[UIFont fontWithName:kCMQFontGeometriaBold size:14]];
    [self.urlLabel setFont:[UIFont fontWithName:kCMQFontGeometriaLight size:14]];
    [self.communityPostContentLabel setFont:[UIFont fontWithName:kCMQFontGeometriaLight size:14]];
    [self.nameLabel setFont:[UIFont fontWithName:kCMQFontGeometriaLight size:17]];
    [self.dateLabel setFont:[UIFont fontWithName:kCMQFontGeometriaLight size:10]];
    
    self.userPhoto.layer.cornerRadius = 20.0f;
    self.userPhoto.layer.borderWidth = .5f;
    self.userPhoto.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.userPhoto.layer.masksToBounds = YES;
}

@end
