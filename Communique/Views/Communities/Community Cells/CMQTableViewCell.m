//
//  CMQTableViewCell.m
//  Communique
//
//  Created by Colby Melvin on 11/30/15.
//  Copyright © 2015 WaveRider, Inc. All rights reserved.
//

#import "CMQTableViewCell.h"

@implementation CMQTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self configureUI];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

/**
 *  initially configures the UI
 */
-(void)configureUI{
    [self.communityNameLabel setVerticalAlignment:TTTAttributedLabelVerticalAlignmentCenter];
    [self.communityNameLabel setFont:[UIFont fontWithName:kCMQFontGeometriaBold size:13]];
    
    [self.communityAddressLabel setVerticalAlignment:TTTAttributedLabelVerticalAlignmentCenter];
    [self.communityAddressLabel setFont:[UIFont fontWithName:kCMQFontGeometriaLight size:11]];
}

@end
