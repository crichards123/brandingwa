//
//  CMQCommunityPostCell.h
//  Communique
//
//  Created by Colby Melvin on 12/7/15.
//  Copyright © 2015 WaveRider, Inc. All rights reserved.
//

//@import UIKit;
#import <UIKit/UIKit.h>

@interface CMQCommunityPostCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *communityID;
@property (weak, nonatomic) IBOutlet TTTAttributedLabel *communityPostMainLabel;
@property (weak, nonatomic) IBOutlet TTTAttributedLabel *communityPostImageLoadingLabel;
@property (weak, nonatomic) IBOutlet UIImageView *communityPostImageView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *communityPostImageViewHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *communityPostImageViewTopSpacingConstraint;
@property (weak, nonatomic) IBOutlet TTTAttributedLabel *communityPostTitleLabel;
@property (weak, nonatomic) IBOutlet TTTAttributedLabel *communityPostContentLabel;
@property (weak, nonatomic) IBOutlet UIImageView *userPhoto;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *urlLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentLabelVerticalSpacingToTitle;

@end
