//
//  CMQPackageFlag.h
//  Communique
//
//  Created by Andre White on 1/10/18.
//  Copyright © 2018 Communique, LLC. All rights reserved.
//

#import <Parse/Parse.h>
@class CMQUser;
@interface CMQPackageFlag : PFObject<PFSubclassing>
@property(assign, nonatomic)BOOL hasPackageCheckedIn;
@property(assign, nonatomic)BOOL hasPackageCheckedOut;
@property(retain, nonatomic)NSString* recipientString;
@property(retain, nonatomic)NSString* aptComplexID;
@property(retain, nonatomic)CMQUser* recipient;

@end
