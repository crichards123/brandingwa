//
//  CMQConfig.swift
//  Communique
//
//  Created by Andre White on 5/4/18.
//  Copyright © 2018 Communique, LLC. All rights reserved.
//

import Foundation

//MARK: - UIImage Names
/*
 Used in CMQNoContentView
 
 */
let CMQMessageLogoImageName: String = "ic_message"
let CMQNewsLogoImageName: String = "ic_news"
let CMQEventLogoImageName: String = "ic_event"
let CMQScheduledMessageLogoImageName : String = "ic_message"
/*
 Used in CMQComposeViewController
    
 */
let CMQSendButtonImageName : String = "ic_send_blue"
let CMQSelectCommunitiesImageName: String = "ic_comm"
/*
 Used in CMQFeedContentView
 */
let CMQUserIconPlaceHolderImageName: String = "user-icon"
/*
  Used in CMQKabaButtonItem
 
 */
let CMQKabaUnlockButtonImageName: String = "key-Navicon"

//MARK: - UINib Names
let CMQNoContentNibName: String = "CMQNoContentView" //Used in CMQNoContentView.instanceFromNib()
let CMQHomeFeedHeaderNibName: String = "CMQHomeFeedHeader"



//MARK: - DormaKaba Display
/* Strings */
//DormaKabaView
let CMQDisplayDormaKabaUnlockString: String = "Unlock"
let CMQDisplayDormaKabaSetUpString: String = "Set Up Common Access"

//Generating Key
let CMQDisplayDormaKabaGenKeyPromptString: String = "Generating digital key. This may take a few minutes..."

//Will Generate Key
let CMQDisplayDormaKabaWillGenKeyInterruptedPromptString:String = "Process was interrupted. Would you like to try again?"
let CMQDisplayDormaKabaWillGenKeyPromptString:String = "To use the bluetooth enabled door, you need to generate a digital key. Would you like to generate one?"

//Unlocking Door
let CMQDisplayDormaKabaDoorUnlockedString: String = "Unlocked!"
let CMQDisplayDormaKabaDoorLockedString: String = "Locked"

