//
//  CMQBaseTableViewController.swift
//  Communique
//
//  Created by Andre White on 5/19/18.
//  Copyright © 2018 Communique, LLC. All rights reserved.
//

import UIKit

public class CMQBaseTableViewController: IDTableViewController {
    @IBOutlet weak var scheduledMessageButton: UIBarButtonItem?
    override public func viewDidLoad() {
        super.viewDidLoad()
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    /*
    override public func addObservers(){
        NotificationCenter.default.addObserver(self, selector: #selector(didRespondToPackagePrompt(notification:)), name: .didRespondToPackagePrompt, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleAttendeesTapped(notification:)), name: .didTapAttendees, object: nil)
        super.addObservers()
    }
    override public func removeObservers(){
        NotificationCenter.default.removeObserver(self, name: .didRespondToPackagePrompt, object: nil)
        NotificationCenter.default.removeObserver(self, name: .didTapAttendees, object: nil)
        super.removeObservers()
    }*/
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
