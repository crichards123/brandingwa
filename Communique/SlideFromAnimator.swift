//
//  SlideFromAnimator.swift
//  Communique
//
//  Created by Andre White on 10/4/17.
//  Copyright © 2017 Communique, LLC. All rights reserved.
//

import Foundation
public class SlideFromAnimator: NSObject {
    var presenting: Bool?
    override init() {
        super.init()
    }
    init(presentingVC:CMQLogCollectionViewController) {
        if presentingVC.isCheckIn {
            self.presenting = true
        }
        else{
            self.presenting = false
        }
        super.init()
    }
    
}
extension SlideFromAnimator: UIViewControllerAnimatedTransitioning{
    public func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.5
    }
    public func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        let containerView = transitionContext.containerView
        let fromVC = transitionContext .viewController(forKey: UITransitionContextViewControllerKey.from)
        let toVC = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to)
        let toView = transitionContext.view(forKey: UITransitionContextViewKey.to)
        let fromView = transitionContext.view(forKey: UITransitionContextViewKey.from)
        // Animate the views to their final positions.
        self.presenting = toVC?.presentedViewController != fromVC
        let containerFrame = containerView.frame
        var toStartFrame = transitionContext.initialFrame(for: toVC!)
        let toFinalFrame = containerFrame
        var fromFinalFrame = transitionContext.finalFrame(for: fromVC!)
        if presenting!==true {
            //This means that CheckIn VC is the fromView
            //toView should start at the right of the screen
            toStartFrame.origin = CGPoint.init(x: containerFrame.size.width, y: containerFrame.origin.y)
            toStartFrame.size = containerFrame.size
            fromFinalFrame.origin = CGPoint.init(x: -containerFrame.size.width, y: containerFrame.origin.y)
            fromFinalFrame.size = containerFrame.size
        }
        else{
            //CheckOut VC is the fromView
            //toView should start from the left of the screen
            toStartFrame.origin = CGPoint.init(x: -containerFrame.size.width, y: containerFrame.origin.y)
            toStartFrame.size = containerFrame.size
            fromFinalFrame.origin = CGPoint.init(x: containerFrame.size.width, y: containerFrame.origin.y)
            fromFinalFrame.size = containerFrame.size
        }
        containerView.addSubview(toView!)
        toView?.frame = toStartFrame
        UIView.animate(withDuration: self.transitionDuration(using: transitionContext), animations: {
            toView?.frame = toFinalFrame
            fromView?.frame = fromFinalFrame
        }) { (finished) in
            let success = !transitionContext.transitionWasCancelled
            if (self.presenting! && !success)||(!self.presenting! && success){
                toView?.removeFromSuperview()
            }
            transitionContext.completeTransition(success)
        }
    }
}
class InteractiveAnimator: UIPercentDrivenInteractiveTransition {
    private var panGesture : UIScreenEdgePanGestureRecognizer!
    private var contextData: UIViewControllerContextTransitioning!
    private var startEdge: UIRectEdge!
    
    override init() {
        super.init()
    }
    
    init(gestureRecognizer: UIScreenEdgePanGestureRecognizer, edge:UIRectEdge){
        panGesture = gestureRecognizer
        startEdge = edge
        super.init()
        panGesture.addTarget(self, action: #selector (handleSwipe(gesture:)))
    }
    override func startInteractiveTransition(_ transitionContext: UIViewControllerContextTransitioning) {
        super.startInteractiveTransition(transitionContext)
        contextData = transitionContext
        
        
    }
    func percentForGesture(gesture:UIScreenEdgePanGestureRecognizer)->CGFloat{
        let containerView = contextData.containerView
        let pointInView = gesture.location(in: containerView)
        let width = containerView.bounds.width
        switch startEdge {
        case UIRectEdge.right:
            let divisor = width - pointInView.x
            return divisor/width
        case UIRectEdge.left:
            return pointInView.x/width
        default:
            return 0.0
        }
    }
   
    @objc func handleSwipe(gesture:UIScreenEdgePanGestureRecognizer){
        let translation = gesture.translation(in:gesture.view!.superview!)
        var progress = (translation.x / 200)
        progress = CGFloat(fminf(fmaxf(Float(progress), 0.0), 1.0))
        switch gesture.state {
        case .began:
            //Taken care of in the VC
            break
        case .changed:
            self.update(self.percentForGesture(gesture: gesture))
        case .cancelled:
            self.cancel()
        case .ended:
            let percent = self.percentForGesture(gesture: gesture)
            if percent >= 0.5 {
                self.finish()
            } else {
                self.cancel()
            }
        default:
            self.cancel()
        }
        
    }
}
