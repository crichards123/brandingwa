//
//  CMQAPIClient.m
//  Communique
//
//  Created by Chris Hetem on 9/24/14.
//  Copyright (c) 2014 WaveRider, Inc. All rights reserved.
//

@implementation CMQAPIClient

+ (CMQAPIClient *)sharedClient{
    static CMQAPIClient *_sharedClient = nil;
    static dispatch_once_t _onceToken;
    
    dispatch_once(&_onceToken, ^{
        _sharedClient = [[CMQAPIClient alloc]init];
    });
    
    return _sharedClient;
}
-(PFQuery*)eventsQuery{
    PFQuery *query = [CMQEvent query];
    CMQUser *currentUser = [CMQUser currentUser];
    
    // Fetch all events that have a date greater than
    // midnight of current day
    NSDate *date = [NSDate date];
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSIntegerMax fromDate:date];
    [components setHour:0];
    [components setMinute:0];
    [components setSecond:0];
    NSDate *compareDate = [[NSCalendar currentCalendar] dateFromComponents:components];
    
    if (currentUser.tempAptComplexID && currentUser.tempAptComplexID.length > 0)
        [query whereKey:kParseAptComplexIDKey equalTo:currentUser.tempAptComplexID];
    else
        [query whereKey:kParseAptComplexIDKey equalTo:currentUser.aptComplexID];
    
    [query whereKey:kParseEventDateKey greaterThanOrEqualTo:compareDate];
    [query orderByAscending:kParseEventDateKey];
    [query includeKey:@"userWhoPosted"];
    return query;
}
#pragma mark - USER
-(NSString *)getDateForMsg:(CMQMessage*)message{
    //get date from data
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    
    //calendar to extract day and month compenents
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateFormatter* formatter = [[NSDateFormatter alloc]init];
    formatter.dateFormat = @"yyyy-MM-dd'T'HH:mm";
    formatter.timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
    
    NSDateComponents *components = [calendar components:(NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear) fromDate:message.createdAt];
    
    NSInteger month = [components month]-1;
    NSInteger day = [components day];
    
    NSString *monthName = [[dateFormatter shortMonthSymbols]objectAtIndex:month];
    NSString *dateNumber = [NSString stringWithFormat:@"%ld", (long)day];
    
    NSString *formatStringForHours = [NSDateFormatter dateFormatFromTemplate:@"j" options:0 locale:[NSLocale currentLocale]];
    
    NSRange containsA = [formatStringForHours rangeOfString:@"a"];
    BOOL hasAMPM = containsA.location != NSNotFound;
    
    if (hasAMPM) { dateFormatter.dateFormat = @"h:mm a";}
    else { dateFormatter.dateFormat = @"HH:mm";}
    NSString *time = [dateFormatter stringFromDate:message.createdAt];
    
    return [NSString stringWithFormat:@"%@. %@, %@", monthName, dateNumber, time];
}
/*
 Creates completion of login block. If a successful login occurs "Login" segue is performed on the viewController given as a parameter.
 
 
 */
-(PFUserResultBlock)blockWithController:(UIViewController*)controller{
    return ^(PFUser *user, NSError *error) {
        NSString *currentManagement = [[user objectForKey:@"management"] objectId];
        
        NSArray *acceptedMgmtValues = [kAcceptedManagements componentsSeparatedByString:@","];
        DLogGreen(@"Found accepted managements: %@\nFound current management: %@", acceptedMgmtValues, currentManagement);
        NSString* channel = kParsePushChannelAllKey;
        if (user && ([acceptedMgmtValues containsObject: @"all"] || [acceptedMgmtValues containsObject: currentManagement])) {
            // login successful
            // ask for push
            if (channel && channel.length > 0)
                [[CMQNotificationHandler sharedHandler]requestPushPermissions];
            
            //check user permissions to set our global variable
            [controller performSegueWithIdentifier:@"Login" sender:nil];
            
            //Associate the user with the installation to allow push notifications
            PFInstallation *installation = [PFInstallation currentInstallation];
            installation[@"user"] = user;
            installation[@"userFullName"] = user[@"fullName"];
            if (channel && channel.length > 0){
                //subscribe to channel, removing from separate call
                installation.channels = @[channel];
            }
            [installation saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error){
                if (!error) {
                    if(succeeded)DLogGreen(@"associated user with installation");
                } else {
                    // Query failed - handle an error.
                    [self handleParseError:error];
                }
            }];
            //fetch Apartment
            CMQUser* currentUser = (CMQUser*) user;
            if (currentUser.communities.count>1){
                [self fetchApartmentWithId:currentUser.complexID];
            }
            else{
                PFObject* community = (PFObject*)currentUser.communities.firstObject;
                [self fetchApartmentWithId:community.objectId];
            }
        }else{
            // The login failed, notify our delegate
            if(user) {
                [self logout];
                if([self.delegate respondsToSelector:@selector(CMQAPIClientDidFailLoginWithMessage:)])
                    [self.delegate CMQAPIClientDidFailLoginWithMessage:@"You do not have permission to access this app"];
            }
            else {
                if([self.delegate respondsToSelector:@selector(CMQAPIClientDidFailLoginWithMessage:)])
                    [self.delegate CMQAPIClientDidFailLoginWithMessage:@"Failed to login, please try again"];
            }
        }
    };  
}
-(PFArrayResultBlock)apartmentQueryCompletionBlock{
    return ^(NSArray* objects, NSError* error){
        
        
    };
}
- (void)loginWithUsername:(NSString *)username password:(NSString *)password alerts:(BOOL)alerts notifications:(BOOL)notifications channel:(NSString *)channel{
    [SVProgressHUD showWithStatus:@"Logging in"];
    
    [CMQUser logInWithUsernameInBackground:username password:password block:^(PFUser *user, NSError *error) {
        NSString *currentManagement = [[user objectForKey:@"management"] objectId];
        
        NSArray *acceptedMgmtValues = [kAcceptedManagements componentsSeparatedByString:@","];
        DLogGreen(@"Found accepted managements: %@\nFound current management: %@", acceptedMgmtValues, currentManagement);
        
        if (user && ([acceptedMgmtValues containsObject: @"all"] || [acceptedMgmtValues containsObject: currentManagement])) {
            // login successful
            // ask for push
            if (channel && channel.length > 0)
                [[CMQNotificationHandler sharedHandler]requestPushPermissions];
            
            //check user permissions to set our global variable
            if([self.delegate respondsToSelector:@selector(CMQAPIClientDidLoginSuccessfullyWithAlerts:channel:notifications:)]){
                [self.delegate CMQAPIClientDidLoginSuccessfullyWithAlerts:alerts channel:channel notifications:notifications];
                
                //Associate the user with the installation to allow push notifications
                PFInstallation *installation = [PFInstallation currentInstallation];
                installation[@"user"] = [PFUser currentUser];
                installation[@"userFullName"] = [PFUser currentUser][@"fullName"];
                [installation saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error){
                    if (!error) {
                        if(succeeded)DLogGreen(@"associated user with installation");
                    } else {
                        // Query failed - handle an error.
                        [self handleParseError:error];
                    }
                }];
            }
        }else{
            // The login failed, notify our delegate
            if(user) {
                [self logout];
                if([self.delegate respondsToSelector:@selector(CMQAPIClientDidFailLoginWithMessage:)])
                    [self.delegate CMQAPIClientDidFailLoginWithMessage:@"You do not have permission to access this app"];
            }
            else {
                if([self.delegate respondsToSelector:@selector(CMQAPIClientDidFailLoginWithMessage:)])
                    [self.delegate CMQAPIClientDidFailLoginWithMessage:@"Failed to login, please try again"];
            }
        }
    }];
}

-(void)logout{
    [CMQUser logOut];
    if([self.delegate respondsToSelector:@selector(CMQAPIClientDidLogout)]){
        [self.delegate CMQAPIClientDidLogout];
    }
}

-(void)preemptiveLogout{
    [CMQUser logOut];
    if([self.delegate respondsToSelector:@selector(CMQAPIClientDidLogoutPreemptively)]){
        [self.delegate CMQAPIClientDidLogoutPreemptively];
    }
}

- (void)updateUser:(CMQUser *)user didSubscribe:(BOOL)subscribed{
    [user saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (!error) {
            if([self.delegate respondsToSelector:@selector(CMQAPIClientDidUpdateUserWithSuccess:error:subscribedToChannel:)]){
                [self.delegate CMQAPIClientDidUpdateUserWithSuccess:succeeded error:error subscribedToChannel:subscribed];
            }
        } else {
            // Query failed - handle an error.
            [self handleParseError:error];
        }
    }];
}

-(void)setUserAlerts:(BOOL)alerts{
    //set user alert properties
    CMQUser *user =  [CMQUser currentUser];
    user.alerts = alerts;
    [user saveEventually:^(BOOL succeeded, NSError *error) {
        if (!error) {
            if(succeeded)DLogGreen(@"set user alerts");
        } else {
            // Query failed - handle an error.
            [self handleParseError:error];
        }
    }];
}

#pragma mark - PUSH

-(void)subscribeToChannel:(NSString *)channel{
    PFInstallation *installation = [PFInstallation currentInstallation];
    if(channel)installation.channels = @[channel];
    [installation saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (!error) {
            if(succeeded)DLogGreen(@"saved installation push channel");
        } else {
            // Query failed - handle an error.
            [self handleParseError:error];
        }
    }];
}

#pragma mark - FETCH

- (void)fetchResidents{
    NSMutableArray *allResidents = [NSMutableArray array];
    __block NSMutableArray *objectsPlaceholder = [NSMutableArray array];
    __block NSUInteger limit = 1000;
    __block NSUInteger skip = 0;
    
    PFQuery *query = [CMQUser query];
    PFQuery *appendQuery = [CMQUser query];
    
    CMQUser *currentUser = [CMQUser currentUser];
    
    if (currentUser.tempAptComplexID && currentUser.tempAptComplexID.length > 0){
        [query whereKey:kParseAptComplexIDKey equalTo:currentUser.tempAptComplexID];
        [appendQuery whereKey:kParseAptComplexIDKey equalTo:currentUser.tempAptComplexID];
    }else{
        [query whereKey:kParseAptComplexIDKey equalTo:currentUser.aptComplexID];
        [appendQuery whereKey:kParseAptComplexIDKey equalTo:currentUser.aptComplexID];
    }
    
    [query whereKey:@"userRole" equalTo:@"Resident"];
    [query orderByAscending:@"fullNameToLowercase"];
    [query setLimit:limit];
    [query setSkip:skip];
    
    [appendQuery whereKey:@"userRole" equalTo:@"Resident"];
    [appendQuery orderByAscending:@"fullNameToLowercase"];
    [appendQuery setLimit:limit];
    [appendQuery setSkip:skip];
    
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            objectsPlaceholder = [objects mutableCopy];
            [allResidents addObjectsFromArray:objects];
            
            dispatch_queue_t residentsQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
            dispatch_async(residentsQueue, ^{
                while (objectsPlaceholder.count == limit) {
                    skip += limit;
                    [appendQuery setSkip:skip];
                    
                    objectsPlaceholder = (NSMutableArray *)[appendQuery findObjects];
                    [allResidents addObjectsFromArray:objectsPlaceholder];
                }
                
                //successful api request, notify the caller
                if([self.delegate respondsToSelector:@selector(CMQAPIClientDidFetchResidentsSuccessfully:)]){
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self.delegate CMQAPIClientDidFetchResidentsSuccessfully:allResidents];
                    });
                }
            });
        }else{ //api request failed, notify caller
            [self sendFetchErrorMessage:error.localizedDescription isCache:NO];
            DLogRed(@"error fetching messages: %@", error.localizedDescription);
            [self handleParseError:error];
        }
    }];
}

- (void)fetchMessages{
    CMQUser *currentUser = [CMQUser currentUser];
    
    NSString *communityIDToUse = currentUser.aptComplexID;
    if (currentUser.tempAptComplexID && currentUser.tempAptComplexID.length > 0)
        communityIDToUse = currentUser.tempAptComplexID;
    
    if (![currentUser.userRole isEqualToString:kPermissionInterestedParty]){
        PFQuery *query;
        if ([currentUser.userRole isEqualToString:kPermissionResident]){ // Resident - restrict viewing
            // Direct Message (current user)
            PFQuery *directMessageQuery = [CMQMessage query];
            [directMessageQuery whereKey:kParseAptComplexIDKey equalTo:communityIDToUse];
            [directMessageQuery whereKey:@"isIndividualMessage" equalTo:[NSNumber numberWithBool:YES]];
            [directMessageQuery whereKey:@"recipients" equalTo:currentUser.objectId];
            //PFQuery* packageQuery =[CMQPackage query];
            //[packageQuery whereKeyDoesNotExist:@"checkedOutDate"];
    
            
            //[directMessageQuery whereKey:@"Package" matchesQuery:packageQuery];
            //[directMessageQuery whereKey:@"messageContent" containsString:@"This is a confirmation that you have picked up your package"];
        
            // Messages with specific recipients (no groups)
            PFQuery *specificMessageQuery = [CMQMessage query];
            [specificMessageQuery whereKey:kParseAptComplexIDKey equalTo:communityIDToUse];
            [specificMessageQuery whereKey:@"recipients" equalTo:currentUser.buildingUnit];
            [specificMessageQuery whereKeyDoesNotExist:@"groups"];
            [specificMessageQuery whereKey:@"scheduleSent" equalTo:[NSNumber numberWithBool:YES]];
            
            
            // Messages with specific recipients (with groups)
            PFQuery *specificMessageQueryWithGroups = [CMQMessage query];
            [specificMessageQueryWithGroups whereKey:kParseAptComplexIDKey equalTo:communityIDToUse];
            [specificMessageQueryWithGroups whereKey:@"recipients" equalTo:currentUser.buildingUnit];
            [specificMessageQueryWithGroups whereKey:@"groups" containedIn:currentUser.groups];
            
            // Messages without specified recipients (no groups)
            PFQuery *nonSpecificMessageQuery = [CMQMessage query];
            [nonSpecificMessageQuery whereKey:kParseAptComplexIDKey equalTo:communityIDToUse];
            [nonSpecificMessageQuery whereKeyDoesNotExist:@"recipients"];
            [nonSpecificMessageQuery whereKeyDoesNotExist:@"groups"];

            // Messages without specified recipients (with groups)
            PFQuery *nonSpecificMessageQueryWithGroups = [CMQMessage query];
            [nonSpecificMessageQueryWithGroups whereKey:kParseAptComplexIDKey equalTo:communityIDToUse];
            [nonSpecificMessageQueryWithGroups whereKeyDoesNotExist:@"recipients"];
            [nonSpecificMessageQueryWithGroups whereKey:@"groups" containedIn:currentUser.groups];
            
            // Announcements without specified groups
            PFQuery *announcementQuery = [CMQMessage query];
            [announcementQuery whereKey:@"isAnnouncement" equalTo:[NSNumber numberWithBool:YES]];
            [announcementQuery whereKey:@"recipients" equalTo:communityIDToUse];
            [announcementQuery whereKeyDoesNotExist:@"groups"];
            
            // Announcements with specific groups
            PFQuery *announcementQueryWithGroups = [CMQMessage query];
            [announcementQueryWithGroups whereKey:@"isAnnouncement" equalTo:[NSNumber numberWithBool:YES]];
            [announcementQueryWithGroups whereKey:@"recipients" equalTo:communityIDToUse];
            [announcementQueryWithGroups whereKey:@"groups" containedIn:currentUser.groups];
            
            query = [PFQuery orQueryWithSubqueries:[NSArray arrayWithObjects:
                                                    directMessageQuery,
                                                    specificMessageQuery,
                                                    specificMessageQueryWithGroups,
                                                    nonSpecificMessageQuery,
                                                    nonSpecificMessageQueryWithGroups,
                                                    announcementQuery,
                                                    announcementQueryWithGroups,
                                                    nil]];
        }else{ // Staff/Admin - ignore buildingUnit and groups (exclude DMs)
            // Direct Message (current user)
            PFQuery *directMessageQuery = [CMQMessage query];
            [directMessageQuery whereKey:kParseAptComplexIDKey equalTo:communityIDToUse];
            [directMessageQuery whereKey:@"isIndividualMessage" equalTo:[NSNumber numberWithBool:YES]];
            [directMessageQuery whereKey:@"userWhoPosted" equalTo:currentUser];
            [directMessageQuery whereKeyDoesNotExist:@"Package"];
            
            // Messages to this community
            PFQuery *messageQuery = [CMQMessage query];
            [messageQuery whereKey:kParseAptComplexIDKey equalTo:communityIDToUse];
            [messageQuery whereKey:@"isIndividualMessage" notEqualTo:[NSNumber numberWithBool:YES]];
            
            // Announcements to this community
            PFQuery *announcementQuery = [CMQMessage query];
            [announcementQuery whereKey:@"isAnnouncement" equalTo:[NSNumber numberWithBool:YES]];
            [announcementQuery whereKey:@"isIndividualMessage" notEqualTo:[NSNumber numberWithBool:YES]];
            [announcementQuery whereKey:@"recipients" equalTo:communityIDToUse];
            
            query = [PFQuery orQueryWithSubqueries:[NSArray arrayWithObjects:
                                                    directMessageQuery,
                                                    messageQuery,
                                                    announcementQuery,
                                                    nil]];
        }
        
        [query orderByDescending:kParseObjectCreateDateKey];
        [query includeKey:@"userWhoPosted"];
        [query includeKey:@"Package"];
        [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
            if(!error){
                //successful api request, notify the caller
                if([self.delegate respondsToSelector:@selector(CMQAPIClientDidFetchMessagesSuccessfully:)]){
                    [self.delegate CMQAPIClientDidFetchMessagesSuccessfully:objects];
                }
            }else{
                [self sendFetchErrorMessage:error.localizedDescription isCache:NO];
                DLogRed(@"error fetching messages: %@", error.localizedDescription);
                [self handleParseError:error];
            }
        }];
    }else if ([currentUser.userRole isEqualToString:kPermissionInterestedParty]) {
        PFQuery *userQuery = [CMQUser query];
        [userQuery whereKey:@"objectId" containedIn:currentUser.associatedUsers];
        [userQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
            if (!error) {
                NSMutableArray *queries = [[NSMutableArray alloc] init];
                
                if (currentUser.associatedUsers && [currentUser.associatedUsers count] > 0){
                    NSMutableArray *userAttributes = [[NSMutableArray alloc] init];
                    for (int i = 0; i < [objects count]; i ++){
                        CMQUser *associatedUser = objects[i];
                        
                        if (associatedUser.buildingUnit)
                            [userAttributes addObject:associatedUser.buildingUnit];
                    }
                    
                    // Public Messages with specific recipients
                    PFQuery *specificMessageQuery = [CMQMessage query];
                    [specificMessageQuery whereKey:kParseAptComplexIDKey equalTo:communityIDToUse];
                    [specificMessageQuery whereKey:@"recipients" containedIn:userAttributes];
                    [specificMessageQuery whereKey:@"isPrivate" equalTo:[NSNumber numberWithBool:NO]];
                    
                    [queries addObject:specificMessageQuery];
                }

                // Public Messages without specific recipients
                PFQuery *messageQuery = [CMQMessage query];
                [messageQuery whereKey:kParseAptComplexIDKey equalTo:communityIDToUse];
                [messageQuery whereKeyDoesNotExist:@"recipients"];
                [messageQuery whereKey:@"isPrivate" equalTo:[NSNumber numberWithBool:NO]];
                
                [queries addObject:messageQuery];
                
                PFQuery *query;
                query = [PFQuery orQueryWithSubqueries:queries];
                [query orderByDescending:kParseObjectCreateDateKey];
                [query includeKey:@"userWhoPosted"];
                [query includeKey:@"Package"];
                [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
                    if(!error){
                        //successful api request, notify the caller
                        if([self.delegate respondsToSelector:@selector(CMQAPIClientDidFetchMessagesSuccessfully:)]){
                            [self.delegate CMQAPIClientDidFetchMessagesSuccessfully:objects];
                        }
                    }else{
                        [self sendFetchErrorMessage:error.localizedDescription isCache:NO];
                        DLogRed(@"error fetching messages: %@", error.localizedDescription);
                        [self handleParseError:error];
                    }
                }];
            }else{
                [self sendFetchErrorMessage:error.localizedDescription isCache:NO];
                DLogRed(@"error fetching messages: %@", error.localizedDescription);
                [self handleParseError:error];
            }
        }];
    }
}

- (void)fetchEvents{
    PFQuery *query = [CMQEvent query];
    CMQUser *currentUser = [CMQUser currentUser];
    
    // Fetch all events that have a date greater than
    // midnight of current day
    NSDate *date = [NSDate date];
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSIntegerMax fromDate:date];
    [components setHour:0];
    [components setMinute:0];
    [components setSecond:0];
    NSDate *compareDate = [[NSCalendar currentCalendar] dateFromComponents:components];
    
    if (currentUser.tempAptComplexID && currentUser.tempAptComplexID.length > 0)
        [query whereKey:kParseAptComplexIDKey equalTo:currentUser.tempAptComplexID];
    else
        [query whereKey:kParseAptComplexIDKey equalTo:currentUser.aptComplexID];
    
    [query whereKey:kParseEventDateKey greaterThanOrEqualTo:compareDate];
    [query orderByAscending:kParseEventDateKey];
    [query includeKey:@"userWhoPosted"];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if(!error){
            //successful api request, notify the caller
            if([self.delegate respondsToSelector:@selector(CMQAPIClientDidFetchEventsSuccessfully:)]){
                [self.delegate CMQAPIClientDidFetchEventsSuccessfully:objects];
            }
        }else{  //api request failed, notify caller
            [self sendFetchErrorMessage:error.localizedDescription isCache:NO];
            DLogRed(@"error fetching messages: %@", error.localizedDescription);
            [self handleParseError:error];

        }
    }];
}

-(void)fetchEventAttendees:(CMQEvent *)event{
    PFQuery *query = [CMQUser query];
    [query whereKey:@"objectId" containedIn:event.eventAttendees];
    [query orderByAscending:@"fullNameToLowercase"];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            //successful api request, notify the caller
            if([self.delegate respondsToSelector:@selector(CMQAPIClientDidFetchEventAttendeesSuccessfully:)]){
                [self.delegate CMQAPIClientDidFetchEventAttendeesSuccessfully:objects];
            }
        }else{ //api request failed, notify caller
            [self sendFetchErrorMessage:error.localizedDescription isCache:NO];
            DLogRed(@"error fetching messages: %@", error.localizedDescription);
            [self handleParseError:error];
        }
    }];
}

- (void)fetchNews{
    PFQuery *query = [CMQNewsArticle query];
    CMQUser *currentUser = [CMQUser currentUser];
    
    if (currentUser.tempAptComplexID && currentUser.tempAptComplexID.length > 0)
        [query whereKey:kParseAptComplexIDKey equalTo:currentUser.tempAptComplexID];
    else
        [query whereKey:kParseAptComplexIDKey equalTo:currentUser.aptComplexID];
    
    [query orderByDescending:kParseObjectCreateDateKey];
    [query includeKey:@"userWhoPosted"];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if(!error){
            //successful api request, notify the caller
            if([self.delegate respondsToSelector:@selector(CMQAPIClientDidFetchNewsSuccessfully:)]){
                [self.delegate CMQAPIClientDidFetchNewsSuccessfully:objects];
            }
        }else{
            [self sendFetchErrorMessage:error.localizedDescription isCache:NO];
            DLogRed(@"error fetching messages: %@", error.localizedDescription);
            [self handleParseError:error];
        }
    }];
}

- (void)fetchCommunityPosts{
    NSDate *date = [NSDate date];
    NSDate *pastDate = [date dateByAddingTimeInterval:-90*24*60*60]; // 90 days ago
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSIntegerMax fromDate:pastDate];
    [components setHour:0];
    [components setMinute:0];
    [components setSecond:0];
    NSDate *pastDateZeroed = [[NSCalendar currentCalendar] dateFromComponents:components];
    
    // Grab the ids of current user's communities
    NSArray *communities = [CMQUser currentUser].communities;
    NSMutableArray *communityIDS = [[NSMutableArray alloc] init];
    for (PFObject *object in communities){
        [communityIDS addObject:[object objectId]];
    }
    
    NSMutableArray *communityPosts = [[NSMutableArray alloc] init];
    
    // Messages to this community
    PFQuery *messageQuery = [CMQMessage query];
    [messageQuery whereKey:kParseAptComplexIDKey containedIn:communityIDS];
    [messageQuery whereKey:@"isIndividualMessage" notEqualTo:[NSNumber numberWithBool:YES]];
    [messageQuery whereKey:kParseObjectCreateDateKey greaterThanOrEqualTo:pastDateZeroed];
    
    // Announcements to this community
    PFQuery *announcementQuery = [CMQMessage query];
    [announcementQuery whereKey:@"recipients" containedIn:communityIDS];
    [announcementQuery whereKey:@"isAnnouncement" equalTo:[NSNumber numberWithBool:YES]];
    [announcementQuery whereKey:@"isIndividualMessage" notEqualTo:[NSNumber numberWithBool:YES]];
    [announcementQuery whereKey:kParseObjectCreateDateKey greaterThanOrEqualTo:pastDateZeroed];
    
    PFQuery *query = [PFQuery orQueryWithSubqueries:[NSArray arrayWithObjects:
                                                     messageQuery,
                                                     announcementQuery,
                                                     nil]];
    
    [query orderByDescending:kParseObjectCreateDateKey];
    [query includeKey:@"userWhoPosted"];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if(!error){
            [communityPosts addObjectsFromArray:objects];
            
            PFQuery *eventQuery = [CMQEvent query];
            [eventQuery whereKey:kParseAptComplexIDKey containedIn:communityIDS];
            [eventQuery orderByDescending:kParseObjectCreateDateKey];
            [eventQuery whereKey:kParseObjectCreateDateKey greaterThanOrEqualTo:pastDateZeroed];
            [eventQuery includeKey:@"userWhoPosted"];
            [eventQuery findObjectsInBackgroundWithBlock:^(NSArray *events, NSError *error) {
                if(!error){
                    [communityPosts addObjectsFromArray:events];
                    
                    PFQuery *newsQuery = [CMQNewsArticle query];
                    [newsQuery whereKey:kParseAptComplexIDKey containedIn:communityIDS];
                    [newsQuery orderByDescending:kParseObjectCreateDateKey];
                    [newsQuery whereKey:kParseObjectCreateDateKey greaterThanOrEqualTo:pastDateZeroed];
                    [newsQuery includeKey:@"userWhoPosted"];
                    [newsQuery findObjectsInBackgroundWithBlock:^(NSArray *newsArticles, NSError *error) {
                        if(!error){
                            [communityPosts addObjectsFromArray:newsArticles];
                            
                            NSArray *sortedCommunityPosts;
                            sortedCommunityPosts = [communityPosts sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
                                NSDate *first = [(PFObject*)a createdAt];
                                NSDate *second = [(PFObject*)b createdAt];
                                return [second compare:first];
                            }];
                            
                            // successful api request, notify the caller
                            if([self.delegate respondsToSelector:@selector(CMQAPIClientDidFetchCommunityPostsSuccessfully:)]){
                                [self.delegate CMQAPIClientDidFetchCommunityPostsSuccessfully:sortedCommunityPosts];
                            }
                        }else{
                            [self sendFetchErrorMessage:error.localizedDescription isCache:NO];
                            DLogRed(@"error fetching news articles: %@", error.localizedDescription);
                            [self handleParseError:error];
                        }
                    }];
                }else{
                    [self sendFetchErrorMessage:error.localizedDescription isCache:NO];
                    DLogRed(@"error fetching events: %@", error.localizedDescription);
                    [self handleParseError:error];
                }
            }];
        }else{
            [self sendFetchErrorMessage:error.localizedDescription isCache:NO];
            DLogRed(@"error fetching messages: %@", error.localizedDescription);
            [self handleParseError:error];
        }
    }];
}

- (void)fetchNotificationQueue{
    PFQuery *query = [CMQPackage query];
    
    CMQUser *currentUser = [CMQUser currentUser];
    if (currentUser.tempAptComplexID && currentUser.tempAptComplexID.length > 0)
        [query whereKey:kParseAptComplexIDKey equalTo:currentUser.tempAptComplexID];
    else
        [query whereKey:kParseAptComplexIDKey equalTo:currentUser.aptComplexID];

    [query whereKey:@"isRecipientNotified" notEqualTo:[NSNumber numberWithBool:YES]];
    
    [query orderByDescending:kParseObjectCreateDateKey];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if(!error){
            //successful api request, notify the caller
            if([self.delegate respondsToSelector:@selector(CMQAPIClientDidFetchQueueSuccessfully:)]){
                [self.delegate CMQAPIClientDidFetchQueueSuccessfully:objects];
            }
        }else{
            [self sendFetchErrorMessage:error.localizedDescription isCache:NO];
            DLogRed(@"error fetching queue: %@", error.localizedDescription);
            [self handleParseError:error];
        }
    }];
}

- (void)fetchCheckoutQueue{
    PFQuery *query = [CMQPackage query];
    
    CMQUser *currentUser = [CMQUser currentUser];
    if (currentUser.tempAptComplexID && currentUser.tempAptComplexID.length > 0)
        [query whereKey:kParseAptComplexIDKey equalTo:currentUser.tempAptComplexID];
    else
        [query whereKey:kParseAptComplexIDKey equalTo:currentUser.aptComplexID];
    
    [query whereKey:@"isRecipientNotified" notEqualTo:[NSNumber numberWithBool:NO]];
    [query whereKey:@"pickedUp" notEqualTo:[NSNumber numberWithBool:YES]];
    
    [query orderByDescending:kParseObjectCreateDateKey];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if(!error){
            //successful api request, notify the caller
            if([self.delegate respondsToSelector:@selector(CMQAPIClientDidFetchQueueSuccessfully:)]){
                [self.delegate CMQAPIClientDidFetchQueueSuccessfully:objects];
            }
        }else{
            [self sendFetchErrorMessage:error.localizedDescription isCache:NO];
            DLogRed(@"error fetching queue: %@", error.localizedDescription);
            [self handleParseError:error];
        }
    }];
}

- (void)fetchApartmentWithId:(NSString *)objectId{
    PFQuery *query = [CMQApartment query];
    query.cachePolicy = kPFCachePolicyCacheThenNetwork;
    [query whereKey:@"objectId" equalTo:objectId];
    __block BOOL cachedResults = YES;
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
            if(!error){ //successful request, notify caller
                if([self.delegate respondsToSelector:@selector(CMQAPIClientDidFetchApartmentSuccessfully:)]){
                    [self.delegate CMQAPIClientDidFetchApartmentSuccessfully:objects];
                }
            }else{  //failed request, notify caller
                [self sendFetchErrorMessage:error.localizedDescription isCache:cachedResults];
                [self handleParseError:error];
            }
        if(cachedResults)cachedResults = NO;

    }];
}

- (void)fetchApartmentsWithIds:(NSArray *)Ids{
    PFQuery *query = [CMQApartment query];
    query.cachePolicy = kPFCachePolicyCacheThenNetwork;
    [query whereKey:@"objectId" containedIn:Ids];
    __block BOOL cachedResults = YES;
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if(!error){ //successful request, notify caller
            if([self.delegate respondsToSelector:@selector(CMQAPIClientDidFetchApartmentsSuccessfully:)]){
                [self.delegate CMQAPIClientDidFetchApartmentsSuccessfully:objects];
            }
        }else{  //failed request, notify caller
            [self sendFetchErrorMessage:error.localizedDescription isCache:cachedResults];
            [self handleParseError:error];
        }
        if(cachedResults)cachedResults = NO;
        
    }];
}

- (void)switchApartmentWithId:(NSString *)objectId{
    CMQUser *user =  [CMQUser currentUser];
    user[@"tempAptComplexID"] = objectId;
    __block BOOL cachedResults = YES;
    [user saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if(succeeded){
            if([self.delegate respondsToSelector:@selector(CMQAPIClientDidSwitchApartmentSuccessfully)]){
                [self.delegate CMQAPIClientDidSwitchApartmentSuccessfully];
            }
        }else{
            [self sendSwitchErrorMessage:error.localizedDescription isCache:cachedResults];
            if(error) [self handleParseError:error];
        }
        
        if(cachedResults)cachedResults = NO;
    }];
}

- (void)fetchManagementWithId:(NSString *)objectId{
    PFQuery *query = [CMQManagement query];
    query.cachePolicy = kPFCachePolicyCacheThenNetwork;
    [query whereKey:@"objectId" equalTo:objectId];
    __block BOOL cachedResults = YES;
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if(!error){ //successful request, notify caller
            if([self.delegate respondsToSelector:@selector(CMQAPIClientDidFetchManagementSuccessfully:)]){
                [self.delegate CMQAPIClientDidFetchManagementSuccessfully:objects];
            }
        }else{  //failed request, notify caller
            [self sendFetchErrorMessage:error.localizedDescription isCache:cachedResults];
            [self handleParseError:error];
        }
        if(cachedResults)cachedResults = NO;
        
    }];
}

- (void)fetchUpdate{
    //check for any updates
    PFQuery *query = [CMQUpdate query];
    [query getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
        if (error){
            [self sendFetchErrorMessage:error.localizedDescription isCache:NO];
            [self handleParseError:error];
        }else{
            if([self.delegate respondsToSelector:@selector(CMQAPIClientDidFetchUpdateSuccessfully:)]){
                [self.delegate CMQAPIClientDidFetchUpdateSuccessfully:object];
            }
        }
    }];
}

-(NSMutableString *)fetchUserAttributes{
    NSMutableString *attributes = [NSMutableString stringWithString:@""];
    
    CMQApartment *apartment = [CMQAD apartment];
    if (apartment.aptName && apartment.aptName.length > 0)
        [attributes appendString:[NSString stringWithFormat:@"--- Community Info ---\n%@", apartment.aptName]];
    if (apartment.aptPhone && apartment.aptPhone.length> 0)
        [attributes appendString:[NSString stringWithFormat:@"\n%@", apartment.aptPhone]];
    
    CMQManagement *management = [CMQAD management];
    if (management.companyName && management.companyName.length > 0)
        [attributes appendString:[NSString stringWithFormat:@"\n%@", management.companyName]];
    
    CMQUser *currentUser = [CMQUser currentUser];
    if (currentUser.fullName && currentUser.fullName.length > 0)
        [attributes appendString:[NSString stringWithFormat:@"\n\n--- User Info ---\n%@", currentUser.fullName]];
    
    if (currentUser.email && currentUser.email.length> 0)
        [attributes appendString:[NSString stringWithFormat:@"\n%@", currentUser.email]];
    
    if (currentUser.phone && currentUser.phone.length> 0)
        [attributes appendString:[NSString stringWithFormat:@"\n%@", currentUser.phone]];
    
    if (currentUser.buildingUnit && currentUser.buildingUnit.length> 0)
        [attributes appendString:[NSString stringWithFormat:@"\n%@", currentUser.buildingUnit]];
    
    return attributes;
}

#pragma mark - POST

- (void)postPackage:(CMQPackage *)package{
    [package saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if(succeeded){
            [self sendPostSuccessForObject:package];
        }else{
            [self sendPostErrorMessage:error.localizedDescription forObject:package];
            if(error) [self handleParseError:error];
        }
    }];
    
}

- (void)postEvent:(CMQEvent *)event{
    [event saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if(succeeded){
            [self sendPostSuccessForObject:event];
        }else{
            [self sendPostErrorMessage:error.localizedDescription forObject:event];
            if(error) [self handleParseError:error];
        }
    }];

}

- (void)postMessage:(CMQMessage *)message{
    
    [message saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if(succeeded){
            [self sendPostSuccessForObject:message];
        }else{
            [self sendPostErrorMessage:error.localizedDescription forObject:message];
            if(error) [self handleParseError:error];
        }
    }];
    
}

- (void)postNews:(CMQNewsArticle *)newsArticle{
    
    [newsArticle saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if(succeeded){
            [self sendPostSuccessForObject:newsArticle];
        }else{
            [self sendPostErrorMessage:error.localizedDescription forObject:newsArticle];
            if(error) [self handleParseError:error];
        }
    }];
    
}

#pragma mark - Internal/Delegate Message

//ERROR//

-(void)sendFetchErrorMessage:(NSString *)errorMessage isCache:(BOOL)isCache{
    if([self.delegate respondsToSelector:@selector(CMQAPIClientDidFailFetchWithMessage:isCache:)]){
        [self.delegate CMQAPIClientDidFailFetchWithMessage:errorMessage isCache:isCache];
    }
}

-(void)sendSwitchErrorMessage:(NSString *)errorMessage isCache:(BOOL)isCache{
    if([self.delegate respondsToSelector:@selector(CMQAPIClientDidFailSwitchWithMessage:isCache:)]){
        [self.delegate CMQAPIClientDidFailSwitchWithMessage:errorMessage isCache:isCache];
    }
}

-(void)sendPostErrorMessage:(NSString *)errorMessage forObject:(id)object{
    if([self.delegate respondsToSelector:@selector(CMQAPIClientDidFailToPostObject:withMessage:)]){
        [self.delegate CMQAPIClientDidFailToPostObject:object withMessage:errorMessage];
    }
}


//SUCCESS//
-(void)sendPostSuccessForObject:(PFObject *)object{
    if([self.delegate respondsToSelector:@selector(CMQAPIClientDidPostObjectSuccessfully:)]){
        [self.delegate CMQAPIClientDidPostObjectSuccessfully:object];
    }
}

#pragma mark - ERROR Handling
- (void)handleParseError:(NSError *)error {
    if (![error.domain isEqualToString:PFParseErrorDomain]) {
        return;
    }
    
    switch (error.code) {
        case kPFErrorInvalidSessionToken: {
            [self handleInvalidSessionTokenError];
            break;
        }
    }
}

// force user to log back in, to get new session
- (void)handleInvalidSessionTokenError {
    [SVProgressHUD showInfoWithStatus:@"Your session has expired, please login to continue."];
    [self logout];
}

@end
