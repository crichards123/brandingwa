//
//  CMQAPIClient.h
//  Communique
//
//  Created by Chris Hetem on 9/24/14.
//  Copyright (c) 2014 WaveRider, Inc. All rights reserved.
//

//@import Foundation;
#import <Foundation/Foundation.h>
#import "CMQEvent.h"
#import "CMQMessage.h"
#import "CMQNewsArticle.h"
#import "CMQPackage.h"
#import "CMQUser.h"
#import "CMQUpdate.h"
#import "CMQApartment.h"
#import "CMQManagement.h"
//@import Parse;
//#import <Parse/Parse.h>

#pragma mark - CMQAPIClient

@protocol CMQAPIClientDelegate;

@interface CMQAPIClient : NSObject

@property (weak) id<CMQAPIClientDelegate> delegate;
@property (readonly) PFUserResultBlock block;
-(PFQuery*)eventsQuery;
-(NSString *)getDateForMsg:(CMQMessage*)message;
-(PFUserResultBlock)blockWithController:(UIViewController*)controller;
+ (CMQAPIClient *)sharedClient;


#pragma mark - USER

/**
 *  logs in a user to parse
 *
 *  @param username      the user's username (in email form)
 *  @param password      the user's password
 *  @param alerts        whether or not they want to sign up for email alerts
 *  @param notifications whether or not they want to sign up for push notifications
 *  @param channel       the parse push channel they want to subscribe to.
 */
- (void)loginWithUsername:(NSString *)username password:(NSString *)password alerts:(BOOL)alerts notifications:(BOOL)notifications channel:(NSString *)channel;

/**
 *  logs a user out of parse
 */
- (void)logout;

/**
 *  logs a user out of parse preemptively before accessing app
 */
- (void)preemptiveLogout;

/**
 *  sets the user's email alerts setting
 *
 *  @param alerts whether or not they want alerts
 */
- (void)setUserAlerts:(BOOL)alerts;

/**
 *  updates a user's details
 *
 *  @param user       the user whose details need updating
 *  @param subscribed whether or not they want to subscribe to push notifications
 */
- (void)updateUser:(CMQUser *)user didSubscribe:(BOOL)subscribed;


#pragma mark - FETCH

/**
 *  fetches all residents associated with the user's apartment complex
 */
- (void)fetchResidents;

/**
 *  fetches all messages associated with the user's apartment complex
 */
- (void)fetchMessages;

/**
 *  fetches all events associated with the user's apartment complex
 */
- (void)fetchEvents;

/**
 * Fetches users from event.eventAttendees
 */
-(void)fetchEventAttendees:(CMQEvent *)event;

/**
 *  fetches all news associated with the user's apartment complex
 */
- (void)fetchNews;

/**
 *  fetches all posts associated with a user's communities
 */
- (void)fetchCommunityPosts;

/**
 *  fetches notificationQueue for user's apartment complex
 */
- (void)fetchNotificationQueue;


- (void)fetchCheckoutQueue;

/**
 *  fetches the apartment with the given objectId
 *
 *  @param objectId the currentUser.apartComplexId equivalent to an apartment's objectId
 */
- (void)fetchApartmentWithId:(NSString *)objectId;

/**
 *  fetches the apartments with the given objectIds
 *
 *  @param Ids an array of NSStrings equivalent to a apartment(s) objectId
 */
- (void)fetchApartmentsWithIds:(NSArray *)Ids;

/**
 *  switches the current apartment with the given objectId
 *
 *  @param objectId the currentUser.apartComplexId equivalent to an apartment's objectId
 */
- (void)switchApartmentWithId:(NSString *)objectId;

/**
 *  fetches the management company with the given objectId
 *
 *  @param objectId the currentUser's apartment.management equivalent to a management object's objectId
 */
- (void)fetchManagementWithId:(NSString *)objectId;

/**
 *  checks update object to see if this is latest release
 */
- (void)fetchUpdate;

/**
 * Constructs NSMutableString of user's attributes,
 * primarily used for email
 */
-(NSMutableString *)fetchUserAttributes;


#pragma mark - POST

/**
 *  posts a package to parse
 *
 *  @param package the package to post
 */
- (void)postPackage:(CMQPackage *)package;

/**
 *  posts an event to parse
 *
 *  @param event the event to post
 */
- (void)postEvent:(CMQEvent *)event;

/**
 *  posts a message to parse
 *
 *  @param message the message to post
 */
- (void)postMessage:(CMQMessage *)message;

/**
 *  posts a news article to parse
 *
 *  @param newsArticle the news article to post
 */
- (void)postNews:(CMQNewsArticle *)newsArticle;


#pragma mark - PUSH

/**
 *  subscribes a user to the given parse channel
 *
 *  @param channel the channel to subscribe the user to
 */
- (void)subscribeToChannel:(NSString *)channel;

#pragma mark - ERROR handling

/**
 *  called when a request responds with an error
 *
 *  @param error  the error returned
 */
- (void)handleParseError:(NSError *)error;

@end

#pragma mark - CMQAPIClientDelegate

//API Client Delegate
@protocol CMQAPIClientDelegate <NSObject>

@optional

#pragma mark - USER

/**
 *  called when a user successfully logs in
 *
 *  @param alerts        whether or not the user subscribed to email alerts
 *  @param channel       the parse push channel the user subscribed to
 *  @param notifications whether or not the user subscribed to parse push
 */
- (void)CMQAPIClientDidLoginSuccessfullyWithAlerts:(BOOL)alerts channel:(NSString *)channel notifications:(BOOL)notifications;

/**
 *  called when the user's login attemp fails.
 *
 *  @param message the error message as to why the login failed
 */
- (void)CMQAPIClientDidFailLoginWithMessage:(NSString *)message;

/**
 *  called when the user successfully logs out
 */
- (void)CMQAPIClientDidLogout;

/**
 *  called when the user successfully logs out
 */
- (void)CMQAPIClientDidLogoutPreemptively;

/**
 *  called when the user attempts to update their details
 *
 *  @param success    whether or not the update was successful on parse
 *  @param error      the error, if any occurred
 *  @param subscribed whether or not they subscribed to a channel (this only matters if they weren't prior to saving)
 */
- (void)CMQAPIClientDidUpdateUserWithSuccess:(BOOL)success error:(NSError *)error subscribedToChannel:(BOOL)subscribed;


#pragma mark - FETCH

/**
 *  called when residents were successfully fetched from parse
 *
 *  @param residents the residents that were fetched
 */
- (void)CMQAPIClientDidFetchResidentsSuccessfully:(NSMutableArray *)residents;

/**
 *  called when events were successfully fetched from parse
 *
 *  @param events the events that were fetched
 */
- (void)CMQAPIClientDidFetchEventsSuccessfully:(NSArray *)events;

/**
 *  called when events attendees were successfully fetched from parse
 *
 *  @param attendees the attendees that were fetched
 */
- (void)CMQAPIClientDidFetchEventAttendeesSuccessfully:(NSArray *)attendees;

/**
 *  called when news was successfully fetched from parse
 *
 *  @param news the news article's that were successfully fetched
 */
- (void)CMQAPIClientDidFetchNewsSuccessfully:(NSArray *)news;

/**
 *  called when messages were successfully fetched from parse
 *
 *  @param messages the messages that were fetched
 */
- (void)CMQAPIClientDidFetchMessagesSuccessfully:(NSArray *)messages;

/**
 *  called when posts were successfully fetched from parse
 *
 *  @param posts the posts that were fetched
 */
- (void)CMQAPIClientDidFetchCommunityPostsSuccessfully:(NSArray *)posts;

/**
 *  called when queue is successfully fetched from parse
 *
 *  @param queue the queue that was fetched
 */
- (void)CMQAPIClientDidFetchQueueSuccessfully:(NSArray *)queue;

/**
 *  called when the user's apartment was successfully fetched from parse
 *
 *  @param apartments the apartments fetched from parse (this should only ever contain 1 object)
 */
- (void)CMQAPIClientDidFetchApartmentSuccessfully:(NSArray *)apartments;

/**
 *  called when the user's apartments were successfully fetched from parse
 *
 *  @param apartments the apartments fetched from parse (this could contain > 1 object)
 */
- (void)CMQAPIClientDidFetchApartmentsSuccessfully:(NSArray *)apartments;

/**
 *  called when the user's apartment was successfully switched
 */
- (void)CMQAPIClientDidSwitchApartmentSuccessfully;

/**
 *  called when the user's apartment was successfully fetched from parse
 *
 *  @param mgmt the mgmt fetched from parse (this should only ever contain 1 object)
 */
- (void)CMQAPIClientDidFetchManagementSuccessfully:(NSArray *)mgmt;

/**
 *  called when update object is fetched from parse
 *
 *  @param update the update fetched from parse (this should only ever contain 1 object)
 */
- (void)CMQAPIClientDidFetchUpdateSuccessfully:(PFObject *)update;

/**
 *  called when a fetch attempt fails
 *
 *  @param message the message associated with the error that occurred
 *  @param isCache whether or not this fetch was from cache
 */
- (void)CMQAPIClientDidFailFetchWithMessage:(NSString *)message isCache:(BOOL)isCache;

/**
 *  called when a switch attempt fails
 *
 *  @param message the message associated with the error that occurred
 *  @param isCache whether or not this fetch was from cache
 */
- (void)CMQAPIClientDidFailSwitchWithMessage:(NSString *)message isCache:(BOOL)isCache;


#pragma mark - POST

/**
 *  called when an object was successfully posted to parse
 *
 *  @param object the object that was posted
 */
- (void)CMQAPIClientDidPostObjectSuccessfully:(PFObject *)object;

/**
 *  called when an object failed to post to parse
 *
 *  @param object  the object that failed to post
 *  @param message the message associated with the error that occurred
 */
- (void)CMQAPIClientDidFailToPostObject:(PFObject *)object withMessage:(NSString *)message;

@end
