//
//  CMQNotificationHandler.m
//  Communique
//
//  Created by Chris Hetem on 11/4/14.
//  Copyright (c) 2014 WaveRider, Inc. All rights reserved.
//

#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v) ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)

@implementation CMQNotificationHandler

+ (CMQNotificationHandler *)sharedHandler{
    static CMQNotificationHandler *_sharedHandler = nil;
    static dispatch_once_t _onceToken;
    
    dispatch_once(&_onceToken, ^{
        _sharedHandler = [[CMQNotificationHandler alloc]init];
    });
    
    return _sharedHandler;
}

- (void)requestPushPermissions{
    DLogOrange(@"requesting push permissions");
    //push notifications
    UIApplication *application = [UIApplication sharedApplication];
    
    if(SYSTEM_VERSION_LESS_THAN( @"10.0" )){
        UIUserNotificationType userNotificationTypes = (UIUserNotificationTypeAlert | UIUserNotificationTypeBadge | UIUserNotificationTypeSound);
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:userNotificationTypes categories:nil];
        
        [application registerUserNotificationSettings:settings];
        [application registerForRemoteNotifications];
    }else{
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        center.delegate = (CMQAppDelegate *)[[UIApplication sharedApplication] delegate];
        [center requestAuthorizationWithOptions:(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge) completionHandler:^(BOOL granted, NSError * _Nullable error){
             if(!error){
                 [application registerForRemoteNotifications];
                 DLogGreen( @"Push registration SUCCESS" );
             }else{
                 DLogRed( @"Push registration FAILED" );
                 DLogRed( @"ERROR: %@ - %@", error.localizedFailureReason, error.localizedDescription );
                 DLogRed( @"SUGGESTIONS: %@ - %@", error.localizedRecoveryOptions, error.localizedRecoverySuggestion );
             }  
         }];  
    }
}

- (void)saveDeviceToken:(NSData *)deviceToken{
    DLogOrange(@"did register, saving device token");
    //Store the deviceToken in the current installation to be used when subscribing to push notifications
    PFInstallation *currentInstallation = [PFInstallation currentInstallation];
    [currentInstallation setDeviceTokenFromData:deviceToken];
    [currentInstallation saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error){
        if(succeeded)DLogGreen(@"saved deviceToken to installation");
        if(error)[[CMQAPIClient sharedClient] handleParseError:error];
    }];
}


-(void)unsubscribeFromParsePush{
    PFInstallation *installation = [PFInstallation currentInstallation];
    NSArray *channels = installation.channels;
    if(channels && [channels count] > 0) {
        [PFPush unsubscribeFromChannelInBackground:[channels objectAtIndex:0] block:^(BOOL succeeded, NSError *error) {
            if(succeeded){
                DLogGreen(@"successfully unsubscribed from channel %@", [channels objectAtIndex:0]);
            }else{
                DLogRed(@"error unsubscribing %@", error.localizedDescription);
                if(error)[[CMQAPIClient sharedClient] handleParseError:error];
            }
        }];
    }
}

@end
