//
//  CMQNotificationHandler.h
//  Communique
//
//  Created by Chris Hetem on 11/4/14.
//  Copyright (c) 2014 WaveRider, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CMQNotificationHandler : NSObject

+ (CMQNotificationHandler *)sharedHandler;

/**
 *  requests the user for system-wide push notifications
 */
- (void)requestPushPermissions;

/**
 *  saves the current device's device token to parse
 *
 *  @param deviceToken the device token of this device
 */
- (void)saveDeviceToken:(NSData *)deviceToken;

/**
 *  unsubscribes a user from their current parse push channel
 */
- (void)unsubscribeFromParsePush;
@end
