//
//  CMQHTMLParser.m
//  Communique
//
//  Created by Chris Hetem on 9/30/14.
//  Copyright (c) 2014 WaveRider, Inc. All rights reserved.
//

#import "CMQHTMLParser.h"
#import "TFHpple.h"

@implementation CMQHTMLParser 

//Init
+ (CMQHTMLParser *)sharedParser{
    static CMQHTMLParser *_sharedParser = nil;
    static dispatch_once_t _onceToken;
    
    dispatch_once(&_onceToken, ^{
        _sharedParser= [[CMQHTMLParser alloc]init];
    });
    
    return _sharedParser;
}

//load images
- (void)parseURL:(NSURL *)url{

    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:urlRequest
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error){
                                                    if (error){
                                                        dispatch_async(dispatch_get_main_queue(), ^{
                                                            [self.delegate CMQHTMLParserFailedToFindData:error.localizedDescription];
                                                        });
                                                    }else{
                                                        dispatch_async(dispatch_get_main_queue(), ^{
                                                            [self processData:data];
                                                        });
                                                    }
                                                }];
    [dataTask resume];
}

-(void) processData:(NSData *)data{
    //create our html parser
    TFHpple *parser = [TFHpple hppleWithHTMLData:data];
    
    //query for the web page's images
    NSString *imgQuery = @"//img";
    NSArray *imageNodes = [parser searchWithXPathQuery:imgQuery];
    
    NSMutableArray *images = [[NSMutableArray alloc]initWithCapacity:0];
    for(TFHppleElement *element in imageNodes){
        @try {
            NSString *imageURLString = [element.attributes objectForKey:@"src"];  //the url associated with the image
            
            if(![imageURLString hasPrefix:@"/"] &&   //only take certain file types and not relative image urls
               ([imageURLString hasSuffix:@".jpg"] || [imageURLString hasSuffix:@".jpeg"] ||
                [imageURLString hasSuffix:@".png"] || [imageURLString hasSuffix:@".gif"])){
                   NSURL *imageURL = [NSURL URLWithString:imageURLString];
                   [images addObject:imageURL];    //add the image urls to our mutable array
               }
            
        }
        @catch (NSException *exception) {
            DLogRed(@"error getting image url. exception: %@", exception.description);
        }
        
    }
    
    //query for the webpage title
    NSString *titleQuery = @"//title";
    NSArray *titleNodes = [parser searchWithXPathQuery:titleQuery];
    NSDictionary *dictionary;
    @try {
        TFHppleElement *element = [titleNodes objectAtIndex:0];
        NSString *title = [element text];
        dictionary = @{kHTMLParserTitleKey        : title,
                       kHTMLParserImageURLArray   : images};
    }
    @catch (NSException *exception) {
        DLogRed(@"error retrieving title: %@", exception.description);
    }
    
    
    //notify delegate of our found image URLs
    if([self.delegate respondsToSelector:@selector(CMQHTMLParserDidFindData:)]){
        [self.delegate CMQHTMLParserDidFindData:dictionary];
    }
}

@end
