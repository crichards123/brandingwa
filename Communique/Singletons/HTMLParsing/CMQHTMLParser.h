//
//  CMQHTMLParser.h
//  Communique
//
//  Created by Chris Hetem on 9/30/14.
//  Copyright (c) 2014 WaveRider, Inc. All rights reserved
//

//@import Foundation;
#import <Foundation/Foundation.h>

@protocol CMQHTMLParserDelegate;

@interface CMQHTMLParser : NSObject <NSURLConnectionDataDelegate>

@property (weak)id<CMQHTMLParserDelegate> delegate;

+ (CMQHTMLParser *)sharedParser;

/**
 *  scrapes the given URL for the "title" tag and all "img" tags
 *
 *  @param URL the url to scrape
 */
- (void)parseURL:(NSURL *)URL;

@end

@protocol CMQHTMLParserDelegate <NSObject>

@required

/**
 *  called when the CMQHTMLParser successfully fetches data from a URL
 *
 *  @param urlData the url data returned from web scraping
 */
-(void)CMQHTMLParserDidFindData:(NSDictionary *)urlData;

/**
 *  called whent he CMQHTMLParser fails to fetch data from a URL
 *
 *  @param error the error associated with the failure to fetch
 */
-(void)CMQHTMLParserFailedToFindData:(NSString *)error;

@end
