//
//  CMQAppConfig.m
//  Communique
//
//  Created by Chris Hetem on 9/17/14.
//  Copyright (c) 2014 WaveRider, Inc. All rights reserved.
//
#import <asl.h>
#ifdef DEBUGo
    #define COMQ_PARSE_APP_ID                       @"h6c4JL70FLcvOVV2qNo6M8iFKap8yrKJRZWpUXR0"
    #define COMQ_PARSE_CLIENT_KEY                   @"qWkLBv5Wmgwz5zT9YkihgKTspRQiv4Vy7bOeH3VW"
    #define COMQ_PARSE_SERVER_URL                   @"https://dev-admin.communique.us/parse"

    #ifdef EPPROACHCOMQ
        #define COMQ_ACCEPTED_MANAGEMENTS            @"CFHeQbAL8V"
        #define COMQ_APP_ID                          @"1008786215"
    #elif PREISSCOMQ
        #define COMQ_ACCEPTED_MANAGEMENTS            @"EpTPNhV3PS,2ZHPkjQh8A,auFdt5Ny9O"
        #define COMQ_APP_ID                          @"1013896498"
    #elif BURKELYCOMQ
        #define COMQ_ACCEPTED_MANAGEMENTS            @"7VNaGC14Hr"
        #define COMQ_APP_ID                          @"1026357753"
    #elif LANDMARKCOMQ
        #define COMQ_ACCEPTED_MANAGEMENTS            @"7VNaGC14Hr"
        #define COMQ_APP_ID                          @"1030756918"
    #elif EDRCOMQ
        #define COMQ_ACCEPTED_MANAGEMENTS            @"IhVlfmN5qJ"
        #define COMQ_APP_ID                          @"1069971144"
    #elif SUMMITCOMQ
        #define COMQ_ACCEPTED_MANAGEMENTS            @"QBrzSCjEmo"
        #define COMQ_APP_ID                          @"1093951785"
    #elif CARLISLECOMQ
        #define COMQ_ACCEPTED_MANAGEMENTS            @"emCOcmXWrG"
        #define COMQ_APP_ID                          @"1125006150"
    #elif ACHCOMQ
        #define COMQ_ACCEPTED_MANAGEMENTS            @"369wpQ7lvi"
        #define COMQ_APP_ID                          @"1125006764"
    #elif HOMETEAMCOMQ
        #define COMQ_ACCEPTED_MANAGEMENTS            @"6g3OBnt9Ll"
        #define COMQ_APP_ID                          @"1125006782"
    #elif ASPENCOMQ
        #define COMQ_ACCEPTED_MANAGEMENTS            @"hyboGLjHZq"
        #define COMQ_APP_ID                          @"1141865475"
    #elif CAPSTONECOMQ
        #define COMQ_ACCEPTED_MANAGEMENTS            @"d4DYEHG0Uz"
        #define COMQ_APP_ID                          @"1141865483"
    #elif EPOCHCOMQ
        #define COMQ_ACCEPTED_MANAGEMENTS            @"pGrGepgCZD"
        #define COMQ_APP_ID                          @"1161752918"
    #elif STATION54COMQ
        #define COMQ_ACCEPTED_MANAGEMENTS            @"seRWU6ii72"
        #define COMQ_APP_ID                          @"1165409634"
    #elif WALDENSTATIONCOMQ
        #define COMQ_ACCEPTED_MANAGEMENTS            @"0eqBy8x3p3"
        #define COMQ_APP_ID                          @"1165409648"
    #elif STRATFORDHILLSCOMQ
        #define COMQ_ACCEPTED_MANAGEMENTS            @"15MJqsPzI8"
        #define COMQ_APP_ID                          @"1165409639"
    #elif CAROLINAMEADOWSCOMQ
        #define COMQ_ACCEPTED_MANAGEMENTS            @"HHWtVkDLeB"
        #define COMQ_APP_ID                          @"1201309573"
    #elif BLUERIDGECOMQ
        #define COMQ_ACCEPTED_MANAGEMENTS            @"dB2UBk9bW3"
        #define COMQ_APP_ID                          @"1201320301"
    #elif SOUNDVIEWCOMQ
        #define COMQ_ACCEPTED_MANAGEMENTS            @"32J1NZkywJ"
        #define COMQ_APP_ID                          @"1201321041"
    #elif BURNSMANAGEMENTCOMQ
        #define COMQ_ACCEPTED_MANAGEMENTS            @"p8Va04BdtE"
        #define COMQ_APP_ID                          @"1212510278"
    #elif TAFTCOMQ
        #define COMQ_ACCEPTED_MANAGEMENTS            @"xIjiybtQol"
        #define COMQ_APP_ID                          @"1229148652"
    #elif EDWARDSCOMQ
        #define COMQ_ACCEPTED_MANAGEMENTS            @"RLGd0KQKlq"
        #define COMQ_APP_ID                          @"1229149689"
    #elif LOMAXCOMQ
        #define COMQ_ACCEPTED_MANAGEMENTS            @"maMlb47pEo"
        #define COMQ_APP_ID                          @"1229156057"
    #elif COLLIERCOMQ
        #define COMQ_ACCEPTED_MANAGEMENTS            @"Qh8ZGDiBjZ"
        #define COMQ_APP_ID                          @"1229169413"
    #elif PICKERINGCOMQ
        #define COMQ_ACCEPTED_MANAGEMENTS            @"I5SaUtYzrJ"
        #define COMQ_APP_ID                          @"1229171248"
    #elif DEMOCOMQ
        #define COMQ_ACCEPTED_MANAGEMENTS            @"all"
        #define COMQ_APP_ID                          @"947943911"
    #elif THEVIEWCOMQ
        #define COMQ_ACCEPTED_MANAGEMENTS            @"fErFi9IUyG"
        #define COMQ_APP_ID                          @"947943911"
    #else
        #define COMQ_ACCEPTED_MANAGEMENTS            @"all"
        #define COMQ_APP_ID                          @"947943911"
    #endif
#else
    #define COMQ_PARSE_APP_ID                       @"QJBbQpHntTGq6caLGvrQSGq7bt2wYUYogdv3hNSw"
    #define COMQ_PARSE_CLIENT_KEY                   @"c9JY6vNdpteWokK79xtpT9QsVd61sVPF56nUex8D"
    #define COMQ_PARSE_SERVER_URL                   @"https://admin.communique.us/parse"

    #ifdef EPPROACHCOMQ
        #define COMQ_ACCEPTED_MANAGEMENTS            @"FhRzdPe1lh,mVb8mRiFr6,c3VLmKtgFF"
        #define COMQ_APP_ID                          @"1008786215"
    #elif PREISSCOMQ
        #define COMQ_ACCEPTED_MANAGEMENTS            @"iL6Ebw1tiq,mrotAMoiac,Qi9svT3VcE"
        #define COMQ_APP_ID                          @"1013896498"
    #elif BURKELYCOMQ
        #define COMQ_ACCEPTED_MANAGEMENTS            @"DmBG4m65Fn"
        #define COMQ_APP_ID                          @"1026357753"
    #elif LANDMARKCOMQ
        #define COMQ_ACCEPTED_MANAGEMENTS            @"Em7s4GItOS"
        #define COMQ_APP_ID                          @"1030756918"
    #elif EDRCOMQ
        #define COMQ_ACCEPTED_MANAGEMENTS            @"mZFjKWshDy"
        #define COMQ_APP_ID                          @"1069971144"
    #elif SUMMITCOMQ
        #define COMQ_ACCEPTED_MANAGEMENTS            @"SLyAnHfP04"
        #define COMQ_APP_ID                          @"1093951785"
    #elif CARLISLECOMQ
        #define COMQ_ACCEPTED_MANAGEMENTS            @"MAoqE3FQdx"
        #define COMQ_APP_ID                          @"1125006150"
    #elif ACHCOMQ
        #define COMQ_ACCEPTED_MANAGEMENTS            @"IHxORhRqjl"
        #define COMQ_APP_ID                          @"1125006764"
    #elif HOMETEAMCOMQ
        #define COMQ_ACCEPTED_MANAGEMENTS            @"USWOcU3bEF"
        #define COMQ_APP_ID                          @"1125006782"
    #elif ASPENCOMQ
        #define COMQ_ACCEPTED_MANAGEMENTS            @"VQc62xDUKs"
        #define COMQ_APP_ID                          @"1141865475"
    #elif CAPSTONECOMQ
        #define COMQ_ACCEPTED_MANAGEMENTS            @"LaCqO1NfTS"
        #define COMQ_APP_ID                          @"1141865483"
    #elif EPOCHCOMQ
        #define COMQ_ACCEPTED_MANAGEMENTS            @"wq4O7rDKOk"
        #define COMQ_APP_ID                          @"1161752918"
    #elif STATION54COMQ
        #define COMQ_ACCEPTED_MANAGEMENTS            @"IBWhBFtNce"
        #define COMQ_APP_ID                          @"1165409634"
    #elif WALDENSTATIONCOMQ
        #define COMQ_ACCEPTED_MANAGEMENTS            @"E8fusfFG2W"
        #define COMQ_APP_ID                          @"1165409648"
    #elif STRATFORDHILLSCOMQ
        #define COMQ_ACCEPTED_MANAGEMENTS            @"YDC04FA6pS"
        #define COMQ_APP_ID                          @"1165409639"
    #elif CAROLINAMEADOWSCOMQ
        #define COMQ_ACCEPTED_MANAGEMENTS            @"LYeAoDJBiY"
        #define COMQ_APP_ID                          @"1201309573"
    #elif BLUERIDGECOMQ
        #define COMQ_ACCEPTED_MANAGEMENTS            @"lFCMlkGYzc"
        #define COMQ_APP_ID                          @"1201320301"
    #elif SOUNDVIEWCOMQ
        #define COMQ_ACCEPTED_MANAGEMENTS            @"Fm8KqCoj3y"
        #define COMQ_APP_ID                          @"1201321041"
    #elif BURNSMANAGEMENTCOMQ
        #define COMQ_ACCEPTED_MANAGEMENTS            @"hnO8Etadxc"
        #define COMQ_APP_ID                          @"1212510278"
    #elif TAFTCOMQ
        #define COMQ_ACCEPTED_MANAGEMENTS            @"1l4xEodiUN"
        #define COMQ_APP_ID                          @"1229148652"
    #elif EDWARDSCOMQ
        #define COMQ_ACCEPTED_MANAGEMENTS            @"Jq7JbLAIiG"
        #define COMQ_APP_ID                          @"1229149689"
    #elif LOMAXCOMQ
        #define COMQ_ACCEPTED_MANAGEMENTS            @"vsPgUHb3XS"
        #define COMQ_APP_ID                          @"1229156057"
    #elif COLLIERCOMQ
        #define COMQ_ACCEPTED_MANAGEMENTS            @"QVKrtr1Ons"
        #define COMQ_APP_ID                          @"1229169413"
    #elif PICKERINGCOMQ
        #define COMQ_ACCEPTED_MANAGEMENTS            @"NLyucdkYJH"
        #define COMQ_APP_ID                          @"1229171248"
    #elif DEMOCOMQ
        #define COMQ_ACCEPTED_MANAGEMENTS            @"all"
        #define COMQ_APP_ID                          @"947943911"
    #elif THEVIEWCOMQ
        #define COMQ_ACCEPTED_MANAGEMENTS            @"tCxOvmJwih"
        #define COMQ_APP_ID                          @"947943911"
    #else
#define COMQ_ACCEPTED_MANAGEMENTS            @"all"
#define COMQ_APP_ID                          @"947943911"
    #endif
#endif

//Font
NSString *const kCMQFontGeometriaLight              = @"Geometria-Light";
NSString *const kCMQFontGeometriaBold               = @"Geometria-Bold";
//NSString *const kCMQFontLMMLBold                    = @"lmmonolt10-bold";
//NSString *const kCMQFontLMMLBoldOblique             = @"lmmonolt10-boldoblique";

//Parse
NSString *const kParseAppId                         = COMQ_PARSE_APP_ID;
NSString *const kParseClientKey                     = COMQ_PARSE_CLIENT_KEY;
NSString *const kParseServerURL                     = COMQ_PARSE_SERVER_URL;
NSString *const kParseAptComplexIDKey               = @"aptComplexID";
NSString *const kParseEventAttendeesKey             = @"eventAttendees";
NSString *const kParseMessageRecipientsKey          = @"recipients";
NSString *const kParseEventDateKey                  = @"eventDate";
NSString *const kParseObjectCreateDateKey           = @"createdAt";

//Push Notifications
NSString *const kParsePushChannelAllKey             = @"All";
NSString *const kParsePushChannelUrgentKey          = @"Urgent";

//HTML Parsing
NSString *const kHTMLParserTitleKey                 = @"kHTMLParserTitleKey";
NSString *const kHTMLParserImageURLArray            = @"kHTMLParserImageURLArray";

//Crashlytics
NSString *const kCrashlyticsAppKey                  = @"6410761f7ac376e3c2ee5b0b06696812ff1c8059";

//Management
NSString *const kAcceptedManagements                = COMQ_ACCEPTED_MANAGEMENTS;

//CQ
NSString *const kApplicationId                      = COMQ_APP_ID;
NSString *const kiTunesURL                          = @"itms-apps://itunes.apple.com/app/id";
NSString *const kLoginSupportEmail                  = @"loginsupport@communique.us";
NSString *const kGeneralSupportEmail                = @"support@communique.us";
NSString *const kTermsOfServiceURL                  = @"http://www.communique.us/terms.html";
NSString *const kPrivacyPolicyURL                   = @"http://communique.us/privacypolicy.html";
NSString *const kActionSheetHelp                    = @"help";
NSString *const kActionSheetUber                    = @"uber";
NSString *const kActionSheetContact                 = @"contact";
NSString *const kContactModeEmail                   = @"email";
NSString *const kContactModePhone                   = @"phone";
NSString *const kContactModeWiFi                    = @"wifi";

//Permissions
NSString *const kPermissionResident                 = @"Resident";
NSString *const kPermissionInterestedParty          = @"InterestedParty";
NSString *const kPermissionStaff                    = @"Staff";
NSString *const kPermissionAdmin                    = @"Admin";
NSString *const kPermissionRegionalAdmin            = @"RegionalAdmin";
NSString *const kPermissionCompanyAdmin             = @"CompanyAdmin";
NSString *const kPermissionSuperadmin               = @"Superadmin";

//Epproach
NSString *const kEpproachSupportPhoneNumber         = @"tel:877-364-5907";
NSString *const kEpproachSupportEmail               = @"support@epproach.net";

//PayLease
NSString *const kPayLeaseRegistrationURL            = @"https://www.paylease.com/registration/renter";

//Packages
NSString *const kPackageCarrierUSPS                 = @"USPS";
NSString *const kPackageCarrierUPS                  = @"UPS";
NSString *const kPackageCarrierFedEx                = @"FedEx";
NSString *const kPackageCarrierOther                = @"Other";
NSString *const kPackageTypeSmallBox                = @"Small Box";
NSString *const kPackageTypeLargeBox                = @"Large Box";
NSString *const kPackageTypeEnvelope                = @"Envelope";
NSString *const kPackageTypeOther                   = @"Other";
NSString *const kPackageTagPerishable               = @"Perishable";
NSString *const kPackageTagUrgent                   = @"Urgent";
NSString *const kPackageTagOther                    = @"Other";
NSString *const kPackageNoneSelected                = @"None Selected";

//Uber
NSString *const kUberAccessTokenString              = @"uber_access_token_string";
NSString *const kUberConnectionErrorString          = @"We're having trouble connecting to Uber, please try again later";
NSString *const kUberModeHome                       = @"home";
NSString *const kUberModePickup                     = @"pickup";

//Dormakaba
NSString* const kDormaKabaServerUrl                 = @"https://api.legicconnect.com/connect";
NSString* const kDormaKabaUserName                  = @"WalletEpproachTechUser";
NSString* const kDormaKabaPassword                  = @"Tt474yL/lhq9rkmLsAkPg7I89VGs4P8QzYNGenYM1/k=";

