//
//  CMQPackageFlag.m
//  Communique
//
//  Created by Andre White on 1/10/18.
//  Copyright © 2018 Communique, LLC. All rights reserved.
//

#import "CMQPackageFlag.h"

@implementation CMQPackageFlag
@dynamic recipient;
@dynamic recipientString;
@dynamic hasPackageCheckedIn;
@dynamic hasPackageCheckedOut;
@dynamic aptComplexID;

+ (void)load{
    [self registerSubclass];
}

+ (NSString *)parseClassName{
    return @"PackageFlag";
}
@end
