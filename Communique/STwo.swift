//
//  STwo.swift
//  Communique
//
//  Created by Andre White on 9/22/18.
//  Copyright © 2018 Communique, LLC. All rights reserved.
//

import Foundation

public class StwoLock:PFObject{
    @NSManaged var unit:String
    @NSManaged var aptComplexID:String
    @NSManaged var doorID:String
    @NSManaged var name:String
    @NSManaged var building:String
    @NSManaged var buildingEntry:Bool
}
extension StwoLock:PFSubclassing{
    public static func parseClassName() -> String {
        return "sTwoLocks"
    }
}
extension PFQuery where PFGenericObject == PFObject{
    static var BuildingKeyQuery:PFQuery<PFObject>{
        let buildingKeys = StwoLock.query()!
        buildingKeys.whereKey("aptComplexID", equalTo: CMQUser.current().complexID)
        buildingKeys.whereKey("buildingEntry", equalTo: true)
        if !CMQUser.current().isAdmin{
            buildingKeys.whereKey("building", equalTo: CMQUser.current().building)
        }
        return buildingKeys
    }
    static var ComplexKeyQuery:PFQuery<PFObject>{
        let complexKeys = StwoLock.query()!
        complexKeys.whereKey("aptComplexID", equalTo: CMQUser.current().complexID)
        complexKeys.whereKey("buildingEntry", equalTo: false)
        complexKeys.whereKey("commonArea", equalTo: true)
        return complexKeys
    }
    static var UnitKeyQuery:PFQuery<PFObject>{
        let unitKeys = StwoLock.query()!
        unitKeys.whereKey("aptComplexID", equalTo: CMQUser.current().complexID)
        unitKeys.whereKey("buildingEntry", equalTo: false)
        unitKeys.whereKey("commonArea", equalTo: false)
        unitKeys.whereKey("buildingUnit", equalTo: CMQUser.current().buildingUnit)
        return unitKeys
    }
}

extension StwoLock{
    static func getBuildingKeys(completion:@escaping CMQParseReturnCompletion){
        PFQuery.BuildingKeyQuery.findObjectsInBackground(block: completion)
    }
    static func getComplexKeys(completion:@escaping CMQParseReturnCompletion){
        PFQuery.ComplexKeyQuery.findObjectsInBackground(block: completion)
    }
    static func getUnitKeys(completion:@escaping CMQParseReturnCompletion){
        PFQuery.UnitKeyQuery.findObjectsInBackground(block: completion)
    }
    
    static func getKeys(completion:@escaping CMQParseReturnCompletion){
        let queries = CMQUser.current().isAdmin ? [PFQuery.ComplexKeyQuery, PFQuery.BuildingKeyQuery] : [PFQuery.ComplexKeyQuery, PFQuery.BuildingKeyQuery, PFQuery.UnitKeyQuery]
       
        CMQAPIManager.runConcurrentQueries(queries: queries, completion: completion)
    }
    static func unlockDoor(doorID:String, completion:@escaping ()->Void){
        PFCloud.callFunction(inBackground: "sTwoUnlock", withParameters: ["aptComplexID":CMQUser.current().complexID, "doorID":doorID]) { (some, error) in
            if let error = error{
                handle(error: error, message: "Unable to Unlock Door", shouldDisplay: true)
                completion()
            }else if let _ = some{
                completion()
            }
        }
    }
}

public class STwoCollectionViewController:UICollectionViewController{
    var updatingView:UpdatingView!
    fileprivate let reuseID = "STwo"
    fileprivate var keys:[StwoLock]! = [StwoLock](){
        didSet{
            self.collectionView?.reloadData()
        }
    }
    private var keysCompletion:CMQParseReturnCompletion{
        return { (nKeys, error) in
            
            if let error = error{
                handle(error: error, message: "Unable to retrieve keys", shouldDisplay: true)
            }else if let some = nKeys{
                self.keys = some as? [StwoLock]
            }
        }
    }
    override public func viewDidLoad() {
        StwoLock.getKeys(completion: keysCompletion)
    }
    override public func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    override public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return keys.count
    }
    override public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseID, for: indexPath) as! STwoCell
        cell.nameLabel.text = keys[indexPath.row].name
        return cell
    }
    public override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //FeedbackManager.shared.show()
        self.startUpdating()
        StwoLock.unlockDoor(doorID: keys[indexPath.row].doorID) {
            DispatchQueue.main.asyncAfter(deadline: .now()+1.5, execute: {
                self.stopUpdating()
            })
        }
    }
    
}
extension STwoCollectionViewController:UICollectionViewDelegateFlowLayout{
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (collectionView.frame.width / 2) - 20
        let height:CGFloat = 185.0
        return CGSize(width: width, height: height)
    }
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.init(top: 10, left: 10, bottom: 5, right: 10)
    }
}
public class STwoCell:UICollectionViewCell{
    @IBOutlet var nameLabel:UILabel!
}
public class STwoButtonItem:UIBarButtonItem{
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        let button = UIButton.init(type: .custom)
        button.frame = CGRect.init(x: 0, y: 0, width: 44, height: 44)
        button.imageView?.contentMode = .scaleToFill
        button.setImage(UIImage.init(named: CMQKabaUnlockButtonImageName, in: CMQBundle, compatibleWith: nil), for: .normal)
        button.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
        self.customView = button
    }
    @objc public func buttonPressed(){
        UIApplication.shared.sendAction(self.action!, to: self.target!, from: nil, for: nil)
    }
}

extension STwoCollectionViewController:Updating{
    var bottomView:BottomView{
        let uiView = UIView.init(frame: CGRect.init(origin: .zero, size: CGSize.init(width: self.view.frame.width, height: 30)))
        let label = UILabel.init(frame: uiView.frame)
        label.textColor = .white
        label.textAlignment = .center
        label.text = "Unlocked!"
        uiView.addSubview(label)
        uiView.addShadow()
        uiView.layer.shadowOffset = CGSize.init(width: 0, height: -2)
        uiView.backgroundColor = UIColor.init(red: 52/255.0, green: 89/255.0, blue: 124/255.0, alpha: 1.0)
        let bottom = BottomView.init(view: uiView, parentView: self.view, updatingLabel: label)
        
        return bottom
    }
    public func startUpdating() {
        self.updatingView = UpdatingView.init(bottomView: self.bottomView)
        self.updatingView.startUpdatingWithOutLabelAnimation()
    }
    public func stopUpdating() {
        self.updatingView?.stopUpdatingWithOutLabelAnimation()
    }
}
