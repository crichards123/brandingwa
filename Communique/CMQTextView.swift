//
//  CMQTextView.swift
//  Communique
//
//  Created by Andre White on 3/21/18.
//  Copyright © 2018 Communique, LLC. All rights reserved.
//

import UIKit

public class CMQTextView: UITextView {
    @IBOutlet weak public var placeHolder:UILabel!{
        didSet{
            placeHolder.font = self.font
        }
    }
    public var editingPropertyType:PropertyType?
    public override func awakeFromNib() {
        super.awakeFromNib()
        self.returnKeyType = .done
        self.delegate = self
        let toolBar = UIToolbar.init(frame: CGRect.init(origin: CGPoint.init(x: 0, y: 0), size: CGSize.init(width: self.frame.width, height: 50)))
        let doneButton = UIBarButtonItem.init(barButtonSystemItem: .done, target: self, action: #selector(donePressed(sender:)))
        let prevButton = UIBarButtonItem.init(title: "Prev", style: .plain, target: self, action: #selector(prevPressed(sender:)))
        let nextButton = UIBarButtonItem.init(title: "Next", style: .plain, target: self, action: #selector(nextPressed(sender:)))
        let space = UIBarButtonItem.init(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        toolBar.items = [prevButton, nextButton, space, doneButton]
        self.inputAccessoryView = toolBar
    }
    @objc public func donePressed(sender:UIBarButtonItem){
        self.resignFirstResponder()
    }
    @objc public func nextPressed(sender:UIBarButtonItem){
        NotificationCenter.default.post(name: .nextPressed, object: nil)
    }
    @objc public func prevPressed(sender:UIBarButtonItem){
        NotificationCenter.default.post(name: .prevPressed, object: nil)
    }
}
extension CMQTextView:UITextViewDelegate{
    public func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n"{
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    public func textViewDidChange(_ textView: UITextView) {
        guard let type = editingPropertyType, let curText = textView.text else {return}
        var object = [String:Any]()
        object["property"] = type
        object["content"] = curText
        NotificationCenter.default.post(name: .updateComposable, object: object)
    }
    public func textViewDidBeginEditing(_ textView: UITextView) {
        if !placeHolder.isHidden{
            placeHolder.isHidden = true
        }
    }
    public func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty{
            placeHolder.isHidden = false
        }
    }
}
