//
//  CMQPackageCollectionViewCell.h
//  Communique
//
//  Created by Andre White on 9/8/17.
//  Copyright © 2017 Communique, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ParseUI.h"

@interface CMQPackageCollectionViewCell : PFCollectionViewCell
@property(weak, nonatomic)IBOutlet UILabel* residentNameLabel;
@property(weak, nonatomic)IBOutlet UILabel* buildingLabel;
@end
