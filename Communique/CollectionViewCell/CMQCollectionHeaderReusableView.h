//
//  CMQCollectionHeaderReusableView.h
//  Communique
//
//  Created by Andre White on 9/8/17.
//  Copyright © 2017 Communique, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CMQCollectionHeaderReusableView : UICollectionReusableView
@property(retain, nonatomic)IBInspectable UIColor* highlightedColor;
@property(retain, nonatomic)IBInspectable UIColor* unhighlightedColor;


@end
