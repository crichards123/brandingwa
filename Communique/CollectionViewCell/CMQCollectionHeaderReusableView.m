//
//  CMQCollectionHeaderReusableView.m
//  Communique
//
//  Created by Andre White on 9/8/17.
//  Copyright © 2017 Communique, LLC. All rights reserved.
//

#import "CMQCollectionHeaderReusableView.h"
@interface CMQCollectionHeaderReusableView()
@property (weak, nonatomic) IBOutlet UIView* view;
@property(strong, nonatomic)IBOutletCollection(UILabel)NSArray* labels;
@end
@implementation CMQCollectionHeaderReusableView

- (IBAction)buttonPressed:(UIButton*)sender {
    for (UILabel* label in _labels) {
        if (sender.tag==label.tag) {
            label.textColor=_highlightedColor;
            //label.font=[UIFont systemFontOfSize:17 weight:UIFontWeightBold];
        }
        else{
            label.textColor=_unhighlightedColor;
           // label.font=[UIFont systemFontOfSize:17 weight:UIFontWeightThin];
        }
    }
    [UIView animateWithDuration:.5 animations:^{
        _view.frame=CGRectMake(sender.frame.origin.x, _view.frame.origin.y, _view.frame.size.width, _view.frame.size.height);
    }];
    
}

@end
