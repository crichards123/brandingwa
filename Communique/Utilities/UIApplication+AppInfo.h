//
//  UIApplication+AppInfo.h
//  Communique
//
//  Created by Chris Hetem on 10/2/14.
//  Copyright (c) 2014 WaveRider, Inc. All rights reserved.
//

//@import UIKit;
#import <UIKit/UIKit.h>

@interface UIApplication (AppInfo)

/**
 *  returns the app's version number (ex. "1.0")
 *
 *  @return the app's version number
 */
+ (NSString *)appVersion;

/**
 *  returns the app's build number
 *
 *  @return the app's build number
 */
+ (NSString *)buildNumber;

/**
 *  returns the app's version and build number (ex "1.0 (1.0.0)")
 *
 *  @return the app's version and build number
 */
+ (NSString *)versionAndBuild;

@end
