//
//  UIImage+PFFile.m
//  Communique
//
//  Created by Chris Hetem on 11/17/14.
//  Copyright (c) 2014 WaveRider, Inc. All rights reserved.
//

#import "UIImage+PFFile.h"

@implementation UIImage (PFFile)

+ (UIImage *)getImageFromPFFile:(PFFile *)imageFile{
    NSData *imageData = [imageFile getData];
    UIImage *eventImage = [UIImage imageWithData:imageData];
    return eventImage;
}
+(void)getImageDataFromPFFile:(PFFile*)imageFile withCompletion:(void(^)(UIImage* image, NSError* error))completion{
    [imageFile getDataInBackgroundWithBlock:^(NSData * _Nullable data, NSError * _Nullable error) {
        completion([UIImage imageWithData:data],error);
    }];
    
}
+ (CGFloat)getHeightForImageContainerWithWidth:(CGFloat)containerWidth imageWidth:(CGFloat)width imageHeight:(CGFloat)height{
    CGFloat estimatedHeight;
    
    if (width > height && width > containerWidth) //landscape - larger than UIImageView
        estimatedHeight = height * (containerWidth / width);
    else if (width > height) //landscape - smaller than (or equal to) UIImageView
        estimatedHeight = height;
    else if (height > width && width > containerWidth) //portrait - larger than UIImageView
        estimatedHeight = height * (containerWidth / width);
    else if (height > width) //portrait - smaller (or equal to) than UIImageView
        estimatedHeight = height;
    else if (height == width && width > containerWidth) //sqaure - larger than UIImageView
        estimatedHeight = containerWidth;
    else if (height == width) //sqaure - smaller (or equal to) than UIImageView
        estimatedHeight = height;
    else
        estimatedHeight = height;
    
    return estimatedHeight;
}

+ (UIImage *)resizeImage:(UIImage *)image newWidth:(CGFloat)width newHeight:(CGFloat)height{
    CGSize size = CGSizeMake(width, height);
    
    // Resize image
    UIGraphicsBeginImageContext(size);
    [image drawInRect: CGRectMake(0, 0, size.width, size.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}

@end
