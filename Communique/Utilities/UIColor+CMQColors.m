//
//  UIColor+CMQColors.m
//  Communique
//
//  Created by Chris Hetem on 9/15/14.
//  Copyright (c) 2014 WaveRider, Inc. All rights reserved.
//

#import "UIColor+CMQColors.h"
#import "UIColor+StandardRGB.h"

@implementation UIColor (CMQColors)

//These color values come from using the Sip app found in the Mac App Store. Simply mouse over a color and click and it'll generate this code.

+ (UIColor *)CMQTanColor{
    return [UIColor colorWithRed:0.98 green:0.89 blue:0.82 alpha:1];
}

+ (UIColor *)CMQRedColor{
    return [UIColor colorWithRed:0.87 green:0.33 blue:0.33 alpha:1];
}

+ (UIColor *)CMQBlueColor{
    return [UIColor colorWithRed:0.67 green:0.73 blue:0.77 alpha:1];
}

+ (UIColor *)CMQPurpleColor{
    return [UIColor colorWithRed:0.68 green:0.51 blue:0.75 alpha:1];
}

+ (UIColor *)CMQYellowColor{
    return [UIColor colorWithRed:0.98 green:0.81 blue:0.58 alpha:1];
}

+ (UIColor *)CMQGreenColor{
    return [UIColor colorWithRed:0.6 green:0.76 blue:0.68 alpha:1];
}

+ (UIColor *)CMQBrownColor{
    return [UIColor colorWithStandardRed:76 green:69 blue:61 alpha:1.0];
}

+ (UIColor *)CMQMochaColor{
    return [UIColor colorWithStandardRed:119 green:50 blue:30 alpha:1.0];
}

@end
