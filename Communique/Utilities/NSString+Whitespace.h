//
//  NSString+Whitespace.h
//  Communique
//
//  Created by Chris Hetem on 11/17/14.
//  Copyright (c) 2014 WaveRider, Inc. All rights reserved.
//

//@import Foundation;
#import <Foundation/Foundation.h>

@interface NSString (Whitespace)

/**
 *  strips any whitespace from the passed in string
 *
 *  @param string the string to strip whitespace from
 *
 *  @return the string after all whitespace was stripped
 */
+ (NSString *)stringByStrippingWhitespace:(NSString *)string;

@end
