//
//  UIColor+CMQColors.h
//  Communique
//
//  Created by Chris Hetem on 9/15/14.
//  Copyright (c) 2014 WaveRider, Inc. All rights reserved.
//

//@import UIKit;
#import <UIKit/UIKit.h>

@interface UIColor (CMQColors)

+ (UIColor *)CMQTanColor;
+ (UIColor *)CMQRedColor;
+ (UIColor *)CMQBlueColor;
+ (UIColor *)CMQPurpleColor;
+ (UIColor *)CMQYellowColor;
+ (UIColor *)CMQGreenColor;
+ (UIColor *)CMQBrownColor;
+ (UIColor *)CMQMochaColor;

@end
