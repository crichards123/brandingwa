//
//  UIImage+PFFile.h
//  Communique
//
//  Created by Chris Hetem on 11/17/14.
//  Copyright (c) 2014 WaveRider, Inc. All rights reserved.
//

//@import UIKit;
#import <UIKit/UIKit.h>

@interface UIImage (PFFile)

/**
 *  takes a PFFile from parse and turns it into a UIImage
 *
 *  @param imageFile the PFFile to get the image from
 *
 *  @return the image retrieved from the PFFile image data
 */
+ (UIImage *)getImageFromPFFile:(PFFile *)imageFile;
+(void)getImageDataFromPFFile:(PFFile*)imageFile withCompletion:(void(^)(UIImage* image, NSError* error))completion;

/**
 *  takes dimensions of container and UIImage to compute estimated height of container
 *
 *  @param containerWidth the width of a UIImageView
 *  @param width the width of a UIImage
 *  @param height the height of a UIImage
 *
 *  @return CGFloat of computed container height
 */
+ (CGFloat)getHeightForImageContainerWithWidth:(CGFloat)containerWidth imageWidth:(CGFloat)width imageHeight:(CGFloat)height;

/**
 *  takes a UIImage and resizes it to given width/height
 *
 *  @param image the UIImage to resize
 *
 *  @return the resized UIImage
 */
+ (UIImage *)resizeImage:(UIImage *)image newWidth:(CGFloat)width newHeight:(CGFloat)height;

@end
