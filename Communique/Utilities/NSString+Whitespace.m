//
//  NSString+Whitespace.m
//  Communique
//
//  Created by Chris Hetem on 11/17/14.
//  Copyright (c) 2014 WaveRider, Inc. All rights reserved.
//

#import "NSString+Whitespace.h"

@implementation NSString (Whitespace)

+ (NSString *)stringByStrippingWhitespace:(NSString *)string{
    
    NSArray *segments = [string componentsSeparatedByCharactersInSet :[NSCharacterSet whitespaceCharacterSet]];    //used to remove any whitespace
    string = [segments componentsJoinedByString:@""];
    
    return string;
}

@end
