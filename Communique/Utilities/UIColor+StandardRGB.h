//
//  UIColor+StandardRGB.h
//
//  Created by Chris Hetem on 8/14/14.
//  Copyright (c) Chris Hetem. All rights reserved.
//

//@import UIKit;
#import <UIKit/UIKit.h>

@interface UIColor (StandardRGB)

/**
 *  Used for passing in standard rgb values instead of having to divide by 255
 *
 *  @param red   the red rgb value from 0-255
 *  @param green the green rgb value from 0-255
 *  @param blue  the blue rgb value from 0-255
 *  @param alpha the alpha value of the color
 *
 *  @return the color created with the passed in rgb and alpha values
 */
+ (UIColor *)colorWithStandardRed:(CGFloat)red green:(CGFloat)green blue:(CGFloat)blue alpha:(CGFloat)alpha;

@end
