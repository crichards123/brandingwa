//
//  CMQProtocols.swift
//  Communique
//
//  Created by Andre White on 3/22/18.
//  Copyright © 2018 Communique, LLC. All rights reserved.
//

import Foundation
public protocol Composable {
    init()
    var composableType:PFObject.ItemType {get}
    var composableProperties:[ItemProperty]? {get}
    var reqProperties:[ItemProperty]? {get}
    var visibleProperties:[ItemProperty] {get}
    var unSetReqProperties:[ItemProperty.PropertyType]? {get}
    var isComposed:Bool {get}
    func setProperty(propertyType:ItemProperty.PropertyType, propertyContent:Any?)
    func deletePropertyContent(propertyType:ItemProperty.PropertyType)
    func propertyContent(propertyType:ItemProperty.PropertyType)->Any?
    func getProperty(propertyType:ItemProperty.PropertyType)->ItemProperty?
    func post(completion:@escaping PFBooleanResultBlock)
}
public protocol Viewing {
    init()
    var viewableType:PFObject.ItemType {get}
    var viewableProperties:[ItemProperty]? {get}
    var displayedProperties:[ItemProperty] {get}
    func propertyContent(propertyType:ItemProperty.PropertyType)->Any?
    func getViewableProperty(propertyType:ItemProperty.PropertyType)->ItemProperty?
}
public protocol CMQDataSourceDelegate{
    func objectsLoaded(_ error:Error?)
}

protocol LayerApplyable {
    func apply(layer:CALayer)
}
protocol Gradient:LayerApplyable{
    var locations:[NSNumber]? {get}
    var colors:[CGColor] {get}
}
protocol Border:LayerApplyable{
    var color:CGColor {get}
    var width:CGFloat {get}
}
extension Border{
    func apply(layer: CALayer) {
        layer.borderColor = color
        layer.borderWidth = width
    }
}
protocol Shadow:LayerApplyable{
    var color:CGColor {get}
    var offset:CGSize {get}
    var opacity:Float {get}
}
extension Gradient{
    func apply(layer: CALayer) {
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = colors
        gradientLayer.locations = locations
        layer.insertSublayer(gradientLayer, at: 0)
    }
}
extension Shadow{
    func apply(layer: CALayer) {
        layer.shadowColor = color
        layer.shadowOffset = offset
        layer.shadowOpacity = opacity
    }
}
protocol GradientProtocol{
    var layer:CALayer {get}
    var colors:[UIColor] {get}
    func addGradient()
}
protocol ShadowProtocol{
    var layer:CALayer{get}
    func addShadow()
}
extension ShadowProtocol{
    func addShadow(){
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.masksToBounds = false
        self.layer.shadowOffset = CGSize(width: 0, height: 5)
        self.layer.shadowOpacity = 0.5
    }
}
extension GradientProtocol{
    func addGradient(){
        let gradientlayer = self.layer as! CAGradientLayer
        gradientlayer.colors = self.colors.map({$0.cgColor})
    }
}
public indirect enum IDDataSourceState:Equatable {
    case loaded(prev:IDDataSourceState?)
    case loading
    case refreshing
    case paginating
    case filtering(filter:String)
}

protocol IDNewContainerProtcol {
    var state:IDDataSourceState {get}
    mutating func willChangeState(to:IDDataSourceState)
}
public extension IDViewDataSourceProtocol{
    var delegate:IDDataSourceDelegateProtocol?{
        return nil
    }
}
public protocol IDDataSourceDelegateProtocol {
    func didChange(dataSource:IDViewDataSourceProtocol, toState:IDDataSourceState)
}
protocol IDTableViewDataSourceChangeProtocol {
    associatedtype DataSource
    func willChangeSource(to:DataSource)
}
protocol IDParseQueryOrderByProtocol {
    associatedtype Modifier
    var orderModifier:Modifier {get}
}
public protocol IDViewDataSourceProtocol {
    var delegate:IDDataSourceDelegateProtocol? {set get}
    var numOfRows:Int {get}
    var numOfSec:Int {get}
    var canPaginate:Bool {get}
    func object(at:IndexPath)->Any?
    mutating func willChangeState(to:IDDataSourceState)
}
public protocol IDTableDataSourceWithCell: IDViewDataSourceProtocol{
    var cellCreator:IDTableViewCellCreatorProtocol{get}
    
}
protocol IDConnectedDataRetryProtocol {
    var retryAttempts:Int {get set}
    var maxRetryAttempts:Int {get}
    mutating func retry(completion:@escaping (Error?)->Void)
}

protocol IDContainerProtocol {
    associatedtype Query
    associatedtype Item
    var data:[Item] {get set}
    var loadQuery:Query {get set}
    var state: IDDataSourceState {get set}
}
protocol IDConnectedDataSourceProtocol {
    mutating func load(completion:@escaping (Error?)->Void)
    mutating func refresh(completion:@escaping (Error?)->Void)
}
protocol IDPagination {
    
    associatedtype Modifier
    var hasMore:Bool {get}
    var offset:Int {get}
    var paginationModifier:Modifier {get}
    var pageNumber:Int {get}
    func paginate(completion:@escaping(_ error:Error?)->Void)
}

protocol IDSeguePreparationProtocol {
    func prepare(dest:UIViewController, sender:Any?, transitionHandler:UIViewControllerTransitioningDelegate?)
    func prepareTransitionHandler(viewFrame:CGRect)->CMQTransitionDelegate?
}
protocol CMQFeedItemProtocol: IDConnectedDataSourceProtocol{}


/* UITableView Pagination*/
protocol IDQueryProtcol {
    associatedtype Completion //@escaping(_ error:Error?)->Void
    func runQuery(completion:Completion)
}

/* Modifier - Used to modify classes */
protocol IDModify {
    associatedtype Manipulatable
    associatedtype Modifier
    var modifier:Modifier {get}
    func modify(item:Manipulatable)->Manipulatable
    //func modify(item:Manipulatable, completion:(_ returnable:Manipulatable,_ error:Error?)->Void)
}

protocol IDManipulatable {
    associatedtype Query
    func combine(with:[Query])->Query
}
protocol IDSearchable {
    mutating func search(string:String, completion: () -> Void)
}
protocol IDDataSourceStateChange {
    func updateUI(wState:IDDataSourceState)
}
public protocol IDTableViewCellCreatorProtocol{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
}
public protocol IDCollectionViewCellCreatorProtocol{
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
}
public protocol IDCellConfigureProtocol {
    associatedtype ViewType
    associatedtype CellType
    var dataSource:IDViewDataSourceProtocol {get}
    var reuseIdentifier:String {get}
    func cell(forTable:ViewType, atIndex:IndexPath)->CellType
    func configure(cell:CellType, atIndex:IndexPath)
}
public protocol IDCollectionCellConfigureProtocol:IDCellConfigureProtocol where ViewType == UICollectionView{
    
}
extension IDCollectionCellConfigureProtocol where CellType == UICollectionViewCell{
    func cell(forTable: ViewType, atIndex: IndexPath) -> CellType {
        let cell = forTable.dequeueReusableCell(withReuseIdentifier: self.reuseIdentifier, for: atIndex)
        self.configure(cell: cell, atIndex: atIndex)
        return cell
    }
}
public protocol IDTableCellConfigureProtocol:IDCellConfigureProtocol where ViewType == UITableView {
    
}

extension IDTableCellConfigureProtocol where CellType == UITableViewCell {
    func cell(forTable: ViewType, atIndex: IndexPath) -> CellType {
        let cell = forTable.dequeueReusableCell(withIdentifier: self.reuseIdentifier, for: atIndex)
        self.configure(cell: cell, atIndex: atIndex)
        cell.selectionStyle = .none
        return cell
    }
}

protocol IDSearchControllerProtocol:UISearchResultsUpdating {
    var searchController:UISearchController! {get set}
    var resultsController:UITableViewController! {get set}
}

extension IDSearchControllerProtocol{
    public func loadResultsController(){
        self.resultsController = resultsControllerFromStoryBoard()
        self.searchController = UISearchController.init(searchResultsController: self.resultsController)
        self.customize(controller: self.searchController)
    }
    private func resultsControllerFromStoryBoard()->IDTableViewController{
        return UIStoryboard.init(name: "Packages", bundle: CMQBundle).instantiateViewController(withIdentifier: "SearchResults") as! IDTableViewController
    }
    public func customize(controller:UISearchController){
        controller.searchResultsUpdater = self
        controller.dimsBackgroundDuringPresentation = false
        self.customize(searchBar: controller.searchBar)
    }
    
    public func customize(searchBar:UISearchBar){
        searchBar.tintColor = .white
        if let textField = searchBar.value(forKey: "searchField") as? UITextField, let backgroundView = textField.subviews.first{
            backgroundView.backgroundColor = .white
            backgroundView.layer.cornerRadius = 10.0
            backgroundView.clipsToBounds = true
            searchBar.tintColor = .white
            searchBar.sizeToFit()
            searchBar.placeholder = "Resident Name"
        }
    }
    public func search(){
        guard let searchText = searchController.searchBar.text else {return}
        guard let results = self.resultsController as? IDTableViewController else {return}
        results.dataSource.willChangeState(to: .filtering(filter: searchText))
    }
}

extension Notification.Name{
    public static let IDDataSourceStateChanged = Notification.Name("IDDataSourceStateChanged")
}

extension Array:IDSearchable where Element : CMQUser{
     mutating func search(string: String, completion: () -> Void) {
        self = self.filter({ (resident) -> Bool in
            return (resident.fullName.uppercased().range(of: string.uppercased()) != nil) || (resident.buildingUnit.uppercased().range(of: string.uppercased()) != nil)
        })
    }
}


