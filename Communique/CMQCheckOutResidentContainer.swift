//
//  CMQCheckOutResidentContainer.swift
//  Communique
//
//  Created by Andre White on 6/4/18.
//  Copyright © 2018 Communique, LLC. All rights reserved.
//

import UIKit

public class CMQCheckOutResidentContainer: CMQResidentContainer {
    
    public init(){
        super.init(checkOut: true)
    }

}
