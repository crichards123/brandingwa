//
//  CMQTransitionDelegate.swift
//  Communique
//
//  Created by Andre White on 1/19/18.
//  Copyright © 2018 Communique, LLC. All rights reserved.
//

import UIKit

public class CMQTransitionDelegate: NSObject, UIViewControllerTransitioningDelegate {
    private var size:CGSize
    private var origin:CGPoint
    public init(size:CGSize, origin:CGPoint){
        self.size = size
        self.origin = origin
        super.init()
    }
    public func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
        var controller : UIPresentationController?
        switch source {
        case is CMQHomeFeedViewController, is CMQCreatePresetViewController:
            let aController = CMQPresentationController.init(presentedViewController: presented, presenting: presenting)
            aController.size = size
            aController.origin = origin
            aController.specialDimming = presented is CMQMenuViewController
            aController.isDimmingViewTappable = !(presented is CMQLoadingViewController)
            controller = aController
        case is CMQCheckOutSearchTableViewController:
            let aController = CMQPackageCheckOutPinPresentationController.init(presentedViewController: presented, presenting: presenting)
            aController.contentSize = size
            controller = aController
        case is CMQNotifyResidentViewController:
            controller = CMQSuccessPresentationController.init(presentedViewController: presented, presenting: presenting)
        case is CMQComposeViewController:
            let aController = CMQPackageCheckOutPinPresentationController.init(presentedViewController: presented, presenting: presenting)
            if let scheduleController = presented as? CMQScheduledMessageViewController{
                aController.contentSize = scheduleController.preferredSize
            }
            controller = aController
        case is CMQAdditionalInfoViewController:
            let aController = CMQPresentationController.init(presentedViewController: presented, presenting: presenting)
            aController.size = size
            aController.origin = origin
            aController.isDimmingViewTappable = presented is CMQUnlockViewController
            controller = aController
        default:
            let aController = CMQPresentationController.init(presentedViewController: presented, presenting: presenting)
            aController.size = size
            aController.origin = origin
            controller = aController
        }
        return controller
    }
    public func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        if presented is CMQPackageDeliveryConfirmationViewController{
            return nil
        }
        if source is CMQHomeFeedViewController {
            return CMQAnimator(dimiss: false, fromBottom: (presented is CMQComposeMenuViewController)||(presented is CMQUnlockViewController))
        }
        return nil
        
    }
    public func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        if dismissed is CMQMenuViewController || dismissed is CMQComposeMenuViewController{
            return CMQAnimator(dimiss: true, fromBottom: (dismissed is CMQComposeMenuViewController))
        }
        return nil
    }
    

}
