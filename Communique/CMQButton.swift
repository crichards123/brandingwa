//
//  CMQButton.swift
//  Communique
//
//  Created by Andre White on 2/22/18.
//  Copyright © 2018 Communique, LLC. All rights reserved.
//

import UIKit

public class CMQButton: UIButton {
    override public var frame: CGRect{
        didSet{
            //buttonImageView.frame = CGRect.init(x: (frame.size.width/2), y: (frame.size.height/2), width: 40, height: 40)
        }
    }
    public enum CMQButtonType {
        case Message
        case Events
        case News
        case Compose
    }
    public var className = String()
    private var buttonImageView = UIImageView()
    
    init(buttonType:CMQButtonType, origin:CGPoint){

        
        let size = CGSize.init(width: 75, height: 75)
        super.init(frame: CGRect.init(origin: origin, size: size))
        //buttonImageView.frame = CGRect.init(x: 0, y: 0, width: 40, height: 40)
        switch buttonType {
        case .Message:
            tag = 0
            backgroundColor = .MessageColor
            //buttonImageView.image = #imageLiteral(resourceName: "ic_mail_outline")
            setImage(#imageLiteral(resourceName: "ic_mail_outline"), for: .normal)
            className = "Messages"
        case .Events:
            tag = 1
            backgroundColor = .EventColor
            //buttonImageView.image = #imageLiteral(resourceName: "ic_event")
            setImage(#imageLiteral(resourceName: "ic_event"), for: .normal)
            className = "Events"
        case .News:
            tag = 2
            //buttonImageView.image = #imageLiteral(resourceName: "ic_news")
            setImage(#imageLiteral(resourceName: "ic_news"), for: .normal)
            backgroundColor = .NewsColor
            className = "News"
        case .Compose:
            tag = 3
            //buttonImageView.image = #imageLiteral(resourceName: "ic_create")
            setImage(#imageLiteral(resourceName: "ic_create1"), for: .normal)
            backgroundColor = .DarkColor
            className = ""
        }
        layer.cornerRadius = size.height/2
        tintColor = .white
        //addSubview(buttonImageView)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}
