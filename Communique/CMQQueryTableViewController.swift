//
//  CMQQueryTableViewController.swift
//  Communique
//
//  Created by Andre White on 1/11/18.
//  Copyright © 2018 Communique, LLC. All rights reserved.
//

import UIKit
import ParseUI

public class CMQQueryTableViewController: PFQueryTableViewController {
    public var showingScheduled:Bool = false
    public var noContentViewAdded = false
    private var noContentView:CMQNoContentView? {
        guard let view = CMQNoContentView.instanceFromNib() else {return nil}
        view.set(parseClassName: showingScheduled ? "Scheduled Messages":self.parseClassName!)
        return view
    }
    override public func viewDidLoad() {
        super.viewDidLoad()
        self.pullToRefreshEnabled = true
        initView()

    }
    public func setTitle(){
        self.title = self.parseClassName
    }
    public func handleScheduleButton(){
        if self.parseClassName != "Messages" || !CMQUser.current().isAdmin{
            self.navigationItem.rightBarButtonItem = nil
        }
    }
    public func initView(){
        setTitle()
        setNavColor()
        handleScheduleButton()
        
    }
    public func setNavColor(){
        var barColor:UIColor?
        switch self.parseClassName {
        case .some("Messages"):
            barColor = .MessageColor
        case .some("Events"):
            barColor = .EventColor
        case .some("News"):
            barColor = .NewsColor
        default:
            barColor = nil
        }
        if barColor != nil{
            self.navigationController?.navigationBar.barTintColor = barColor
        }
    }
    public func restoreNavColor(){
        self.navigationController?.navigationBar.barTintColor = .DarkColor
    }
    
    
    override public func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == objects?.endIndex{
            loadNextPage()
        }
    }
    override public func tableView(_ tableView: UITableView, cellForNextPageAt indexPath: IndexPath) -> PFTableViewCell? {
        var cell = tableView.dequeueReusableCell(withIdentifier: "Refresh") as? PFTableViewCell
        if cell == nil{
            cell = PFTableViewCell.init(style: .default, reuseIdentifier: "Refresh")//UITableViewCell(style: .default, reuseIdentifier: "Refresh")
            let refresh = UIActivityIndicatorView.init(style: .whiteLarge)
            let centerX = NSLayoutConstraint.init(item: refresh, attribute: .centerX, relatedBy: .equal, toItem: cell?.contentView, attribute: .centerX, multiplier: 1, constant: 0)
            let centerY = NSLayoutConstraint.init(item: refresh, attribute: .centerY, relatedBy: .equal, toItem: cell?.contentView, attribute: .centerY, multiplier: 1, constant: 0)
            cell?.contentView.addSubview(refresh)
            cell?.contentView.addConstraints([centerY, centerX])
        }
        return cell
    }
    public func addNoContentView(){
        if !noContentViewAdded{
            guard let view = noContentView else {return}
            view.frame = self.view.frame
            self.tableView.tableFooterView = UIView()
            self.view.addSubview(view)
            noContentViewAdded = true
            
        }else{
            guard let contentView = self.view.subviews.filter({$0 is CMQNoContentView}).first as? CMQNoContentView else {return}
            contentView.set(parseClassName: showingScheduled ? "Scheduled Messages":self.parseClassName!)
        }
    }
    public func removeNoContentView(){
        guard let view = self.view.subviews.filter({$0 is CMQNoContentView}).first else {return}
        view.removeFromSuperview()
        noContentViewAdded = false
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
