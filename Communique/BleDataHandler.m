//
//  BleDataHandler.m
//  legic-sdk
//
//  Created by admin on 03/10/2014.
//  Copyright (c) 2014 Legic. All rights reserved.
//

#import "BleDataHandler.h"

@implementation BleDataHandler
{
    BOOL accessGranted;
    NSInteger errorCode;
    NSInteger flags;
    BleBatteryStatus batteryStatus;
    NSString* batteryVoltage;
    NSMutableString* messageString;
}

-(id)init
{
    @throw ([NSException exceptionWithName:@"InvalidMethodException" reason:@"This method should not be used - use initWithCode:Length:Data:." userInfo:nil]);
}

-(id)initWithCode:(int)code Message:(NSData*)message;
{
    self = [super init];
    if(self)
    {
        [self parseTlv:message];
    }
    return self;
}

-(NSMutableString*) getMessageString
{
    return messageString;
}

-(BOOL)isAccessGranted
{
    return accessGranted;
}

-(NSInteger)getAccessError
{
    return errorCode;
}

-(NSInteger)getFlags
{
    return flags;
}

-(BleBatteryStatus)getBatteryStatus
{
    return batteryStatus;
}

-(NSString*)getBatteryVoltage
{
    return batteryVoltage;
}

-(void)parseTlv:(NSData*)data
{
    NSInteger offset = 0;
    NSInteger totalLength = [data length];
    uint8_t* array = (uint8_t*)data.bytes;
    messageString = [NSMutableString stringWithString:@""];
    
    do
    {
        [messageString appendFormat:@"%02x ", array[offset]];
        
        switch(array[offset++])
        {
            case 0:
            {
                [messageString appendFormat:@"%02x ", array[offset]];
                NSInteger length = array[offset++];
                accessGranted = (array[offset] == 0);
                errorCode = array[offset];
                [messageString appendFormat:@"%02x", array[offset]];
                flags = array[offset +1];
                [messageString appendFormat:@"%02x\n", array[offset+1]];
                offset += length;
                break;
            }
            case 1:
            {
                [messageString appendFormat:@"%02x ", array[offset]];
                NSInteger length = array[offset++];
                batteryStatus = (array[offset] == 1) ? BleBatteryStatus_OK : BleBatterStatus_Low;
                [messageString appendFormat:@"%02x\n", array[offset]];
                offset += length;
                break;
            }
            case 2:
            {
                [messageString appendFormat:@"%02x ", array[offset]];
                NSInteger length = array[offset++];
                NSInteger voltage = array[offset] << 8;
                [messageString appendFormat:@"%02x", array[offset]];
                voltage += array[offset + 1];
                [messageString appendFormat:@"%02x\n", array[offset + 1]];
                batteryVoltage = [NSString stringWithFormat:@"%ld mV Battery", (long)voltage];
                offset += length;
                break;
            }
            default:
            {
                // Skip Over
                [messageString appendFormat:@"%02x ", array[offset]];
                NSInteger length = array[offset++];
                int i = 0;
                
                for(i = 0; i < length; i++)
                {
                    [messageString appendFormat:@"%02x", array[offset+i]];
                }
                
                [messageString appendString:@"\n"];
                
                offset+=length;
                break;
            }
        }
    }while(offset < totalLength);
}
@end
