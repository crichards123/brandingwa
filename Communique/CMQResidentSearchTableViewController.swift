//
//  CMQResidentSearchTableViewController.swift
//  Communique
//
//  Created by Andre White on 1/12/18.
//  Copyright © 2018 Communique, LLC. All rights reserved.
//

import UIKit
import ParseUI
private let cellIdentifier = "reuse"
public class CMQResidentSearchTableViewController: CMQSearchControllerTableViewController {
    override public func queryForTable() -> PFQuery<PFObject> {
        let residentQuery = PFQuery.residentQuery()
        let packageQuery = PFQuery.packageQuery()
        residentQuery.whereKey("objectId", matchesKey: "recipientID", in: packageQuery)
        return residentQuery
    }
    override public func viewDidLoad() {
        super.viewDidLoad()
        self.resultsController?.tableView.delegate = self
    }
    override public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    override public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath, object: PFObject?) -> PFTableViewCell? {
        guard let resident = object as? CMQUser, let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? CMQPackageDetailsTableViewCell else {return nil}
        cell.residentNameLabel.text = resident.fullName
        cell.buildingButton.setTitle(resident.buildingUnit, for: .normal)
        return cell
    }
    override public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let completion = { (packages:[CMQPackage]?, error:Error?) in
            if let error = error{
                //unable to load packages for resident
                handle(error: error, message: "Unable to load packages for resident. Please try again later", shouldDisplay: true)
            }else if let packages = packages{
                self.performSegue(withIdentifier: "Select", sender: packages)
            }
        }
        if let resident = tableView == self.tableView ? object(at: indexPath) as? CMQUser : resultsController?.filteredResidents[indexPath.row] as? CMQUser
         {
            CMQPackageDataSourceManager.sharedInstance().packagesFor(resident: resident, isCheckIn: true, completion: completion)
        }
    
    }
    override public func updateSearchResults(for searchController: UISearchController) {
        
        resultsController?.tableType = 1
        super.updateSearchResults(for: searchController)
    }
    // MARK: - Navigation
    override public func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let dest = segue.destination as? CMQPackageLogResidentViewController, let packages = sender as? [Any]{
            dest.dataSource = packages
        }
    }

}
