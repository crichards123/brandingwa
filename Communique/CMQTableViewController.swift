//
//  CMQTableViewController.swift
//  Communique
//
//  Created by Andre White on 1/31/18.
//  Copyright © 2018 Communique, LLC. All rights reserved.
//

import UIKit
import ParseUI
public class CMQTableViewController: UITableViewController {
    private var currentObjects:[PFObject] = Array<PFObject>()
    private var loading:Bool = false
    public var loaded:Bool = false
    public var shouldLoadAgain:Bool = false
    private var retryAttempts = 0
    private var maxRetryAttempts = 7
    public var shouldLoadFromBotton: Bool = true
    private let offset = -14
    private var multiplier:Int = 1
    private var hasMore = true
    public var queryGroup = DispatchGroup()
    @IBOutlet var headerView: CMQHomeFeedHeader!
    
    private var noContentViewAdded = false
    private var noContentView:CMQNoContentView? {
        guard let view = CMQNoContentView.instanceFromNib() else {return nil}
        view.set(parseClassName: "Posts")
        return view
    }
    override public func viewDidLoad() {
        super.viewDidLoad()
        self.loadObjects()
        if #available(iOS 10.0, *) {
            let refresh = UIRefreshControl.init()
            refresh.addTarget(self, action: #selector(refreshObjects), for: .valueChanged)
            self.tableView.refreshControl = refresh
        } else {
            // Fallback on earlier versions
        }
    }
    public override func viewWillAppear(_ animated: Bool) {
        if loaded{
            //self.refreshObjects()
        }
    }
    public func restoreInitialState(){
        multiplier = 1
        hasMore = true
        retryAttempts = 0 
    }
    public func queryForObjects()->[PFQuery<PFObject>]{
        /*var eventQueries:[PFQuery] = [PFQuery]()
        var messageQueries:[PFQuery] = [PFQuery]()
        var newsQueries:[PFQuery] = [PFQuery]()*/
        var apartmentIDs:[String] = [String]()
        if CMQUser.current().isAdmin{
            if let apartment = CMQAPIManager.sharedInstance().apartment{
                apartmentIDs.append(apartment.objectId!)
            }else if let apartments = CMQAPIManager.sharedInstance().apartments{
                apartmentIDs = apartments.compactMap({$0.objectId})
            }
        }else{
            apartmentIDs = [CMQUser.current().complexID]
        }
        let comparisonDate = Date.MidnightToday?.adding(days:offset * multiplier)
        let events = PFQuery.eventsQuery(apartmentIDs: apartmentIDs)!
        events.whereKey("createdAt", greaterThan:comparisonDate!)
        let messages = PFQuery.messageQuery(apartmentIDs: apartmentIDs) as! PFQuery
        messages.whereKey("createdAt", greaterThan:comparisonDate!)
        let news = PFQuery.newsQuery(apartmentIDs: apartmentIDs)!
        news.whereKey("createdAt", greaterThan:comparisonDate!)

        
        let scheduledMessages = PFQuery.messageQuery(apartmentIDs: apartmentIDs) as! PFQuery
        scheduledMessages.whereKey("scheduleMessage", equalTo: true)
        scheduledMessages.whereKey("scheduleTime", greaterThan: comparisonDate!.toParse)
        /*for apartmentID in apartmentIDs{
            
            let events = PFQuery.eventsQuery(apartmentIDs: [apartmentID])!
            events.whereKey("createdAt", greaterThan:comparisonDate!)
            eventQueries.append(events)
            
            let messages = PFQuery.messageQuery(apartmentIDs: [apartmentID]) as! PFQuery
            messages.whereKey("createdAt", greaterThan:comparisonDate!)
            messageQueries.append(messages)
            
            let news = PFQuery.newsQuery(apartmentIDs: [apartmentID])!
            news.whereKey("createdAt", greaterThan:comparisonDate!)
            newsQueries.append(news)
            
            let scheduledMessages = PFQuery.messageQuery(apartmentIDs: [apartmentID]) as! PFQuery
            scheduledMessages.whereKey("scheduleMessage", equalTo: true)
            scheduledMessages.whereKey("scheduleTime", greaterThan: comparisonDate?.toParse())
            messageQueries.append(scheduledMessages)
        }*/
        //let eventOr = PFQuery.orQuery(withSubqueries: eventQueries)
        events.includeKey("userWhoPosted")
        events.whereKey("objectId", notContainedIn: self.currentObjects.compactMap({$0.objectId}))
        //let messageOr = PFQuery.orQuery(withSubqueries: messageQueries)
        messages.includeKey("userWhoPosted")
        messages.includeKey("Package")
        messages.whereKey("packageNoti", notEqualTo: true)
        messages.whereKey("objectId", notContainedIn: self.currentObjects.compactMap({$0.objectId}))
        //let newsOr = PFQuery.orQuery(withSubqueries: newsQueries)
        news.includeKey("userWhoPosted")
        news.whereKey("objectId", notContainedIn: self.currentObjects.compactMap({$0.objectId}))
        let queries = [events, messages, news]
        
        /*let events = PFQuery.eventsQuery()! as! PFQuery
        let messages = PFQuery.messageQuery() as! PFQuery
        let news = PFQuery.newsQuery()! as! PFQuery
        let scheduledMessages = PFQuery.messageQuery() as! PFQuery
        scheduledMessages.whereKey("scheduleMessage", equalTo: true)
        let queries = [/*events, news,*/ messages, scheduledMessages]
        
        for query in queries{
            if query != scheduledMessages{
                query.whereKey("createdAt", greaterThan:comparisonDate!)
            }else{
                query.whereKey("scheduleTime", greaterThan: comparisonDate?.toParse())
            }
        }*/
        return queries
    }
    public func completionForObjects()-> (_ objects:[PFObject]?,_ error:Error?)->Void{
        return { (objects:[PFObject]?, error:Error?)->Void in
            guard error == nil else {
                handle(error: error!, message: "Error retrieving. Please try again later", shouldDisplay: true)
                return
            }
            CMQUser.current().messagesUpToDate = true
            CMQUser.current().eventsUpToDate = true
            CMQUser.current().newsUpToDate = true
            CMQUser.current().saveInBackground()
            if let objects = objects{
                let returnedObjects = Set(objects)
                if returnedObjects.isEmpty{
                    if self.retryAttempts <= self.maxRetryAttempts{
                        self.retryAttempts += 1
                        self.multiplier += 1
                        self.loadObjects()
                    }else{
                        self.hasMore = false
                        self.tableView.reloadData()
                        if self.currentObjects.isEmpty {
                            self.addNoContentView()
                        }
                        return
                    }
                    
                }
                guard !returnedObjects.isEmpty else {return}
                if self.retryAttempts > 0{
                    self.retryAttempts = 0
                }
                if self.currentObjects.count == 0 {
                    self.currentObjects = objects
                    self.currentObjects = objects.filter({ (object) -> Bool in
                        objects.filter({$0.objectId == object.objectId}).count == 0 
                        if let message = object as? CMQMessage{
                            return message.deliveryStatus == nil
                            //return !message.messageContent.contains("You have a package to pick up at the office.")
                        }else{
                            return true
                        }
                    })
                }else{
                    self.hasMore = objects.count != 0
                    for object in objects{
                        if self.currentObjects.filter({$0==object}).count==0{
                            self.currentObjects.append(object)
                        }
                    }
                }
                self.currentObjects = self.currentObjects.sorted(by: {$0.sortDate>$1.sortDate})
                if self.currentObjects.count > 0 {
                    if self.noContentViewAdded{
                        self.removeNoContentView()
                    }
                    self.tableView.reloadData()
                }else{
                    self.addNoContentView()
                }
            }
            
            self.loading = false
            self.loaded = true
            self.queryGroup.leave()
        }
    }
    public func completionForRefresh()-> (_ objects:[PFObject]?,_ error:Error?)->Void{
        return  { (objects:[PFObject]?, error:Error?)->Void in
            if #available(iOS 10.0, *) {
                self.tableView.refreshControl?.endRefreshing()
            } else {
                // Fallback on earlier versions
            }
            if let error = error{
                handle(error: error, message: "Error retrieving. Please try again later", shouldDisplay: true)
            }
            else if let newObjects = objects?.sorted(by: {$0.sortDate>$1.sortDate}){
                if newObjects.count > 0{
                    self.currentObjects.append(contentsOf: newObjects)
                    self.currentObjects.sort(by: {$0.sortDate>$1.sortDate})
                    if self.currentObjects.count > 0 {
                        if self.noContentViewAdded{
                            self.removeNoContentView()
                        }
                        self.tableView.reloadData()
                    }else{
                        self.addNoContentView()
                    }
                }
            }
            self.loading = false
        }
    }
    public func loadObjects(){
        queryGroup.enter()
        loading = true
        let queries = queryForObjects()
        let completion = completionForObjects()
        /*for query in queries{
            //query.limit = 10
            //query.skip = currentObjects.filter({$0.parseClassName == query.parseClassName}).count
        }*/
        if queries.count>1{
            CMQAPIManager.runConcurrentQueries(queries: queries, completion: completion)
        }else{
            queries.first?.findObjectsInBackground(block: completion)
        }
        queryGroup.notify(queue: .main) {
            
        }
    }
    public func onRefresh(){
        if !loading{
            self.refreshObjects()
        }
        
    }

    public func addNoContentView(){
        if !noContentViewAdded{
            guard let contentView = noContentView else {return}
            contentView.frame = CGRect.init(origin: .zero, size: self.view.frame.size)
            self.tableView.tableFooterView = UIView()
            self.view.addSubview(contentView)
            noContentViewAdded = true
            
        }
    }
    public func removeObjects(){
        self.currentObjects.removeAll()
        self.hasMore = true
    }
    public func removeNoContentView(){
        guard let view = self.view.subviews.filter({$0 is CMQNoContentView}).first else {return}
        view.removeFromSuperview()
        noContentViewAdded = false
    }
    @objc public func refreshObjects(){
        if !loading{
            loading = true
            let queries = queryForObjects(); let completion = completionForRefresh()
            for query in queries{
                query.whereKey("objectId", notContainedIn: self.currentObjects.compactMap({$0.objectId}))
            }
            if queries.count>1{
                CMQAPIManager.runConcurrentQueries(queries: queries, completion: completion)
            }else{
                queries.first?.findObjectsInBackground(block: completion)
            }
        }
        
    }

    // MARK: - Table view data source
    public override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        guard tableView == self.tableView, CMQAPIManager.sharedInstance().apartment != nil else {return 1}
        if noContentViewAdded{
            return 1
        }
        if UIDevice.current.userInterfaceIdiom == .pad{
            return self.view.frame.height * 0.45
        }
        return self.view.frame.height*0.33
    }
    override public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let header = headerView, tableView == self.tableView, self.tableView(tableView, heightForHeaderInSection: 0) != 1 else {return nil}
        header.aptImageView.image = nil
        header.aptImageView.file = CMQAPIManager.sharedInstance().apartment.aptPhoto
        header.aptImageView.loadInBackground()
        return header
    }
    override public func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == self.currentObjects.count{
            if !loading && hasMore{
                multiplier += 1
                loadObjects()
            }
        }
    }
    override public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return hasMore ? currentObjects.count+1 : currentObjects.count
    }
    override public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == currentObjects.count{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Refresh", for: indexPath)
            let refreshControl = cell.viewWithTag(3) as? UIActivityIndicatorView
            refreshControl?.startAnimating()
            return cell
        }else{
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "Message", for: indexPath) as? CMQFeedTableViewCell else {return UITableViewCell()}
            let object = currentObjects[indexPath.row]
            cell.reset()
            cell.object = object
            cell.selectionStyle = .none
            return cell
        }
        
    }

}
