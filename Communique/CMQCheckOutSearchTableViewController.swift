//
//  CMQCheckOutSearchTableViewController.swift
//  Communique
//
//  Created by Andre White on 1/13/18.
//  Copyright © 2018 Communique, LLC. All rights reserved.
//

import UIKit
import ParseUI

public class CMQCheckOutSearchTableViewController: CMQSearchControllerTableViewController {
    
    private var transitionHandler: CMQTransitionDelegate?
    override public func queryForTable() -> PFQuery<PFObject> {
        let residentQuery = PFQuery.residentQuery()
        let checkOutQuery = PFQuery.packagesToCheckOutQuery()
        residentQuery.whereKey("objectId", matchesKey: "recipientID", in: checkOutQuery)
        return residentQuery
    }
    @IBAction func correctPinEntry(sender:CMQPinExitSegue){
        guard let sourceVC = sender.source as? CMQCheckOutEnterPinViewController else {return}
        sender.completion = { () in
            self.performSegue(withIdentifier: "CheckOut", sender: sourceVC.packagesForResident)
        }
    }
    @IBAction func cancelPinEntry(sender:UIStoryboardSegue){
        
    }
    public override func initView() {
        //
    }
    override public func viewDidLoad() {
        super.viewDidLoad()
        self.resultsController?.tableView.delegate = self
    }
    @objc public func deliveryUpdate(){
        self.loadObjects()
    }
    
    public func prepareTransition(){
        let size = CGSize.init(width: 350.0, height: 250.0)
        let startX = self.view.center.x - size.width/2
        let startY = self.view.center.y - size.height/2
        self.transitionHandler = CMQTransitionDelegate(size: size, origin: CGPoint.init(x: startX, y: startY))
    }
    public override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
   
    public override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath, object: PFObject?) -> PFTableViewCell? {
        guard let cell = tableView .dequeueReusableCell(withIdentifier: "PackageCell", for: indexPath) as? CMQPackageDetailsTableViewCell, let resident = object as? CMQUser else {return PFTableViewCell()}
        cell.reset()
        let completion = { (packages:[CMQPackage]?, error:Error?) in
            if let error = error as NSError? {
                handle(error: error, message: "Unable to Fetch Packages", shouldDisplay: true)
                //unable to load packages for resident
            }else if let packages = packages{
                let packageByType = packages.separateBy(separator: Array.Separator.PackageType)
                cell.configure(with: resident, andPackages: packageByType)
            }
        }
        CMQPackageDataSourceManager.sharedInstance().checkOutPackages(resident: resident, completion: completion)
        cell.selectionStyle = .none
        return cell
    }
    override public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if objects?.endIndex == indexPath.row{
            super.tableView(tableView, didSelectRowAt: indexPath)
        }else{
            if let resident = tableView == self.tableView ? object(at: indexPath) as? CMQUser : resultsController?.filteredResidents[indexPath.row] as? CMQUser {
                
                let completion = { (packages:[CMQPackage]?, error:Error?) in
                    if let error = error{
                        handle(error: error, message: "Unable to CheckOut Package", shouldDisplay: true)
                        //unable to load packages for resident
                    }else if let packages = packages{
                        let hasPending = CMQPackageDataSourceManager.sharedInstance().isPendingDelivery(packages: packages)
                        let segID = hasPending ? "CheckOut" : "Pin"
                        self.performSegue(withIdentifier: segID, sender: packages)
                    }
                }
                CMQPackageDataSourceManager.sharedInstance().checkOutPackages(resident: resident, completion: completion)
            }
        }
    }
    override public func updateSearchResults(for searchController: UISearchController) {
        self.resultsController?.tableType = 2
        super.updateSearchResults(for: searchController)
        
    }
    override public func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Pin"{
            if let dest = segue.destination as?
                CMQCheckOutEnterPinViewController{
                self.prepareTransition()
                dest.transitioningDelegate = self.transitionHandler
                dest.modalPresentationStyle = .custom
                dest.packagesForResident = sender as? [CMQPackage]
            }
        }else if let dest = segue.destination as? CMQPackagesCheckOutViewController{
            dest.packagesForResident = sender as? [CMQPackage]
        }
    }

}
