//
//  IDCollectionViewController.swift
//  Communique
//
//  Created by Andre White on 6/4/18.
//  Copyright © 2018 Communique, LLC. All rights reserved.
//

import UIKit


struct AptCellCreator:IDCollectionCellConfigureProtocol{
    typealias ViewType = UICollectionView
    typealias CellType = UICollectionViewCell
    
    var dataSource: IDViewDataSourceProtocol
    var reuseIdentifier: String{
        return "communityCell"
    }
    func configure(cell: UICollectionViewCell, atIndex: IndexPath) {
        guard let cell = cell as? CMQCommunityCollectionViewCell else {return}
        guard let community = self.dataSource.object(at: atIndex) as? CMQApartment else {return}
        cell.alpha = 1
        if let file = community.aptPhoto{
            cell.communityImageView.file = file
            cell.communityImageView.loadInBackground()
        }else{
            cell.communityImageView.image = nil
        }
        cell.communityNameLabel.text = community.aptName
        if let apt = CMQAPIManager.sharedInstance().apartment{
            if community.objectId == apt.objectId{
                cell.alpha = 0.25
            }
        }
    }
}




public class IDCollectionViewController: UICollectionViewController, IDDataSourceDelegateProtocol {
    public func didChange(dataSource: IDViewDataSourceProtocol, toState: IDDataSourceState) {
        
    }
    open var cellCreator:IDCollectionViewCellCreatorProtocol!
    public var dataSource: IDViewDataSourceProtocol!{
        didSet{
            dataSource.delegate = self
        }
    }
    override public func viewDidLoad() {
        super.viewDidLoad()


        // Do any additional setup after loading the view.
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

    // MARK: UICollectionViewDataSource

    override public func numberOfSections(in collectionView: UICollectionView) -> Int {
        return dataSource.numOfSec
    }


    override public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataSource.numOfRows
    }

    override public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        return self.cellCreator.collectionView(collectionView, cellForItemAt: indexPath)
    }

    // MARK: UICollectionViewDelegate

    /*
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    override func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment this method to specify if the specified item should be selected
    override func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
    override func collectionView(_ collectionView: UICollectionView, shouldShowMenuForItemAt indexPath: IndexPath) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, canPerformAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
    
    }
    */

}
extension IDCollectionViewController: IDDataSourceStateChange{
    func updateUI(wState: IDDataSourceState) {
        print(wState)
        switch wState {
        case .loaded(_):
            self.collectionView?.reloadData()
        default:
            return
        }
    }
    
    
}
