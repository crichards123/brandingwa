//
//  Classes.swift
//  Communique
//
//  Created by Andre White on 10/23/18.
//  Copyright © 2018 Communique, LLC. All rights reserved.
//

import Foundation
import SVProgressHUD
protocol ErrorHandlerProtocol {
    func log(error:Error?, message:String?)
    func showError(message:String)
    func handle(error:Error, shouldShow:Bool, message:String?)
}
extension ErrorHandlerProtocol{
    func log(error:Error?, message:String?){
        print("Error: \(error?.localizedDescription ?? "")\nMessage:\(message ?? "")")
    }
    func handle(error:Error, shouldShow:Bool, message:String?){
        self.log(error: error, message: message)
        if shouldShow{
            self.showError(message: message ?? error.localizedDescription)
        }
    }
}
protocol FeedBackProtocol {
    var topController:UIViewController? {get}
    func showSuccess(message:String)
    func showError(message:String)
    func show()
    func dismiss()
}
extension FeedBackProtocol{
    func showSuccess(message: String) {
        SVProgressHUD.showSuccess(withStatus: message)
        SVProgressHUD.dismiss(withDelay: 2)
    }
    
    func showError(message: String) {
        SVProgressHUD.showError(withStatus: message)
        SVProgressHUD.dismiss(withDelay: 2)
    }
    func show(message:String){
        SVProgressHUD.show(withStatus: message)
    }
    func show() {
        SVProgressHUD.show()
    }
    func dismiss() {
        SVProgressHUD.dismiss()
    }
}
class FeedbackManager:FeedBackProtocol{
    var topController: UIViewController?
    
    static let shared = FeedbackManager()
    init() {
        DispatchQueue.main.async {
            SVProgressHUD.setBackgroundColor(UIColor.DarkColor)
            SVProgressHUD.setForegroundColor(UIColor.white)
        }
    }
}
