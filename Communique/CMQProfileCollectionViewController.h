//
//  CMQProfileCollectionViewController.h
//  Communique
//
//  Created by Colby Melvin on 12/20/16.
//  Copyright © 2016 Communique, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CMQProfileCollectionViewController : GAITrackedViewController

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UIButton *signOutButton;

@end
