//
//  CMQAppConfig.h
//  Communique
//
//  Created by Chris Hetem on 9/17/14.
//  Copyright (c) 2014 WaveRider, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
//enums
typedef NS_ENUM(NSInteger, CMQPackageType){
    CMQPackageTypeBoxSmall,
    CMQPackageTypeBoxLarge,
    CMQPackageTypeEnvelope,
    CMQPackageTypeOther
};
//Protocols
@protocol CMQPackageDataSourceProtocol
-(NSArray*)packagesForType:(CMQPackageType)packageType;
-(NSString*)residentName;
-(NSString*)residentBuilding;
@end


//Fonts
NSString *const kCMQFontGeometriaLight;
NSString *const kCMQFontGeometriaBold;
//NSString *const kCMQFontLMMLBold;
//NSString *const kCMQFontLMMLBoldOblique;

//Parse
NSString *const kParseAppId;
NSString *const kParseClientKey;
NSString *const kParseServerURL;
NSString *const kParseAptComplexIDKey;
NSString *const kParseEventAttendeesKey;
NSString *const kParseMessageRecipientsKey;
NSString *const kParseEventDateKey;
NSString *const kParseObjectCreateDateKey;

//Push Notifications
NSString *const kParsePushChannelAllKey;
NSString *const kParsePushChannelUrgentKey;

//HTMLParsing
NSString *const kHTMLParserTitleKey;
NSString *const kHTMLParserImageURLArray;

//Crashlytics
NSString *const kCrashlyticsAppKey;

//Management
NSString *const kAcceptedManagements;

//CQ
NSString *const kApplicationId;
NSString *const kiTunesURL;
NSString *const kLoginSupportEmail;
NSString *const kGeneralSupportEmail;
NSString *const kTermsOfServiceURL;
NSString *const kPrivacyPolicyURL;
NSString *const kActionSheetHelp;
NSString *const kActionSheetUber;
NSString *const kActionSheetContact;
NSString *const kContactModeEmail;
NSString *const kContactModePhone;
NSString *const kContactModeWiFi;

//Permissions
NSString *const kPermissionResident;
NSString *const kPermissionInterestedParty;
NSString *const kPermissionStaff;
NSString *const kPermissionAdmin;
NSString *const kPermissionRegionalAdmin;
NSString *const kPermissionCompanyAdmin;
NSString *const kPermissionSuperadmin;

//Epproach
NSString *const kEpproachSupportPhoneNumber;
NSString *const kEpproachSupportEmail;

//PayLease
NSString *const kPayLeaseRegistrationURL;

//Packages
NSString *const kPackageCarrierUSPS;
NSString *const kPackageCarrierUPS;
NSString *const kPackageCarrierFedEx;
NSString *const kPackageCarrierOther;
NSString *const kPackageTypeSmallBox;
NSString *const kPackageTypeLargeBox;
NSString *const kPackageTypeEnvelope;
NSString *const kPackageTypeOther;
NSString *const kPackageTagPerishable;
NSString *const kPackageTagUrgent;
NSString *const kPackageTagOther;
NSString *const kPackageNoneSelected;

//Uber
NSString *const kUberAccessTokenString;
NSString *const kUberConnectionErrorString;
NSString *const kUberModeHome;
NSString *const kUberModePickup;

//Dormakaba
NSString* const kDormaKabaServerUrl;
NSString* const kDormaKabaUserName;
NSString* const kDormaKabaPassword;


#define CMQAD ( (CMQAppDelegate *)[[UIApplication sharedApplication] delegate] )
