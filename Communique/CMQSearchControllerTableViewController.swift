//
//  CMQSearchControllerTableViewController.swift
//  Communique
//
//  Created by Andre White on 1/12/18.
//  Copyright © 2018 Communique, LLC. All rights reserved.
//

import UIKit

public class CMQSearchControllerTableViewController: CMQQueryTableViewController {
    open var selectionCompletion: PFArrayResultBlock?
    public var resultsController : CMQResidentSearchResultsTableViewController? {
        didSet{
            guard let resultsController = resultsController else{ return }
            resultsController.tableView.delegate = self
            self.searchController = CMQSearchController.init(searchResultsController: resultsController)
        }
    }
    public var searchController : CMQSearchController?{
        didSet{
            guard let searchController = searchController else {return}
            searchController.searchResultsUpdater = self
            searchController.delegate = self
            if #available(iOS 11.0, *) {
                self.navigationItem.searchController = searchController
                self.navigationItem.hidesSearchBarWhenScrolling = false
            } else {
                self.tableView.tableHeaderView = searchController.searchBar
            }
        }
    }
    override public func viewDidLoad() {
        super.viewDidLoad()
        setUpSearchController()
    }
    
    func setUpSearchController(){
        if let aClass = NSClassFromString("CMQPackage"){
            let bundle = Bundle.init(for: aClass)
            self.resultsController = UIStoryboard.init(name: "Packages", bundle: bundle).instantiateViewController(withIdentifier: "SearchResults") as? CMQResidentSearchResultsTableViewController
        }
    }
}
extension CMQSearchControllerTableViewController : UISearchResultsUpdating, UISearchControllerDelegate{
    public func updateSearchResults(for searchController: UISearchController) {
        guard let searchText = searchController.searchBar.text, let resultsController = self.resultsController, let residents = objects as? [CMQUser] else {return}
        resultsController.filteredResidents = residents.filter({ (resident) -> Bool in
            return (resident.fullName.uppercased().range(of: searchText.uppercased()) != nil) || (resident.buildingUnit.uppercased().range(of: searchText.uppercased()) != nil)
        }) as? [CMQPackageDataSourceProtocol]
        resultsController.tableView.reloadData()
    }
}

