//
//  CMQResidentContainer.swift
//  Communique
//
//  Created by Andre White on 5/30/18.
//  Copyright © 2018 Communique, LLC. All rights reserved.
//

import UIKit

public class CMQResidentContainer: CMQParseContainer {
    fileprivate var isFiltering:Bool = false
    var filteredData:[Item] = [Item]()
    public init(checkOut:Bool = false, log:Bool = false){
        var parseQuery = CMQParseQuery.init(queries: [PFQuery.residentQuery()])
        if checkOut{
            let checkOutResidentQuery = PFQuery.residentQuery()
            let checkOutPackageQuery = PFQuery.packagesToCheckOutQuery()
            checkOutResidentQuery.whereKey("objectId", matchesKey: "recipientID", in: checkOutPackageQuery)
            parseQuery = CMQParseQuery.init(queries: [checkOutResidentQuery])
        }
        if log{
            let residentQuery = PFQuery.residentQuery()
            let packageQuery = PFQuery.packageQuery()
            residentQuery.whereKey("objectId", matchesKey: "recipientID", in: packageQuery)
            parseQuery = CMQParseQuery.init(queries: [residentQuery])
        }
        super.init(query: parseQuery)
    }
    public init(objects:[CMQUser]){
        let query = CMQParseQuery.init(queries: [PFQuery.residentQuery()])
        super.init(query: query, objects: objects)
        
    }
    override func handleReturnedObjects(objects: [PFObject], completion: @escaping (Error?) -> Void) {
        self.data.append(contentsOf: objects)
        completion(nil)
    }
    func search(string:String, completion: () -> Void){
        self.filteredData = self.data.filter { (object) -> Bool in
            guard let resident = object as? CMQUser else {return false}
            return (resident.fullName.uppercased().range(of: string.uppercased()) != nil) || (resident.buildingUnit.uppercased().range(of: string.uppercased()) != nil)
        }
        completion()
    }
}
extension CMQResidentContainer: IDViewDataSourceProtocol{
    public var numOfRows: Int {
        return isFiltering ? filteredData.count : data.count
    }
    
    public var numOfSec: Int {
        return 1
    }
    
    public var canPaginate: Bool {
        return false
    }
    
    public func object(at: IndexPath) -> Any? {
        guard at.row < self.numOfRows else {return nil}
        return isFiltering ? filteredData[at.row] : data[at.row]
    }
    
    public func willChangeState(to: IDDataSourceState) {
        guard self.state != to else {return}
        switch to {
        case .loading:
            self.state = to
            self.load { (error) in
                guard error == nil else {return}
                self.state = .loaded(prev: to)
            }
        case .filtering(let filter):
            self.state = to
            self.isFiltering = filter != ""
            self.search(string: filter) {
                self.state = .loaded(prev: to)
            }
        default:
            break
        }
    }
    
    
}
