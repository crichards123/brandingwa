//
//  CMQNewsFeedContainer.swift
//  Communique
//
//  Created by Andre White on 5/26/18.
//  Copyright © 2018 Communique, LLC. All rights reserved.
//

import UIKit

public class CMQNewsFeedContainer: CMQFeedContainer {

    
    public init(){
        super.init(query: CMQParseQuery.init(queries: [PFQuery.NewsQuery!]))
    }
    override var orderIncludeManipulator: ParseQueryManip{
        return { (Query) in
            Query.order(byDescending: kParseObjectCreateDateKey)
            Query.includeKey("userWhoPosted")
        }
    }
}
