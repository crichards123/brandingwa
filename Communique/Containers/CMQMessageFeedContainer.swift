//
//  CMQMessageFeedContainer.swift
//  Communique
//
//  Created by Andre White on 5/26/18.
//  Copyright © 2018 Communique, LLC. All rights reserved.
//

import UIKit

public class CMQMessageFeedContainer: CMQFeedContainer {
    
    init(){
        let messages = PFQuery.messageQuery(apartmentIDs: [CMQUser.current().complexID]) as! PFQuery
        messages.whereKey("deliveryStatus", notEqualTo: "DELIVERED OR PICKED UP")
        let query = CMQParseQuery.init(queries: [messages])
        super.init(query: query)
    }
    override var paginateManipulator: ParseQueryManip{
        return { (Query) in
            let comparisonDate = Date.MidnightToday?.adding(days:self.offset * self.pageNumber)
            Query.whereKey("updatedAt", greaterThan:comparisonDate!)
            Query.whereKey("objectId", notContainedIn: self.data.compactMap({$0.objectId}))
        }
    }
    var paginationModifierScheduled:Modifier{
        return Modifier.init(modifier: self.paginateManipulatorScheduled)
    }
    var paginateManipulatorScheduled:ParseQueryManip{
        return { (Query) in
            let comparisonDate = Date.MidnightToday?.adding(days:self.offset * self.pageNumber)
            Query.whereKey("scheduleMessage", equalTo: true)
            Query.whereKey("scheduleTime", greaterThan: comparisonDate!.toParse)
            Query.whereKey("objectId", notContainedIn: self.data.compactMap({$0.objectId}))
        }
    }
    override var orderIncludeManipulator: ParseQueryManip{
        return { (Query) in
            Query.order(byDescending: "updatedAt")
            Query.includeKey("Package")
            Query.includeKey("userWhoPosted")
        }
    }
    override func prepareToLoad() -> CMQParseContainer.Query {
        let qOne = self.loadQuery.queries.map({self.paginationModifier.modify(item: $0.copy() as! ParseQueryModifier.Manipulatable)})
        let qTwo = self.loadQuery.queries.map({self.paginationModifierScheduled.modify(item: $0.copy() as! ParseQueryModifier.Manipulatable)})
        
        let queries = zip(qOne, qTwo).map({PFQuery.orQuery(withSubqueries: [$0,$1])}).map({self.orderIncludeModifier.modify(item: $0)})
        return CMQParseQuery.init(queries: queries)
    }

}
