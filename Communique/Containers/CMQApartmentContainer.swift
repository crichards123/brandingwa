//
//  CMQApartmentContainer.swift
//  Communique
//
//  Created by Andre White on 5/30/18.
//  Copyright © 2018 Communique, LLC. All rights reserved.
//

import UIKit

public class CMQApartmentContainer: CMQParseContainer {
    public init(){
        let query = CMQApartment.query()!
        let commIDs:[String] = CMQUser.current().communities.compactMap({($0 as! CMQApartment).objectId})
        //query.whereKey("objectId", containedIn: commIDs)
        query.whereKey("objectId", containedIn: commIDs)
        query.order(byAscending: "aptName")
        super.init(query: CMQParseQuery.init(queries: [query]))
    }

    override func handleReturnedObjects(objects: [PFObject], completion: @escaping (Error?) -> Void) {
        self.data.append(contentsOf: objects)
        completion(nil)
    }
}

extension CMQApartmentContainer: IDViewDataSourceProtocol{
    public var numOfRows: Int {
        return data.count
    }
    
    public var numOfSec: Int {
        return 1
    }
    
    public var canPaginate: Bool {
        return false
    }
    
    public func object(at: IndexPath) -> Any? {
        guard at.row < numOfRows else {return nil}
        return self.data[at.row]
    }
    
    public func willChangeState(to: IDDataSourceState) {
        guard self.state != to else {return}
        switch to {
        case .loading:
            self.state = to
            self.load { (error) in
                guard error == nil else {return}
                self.state = .loaded(prev: to)
            }
        default:
            break
        }
    }
    
    
}
