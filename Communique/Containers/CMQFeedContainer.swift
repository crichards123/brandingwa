//
//  CMQFeedContainer.swift
//  Communique
//
//  Created by Andre White on 5/28/18.
//  Copyright © 2018 Communique, LLC. All rights reserved.
//

import UIKit

public class CMQFeedContainer: CMQParseContainer, IDPagination {
    //IDConnectedDataRetryProtocol
    var retryAttempts: Int = 0
    
    //All functions are guarded by the shouldPaginate property. If false, none will execute.
    //IDPagination
    var pageNumber: Int = 1
    internal var hasMore:Bool = true
    var shouldPaginate:Bool = true
    
    typealias Modifier = ParseQueryModifier
    var paginationModifier:Modifier{
        return Modifier.init(modifier: self.paginateManipulator)
    }
    var orderIncludeModifier:Modifier{
        return Modifier.init(modifier: self.orderIncludeManipulator)
    }
    var paginateManipulator:ParseQueryManip {
        return { (Query) in
            let comparisonDate = Date.MidnightToday?.adding(days:self.offset * self.pageNumber)
            Query.whereKey("createdAt", greaterThan:comparisonDate!)
            Query.whereKey("objectId", notContainedIn: self.data.compactMap({$0.objectId}))
        }
    }
    var orderIncludeManipulator:ParseQueryManip{
        return { (Query) in
            Query.order(byDescending: "createdAt")
            Query.includeKey("userWhoPosted")
        }
    }
    var offset:Int {
        return -21
    }
    func paginate(completion:@escaping(_ error:Error?)->Void){
        guard self.shouldPaginate else {return}
        self.pageNumber += 1
        self.load(completion: completion)
    }
    func prepareToLoad()->Query{
        let queries = self.loadQuery.queries.map({self.paginationModifier.modify(item: $0)}).map({self.orderIncludeModifier.modify(item: $0)})
        
        let query = CMQParseQuery.init(queries: queries)
        return query
    }
    
    override func load(completion: @escaping (Error?) -> Void) {
        let query = self.prepareToLoad()
        query.runQuery { (objects, error) in
            if let objects = objects{
                if objects.count == 0{
                    self.retry(completion: completion)
                }else{
                    self.retryAttempts = 0
                    self.hasMore = true
                    self.handleReturnedObjects(objects: objects, completion: completion)
                }
            }else if let error = error{
                completion(error)
            }
        }
    }
}

//Adds retry if the query returns empty objects up to max retry attemps.
extension CMQFeedContainer:IDConnectedDataRetryProtocol{
    
    var maxRetryAttempts: Int {
        return 7
    }
    /*
     If the current state isn't refresh, (because theres no reason to retry a refresh) update retry attempts and retry. Only works for loading and pagination states. Otherwise, unknown state.
     
     */
    func retry(completion: @escaping (Error?) -> Void) {
        switch state {
        case .loading:
            if self.retryAttempts <= maxRetryAttempts{
                self.retryAttempts += 1
                self.load(completion: completion)
            }else{
                //Done retrying
                self.hasMore = false
                completion(nil)
            }
        case .paginating:
            if (self.offset*self.pageNumber > -365){ //Will go back about 26 times with current offset being 14 days
                self.paginate(completion: completion)
            }else{
                //Done retrying
                self.hasMore = false
                completion(nil)
            }
        default:
            completion(nil)
        }
    }
}
extension CMQFeedContainer:IDViewDataSourceProtocol{
    public var numOfRows: Int {
        return data.count
    }
    
    public var numOfSec: Int {
        return 1
    }
    
    public var canPaginate: Bool {
        return self.hasMore
    }
    
    public func object(at: IndexPath) -> Any? {
        guard at.row < self.numOfRows else {return nil}
        return self.data[at.row]
    }
    
    public func willChangeState(to: IDDataSourceState) {
        guard self.state != to else {return}
        switch to {
        case .loaded(_):
            break
        case .loading:
            self.state = to
            self.load { (error) in
                guard error == nil else {return}
                self.state = .loaded(prev: to)
            }
            
        case .refreshing:
            guard self.state != .loading else {return}
            self.state = to
            self.refresh { (error) in
                guard error == nil else {return}
                self.state = .loaded(prev: to)
            }
        case .paginating:
            guard self.state != .loading else {return}
            guard self.canPaginate else {return}
            self.state = to
            self.paginate { (error) in
                guard error == nil else {return}
                self.state = .loaded(prev: to)
            }
        case .filtering(let filter):
            print("Should filter \(filter)")
            //self.search(string: filter, completion: {
            //     self.state = .loaded(prev: to)
            // })
        }
    }
    
    
}
