//
//  CMQEventsFeedContainer.swift
//  Communique
//
//  Created by Andre White on 5/26/18.
//  Copyright © 2018 Communique, LLC. All rights reserved.
//

import UIKit

public class CMQEventsFeedContainer: CMQFeedContainer {
    
    init() {
        super.init(query: CMQParseQuery.init(queries: [PFQuery.eventsQuery(apartmentIDs: [CMQUser.current().complexID])!]))
    }
    
    override var orderIncludeManipulator: ParseQueryManip{
        return { (Query) in
            Query.order(byAscending: kParseEventDateKey)
            Query.includeKey("userWhoPosted")
        }
    }

}
