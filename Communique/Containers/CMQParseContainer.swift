//
//  CMQParseContainer.swift
//  Communique
//
//  Created by Andre White on 5/26/18.
//  Copyright © 2018 Communique, LLC. All rights reserved.
//

import UIKit

public class CMQParseContainer:IDContainerProtocol,IDConnectedDataSourceProtocol{
    typealias Query = CMQParseQuery
    typealias Item = PFObject
    public var delegate: IDDataSourceDelegateProtocol?
    var data: [PFObject]
    internal var loadQuery: CMQParseQuery
    var state: IDDataSourceState{
        didSet{
            self.delegate?.didChange(dataSource: self as! IDViewDataSourceProtocol, toState: state)
        }
    }
    
    init(query:CMQParseQuery, objects:[PFObject] = [PFObject]()) {
        self.data = objects
        self.loadQuery = query
        self.state = .loaded(prev: nil)
    }
    
    //IDConnectedDataSourceProtocol
    func load(completion: @escaping (Error?) -> Void) {
        let query = self.loadQuery
        query.runQuery { (objects, error) in
            if let error = error{
                completion(error)
            }
            else if let objects = objects{
                if objects.count != 0{
                    self.handleReturnedObjects(objects: objects, completion: completion)
                }else{
                    completion(nil)
                }
                
            }
        }
    }
    
    func refresh(completion: @escaping (Error?) -> Void) {
        self.load(completion: completion)
    }
    
    /*
     
     Handles returned objects from parse query.
     
     */
    internal func handleReturnedObjects(objects:[PFObject], completion: @escaping (Error?) -> Void){
        switch self.state {
        case .refreshing:
            self.data.append(contentsOf: objects)
        default:
            self.mergeObjects(objects: objects)
        }
        self.data.sort(by: {$0.sortDate>$1.sortDate})
        completion(nil)
    }
    /*
     Merges objects returned from Parse query with the current objects without adding duplicates.
     
     */
    private func mergeObjects(objects:[PFObject]){
        if self.data.count == 0 {
            //self.data = objects
            self.data = objects.filter({ (object) -> Bool in
                //objects.filter({$0.objectId == object.objectId}).count == 0
                if let message = object as? CMQMessage{
                    return message.deliveryStatus == nil
                }else{
                    return true
                }
            })
        }else{
            //self.hasMore = objects.count != 0
            for object in objects{
                if self.data.filter({$0.objectId==object.objectId}).count==0{
                    self.data.append(object)
                }
            }
        }
    }
}
public typealias ParseQueryManip = (PFQuery<PFObject>)->Void
typealias CMQParseReturnCompletion = (_ objects:[PFObject]?, _ error:Error?)->Void

//Applies a Modififcation to a PFQuery and returns the new query.
struct ParseQueryModifier:IDModify {
    typealias Manipulatable = PFQuery<PFObject>
    typealias Modifier = (Manipulatable)->Void
    var modifier: (Manipulatable) -> Void
    func modify(item: Manipulatable) -> Manipulatable {
        modifier(item)
        return item
    }
}
//Struct to encapsulate running a parse query. Takes an array of queries and runs the queries.
public struct CMQParseQuery:IDQueryProtcol {
    typealias Completion = CMQParseReturnCompletion
    var queries:[PFQuery<PFObject>]
    func runQuery(completion: @escaping CMQParseReturnCompletion) {
        if queries.count>1{
            self.runConcurrentQueries(queries: queries, completion: completion)
        }else{
            queries.first?.findObjectsInBackground(block: completion)
        }
    }
    private func runConcurrentQueries(queries:[PFQuery<PFObject>], completion:@escaping ([PFObject]?, Error?)->Void){
        var queryError:Error?
        let queryGroup = DispatchGroup()
        var posts = Set<PFObject>()
        for query in queries{
            queryGroup.enter()
            query.findObjectsInBackground(block: { (objects, error) in
                if let error = error{
                    queryError = error
                }else if let objects = objects{
                    posts.formUnion(objects)
                }
                queryGroup.leave()
            })
        }
        queryGroup.notify(queue: DispatchQueue.main) {
            completion(posts.sorted(by: {$0.sortDate>$1.sortDate}), queryError)
        }
    }
}
