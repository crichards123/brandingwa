//
//  CMQHomeFeedContainer.swift
//  Communique
//
//  Created by Andre White on 5/29/18.
//  Copyright © 2018 Communique, LLC. All rights reserved.
//

import UIKit

public class CMQHomeFeedContainer: CMQFeedContainer {
    
    
    init(){
        var apartmentIDs = [String]()
        if CMQUser.current().isAdmin{
            if let apartment = CMQAPIManager.sharedInstance().apartment{
                apartmentIDs.append(apartment.objectId!)
            }else if let apartments = CMQAPIManager.sharedInstance().apartments{
                apartmentIDs = apartments.map({$0.objectId!})
            }
        }else{
            apartmentIDs = [CMQUser.current().complexID]
        }
        let events = PFQuery.eventsQuery(apartmentIDs: apartmentIDs)!
        let messages = PFQuery.messageQuery(apartmentIDs: apartmentIDs) as! PFQuery
        let news = PFQuery.newsQuery(apartmentIDs: apartmentIDs)!
        let scheduledMessages = PFQuery.messageQuery(apartmentIDs: apartmentIDs) as! PFQuery
        scheduledMessages.whereKey("scheduleMessage", equalTo: true)
        let queries = [events, news, PFQuery.orQuery(withSubqueries: [scheduledMessages,messages])]
        super.init(query: CMQParseQuery.init(queries: queries))
    }
    
    
    var paginationModifierScheduled:Modifier{
        return Modifier.init(modifier: self.paginateManipulatorScheduled)
    }
    var paginateManipulatorScheduled:ParseQueryManip{
        return { (Query) in
            let comparisonDate = Date.MidnightToday?.adding(days:self.offset * self.pageNumber)
            Query.whereKey("scheduleMessage", equalTo: true)
            Query.whereKey("scheduleTime", greaterThan: comparisonDate!.toParse)
            Query.whereKey("objectId", notContainedIn: self.data.map({$0.objectId!}))
        }
    }
    override var orderIncludeManipulator: ParseQueryManip{
        return { (Query) in
            Query.includeKey("userWhoPosted")
        }
    }
    
    override func prepareToLoad() -> CMQParseContainer.Query {
        var apartmentIDs = [String]()
        if CMQUser.current().isAdmin{
            if let apartment = CMQAPIManager.sharedInstance().apartment{
                apartmentIDs.append(apartment.objectId!)
            }else if let apartments = CMQAPIManager.sharedInstance().apartments{
                apartmentIDs = apartments.map({$0.objectId!})
            }
        }else{
            apartmentIDs = [CMQUser.current().complexID]
        }
        let events = self.paginationModifier.modify(item: PFQuery.eventsQuery(apartmentIDs: apartmentIDs)!)
        
        let messages = self.paginationModifier.modify(item: PFQuery.messageQuery(apartmentIDs: apartmentIDs) as! PFQuery)
        messages.whereKey("packageNoti", notEqualTo: true)
        
        let news = self.paginationModifier.modify(item: PFQuery.newsQuery(apartmentIDs: apartmentIDs)!)
        
        let scheduledMessages = self.paginationModifierScheduled.modify(item: PFQuery.messageQuery(apartmentIDs: apartmentIDs) as! PFQuery)
        
        let messageQuery = PFQuery.orQuery(withSubqueries: [scheduledMessages,messages])
        messageQuery.order(byDescending: "updatedAt")
        messageQuery.includeKey("Package")
        
        let queries = [events, news, messageQuery].map({self.orderIncludeModifier.modify(item: $0)})
        return CMQParseQuery.init(queries: queries)
        
    }
}
