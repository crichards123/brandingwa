//
//  CMQScheduledMessageContainer.swift
//  Communique
//
//  Created by Andre White on 5/30/18.
//  Copyright © 2018 Communique, LLC. All rights reserved.
//

import UIKit

public class CMQScheduledMessageContainer: CMQParseContainer {
    public init(){
        let messages = CMQMessage.query()!
        messages.whereKey(kParseAptComplexIDKey, equalTo: CMQAPIManager.sharedInstance().apartment.objectId!)
        messages.includeKey("userWhoPosted")
        messages.whereKey("scheduleMessage", equalTo: true)
        messages.whereKey("scheduleSent", equalTo: false)
        let query = CMQParseQuery.init(queries: [messages])
        super.init(query: query)
    }
    
}
extension CMQScheduledMessageContainer: IDViewDataSourceProtocol{
    public var numOfRows: Int {
        return self.data.count
    }
    
    public var numOfSec: Int {
        return 1
    }
    
    public var canPaginate: Bool {
        return false
    }
    
    public func object(at: IndexPath) -> Any? {
        guard at.row < self.data.count else {return nil}
        return data[at.row]
    }
    
    public func willChangeState(to: IDDataSourceState) {
        guard self.state != to else {return}
        switch to {
        case .loaded(_):
            break
        case .loading:
            self.state = to
            self.load { (error) in
                guard error == nil else {return}
                self.state = .loaded(prev: to)
            }
        default:
            break
        }
    }
    
    
}
