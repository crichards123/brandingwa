//
//  CMQCommunityContactsViewController.swift
//  Communique
//
//  Created by Andre White on 3/12/18.
//  Copyright © 2018 Communique, LLC. All rights reserved.
//

import UIKit
import MessageUI
public class CMQCommunityInfoViewController: UICollectionViewController,  UICollectionViewDelegateFlowLayout, MFMailComposeViewControllerDelegate {
    private var items:[MenuItem] =
        [                           MenuItem.init(name: "Email",
                                                  image: UIImage.init(named: "email-Icon",
                                                                      in:CMQBundle!,
                                                                      compatibleWith: nil),
                                                  priority: 1),
                                    MenuItem.init(name: "Phone",
                                                  image: UIImage.init(named: "phone-icon",
                                                                      in:CMQBundle!,
                                                                      compatibleWith: nil),
                                                  priority: 2),
                                    MenuItem.init(name: "Address",
                                                  image: UIImage.init(named: "address-icon",
                                                                      in:CMQBundle!,
                                                                      compatibleWith: nil),
                                                  priority: 3),
                                    MenuItem.init(name: "Community Contact",
                                                  image: UIImage.init(named: "community-contact-icon",
                                                                      in:CMQBundle!,
                                                                      compatibleWith: nil),
                                                  priority: 4),
                                    MenuItem.init(name: "Community Code",
                                                  image: UIImage.init(named: "pincode-icon2",
                                                                      in:CMQBundle!,
                                                                      compatibleWith: nil),
                                                  priority: 5)
            ].filter({ (item) -> Bool in
                switch(item.name){
                case "Community Contact":
                    guard let contacts = CMQAPIManager.sharedInstance().apartment.additionalContacts else {return false}
                    return contacts.count>0
                case "Community Code":
                    guard let codes = CMQAPIManager.sharedInstance().apartment.aptAccessCodes else {return false}
                    return codes.count>0 || CMQAPIManager.sharedInstance().apartment.hasKaba
                default:
                    return true
                }
            }).sorted(by: {$0.priority<$1.priority})
    
    fileprivate var mailComposerVC : MFMailComposeViewController!{
        let controller = MFMailComposeViewController()
        controller.mailComposeDelegate = self
        controller.setMessageBody(String.init(format: "\n\n\n\n\n\nSent from the %@ App %@\nA Communiqué Product", Bundle.main.localizedInfoDictionary!["CFBundleDisplayName"] as! CVarArg,UIApplication.versionAndBuild()), isHTML: false)
        return controller
    }
    //MARK: - Life cycle
    override public func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Community Info"
        
    }
    //MARK: - MailComposeViewControllerDelegate
    public func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        self.dismiss(animated: true, completion: nil)
    }
    //MARK: - CollectionViewFlowLayout
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.init(top: 5, left: 5, bottom: 5, right: 5)
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if indexPath.row == 2{
            return CGSize.init(width: self.view.frame.width-10, height: self.view.frame.height*0.31
            )
        }
        return CGSize.init(width: self.view.frame.width/2 - 10 , height: self.view.frame.height*0.31)
    }
    //MARK: - CollectionView Delegate/ DataSource
    override public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    public override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    override public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CommunityContacts", for: indexPath) as? CMQCommunityInfoCollectionViewCell else {return UICollectionViewCell()}
        let item = items[indexPath.row]
        cell.imageView.image = item.image
        cell.titleLabel.text = item.name
        return cell
    }
    override public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let item = items[indexPath.row]
        item.action(controller: self)
    }
    // MARK: - Navigation
    public func presentMailController(){
        guard let email = CMQAPIManager.sharedInstance().apartment.aptEmail, let mailVC = mailComposerVC else {CMQAPIManager.showError(message: "Email not found"); return }
        mailVC.setToRecipients([email])
        self.present(mailVC, animated: true, completion: nil)
    }
    
    override public func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Additional" {
            let dest = segue.destination as? CMQAdditionalInfoTableViewController
            switch sender as? String{
            case .some("Contact"):
                // dest?.title = "Community Contacts"
                dest?.isForAccessCodes = false
            case .some("Codes"):
                //dest?.title = "Community Codes"
                dest?.isForAccessCodes = true
            default:
                print("unknown sender")
            }
        }
        if segue.identifier == "AdditionalWKaba" {
            let dest = segue.destination as? CMQAdditionalInfoViewController
            switch sender as? String{
            case .some("Contact"):
                dest?.isForAccessCodes = false
            case .some("Codes"):
                dest?.isForAccessCodes = true
            default:
                print("unknown sender")
            }
        }
    }
}
//Simple Extension to MenuItem to allow for the selection of a menu item to perfrom a specific action based on the  name.
extension MenuItem{
    public func action(controller:UIViewController?){
        guard let vc = controller as? CMQCommunityInfoViewController else {return}
        switch self.name{
        case "Email":
            vc.presentMailController()
        case "Phone":
            guard let number = CMQAPIManager.sharedInstance().apartment.aptPhone else {CMQAPIManager.showError(message: "Phone Number not found"); return}
            let phoneNumber: String = "telprompt://".appending(number)
            UIApplication.shared.open(NSURL(string:phoneNumber)! as URL, options: [UIApplication.OpenExternalURLOptionsKey : Any](), completionHandler: nil)
        case "Address":
            guard let address = CMQAPIManager.sharedInstance().apartment.aptAddress, let zip = CMQAPIManager.sharedInstance().apartment.aptZip else {CMQAPIManager.showError(message: "Address not found"); return}
            let mapsLink = "http://maps.apple.com/?address=\(address.replacingOccurrences(of: " ", with: ",")),\(zip)"
            if let url = URL.init(string: mapsLink){
                UIApplication.shared.open(url, options: [UIApplication.OpenExternalURLOptionsKey : Any](), completionHandler: nil)
                
            }
            print("Address")
        case "Community Contact":
            vc.performSegue(withIdentifier: "Additional", sender: "Contact")
        case "Community Code":
            guard let apt = CMQAPIManager.sharedInstance().apartment else {return}
            if apt.hasKaba && CMQUser.current().canUseKaba{
                vc.performSegue(withIdentifier: "AdditionalWKaba", sender: "Codes")
            }else{
                vc.performSegue(withIdentifier: "Additional", sender: "Codes")
            }
        default:
            print("unknown action")
        }
    }
}
extension CMQUser{
    var canUseKaba:Bool{
        return self.kabaReservationNumber != nil
    }
}
