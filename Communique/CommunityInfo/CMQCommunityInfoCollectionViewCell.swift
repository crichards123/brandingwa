//
//  CMQCommunityContactCollectionViewCell.swift
//  Communique
//
//  Created by Andre White on 3/28/18.
//  Copyright © 2018 Communique, LLC. All rights reserved.
//

import UIKit

public class CMQCommunityInfoCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    public override func awakeFromNib() {
        self.contentView.layer.cornerRadius = 2.0
        self.contentView.layer.borderWidth = 1.0
        self.contentView.layer.borderColor = UIColor.clear.cgColor
        self.contentView.layer.masksToBounds = true
        
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowOffset = .zero
        self.layer.shadowRadius = 4.0
        self.layer.shadowOpacity = 1.0
        self.layer.masksToBounds = false
    
    }
    
}
