//
//  CMQDormaKabaManager.swift
//  Communique
//
//  Created by Andre White on 4/17/18.
//  Copyright © 2018 Communique, LLC. All rights reserved.
//

import UIKit
import CoreBluetooth
public class CMQDormaKabaManager: NSObject, LegicSeUIDelegate,LegicRegisterWalletDelegate, LegicConnectionDelegate, CBCentralManagerDelegate {
    public enum KabaState:Int{
        case Unregistered
        case RegistrationInProgess
        case CompletionInProgess
        case RegistrationComplete
        case NotDeployed
        case Ready
    }
    public var currentState:KabaState!{
        didSet{
            UserDefaults.standard.set(currentState.rawValue, forKey: "KabaState")
        }
    }
    private func determineState(){
        let state = KabaState.init(rawValue: UserDefaults.standard.integer(forKey: "KabaState"))
        guard state == nil else {self.currentState = state; return}
        guard let manager = self.connectManager else {self.currentState = .Unregistered; return}
        if let application = self.walletApplications?.first{
            let status = application.getAppStatus().getStatus()
            switch status{
            case Deployed:
                self.currentState = .Ready
            default:
                self.currentState = .NotDeployed
            }
        }else if manager.isUserRegistered(){
            self.currentState = .RegistrationComplete
        }else{
            self.currentState = .Unregistered
        }
        
    }
    //MARK: CBCentralManagerDelegate
    public func centralManagerDidUpdateState(_ central: CBCentralManager) {
        switch central.state {
        case .unknown:
        break
        case .resetting:
        break
        case .unsupported:
        break
        case .unauthorized:
        break
        case .poweredOff:
        self.bluetoothEnabled = false
        case .poweredOn:
        self.bluetoothEnabled = true
        }
    }
    //MARK: Singleton
    private static let shared: CMQDormaKabaManager = CMQDormaKabaManager()
    @objc public class func sharedInstance()->CMQDormaKabaManager{
        return .shared
    }
    override private init(){
        super.init()
        guard let walletID = CMQUser.current().walletID else {return}
        let bleProfile = LegicProfileIds.init(profileId: PROFILEID_BLE)!
        connectManager = LegicIDConnectFactory.createIDConnectManager(withWalletId: walletID.intValue,profileIds: [bleProfile], seUiDelegate: self)
        connectManager?.setUsername(kDormaKabaUserName)
        connectManager?.setPassword(kDormaKabaPassword)
        connectManager?.setServerURL(kDormaKabaServerUrl)
        connectManager?.setConfigParam(FILE_SELECTION_MODE_PARAM_NAME, value: FILE_SELECTION_MODE_VALUE_PRESELECTED_FILE)
        let appDelegate = UIApplication.shared.delegate as! CMQAppDelegate
        connectManager?.setLoggingMode(appDelegate.aslLevelNotice)
        NotificationCenter.default.addObserver(self, selector: #selector(syncComplete), name: NSNotification.Name(NOTIFICATION_FINAL_SYNCHRONIZE_COMPLETE), object: nil)
        cbManager = CBCentralManager.init(delegate: self, queue: .main)
    }
    
    //MARK: Properties
    private var cbManager:CBCentralManager?
    private var transitionDelegate:CMQTransitionDelegate?
    private var isRebooking = false
    private var isLoading = false
    public var registrationComplete = false
    public var isRegistering:Bool = false
    public var isSyncing:Bool = false
    public var bluetoothEnabled:Bool = false
    private var retryAttempts = 0
    private var maxRetryAttempts = 5
    private var hasTried = false
    
    
    private var walletApplications:[LegicWalletApplication]?{
        guard let manager = connectManager else {return nil}
        return manager.getAllCards() as? [LegicWalletApplication]
    }
    @objc public var connectManager:LegicIDConnectManager?
    public var activationToken:String?{
        didSet{
            self.completeWalletRegistration()
        }
    }
    public var topController:UIViewController?
    private var rebookCompletion:PFIdResultBlock{
        return { (success, error) in
            
            if let error = error{
                handle(error: error, message: "Rebook", shouldDisplay: false)
            }else{
                //print(success)
                self.synchronize()
                self.retryAttempts += 1
            }
        }
    }
    public var hasCards:Bool{
        return self.walletApplications != nil
    }
    public var readyToUse:Bool{
        guard let cards = self.walletApplications else {return false}
        guard let application = cards.first else {return false}
        guard application.getAppStatus().getStatus() == Deployed else {return false}
        return true
        
    }
    public var wasInterrupted:Bool{
        guard readyToUse == false else {return false}
        if hasCards && !hasTried{
            return true
        }
        guard let manager = self.connectManager else {return false}
        if manager.isUserRegistered(){
            return true
        }
        return false
    }
    
    //MARK: Functions
    
    
    public func initDormakaba(){
        
    }
    
    //Dormakaba Functions
    //The start of the registration process. Calls the registerWallet() with confirmation method NONE. Uses seid from current CMQUser
    public func registerWallet(){
        guard let manager = connectManager, let seid = CMQUser.current().seid else {return}
        isRegistering = true
        currentState = .RegistrationInProgess
        manager.registerWallet(self, info: nil, publicSEID: seid, confirmationMethod: LegicConfirmationMethods.init(method: NONE))
    }
    //Called right after the register wallet function returns. Uses kabaRegToken from current CMQUser
    private func completeWalletRegistration(){
        guard let manager = connectManager, let token = CMQUser.current().kabaRegToken else {return}
        currentState = .CompletionInProgess
        manager.completeRegistration(self, walletToken: token)
    }
    //Called right after the completeRegistration() returns
    private func synchronize(){
        guard let manager = connectManager else {return}
        manager.synchronize()
    }
    //Should be called right during log out.
    public func unregisterWallet(){
        guard let conMan = self.connectManager else {return}
        conMan.unregisterWallet(CMQAPIManager.sharedInstance())
    }
    //Start the process of unlocking door.
    public func activateCard(){
        guard let cards = self.walletApplications else {return}
        guard bluetoothEnabled else {CMQAPIManager.showError(message: "Bluetooth Not Active");return}
        guard readyToUse else {return }
        guard let application = cards.first else {return}
        topController!.performSegue(withIdentifier: "Unlock", sender: nil)
        let walletAppID = application.getWalletAppId()
        let qualifier = application.getQualifier()
        guard let manager = connectManager else {return}
        manager.activateCard(walletAppID, qualifier: qualifier)
    }
    //Stop the process of unlocking door.
    public func deactivateCard(){
        guard let cards = self.walletApplications else {return}
        if let application = cards.first{
            guard application.getAppStatus().getStatus() == Deployed else {print("Application not Deployed");return}
            let walletAppID = application.getWalletAppId()
            let qualifier = application.getQualifier()
            guard let manager = connectManager else {return}
            manager.deactivateCard(walletAppID, qualifier: qualifier)
        }
    }
    //Called when the SDK posts notification:  NOTIFICATION_FINAL_SYNCHRONIZE_COMPLETE.
    @objc public func syncComplete(){
        isSyncing = false
        print("Synchronize Complete")
        if self.readyToUse{
            self.hasTried = false
            topController?.dismiss(animated: true, completion: {
                if let controller = self.topController as? CMQAdditionalInfoViewController{
                    controller.kabaView.updateLabel()
                    self.isLoading = false
                }
            })
        }else{
            if retryAttempts <= maxRetryAttempts{
                isRebooking = true
                PFCloud.callFunction(inBackground: "kabaRebook", withParameters: nil, block: rebookCompletion)
            }else{
                topController?.dismiss(animated: true, completion: {
                    if let controller = self.topController as? CMQAdditionalInfoViewController{
                        controller.kabaView.updateLabel()
                        self.isLoading = false
                        self.retryAttempts = 0
                        self.hasTried = true
                        CMQAPIManager.showError(message: "Unable to create digital key. Please try again later.")
                    }
                })
                guard topController == nil else {return}
                CMQAPIManager.showError(message: "Unable to create digital key. Please try again later.")
                self.retryAttempts = 0
                self.isLoading = false
                self.hasTried = true
            }
            
        }
        
        
    }
    //MARK: LegicRegisterWallerDelegate
    public func result(_ successful: Bool, status: LegicStatus!) {
        print("Connection Result:\(successful)\nStatus:\(String(describing: status.getDescription()))")
    }
    public func success(_ alreadyRegistered: Bool) {
        guard let manager = connectManager else {return}
        if manager.isUserRegistered(){
            print("Completed Registration")
            currentState = .RegistrationComplete
            registrationComplete = true
            synchronize()
            isSyncing = true
        }else{
            print("Successfully registered Wallet")
            isRegistering = false
            completeWalletRegistration()
        }
    }
    public func fail(_ status: LegicStatus!) {
        print("Failed to register wallet: \(String(describing: status!.getDescription()))")
        PFCloud.callFunction(inBackground: "reregisterWallet", withParameters: nil) { (returnable, error) in
            if let error = error{
                handle(error: error, message: nil, shouldDisplay: false)
            }else{
                if let array = returnable as? [Any]{
                    guard let newSEID = array[0] as? String, let token = array[1] as? String else {return}
                    print("Retrying with new SEID: \(newSEID)\n NewToken:\(token)")
                    CMQUser.current().kabaPublicSEId = newSEID
                    CMQUser.current().kabaRegToken = token
                    CMQUser.current().saveInBackground()
                    self.registerWallet()
                }
                
            }
        }
    }
    public func token(_ token: String!) {
        print("token received: \(String(describing: token))")
        if token != nil{
            self.activationToken = token
        }
    }
    //MARK: Legic SeUIDelegate
    public func onReceiveMessage(fromReader code: UInt, data message: Data!) {
        print("onReceiveMessageFromReader called")
        if let codeAsInt = E_LegicBlePluginMessageType.init(rawValue: code){
            switch codeAsInt{
            case .PluginMessageTypeIdcMessage:
                guard let bleHandler = BleDataHandler.init(code: Int32(code), message: message) else {print("Unable to create Handler");return}
                if bleHandler.isAccessGranted(){
                    if let controller = self.topController as? CMQUnlockViewController{
                        controller.doorOpen()
                    }
                    print("Access Granted")
                    
                }else{
                    if let controller = self.topController as? CMQUnlockViewController{
                        controller.doorNotOpen()
                    }
                    print("Access Not Granted: \(bleHandler.getAccessError())")
                    
                }
            case .PluginMessageTypeBluetoothState:
                print("Bluetooth State")
                let messageAsUInt = message.to(type: UInt.self)
                guard let state = E_LegicBlePluginBLEState.init(rawValue: messageAsUInt) else {return}
                switch state{
                    
                case .PluginBLEStateUnknown:
                    print("Bluetooth in Unknown State")
                case .PluginBLEStateInitDone:
                    print("Bluetooth Done with Init")
                case .PluginBLEStateDisabled:
                    print("Bluetooth Disabled")
                case .PluginBLEStateEnabled:
                    self.bluetoothEnabled = true
                    print("Bluetooth Enabled")
                case .PluginBLEStateErrorBleNotSupported:
                    print("BLE Not supported")
                case .PluginBLEStateErrorBleNotActivated:
                    self.bluetoothEnabled = false
                    //CMQAPIManager.showError(message: "Bluetooth Not Activated")
                    print("BLE Not activated")
                case .PluginBLEStateErrorGeneral:
                    print("Generic BLE Error")
                }
            //print("\(state)")
            case .PluginMessageTypeProjectState:
                print("Project State: Selecting Digital Key")
                break
            case .PluginMessageTypeIdcPolling:
                print("Polling: Waiting for command From UI")
                break
            case .PluginMessageTypeIdcFileWasRead:
                print("Sending unlock call")
                let uSignedCharArray = [CUnsignedChar].init([0,1,1])
                let data = Data.init(bytes: uSignedCharArray)
                guard let manager = connectManager else {return}
                manager.sendMessage(toBLEReader: 0, message: data)
                break;
            }
        }
    }
    //MARK:Navigation
    private func prepareToLoad()->CMQLoadingViewController{
        let controller = UIStoryboard.init(name: "main", bundle: CMQBundle!).instantiateViewController(withIdentifier: "Loading") as! CMQLoadingViewController
        controller.promptText = "Regenerating digital key. This may take a few minutes..."
        controller.transitioningDelegate = self.transitionDelegate!
        controller.modalPresentationStyle = .custom
        return controller
        
    }
    private func prepateTransitionDelegate(frame:UIView){
        let frame = promptSize(forView: frame)
        self.transitionDelegate = CMQTransitionDelegate(size: frame.size, origin: frame.origin)
    }
    public func showLoading(){
        guard topController != nil else {return}
        prepateTransitionDelegate(frame: topController!.view)
        let loadingController = prepareToLoad()
        topController?.present(loadingController, animated: true, completion: {
            self.isLoading = true
        })
        
    }
    
    @objc public func restoreSession(){
        if isRegistering{
            self.registerWallet()
        }else if !registrationComplete{
            self.completeWalletRegistration()
        }else if isSyncing{
            self.synchronize()
        }
    }
    
}
extension Data {
    struct HexEncodingOptions: OptionSet {
        let rawValue: Int
        static let upperCase = HexEncodingOptions(rawValue: 1 << 0)
    }
    
    func hexEncodedString(options: HexEncodingOptions = []) -> String {
        let hexDigits = Array((options.contains(.upperCase) ? "0123456789ABCDEF" : "0123456789abcdef").utf16)
        var chars: [unichar] = []
        chars.reserveCapacity(2 * count)
        for byte in self {
            chars.append(hexDigits[Int(byte / 16)])
            chars.append(hexDigits[Int(byte % 16)])
        }
        return String(utf16CodeUnits: chars, count: chars.count)
    }
    func to<T>(type: T.Type) -> T {
        return self.withUnsafeBytes { $0.pointee }
    }
}

