//
//  CMQAdditionalInfoTableViewController.swift
//  Communique
//
//  Created by Andre White on 4/4/18.
//  Copyright © 2018 Communique, LLC. All rights reserved.
//

import UIKit
import MessageUI
public class CMQAdditionalInfoTableViewController: UITableViewController, MFMailComposeViewControllerDelegate {
    
    public var isForAccessCodes:Bool = false
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.tableFooterView = UIView.init()
    }

    override public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isForAccessCodes{
            guard let codes = CMQAPIManager.sharedInstance().apartment.aptAccessCodes else {return 0 }
            return codes.count
        }else{
            guard let contacts = CMQAPIManager.sharedInstance().apartment.additionalContacts else {return 0}
            return contacts.count
        }
    }
    public override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuse", for: indexPath)
        var data = isForAccessCodes ? CMQAPIManager.sharedInstance().apartment.aptAccessCodes! : CMQAPIManager.sharedInstance().apartment.additionalContacts!
        let curItem = data[indexPath.row] as! [String]
        cell.textLabel?.text = isForAccessCodes ? curItem[0] : curItem[1]
        cell.detailTextLabel?.text = isForAccessCodes ? curItem[1] : curItem[2]
        cell.selectionStyle = .none
        return cell
    }
    public override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard !isForAccessCodes else {return}
        var data = CMQAPIManager.sharedInstance().apartment.additionalContacts!
        let curItem = data[indexPath.row] as! [String]
        let contact = curItem[2]
        if contact.isValidEmail{
            self.presentMailController(email: contact)
        }else if contact.isValidPhone{
            let phoneNumber: String = "telprompt://".appending(contact)
            UIApplication.shared.open(NSURL(string:phoneNumber)! as URL, options: [UIApplication.OpenExternalURLOptionsKey : Any](), completionHandler: nil)
        }
    }
    fileprivate var mailComposerVC : MFMailComposeViewController!{
        let controller = MFMailComposeViewController()
        controller.mailComposeDelegate = self
        controller.setMessageBody(String.init(format: "\n\n\n\n\n\nSent from the %@ App %@\nA Communiqué Product", Bundle.main.localizedInfoDictionary!["CFBundleDisplayName"] as! CVarArg,UIApplication.versionAndBuild()), isHTML: false)
        return controller
    }
    
    public func presentMailController(email:String){
        guard let mailVC = mailComposerVC else {CMQAPIManager.showError(message: "Email not found"); return }
        mailVC.setToRecipients([email])
        self.present(mailVC, animated: true, completion: nil)
    }
    public func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        self.dismiss(animated: true, completion: nil)
    }
}
