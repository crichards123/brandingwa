//
//  CMQDormaKabaView.swift
//  Communique
//
//  Created by Andre White on 4/18/18.
//  Copyright © 2018 Communique, LLC. All rights reserved.
//

import UIKit

public class CMQDormaKabaView: UIView {
    @IBOutlet public var promptLabel:UILabel!
    
    public func updateLabel(){
        guard let _ = CMQDormaKabaManager.sharedInstance().connectManager else {promptLabel.text = "";return}
        let text:String = CMQDormaKabaManager.sharedInstance().readyToUse ? CMQDisplayDormaKabaUnlockString : CMQDisplayDormaKabaSetUpString
        promptLabel.text = text
    }
}
