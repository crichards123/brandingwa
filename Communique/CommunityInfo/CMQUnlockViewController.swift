//
//  CMQUnlockViewController.swift
//  Communique
//
//  Created by Andre White on 4/18/18.
//  Copyright © 2018 Communique, LLC. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
public class CMQUnlockViewController: UIViewController {
    private var timer:Timer?
    @IBOutlet weak var promptLabel: UILabel!
    @IBOutlet public var timeLabel:UILabel!
    @IBOutlet weak var activityView: NVActivityIndicatorView!
    var unlockedColor:UIColor!
    var lockedColor:UIColor!
    public var startTime = 10
    
    //MARK: - Life cycle
    public override func viewDidLoad() {
        super.viewDidLoad()
        activityView.startAnimating()
        updateLabel()
        CMQDormaKabaManager.sharedInstance().topController = self
        if #available(iOS 10.0, *) {
            self.timer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true, block: { (timer) in
                self.changeTimerLabel()
            })
        } else {
            // Fallback on earlier versions
        }
    }
    public override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.timer?.invalidate()
        CMQDormaKabaManager.sharedInstance().deactivateCard()
    }
    
    
    //Called when the door is open successfully
    public func doorOpen(){
        self.timer?.invalidate()
        handleLabel(isUnlocked: true)
        DispatchQueue.main.asyncAfter(deadline: .now()+1.5) {
            self.dismissView()
        }
    }
    //Called when the door is not opened succesfully
    public func doorNotOpen(){
        self.timer?.invalidate()
        handleLabel(isUnlocked: false)
        DispatchQueue.main.asyncAfter(deadline: .now()+1.5) {
            self.dismissView()
        }
    }
    //MARK: - Navigation
    //Deactivates card and dismisses view
    public func dismissView(){
        self.presentingViewController?.dismiss(animated: true, completion: {
            CMQDormaKabaManager.sharedInstance().deactivateCard()
            if let controller = self.presentingViewController as? CMQAdditionalInfoViewController{
                controller.kabaView.updateLabel()
            }
        })
    }
    //MARK: - UI
    public func handleLabel(isUnlocked:Bool){
        self.promptLabel?.text = isUnlocked ? CMQDisplayDormaKabaDoorUnlockedString:CMQDisplayDormaKabaDoorLockedString
        self.promptLabel?.textColor = isUnlocked ? unlockedColor:lockedColor
    }
    public func updateLabel(){
        self.timeLabel.text = String(startTime)
    }
    public func changeTimerLabel(){
        startTime -= 1
        if startTime != 0{
            updateLabel()
        }else{
            self.timer?.invalidate()
            self.timer = nil
            self.dismissView()
        }
    }
}
