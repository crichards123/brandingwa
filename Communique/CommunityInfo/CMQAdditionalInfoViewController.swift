//
//  CMQCustomLinksViewController.swift
//  Communique
//
//  Created by Andre White on 4/18/18.
//  Copyright © 2018 Communique, LLC. All rights reserved.
//

import UIKit

public class CMQAdditionalInfoViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var kabaView: CMQDormaKabaView!
    private var transitionHandler: CMQTransitionDelegate?
    public var isForAccessCodes:Bool = false
    //MARK: - Life cycle
    public override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.tableFooterView = UIView()
        self.kabaView.updateLabel()
    }
    //MARK: - TableView DataSource/ Delegate
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isForAccessCodes{
            guard let codes = CMQAPIManager.sharedInstance().apartment.aptAccessCodes else {return 0 }
            return codes.count
        }else{
            guard let contacts = CMQAPIManager.sharedInstance().apartment.additionalContacts else {return 0}
            return contacts.count
        }
    }
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuse", for: indexPath)
        var data = isForAccessCodes ? CMQAPIManager.sharedInstance().apartment.aptAccessCodes! : CMQAPIManager.sharedInstance().apartment.additionalContacts!
        let curItem = data[indexPath.row] as! [String]
        cell.textLabel?.text = isForAccessCodes ? curItem[0] : curItem[1]
        cell.detailTextLabel?.text = isForAccessCodes ? curItem[1] : curItem[2]
        cell.selectionStyle = .none
        return cell
    }
    //MARK: - IBActions
    //Called when the DormakabaView is tapped.
    @IBAction func didTapView(_ sender: Any) {
        guard let _ = CMQDormaKabaManager.sharedInstance().connectManager else {return}
        CMQDormaKabaManager.sharedInstance().topController = self
        if CMQDormaKabaManager.sharedInstance().readyToUse{
            CMQDormaKabaManager.sharedInstance().topController = self
            CMQDormaKabaManager.sharedInstance().activateCard()
        }else{
            self.performSegue(withIdentifier: "Prompt", sender: nil)
        }
        
    }
    //MARK: - Exit Segues
    //Called after user accepts key Generation
    @IBAction func didAcceptKeyGeneration(sender:UIStoryboardSegue){
        DispatchQueue.main.asyncAfter(deadline: .now()+0.75) {
            guard let _ = CMQDormaKabaManager.sharedInstance().connectManager else {return}
            CMQDormaKabaManager.sharedInstance().topController = self
            self.performSegue(withIdentifier: "Generate", sender: nil)
            CMQDormaKabaManager.sharedInstance().registerWallet()
            
        }
    }
    //Called after user declines key Generation
    @IBAction func didDeclineKeyGeneration(sender:UIStoryboardSegue){
        
    }
    @IBAction public func dismissPrompt(segue:UIStoryboardSegue){
        
    }
    //MARK: - Navigation
    
    public func prepareTransition(segueIdentifier:String){
        switch segueIdentifier {
        case "Prompt":
            let frame = promptSize(forView: self.view)
            self.transitionHandler = CMQTransitionDelegate(size: frame.size, origin: frame.origin)
        case "Unlock":
            let height = self.view.frame.size.height*0.50
            let startY = (self.view.frame.size.height*0.25)
            let width = UIDevice.current.userInterfaceIdiom == .pad ? self.view.frame.width * 0.40 : self.view.frame.width*0.75
            self.transitionHandler = CMQTransitionDelegate(size: CGSize.init(width: width, height: height), origin: CGPoint.init(x: self.view.center.x - (width/2), y: startY))
        case "Generate":
            let height = CGFloat(331.0)//self.view.frame.size.height*0.55
            let width = UIDevice.current.userInterfaceIdiom == .pad ? self.view.frame.width * 0.40 : CGFloat(281)//self.view.frame.width*0.75
            let startY = (self.view.frame.size.height*0.25)//self.view.center.y - (height/2)
            let startX = self.view.center.x - (width/2)
            let size = CGSize.init(width: width, height: height)
            let point = CGPoint.init(x: startX, y: startY)
            self.transitionHandler = CMQTransitionDelegate(size: size, origin: point)
        default:
            print("Unknown Identifier")
        }
    
    }
    public override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
            let dest = segue.destination
            self.prepareTransition(segueIdentifier: segue.identifier!)
            dest.transitioningDelegate = transitionHandler
            dest.modalPresentationStyle = .custom
        if segue.identifier == "Prompt"{
            guard let label = dest.view.viewWithTag(300) as? UILabel else {return}
            label.text = CMQDormaKabaManager.sharedInstance().wasInterrupted ? CMQDisplayDormaKabaWillGenKeyInterruptedPromptString : CMQDisplayDormaKabaWillGenKeyPromptString
        }
    }

}
