//
//  CMQKabaGeneratingKeyViewController.swift
//  Communique
//
//  Created by Andre White on 4/20/18.
//  Copyright © 2018 Communique, LLC. All rights reserved.
//

import UIKit
import Lottie

public class CMQKabaGeneratingKeyViewController:CMQLoadingViewController {
    @IBOutlet weak var loadingView: UIView!
    override public func viewDidLoad() {
        promptText = CMQDisplayDormaKabaGenKeyPromptString
        super.viewDidLoad()
    }
    //Override view to add in Lottie animation 
    public override func initView(){
        let loadingAnimation = LOTAnimationView.init(filePath: GeneratingKeyAnimationPath!)
        loadingAnimation.frame = CGRect.init(origin: .zero, size: loadingView.frame.size)
        loadingAnimation.contentMode = .scaleToFill
        loadingView.addSubview(loadingAnimation)
        loadingAnimation.play()
        loadingAnimation.loopAnimation = true
        promptLabel.text = promptText
    }
}
