//
//  CMQNavigationController.swift
//  Communique
//
//  Created by Andre White on 2/8/18.
//  Copyright © 2018 Communique, LLC. All rights reserved.
//

import UIKit

public class CMQNavigationController: UINavigationController {
    public var navBarHidden = false
    @IBOutlet var composeButton: UIButton!
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        initButton()
        let _ = FeedbackManager.shared
        self.navigationBar.barTintColor = .DarkColor
    }
    override public func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.toolbar.isHidden = true
        
    }
    private func initButton(){
        if let button = self.composeButton{
            let padding:CGFloat = 10.0
            let buttonWidth  = min(self.view.frame.width*(2/8), 75)
            let size = CGSize.init(width: buttonWidth, height: buttonWidth)
            let pos = CGPoint.init(x: self.view.frame.width - padding - buttonWidth , y: self.view.frame.height - (padding*2) - buttonWidth)
            button.frame = CGRect.init(origin: pos, size: size)
            button.layer.cornerRadius = buttonWidth / 2
            button.addTarget(self, action: #selector(buttonAction(sender:)), for: .touchUpInside)
        }
    }
    
    public func addButtonToView(){
        guard CMQUser.current().isAdmin else {return}
        self.view.addSubview(composeButton)
    }
    
    public func removeButton(){
        self.composeButton.removeFromSuperview()
    }
    /*
    override public func pushViewController(_ viewController: UIViewController, animated: Bool) {
        if let _ = viewController as? CMQHomeFeedViewController {
            if CMQAPIManager.sharedInstance().apartment != nil{
                self.hideNavbar()
            }
            
        }else{
            if let _ = self.topViewController as? CMQHomeFeedViewController{
                if navBarHidden{
                    restoreNavBar()
                }
            }
        }
        super.pushViewController(viewController, animated: animated)
    }
    
    override public func popViewController(animated: Bool) -> UIViewController? {
        let vc = super.popViewController(animated: animated)
        if !self.navigationBar.isTranslucent && self.topViewController is CMQHomeFeedViewController {
            self.navigationBar.isTranslucent = true
        }
        if let viewCon = self.topViewController as? CMQHomeFeedViewController{
            self.navigationBar.barTintColor = .CMQDarkColor()
            if viewCon.shouldHideNavBar{
                if CMQAPIManager.sharedInstance().apartments != nil{
                    hideNavbar()
                }
            }else{
                restoreNavBar()
            }
        }
        
        return vc
    }
    public func hideNavbar(){
        /*if !navBarHidden{
            navBarHidden = true
            self.navigationBar.setBackgroundImage(UIImage(), for: .default)
            self.navigationBar.shadowImage = UIImage()
            //self.navigationBar.isTranslucent = true
            self.topViewController?.navigationItem.leftBarButtonItem?.tintColor = .gray
            self.topViewController?.navigationItem.rightBarButtonItem?.tintColor = .gray
            UIApplication.shared.statusBarStyle = .default
            self.topViewController?.title = nil
        }*/
        
        
    }
    public func restoreNavBar(){
       /* if navBarHidden{
            self.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
            self.navigationBar.setBackgroundImage(nil, for: .default)
            //self.navigationBar.isTranslucent = false
            self.navigationBar.shadowImage = nil
            self.topViewController?.navigationItem.leftBarButtonItem?.tintColor = .white
            self.topViewController?.navigationItem.rightBarButtonItem?.tintColor = .white
            self.navigationBar.barTintColor = .CMQDarkColor()
            UIApplication.shared.statusBarStyle = .lightContent
            if let topVC = self.topViewController as? CMQHomeFeedViewController{
                topVC.title = "Home"
                //topVC.tableView.contentInset = UIEdgeInsetsMake(44, 0, 0, 0)
            }
            navBarHidden = false
        }*/
        
        
    }*/
    @objc public func buttonAction(sender:CMQButton){
        if let front = self.visibleViewController as? CMQHomeFeedViewController{
            front.performSegue(withIdentifier: "ComposeMenu", sender: nil)
        }else if let front = self.visibleViewController as? CMQFeedItemTableViewController{
            front.performSegue(withIdentifier: "Compose", sender: nil)
        }
    }

}
