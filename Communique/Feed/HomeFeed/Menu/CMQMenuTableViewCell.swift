//
//  CMQMenuTableViewCell.swift
//  Communique
//
//  Created by Andre White on 1/25/18.
//  Copyright © 2018 Communique, LLC. All rights reserved.
//

import UIKit

class CMQMenuTableViewCell: UITableViewCell {
    @IBOutlet weak var menuIcon:UIImageView!
    @IBOutlet weak var menuLabel:UILabel!
}
