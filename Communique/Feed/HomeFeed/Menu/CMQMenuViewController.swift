//
//  CMQMenuViewController.swift
//  Communique
//
//  Created by Andre White on 1/19/18.
//  Copyright © 2018 Communique, LLC. All rights reserved.
//

import UIKit
import ParseUI
public class CMQMenuViewController: UITableViewController {
    @IBOutlet var headerView: CMQProfileHeaderView!
    private var currentStatusStyle:UIStatusBarStyle!
    private var homeViewController:CMQHomeFeedViewController!
    @IBOutlet weak var apartmentImageView: PFImageView!{
        didSet{
            guard let view = apartmentImageView else {return}
            view.file = CMQUser.current().userPhoto
            view.loadInBackground()
        }
    }
    @IBOutlet weak var communityNameLabel: UILabel!{
        didSet{
            guard let label = communityNameLabel else {return}
            label.text = CMQUser.current().fullName
        }
    }
    public var supportCloutFunctionCompltion:PFIdResultBlock{
        return { (dict:Any?, error:Error?) in
            if let error = error{
                handle(error: error, message: "Unable to Load Epproach Support", shouldDisplay: true)
            }else if let dict = dict as? [String:Any]{
                let url = dict["sso_url"] as! String
                self.homeViewController.performSegue(withIdentifier: CMQHomeSegue.Support.rawValue, sender: URL.init(string: url))
            }
        }
    }
    public var smartHomeCloudFunctionCompletion:PFIdResultBlock{
        return { (urlString:Any?, error:Error?) in
            if let error = error{
                handle(error: error, message: "Unable to Load Smart Home", shouldDisplay: true)
            }else if let string = urlString as? String, let url = URL.init(string: string){
                self.homeViewController.performSegue(withIdentifier: CMQHomeSegue.LegacySmartHome.rawValue, sender: url)
            }
        }
    }
    public var payRentCloudFunctionCompletion:PFIdResultBlock{
        return { (dict:Any?, error:Error?) in
            if let error = error{
                handle(error: error, message: "Unable to Load Rent Payment", shouldDisplay: true)
            }else if let dict = dict as? [String:Any]{
                let url = dict["sso_url"] as! String
                self.homeViewController.performSegue(withIdentifier: CMQHomeSegue.PayRent.rawValue, sender: URL.init(string: url))
            }
        }
    }
    public var workOrderCloudFunctionCompletion:PFIdResultBlock{
        return { (dict:Any?, error:Error?) in
            if let error = error{
                handle(error: error, message: "Unable to Load Work Order", shouldDisplay: true)
            }else if let dict = dict as? [String:Any]{
                let url = dict["sso_url"] as! String
                self.homeViewController.performSegue(withIdentifier: CMQHomeSegue.WorkOrder.rawValue, sender: URL.init(string: url))
            }
        }
    }
    public var payLeaseCloudFunctionCompletion:PFIdResultBlock{
        return { (dict:Any?, error:Error?) in
            if let error = error{
                handle(error: error, message: "Unable to Load Pay Lease", shouldDisplay: true)
            }else{
                if let dict = dict as? [String:Any]{
                    let url = dict["sso_url"] as! String
                    self.homeViewController.performSegue(withIdentifier: CMQHomeSegue.Pay.rawValue, sender: URL.init(string: url))
                }else{
                    CMQAPIManager.showError(message: "Unable to Load. Please try again Later")
                }
            }
        }
    }
    //MARK: - Life Cycle
    override public func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.tableFooterView = UIView.init()
        let nav = self.presentingViewController as! UINavigationController
        
        self.homeViewController = nav.topViewController as? CMQHomeFeedViewController
    }
    override public var preferredStatusBarStyle: UIStatusBarStyle{
        return .lightContent
    }
    override public func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        currentStatusStyle = UIApplication.shared.statusBarStyle
        //TODO:Update StatusBar
        UIApplication.shared.statusBarStyle = .lightContent
    }
    override public func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        //TODO:Update StatusBar
        UIApplication.shared.statusBarStyle = currentStatusStyle
    }
    
    
    //MARK: - TableView Delegate/DataSource
    override public func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.y < 0 {
            scrollView.contentOffset.y = 0
        }
    }
    public override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //guard let items = menuItems else {return 0}
        return HomeMenuItem.all.count
    }
    public override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55//tableView.frame.height/CGFloat(items.count)
    }
    override public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let header = headerView, section == 0 else {return nil}
        header.user = CMQUser.current()
        header.backgroundColor = .DarkColor
        return header
    }
    public override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat{
        guard section == 0 else {return 10}
        return self.view.frame.height*0.30
    }
    override public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = HomeMenuItem.all[indexPath.row]
        item.prepareForSegue { (should, sender) in
            if should{
                var completion : ()->Void
                switch item{
                case .LogOut:
                    completion = {
                        CMQAPIManager.sharedInstance().logOut(homeController: self.homeViewController)
                    }
                default:
                    completion = {
                        self.homeViewController.performSegue(withIdentifier: item.segueID, sender: sender)
                    }
                }
                self.homeViewController.dismiss(animated: true, completion: completion)
            }else{
                FeedbackManager.shared.showError(message: "Unable to load \(item.title)")
            }
        }
    }
    public override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "menuCell", for: indexPath) as? CMQMenuTableViewCell else {return UITableViewCell()}
        let item = HomeMenuItem.all[indexPath.row]
        cell.menuIcon.image = item.image
        cell.menuLabel.text = item.title
        return cell
    }
}
///Object that can be represented using a cell with title and image properties.
protocol CellRepresentable{
    var title:String {get}
    var image:UIImage? {get}
}
///Object that can be shown (and hidden)
protocol Showable{
    var shouldShow:Bool {get}
}
///Object that can prepare for a segue.
protocol SeguePreparable {
    var segueID:String {get}
    func prepareForSegue(completion:@escaping(_ shouldSegue:Bool, _ sender:Any?)->Void)
}
///Protocol representing an object that has an orderPriority variable that can be used to order
protocol Orderable {
    var orderPriority:Int {get}
}
///protocol representing an object that has a UIColor
protocol Colorable {
    var color:UIColor {get}
}
enum HomeMenuItem{
    static var all:[HomeMenuItem]{
        var returnable:[HomeMenuItem] = [.Message, .News, .Events, .Packages, .PayLease, .PayRent,
                                        .WorkOrder, .SmartHome, .Account, .Support, .Info, .LogOut]
        if let links = CMQAPIManager.sharedInstance().customLinks{
            for link in links{
                returnable.append(.Custom(title: link["title"]!, link: link["url"]!))
            }
        }
        if let apartment = CMQAPIManager.sharedInstance().apartment{
            if let tab = apartment.firstTabTitle{
                if let link = apartment.aptRepairURL{
                    returnable.append(.Custom(title: tab, link: link))
                }
            }
            if let tab = apartment.secondTabTitle{
                if let link = apartment.aptPaymentURL{
                    returnable.append(.Custom(title: tab, link: link))
                }
            }
        }
        return returnable.filter({$0.shouldShow}).sorted(by: {$0.orderPriority < $1.orderPriority})
    }
    case Message
    case News
    case Events
    case Packages
    case PayLease
    case PayRent
    case WorkOrder
    case SmartHome
    case Account
    case Support
    case Info
    case LogOut
    case Custom(title:String, link:String)
    
    var imageName:String{
        switch self {
        case .Message:
            return "MessageButton"
        case .News:
            return "NewsButton"
        case .Events:
            return "EventsButton"
        case .Packages:
            return "PD-icon"
        case .PayLease:
            return "home-payleasebutton"
        case .PayRent:
            return "rent"
        case .WorkOrder:
            return "WorkOrderIcon"
        case .SmartHome:
            return "home-smarthome-button"
        case .Account:
            return "AccountButton"
        case .Support:
            return "Support"
        case .Info:
            return "InfoButton"
        case .Custom:
            return "StarButton"
        case .LogOut:
            return "LogoutButton"
        }
    }
}

extension HomeMenuItem:CellRepresentable{
    var title: String {
        switch self{
            
        case .Message:
            return "Messages"
        case .News:
            return "News"
        case .Events:
            return "Events"
        case .Packages:
            return "Packages"
        case .PayLease:
            return "Pay Lease"
        case .PayRent:
            return "Pay Rent"
        case .WorkOrder:
            return "Work Order"
        case .SmartHome:
            return "Smart Home"
        case .Account:
            return "Account"
        case .Support:
            return "Epproach Support"
        case .Info:
            return "Community Info"
        case .Custom(let title, _):
            return title
        case .LogOut:
            return "Log Out"
        }
    }
    
    var image: UIImage? {
        return UIImage.init(named: self.imageName, in: Bundle.init(for: CMQPackage.self), compatibleWith: nil)
    }
}
extension HomeMenuItem:Showable{
    var shouldShow: Bool {
        switch self{
            
        case .Message,.News,.Events,.Info:
            return CMQAPIManager.sharedInstance().apartment != nil
        case .Packages:
            return CMQAPIManager.sharedInstance().shouldShowPackages
        case .PayLease:
            return CMQAPIManager.sharedInstance().shouldShowPayLease
        case .PayRent:
            return CMQAPIManager.sharedInstance().shouldShowPayRentNWorkOrder
        case .WorkOrder:
            return CMQAPIManager.sharedInstance().shouldShowPayRentNWorkOrder
        case .SmartHome:
            return CMQAPIManager.sharedInstance().shouldShowSmartHome
        case .Support:
            return CMQAPIManager.sharedInstance().shouldShowSupport
        case .Account,.Custom,.LogOut:
            return true
        }
    }
}
extension HomeMenuItem:Orderable{
    var orderPriority: Int {
        switch self{
            
        case .Message:
            return 0
        case .News:
            return 1
        case .Events:
            return 2
        case .Packages:
            return 3
        case .PayLease:
            return 4
        case .PayRent:
            return 5
        case .WorkOrder:
            return 6
        case .SmartHome:
            return 7
        case .Account:
            return 8
        case .Support:
            return 9
        case .Info:
            return 10
        case .LogOut:
            return 500
        case .Custom(_,_):
            return 9
        }
    }
}
extension HomeMenuItem:Colorable{
    var color: UIColor {
        switch self{
        case .Message:
            return .MessageColor
        case .Events:
            return .EventColor
        case .News:
            return .NewsColor
        default:
            return .DarkColor
        }
    }
}
extension HomeMenuItem:SeguePreparable{
    var cloudFunctionName:String?{
        switch self{
            
        case .PayLease:
            return "performPayLeaseSSO"
        case .PayRent:
            return "welcomeHomePayment"
        case .WorkOrder:
            return "welcomeHomeWorkorder"
        case .SmartHome:
            return self.segueID == "LegacySmartHome" ? "performSmartHomeSSO" : "managerSmartSSO"
        case .Support:
            return "supportPlaceholder"
        default:
            return nil
        }
    }
    var segueID: String {
        switch self{
            
        case .Message,.News,.Events:
            return "ViewFeed"
         case .Packages:
            return "Packages"
        case .PayLease:
            return "PayLease"
        case .PayRent:
            return "PayRent"
        case .WorkOrder:
            return "WorkOrder"
        case .SmartHome:
            return CMQAPIManager.sharedInstance().apartment.legacySmartHome ? "LegacySmartHome": CMQUser.current()?.isAdmin ?? false ? "Custom":"SmartHome"
        case .Account:
             return "Account"
        case .Support:
            return "Support"
        case .Info:
            return "Community Info"
        case .LogOut:
            return "LogOut"
        case .Custom:
            return "Custom"
        }
    }
    
    func prepareForSegue(completion: @escaping (Bool, Any?) -> Void) {
        switch self{
            
        case .Message:
            completion(true,CMQMessageFeedContainer())
        case .News:
            completion(true,CMQNewsFeedContainer())
        case .Events:
            completion(true,CMQEventsFeedContainer())
        case .PayLease,.PayRent,.WorkOrder,.Support:
            guard let name = self.cloudFunctionName else {completion(false,nil);return}
            PFCloud.callFunction(inBackground: name, withParameters: nil) { (dict, error) in
                if let error = error{
                    handle(error: error, message: "Unable to load \(self.title)", shouldDisplay: true)
                    completion(false, nil)
                }else{
                    if let dict = dict as? [String:Any], let url = dict["sso_url"] as? String{
                        let url = URL.init(string: url)
                        completion(url != nil, url)
                    }
                }
            }
        case .SmartHome:
            if segueID == "LegacySmartHome"{
                PFCloud.callFunction(inBackground: self.cloudFunctionName!, withParameters: nil, block: { (urlString, error) in
                    if let error = error{
                        handle(error: error, message: "Unable to Load Smart Home", shouldDisplay: true)
                    }else if let string = urlString as? String, let url = URL.init(string: string){
                        completion(true,url)
                    }
                })
            }else{
                if CMQUser.current()?.isAdmin ?? false{
                    guard let name = self.cloudFunctionName else {return}
                    PFCloud.callFunction(inBackground: name, withParameters: ["aptComplexID":CMQUser.current().complexID]) { (urlString, error) in
                        if let error = error{
                            handle(error: error, message: "Unable to Load Smart Home", shouldDisplay: true)
                        }else if let string = urlString as? String, let url = URL.init(string: string){
                            completion(true,url)
                        }
                    }
                }else{
                    guard let _ = CMQAPIManager.sharedInstance().smartAptUnit else {completion(false,nil);return}
                    completion(true,self.title)
                }
            }
        case .Custom(let title, let link):
            if let url = URL.init(string: link){
                var dict = [String:Any]()
                dict["title"] = title
                dict["url"] = url
                completion (true,dict)
            }else{
                completion(false,nil)
            }
        default:
            completion(true,nil)
        }
        
    }
}
