//
//  CMQComposeMenuViewController.swift
//  Communique
//
//  Created by Andre White on 2/27/18.
//  Copyright © 2018 Communique, LLC. All rights reserved.
//

import UIKit
/* Compose Menu that is displayed after user presses Compose button.
    3 buttons that have the Tags 0 1 2 for messages events and news respectively
    1 button that Unwinds to the Home VC for "Cancel"
    Close Button also has tag 0 to quickly remove other buttons if in Multi Manager
 */
public class CMQComposeMenuViewController: UIViewController {
    @IBOutlet var stacks:[UIStackView]!
    @IBOutlet var buttons: [UIButton]!
    @IBOutlet weak var messagesLabel: UILabel!{
        didSet{
            guard let label = messagesLabel else {return}
            label.text = CMQAPIManager.sharedInstance().apartment == nil ? "Announcement":"Messages"
        }
    }
    /*
        Remove Event and News Buttons if in Multi Manager
     */
    override public func viewDidLoad() {
        self.view.layer.cornerRadius = 5
        if CMQAPIManager.sharedInstance().apartment == nil{
            for stack in stacks{
                if stack.tag != 0{
                    stack.removeFromSuperview()
                }
            }
        }
    }
    /*
        Make Home VC Segue To Compose Menu with the Tag in order to create Composable
    */
    @IBAction func buttonPressed(_ sender: UIButton) {
        if let navController = self.presentingViewController as? UINavigationController, let home = navController.topViewController{
            home.dismiss(animated: true, completion: {
                home.performSegue(withIdentifier: "Compose", sender: sender.tag)
            })
            
        }
    }
    
}
