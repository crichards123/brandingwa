//
//  CMQHomeFeedHeader.swift
//  Communique
//
//  Created by Andre White on 3/14/18.
//  Copyright © 2018 Communique, LLC. All rights reserved.
//

import UIKit
import ParseUI
public class CMQHomeFeedHeader: UIView {
    
    public class func instanceFromNib()->CMQHomeFeedHeader?{
        guard let nib = loadNib(name: CMQHomeFeedHeaderNibName) else {return nil}
        return nib.instantiate(withOwner: nil, options: nil).first as? CMQHomeFeedHeader
    }
    
    @IBOutlet weak var aptImageView: PFImageView!{
        didSet{
            guard let view = aptImageView, let apartment = CMQAPIManager.sharedInstance().apartment, let file = apartment.aptPhoto else{return}
            view.file = file
            view.loadInBackground()
        }
    }
    /*
    public func animateToOrginalHeight(top:CGFloat){
        guard let constraint = self.constraints.filter({$0.identifier == "Height"}).first else {return}
        constraint.constant = 0
        UIView.animate(withDuration: 0.4, delay: 0.0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.5, options: .curveEaseInOut, animations: {
            self.layoutIfNeeded()
        }, completion: nil)
    }*/
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
}
