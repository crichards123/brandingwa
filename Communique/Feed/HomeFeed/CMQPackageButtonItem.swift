//
//  CMQPackageButtonItem.swift
//  Communique
//
//  Created by Andre White on 4/20/18.
//  Copyright © 2018 Communique, LLC. All rights reserved.
//

import UIKit
//Delivery Notification Button
public class CMQPackageButtonItem: UIBarButtonItem {
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        let button = UIButton.init(type: .custom)
        button.frame = CGRect.init(x: 0, y: 0, width: 44, height: 44)
        button.imageView?.contentMode = .scaleToFill
        button.setImage(UIImage.init(named: "PD-Navicon", in: CMQBundle, compatibleWith: nil), for: .normal)
        button.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
        self.customView = button
    }
    @objc public func buttonPressed(){
        UIApplication.shared.sendAction(self.action!, to: self.target!, from: nil, for: nil)
    }
}
