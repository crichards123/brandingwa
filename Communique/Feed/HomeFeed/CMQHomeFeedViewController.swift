//
//  CMQHomeFeedViewController.swift
//  Communique
//
//  Created by Andre White on 1/19/18.
//  Copyright © 2018 Communique, LLC. All rights reserved.
//

import UIKit
import ParseUI
extension CMQHomeFeedViewController{
    
}
public class CMQHomeFeedViewController: IDTableViewController {
    private let flexSpace = UIBarButtonItem.init(barButtonSystemItem: .fixedSpace, target: nil, action: nil)
    @IBOutlet var sTwoButton:UIBarButtonItem!
    @IBOutlet var kabaButton: UIBarButtonItem!
    @IBOutlet var packageNavButton: UIBarButtonItem!
    @IBOutlet weak var headerTopConstraint: NSLayoutConstraint!
    @IBOutlet var headerView: CMQHomeFeedHeader!
    private var respondingContentView:Any?
    private var transitionHandler: CMQTransitionDelegate?
    private var navBarHidden:Bool{
        guard let navController = self.navigationController as? CMQNavigationController else {return false}
        return navController.navBarHidden
    }
    //override public var dataSource: CMQFeedItems! = CMQHomeFeedItems()
    public var isAll:Bool{
        return CMQAPIManager.sharedInstance().apartment == nil
    }
    
    public var pendingPackageMessage:CMQMessage?{
        didSet{
            handlePackageButton()
        }
    }
    public var shouldHideNavBar:Bool{
        if isAll{
            return false
        }
        guard let headerView = self.headerView?.aptImageView else {return true}
        if self.tableView.contentOffset.y > 0{
            return self.tableView.contentOffset.y <= headerView.frame.height*0.75 && !navBarHidden
        }
        return true
    }
    
    public var packageMessageCompletion:PFObjectResultBlock{
        return { (object:PFObject?, error:Error?) -> Void in
            if let error = error{
                self.pendingPackageMessage = nil
                handle(error: error, message: "Unable to get Package Message", shouldDisplay: false)
            }else{
                if let message = object as? CMQMessage{
                    self.pendingPackageMessage = message
                }
            }
        }
    }
    
    //MARK: - IBActions
    @IBAction func didPressDelivery(_ sender: Any) {
    }
    @IBAction public func kabaButtonPressed(){
        CMQDormaKabaManager.sharedInstance().topController = self
        CMQDormaKabaManager.sharedInstance().activateCard()
    }
    @IBAction public func sTwoButtonPressed(){
        self.performSegue(withIdentifier: "STwo", sender: nil)
    }
    
    
    //MARK: - Exit Segues
    @IBAction func cancelMessage(sender:UIStoryboardSegue){

    }
    @IBAction func postObject(sender:UIStoryboardSegue){
        
    }
    @IBAction func composeButtonPressed(sender:UIStoryboardSegue){
        
    }
    @IBAction func apartmentSelection(sender:UIStoryboardSegue){
        self.willChangeSource(to: CMQHomeFeedContainer())
        
    }
    @IBAction func packageConfirmation(sender:UIStoryboardSegue){
        
    }
    
    // MARK: - Navigation Bar customization
    
    override public func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView != self.tableView{
            if scrollView.contentOffset.y < 0 {
                scrollView.contentOffset.y = 0
            }
        }
        guard scrollView == self.tableView else {return}
        if scrollView.contentOffset.y < 0{
            headerTopConstraint.constant = scrollView.contentOffset.y
        }
    }
    
    @objc public func handleKabaButton(){
        guard let apt = CMQAPIManager.sharedInstance().apartment else {return}
        guard apt.hasKaba else {return}
        guard CMQDormaKabaManager.sharedInstance().readyToUse else {return}
        
        if let rightButton = self.navigationItem.rightBarButtonItem{
            if rightButton != kabaButton{
                self.navigationItem.rightBarButtonItems = [rightButton,flexSpace, kabaButton]
            }
        }
        else{
            self.navigationItem.rightBarButtonItem = kabaButton
        }
    }
    
    @objc public func handleSTwoButton(){
        guard let apt = CMQAPIManager.sharedInstance().apartment else { removeStwoButton(); return}
        guard apt.hasSTwo else { removeStwoButton();return}
        if let rightButton = self.navigationItem.rightBarButtonItem{
            if rightButton != sTwoButton{
                self.navigationItem.rightBarButtonItems = [rightButton,flexSpace, sTwoButton]
            }
        }
        else{
            self.navigationItem.rightBarButtonItem = sTwoButton
        }
    }
    @objc public func handlePackageButton(){
        guard let _ = pendingPackageMessage else {removePackageButton();return}
        if let rightButton = self.navigationItem.rightBarButtonItem{
            if rightButton != packageNavButton{
                self.navigationItem.rightBarButtonItems = [rightButton,flexSpace, packageNavButton]
            }
        }else{
            self.navigationItem.rightBarButtonItem = packageNavButton
        }
    }
    private func removePackageButton(){
        guard let buttons = self.navigationItem.rightBarButtonItems?.filter({$0 != packageNavButton}) else {return}
        self.navigationItem.rightBarButtonItems = buttons
    }
    private func removeStwoButton(){
        guard let buttons = self.navigationItem.rightBarButtonItems?.filter({$0 != sTwoButton}) else {return}
        self.navigationItem.rightBarButtonItems = buttons
    }
    override public func willChangeSource(to: CMQParseContainer) {
        super.willChangeSource(to: to)
        self.title = isAll ? "All Posts": " "
        if let nav = self.navigationController as? CMQNavigationController{
            nav.addButtonToView()
        }
        handleSTwoButton()
        self.tableView.scrollToRow(at: IndexPath.init(row: 0, section: 0), at: .top, animated: true)
    }
    // MARK: - Life Cycle
    override public func viewDidLoad() {
        //TODO: Update from deprecated
        //self.automaticallyAdjustsScrollViewInsets = false
        super.viewDidLoad()
        dataSource = CMQHomeFeedContainer()
        self.cellCreator = CellCreator.init(data: dataSource)
        
        NotificationCenter.default.addObserver(self, selector: #selector(handleKabaButton), name: NSNotification.Name(NOTIFICATION_FINAL_SYNCHRONIZE_COMPLETE), object: nil)
        
        
        if navBarHidden{
            self.navigationItem.leftBarButtonItem?.tintColor = .gray
            self.navigationItem.rightBarButtonItem?.tintColor = .gray
            
        }else{
            if !CMQUser.current().isMultiUser{
                self.navigationItem.rightBarButtonItem = nil
            }
            self.title = isAll ? "All Posts":"Home"
        }
        self.checkForPackageMessage()
    }
    
    override public func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if let nav = self.navigationController as? CMQNavigationController{
            nav.removeButton()
        }
    }
    override public func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        handleKabaButton()
        handleSTwoButton()
        if let nav = self.navigationController as? CMQNavigationController{
            nav.addButtonToView()
        }
        
    }
    
    override public func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    public func checkForPackageMessage(){
        if !CMQUser.current().isAdmin{
            let query = CMQMessage.query()!

            query.whereKey("packageNoti", equalTo: true)
            query.whereKey("packageState", equalTo: "notify")
            query.whereKey("deliveryStatus", notEqualTo: "DELIVERED OR PICKED UP")
            
            query.whereKey("recipients", equalTo: CMQUser.current().objectId!)
            query.includeKey("Package")
            query.getFirstObjectInBackground(block: self.packageMessageCompletion)
        }
    }
    override public func refreshObjects() {
        super.refreshObjects()
        self.checkForPackageMessage()
        if #available(iOS 10.0, *) {
            CMQPushNotificationHandler.sharedInstance().clearNotificationBadge()
        } else {
            // Fallback on earlier versions
        }
    }

    // MARK: - Navigation
    
    override public func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let identifier = segue.identifier{
            guard let aSegue = CMQHomeSegue.init(rawValue: identifier) else {return}
            var someSender = sender
            self.transitionHandler = aSegue.prepareTransitionHandler(viewFrame: self.view.frame)
            switch aSegue{
            case .PackagePrompt:
                someSender = self.pendingPackageMessage
            case .LogOut:
                if let nav = self.navigationController as? CMQNavigationController{
                    nav.removeButton()
                }
            default:
                break
            }
            aSegue.prepare(dest: segue.destination, sender: someSender, transitionHandler: transitionHandler)
            
        }
        
    }
   
}
    //MARK: - TableView Delegate/ DataSource
extension CMQHomeFeedViewController {
    public override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
        
    }
    public override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        guard tableView == self.tableView, CMQAPIManager.sharedInstance().apartment != nil else {return 1}
        if noContentViewAdded{
            return 1
        }
        if UIDevice.current.userInterfaceIdiom == .pad{
            return self.view.frame.height * 0.45
        }
        return self.view.frame.height*0.33
    }
    public override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let header = headerView, tableView == self.tableView, self.tableView(tableView, heightForHeaderInSection: 0) != 1 else {return nil}
        header.aptImageView.image = nil
        header.aptImageView.file = CMQAPIManager.sharedInstance().apartment.aptPhoto
        header.aptImageView.loadInBackground()
        return header
    }

    override public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "ViewItem", sender: tableView.cellForRow(at: indexPath))
    }
}

public enum CMQHomeSegue:String, IDSeguePreparationProtocol{
    

    case Feed = "ViewFeed"
    case SmartHome = "SmartHome"
    case LegacySmartHome = "LegacySmartHome"
    case Pay = "PayLease"
    case Custom = "Custom"
    case Item = "ViewItem"
    case Compose = "Compose"
    case Menu = "Menu"
    case SelectCommunity = "SelectCommunity"
    case Unlock = "Unlock"
    case ComposeMenu = "ComposeMenu"
    case LogOut = "LogOut"
    case PackagePrompt = "PackagePrompt"
    case Account = "Account"
    case CommunityInfo = "Community Info"
    case More = "More"
    case Support
    case Packages
    case PayRent
    case WorkOrder
    
    func prepareTransitionHandler(viewFrame:CGRect)->CMQTransitionDelegate?{
        switch self {
        case .Unlock:
            let height = viewFrame.size.height*0.50
            let startY = (viewFrame.size.height*0.25)
            let width = UIDevice.current.userInterfaceIdiom == .pad ? viewFrame.width * 0.40 : viewFrame.width*0.75
            return CMQTransitionDelegate(size: CGSize.init(width: width, height: height), origin: CGPoint.init(x: viewFrame.width/2 - (width/2), y: startY))
        case .Menu:
            let width = UIDevice.current.userInterfaceIdiom == .pad ? viewFrame.size.width*0.55 : (viewFrame.size.width/4)*3
            return CMQTransitionDelegate(size: CGSize.init(width: width, height: UIScreen.main.bounds.height), origin: CGPoint.init(x: 0, y: 0))
        case .PackagePrompt:
            let size = CGSize.init(width: UIDevice.current.userInterfaceIdiom == .pad ? viewFrame.width * 0.40 : viewFrame.width*0.75, height: UIDevice.current.userInterfaceIdiom == .pad ? viewFrame.height * 0.35 : viewFrame.height*0.50)
            let origin = CGPoint.init(x: viewFrame.size.width/2 - size.width/2, y: (viewFrame.size.height*0.25))
            return CMQTransitionDelegate(size: size, origin: origin)
            
        case .ComposeMenu:
            let height = viewFrame.size.height*0.25
            return CMQTransitionDelegate(size: CGSize.init(width: viewFrame.size.width-10, height: height), origin: CGPoint.init(x: 5, y: viewFrame.origin.y+viewFrame.height - height))
        default:
            return nil
        }
        
    }
    func prepare(dest: UIViewController, sender: Any?, transitionHandler:UIViewControllerTransitioningDelegate? = nil) {
        switch self {
            
        case .Feed:
            guard let dest = dest as? IDTableViewController, let container = sender as? CMQFeedContainer else {return}
            dest.dataSource = container
            dest.cellCreator = CellCreator.init(data: dest.dataSource)
            
        case .PayRent, .WorkOrder, .SmartHome, .LegacySmartHome, .Pay,.Support:
            guard let dest = dest as? CMQWebViewController, let url = sender as? URL else {return}
            dest.url = url
            dest.title = self.navigationTitle
            dest.navTitle = self.navigationTitle
        case .Custom:
            guard let dest = dest as? CMQWebViewController else {return}
            if let dict = sender as? [String:Any]{
                dest.url = dict["url"] as? URL
                dest.title = dict["title"] as? String
            }else if let url = sender as? URL{
                dest.url = url
                dest.title = "SmartHome"
            }
        case .Item:
            guard let cell = sender as? CMQFeedTableViewCell, let dest = dest as? CMQFeedContentViewController else {return}
            dest.object = cell.object
        
        case .Compose:
            guard let dest = dest as? CMQComposeViewController, let tag = sender as? Int else {return}
                switch tag{
                case 0:
                    dest.composableItem = CMQFeedItem.init(object: CMQMessage.init())
                case 1:
                    dest.composableItem = CMQFeedItem.init(object: CMQEvent.init())
                case 2:
                    dest.composableItem = CMQFeedItem.init(object: CMQNewsArticle.init())
                default:
                    return
                }
        
        case .Menu:
            guard let dest = dest as? CMQMenuViewController else {return}
            dest.transitioningDelegate = transitionHandler
            dest.modalPresentationStyle = .custom
            
        case .SelectCommunity:
            guard let dest = dest as? CMQCommunitiesViewController else {return}
            let query = CMQApartment.query()
            let _ = try? CMQUser.current()?.fetch()
            let commIDs:[String] = CMQUser.current().communities.compactMap({($0 as! CMQApartment).objectId})
            //query?.whereKey("objectId", containedIn: commIDs)
            query?.whereKey("objectId", containedIn: commIDs)
            query?.order(byAscending: "aptName")
            dest.dataSource = CMQDataSourceManager.init(query: query!, delegate: dest)
        
        case .Unlock:
            dest.transitioningDelegate = transitionHandler
            dest.modalPresentationStyle = .custom
        
        case .ComposeMenu:
            guard let dest = dest as? CMQComposeMenuViewController else {return}
            dest.transitioningDelegate = transitionHandler
            dest.modalPresentationStyle = .custom
        
        case .LogOut:
            //break
            UIApplication.shared.statusBarStyle = .lightContent
        case .PackagePrompt:
            guard let dest = dest as? CMQPackageDeliveryConfirmationViewController, let message = sender as? CMQMessage else {return}
            dest.item = FeedItemStruct.init(object: message)
            dest.transitioningDelegate = transitionHandler
            dest.modalPresentationStyle = .custom
        default:
            break
        }
        
    }
    
}
extension CMQHomeSegue{
    var navigationTitle:String{
        switch self {
        case .LegacySmartHome:
            return "SmartHome"
        case .SmartHome:
            return "SmartHome By Epproach"
        case .Pay:
            return "PayLease"
        case .PayRent:
            return "Pay Rent"
        case .WorkOrder:
            return "Work Order"
        case .Support:
            return "Epproach Support"
        default:
            return self.rawValue
        }
        
    }
}
