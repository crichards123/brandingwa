//
//  CMQComposeTableViewCell.swift
//  Communique
//
//  Created by Andre White on 3/21/18.
//  Copyright © 2018 Communique, LLC. All rights reserved.
//

import UIKit

public class CMQComposeTableViewCell: UITableViewCell {
    @IBOutlet weak var inputField:CMQTextField!
    @IBOutlet weak var inputTextView: CMQTextView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var pendingImageView: UIImageView!
    
    @IBOutlet weak var closeButton: UIButton!
    
    override public func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        if let id = self.reuseIdentifier, let type = PropertyType(rawValue: id){
            switch type{
            case .Image:
                print("image")
            case .Date:
                print ("Date")
            case .Content:
                inputTextView.editingPropertyType = type
            default:
                inputField.editingPropertyType = type
            }
        }
    }
    public func set(content:Any?){
        guard let id = self.reuseIdentifier, let type = ItemProperty.PropertyType(rawValue: id) else {return}
        switch type {
        case .Image:
            guard let view = pendingImageView, let image = content as? UIImage else {return}
            view.image = image
        case .Date:
            guard let view = dateLabel, let date = content as? Date else {return}
            view.text = date.scheduleMessageDateView
        case .Content:
            guard let view = inputTextView, let text = content as? String else {return}
            view.text = text
        default:
            guard let view = inputField, let text = content as? String else {return}
            view.text = text
        }
        
    }
}
