//
//  CMQFeedItem.swift
//  Communique
//
//  Created by Andre White on 4/21/18.
//  Copyright © 2018 Communique, LLC. All rights reserved.
//

import UIKit
//Wrapper for CMQMessage, CMQEvent, and CMQNewsArticle that allows for the data to easily be displayed and manipulated. Currently only used in ComposeViewController. 
public class CMQFeedItem: NSObject {
    public enum ItemType:String{
        case Message = "Message"
        case Event = "Event"
        case News = "News"
        
        var color:UIColor{
            switch self {
            case .Message:
                return UIColor.MessageColor
            case .Event:
                return UIColor.EventColor
            case .News:
                return UIColor.NewsColor
            }
        }
    }
    public var itemType:ItemType{
        return ItemType.init(rawValue: self.object.parseClassName)!
    }
    private(set) var object:PFObject
    public var objectId:String{
        return object.objectId!
    }
    public var sortDate:Date{
        return object.sortDate
    }
    public var composableProperties:[PropertyType]{
        var props = [PropertyType.Title, PropertyType.Content, PropertyType.Image]
        switch self.object {
        case is CMQMessage:
            props.append(contentsOf: [PropertyType.Urgent, PropertyType.Date])
        case is CMQEvent:
            props.append(contentsOf: [PropertyType.Date, PropertyType.Location])
        case is CMQNewsArticle:
            props.append(PropertyType.Url)
        default:
            break
        }
        return props
    }
    public var requiredProperties:[PropertyType]{
        return self.composableProperties.filter({$0.isReq(object: self.object)})
    }
    public var unSetReqProperties:[PropertyType]{
        return self.requiredProperties.filter({ (property) -> Bool in
            if property == .Content || property == .Title || property == .Location{
                guard let content = self.content(forProperty: property) as? String else {return true}
                return content.isEmpty()
            }else{
                return content(forProperty: property) == nil
            }
        })
    }
    
    public var isComposed:Bool{
        return unSetReqProperties.count == 0 
    }
    public func content(forProperty:PropertyType)->Any?{
        if let key = forProperty.key(forObject: self.object){
            if forProperty == .Date && itemType == .Message{
                guard let dateString = self.object.object(forKey: key) as? String else {return nil}
                return Date.fromParse(string: dateString)
            }else{
                return self.object.object(forKey: key)
            }
        }else{
            return nil
        }
    }
    public func set(property:PropertyType, content:Any?){
        if property != .Attendees || property != .Package || property != .Color || property != .EventDetails{
            if let key = property.key(forObject: self.object){
                if property == .Date {
                    switch self.itemType{
                    case .Message:
                        if content == nil{
                            self.object.remove(forKey: key)
                            self.object.remove(forKey: "scheduleMessage")
                            self.object.remove(forKey: "scheduleSent")
                            
                        }else{
                            guard let date = content as? Date else {return}
                            self.object.setObject(date.toParse, forKey: "scheduleTime")
                            self.object.setObject(true, forKey: "scheduleMessage")
                            self.object.setObject(false, forKey: "scheduleSent")
                        }
                        
                    case .Event:
                        if content == nil{
                            self.object.remove(forKey: key)
                            self.object.remove(forKey: "utcOffset")
                        }else{
                            guard let date = content as? Date else {return}
                            self.object.setObject(TimeZone.current.secondsFromGMT(for: date), forKey: "utcOffset")
                            self.object.setObject(date, forKey: key)
                        }
                       
                    case .News:
                        return
                    }
                }else if property == .Image{
                    if content == nil {
                        self.object.remove(forKey: key)
                    }
                    guard let photo = content as? UIImage else {return}
                    self.object.setObject(photo.toParse()!, forKey: key)
                    
                }else if property == .Recipients{
                    guard let message = object as? CMQMessage, let recipients = content as? [String] else {return}
                    message.addUniqueObjects(from: recipients, forKey: key)
                    message.isAnnouncement = true
                    message.aptComplexID = nil
                }else{
                    self.object.setObject(content!, forKey: key)
                }
                
            }
        }
    }
    public init(object:PFObject){
        self.object = object
        super.init()
    }
    func post(completion:@escaping PFBooleanResultBlock){
        self.object.setObject(CMQUser.current(), forKey: "userWhoPosted")
        if content(forProperty: .Recipients) == nil{
            self.object.setObject(CMQUser.current().complexID, forKey: "aptComplexID")
        }
        self.object.saveInBackground(block: completion)
    }

}
extension PropertyType{
    public func key(forObject:PFObject)->String?{
        switch self {
        
        case .Title:
            switch forObject{
            case is CMQEvent:
                return "eventTitle"
            case is CMQMessage:
                return "messageTitle"
            case is CMQNewsArticle:
                return "newsTitle"
            default:
                return nil
            }
        case .Content:
            switch forObject{
            case is CMQEvent:
                return "eventContent"
            case is CMQMessage:
                return "messageContent"
            case is CMQNewsArticle:
                return "newsContent"
            default:
                return nil
            }
        case .Url:
            guard forObject is CMQNewsArticle else {return nil}
            return "articleURL"
        case .Location:
            guard forObject is CMQEvent else {return nil}
            return "eventLocation"
        case .Date:
            switch forObject{
            case is CMQEvent:
                return "eventDate"
            case is CMQMessage:
                return "scheduleTime"
            default:
                return nil
            }
        case .Image:
            switch forObject{
            case is CMQEvent:
                return "eventPhoto"
            case is CMQMessage:
                return "messagePhoto"
            case is CMQNewsArticle:
                return "newsPhoto"
            default:
                return nil
            }
        case .Urgent:
            guard forObject is CMQMessage else {return nil}
            return "isCritical"
        case .Recipients:
            guard forObject is CMQMessage else {return nil}
            return "recipients"
        case .TimeSent:
            return "sortDate"
        case .Attendees:
            return nil
        case .Sender:
            return "userWhoPosted"
        case .Package:
            return nil
        case .Color:
            return nil
        case .EventDetails:
            return nil
        }
    
    }
    public func isReq(object:PFObject)->Bool{
        switch self {
        case .Title:
            return true
        case .Content:
            return true
        case .Urgent:
            guard object is CMQMessage else {return false}
            return true
        case .Date:
            guard object is CMQEvent else {return false}
            return true
        case .Location:
            guard object is CMQEvent else {return false}
            return true
        default:
            return false
        }
    }
}

