//
//  CMQComposeViewController.swift
//  Communique
//
//  Created by Andre White on 2/5/18.
//  Copyright © 2018 Communique, LLC. All rights reserved.
//

import UIKit
import CropViewController
public class CMQComposeViewController: CMQParentViewController, UITextFieldDelegate, UITextViewDelegate, CropViewControllerDelegate {

    @IBOutlet weak var urgentSwitch: UISwitch!
    
    
    @IBOutlet weak var scheduleButton:UIButton!{
        didSet{
            scheduleButton.isHidden = composableItem.itemType == .News
        }
    }
    @IBOutlet weak var separatorView: UIView!{
        didSet{
            separatorView.isHidden = composableItem.itemType != .Message
        }
    }
    @IBOutlet weak var normalOrUrgentSwitch:UISwitch!{
        didSet{
            if let aSwitch = normalOrUrgentSwitch{
                aSwitch.isHidden = composableItem.itemType != .Message
                if !aSwitch.isHidden{
                    composableItem.set(property: .Urgent, content: false)
                }
            }
        }
    }
    @IBOutlet weak var normalOrUrgentLabel: UILabel!{
        didSet{
            if let aLabel = normalOrUrgentLabel{
                aLabel.isHidden = composableItem.itemType != .Message
            }
        }
    }
    private var postCompletion: PFBooleanResultBlock {
        return { (completed:Bool, error:Error?)->() in
            if let error = error{
                handle(error: error, message: "Posting Object", shouldDisplay: true)
            }else{
                if completed{
                    //Post FeedBack
                    CMQAPIManager.showFeedback(message: "Posted Successfully")
                    self.performSegue(withIdentifier: "Post", sender: nil)
                }
            }
        }
    }
    fileprivate var localPhoto:UIImage?
    @IBOutlet weak var postButton: UIBarButtonItem!
    private lazy var sendButtonImage:UIImage? = UIImage.init(named: CMQSendButtonImageName, in: CMQBundle, compatibleWith: nil)
    private lazy var communityButtonImage:UIImage? = UIImage.init(named: CMQSelectCommunitiesImageName, in: CMQBundle, compatibleWith: nil)
    private var transitionHandler : CMQTransitionDelegate?
    
    private var photoPicker: CMQPhotoPickerDelegate?{
        didSet{
            guard let picker = self.photoPicker else {return}
            picker.photoCompletionBlock = {
                (image: UIImage?)->Void in
                guard let image = image else {return}
                self.dismiss(animated: true, completion: {
                    let cropViewController = CropViewController(image: image)
                    cropViewController.delegate = self
                    self.present(cropViewController, animated: true, completion: nil)
                })
            }
        }
    }
    //MARK: - Crop ViewController
    public func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        self.dismiss(animated: true) {
            self.composableItem.set(property: .Image, content: image)
            self.localPhoto = image
            self.tableView.reloadData()
        }
        
    }
    public var composableItem:CMQFeedItem!
    @IBOutlet weak var tableView:UITableView!
    
    //MARK: - Life cycle
    public override func viewDidLoad() {
        super.viewDidLoad()
        self.photoPicker = CMQPhotoPickerDelegate.init(controller: self, wEditing: false)
        if let navButton = self.navigationItem.rightBarButtonItem{
            navButton.image = CMQAPIManager.sharedInstance().apartment == nil ? communityButtonImage : sendButtonImage
        }
        NotificationCenter.default.addObserver(self, selector: #selector(handleUpdateNotification(sender:)), name: .updateComposable, object: nil)
        self.tableView.contentInset = UIEdgeInsets.init(top: 5, left: 0, bottom: 0, right: 0)
    }
    
    override public func viewDidAppear(_ animated: Bool) {
        tableView.tableFooterView = UIView()
    }
    
    //MARK: - Exit Segues
    @IBAction func scheduleMessage(sender:UIStoryboardSegue){
        guard let source = sender.source as? CMQScheduledMessageViewController else {return}
        composableItem.set(property: .Date, content: source.selectedDate)
        self.tableView.reloadData()
        updateSendButton()
    }
    @IBAction func cancel(sender:UIStoryboardSegue){
        
    }
    @IBAction func didSelectCommunities(sender: UIStoryboardSegue){
        if let communitiesVC = sender.source as? CMQSelectCommunitiesForSendViewController{
            self.composableItem.set(property: .Recipients, content: communitiesVC.selectedApartments.compactMap({$0.objectId}))
           composableItem.post(completion: postCompletion)
        }
    }
    //MARK: - IBActions
    @IBAction func didPressPhotoButton(sender:UIButton){
        guard let photoPicker = self.photoPicker else {return}
        if (UIDevice.current.userInterfaceIdiom == .pad){
            photoPicker.popOverView = sender
        }
        self.present(photoPicker.alertController, animated: true, completion: nil)
    }
    @IBAction func didSwitch(sender:UISwitch){
        guard composableItem.itemType == .Message else {return}
        normalOrUrgentLabel?.text = sender.isOn ? "Urgent" : "Normal"
        composableItem.set(property: .Urgent, content: sender.isOn)
    }
    @IBAction func didPressPost(){
        if self.composableItem.unSetReqProperties.count > 0 {
            var message = "Required fields: "
            for prop in self.composableItem.unSetReqProperties{
                message.append(prop.rawValue)
                message.append(" ")
            }
            CMQAPIManager.showError(message: message)
        }else{
            if CMQAPIManager.sharedInstance().apartment == nil{
                performSegue(withIdentifier: "SelectCommunity", sender: nil)
            }
            else{
                self.composableItem.post(completion: postCompletion)
            }
        }
        
    }
    @IBAction func textFieldChanged(_ sender: CMQTextField) {
        guard let type = sender.editingPropertyType, let curText = sender.text else {return}
        self.updateComposable(propertyType: type, content: curText)
    }
    @IBAction func shouldDelete(sender:UIButton){
        guard let cell = sender.superview?.superview as? CMQComposeTableViewCell, let propertyType = PropertyType(rawValue: cell.reuseIdentifier!) else {return}
        self.composableItem.set(property: propertyType, content: nil)
        if propertyType == .Image{
            if localPhoto != nil{
                localPhoto = nil
            }
        }
        self.tableView.reloadData()
    }
    
    public func updateSendButton(){
        postButton.isEnabled = composableItem.isComposed
    }
    public func updateComposable(propertyType:PropertyType,  content:Any?){
        self.composableItem.set(property: propertyType, content: content)
        updateSendButton()
    }
    @objc public func handleUpdateNotification(sender:Notification){
        guard let object = sender.object as? [String:Any], let type = object["property"] as? PropertyType, let content = object["content"] else {return}
        self.updateComposable(propertyType: type, content: content)
    }
    //MARK: - Navigation
    func prepareTransition(){
        transitionHandler = CMQTransitionDelegate.init(size: CGSize.init(), origin: CGPoint.init())
    }
    
    public override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ScheduleMessage"{
            guard let dest = segue.destination as? CMQScheduledMessageViewController else {return}
            self.prepareTransition()
            dest.transitioningDelegate = transitionHandler
            dest.modalPresentationStyle = .custom
            //dest.selectedDate = composableItem.propertyContent(propertyType: .Date) as? Date
            //dest.isScheduledMessage = composableItem.composableType == .Message
            dest.selectedDate = composableItem.content(forProperty: .Date) as? Date
            dest.isScheduledMessage = composableItem.itemType == .Message
            
        }
        if segue.identifier == "SelectCommunity"{
            guard let dest = segue.destination as? CMQSelectCommunitiesForSendViewController else {return}
            let query = CMQApartment.query()
            let commIDs:[String] = CMQUser.current().communities.compactMap({($0 as! CMQApartment).objectId})
            //query?.whereKey("objectId", containedIn: commIDs)
            query?.whereKey("objectId", containedIn:commIDs )
            query?.order(byAscending: "aptName")
            dest.dataSource = CMQDataSourceManager.init(query: query!, delegate: dest)
        }
    }
}
//MARK: - TableView Delegate/ DataSource
extension CMQComposeViewController : UITableViewDelegate, UITableViewDataSource{
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return composableItem.visibleProperties.count
    }
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let property = composableItem.visibleProperties[indexPath.row]
        let height = self.tableView.frame.height
        let numberOfFields = composableItem.visibleProperties.filter({$0 != .Image && $0 != .Content}).count
        switch property{
        case .Content:
            let float = CGFloat(numberOfFields)*0.11
            let availableSpace = CGFloat(composableItem.visibleProperties.contains(where: {$0 == .Image}) ? (numberOfFields == 3 ? 0.51:0.31): 1.0)
            return height*(availableSpace-float)
        case .Image:
            return numberOfFields == 3 ? height*0.49:height*0.69
        default:
            return height*0.10
        }
        
    }
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let cell = tableView.cellForRow(at: indexPath) as? CMQComposeTableViewCell else {return}
        if let field = cell.inputField{
            field.becomeFirstResponder()
        }else if let view = cell.inputTextView{
            view.becomeFirstResponder()
        }
    }
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let property = composableItem.visibleProperties[indexPath.row]
        guard let cell = tableView.dequeueReusableCell(withIdentifier: property.rawValue, for: indexPath) as? CMQComposeTableViewCell else {return CMQComposeTableViewCell()}
        if let content = composableItem.content(forProperty: property){//composableItem.propertyContent(propertyType: property.propertyType){
            if property == .Image{
                cell.pendingImageView.image = self.localPhoto
            }else{
                cell.set(content: content)
            }
            if property == .Date && composableItem.itemType == .Event{
                cell.closeButton?.isHidden = true
            }else{
                cell.closeButton?.isHidden = false
            }
        }
        if property == .Content{
            cell.inputTextView?.placeHolder.text = composableItem.itemType == .Message ? "Enter Message" : "Enter Description"
        }
        if let view = cell.inputTextView {
            if self.textViews == nil{
                self.textViews = [UITextView]()
            }
            self.textViews?.append(view)
        }
        if let field = cell.inputField{
            if self.textFields == nil{
                self.textFields = [UITextField]()
            }
            self.textFields?.append(field)
        }
        cell.selectionStyle = .none
        return cell
    }
}
extension CMQFeedItem{
    public var visibleProperties:[PropertyType]{
        var visible = [PropertyType]()
        let properties = self.composableProperties
        if let property = properties.filter({$0 == .Title}).first{
            visible.append(property)
        }
        if let property = properties.filter({$0 == .Url}).first{
            visible.append(property)
        }
        if let property = properties.filter({$0 == .Location}).first{
            visible.append(property)
        }
        if let property = properties.filter({$0 == .Content}).first{
            visible.append(property)
        }
        if let property = properties.filter({$0 == .Image}).first{
            if let _ = self.content(forProperty: property){
                visible.append(property)
            }
        }
        if let property = properties.filter({$0 == .Date}).first{
            if self.itemType == .Event{
                visible.append(property)
            }else if let _ = self.content(forProperty: property){
                 visible.append(property)
            }
        }
        return visible
    }
}

