//
//  CMQFeedItemProperty.swift
//  Communique
//
//  Created by Andre White on 4/21/18.
//  Copyright © 2018 Communique, LLC. All rights reserved.
//

import UIKit
public enum PropertyType:String{
    case Title = "Title"
    case Content = "Content"
    case Url = "Url"
    case Location = "Location"
    case Date = "Date"
    case Image = "Image"
    case Urgent = "Urgent"
    case Recipients = "Recipients"
    case TimeSent = "TimeSent"
    case Attendees = "Attendees"
    case Sender = "Sender"
    case Package = "Package"
    case Color = "Color"
    case EventDetails = "EventDetails"
}
public class CMQFeedItemProperty: NSObject {
    public var propertyContent:Any?
    public var propertyType:PropertyType
    
    public init(type:PropertyType, content:Any?) {
        self.propertyType = type
        self.propertyContent = content
        super.init()
    }

}
