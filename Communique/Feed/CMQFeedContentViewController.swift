//
//  CMQFeedContentViewController.swift
//  Communique
//
//  Created by Andre White on 1/23/18.
//  Copyright © 2018 Communique, LLC. All rights reserved.
//

import UIKit

public class CMQFeedContentViewController: UITableViewController {
    
    public var object:PFObject!
    private var transitionHandler: CMQTransitionDelegate?
    private var respondingContentView:CMQObjectContentView?
    //MARK: - Life Cycle
    public override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.tableFooterView = UIView()
    }
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        addObservers()
    }
    public override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        removeObservers()
    }
    
    
    
    @IBAction func packageConfirmation(sender:UIStoryboardSegue){
        guard let confirmationVC = sender.source as? CMQPackageDeliveryConfirmationViewController, let ocView = respondingContentView else {return}
        if confirmationVC.isConfirmed{
            ocView.acceptedDelivery()
        }
    }
    //MARK: - Observers
    public func addObservers(){
        NotificationCenter.default.addObserver(self, selector: #selector(didRespondToPackagePrompt(notification:)), name: .didRespondToPackagePrompt, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleAttendeesTapped(notification:)), name: .didTapAttendees, object: nil)
    }
    public func removeObservers(){
        NotificationCenter.default.removeObserver(self, name: .didRespondToPackagePrompt, object: nil)
        NotificationCenter.default.removeObserver(self, name: .didTapAttendees, object: nil)
    }
    @objc public func handleAttendeesTapped(notification:Notification){
        if let attendees = notification.object as? [String]{
            let query = CMQUser.query()!
            query.whereKey("objectId", containedIn: attendees)
            self.performSegue(withIdentifier: "RSVP", sender: query)
        }
    }
    @objc public func didRespondToPackagePrompt(notification:Notification){
        guard let ocView = notification.object as? CMQObjectContentView else {return}
        self.respondingContentView = ocView
        self.showDeliveryConfirmation()
    }
    //MARK: - TableView DataSource/ Delegate
    public override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    public override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
   public override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    guard  let object = object, let cell = tableView.dequeueReusableCell(withIdentifier: object.parseClassName, for: indexPath) as? CMQFeedTableViewCell else {return UITableViewCell()}
    cell.feedContentView.layer.shadowOffset = .zero
    cell.feedContentView.layer.shadowRadius = 0
    if UIDevice.current.userInterfaceIdiom == .pad{
        cell.preferredImageHeight = 700
    }else{
        cell.preferredImageHeight = 400
    }
    cell.object = object
    cell.feedContentView.objectContentView.feedImageView.contentMode = .scaleAspectFit
    //cell.feedContentView.objectContentView.imageHeight.isActive = false
    
    cell.selectionStyle = .none
    return cell
    }
    //MARK: - Scrollview Delegate
    override public func scrollViewDidScroll(_ scrollView: UIScrollView) {
        guard let height = self.navigationController?.navigationBar.frame.height else {return}
        if scrollView.contentOffset.y<(-1)*(height+UIApplication.shared.statusBarFrame.height){
            scrollView.contentOffset.y = (-1)*(height+UIApplication.shared.statusBarFrame.height)
        }
    }
    //MARK: - Navigation
    public func showDeliveryConfirmation(){
        guard let confimationController = CMQPackageDeliveryConfirmationViewController.instanceFromStoryBoard() else {return}
        self.prepareTransition()
        //confimationController.item = CMQFeedItem.init(object: self.object)
        confimationController.transitioningDelegate = transitionHandler
        confimationController.modalPresentationStyle = .custom
        self.present(confimationController, animated: true, completion: nil)
    }
    public func prepareTransition(){
        let width = self.view.frame.size.width*0.75
        let height = self.view.frame.size.height*0.25
        let x = self.view.center.x-(width/2)
        let y = self.view.center.y-(height/2)
        self.transitionHandler = CMQTransitionDelegate(size: CGSize.init(width:width, height: height), origin: CGPoint.init(x: x, y: y))
        
    }
    override public func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "RSVP"{
            if let query = sender as? PFQuery<PFObject>,
                let dest = segue.destination as?
                CMQEventRSVPTableViewController{
                dest.dataSource = CMQDataSourceManager.init(query: query, delegate: dest)
            }
        }
    }
}
