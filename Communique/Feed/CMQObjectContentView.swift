//
//  CMQObjectContentView.swift
//  Communique
//
//  Created by Andre White on 1/3/18.
//  Copyright © 2018 Communique, LLC. All rights reserved.
//

import UIKit
import ParseUI

public class CMQObjectContentView: UIView {
    @IBOutlet weak var feedTitleLabel: UILabel!
    @IBOutlet weak var feedContentLabel: UILabel!
    @IBOutlet weak var packageViewHeight:NSLayoutConstraint!
    @IBOutlet weak var imageHeight: NSLayoutConstraint!
    @IBOutlet weak var feedDateLabel:UILabel!
    @IBOutlet weak var feedTimeLabel:UILabel!
    @IBOutlet weak var feedLocationLabel:UILabel!
    @IBOutlet weak var attendeesLabel:UILabel!{
        didSet{
            attendeesLabel?.addGestureRecognizer(UITapGestureRecognizer.init(target: self, action: #selector(didTapAteendees(gesture:))))
        }
    }
    @IBOutlet weak var attendingButton:UIButton!
    @IBOutlet weak var deliveryLabel:UILabel!
    @IBOutlet weak var deliveryPromptView:CMQPackagePromptView!
    @IBOutlet weak var noButton:UIButton!{
        didSet{
            self.buttonWidth = noButton.frame.size.width
        }
    }
    @IBOutlet weak var yesButton:UIButton!
    private var buttonWidth:CGFloat!
    @IBOutlet weak var urlLabel:UILabel!{
        didSet{
            urlLabel?.addGestureRecognizer(UITapGestureRecognizer.init(target: self, action: #selector(didTapUrl(sender:))))
        }
    }
    @IBOutlet weak var feedImageView: PFImageView!{
        didSet{
            guard let view = self.feedImageView else {return}
            view.contentMode = .scaleAspectFill
            view.layer.cornerRadius = 5
            view.clipsToBounds = true
        }
    }
    public var item:FeedItemStruct!{
        didSet{
            guard let item = item else {return}
            if let photo = item.photo, let view = feedImageView{
                view.file = photo
                if UIDevice.current.userInterfaceIdiom == .pad{
                    view.contentMode = .scaleAspectFit
                }else{
                    view.contentMode = .scaleAspectFit
                }
                
                let completion = item.photoCompletion(view: view)
                view.load(inBackground: completion)
                //imageHeight?.isActive = false
                imageHeight?.constant = (preferedImageHeight != nil) ? preferedImageHeight! : UIDevice.current.userInterfaceIdiom == .pad ? 600 : 200
            }else{
                imageHeight?.constant = 0
                //imageHeight?.isActive = true
            }
            if let contentLabel = feedContentLabel, let content = item.content{
                contentLabel.text = content
            }
            if let titleLabel = feedTitleLabel, let title = item.title{
                titleLabel.text = title
            }
            if let dateLabel = feedDateLabel, let timeLabel = feedTimeLabel, let date = item.date, let time = item.time{
                dateLabel.text = date
                timeLabel.text = time
            }
            if let locationLabel = feedLocationLabel, let location = item.location{
                locationLabel.text = location
            }
            if let attendeesLabel = attendeesLabel, let string = item.attendingString{
                attendeesLabel.text = string
            }
            if let _ = attendingButton, let attending = item.attendingEvent{
                localAttending = attending
                setButton(attending: attending)
            }
            if let label = urlLabel, let url = item.url{
                label.text = url
            }
            if let isPckgeMsg = item.packageMessage{
                if isPckgeMsg{
                    packageViewHeight.constant = 200
                    self.deliveryPromptView.item = item
                    if let theMessage = item.object as? CMQMessage{
                        theMessage.addObserver(self, forKeyPath: #keyPath(CMQMessage.deliveryStatus), options: [.old, .new], context: nil)
                    }
                    
                   /* switch item.deliveryStatus{
                    case .some("initial"):
                        packageViewInitial()
                    case .some("YES"):
                        packageViewAccepted()
                    case .some("NO"):
                        packageViewDeclined()
                    default:
                        packageViewInitial()
                    }*/
                }else{
                    packageViewHeight.constant = 0
                }
            }else{
                packageViewHeight?.constant = 0
            }
            urlLabel?.isHidden = item.url == nil
        }
    }
    public var preferedImageHeight: CGFloat?
    public var declineDeliveryCompletion:((Error?)->Void)?{
        guard self.item.packageMessage == true else {return nil}
        let completion = { (error:Error?)->Void in
            //declined delivery. Update UI: remove pacakge Prompt, Show package declined view.
            guard error == nil else {return}
            self.packageViewDeclined()
            
        }
        return completion
    }
    private var packageLabelRecognizer:UITapGestureRecognizer{
        return UITapGestureRecognizer.init(target: self, action: #selector(didTapDeliveryPrompt(gestureRecogizer:)))
    }
    public var acceptDeliveryCompletion:((Error?)->Void)?{
        guard self.item.packageMessage == true else {return nil}
        let completion = { (error:Error?)->Void in
            //delivery accepted.  Update UI: remove pacakge Prompt, show delivery accepted view.
            guard error == nil else {return}
            self.packageViewAccepted()
            
        }
        return completion
    }
    public var attendCompletion:((Error?)->Void)?{
        return { (error:Error?)->Void in
            guard error == nil else {return}
            self.localAttending = !self.localAttending
            self.setButton(attending: self.localAttending)
            self.updateAttendingLabel()
        }
    }
    public var object: PFObject!{
        didSet{
            guard let object = object else {return}
            self.item = FeedItemStruct.init(object: object)
        }
    }
    private var localAttending:Bool = false
    @objc private func didTapUrl(sender:UITapGestureRecognizer){
        if let label = sender.view as? UILabel, let text = label.text{
            let urlText = text.contains("https://") || text.contains("http://") ? text : String.init(format: "https://%@", text)
            guard let url = URL.init(string: urlText) else {return}
            if #available(iOS 10.0, *) {
                DispatchQueue.main.async {
                    if UIApplication.shared.canOpenURL(url as URL){
                        UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
                    }
                }
            } else {
                UIApplication.shared.openURL(url as URL)
            }
        }
    }
    //MARK: - IBActions
    @IBAction public func didDeclineDelivery(sender:UIButton){
        guard let completion = declineDeliveryCompletion else {return}
        item.set(delivery:false, completion:completion)
    }
    @IBAction public func didAcceptDelivery(sender:UIButton){
        guard let _ = acceptDeliveryCompletion else {return}
        NotificationCenter.default.post(name: .didRespondToPackagePrompt, object: self)
    }
    @IBAction public func didPressAttendButton(sender:UIButton){
        guard let completion = attendCompletion else {return}
        item.set(attend: !localAttending, completion: completion)
    }
    @objc public func didTapDeliveryPrompt(gestureRecogizer:UITapGestureRecognizer){
        self.deliveryLabel.removeGestureRecognizer(gestureRecogizer)
        self.packageViewInitial()
    }
    @objc public func didTapAteendees(gesture:UITapGestureRecognizer){
        guard let event = self.object as? CMQEvent else {return}
        NotificationCenter.default.post(name: .didTapAttendees, object: event.eventAttendees)
    }
    
    public func acceptedDelivery(){
        guard let completion = acceptDeliveryCompletion else {return}
        item.set(delivery:true, completion:completion)
    }
    //MARK: - KVO 
    override public func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == #keyPath(CMQMessage.deliveryStatus){
            self.deliveryPromptView?.awakeFromNib()
        }
    }
    //MARK: - UI
    public func setButton(attending:Bool){
        attendingButton.setTitle(attending ? "ATTENDING" : "ATTEND", for: .normal)
        attendingButton.backgroundColor = attending ? .EventColor : .DarkColor
        setNeedsLayout()
    }
   
    public func updateAttendingLabel(){
        attendeesLabel?.text = item.attendingString
        setNeedsLayout()
    }
    public func packageViewInitial(){
        self.noButton.isHidden = false
        self.yesButton.isHidden = false
        self.deliveryLabel.isHidden = true
        
    }
    public func packageViewDeclined(){
        self.deliveryLabel.text = "Picking up myself"
        self.noButton.isHidden = true
        self.yesButton.isHidden = true
        self.deliveryLabel.isHidden = false
        self.deliveryLabel.addGestureRecognizer(self.packageLabelRecognizer)
        self.setNeedsLayout()
    }
    public func packageViewAccepted(){
        self.deliveryLabel.isHidden = false
        self.deliveryLabel.text = "Delivery Pending"
        self.noButton.isHidden = true
        self.yesButton.isHidden = true
        self.setNeedsLayout()
    }
    
    public func reset(){
        if let view = feedImageView{
            view.file = nil
            view.image = nil
        }
        if let label = feedContentLabel{
            label.text = nil
        }
        if let label = feedTitleLabel{
            label.text = nil
        }
        if let label = feedDateLabel{
            label.text = nil
        }
        if let label = feedLocationLabel{
            label.text = nil
        }
        if let label = attendeesLabel{
            label.text = nil
        }
    }
}
