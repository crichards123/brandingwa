//
//  CMQFeedTableViewCell.swift
//  Communique
//
//  Created by Andre White on 1/19/18.
//  Copyright © 2018 Communique, LLC. All rights reserved.
//

import UIKit
import ParseUI
public class CMQFeedTableViewCell: PFTableViewCell, UITableViewDataSource, UITableViewDelegate {
    
    public var viewable:Viewing!{
        didSet{
            self.object = viewable as? PFObject
            self.viewableProperties = viewable.displayedProperties.filter({$0.propertyType != .TimeSent})
        }
    }
    //MARK: - Attempt to embed cell with a tableview. Currently working
    public var viewableProperties:[ItemProperty]!
    @IBOutlet weak var tableview: UITableView!{
        didSet{
            tableview?.delegate = self
            tableview?.dataSource = self
        }
    }
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewableProperties.count
    }
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
     public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let property = viewableProperties[indexPath.row]
        guard let cell = tableView.dequeueReusableCell(withIdentifier: property.propertyType.rawValue, for: indexPath) as? CMQViewablePropertyCell else {return CMQViewablePropertyCell()}
        if let content = viewable.propertyContent(propertyType: property.propertyType){
            if property.propertyType == .Sender{
                cell.setView(propertyType: property.propertyType, content: content)
                cell.setView(propertyType: .TimeSent, content: viewable.propertyContent(propertyType: .TimeSent))
            }else{
                cell.setView(propertyType: property.propertyType, content: content)
            }
        }
        return cell
    }
    //MARK: - End tableview attempt
    @IBOutlet weak var shadowView: UIView!{
        didSet{
            guard let view = shadowView else {return}
            
            view.layer.shadowRadius = 2
            view.layer.shadowColor = UIColor.black.cgColor
            view.layer.shadowOpacity = 0.5
            view.layer.shadowOffset = .zero
        }
    }
    
    @IBOutlet weak var feedContentView: CMQFeedContentView!{
        didSet{
            guard let view = feedContentView else{return}
            //view.layer.masksToBounds = false
            view.clipsToBounds = true
            view.layer.cornerRadius = 5
            
        }
    }
    
    public var object: PFObject!{
        didSet{
            guard let object = object, let view = feedContentView else {return}
            view.object = object
        }
    }
    public var preferredImageHeight: CGFloat?{
        didSet{
            guard let view = feedContentView else {return}
            view.objectContentView.preferedImageHeight = preferredImageHeight
        }
    }
    public func reset(){
        feedContentView.reset()
    }
}
