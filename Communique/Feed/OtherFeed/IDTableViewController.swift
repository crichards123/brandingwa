//
//  IDTableViewController.swift
//  Communique
//
//  Created by Andre White on 5/19/18.
//  Copyright © 2018 Communique, LLC. All rights reserved.
//

import UIKit

struct FeedCellCreator:IDTableCellConfigureProtocol{
    typealias ViewType = UITableView
    typealias CellType = UITableViewCell
    
    var dataSource: IDViewDataSourceProtocol
    var reuseIdentifier: String {
        switch dataSource {
        case is CMQEventsFeedContainer:
            return "Event"
        case is CMQNewsFeedContainer:
            return "News"
        default:
            return "Message"
        }
    }
    
    func configure(cell: UITableViewCell, atIndex: IndexPath) {
        guard let cell = cell as? CMQFeedTableViewCell else {return}
        guard let object = self.dataSource.object(at:atIndex) as? PFObject else {return}
        cell.reset()
        cell.object = object
    }
}
struct RefreshCellCreator:IDTableCellConfigureProtocol {
    typealias ViewType = UITableView
    typealias CellType = UITableViewCell
    
    var dataSource: IDViewDataSourceProtocol
    var reuseIdentifier: String{
        return "Refresh"
    }
    func configure(cell: UITableViewCell, atIndex: IndexPath) {
        let refreshControl = cell.viewWithTag(3) as? UIActivityIndicatorView
        refreshControl?.startAnimating()
    }
}
struct CellCreator:IDTableViewCellCreatorProtocol{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == dataSource.numOfRows{
            return self.refresh.cell(forTable: tableView, atIndex: indexPath)
        }else{ 
            return self.feed.cell(forTable: tableView, atIndex: indexPath)
        }
    }
    
    var refresh:RefreshCellCreator
    var feed:FeedCellCreator
    var dataSource: IDViewDataSourceProtocol
    init(data:IDViewDataSourceProtocol) {
        self.dataSource = data
        feed = FeedCellCreator(dataSource: dataSource)
        refresh = RefreshCellCreator(dataSource: dataSource)
    }
}

public class IDTableViewController: UITableViewController,IDTableViewDataSourceChangeProtocol {
    public var dataSource:IDViewDataSourceProtocol!{
        didSet{
            dataSource.delegate = self
        }
    }
    @IBInspectable public var pullToRefreshEnabled:Bool = true
    @IBInspectable public var paginationEnabled:Bool = true
    public var noContentViewAdded = false
    private var noContentView:CMQNoContentView? {
        guard let view = CMQNoContentView.instanceFromNib() else {return nil}
        view.set(parseClassName: "Posts")
        return view
    }
    open var cellCreator:IDTableViewCellCreatorProtocol!
    
    //MARK: - Life Cycle
    override public func viewDidLoad() {
        super.viewDidLoad()
    }
    private func handlePullToRefresh(){
        if pullToRefreshEnabled{
            let refresh = UIRefreshControl.init()
            refresh.addTarget(self, action: #selector(refreshObjects), for: .valueChanged)
            if #available(iOS 10.0, *) {
                self.tableView.refreshControl = refresh
            } else {
                // Fallback on earlier versions
            }
        }
    }
    private func removePullToRefresh(){
        if #available(iOS 10.0, *) {
            self.tableView.refreshControl = nil
        } else {
            // Fallback on earlier versions
        }
    }
    @objc public func refreshObjects(){
        dataSource.willChangeState(to: .refreshing)
    }
    
    
    // MARK: - Table view data source
    
    override public func numberOfSections(in tableView: UITableView) -> Int {
        return dataSource.numOfSec
    }
    
    override public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.numOfRows + (paginationEnabled && dataSource.canPaginate ? 1 : 0)
    }
    
    override public func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard paginationEnabled && dataSource.canPaginate else {return}
        if indexPath.row == dataSource.numOfRows{
            dataSource.willChangeState(to: .paginating)
        }
    }
    
    override public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return cellCreator.tableView(tableView, cellForRowAt: indexPath)
    }
    
    //IDDataSourceChange Protocol
    typealias DataSource = CMQParseContainer
    func willChangeSource(to:DataSource){
            self.dataSource = to as? IDViewDataSourceProtocol
            self.cellCreator = CellCreator.init(data: dataSource)
            self.tableView.reloadData()
            self.dataSource.willChangeState(to: .loading)
        
    }
    public func addNoContentView(){
        guard let contentView = noContentView else {return}
        guard noContentViewAdded == false else {return}
        var className:String?
        switch dataSource {
        case is CMQEventsFeedContainer:
            className = "Events"
        case is CMQNewsFeedContainer:
            className = "News"
        case is CMQMessageFeedContainer:
            className = "Messages"
        case is CMQScheduledMessageContainer:
            className = "Scheduled Messages"
        case is CMQResidentContainer:
            className = "Residents"
        default:
            className = nil
        }
        if let name = className{
            contentView.set(parseClassName: name)
        }
        contentView.frame = CGRect.init(origin: .zero, size: self.view.frame.size)
        self.tableView.tableFooterView = UIView()
        self.view.addSubview(contentView)
        noContentViewAdded = true
    }
    public func removeNoContentView(){
        guard let view = self.view.subviews.filter({$0 is CMQNoContentView}).first else {return}
        view.removeFromSuperview()
        noContentViewAdded = false
    }
}
extension IDTableViewController: IDDataSourceDelegateProtocol{
    public func didChange(dataSource: IDViewDataSourceProtocol, toState: IDDataSourceState) {
        self.updateUI(wState: toState)
    }
}
extension IDTableViewController: IDDataSourceStateChange{
    func updateUI(wState: IDDataSourceState) {
        print(wState)
        switch wState {
        case .loaded(let prev):
            switch prev{
            case .some(.loading):
                self.tableView.reloadData()
                self.handlePullToRefresh()
            case .some(.refreshing):
                if #available(iOS 10.0, *) {
                    self.tableView.refreshControl?.endRefreshing()
                } else {
                    // Fallback on earlier versions
                }
                let currentRows = self.tableView.numberOfRows(inSection: 0)
                let dataCount = self.dataSource.numOfRows + (dataSource.canPaginate ? 1 : 0)
                let difference = dataCount - currentRows
                if  difference != 0{
                    var indexPaths = [IndexPath]()
                    for digit in 0...difference-1 {
                        indexPaths.append(IndexPath.init(row: digit, section: 0))
                    }
                    self.tableView.insertRows(at: indexPaths, with: .top)
                    
                }
            case .some(.paginating):
                self.tableView.reloadData()
                self.handlePullToRefresh()
            case .some(.filtering):
                self.tableView.reloadData()
            default:
                break
            }
            if self.dataSource.numOfRows == 0{
                self.addNoContentView()
            }else{
                removeNoContentView()
            }
        case .loading:
            print("Loading Items")
            removePullToRefresh()
        case .refreshing:
            if #available(iOS 10.0, *) {
                self.tableView.refreshControl?.beginRefreshing()
            } else {
                // Fallback on earlier versions
            }
        case .paginating:
            removePullToRefresh()
        case .filtering:
            print("filtering Items")
        }
    }
}

