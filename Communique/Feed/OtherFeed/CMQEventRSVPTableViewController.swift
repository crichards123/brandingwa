//
//  CMQEventRSVPTableViewController.swift
//  Communique
//
//  Created by Andre White on 4/23/18.
//  Copyright © 2018 Communique, LLC. All rights reserved.
//

import UIKit

public class CMQEventRSVPTableViewController: UITableViewController, CMQDataSourceDelegate {
    public var dataSource:CMQDataSourceManager!
    private let photoViewTag = 300
    private let labelViewTag = 301
    override public func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.tableFooterView = UIView()
        dataSource.loadObjects()

    }

    public func objectsLoaded(_ error: Error?) {
        self.tableView.reloadData()
    }
    override public func numberOfSections(in tableView: UITableView) -> Int {
        return dataSource.numberOfSections
    }

    override public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.numberOfRows
    }
    override public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "userCell", for: indexPath)
        let user = dataSource.object(at: indexPath) as! CMQUser
        if let userPhoto = cell.viewWithTag(photoViewTag) as? CMQImageView {
            userPhoto.file = user.userPhoto
            userPhoto.loadInBackground()
        }
        if let userLabel = cell.viewWithTag(labelViewTag) as? UILabel{
            userLabel.text = user.fullName
        }
        return cell
    }
}
