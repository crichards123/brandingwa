//
//  CMQFeedTableViewController.swift
//  Communique
//
//  Created by Andre White on 1/29/18.
//  Copyright © 2018 Communique, LLC. All rights reserved.
//

import UIKit
import ParseUI

public class CMQFeedItemTableViewController: IDTableViewController {
    //var sortedMessages:[PFObject]?
    public var itemType:CMQFeedItem.ItemType!{
        switch dataSource {
        case is CMQNewsFeedContainer:
            return .News
        case is CMQEventsFeedContainer:
            return .Event
        case is CMQMessageFeedContainer:
            return .Message
        case is CMQScheduledMessageContainer:
            return .Message
        default:
            return nil
        }
    }
    private var respondingContentView:CMQObjectContentView?
    private var transitionHandler: CMQTransitionDelegate?
    private var messageDataSource:CMQMessageFeedContainer!
    private var scheduledMessageDataSource:CMQScheduledMessageContainer = CMQScheduledMessageContainer()
    private var showingScheduled:Bool = false
    @IBOutlet weak var scheduledMessageButton: UIBarButtonItem?
    
   @objc public func onRefresh(){
        self.dataSource.willChangeState(to: .refreshing)
    }
    
    public func handleTitle(){
        if self.itemType != .News{
            self.title = itemType.rawValue.appending("s")
        }else{
            self.title = itemType.rawValue
        }
        
    }
    public func handleScheduleButton(){
        if self.itemType != .Message || !CMQUser.current().isAdmin{
            self.navigationItem.rightBarButtonItem = nil
        }
    }
    public func setNavColor(){
        let barColor:UIColor? = self.itemType?.color
        if barColor != nil{
            self.navigationController?.navigationBar.barTintColor = barColor
        }
    }
    public func restoreNavColor(){
        self.navigationController?.navigationBar.barTintColor = .DarkColor
    }
    @objc public func didRespondToPackagePrompt(notification:Notification){
        guard let ocView = notification.object as? CMQObjectContentView else {return}
        self.respondingContentView = ocView
        self.showDeliveryConfirmation()
    }
    public func showDeliveryConfirmation(){
        guard let confimationController = CMQPackageDeliveryConfirmationViewController.instanceFromStoryBoard() else {return}
        self.prepareTransition()
        confimationController.transitioningDelegate = transitionHandler
        confimationController.modalPresentationStyle = .custom
        self.present(confimationController, animated: true, completion: nil)
    }
    public func prepareTransition(){
        let width = self.view.frame.size.width*0.75
        let height = self.view.frame.size.height*0.25
        let x = self.view.center.x-(width/2)
        let y = self.view.center.y-(height/2)
        self.transitionHandler = CMQTransitionDelegate(size: CGSize.init(width:width, height: height), origin: CGPoint.init(x: x, y: y))
        
    }
    public func addObservers(){
        NotificationCenter.default.addObserver(self, selector: #selector(didRespondToPackagePrompt(notification:)), name: .didRespondToPackagePrompt, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleAttendeesTapped(notification:)), name: .didTapAttendees, object: nil)
    }
    public func removeObservers(){
        NotificationCenter.default.removeObserver(self, name: .didRespondToPackagePrompt, object: nil)
        NotificationCenter.default.removeObserver(self, name: .didTapAttendees, object: nil)
    }

    // MARK: - TableView Delegate
    /*override public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath, object: PFObject?) -> PFTableViewCell? {
        guard  let object = object, let cell = tableView.dequeueReusableCell(withIdentifier: object.parseClassName, for: indexPath) as? CMQFeedTableViewCell else {return nil}
        cell.reset()
        cell.object = object
        cell.selectionStyle = .none
        return cell
    }*/
    override public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == self.dataSource.numOfRows{
            super.tableView(tableView, didSelectRowAt: indexPath)
        }
        else{
            self.performSegue(withIdentifier: "ViewItem", sender: tableView.cellForRow(at: indexPath))
        }
    }
    // MARK: - Exit Segues
    @IBAction func cancelMessage(sender:UIStoryboardSegue){
        
    }
    @IBAction func postObject(sender:UIStoryboardSegue){
        let _ = CMQAPIManager.sharedInstance().booleanCompletion(message: "Posting Object")
        if let _ = sender.source as? CMQComposeViewController{
            /*if let object = source.pendingObject{
                object.saveInBackground(block: completion)
            }*/
        }
    }
    @IBAction func packageConfirmation(sender:UIStoryboardSegue){
        guard let confirmationVC = sender.source as? CMQPackageDeliveryConfirmationViewController, let ocView = respondingContentView else {return}
        if confirmationVC.isConfirmed{
            ocView.acceptedDelivery()
        }
        //self.tableView.reloadRows(at: [index], with: .none)
    }
    @IBAction func scheduledButtonPressed(sender:UIBarButtonItem){
        guard self.itemType == .Message else {return}
        self.showingScheduled = !self.showingScheduled
        if self.showingScheduled{
            self.willChangeSource(to: scheduledMessageDataSource)
            
        }else{
            self.willChangeSource(to: messageDataSource)
        }
    }
    
    // MARK: - Life Cycle
    override public func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.tableFooterView = UIView.init()
        self.pullToRefreshEnabled = true
        handleTitle()
        handleScheduleButton()
        guard self.itemType == .Message else {return}
        self.messageDataSource = self.dataSource as? CMQMessageFeedContainer
    }
    override public func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        restoreNavColor()
        //removeObservers()
        if let _ = self.navigationController as? CMQNavigationController{
            //nav.removeButton()
            //nav.navigationBar.isTranslucent = true
        }
    }
    override public func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNavColor()
        //addObservers()
        if let _ = self.navigationController as? CMQNavigationController{
            //nav.addButtonToView()
        }
        guard self.itemType == .Message else {return}
        guard CMQAPIManager.sharedInstance().apartment != nil else {return}
        guard CMQAPIManager.sharedInstance().apartment.hasPackageDelivery else {return}
        self.tableView.reloadData()
        
    }
    // MARK: - Navigation
    @objc public func handleAttendeesTapped(notification:Notification){
        if let attendees = notification.object as? [String]{
            let query = CMQUser.query()!
            query.whereKey("objectId", containedIn: attendees)
            self.performSegue(withIdentifier: "RSVP", sender: query)
        }
    }
    override public func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let cell = sender as? CMQFeedTableViewCell, let path = self.tableView.indexPath(for: cell),let object = self.dataSource.object(at: path) as? PFObject, let dest = segue.destination as? CMQFeedContentViewController{
            dest.object = object
        }else if segue.identifier == "RSVP"{
            if let query = sender as? PFQuery<PFObject>,
            let dest = segue.destination as?
                CMQEventRSVPTableViewController{
                dest.dataSource = CMQDataSourceManager.init(query: query, delegate: dest)
            }
        }
    }

}
