//
//  CMQViewablePropertyCell.swift
//  Communique
//
//  Created by Andre White on 3/29/18.
//  Copyright © 2018 Communique, LLC. All rights reserved.
//

import UIKit
import ParseUI

public class CMQViewablePropertyCell: UITableViewCell {
    @IBOutlet weak var senderImageView: CMQImageView!
    @IBOutlet weak var colorView: UIView!
    @IBOutlet weak var sentTimeLabel: UILabel!
    @IBOutlet weak var mainLabel: UILabel!
    @IBOutlet weak var contentImageView: PFImageView!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var senderNameLabel: UILabel!
    @IBOutlet weak var attendeesLabel: UILabel!

    
    public func setView(propertyType:ItemProperty.PropertyType, content:Any?){
        switch propertyType {
        case .Sender:
            if let sender = content as? CMQUser{
                senderNameLabel?.text = sender.fullName
                senderImageView?.file = sender.userPhoto
                senderImageView.loadInBackground()
            }else{
                senderNameLabel?.text = "Staff Member"
            }
        case .Image:
            guard let photo = content as? PFFile else {return}
            contentImageView?.file = photo
            contentImageView?.loadInBackground()
        case .TimeSent:
            guard let sentTime = content as? Date else {return}
            sentTimeLabel?.text = sentTime.feedDateView
        case .EventDetails:
            guard let dict = content as? [String:Any?] else {return}
            if let date = dict["Date"] as? Date{
                timeLabel?.text = date.timeOnlyString
                dateLabel?.text = date.dateOnlyString
            }
            if let location = dict["Location"] as? String{
                 locationLabel?.text = location
            }
        case .Attendees:
            guard let string = content as? String else {return}
            attendeesLabel?.text = string
        default:
            if let text = content as? String{
                mainLabel?.text = text
            }
        }
        
    }
    public func setViews(propertTypes:[ItemProperty.PropertyType], content:[Any?]){
        
    }
    override public func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override public func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
