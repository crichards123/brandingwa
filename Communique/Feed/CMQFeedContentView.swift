//
//  CMQFeedContentView.swift
//  Communique
//
//  Created by Andre White on 1/23/18.
//  Copyright © 2018 Communique, LLC. All rights reserved.
//

import UIKit
import ParseUI
//Parent View with Sender Image, Sender Name and TimeSent Labels, and ColorView if any. This also has a CMQObjectContentView as a subView which will display object level properties. This View ultimately takes care of most of the commom properties for the feedItem. 
public class CMQFeedContentView: UIView {
    @IBOutlet weak var objectContentView: CMQObjectContentView!
    @IBOutlet weak var senderLabel: UILabel!
    @IBOutlet weak var sentTimeLabel: UILabel!
    @IBOutlet weak var senderImageView: PFImageView!
    @IBOutlet weak var colorView: UIView!
    private var userIcon:UIImage?{
        return UIImage.init(named: CMQUserIconPlaceHolderImageName, in: CMQBundle!, compatibleWith: nil)
    }
    public var sender :CMQUser!{
        didSet{
            if let user = sender{
                guard let label = senderLabel,let view = senderImageView else {return}
                label.text = user.fullName.isEmpty ? "Staff Member" : user.fullName
                view.file = user.userPhoto
                view.loadInBackground()
            }else{
                guard let label = senderLabel,let view = senderImageView else {return}
                label.text = "Staff Member"
                view.image = userIcon
            }
        }
    }
    public var object: PFObject!{
        didSet{
            guard let object = object, let view = objectContentView else {return}
            if let label = sentTimeLabel {
                label.text = object.sortDate.feedDateView
            }
            switch object.parseClassName {
            case "Message":
                if let view = colorView{
                    view.backgroundColor = UIColor.MessageColor
                }
                if let message = object as? CMQMessage{
                    sender = message.userWhoPosted 
                }
            case "Event":
                if let view = colorView{
                    view.backgroundColor = UIColor.EventColor
                }
                if let event = object as? CMQEvent{
                    sender = event.userWhoPosted as? CMQUser
                }
            case "News":
                if let view = colorView{
                    view.backgroundColor = UIColor.NewsColor
                }
                if let news = object as? CMQNewsArticle{
                    sender = news.userWhoPosted as? CMQUser
                }
            default:
                if let view = colorView{
                    view.backgroundColor = .white
                }
            }
            //sender = object.object(forKey: "userWhoPosted") as! CMQUser
            view.object = object
        }
    }
    public func reset(){
        self.object = nil
        self.sender = nil

        if let label = sentTimeLabel{
            label.text = ""
        }
        if let view = colorView{
            view.backgroundColor = .white
        }
        if let label = senderLabel{
            label.text = ""
        }
        if let view = senderImageView{
            view.file = nil
            view.image = userIcon
        }
        if let view = objectContentView{
            view.reset()
        }
    }
}
