//
//  CMQNoContentView.swift
//  Communique
//
//  Created by Andre White on 3/12/18.
//  Copyright © 2018 Communique, LLC. All rights reserved.
//

import UIKit

class CMQNoContentView: UIView {
    private var noContentMessage = "No ( )"
    @IBOutlet weak var noContentLabel:UILabel!
    @IBOutlet weak var noContentImageView:UIImageView!
    
    public class func instanceFromNib()->CMQNoContentView?{
        guard let nib = loadNib(name: CMQNoContentNibName) else {return nil}
        return nib.instantiate(withOwner: nil, options: nil).first as? CMQNoContentView
    }
    public func setMessage(message:String){
        noContentLabel?.text = self.noContentMessage.replacingOccurrences(of: "( )", with: message)
    }
    
    public func set(parseClassName:String){
        setMessage(message: parseClassName)
        switch parseClassName {
        case "Events":
            noContentImageView.tintColor = .EventColor
            noContentImageView.image = UIImage.init(named: CMQEventLogoImageName, in: CMQBundle!, compatibleWith: nil)
        case "News":
            noContentImageView.tintColor = .NewsColor
            noContentImageView.image = UIImage.init(named: CMQNewsLogoImageName, in: CMQBundle!, compatibleWith: nil)
        case "Message":
            noContentImageView.tintColor = .MessageColor
            noContentImageView.image = UIImage.init(named: CMQMessageLogoImageName, in: CMQBundle!, compatibleWith: nil)
        case "Scheduled Messages":
            noContentImageView.tintColor = .MessageColor
            noContentImageView.image = UIImage.init(named:CMQScheduledMessageLogoImageName , in: CMQBundle!, compatibleWith: nil)
        default:
            noContentImageView.tintColor = .DarkColor
            noContentImageView.image = nil
        }
    }
}
