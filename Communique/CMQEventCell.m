//
//  CMQEventCell.m
//  Communique
//
//  Created by Colby Melvin on 2/24/17.
//  Copyright © 2017 Communique, LLC. All rights reserved.
//

#import "CMQEventCell.h"

@implementation CMQEventCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    // Initialization code
    [self configureUI];
}

/**
 *  configures the UI on initial loading
 */
-(void)configureUI{
    //TTTAttributedLabel setup
    [self.eventContentLabel setVerticalAlignment:TTTAttributedLabelVerticalAlignmentTop];
    [self.eventContentLabel setTextColor:[UIColor blackColor]];
    [self.eventContentLabel setLineBreakMode:NSLineBreakByWordWrapping];
    [self.eventContentLabel setNumberOfLines:0];

    //fonts
    [self.nameLabel setFont:[UIFont fontWithName:kCMQFontGeometriaLight size:17]];
    [self.eventContentLabel setFont:[UIFont fontWithName:kCMQFontGeometriaLight size:14]];
    [self.eventTitleLabel setFont:[UIFont fontWithName:kCMQFontGeometriaBold size:14]];
    [self.eventDateLabel setFont:[UIFont fontWithName:kCMQFontGeometriaLight size:10]];
    [self.imageLoadingLabel setFont:[UIFont fontWithName:kCMQFontGeometriaLight size:12]];
    [self.eventDateStartButton.titleLabel setFont:[UIFont fontWithName:kCMQFontGeometriaLight size:12]];
    [self.eventLocationLabel setFont:[UIFont fontWithName:kCMQFontGeometriaLight size:12]];
    [self.eventAttendeesButton.titleLabel setFont:[UIFont fontWithName:kCMQFontGeometriaLight size:12]];
    
    self.userPhoto.layer.cornerRadius = 20.0f;
    self.userPhoto.layer.borderWidth = .5f;
    self.userPhoto.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.userPhoto.layer.masksToBounds = YES;
}

@end
