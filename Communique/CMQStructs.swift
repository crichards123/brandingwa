//
//  CMQStructs.swift
//  Communique
//
//  Created by Andre White on 2/21/18.
//  Copyright © 2018 Communique, LLC. All rights reserved.
//

import Foundation
import ParseUI

public struct ItemProperty{
    init(propertyType:PropertyType) {
        self.propertyType = propertyType
        if  propertyType == .Title || propertyType == .Content{
            isRequired = true
        }else{
            isRequired = false
        }
        if propertyType == .Urgent{
            isSet = true
        }else{
            isSet = false
        }
    }
    
    public enum PropertyType:String{
        case Title = "Title"
        case Content = "Content"
        case Url = "Url"
        case Location = "Location"
        case Date = "Date"
        case Image = "Image"
        case Urgent = "Urgent"
        case Recipients = "Recipients"
        case TimeSent = "TimeSent"
        case Attendees = "Attendees"
        case Sender = "Sender"
        case Package = "Package"
        case Color = "Color"
        case EventDetails = "EventDetails"
    }
    public var propertyType:PropertyType
    public var isRequired: Bool
    public var isSet:Bool
    static func viewableProperties()->[ItemProperty]{
        return [ItemProperty.init(propertyType: .Title),
                ItemProperty.init(propertyType: .Content),
                ItemProperty.init(propertyType: .Image),
                ItemProperty.init(propertyType: .TimeSent),
                ItemProperty.init(propertyType: .Sender)]
    }
    static func viewableProperties(itemType:PFObject.ItemType)->[ItemProperty]{
        var props = ItemProperty.viewableProperties()
        switch itemType {
        case .Event:
            props.append(ItemProperty.init(propertyType: .EventDetails))
            props.append(ItemProperty.init(propertyType: .Attendees))
        case .News:
            props.append(ItemProperty.init(propertyType: .Url))
        case .Message:
            props.append(ItemProperty.init(propertyType: .Package))
        }
        return props
    }
    static func composableProperties()->[ItemProperty]{
        return [ItemProperty.init(propertyType: .Title),
                ItemProperty.init(propertyType: .Content),
                ItemProperty.init(propertyType: .Image)]
    }
    static func composableProperties(itemType:PFObject.ItemType)->[ItemProperty]{
        var props = ItemProperty.composableProperties()
        switch itemType {
        case .Event:
            props.append(ItemProperty.init(propertyType: .Date))
            props.append(ItemProperty.init(propertyType: .Location))
        case .News:
            props.append(ItemProperty.init(propertyType: .Url))
        case .Message:
            props.append(ItemProperty.init(propertyType: .Urgent))
            props.append(ItemProperty.init(propertyType: .Date))
        }
        return props
    }
    
}
protocol News{
    var url:String {get set}
}
protocol Message{
    var recipients:[CMQApartment]? {get set}
    var isUrgent:Bool {get set}
    var packagePin:NSNumber? {get}
    var deliveryStatus:String? {get}
    var isPackageMessage:Bool {get}
    func shouldDeliver(delivering:Bool, completion:@escaping (Error?)->Void)
}
protocol Event{
    var location:String {get set}
    var time:String? {get}
    var dateString:String? {get}
    var attendingString:String {get}
    func shouldAttend(attending:Bool, completion:@escaping (Error?)->Void)
}
protocol FeedItemProtocol:Postable{
    var photo:PFFile? {get set}
    var content:String {get set}
    var title:String {get set}
    var date:Date? {get set}
    var createdAt:String {get}
    func photoDownloaded(setting view:PFImageView)->PFImageViewImageResultBlock
}
protocol Postable{
    var isPostable:Bool {get}
    func post(completion:@escaping PFBooleanResultBlock)
}
public enum FeedItem:FeedItemProtocol{
    case Message(object:CMQMessage)
    case Event(object:CMQEvent)
    case News(object:CMQNewsArticle)
    
    init?(object:PFObject){
        if let message = object as? CMQMessage{
            self = .Message(object: message)
        }
        else if let event = object as? CMQEvent{
            self = .Event(object: event)
        }
        else if let news = object as? CMQNewsArticle{
            self = .News(object: news)
        }else{
            return nil
        }
    }
    var object:PFObject{
        switch self{
        case .Message(let object):
            return object
        case .Event(let object):
            return object
        case .News(let object):
            return object
        }
    }
    var createdAt: String{
        switch self{
        case .Message(let object):
            if let string = object.scheduleTime, let scheduledAt = Date.fromParse(string: string) {
               return scheduledAt.feedDateView
            }else{
                return object.createdAt!.feedDateView
            }
        case .Event(let object):
            return object.createdAt!.feedDateView
        case .News(let object):
            return object.createdAt!.feedDateView
        }
    }
    var date: Date?{
        get{
            switch self{
            case .Event(let object):
                return object.eventDate
            default:
                return nil
            }
        }
        set{
            switch self{
            case .Message(let object):
                guard newValue != nil else {return}
                object.scheduleTime = newValue!.toParse
                object.scheduleMessage = true
                object.scheduleSent = false
            case .Event(let object):
                object.eventDate = newValue
                if newValue != nil{
                    object.utcOffset = TimeZone.current.secondsFromGMT(for: newValue!)/60
                }
            case .News:
                return
            }
        }
    }
    var title:String{
        get{
            switch self{
            case .Message(let object):
                return object.messageTitle
            case .Event(let object):
                return object.eventTitle
            case .News(let object):
                return object.newsTitle
            }
        }
        set{
            switch self{
            case .Message(let object):
                object.messageTitle = newValue
            case .Event(let object):
                object.eventTitle = newValue
            case .News(let object):
                object.newsTitle = newValue
            }
        }
    }
    var content:String{
        get{
            switch self{
            case .Message(let object):
                return object.messageContent
            case .Event(let object):
                return object.eventContent
            case .News(let object):
                return object.newsContent
            }
        }
        set{
            switch self{
            case .Message(let object):
                object.messageContent = newValue
            case .Event(let object):
                object.eventContent = newValue
            case .News(let object):
                object.newsContent = newValue
            }
        }
    }
    var photo: PFFile?{
        get{
            switch self{
            case .Message(let object):
                return object.messagePhoto
            case .Event(let object):
                return object.eventPhoto
            case .News(let object):
                return object.newsPhoto
            }
        }
        set{
            switch self{
            case .Message(let object):
                object.messagePhoto = newValue
            case .Event(let object):
                object.eventPhoto = newValue
            case .News(let object):
                object.newsPhoto = newValue
            }
        }
    }
    var isPostable: Bool{
        guard !title.isEmpty, !content.isEmpty else {return false}
        switch self{
        case .Event:
            guard date != nil, !location.isEmpty else {return false}
            return true
        default:
            return true
        }
    }
    func photoDownloaded(setting view: PFImageView) -> PFImageViewImageResultBlock {
        return { (rImage, error) in
            if let error = error {
                handle(error: error, message:  "Error Loading image for \(self.object)", shouldDisplay: false)
                view.image = errorLoadingImage
            }
            else if let image = rImage{
                view.image = image
            }
        }
    }
    func post(completion: @escaping PFBooleanResultBlock) {
        self.object.saveInBackground(block: completion)
    }
}
extension FeedItem:Message{
    func shouldDeliver(delivering: Bool, completion: @escaping (Error?) -> Void) {
        switch self {
        case .Message(let object):
            CMQMessageDataSourceManager.sharedInstance().setDelivery(message: object, response: delivering, completion: completion)
        default:
            completion(nil)
        }
    }
    
    var isUrgent: Bool {
        get {
            switch self{
            case .Message(let message):
                return message.isCritical
            default:
                return false
            }
        }
        set {
            switch self{
            case .Message(let message):
                message.isCritical = newValue
            default:
                return
            }
        }
    }
    
    var recipients: [CMQApartment]? {
        get {
            switch self{
            case .Message(let message):
                return message.recipients as? [CMQApartment]
            default:
                return nil
            }
        }
        set {
            switch self{
            case .Message(let message):
                if newValue == nil{
                    message.isAnnouncement = false
                }else{
                    message.isAnnouncement = true
                }
                message.recipients = newValue
            default:
                return
            }
        }
    }
    
    var packagePin: NSNumber? {
        switch self{
        case .Message(let message):
            guard let package = message.package else {return nil}
            return package.pinCode
        default:
            return nil
        }
    }
    
    var deliveryStatus: String? {
        switch self{
        case .Message(let object):
            if isPackageMessage{
                if let status = object.deliveryStatus{
                    return status.uppercased()
                }else{
                    return "initial"
                }
            }else{
                return nil
            }
        default:
            return nil
        }
    }
    
    var isPackageMessage: Bool {
        switch self{
        case .Message(let object):
            guard let apartment = CMQAPIManager.sharedInstance().apartment, apartment.hasPackageDelivery else {return false}
            if object.packageNoti == true && object.deliveryStatus != "DELIVERED OR PICKED UP" && object.messageContent.contains("You have a package to pick up at the office.") {
                return true
            }else{
                return false
            }
        default:
            return false
        }
    }
}
extension FeedItem:Event{
    
    
    func shouldAttend(attending: Bool, completion: @escaping (Error?) -> Void) {
        switch self{
        case .Event(let event):
            if attending{
                event.addUniqueObject(CMQUser.current().objectId!, forKey: kParseEventAttendeesKey)
            }else{
                event.remove(CMQUser.current().objectId!, forKey: kParseEventAttendeesKey)
            }
            event.saveInBackground { (finised, error) in
                if let error = error{
                    handle(error: error, message: "Updating atteding event", shouldDisplay: false)
                }else{
                    if finised{
                        completion(error)
                    }
                }
            }
        default:
            completion(nil)
        }
    }
    
    var dateString: String? {
        return self.date?.eventDateView
    }
    var time:String?{
        return self.date?.timeOnlyString
    }
    var location: String {
        get {
            switch self{
            case .Event(let object):
                return object.eventLocation
            default:
                return ""
            }
        }
        set {
            switch self{
            case .Event(let object):
                return object.eventLocation = newValue
            default:
                return
            }
        }
    }
    
    var attendingString: String {
        switch self{
        case .Event(let event):
            guard let attendees = event.eventAttendees else {return "Be the first to RSVP"}
            let attending: Bool = CMQUser.current()?.isAttending(event: event) ??  false
            switch attendees.count{
            case 0:
                return "Be the first to RSVP"
            case 1:
                return  attending ? "You are attending" : "1 attending"
            case let x where x > 1 :
                return attending ? "You and \(attendees.count-1) other members are attending" : "\(attendees.count) members are attending"
            default:
                print("Unknown number of attendees")
                return ""
            }
        default:
            return ""
        }
    }
}
extension CMQUser{
    func isAttending(event:CMQEvent)->Bool{
        if let attendees = event.eventAttendees as? [String] {
            if let _ = attendees.filter({ (attendeeID) -> Bool in
                attendeeID == self.objectId
            }).first{
                return true
            }else{
                return false
            }
        }else{
            return false
        }
    }
}
extension FeedItem:News{
    var url: String {
        get {
            switch self{
            case .News(let object):
                return object.articleURL
            default:
                return ""
            }
        }
        set {
            switch self{
            case .News(let object):
                return object.articleURL = newValue
            default:
                return
            }
        }
    }
    
    
}
public struct FeedItemStruct{
    public enum itemType{
        case Message
        case Event
        case News
        
    }
    public var object: PFObject
    public var isPostable:Bool{
        guard let title = self.title, let content = self.content else {return false}
        var isReady = false
        let isntEmpty = !title.isEmpty && !content.isEmpty
        if isntEmpty{
            switch object.parseClassName{
            case "Event":
                guard let _ = self.date, let location = self.location else {return false}
                isReady = !location.isEmpty
            case "News":
                //guard let urlString = self.url, let url = URL.init(string: urlString)else {return false}
                isReady = true//UIApplication.shared.canOpenURL(url)
            case "Message":
                isReady = true
            default:
                return false
            }
        }
        return isntEmpty && isReady
    }
    public var packageMessage: Bool?{
        guard let message = object as? CMQMessage,  let apartment = CMQAPIManager.sharedInstance().apartment, apartment.hasPackageDelivery else {return nil}
        //return message.messageContent.contains("You have a package to pick up at the office.")
        if message.packageNoti == true && message.deliveryStatus != "DELIVERED OR PICKED UP" && message.messageContent.contains("You have a package to pick up at the office.") {
            return true
        }else{
            return false
        }

    }
    public var deliveryStatus:String?{
        guard let isPackageMessage = packageMessage, let message = object as? CMQMessage else  {return nil}
        if isPackageMessage{
            if let status = message.deliveryStatus{
                return status.uppercased()
            }else{
                return "initial"
            }
        }
        return nil
    }
    public var attendingEvent: Bool?{
        guard let event = object as? CMQEvent else {return nil}
        if let attendees = event.eventAttendees as? [String] {
            if let _ = attendees.filter({ (attendeeID) -> Bool in
                attendeeID == CMQUser.current().objectId
            }).first{
                return true
            }else{
                return false
            }
        }else{
            return false
        }
    }
    public var attendingString:String?{
        guard let event = object as? CMQEvent else {return nil}
        guard let attendees = event.eventAttendees else {return "Be the first to RSVP"}
        let amIAttending = { ()-> Bool in
            if let attending = self.attendingEvent{
                return attending
            }else{
                return false
            }
        }
        let attending: Bool = amIAttending()
        switch attendees.count{
        case 0:
            return "Be the first to RSVP"
        case 1:
            if attending{
                return "You are attending"
            }else{
                return "1 attending"
            }
        case let x where x > 1 :
            if attending{
                return "You and \(attendees.count-1) other members are attending"
            }else{
                return "\(attendees.count) members are attending"
            }
        default:
            print("Unknown number of attendees")
            return nil
        }
    }
    public var createdAt:String?{
        guard let createdAt = object.createdAt else{return nil}
        switch object.parseClassName {
        case "Message":
            guard let message = object as? CMQMessage else {return nil}
            if let string = message.scheduleTime, let scheduledAt = Date.fromParse(string: string) {
                return scheduledAt.feedDateView
            }else{
                return createdAt.feedDateView
            }
        default:
            return createdAt.feedDateView
        }
        
    }
    public func  set(isUrgent:Bool){
        guard let message = object as? CMQMessage else {return}
        message.isCritical = isUrgent
    }
    
    public func  set(url:String){
        guard let news = object as? CMQNewsArticle else {return}
        news.articleURL = url
        
    }
    public var url:String?{
        guard let news = object as? CMQNewsArticle, let url = news.articleURL else {return nil}
        return url
    }
    public var date: String?{
        guard let event = object as? CMQEvent, let eventDate = event.eventDate else {return nil}
        return eventDate.eventDateView
    }
    public func  set(date:Date){
        switch object.parseClassName {
        case "Message":
            guard let message = object as? CMQMessage else {return}
            message.scheduleTime = date.toParse
            message.scheduleMessage = true
            message.scheduleSent = false
        case "Event":
            guard let event = object as? CMQEvent else {return}
            event.eventDate = date
            event.utcOffset = TimeZone.current.secondsFromGMT(for: date)/60
        default:
            return
        }
    }
    public var time: String?{
        guard let event = object as? CMQEvent, let eventDate = event.eventDate else {return nil}
        return eventDate.timeOnlyString
    }
    public var location:String?{
        guard let event = object as? CMQEvent else {return nil}
        return event.eventLocation
    }
    public func  set(location:String){
        guard let event = object as? CMQEvent else {return}
        event.eventLocation = location
    }
    public func  set(title:String){
        switch object.parseClassName {
        case "Message":
            guard let message = object as? CMQMessage else {return}
            message.messageTitle = title
        case "Event":
            guard let event = object as? CMQEvent else {return}
            event.eventTitle = title
        case "News":
            guard let news = object as? CMQNewsArticle else {return }
            news.newsTitle = title
        default:
            return
        }
    }
    public var title:String?{
        switch object.parseClassName {
        case "Message":
            guard let message = object as? CMQMessage else {return nil}
            return message.messageTitle
        case "Event":
            guard let event = object as? CMQEvent else {return nil}
            return event.eventTitle
        case "News":
            guard let news = object as? CMQNewsArticle else {return nil}
            return news.newsTitle
        default:
            return nil
        }
    }
    public func  set(content:String){
        switch object.parseClassName {
        case "Message":
            guard let message = object as? CMQMessage else {return}
            message.messageContent = content
        case "Event":
            guard let event = object as? CMQEvent else {return }
            event.eventContent = content
        case "News":
            guard let news = object as? CMQNewsArticle else {return}
            news.newsContent = content
        default:
            return
        }
    }
    public var content:String?{
        switch object.parseClassName {
        case "Message":
            guard let message = object as? CMQMessage else {return nil}
            return message.messageContent
        case "Event":
            guard let event = object as? CMQEvent else {return nil}
            return event.eventContent
        case "News":
            guard let news = object as? CMQNewsArticle else {return nil}
            return news.newsContent
        default:
            return nil
        }
    }
    public var photo:PFFile?{
        switch object.parseClassName {
        case "Message":
            guard let message = object as? CMQMessage else {return nil}
            return message.messagePhoto
        case "Event":
            guard let event = object as? CMQEvent else {return nil}
            return event.eventPhoto
        case "News":
            guard let news = object as? CMQNewsArticle else {return nil}
            return news.newsPhoto
        default:
            return nil
        }
    }
    public func  set(photo:UIImage){
        switch object.parseClassName {
        case "Message":
            guard let message = object as? CMQMessage else {return}
            message.messagePhoto = photo.toParse()
        case "Event":
            guard let event = object as? CMQEvent else {return}
            event.eventPhoto = photo.toParse()
        case "News":
            guard let news = object as? CMQNewsArticle else {return}
            news.newsPhoto = photo.toParse()
        default:
            return
        }
    }
    public var packagePin:NSNumber?{
        guard let message = object as? CMQMessage, let package = message.package else {return nil}
        return package.pinCode
        
    }
    
    
    
    
    public func  set(attend:Bool, completion:@escaping (Error?)->Void){
        guard let event = object as? CMQEvent else {return}
        if attend{
            event.addUniqueObject(CMQUser.current().objectId!, forKey: kParseEventAttendeesKey)
        }else{
            event.remove(CMQUser.current().objectId!, forKey: kParseEventAttendeesKey)
        }
        event.saveInBackground { (finised, error) in
            if let error = error{
                handle(error: error, message: "Updating atteding event", shouldDisplay: false)
            }else{
                if finised{
                    completion(error)
                }
            }
        }
    }
    public func  set(delivery:Bool?, completion:@escaping (Error?)->Void){
        guard let message = object as? CMQMessage else {return}
        CMQMessageDataSourceManager.sharedInstance().setDelivery(message: message, response: delivery, completion: completion)
    }
    
    public func  set(recipients:[CMQApartment]){
        guard let message = object as? CMQMessage else {return}
        message.recipients = recipients
        message.isAnnouncement = true
    }
    public func  photoCompletion(view:PFImageView) ->PFImageViewImageResultBlock?{
        return { (rImage, error) in
            if let error = error {
                handle(error: error, message:  "Error Loading image for \(self.object)", shouldDisplay: false)
                view.image = errorLoadingImage
            }
            else if let image = rImage{
                view.image = image
                //view.image = UIImage.imageWithRoundedCorners(image: image, radius: 3.0)
                //view.image = UIImage.resize(image, newWidth: view.frame.size.width, newHeight: view.frame.size.height)
            }
        }
    }
    public func  post(completion:@escaping PFBooleanResultBlock){
        object.saveInBackground(block: completion)
    }
}
public struct MenuItem{
    var name:String
    var image:UIImage?
    var priority:Int
    var segueID:String?{
        switch name {
        case "Epproach Support":
            return "Support"
        case "Messages":
            return "ViewFeed"
        case "Events":
            return "ViewFeed"
        case "News":
            return "ViewFeed"
        case "Packages":
            return "Packages"
        case "Account":
            return "Account"
        case "Community Info":
            return "Community Info"
        case "Log Out":
            return "LogOut"
        case "Switch Communities":
            return "Communities"
        case "Smart Home":
            return CMQAPIManager.sharedInstance().apartment.legacySmartHome || CMQUser.current().isAdmin ? "LegacySmartHome":"SmartHome"
        case "Pay Lease":
            return "PayLease"
        case "Pay Rent":
            return "PayRent"
        case "Work Order":
            return "WorkOrder"
        case "More":
            return "More"
        default:
            return "Custom"
        }
    }
}


