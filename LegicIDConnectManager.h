//
//  IDConnectManager.h
//  legic-sdk
//
//  Created by admin on 09/09/2014.
//  Copyright (c) 2014 Legic. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Legic_connection_delegate.h"
#import "Legic_technology_name.h"
#import "Legic_register_wallet_delegate.h"
#import "LegicStorageManager.h"
#import "Legic_confirmation_method.h"
#import "Legic_se_types.h"

/// Class to manage all interactions with the IDConnect SDK.
/**
 * This class manages the interactions between the Wallet and the SDK. An instance should be created using LegicIDConnectFactory and retained until it is no longer needed, at which point finalise should be called.
 *
 * @author Ian Stewart
 */
@interface LegicIDConnectManager : NSObject

/// Enum to define errors returned by the LegicIDConnectManager.
enum IDConnectError
{
    IDConnectErrorWalletApplicationNotFound, /**< Error returned if a wallet application matching the provided ID is not found in the persistent store. */
    IDConnectErrorStorageManager,            /**< Error returned if the storage manager encounters an error when loading or saving data. */
    IDConnectErrorCardTypeNotSupported,      /**< Error returned if the requested operation involves an unsupported card type. */
    IDConnectErrorIncorrectPushMessageFormat,/**< Error returned if the push message received was not in the correct format. */
    IDConnectErrorIDCMessageFormat           /**< Error returned if the IDC message received was not in the correct format. */
};

/// Notification name that all screens displaying cards should register for. In the event of a server initiated update to the wallet applications this notification will be broadcast.
FOUNDATION_EXPORT NSString* const NOTIFICATION_CARDS_UPDATED;
/// Notification that screen calling synchronize should register for. Will be broadcast when the initial synchronize has been completed.
FOUNDATION_EXPORT NSString* const NOTIFICATION_INITIAL_SYNCHRONIZE_COMPLETE;
/// Notification that screen calling synchronize should register for. Will be broadcast when the final synchronize has been completed.
FOUNDATION_EXPORT NSString* const NOTIFICATION_FINAL_SYNCHRONIZE_COMPLETE;
/// Notification that screen calling synchronize should register for. Will be broadcast if the synchronize has failed.
FOUNDATION_EXPORT NSString* const NOTIFICATION_SYNCHRONIZE_FAILED;
/// Notification that persistent store data has become corrupted and must be cleared. Application should return user to registration screen in order to re-register with the server.
FOUNDATION_EXPORT NSString* const NOTIFICATION_STORE_DATA_CORRUPTED;

/**
 * Closes any open connection with the Secure Element and destroys the current LegicIDConnectManager instance. This method must be called when the Wallet is closing, or before any attempt to create a new instance in the LegicIDConnectFactory.
 */
-(void)finalise;


#pragma mark Card Functions

/**
 * Activates the LegicWalletApplication matching the supplied walletAppId and Qualifier. This application will be available for interactions.
 *
 * @param walletAppId The ID of the wallet application to be activated.
 * @param qualifier   The qualifier of the wallet application to be activated.
 *
 * @return NSError Error describing the outcome (error == nil is success).
 */
-(NSError*)activateCard:(NSString*)walletAppId Qualifier:(NSNumber*)qualifier;

/**
 * Deactivates the LegicWalletApplication matching the supplied walletAppId and Qualifier. This application will no longer be available for interactions.
 *
 * @param walletAppId The ID of the wallet application to be deactivated.
 * @param qualifier   The qualifier of the wallet application to be activated.
 *
 * @return NSError Error describing the outcome (error == nil is success).
 */
-(NSError*)deactivateCard:(NSString*)walletAppId Qualifier:(NSNumber*)qualifier;

/**
 * Deactivates all the LegicWalletApplications in the wallet, making them unavailable for interactions.
 *
 * @return NSError Error describing the outcome (error == nil is success).
 */
-(NSError*)deactivateAllCards;

/**
 * Retrieves all the LegicWalletApplications from the persistent store.
 *
 * @return NSArray* Array containing LegicWalletApplication instances.
 */
-(NSArray*)getAllCards;


#pragma mark Server Functions

/**
 * Registers the Wallet with the Wallet Server. This initiates the registration process, which should then be completed using the LegicIDConnectManager::completeRegistration method, once the required token has been received. The manner of delivery for the token is dependent on the confirmationMethod (see below).
 *
 * @param delegate The delegate to return the result of the connection.
 * @param info     The information to use to register the wallet with the server.
 * @param publicSeId The identifier for the confirmation method, will be different depending on method used:
 *                    EMAIL - the email address to send the token to (pure BLE wallets only)
 *                    PUSH  - the APN token
 *                    SMS   - the phone number to which the SMS message containing the token should be sent (NFC wallets).
 *                    NONE  - only usable if the server has been prepare properly, will return token in server call.
 * @param confirmationMethod The confirmation method to use to send the token necessary to complete the registration (see publicSeId).
 *
 * @throws RetrieveCUDException Thrown if the SDK is unable to retrieve the CUD from the SD card for authentication (Only applicable in wallets using SDCard profile ID). User should ensure they have attached an iCaisse with an SD card inserted.
 * @throws ServerParamException Thrown if a critical server parameter is not set (URL, Username or Password).
 * @throws IllegalDelegateException Thrown if the delegate provided does not properly implement the LegicRegisterWalletDelegate protocol.
 */
-(void)registerWallet:(id<LegicRegisterWalletDelegate>)delegate
                 Info:(NSArray*)info
           PublicSEID:(NSString*)publicSeId
   ConfirmationMethod:(LegicConfirmationMethods*)confirmationMethod;

/**
 * Completes the registration process with the Wallet Server, using the appropriate walletToken. The manner in which the token is retrieved is dependent on what confirmation method was used in LegicIDConnectManager::registerWallet.
 *
 * @param delegate     The delegate to return the result of the Register Wallet connection.
 * @param walletToken  The wallet token provided by SMS for verification.
 *
 * @throws ServerParamException Thrown if a critical server parameter is not set (URL, Username or Password).
 * @throws IllegalDelegateException Thrown if the delegate provided does not properly implement the LegicRegisterWalletDelegate protocol.
 */
-(void)completeRegistration:(id<LegicRegisterWalletDelegate>)delegate
                WalletToken:(NSString*)walletToken;

/**
 * Connects to the Wallet Server and requests that the LegicWalletApplication matching the supplied walletAppId be added to the Wallet. This should be used to deploy available LegicWalletApplications.
 *
 * @param delegate     The delegate to return the result of the connection.
 * @param walletAppId  The identifier of the wallet application to be added.
 *
 * @throws ServerParamException Thrown if a critical server parameter is not set (URL, Username or Password).
 * @throws IllegalDelegateException Thrown if the delegate provided does not properly implement the LegicConnectionDelegate protocol.
 */
-(void)addApp:(id<LegicConnectionDelegate>)delegate
  WalletAppId:(NSString*)walletAppId;

/**
 * Connects to the Wallet Server and requests that the LegicWalletApplication matching the supplied walletAppId be added to the Wallet. This should be used to deploy available LegicWalletApplications.
 *
 * @param delegate     The delegate to return the result of the connection.
 * @param walletAppId  The identifier of the wallet application to be added.
 * @param parameters   Additional name/value parameters to be sent to the server. These should be of type NSString.
 *
 * @throws ServerParamException Thrown if a critical server parameter is not set (URL, Username or Password) or if additional parameters are not of type NSString.
 * @throws IllegalDelegateException Thrown if the delegate provided does not properly implement the LegicConnectionDelegate protocol.
 */
-(void)addApp:(id<LegicConnectionDelegate>)delegate
  WalletAppId:(NSString*)walletAppId
   Parameters:(NSDictionary*)parameters;

/**
 * Connects to the Wallet Server and requests that the LegicWalletApplication matching the supplied walletAppId be removed from the Wallet. This should be used to remove deployed LegicWalletApplications.
 *
 * @param delegate     The delegate to return the result of the connection.
 * @param walletAppId  The identifier of the wallet application to be removed.
 * @param qualifier    The qualifier of the wallet application to be removed.
 *
 * @throws ServerParamException Thrown if a critical server parameter is not set (URL, Username or Password).
 * @throws IllegalDelegateException Thrown if the delegate provided does not properly implement the LegicConnectionDelegate protocol.
 */
-(void)removeApp:(id<LegicConnectionDelegate>)delegate
     WalletAppId:(NSString*)walletAppId
       Qualifier:(NSNumber *)qualifier;

/**
 * Connects to the Wallet Server and requests that the Wallet be unregistered from the Server. Note that on successfully unregistering from the server the persistent store will be cleared of all data. If you are using Push Messaging you will need to retrieve the APN token and provide it to the SDK using setApplePushNotificationToken.
 *
 * @param delegate     The delegate to return the result of the connection.
 *
 * @throws ServerParamException Thrown if a critical server parameter is not set (URL, Username or Password).
 * @throws IllegalDelegateException Thrown if the delegate provided does not properly implement the LegicConnectionDelegate protocol.
 */
-(void)unregisterWallet:(id<LegicConnectionDelegate>)delegate;

/**
 * Synchronises the Wallet with the Wallet server and performs any outstanding requests. This should be used regularly by the Wallet to ensure that it is up to date with any changes made in the Wallet Server.
 *
 * @throws ServerParamException Thrown if a critical server parameter is not set (URL, Username or Password).
 */
-(void)synchronize;

/**
 * Forces a synchronisation with the Wallet Server to update the list of LegicWalletApplications. Note that it is recommended in most cases that IDConnectManager::synchronize be used to keep the Wallet and the Wallet Server in sync. This method is provided for exceptional cases where it is useful to manually update the list of LegicWalletApplications without waiting for the server.
 *
 * @throws ServerParamException Thrown if a critical server parameter is not set (URL, Username or Password).
 */
-(void)syncDevice;

/**
 * Sets the URL to use for connecting to the Wallet Server. Note that this must be set before any interactions with the Wallet Server, otherwise the SDK will throw an exception.
 *
 * @param newURL The URL to use.
 */
-(void)setServerURL:(NSString *)newURL;

/**
 * Sets the Username to use for authenticating with the Wallet Server. Note that this must be set before any interactions with the Wallet Server, otherwise the SDK will throw an exception.
 *
 * @param username The username to use.
 */
-(void)setUsername:(NSString*)username;

/**
 * Sets the Password to use for authenticating with the Wallet Server. Note that this must be set before any interactions with the Wallet Server, otherwise the SDK will throw an exception.
 *
 * @param password The password to use.
 */
-(void)setPassword:(NSString*)password;

/**
 * Sets the wallet ID to use when communicating with the Wallet Server. This should be a unique idenifier for the Wallet with the server.
 *
 * @param walletId The wallet ID to use.
 */
-(void)setWalletId:(long)walletId;

/**
 * Handles push messages sent to the wallet. Once registration with the Apple Push Notification (APN) service has been successful, any push messages recieved should be passed directly to the SDK using this method so that they can be handled properly.
 *
 * @param pushMessage The push message received from the server.
 *
 * @return NSError describing error encountered handling push message, or nil if successful.
 */
-(NSError*)handlePushMessage:(NSDictionary*)pushMessage;

/**
 * Returns the singleton instance of the LegicStorageManager to access persistent data. This class can be used to retrieve data that has been persistently stored for the Wallet by the SDK.
 *
 * @return LegicStorageManager Singleton instance of the LegicStorageManager.
 */
-(LegicStorageManager*)getPersistentStore;

/**
 * This function returns whether the manager has successfully connected to the given secure element, see LegicSeTypes for the different secure elements available. For SD cards this will return whether the connection has been established yet. Note that on iPhones there is a delay establishing a connection through the iCaisse, any attempts to interact with the SD card secure element should poll this method to ensure it has successfully connected before continuing. For BLE this returns whether the BLE component is connected based on the most recently received BLE state.
 *
 * @param seType The secure element type to check.
 *
 * @return BOOL YES if the manager is connected, otherwise, NO.
 */
-(BOOL)isConnected:(LegicSeTypes*)seType;

/**
 * Sets the apple push notification token for use with server connections. Once the wallet has successfully registered with the Apple Push Notification (APN) service, the token that is returned should be passed into the SDK using this function so that it can be registered with the Wallet Server.
 *
 * @param token The token returned when registering with the Apple Push Notification (APN) service.
 */
-(void)setApplePushNotificationToken:(NSData*)token;

/**
 * Returns whether the user has registered the Wallet with the Wallet Server. Once the IDConnectManager::completeRegistration function has successfully completed, this will return YES. It can also be manually set using the LegicStorageManager::saveUserRegistered function.
 *
 * @return YES if the Wallet is registered, otherwise NO.
 */
-(BOOL)isUserRegistered;

/**
 * Returns the version number of the SDK.
 *
 * @return NSString contatining the version number.
 */
-(NSString*)getSDKVersion;

/**
 * Sends a message to the BLE Reader. This should be used to send any messages from the Wallet to the BLE reader, including any responses to messages received through the LegicSeUIDelegate::onReceiveMessageFromReader function implementation.
 *
 * @param code    The code identifying the message type.
 * @param message The message to send to the reader.
 *
 * @return NSError Error describing result (nil if successful).
 */
-(NSError*)sendMessageToBLEReader:(NSUInteger)code Message:(NSData*)message;

/**
 * Save a configuration parameter to the persistent store.
 *
 * @param paramName The name of the parameter to store.
 * @param value     The value to store.
 */
-(void)setConfigParam:(NSString*)paramName Value:(NSString*)value;

/**
 * Load a configuration parameter from the persistent store.
 *
 * @param paramName The name of the parameter to load.
 *
 * @return NSString containing the value of the parameter.
 */
-(NSString*)getConfigParam:(NSString*)paramName;

/**
 * Sets the level at which the SDK should log information in the device logs. All the logs written in the SDK will be output at this level.
 *
 * @param loggingMode What level data from the SDK should be logged to (see ASL levels in <asl.h>).
 */
-(void)setLoggingMode:(int)loggingMode;

/**
 * Returns an NSArray containing the complete list of logs from the SDK during this session (reset when the Wallet is restarted).
 *
 * @return NSArray containing logs in NSStrings.
 */
-(NSArray*)getLogs;

/**
 * Queries the SDK about what technologies are available to use on this device. This checks the hardware of the device in question and returns which technologies are currently available for use. Note that in the SD card case this will be dependent on the iCaisse being attached to the handset and the SD card properly inserted into the SD card slot.
 *
 * @return NSArray Array containing LegicProfileIds representing supported technologies.
 */
-(NSArray*)getAvailableTechnologies;

@end
