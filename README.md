# Communique

======================

## OVERVIEW

Communique is an app intended for communication between an apartment complex and its residents. There is a web app, intended for Admin-type users, and both an iPhone and Android app intended for the Residents. All three are supported by a Parse backend.

## SETUP
In order to run this project, you must first have [Cocoapods](http://www.cocoapods.org) installed on your local machine.  Once that's done, you must run 'pod install' from the command line in order to install the project's dependencies. You must then use the .xcworkspace file instead of the .xcodeproj file. (Again, see www.cocoapods.org for details and help if you are unfamiliar). This project also makes use of [XcodeColors](https://github.com/robbiehanson/XcodeColors), which allows all log statements to be printed in colors instead of all white. This is an extremely helpful debugging tool, though not necessary. You must install it on your local machine if you'd like to make use of it.

### CLASSES

This project practices the MVVM (Model View ViewModel) design as opposed to the traditional MVC (Model-View-Controller) design. Each View Controller has its respective ViewModel that is used to perform actions/calculations that don't directly involve the UI, leaving all view handling to the view controller. All view controllers have its own xib file. This is simply an overview of not-so-obvious classes. More details can be found in the .h/.m files of each class

### Parse Object Subclasses

All objects found in parse are subclassed here with the 'CMQ' prefix. They're all pretty straightforward and self-explanatory. These include 'CMQUser', 'CMQApartment', 'CMQMessage', 'CMQEvent', 'CMQNewsArticle' and can be found in the 'Model Objects' folder. See the [Parse Subclass Overview](https://parse.com/docs/ios_guide#subclasses/iOS) if you need assistance with subclassing.

#### CMQNotificationHandler
This is a singleton object used to handle all aspects of notifications. It is intended to request permissions, save the device token used for Parse, handle a received notification, play a sound, and clear the icon badge.

#### CMQAPIClient
This is a singleton objects used to handle all aspects of sending/receiving API requests from parse. It handles logging in/out, updating user details, subscribing users to PUSH channels, fetching all object types and posting all object types.

#### CMQAPIClientDelegate
(found in the CMQAPIClient files) This is a protocol used to send API request success/failures to view controllers who have set themselves as a delegate to the CMQAPIClient.

#### CMQHTMLParser

This is a singleton object used to scrape a website for any "//img" tags and its title. Unfortunately this class is not 100% reliable, but that's most likely because we're *web scraping*.

#### CMQDottedLine
This is a simple subclass of UIView. It draws itself by alternating with a lightGrayColor and a clearColor every 5 pixels in order to mimic a dotted or dashed line.

#### View Controllers
The view controlers are really pretty self-explanatory as far as which controller relates to which view, especially with the folder hierarchy the way that it is. Each view controller is pretty well documented, so refer to each .h/.m file for a more in-depth explanaton of things. That being said, here are some high-level explanations of things you'll most likely have questions about.

#### Event's View Event Swiping
The event's view event swiping uses up to two instances of CMQEventView to display event data and allow them to be swiped off the screen. If there are no events, no CMQEventViews are created and swiping is disabled. If there is one event, one CMQEventView is created and swiping is disabled. If there are two events or more, only two CMQEventViews are created and are stacked on top of one another with swiping now enabled. If there are more than two events, the two CMQEventViews simply switch places in the stack while reconfiguring themselves with the next event in the eventsArray in a cyclical fashion. So for example, if there are 3 events (A, B, and C) and we create 2 CMQEventViews for the 3 events, CMQEventView(1) will be on top and will display info from event A and CMQEventView(2) will be on the bottom and will display info from event B. Once CMQEventView(1) is swiped off the screen, it is then reset with info from event C and is placed directly underneath CMQEventView(2), which is now on top. This continues until the end of the eventsArray and then restarts from the beginning again. So to continue with our example, if we now swipe CMQEventView(2) (i.e. event B) off the screen, CMQEventView(1) will be on top again showing event C and CMQEventView(2) is then placed on the bottom again with info from event A. We then swipe CMQEventView(1) off screen and display CMQEventView(2) which is configured with event A, and CMQEventView(1) gets configured with event B, and so on and so forth.



Note: This file is written with Github Flavored Markdown. See [Preview](http://tmpvar.com/markdown.html) and [Cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)# Communique