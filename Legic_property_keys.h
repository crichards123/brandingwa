//
//  property_keys.h
//  legic-server-client
//
//  Created by Ian Stewart on 22/09/2014.
//  Copyright (c) 2014 Legic. All rights reserved.
//

#import <Foundation/Foundation.h>

// Key to use for passing the phone number in a Property.
FOUNDATION_EXPORT NSString* const PROPERTY_KEY_PHONE_NUMBER;
