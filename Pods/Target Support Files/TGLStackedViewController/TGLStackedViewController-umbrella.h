#ifdef __OBJC__
#import <UIKit/UIKit.h>
#else
#ifndef FOUNDATION_EXPORT
#if defined(__cplusplus)
#define FOUNDATION_EXPORT extern "C"
#else
#define FOUNDATION_EXPORT extern
#endif
#endif
#endif

#import "TGLExposedLayout.h"
#import "TGLStackedLayout.h"
#import "TGLStackedViewController.h"

FOUNDATION_EXPORT double TGLStackedViewControllerVersionNumber;
FOUNDATION_EXPORT const unsigned char TGLStackedViewControllerVersionString[];

