//
//  NotificationService.swift
//  SmartHomeNotificationExtension
//
//  Created by Andre White on 8/19/18.
//  Copyright © 2018 Communique, LLC. All rights reserved.
//

import UserNotifications

public class NotificationService: UNNotificationServiceExtension {

    var contentHandler: ((UNNotificationContent) -> Void)?
    var bestAttemptContent: UNMutableNotificationContent?

    override public func didReceive(_ request: UNNotificationRequest, withContentHandler contentHandler: @escaping (UNNotificationContent) -> Void) {
        self.contentHandler = contentHandler
        bestAttemptContent = (request.content.mutableCopy() as? UNMutableNotificationContent)
        
        if let bestAttemptContent = bestAttemptContent {
            // Modify the notification content here...
            switch bestAttemptContent.categoryIdentifier{
            case "thermostat","lock","lightGroup","light","socket","fan":
                bestAttemptContent.body = ""
                bestAttemptContent.title = ""
                bestAttemptContent.badge = 0
            default:
                break
            }
            contentHandler(bestAttemptContent)
        }
    }
    
    override public func serviceExtensionTimeWillExpire() {
        // Called just before the extension will be terminated by the system.
        // Use this as an opportunity to deliver your "best attempt" at modified content, otherwise the original push payload will be used.
        if let contentHandler = contentHandler, let bestAttemptContent =  bestAttemptContent {
            contentHandler(bestAttemptContent)
        }
    }

}
