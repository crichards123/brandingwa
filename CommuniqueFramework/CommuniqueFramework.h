//
//  CommuniqueFramework.h
//  CommuniqueFramework
//
//  Created by Andre White on 1/8/18.
//  Copyright © 2018 Communique, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for CommuniqueFramework.
FOUNDATION_EXPORT double CommuniqueFrameworkVersionNumber;

//! Project version string for CommuniqueFramework.
FOUNDATION_EXPORT const unsigned char CommuniqueFrameworkVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <CommuniqueFramework/PublicHeader.h>


