//
//  technology_name.h
//  legic-server-client
//
//  Created by Ian Stewart on 06/08/2014.
//  Copyright (c) 2014 Legic. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 * Class to represent the Technology Names of Wallet Applications.
 *
 */
@interface LegicTechName : NSObject<NSCoding>

/// Enumerator for Technology Names.
typedef enum LegicTechnologyName
{
    Advant        = 0,          /**< Wallet Application is an Advant application. */
    MifareClassic = 1,          /**< Wallet Application is a MiFare Classic application. */
    MifareDesfire = 2,          /**< Wallet Application is a MiFare DesFire application. */
    Ble           = 3           /**< Wallet Application is a BLE application. */
} LegicTechnologyName;

/**
 * Initialise instance with supplied string.
 *
 * @param statusString NSString representing the technology name.
 *
 * @return Initialised instance.
 */
-(id)initWithString:(NSString*)nameString;

/**
 * Initialise instance with supplied technology name.
 *
 * @param status The technology name.
 *
 * @return Initialised instance.
 */
-(id)initWithTechnologyName:(LegicTechnologyName)name;

/**
 * Set the technology name with the supplied string.
 *
 * @param nameString NSString representing the application status.
 */
-(void)setTechnologyNameWithString:(NSString*)nameString;

/**
 * Set the technology name.
 *
 * @param status The technology name.
 */
-(void)setTechnologyName:(LegicTechnologyName)name;

/**
 * Get the technology name.
 *
 * @return The technology name.
 */
-(LegicTechnologyName)getTechnologyName;

/**
 * Get the string that represents this technology name in server calls.
 *
 * @return NSString that represents this technology name for server calls.
 */
-(NSString*)getTechnologyNameString;

@end
