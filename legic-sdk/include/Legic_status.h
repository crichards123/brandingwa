//
//  status.h
//  legic-server-client
//
//  Created by Ian Stewart on 11/06/2014.
//  Copyright (c) 2014 Legic. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 * This Class is used to return the status of a completed server call. The status is composed of an NSInteger code and an NSString description. It can also be returned in the form of an NSDictionary containing both elements.
 *
 */
@interface LegicStatus : NSObject

/**
 * Initialised instance with the supplied code and description.
 *
 * @param code        The code representing the outcome of the server connection.
 * @param description NSString containing the description of the outcome of the server connection.
 *
 * @return The initialised instance.
 */
-(id)initWithCode:(NSInteger)code Description:(NSString*)description;

/**
 * Initialised instance with the supplied dictionary containing the code and description.
 *
 * @param dictionary The dictionary containing the code and description.
 *
 * @return The initialised instance.
 */
-(id)initWithDictionary:(NSDictionary*)dictionary;

/**
 * Sets the code associated with this status.
 *
 * @param code The code representing the outcome of the server connection.
 */
-(void)setCode:(NSInteger)code;

/**
 * Sets the description associated with this status.
 *
 * @param description NSString containing the description of the outcome of the server connection.
 */
-(void)setDescription:(NSString*)description;

/**
 * Returns the code associated with this status.
 *
 * @return code The code representing the outcome of the server connection.
 */
-(NSInteger)getCode;

/**
 * Returns the description associated with this status.
 *
 * @return description NSString containing the description of the outcome of the server connection.
 */
-(NSString*)getDescription;

/**
 * Returns the status as a dictionary for use in a json object.
 *
 * @return NSDictionary* representing the status.
 */
-(NSDictionary*)getDictionary;

@end
