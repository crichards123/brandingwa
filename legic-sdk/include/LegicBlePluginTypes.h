// -----------------------------------------------------------------------------
// Copyright© 2015 LEGIC® Identsystems AG, CH-8623 Wetzikon
// Confidential. All rights reserved!
// -----------------------------------------------------------------------------
//    Project: BLE Plugin
//      $File: LegicBlePluginTypes.h$
//    $Author: mwuerth$
//      $Date: Freitag, 11. M�rz 2016 13:11:56$
//  $Revision: 1$
// -----------------------------------------------------------------------------

/*! \file */

#ifndef LegicBLEpluginCore_PluginTypes_h
#define LegicBLEpluginCore_PluginTypes_h

/// The bluetooth state changes can be used on the UI to detect if the user has turned the bluetooth device off or on and therefore tell the user to turn it back on.
typedef NS_ENUM(NSUInteger, E_LegicBlePluginBLEState) {
    PluginBLEStateUnknown = 0,                  ///< Unknown State
    PluginBLEStateInitDone = 1,                 ///< BLE Lib initialisation complete
    PluginBLEStateDisabled = 2,                 ///< BLE Lib not activated (Not scanning or advertising)
    PluginBLEStateEnabled = 3,                  ///< BLE Lib activated (Scanning or advertising)
    PluginBLEStateErrorBleNotSupported = 6,     ///< BLE Lib reports, Bluetooth Low Energy is not supported on current hardware
    PluginBLEStateErrorBleNotActivated = 7,     ///< BLE Lib reports, Bluetooth Low Energy is not activated on current hardware
    PluginBLEStateErrorGeneral = 8              ///< BLE Lib General Error
};

/// The state of a project returned to the UI after a request to enable or disable a project.
typedef NS_ENUM(NSUInteger, E_LegicBlePluginProjectState) {
    PluginProjectStateDisabled,       ///< Project has been disabled.
    pluginProjectStateEnabled         ///< Project has been enabled.
};

/// The following messages are at the moment sent from the BLE plugin via non-blocking SDK function. The data is TLV encoded so the messages ahve the following byte sequest: [TAG][LENGTH][VALUES]
typedef NS_ENUM(NSUInteger, E_LegicBlePluginMessageType) {
    PluginMessageTypeIdcMessage = 0,       ///< This code is used to send messages received from the BLE reader (via IDC_MESSAGE) to the UI. E.g. Lock Battery status
    PluginMessageTypeBluetoothState = 1,   ///< This code is used to send BLE State changes from the BLE Reader to the UI see E_LegicBlePluginBLEState for a list of states.
    PluginMessageTypeProjectState = 2,     ///< After a call to enableProjectWithNumber or disableProjectWithNumber, the Bluetooth library informs the Plugin via a delegate asynchronously when a project number was enabled/disabled. This is sent to the UI so that this change can be correctly displayed. The projectNr that was enabled/disabled is at the moment not sent via this message, as the user of the SDK doesn't have access to project numbers.
    PluginMessageTypeIdcPolling = 3,       ///< This event indicates the ongoing polling on the IDC command layer to the upper layers. Can be used as a keep-alive or time indication.
    PluginMessageTypeIdcFileWasRead = 4    ///< This event is sent after a successful read access to an IDC file. File name format: (TLV encoded) Tag 0: Wallet Customer App Id Tag1: Qualifier
};

#endif
