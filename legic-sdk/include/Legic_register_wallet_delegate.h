//
//  complete_registration_listener.h
//  legic-server-client
//
//  Created by Ian Stewart on 05/09/2014.
//  Copyright (c) 2014 Legic. All rights reserved.
//

#ifndef legic_server_client_complete_registration_listener_h
#define legic_server_client_complete_registration_listener_h

#import "Legic_status.h"

/**
 * This protocol defines the functions required for delegates receiving the results of attempts to register the Wallet with the Wallet Server. This should be used for both the registerWallet and completeRegistration functions.
 *
 */
@protocol LegicRegisterWalletDelegate <NSObject>

@required

/**
 * Reports a successful call and whether or not the device was already registered.
 *
 * @param alreadyRegistered True if the user has already registered.
 */
-(void)success:(BOOL)alreadyRegistered;

/**
 * Reports a failed call and status of the call.
 *
 * @param status The status of the server call.
 */
-(void)fail:(LegicStatus*)status;

@optional

/**
 * Returns the Token to use to verify the device with the server in the complete Wallet registration server call. Should be implemented for initiate registration server calls if the Wallet will pass in NONE as the Confirmation Method.
 */
-(void)token:(NSString*)token;

@end

#endif
