//
//  wallet_application_info.h
//  legic-server-client
//
//  Created by Ian Stewart on 11/06/2014.
//  Copyright (c) 2014 Legic. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Legic_app_status.h"

/**
 * This Class is used to group the data required to be sent to the Wallet Server during a synchronise request.
 *
 */
@interface LegicWalletApplicationInfo : NSObject

/**
 * Initialises the instance with the supplied variables.
 *
 * @param walletAppId      The Wallet application ID for this Wallet application.
 * @param metadataVersion  The version number for the Wallet application's metadata.
 * @param appStatus        The Wallet application's LegicAppStatus.
 *
 * @return The initialised instance.
 */
-(id)initWithWalletAppId:(NSString*)walletAppId
               Qualifier:(NSNumber*)qualifier
         MetadataVersion:(NSString*)metadataVersion
               AppStatus:(LegicAppStatus*)appStatus;

/**
 * Returns the Wallet application ID.
 *
 * @return NSString containing the Wallet application ID.
 */
-(NSString*)getWalletAppId;

/**
 * Returns the qualifier.
 *
 * @return NSNumber containing the qualifier.
 */
-(NSNumber*)getQualifier;

/**
 * Returns the Wallet application metadata version.
 *
 * @return NSString containing the Wallet metadata version.
 */
-(NSString*)getMetadataVersion;

/**
 * Returns the Wallet application status.
 *
 * @return LegicAppStatus representing the Wallet application status.
 */
-(LegicAppStatus*)getAppStatus;

@end
