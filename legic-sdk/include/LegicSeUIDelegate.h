//
//  SeUIListener.h
//  BLEpluginCore
//
//  Created by Markus Ruh on 22/08/14.
//  Copyright (c) 2014 Legic. All rights reserved.
//

#ifndef legic_sdk_SeUIDelegate_h
#define legic_sdk_SeUIDelegate_h

/**
 * Protocol to implement in order to receive callbacks from the BLE plugin.
 *
 */
@protocol LegicSeUIDelegate <NSObject>

@required

/**
 * Returns a message to the UI from the BLE reader. This function should handle any messages passed from the BLE reader to the Wallet Application.
 *
 * @param code    The code identifying the message - see LegicBlePluginTypes.h, E_LegicBlePluginMessageType for possible codes.
 * @param message The message sent from the reader, the message format will vary depending on the message type (see code).
 */
-(void)onReceiveMessageFromReader:(NSUInteger)code data:(NSData*)message;

@end

#endif
