//
//  app_status.h
//  legic-server-client
//
//  Created by Ian Stewart on 11/06/2014.
//  Copyright (c) 2014 Legic. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 * Class to represent the status of the Wallet application.
 *
 */
@interface LegicAppStatus : NSObject<NSCoding>

/// Enumerator for Wallet application status.
typedef enum LegicApplicationStatus
{
    Deployed          = 0,                  /**< Deployed to the Wallet and ready for use. */
    Available         = 1,                  /**< Available on the Wallet Server to be deployed to the Wallet. */
    Removed           = 2,                  /**< Removed from the Wallet, no longer available for use. */
    DeployInProgress  = 3,                  /**< Currently in the process of deploying to the Wallet. */
    RemoveInProgress  = 4,                  /**< Currently in the process of being removed from the Wallet. */
    Rejected          = 5,                  /**< Request to add WalletApplication was rejected. */
    RequestAdd        = 6,                  /**< WalletApplication has been requested to be deployed to the Wallet. */
    RequestRemove     = 7                   /**< WalletApplication has been requested to be removed from the Wallet. */
} LegicApplicationStatus;

/**
 * Initialise instance with supplied string.
 *
 * @param statusString NSString representing the application status.
 *
 * @return Initialised instance.
 */
-(id)initWithString:(NSString*)statusString;

/**
 * Initialise instance with supplied status.
 *
 * @param status The application status.
 *
 * @return Initialised instance.
 */
-(id)initWithStatus:(LegicApplicationStatus)status;

/**
 * Set the status with the supplied string.
 *
 * @param statusString NSString representing the application status.
 */
-(void)setStatusWithString:(NSString*)statusString;

/**
 * Set the status.
 *
 * @param status The application status.
 */
-(void)setStatus:(LegicApplicationStatus)status;

/**
 * Get the status.
 *
 * @return The application status.
 */
-(LegicApplicationStatus)getStatus;

/**
 * Get the string that represents this status in server calls.
 *
 * @return NSString that represents this status for server calls.
 */
-(NSString*)getStatusString;

@end
