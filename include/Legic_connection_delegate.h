//
//  connection_listener.h
//  legic-sdk
//
//  Created by Ian Stewart on 01/05/2014.
//  Copyright (c) 2014 Legic. All rights reserved.
//

#ifndef legic_sdk_connection_listener_h
#define legic_sdk_connection_listener_h

#import "Legic_status.h"

/**
 * Protocol to implement in order to receive whether the connection was successful, and to check it's status.
 *
 */
@protocol LegicConnectionDelegate <NSObject>

@required
/**
 * Returns the result of a Wallet Server connection and a LegicStatus including the code and description returned.
 *
 * @param successful   True if the connection was successful.
 * @param status       The status returned by the connection.
 */
-(void)result:(BOOL)successful status:(LegicStatus*)status;

@end

#endif
