//
//  StorageManager.h
//  legic-sdk
//
//  Created by Ian Stewart on 01/10/2014.
//  Copyright (c) 2014 Legic. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 * This class manages all persistent store objects for the SDK. This includes User registration, storing LegicWalletApplications and keeping track of which LegicWalletApplications have been activated.
 *
 */
@interface LegicStorageManager : NSObject

/**
 * Saves list of LegicWalletApplications to the persistent store. This should be the complete list of applications as it will replace the contents of the store with the contents of the list.
 *
 * @param applications The list of LegicWalletApplications.
 *
 * @throws PersistentStoreException Exception thrown if the persistent store was unable to complete the operation.
 */
-(void)saveWalletApplications:(NSArray*)applications;

/**
 * Saves the provided list of identifiers as active LegicWalletApplications.
 *
 * @param walletIdentifiers Array of NSDictionaries containing walletAppIds and Qualifiers as key-value pairs (e.g. @{ "walletAppId" : qualifier }).
 *
 * @throws PersistentStoreException Exception thrown if the persistent store was unable to complete the operation.
 */
-(void)saveWalletApplicationActiveState:(NSArray*)walletIdentifiers;

/**
 * Retrieves the list of identifiers of active LegicWalletApplications from the persistent store.
 *
 * @return NSArray containing NSDictionaries containing the walletAppIds and qualifiers of the active Wallet apps as key-value pairs (e.g. @{ "walletAppId" : qualifier }).
 */
-(NSArray*)retrieveActiveWalletAppList;

/**
 * Removes the supplied identifiers from the list of active LegicWalletApplications.
 *
 * @param walletIdentifiers Array of NSDictionaries containing walletAppIds and Qualifiers as key-value pairs (e.g. @{ "walletAppId" : qualifier }).
 *
 * @throws PersistentStoreException Exception thrown if the persistent store was unable to complete the operation.
 */
-(void)removeWalletApplicationActiveState:(NSArray*)walletIdentifiers;

/**
 * Removes all active Wallet applications from the persistent store.
 *
 * @throws PersistentStoreException Exception thrown if the persistent store was unable to complete the operation.
 */
-(void)removeAllActiveWalletApplications;

/**
 * Returns the Apple Push Notification (APN) token from the persistent store.
 *
 * @return NSString containing the APN token or nil if not stored.
 */
-(NSString*)getAPNToken;

/**
 * Saves whether the user is registered or not to the persistent store.
 *
 * @param isRegistered True if the user is registered, otherwise false.
 *
 * @throws PersistentStoreException Exception thrown if the persistent store was unable to complete the operation.
 */
-(void)saveUserRegistered:(BOOL)isRegistered;

@end
