//
//  se_types.h
//  legic-server-client
//
//  Created by Ian Stewart on 18/08/2014.
//  Copyright (c) 2014 Legic. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 * Class to represent secure element types of Wallet Applications.
 *
 */
@interface LegicSeTypes : NSObject<NSCoding>

/// Enumerator for secure element types.
typedef enum LegicSecureElementType
{
    SETYPE_SD_CARD,         /**< Wallet Application is located on an SD Card. */
    SETYPE_BLE,             /**< Wallet Application is a BLE application. */
    SETYPE_UICC             /**< Wallet Application is located on a UICC Card (Not supported on iOS). */
} LegicSecureElementType;

/**
 * Initialise instance with supplied string.
 *
 * @param seTypeString NSString representing the SE Type.
 *
 * @return Initialised instance.
 */
-(id)initWithString:(NSString*)seTypeString;

/**
 * Initialise instance with supplied SE Type.
 *
 * @param status The SE Type.
 *
 * @return Initialised instance.
 */
-(id)initWithSEType:(LegicSecureElementType)seTypeId;

/**
 * Set the SE Type with the supplied string.
 *
 * @param seTypeString NSString representing the SE Type.
 */
-(void)setSETypeWithString:(NSString*)seTypeString;

/**
 * Set the SE Type.
 *
 * @param operation The SE Type.
 */
-(void)setSEType:(LegicSecureElementType)seType;

/**
 * Get the SE Type.
 *
 * @return The SE Type.
 */
-(LegicSecureElementType)getSEType;

/**
 * Get the string that represents this SE Type in server calls.
 *
 * @return NSString that represents this SE Type for server calls.
 */
-(NSString*)getSETypeString;

@end
