//
//  confirmation_method.h
//  legic-server-client
//
//  Created by Ian Stewart on 31/10/2014.
//  Copyright (c) 2014 Legic. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 * Class to represent the different methods to use to confirm the user's identity with the server.
 *
 */
@interface LegicConfirmationMethods : NSObject

/// Enumerator for confirmation methods.
typedef enum LegicConfirmationMethod
{
    EMAIL,          /**< Confirm identity using email. */
    PUSH,           /**< Confirm identity using push messaging. */
    SMS,            /**< Confirm identity using SMS messaging. */
    NONE            /**< No confirmation method (see LegicRegisterWalletDelegate::token) */
} LegicConfirmationMethod;

/**
 * Initialise instance with supplied string.
 *
 * @param methodString NSString representing the confirmation method.
 *
 * @return Initialised instance.
 */
-(id)initWithString:(NSString*)methodString;

/**
 * Initialise instance with supplied status.
 *
 * @param status The application status.
 *
 * @return Initialised instance.
 */
-(id)initWithMethod:(LegicConfirmationMethod)method;

/**
 * Set the status with the supplied string.
 *
 * @param methodString NSString representing the confirmation method.
 */
-(void)setMethodWithString:(NSString*)methodString;

/**
 * Set the confirmation method.
 *
 * @param status The confirmation method.
 */
-(void)setMethod:(LegicConfirmationMethod)method;

/**
 * Get the confirmation method.
 *
 * @return The confirmation method.
 */
-(LegicConfirmationMethod)getMethod;

/**
 * Get the string that represents this confirmation method in server calls.
 *
 * @return NSString that represents this confirmation method for server calls.
 */
-(NSString*)getMethodString;

@end
