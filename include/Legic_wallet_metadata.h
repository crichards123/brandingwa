//
//  wallet_metadata.h
//  legic-server-client
//
//  Created by Ian Stewart on 11/06/2014.
//  Copyright (c) 2014 Legic. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 * This Class represents the metadata of a LegicWalletApplication, these values will be kept up to date through sycnhronising with the Wallet Server.
 *
 */
@interface LegicWalletMetadata : NSObject<NSCoding>

enum LegicCustomParamType
{
    CUSTOMPARAM_STRING,     /**< Custom parameter is an NSString object. */
    CUSTOMPARAM_NUMBER,     /**< Custom parameter is an NSNumber object. */
    CUSTOMPARAM_BASE64      /**< Custom parameter is a Base64 encoded NSString object. */
};

/**
 * Initialises the metadata with the supplied dictionary.
 *
 * @param content The dictionary containing data to populate the metadata.
 *
 * @return The initialised instance.
 */
-(id)initWithContent:(NSDictionary*)content;

/**
 * Sets the content of the metadata with the supplied dictionary.
 *
 * @param content The dictionary containing data to populate the metadata.
 */
-(void)setContent:(NSDictionary*)content;

/**
 * Returns the Wallet application name. Will always be present.
 *
 * @return NSString* The Wallet app name.
 */
-(NSString*)getName;

/**
 * Returns the Wallet application company name. Optional.
 *
 * @return NSString* The Wallet app company name.
 */
-(NSString*)getCompanyName;

/**
 * Returns the data used to construct the Wallet application icon image.
 * Optional.
 *
 * @return NSData* The Wallet app icon data.
 */
-(NSData*)getIconData;

/**
 * Returns the Wallet application valid from date. Optional.
 *
 * @return NSString* The Wallet app valid from date.
 */
-(NSString*)getValidFromDate;

/**
 * Returns the Wallet application valid to date. Optional.
 *
 * @return NSString* The Wallet app valid to date.
 */
-(NSString*)getValidToDate;

/**
 * Returns the Wallet application description. Optional.
 *
 * @return NSString* The Wallet app description.
 */
-(NSString*)getDescription;

/**
 * Returns the Wallet application category. Optional.
 *
 * @return NSString* The Wallet app category.
 */
-(NSString*)getCategory;

/**
 * Returns an array containing data used to construct up
 * to five images for the Wallet application. Optional.
 *
 * @return NSArray* The Wallet app images data
 */
-(NSArray*)getImages;

/**
 * Returns the Wallet application remarks. Optional.
 *
 * @return NSString* The Wallet app remarks.
 */
-(NSString*)getRemarks;

/**
 * Returns the Wallet application creator user name. Will always be present.
 *
 * @return NSString* The Wallet app creator user name.
 */
-(NSString*)getCreatorUserName;

/**
 * Returns the Wallet application creator timestamp. Will always be present.
 *
 * @return NSString* The Wallet app creator timestamp.
 */
-(NSNumber*)getCreatorTimestamp;

/**
 * Returns the Wallet application modifier user name. Will always be present.
 *
 * @return NSString* The Wallet app modifier user name.
 */
-(NSString*)getModifierUserName;

/**
 * Returns the Wallet application modifier timestamp. Will always be present.
 *
 * @return NSString* The Wallet app modifier timestamp.
 */
-(NSNumber*)getModifierTimestamp;

/**
 * Returns the list of custom parameters associated with this Wallet application metadata.
 *
 * @return NSDictionary* Dictionary containing custom parameters. The parameter names are used as the keys in the dictionary, the values corresponding to the keys are NSArrays. For single values the NSArrays contain two values - the first is an NSNumber representing a LegicCustomParamType value to determine type of the parameter. The second value is the value of the parameter. For multiple values, an NSArray of the described single values is returned.
 *
 */
-(NSDictionary*)getCustomParameters;

/**
 * Updates the WalletMetadata to the latest version from the server.
 *
 * @param content The data containing the latest metadata values.
 */
-(void)updateMetadata:(NSDictionary*)content;

@end
