//
//  wallet_application.h
//  legic-sdk
//
//  Created by Ian Stewart on 02/05/2014.
//  Copyright (c) 2014 Legic. All rights reserved.
//

#ifndef legic_sdk_wallet_application_h
#define legic_sdk_wallet_application_h

#import <Foundation/Foundation.h>
#import "Legic_app_status.h"
#import "Legic_wallet_application_info.h"
#import "Legic_wallet_metadata.h"
#import "Legic_technology_name.h"
#import "Legic_se_types.h"

/**
 * This class is used to represent a Wallet application that can be deployed to the Wallet from the Wallet Server. Each Wallet Application is identified through a combination of the WalletAppID and the Qualifier for a unique identification.
 *
 */
@interface LegicWalletApplication : NSObject<NSCoding>

/**
 * Initialises the WalletApplication with the supplied JSON content.
 *
 * @param content      The JSON data to populate the WalletApplication
 * @param metadata     The metadata for the Wallet application.
 *
 * @return The initialised Wallet application.
 */
-(id)initWithContent:(NSDictionary*)content metadata:(NSDictionary*)metadata;

/**
 * Returns the Wallet application ID, this ID will always be present.
 *
 * @return NSString* The Wallet app ID.
 */
-(NSString*)getWalletAppId;

/**
 * Returns the qualifier that distinguishes applications with the same Wallet app ID.
 *
 * @return NSNumber* The qualifier.
 */
-(NSNumber*)getQualifier;

/**
 * Returns the Wallet application parent ID. If this value is empty,
 * then the application is a parent, otherwise it can be used to identify
 * the parent application.
 *
 * @return NSString* The Wallet app parent ID (empty if app is a parent).
 */
-(NSString*)getParentId;

/**
 * Returns the app status (see LegicAppStatus). Will always be present.
 *
 * @return LegicAppStatus The Wallet application status.
 */
-(LegicAppStatus*)getAppStatus;

/**
 * Returns the Wallet application metadata version. Will always
 * be present.
 *
 * @return NSString* The Wallet app metadata version.
 */
-(NSString*)getMetadataVersion;

/**
 * Returns the Wallet application metadata. Will always be present.
 *
 * @return WalletMetadata The Wallet application metadata.
 */
-(LegicWalletMetadata*)getMetadata;

/**
 * Returns the Wallet application technology name. Will always be present.
 *
 * @return TechName* The technology name of the application.
 */
-(LegicTechName*)getTechnologyName;

/**
 * Returns the Wallet application SE type. Will always be present.
 *
 * @return SeTypes* The SE type of the application.
 */
-(LegicSeTypes*)getSEType;

/**
 * Returns the Wallet application information for making a sync device request.
 *
 * @return WalletApplicationInfo* The Wallet application info for the JSON request.
 */
-(LegicWalletApplicationInfo*)getWalletApplicationInfo;

/**
 * Updates the WalletApplication with the latest data from the server.
 *
 * @param content  The latest version of the WalletApplication content.
 * @param metadata The latest version of the WalletApplication metadata.
 */
-(void)updateWalletData:(NSDictionary*)content metadata:(NSDictionary*)metadata;

/**
 * Checks whether the applications are equal by verifying the walletAppId and qualifier are equal.
 *
 * @param otherApplication The application to compare.
 *
 * @return YES if the applications are equal.
 */
-(BOOL)isEqualToApplication:(LegicWalletApplication*)otherApplication;

@end

#endif
