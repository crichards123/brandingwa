//
//  FileSelectionModes.h
//  legic-sdk
//
//  Created by Ian Stewart on 02/10/2014.
//  Copyright (c) 2014 Legic. All rights reserved.
//

#ifndef legic_sdk_FileSelectionModes_h
#define legic_sdk_FileSelectionModes_h

#import <Foundation/Foundation.h>

/// Configuration parameter that should be set by the wallet application using [IDConnectionManager setConfigParam:(name) value:(value)].

/// The file selection mode parameter name.
FOUNDATION_EXPORT NSString* const FILE_SELECTION_MODE_PARAM_NAME;

/// Only use preselected file
FOUNDATION_EXPORT NSString* const FILE_SELECTION_MODE_VALUE_PRESELECTED_FILE;
/// All files are advertised and can be selected by the reader.
FOUNDATION_EXPORT NSString* const FILE_SELECTION_MODE_VALUE_ALL_FILES;

#endif
