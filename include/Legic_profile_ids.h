//
//  profile_id.h
//  legic-server-client
//
//  Created by Ian Stewart on 01/08/2014.
//  Copyright (c) 2014 Legic. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 * Class to represent Profile IDs for Wallet Applications.
 *
 */
@interface LegicProfileIds : NSObject

/// Enumerator for Profile IDs.
typedef enum LegicProfileId
{
    PROFILEID_SD_CARD,          /**< Wallet would like to use SD card applications. */
    PROFILEID_BLE,              /**< Wallet would like to use BLE applications. */
    PROFILEID_UICC              /**< Wallet would like to use UICC applications (not supported on iOS). */
} LegicProfileId;

/**
 * Initialise instance with supplied string.
 *
 * @param profileString NSString representing the profile.
 *
 * @return Initialised instance.
 */
-(id)initWithString:(NSString*)profileString;

/**
 * Initialise instance with supplied profile ID.
 *
 * @param status The profile ID.
 *
 * @return Initialised instance.
 */
-(id)initWithProfileId:(LegicProfileId)profileId;

/**
 * Set the profile ID with the supplied string.
 *
 * @param profileString NSString representing the profile ID.
 */
-(void)setProfileIdWithString:(NSString*)profileString;

/**
 * Set the Profile ID.
 *
 * @param operation The profile ID.
 */
-(void)setProfileId:(LegicProfileId)profileId;

/**
 * Get the profile ID.
 *
 * @return The profile ID.
 */
-(LegicProfileId)getProfileId;

/**
 * Get the string that represents this profile ID in server calls.
 *
 * @return NSString that represents this profile ID for server calls.
 */
-(NSString*)getProfileIdString;

@end
