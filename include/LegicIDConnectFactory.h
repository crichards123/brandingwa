//
//  IDConnectFactory.h
//  legic-sdk
//
//  Created by Ian Stewart on 09/09/2014.
//  Copyright (c) 2014 Legic. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LegicIDConnectManager.h"
#import "LegicSeUIDelegate.h"

/**
 * A factory class containing static methods to use in order to create the LegicIDConnectManager instance. Note that once the instance has been created and return the instance must be stored by the caller. If create is called again without calling LegicIDConnectManager::finalise first then an exception will be thrown.
 *
 */
@interface LegicIDConnectFactory : NSObject

/**
 * Static method to create the LegicIDConnectManager instance.
 *
 * @param walletId     The Wallet ID.
 * @param profileIds   The profile IDs representing the available secure elements.
 * @param seUiDelegate The delegate for callbacks from the Bluetooth connector to the UI.
 *
 * @return LegicIDConnectManager The instance of the LegicIDConnectManager.
 *
 * @throws ManagerInitialisationException Thrown if the manager has already been initialised. You must call LegicIDConnectManager::finalise before a new instance can be created.
 * @throws IllegalDelegateException Thrown if the seUiDelegate provided does not properly implement the LegicSeUIDelegate protocol.
 */
+(LegicIDConnectManager*)createIDConnectManagerWithWalletId:(long)walletId profileIds:(NSArray*)profileIds seUiDelegate:(id<LegicSeUIDelegate>)seUiDelegate;

@end
