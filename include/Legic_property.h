//
//  Property.h
//  legic-server-client
//
//  Created by Ian Stewart on 11/06/2014.
//  Copyright (c) 2014 Legic. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 * Class to represent properties to be sent in Register Wallet server call.
 *
 */
@interface LegicProperty : NSObject

/**
 * Initialises the instance with the supplied key value pair.
 *
 * @param key    The key for the pair.
 * @param value  The value for the pair.
 *
 * @return The initialised instance.
 */
-(id)initWithKey:(NSString*)key andValue:(NSString*)value;

/**
 * Returns the property key.
 *
 * @return NSString containing the key.
 */
-(NSString*)getKey;

/**
 * Returns the property value.
 *
 * @return NSString containing the value.
 */
-(NSString*)getValue;

/**
 * Sets the property key.
 *
 * @param NSString containing the key.
 */
-(void)setKey:(NSString*)key;

/**
 * Sets the property value.
 *
 * @param NSString containing the value.
 */
-(void)setValue:(NSString*)value;

@end
