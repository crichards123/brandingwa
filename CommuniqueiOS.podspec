3
#
#  Be sure to run `pod spec lint CommuniqueiOS.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see http://docs.cocoapods.org/specification.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |s|

  s.name                = "CommuniqueiOS"

  s.version             = "1.2.3"

  s.platform            = :ios, '9.0'

  s.license             = { :type => "MIT", :file => "/Users/owner/documents/projects/iosworkspace/communique/communique/README" }

  s.summary             = "A codebase for future Communique related applications"

  s.homepage            = "https://bitbucket.org/communiquellc/communique-ios.git"

  s.source              = {
  		:git => "https://bitbucket.org/communiquellc/communique-ios.git",
  		:tag => s.version.to_s
  }

  s.source_files        =
  		"Communique",
      "Communique/*.{swift,h,m}",
  		"Communique/**/*.{swift,h,m,plist}",
  		"Communique/**/**/*.{swift,h,m}",
  		"Pods",
  		"*.{h,m,framework}"

  s.resources	          =
		"Communique/*.{otf,ttf,xcassets,storyboard,xib,json}",
  		"Communique/**/*.{otf,ttf,xcassets,storyboard,xib,json}",
  		"Communique/**/**/*.{otf,ttf,storyboard,xib,json}",
		"Communique/**/**/**/*.{otf,ttf,storyboard,xib,json}",
		"Communique/**/**/**/**/*.{otf,ttf,storyboard,xib,json}",
		"Communique/*.{}"

  
  s.dependency 'TTTAttributedLabel'
  s.dependency 'SVProgressHUD', '2.0.3'
  s.dependency 'DZNWebViewController'
  s.dependency 'hpple', '~> 0.2'
  s.dependency 'SDWebImage', '~>3.6'
  s.dependency 'TGLStackedViewController', '~> 1.0'
  s.dependency 'Masonry'
  s.dependency 'Parse'
  s.dependency 'ParseUI'
  s.dependency 'GoogleAnalytics'
  s.dependency 'Fabric'
  s.dependency 'Crashlytics'
  s.dependency 'JTAppleCalendar'
  #s.dependency 'UberRides'
  s.dependency 'NVActivityIndicatorView'
  s.dependency 'TesseractOCRiOS', '4.0.0'
  s.dependency 'lottie-ios'
  s.dependency 'CropViewController'
  #s.dependency 'GPUImage', '0.1.7'


  s.frameworks = 'SystemConfiguration','Security','CoreData','AdSupport','Fabric','Crashlytics', 'ExternalAccessory'
  
  s.libraries = 'z','sqlite3','c++','GoogleAnalytics','IDConnectSDK_V1.14.1.0'
  s.pod_target_xcconfig = {
				'GCC_PREPROCESSOR_DEFINITIONS' => '$(inherited)',
				'FRAMEWORK_SEARCH_PATHS' => '$(inherited) $(SRCROOT)/Crashlytics/iOS/ $(SRCROOT)/Fabric/iOS/',
				'MODULEMAP_PRIVATE_FILE' => '$(SRCROOT)/module.modulemap',
				'SWIFT_INCLUDE_PATHS' => '$(SRCROOT)/',
				'SWIFT_VERSION' => '3.2',
				'ENABLE_BITCODE' => 'NO',
				'GCC_NO_COMMON_BLOCKS' => 'NO',
				'LIBRARY_SEARCH_PATHS' => '$(SRCROOT)/GoogleAnalytics/Libraries/ $(SRCROOT)/legic-sdk/libs/'
				
  }
  

  s.prefix_header_file  = "Communique/Supporting Files/Communique-Prefix.pch"

  s.author              = { "Colby Melvin" => "colby@communique.us",
			    "Andre White" => "andre@communique.us"
  }

end
